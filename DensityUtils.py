import numpy as np
import matplotlib.pyplot as plt


def G(n, e, r):
    facs = [2.0, 24.0, 720.0, 40320.0, 3628800.0]
    fac = facs[n-1]**(-0.5)
    c = fac*(2.0*e)**(n+0.5)
    return c*r**(n-1)*np.exp(-e*r)

def DG(n, e, r):
    facs = [2.0, 24.0, 720.0, 40320.0, 3628800.0]
    fac = facs[n-1]**(-0.5)
    c = fac*(2.0*e)**(n+0.5)
    sg = c*r**(n-1)*np.exp(-e*r)
    return (-e + (n - 1)/r)*sg

def DDG(n, e, r):
    facs = [2.0, 24.0, 720.0, 40320.0, 3628800.0]
    fac = facs[n-1]**(-0.5)
    c = fac*(2.0*e)**(n+0.5)
    ssg = c*r**(n-1)*np.exp(-e*r)
    return ((-e + (n - 1)/r)**2 - (n-1)/r**2)*ssg