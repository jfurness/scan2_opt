import numpy as np
import Settings
from revinterp import get_coeffs,new_interp

K0 = 0.174
UAK = 0.12345679012345678#10/81

# Unmodified rSCAN X interpolation function pars
rscan_ief_cx = np.asarray([1.0,-0.667,-0.4445555,-0.663086601049, 1.451297044490,\
    -0.887998041597, 0.234528941479,-0.023185843322])
# Unmodified rSCAN C interpolation function pars
rscan_ief_cc = np.asarray([1.0,-0.64,-0.4352,-1.535685604549,3.061560252175,\
    -1.915710236206,0.516884468372,-0.051848879792])

def ief_select(x,pars,var,retderiv=False,cpoles=False):

    if var.lower() == 'x':
        exch = True
        f1,f2,h1,h2 = get_gex_pars(pars)
    elif var.lower() == 'c':
        exch = False
    else:
        raise SystemExit('Can only specify X/x or C/c for ief_select,\nnot {:}'.format(var))

    xorc = var.lower()
    dief = np.zeros_like(x)

    if Settings.Fint == 'NEW':

        if exch:

            c1 = 1 + pars['FX0']
            c4 = pars['DA']
            c2 = -5 + c4*pars['DX'] - 2*pars['FX0'] + (2*c4-3)*f1 + (1 + c4)*f2/2
            c3 = 3 - 2*c4*pars['DX'] + pars['FX0'] + (2 - 3*c4)*f1 - (1 + c4)*f2/2

            x2 = x*x
            x3 = x*x2
            x4 = x*x3

            iefden = 1 + c4*x4*x
            iefnum = 1 + c1*x + c2*x2 + c3*x3 + c4*pars['DX']*x4

            ief = (1 - x)*iefnum/iefden
            if retderiv:
                diefden = 5*c4*x4
                diefnum = c1 + 2*c2*x + 3*c3*x2 + 4*c4*pars['DX']*x3
                dief = (-iefnum + (1-x)*diefnum - diefden*ief)/iefden

        else:

            # interpolation function with control of fc'(0)
            x2 = x*x
            x5 = x2**2*x

            omx3 = (1-x)**3

            ief_num = 1 + (3 + pars['FC0'])*x + pars['DC']*x2
            ief_den = 1 + x5

            ief = omx3*ief_num/ief_den

            if retderiv:
                dief = (-3*(1 - x)**2*ief_num + omx3*( 3 + pars['FC0'] + 2*pars['DC']*x ) - 5*x**4*ief)/ief_den

    elif Settings.Fint == 'R2reopt':

        if exch:
            gepars = {'f1': f1,'f2': f2}
            use_stored = False
        else:
            gepars = {}
            use_stored = True
        cfs = get_coeffs(xorc,xpars=gepars,use_stored=use_stored)
        ief, dief = new_interp(x,cfs,xorc,ret_deriv=retderiv)

    elif Settings.Fint == 'SMTH' or (Settings.Fint == 'MVS' and exch):

        # exchange SMTH and MVS interpolation functions are the same

        if exch:

            if Settings.H1X in ['R2reopt','MVS']:
                c0 = pars['FX0']
                c1 = -(f1 + pars['DX']*(2 + f1*(1 + pars['FX0'])))/(f1 + pars['DX']*(f1 + 1))
            elif Settings.H1X == 'NEW':
                c0 = (2*pars['DX'] + 4*(1 + pars['DX'])*f1 + 2*(1 + pars['DX'])*f1**2 \
                    + (1 + pars['DX'])*f2)/(pars['DX']*f2 - 2*f1**2)
                c1 = -(2*pars['DX'] + (1 + pars['DX'] + c0*pars['DX'])*f1)\
                    /(pars['DX'] + (1 + pars['DX'])*f1)
            else:
                c0 = pars['FX0']
                c1 = pars['C1X']

        else:
            c0 = pars['C1C']
            c1 = pars['C2C']

        d = pars['D'+xorc.upper()]

        ccont = True
        if cpoles:
            ccont = check_poles([1,c0+c1,(1+c1)/d])

        if ccont:
            ief, dief = fsmth(x,c0,c1,d,rderiv = retderiv)

        else:
            # denominator has positive poles!
            ief = 1.e20*np.ones_like(x)
            dief[:] = ief

    elif Settings.Fint == 'MVS' and not exch:

        # correlation MVS interpolation has different structure than exchange

        c0 = (pars['C1C']-3)

        ccont = True
        if cpoles:
            ccont = check_poles([c0, pars['C2C'], 1/pars['DC']])

        if ccont:

            ief_num = (1 - x)**3
            ief_den = 1 + c0*x + pars['C2C']*x**2 + x**3/pars['DC']
            ief = ief_num/ief_den

            if retderiv:
                dnum = -3*(1-x)**2
                dden = c0 + 2*pars['C2C']*x + 3*x**2/pars['DC']
                dief = (dnum - dden*ief)/ief_den

        else:
            # denominator has positive poles!
            ief = 1.e20*np.ones_like(x)
            dief[:] = ief


    elif Settings.Fint in ['ORIGINAL','SCANNU']:

        c1 = pars['C1'+xorc.upper()]
        c2 = pars['C2'+xorc.upper()]
        d = pars['D'+xorc.upper()]

        if Settings.Fint == 'SCANNU':
            y = (1 + pars['NU'])*x
        else:
            y = x

        ief, dief = scan_ief(y,c1,c2,d,rderiv=retderiv)

    elif Settings.Fint in ['rSCAN','R2NU']:

        c1 = pars['C1'+xorc.upper()]
        c2 = pars['C2'+xorc.upper()]
        d = pars['D'+xorc.upper()]

        rpc = get_rpoly_coeff(c1,c2,d,2.5,osmth=3)

        if Settings.Fint == 'R2NU':
            y = (1 + pars['NU'])*x
        else:
            y = x

        ief, dief = rief(y,rpc,c2,d,x0=2.5,rderiv=retderiv)

    elif Settings.Fint == 'JPP':

        rp = (1 - x)/(1 + x)
        if exch:
            if Settings.H1X in ['R2reopt','MVS']:
                c = np.zeros(4)
                c[0] = -2*f1
                c[2] = (1 + pars['DX'])/2 - c[0]
                c[1] = (c[0] + 3*c[2] + 2*(1 - pars['DX']) - pars['FX0']/2)/2
                c[3] = (1 - pars['DX'])/2 - c[1]
            elif Settings.H1X == 'NEW':
                c = np.zeros(5)
                c[0] = -2*f1
                c[1] = 2*f2 - c[0]
                c[3] = (1 - xps['DX'])/2 - c[1]
                c[4] = (xps['FX0']/2 - 3*xps['DX'] + 2*c[0] - 5*c[1] - 7*c[3])/2
                c[2] = 1 - c[0] - c[1] - c[3] - c[4]

        else:
            c = np.zeros(4)
            c[0] = -2*pars['C2C']
            c[2] = (1 + pars['DC'])/2 - c[0]
            c[1] = (c[0] + 3*c[2] + 2*(1 - pars['DC']) - pars['C1C']/2)/2
            c[3] = (1 - pars['DC'])/2 - c[1]

        ief = np.zeros_like(x)
        for ic in range(c.shape[0]):
            ief += c[ic]*rp**(ic+1)

        if retderiv:
            drp = -2/(1 + x)**2
            for ic in range(c.shape[0]):
                dief += c[ic]*(ic+1)*rp**(ic)*drp

    elif Settings.Fint == 'BPOLY':

        if exch:
            c = np.zeros(4)
            c[0] = f1/2
            c[1] = (-pars['FX0'] + 7 + 4*c[0] - pars['DX'])/4
            c[2] = -(1 + pars['DX'] + 2*c[0])/2
            c[3] = (1 - pars['DX'] - 2*c[1])/2
        else:
            c = np.zeros(4)
            c[0] = pars['C2C']/2
            c[1] = (-pars['C1C'] + 7 + 4*c[0] - pars['DC'])/4
            c[2] = -(1 + pars['DC'] + 2*c[0])/2
            c[3] = (1 - pars['DC'] - 2*c[1])/2

        txm1 = 2*x - 1
        ief = txm1*c[3]
        for ic in range(1,c.shape[0]):
            ief = txm1*(c[c.shape[0]-ic-1] + ief)

        if retderiv:
            dief[:] = 4*c[3]
            for ic in range(1,c.shape[0]):

                dief = (c.shape[0]-ic)*c[c.shape[0]-ic-1] + txm1*dief
            dief *= 2

    elif Settings.Fint == 'BRAT':

        tb = 2*x

        if exch:
            ipow = 1
            c1 = pars['FX0']/2 - 1
            c2 = -4/f1 - 2*(1 + c1) - (1 - pars['DX']*(1 + 2*c1))/(4*pars['DX'])
            c3 = (1 - pars['DX']*(1 + 2*c1))/(8*pars['DX']) - c2/2

        else:
            ipow = 3
            c1 = pars['C1C']/2 - 3
            c2 = (1 - pars['DC']*(1 + 2*c1 + 8*pars['C2C']))/(4*pars['DC'])
            c3 = pars['C2C']

        ccont = True
        if cpoles:
            ccont = check_poles([1,2*c1,4*c2,8*c3],ubd=1)

        if ccont:

            ief_num = (1-tb)**ipow
            ief_den = 1 + tb*(c1 + tb*(c2 + tb*c3))
            ief = ief_num/ief_den

            if retderiv:
                regpow = max(0.0,ipow - 1)
                d_ief_num = -ipow*(1-tb)**regpow
                d_ief_den = c1 + tb*(2*c2 + tb*3*c3)
                dief = 2*(d_ief_num - d_ief_den*ief)/ief_den

        else:
            # denominator has positive poles!
            ief = 1.e20*np.ones_like(x)
            dief[:] = ief

    elif Settings.Fint == 'ASCAN':

        c1 = pars['C1'+xorc.upper()]
        c2 = pars['C2'+xorc.upper()]
        d = pars['D'+xorc.upper()]

        f1_den = 1 + x**3/d
        f1 = (1 - x)**3/f1_den

        f2_den = 1 - x + c2*x**2
        f2_int = (3-c1)*x/f2_den
        f2 = np.exp(f2_int)

        ief = f1*f2

        if retderiv:
            df1 = -( 3*(1-x)**2 + 3*x**2*f1/d )/f1_den
            df2_int = (3 - c1)*(1 - c2*x**2)/f2_den**2
            dief = (df1 + f1*df2_int)*f2

    elif Settings.Fint == 'ASCANpoly':

        c1 = pars['C1'+xorc.upper()]
        c2 = 1/pars['D'+xorc.upper()]**(2/3)

        ccont = True
        if cpoles:
            ccont = check_poles([1,c1,c2])

        if ccont:
            ief_den = (1 + c1*x + c2*x**2)**(3/2)
            ief = (1-x)**3/ief_den

            if retderiv:
                dief = -(3*(1 - x)**2 + 3/2*(c1 + 2*c2*x)*ief)/ief_den
        else:
            # denominator has positive poles!
            ief = 1.e20*np.ones_like(x)
            dief[:] = ief

    else:
        raise SystemError("ERROR: Typo in Fint,",Settings.Fint)

    if retderiv:
        return ief, dief

    return ief


def check_poles(c,ubd=None):
    # simple routine to check for poles in denominator of rational polynomial
    # assumes c are the coefficients of a polynomial P_n(x), where n=len(c)
    # P_n(x) = Sum_{i=0}^n c[i]*x^i
    tmp_rts = np.polynomial.polynomial.polyroots(c)
    no_pos_roots = True

    rmax = np.inf
    if ubd is not None:
        rmax = ubd

    for rt in tmp_rts:
        if rt.imag == 0.0 and rt > 0.0 and rt <= rmax:
            no_pos_roots = False
            break
    return no_pos_roots

"""
    Routines for enforcing exchange gradient expansion constraint
"""

def scan2_x_pars(eta):
    fa = 100*(4 + 9*eta)*K0/243
    fb = (200 + 6*K0*(1 + 441*eta))/729
    fc = (657*eta - 511)/6075

    f1 = (-fb - (fb**2 - 4*fa*fc)**(0.5) )/(2*fa)
    f2 = 73/(2500*K0) - f1/50
    h1 = 10/81 + 5*(4 + 9*eta)*K0*f1/27
    h2 = -2*(K0*f1*(6 + 75*eta*(8 + 9*eta))/243 + f1*h1*5*(8 + 9*eta)/27 + K0*f2*25/1458*(8 + 9*eta)**2)
    return f1,f2,h1,h2

def get_pars_from_g1g2(g1,g2,eta,k0):

    fa = 100/243*(4 + 9*eta)*k0
    fb = 200/729 - 20*(1 + k0)*g1/9 + 2*k0/243 + 98*eta*k0/27
    fc = 73*(8 + 9*eta)/6075 - 73/405

    rcnd = fb**2 - 4*fa*fc
    if rcnd < 0:
        return 1e20,1e20,1e20,1e20
    rcnd = rcnd**(0.5)
    f1 = np.asarray([(-fb - rcnd)/(2*fa), (-fb + rcnd)/(2*fa)])
    f2 = 73/(2500*k0) - f1/50
    h1 = 10/81 - g1 + 5*(4 + 9*eta)*k0*f1/27
    h2 = -g2 - 2*(h1*g1 + 25*(8+9*eta)**2*k0*f2/1458 \
        + 5*(8 +9*eta)*(h1*f1 - k0*f1*g1)/27 + (6 + 75*eta*(8 + 9*eta))*k0*f1/243)

    if f1[0] <= 0:
        return f1[0],f2[0],h1[0],h2[0]
    elif f2[0] <= 0:
        return f1[1],f2[1],h1[1],h2[1]
    else:
        return 1e20,1e20,1e20,1e20

def get_gex_pars(xps):

    f1 = None
    f2 = None
    h1 = None
    h2 = None

    if Settings.H1X in ['ADJGE2']:
        # not trying to recover GE constraint
        return f1,f2,h1,h2

    if Settings.H1X == 'NEW' or Settings.Fint == 'NEW':

        # intended to recover fourth-order expansion
        if Settings.GX == "POLY":
            g1 = 0.0
            g2 = -1/(4*xps['A1']**2)
        elif Settings.GX == "ORIGINAL":
            g1 = 0.0
            g2 = 0.0
        if Settings.ISOORB == 'A':
            f1,f2,h1,h2 = get_pars_from_g1g2(g1,g2,0.0,K0)
        elif Settings.ISOORB == 'BA':
            f1,f2,h1,h2 = get_pars_from_g1g2(g1,g2,xps['ETA'],K0)
        elif Settings.ISOORB == 'B':
            raise SystemExit("No one programmed the beta-dependent GEX4 yet :'(")

    else:

        """
            - Intended to recover second-order gradient expansion only.
            - f1 constrains first derivative of interpolation function evaluated on
            uniform density limit of iso-orbital indicator (1 for alpha and alpha_bar,
            1/2 for beta).
            - h1 constrains hx1'(p=0)
        """

        if Settings.Fint in ['ORIGINAL','ASCAN','ASCANpoly']:
            # interpolation function where derivative can't be constrained
            # note that h2 isn't determined here, the user needs to decide to
            # enforce GEX4
            f1 = 0.0
            f2 = 0.0
            h1 = UAK

        elif Settings.Fint == 'rSCAN':
            # interpolation function where derivative can't be constrained
            xrpc = get_rpoly_coeff(pars['C1X'],pars['C2X'],pars['DX'],2.5,osmth=3)
            f1 = -np.sum(xrpc)
            # need coefficients of p (cp) and q (cq) in gradient expansion of iso-orb indicator
            # r2 SCAN correction is then h1 = UAK - (cp + cq/3)*(hx0 - 1)*fx'(1)
            if Settings.ISOORB == 'A':
                cq = 20/9
                cp = -40/27
            elif Settings.ISOORB == 'BA':
                cq = 20/9
                cp = -5*(8 + 9*xps['ETA'])/27
            elif Settings.ISOORB == 'B':
                cq = 5/9
                cp = -85/108
            elif Settings.ISOORB == 'BNB':
                cq = 20/(9*(1 + xps['NU']))
                cp = -5*(8 + 9*xps['NU'] + 9*(1 - xps['NU'])*xps['ETA'])/(27*(1 + xps['NU']))

            h1 = UAK - (cp + cq/3)*K0*f1

        else:
            if Settings.H1X == 'MVS':
                delta = 0.0
                h1 = 0.0
            else:
                delta = xps['DA']
                h1 = UAK*xps['DA']

            if Settings.ISOORB == 'A':
                f1 = 27*(delta-1)*UAK/(20*K0)
            elif Settings.ISOORB == 'BA':
                f1 = 27*(delta-1)*UAK/(5*(4 + 9*xps['ETA'])*K0)
            elif Settings.ISOORB == 'B':
                f1 = 108*(delta-1)*UAK/(65*K0)

    return f1,f2,h1,h2

"""
    Routines for getting rSCAN-like interpolation function
"""

def get_rpoly_coeff(c1,c2,d,x,osmth=3):

    b0 = 1
    b1 = 1-c1
    b2 = c1*(c1 - 2)/2 + b1

    """
        rank x rank coefficient matrix to enforce
            d^j f / d x^j = d^j f_SCAN / d x^j,    j = 0,1,2,...,(rank-1)
        at matching point x

        rank = osmth+1
    """
    rank = osmth+1

    bmat = np.zeros((rank,rank))
    for i in range(rank):
        j = 3 + i
        bmat[0,i] = x**j - x**(j+1)
        bmat[1,i] = j*x**(j-1) - (j+1)*x**j
        bmat[2,i] = j*(j-1)*x**(j-2) - (j+1)*j*x**(j-1)
        if i == 0:
            bmat[3,i] = 6 - 24*x
            if osmth == 4:
                bmat[4,i] = -24
        else:
            bmat[3,i] = j*(j-1)*(j-2)*x**(j-3) - (j+1)*j*(j-1)*x**(j-2)
            if osmth == 4:
                bmat[4,i] = j*(j-1)*(j-2)*(j-3)*x**(j-4) - (j+1)*j*(j-1)*(j-2)*x**(j-3)

    """
        zeroth to fourth derivatives of SCAN x > 1 interpolation function
    """
    res = np.zeros(rank)
    omx = 1-x
    ief = -d*np.exp(c2/omx)
    d1 = ief*c2/omx**2
    d2 = (d1 + 2*ief/omx )*c2/omx**2
    d3 = (d2 + 2*d1/omx + 2*ief/omx**2)*c2/omx**2 + 2*d2/omx
    if osmth == 4:
        res[4] = (d3 + 2*d2/omx + 4*d1/omx**2 + 4*ief/omx**3)*c2/omx**2 \
            + 2*(d2 + 2*d1/omx + 2*ief/omx**2)*c2/omx**3 \
            + 2*d3/omx + 2*d2/omx**2

    """
        RHS of matrix equation
    """
    x2 = x*x
    res[0] = ief - (b0*(1-x) + b1*(x - x2) + b2*(x2 - x2*x))
    res[1] = d1 - (-b0 + b1*(1-2*x) + b2*(2*x - 3*x2))
    res[2] = d2 - (-2*b1 + 2*b2*(1 - 3*x))
    res[3] = d3 + 6*b2

    bl = np.zeros(3+rank)
    bl[:3] = [b0,b1,b2]
    bl[3:] = np.linalg.solve(bmat,res)

    return bl

def rief(x,bv,c2,d,rderiv=False,x0=2.5):
    nlp = len(bv)
    f = np.zeros_like(x)
    xmsk = x <= x0
    xm = x[xmsk]
    for i in range(nlp):
        f[xmsk] += bv[i]*xm**i
    f[xmsk] *= (1-xm)

    if rderiv:
        df = -bv[0]*np.ones_like(x)
        for i in range(1,nlp):
            df += bv[i]*(i - (i+1)*xm)*xm**(i-1)

    xmsk = x>x0
    xm = x[xmsk]
    f[xmsk] = -d*np.exp(c2/(1-xm))
    if rderiv:
        df[xmsk] = c2/(1-xm)**2*f[xmsk]

    return f, df

def rscan_ief(x,bv,c2,d,rderiv=False,x0=2.5):
    f = np.zeros_like(x)
    df = np.zeros_like(x)

    xmsk = x <= x0
    xm = x[xmsk]
    for i in range(8):
        f[xmsk] += bv[i]*xm**i
    if rderiv:
        for i in range(1,8):
            df[xmsk] += i*bv[i]*xm**(i-1)

    xmsk = x>x0
    xm = x[xmsk]
    f[xmsk] = -d*np.exp(c2/(1-xm))
    if rderiv:
        df[xmsk] = c2/(1-xm)**2*f[xmsk]
        return f,df

    return f, df

"""
    SCAN-like interpolation function
"""

def scan_ief(x,c1,c2,d,rderiv=False):
    f = np.zeros_like(x)
    df = np.zeros_like(x)

    xmsk = x < 1
    xm = x[xmsk]
    f[xmsk] = np.exp(-c1*xm/(1-xm))
    if rderiv:
        df[xmsk] = -c1/(1-xm)**2*f[xmsk]

    xmsk = x > 1
    xm = x[xmsk]
    f[xmsk] = -d*np.exp(c2/(1-xm))
    if rderiv:
        df[xmsk] = c2/(1-xm)**2*f[xmsk]
        return f,df

    return f, df


"""
    Smooth rSCAN-style interpolation
"""
def fsmth(x,c0,c1,d,rderiv=False):

    df = np.zeros_like(x)
    x2 = x*x
    f_num = (1 + c1*x - (1 + c1)*x2)
    f_den = (1 + (c0 + c1)*x + (1 + c1)*x2/d)

    f = f_num/f_den

    if rderiv:
        d_f_num = c1 - 2*(1+c1)*x
        d_f_den = (c0 + c1) + 2*(1+c1)*x/d
        df = (d_f_num - d_f_den*ief)/ief_den

    return f,df
