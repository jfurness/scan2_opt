import sys
from os import getcwd

# current working directory for file storage
WORK_DIR = getcwd()+"/"

EVALUATE_PUBLISHED_SCAN = False

NO_PROG = False# Should we show progress bars?

DO_BH6 = False  # Calculate BH6 score?
DO_AE6 = False # Load the AE6 densities. Faster not to if only atoms
DO_AR2 = False # Load the Ar2 densities.
DO_JELLIUM = True # Run the Jellium Calculation
JELLIUM_RS_LIST = [2, 3, 4, 6]  # Which rs values to run for Jellium

SCAN2_DENS = False   # If true, we load AE6, BH6, and Ar2 densities from set_dens/SCAN2/ instead
LOAD_LAP = True  # Should we try to read the Laplacian from the densities. Otherwise this data will be zeros!

FX_is_FC = False # True if fx and fc should have same parameters

X_C_DIFF_RANGE = True # True if fx and fc should search different grids
FLT_HLF = False # True to enforce f '(1/2) = 0, and makes D dependent upon A, B, and C
PARAMETER_SUM_CONSTRAINT = 0.6  # constrain |1 + A/2 + B/4 + C/8 + D/16| <= CONST, None ignores

NPROC = 4

MATCH_SCAN_END = False

Fint = "ASCANpoly" # "ORIGINAL", "BETA", "ONES", or "ZEROS". "TWOBETA", "JPLAP", "SMOOTH", "VSMTH", "CRUDE"
N_PARAMETERS = 25

H0X = "ORIGINAL"  # "MS0", "ORIGINAL", "MGG2"
H1X = "ORIGINAL"  # "ORIGINAL", "REGULARISED", "ORDER2", "COMPENSATED"

GX = "ORIGINAL"   # "ORIGINAL" Or "POLY"

ISOORB = 'BA'

CORRELATION = "R2"   # "ORIGINAL", "ACGGA", "MAXLOC", "MGG2", "COMPENSATED"
BETARS = 'ACGGA'
OPT_ACGGA = True       # Should we redetermine the single orbital parameters?

ERR_TOL_X = 0.5217   # Spherical atom exchange tollerance
ERR_TOL_C = 10.1632   # Same for correlation
ERR_TOL_XC = 0.3 # Tollerance for combined XC

NSP_ERR_TOL_X = None #1.6387  # Just trying to match SCAN against acGGA BM
HA_ERR_TOL_X = None #1.6256   # Again, just SCAN against HF

NSP_ERR_TOL_C = None #9.2035
HA_ERR_TOL_C = None #30.4658

SCAN_AE6_MAE = 4.00

# In kcal/mol
TOL_AE6 = 5.0
TOL_BH6 = None #10.00
TOL_AR2 = None

TOL_JELLIUM_X = None # RMSPE
TOL_JELLIUM_C = None
TOL_JELLIUM_XC = None

JELLIUM_REF_DATA = "QMC"

"""
format: PARDBS['PAR'][0] = 'x', 'c' or 'xc' :
    x to signal that par is needed for x only,
    c for c only
    xc for both x and c
PARDBS['PAR'][1] = min bound
PARDBS['PAR'][2] = max bound
"""
NRUN = 1
K1_STEP_SEARCH = False
POST_REF = True

N_LIN_PARS_X = 4
N_LIN_PARS_C = 4

PARBDS = {
#'ETA': ['xc',1.e-4,.05],#0.42005],
'K1': ['x',0.04,0.174],
'C1X': ['x',0.0,4.0],
#'C2X': ['x',0.26,5],
#'DP': ['xc',0.2, .5],
'DX': ['x',0.05,0.95/.174],
#'DA': ['x',0.0,1.0],
'C1C': ['c',0.0,4.0],
#'C2C' : ['c',0.26,5],
'DC': ['c',0.05,5]
}

#NORMWG = {'SA': 1/0.1583, 'NSA': 1/0.3352, 'LZBX': 1/0.0771,
#'LZBC': 1/0.0142, 'JS': 1/6.2542, 'AR2': 1/1.0654}
#NORMWG = {'SA': 1/0.1, 'NSA': 0.0, 'LZBX': 0.0, 'LZBC': 0.0, 'JS': 1/5, 'AR2': 0.0}
NORMWG = {'SA': 1/0.1583, 'NSA': 1/0.3352, 'LZBX': 1/5,
'LZBC': 1/5, 'JS': 1/3, 'AR2': 0.0}

PLOT_PARS_X = {'ETA': 0.000761715040622921, 'K1': 0.0226718754115067, 'FX0': 0.232556985363938,
'DP': 0.292874872100811, 'DX': 0.951527038472615, 'DA': 0.0287736922890905,
'A1': 4.948698660008248}
PLOT_PARS_C = {'ETA': 0.000761715040622921, 'DP': 0.292874872100811,
'C1C' : 0.149624227149202, 'C2C': -0.0711247560191589, 'DC': 0.519103924896288,
'B1C': 0.028576417228595823, 'B2C': 0.10754700025279458, 'B3C': 0.12554127065950266 }

# H1X SETTINGS
K1_MIN = 0.174
K1_MAX = 0.177
K1_STEP = 0.1

XREG_MIN = 1.0
XREG_MAX = 2.0
XREG_STEP = 3.0

CREG_MIN = 1.0
CREG_MAX = 2.0
CREG_STEP = 3.0


# Bounds for compensated XC
H1DX_MIN = 0.0001
H1DX_MAX = 0.001
H1DX_STEP = 0.0002

H1DC_MIN = 0.00001
H1DC_MAX = 0.0001
H1DC_STEP = 0.00005

# IE FUNCTION SETTINGS
A_MIN = -2.0
A_MAX = 2.0
A_STEP = 0.1

B_MIN = -5.0
B_MAX = 0.0
B_STEP = 0.1

F1_MIN = None#-6.32088 #-(-51.290924 + 1 + A_MIN + B_MIN)
F1_MAX = 0.0
F1_STEP = 0.125

C_MIN = 0
C_MAX = 5
C_STEP = 0.1

D_MIN = 0.0
D_MAX =  5.0
D_STEP = 0.1

ETA_MIN = 0.07
ETA_MAX = 5.0/3.0
ETA_STEP = 10.1

if X_C_DIFF_RANGE:
    C_A_MIN = -2.0
    C_A_MAX = 2.0
    C_A_STEP = 0.1

    C_B_MIN = -2.0
    C_B_MAX = 2.0
    C_B_STEP = 0.1

    C_C_MIN = -5.0
    C_C_MAX = 0.0
    C_C_STEP = 0.1

    C_D_MIN = -0.0
    C_D_MAX = 5.0
    C_D_STEP = 0.1

    C_ETA_MIN = 0.66
    C_ETA_MAX = 1.0
    C_ETA_STEP = 10.1

if Fint == "ORIGINAL" or Fint == "TWOBETA":
    C1X_MIN = 0.5
    C1X_MAX = 1.5
    C1X_STEP = 0.2

    C2X_MIN = 0.5
    C2X_MAX = 1.5
    C2X_STEP = 0.2

    DX_MIN = 0.5
    DX_MAX = 1.6
    DX_STEP = 0.2

    C1C_MIN = 0.5
    C1C_MAX = 1.6
    C1C_STEP = 0.2

    C2C_MIN = 0.5
    C2C_MAX = 1.5
    C2C_STEP = 0.2

    DC_MIN = 0.5
    DC_MAX = 1.5
    DC_STEP = 0.2


# if len(sys.argv) > 1:
#     try:
#         FX_is_FC = sys.argv[1] == "True"
#         Fint = str(sys.argv[2]).upper()
#         H1X = str(sys.argv[3]).upper()
#         GX = str(sys.argv[4]).upper()
#         CORRELATION = str(sys.argv[5]).upper()
#         K1_START = float(sys.argv[6])
#     except IndexError:
#         print "Missing some arguments. Taking defaults..."
#         pass
