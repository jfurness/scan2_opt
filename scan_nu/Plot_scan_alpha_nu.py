import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator, LogLocator)

scan_vals = dict(
    K1 = 0.065, A1 = 4.9479, C1X = 0.667, C2X= 0.8, DX = 1.24, B3X = 0.5,
    C1C = 0.64, C2C = 1.5, DC = 0.7, B1C = 0.0285764,
    B2C = 0.0889, B3C = 0.125541, B4C = 2.3631
)

pfls = {
    'Fixed': './scan_alpha_nu_fixed.csv',
    'Relaxed': './scan_alpha_nu_relaxed.csv'
}

pnorm1 = False
pnorm2 = True
ppar = False

fd = {}
rd = {}
for afl in pfls:
    dat = np.genfromtxt(pfls[afl],delimiter=',',skip_header=1)
    tfl = open(pfls[afl],'r')
    for iln,aln in enumerate(tfl):
        if iln == 0:
            for itmp,tmp in enumerate(aln.strip().split(',')):
                if afl == 'Fixed':
                    fd[tmp.strip()] = dat[:,itmp]
                elif afl == 'Relaxed':
                    rd[tmp.strip()] = dat[:,itmp]
            break
    tfl.close()

if pnorm1:
    fig,ax = plt.subplots(figsize=(6,4))

    ax.plot(fd['eta'],fd['SAXC']/fd['SAXC'][0],color='darkblue')
    ax.annotate('SA fix',(0.017,1.38),color='darkblue',rotation=18,fontsize=12)
    ax.plot(rd['eta'],rd['SAXC']/fd['SAXC'][0],color='darkblue',linestyle='--')
    ax.annotate('SA rel',(.04,.96),xytext=(.04,1.11),color='darkblue',rotation=0,\
    fontsize=12,arrowprops={'color': 'darkblue', 'arrowstyle':'->'})

    ax.plot(fd['eta'],fd['NSAXC']/fd['NSAXC'][0],color='darkorange')
    ax.annotate('NSA fix',(.02,1.23),color='darkorange',rotation=10,fontsize=12)
    ax.plot(rd['eta'],rd['NSAXC']/fd['NSAXC'][0],color='darkorange',linestyle='--')
    ax.annotate('NSA rel',(.028,1.06),color='darkorange',rotation=2,fontsize=12)

    ax.plot(fd['eta'],fd['JSFXC']/fd['JSFXC'][0],color='darkgreen')
    ax.annotate('JS fix',(.042,.78),color='darkgreen',rotation=-5,fontsize=12)
    ax.plot(rd['eta'],rd['JSFXC']/fd['JSFXC'][0],color='darkgreen',linestyle='--')
    ax.annotate('JS rel',(.03,1.0),xytext=(.018,.6),color='darkgreen',rotation=0,\
        fontsize=12,arrowprops={'color': 'darkgreen', 'arrowstyle':'->'})

    ax.plot(fd['eta'],fd['AR2']/fd['AR2'][0],color='purple')
    ax.annotate('Ar$_2$ fix',(.03,.7),color='purple',rotation=-8,fontsize=12)
    ax.plot(rd['eta'],rd['AR2']/fd['AR2'][0],color='purple',linestyle='--')
    ax.annotate('Ar$_2$ rel',(.04,.6),color='purple',rotation=2,fontsize=12)

    ax.set_xlim(fd['eta'][0],fd['eta'][-1])
    ax.hlines(1.0,*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    ax.set_xlabel('$\\eta$',fontsize=14)
    ax.set_ylabel('$\\frac{\\mathrm{Norm~ RMSPE}}{\\mathrm{SCAN~ Norm~ RMSPE}}$',\
        fontsize=14, rotation='horizontal')
    ax.yaxis.set_label_coords(0.0,1.03)

    ax.xaxis.set_minor_locator(MultipleLocator(0.001))
    ax.xaxis.set_major_locator(MultipleLocator(0.01))

    ax.yaxis.set_minor_locator(MultipleLocator(0.1))
    ax.yaxis.set_major_locator(MultipleLocator(0.5))

    plt.savefig('./SCAN_alpha_nu_norms1.pdf',dpi=600,bbox_inches='tight')
    #plt.show()

    plt.cla()
    plt.clf()
    plt.close()

if pnorm2:

    fig,ax = plt.subplots(figsize=(6,4))

    ax.plot(fd['eta'],np.abs(fd['LZBXpe']/fd['LZBXpe'][0]),\
        color='darkblue')
    ax.annotate('LZ $B_\\mathrm{x}$ fix',(.03,16.7),color='darkblue',fontsize=12,\
        rotation=8)
    ax.plot(rd['eta'],np.abs(rd['LZBXpe']/fd['LZBXpe'][0]),\
        color='darkblue',linestyle='--')
    ax.annotate('LZ $B_\\mathrm{x}$ rel',(.03,1.15),color='darkblue',fontsize=12)

    ax.plot(fd['eta'],np.abs(fd['LZBCpe']/fd['LZBCpe'][0]),\
        color='darkorange')
    ax.annotate('LZ $B_\\mathrm{c}$ fix',(.03,42.7),color='darkorange',fontsize=12,\
        rotation=8)
    ax.plot(rd['eta'],np.abs(rd['LZBCpe']/fd['LZBCpe'][0]),\
        color='darkorange',linestyle='--')
    ax.annotate('LZ $B_\\mathrm{c}$ rel',(.03,.59),color='darkorange',fontsize=12)

    ax.set_xlim(fd['eta'][0],fd['eta'][-1])
    ax.set_ylim(1.e-2,2.e2)
    ax.hlines(1.0,*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    ax.set_yscale('log')
    ax.set_xlabel('$\\eta$',fontsize=14)
    ax.set_ylabel('$\\left| \\frac{\\mathrm{Norm~ RMSPE}}{\\mathrm{SCAN~ Norm~ RMSPE}}\\right|$',\
        fontsize=14, rotation='horizontal')
    ax.yaxis.set_label_coords(0.02,1.03)

    ax.xaxis.set_minor_locator(MultipleLocator(0.001))
    ax.xaxis.set_major_locator(MultipleLocator(0.01))

    #ax.yaxis.set_major_locator(LogLocator(base=10.0,numticks=4))
    #ax.yaxis.set_minor_locator(LogLocator(base=10.0,subs=[.2,.4,.6,.8],numticks=14))
    #ax.yaxis.set_minor_formatter(NullFormatter())

    plt.savefig('./SCAN_alpha_nu_norms2.pdf',dpi=600,bbox_inches='tight')
    #plt.show()

    plt.cla()
    plt.clf()
    plt.close()

if ppar:
    fig,ax = plt.subplots(figsize=(6,4))

    par_order = ['K1','C1X','C2X','DX','B3X','C1C','C2C','DC']
    #clist = plt.cm.rainbow(np.linspace(0.0,1.0,len(par_order)))
    clist = ['darkblue','darkorange','darkgreen','tab:red','purple','k','teal']
    lsl = ['-','--','-.',':']

    jpar = 0
    for ipar,apar in enumerate(par_order):

        ax.plot(rd['eta'],rd[apar]/scan_vals[apar],color=clist[ipar%len(clist)],\
            linestyle=lsl[jpar])
        print(apar,clist[ipar%len(clist)],lsl[jpar])
        if ipar == len(clist)-1:
            jpar += 1

        mod_d = {'K1': [(.01,.9),0], 'DX': [(.045,1.15),5], 'C1X': [(.043,1.02,0.045,.93),0],
            'C2C': [(.043,1.32),5], 'DC': [(.042,1.77),0], 'B3X': [(.037,1.93),0],
            'C2X' : [(.04,1.07,0.036,1.3),0], 'C1C': [(.03,1.025,.027,.87),0]
        }
        if len(mod_d[apar][0]) == 2:
            ax.annotate(apar,mod_d[apar][0],rotation=mod_d[apar][1],\
                color=clist[ipar%len(clist)])
        elif len(mod_d[apar][0]) == 4:
            ax.annotate(apar,(mod_d[apar][0][0],mod_d[apar][0][1]),rotation=mod_d[apar][1],\
                color=clist[ipar%len(clist)],xytext=(mod_d[apar][0][2],mod_d[apar][0][3]),\
                arrowprops={'color': clist[ipar%len(clist)], 'arrowstyle': '->', 'lw':1})

    ax.set_xlim(rd['eta'][0],rd['eta'][-1])
    ax.set_ylim(.85,2.3)
    #ax.set_yscale('log')
    ax.hlines(1.0,*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    #ax.hlines(1.0/0.174/scan_vals['DX'],*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    ax.set_xlabel('$\\eta$',fontsize=14)
    ax.set_ylabel('$\\frac{\\mathrm{Rel~ Param}}{\\mathrm{SCAN~ Param}}$',\
        fontsize=14, rotation='horizontal')
    ax.yaxis.set_label_coords(0.02,1.03)

    ax.xaxis.set_minor_locator(MultipleLocator(0.001))
    ax.xaxis.set_major_locator(MultipleLocator(0.01))

    ax.yaxis.set_minor_locator(MultipleLocator(0.05))
    ax.yaxis.set_major_locator(MultipleLocator(0.2))

    plt.savefig('./SCAN_alpha_nu_rel_param.pdf',dpi=600,bbox_inches='tight')
    #plt.show()
