import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator, LogLocator)

scan_vals = dict(
    K1 = 0.065, A1 = 4.9479, C1X = 0.667, C2X= 0.8, DX = 1.24, B3X = 0.5,
    C1C = 0.64, C2C = 1.5, DC = 0.7, B1C = 0.0285764,
    B2C = 0.0889, B3C = 0.125541, B4C = 2.3631
)

pfls = {
    'Fixed': './scan_beta_nu_unrelaxed.csv',
    'Relaxed': './scan_beta_nu_relaxed_w_Ar2_bd_dx.csv'
}

pnorm1 = False
pnorm2 = True
ppar = False

fd = {}
rd = {}
for afl in pfls:
    dat = np.genfromtxt(pfls[afl],delimiter=',',skip_header=1)
    tfl = open(pfls[afl],'r')
    for iln,aln in enumerate(tfl):
        if iln == 0:
            for itmp,tmp in enumerate(aln.strip().split(',')):
                if afl == 'Fixed':
                    fd[tmp.strip()] = dat[:,itmp]
                elif afl == 'Relaxed':
                    rd[tmp.strip()] = dat[:,itmp]
            break
    tfl.close()

if pnorm1:
    fig,ax = plt.subplots(figsize=(6,4))

    ax.plot(fd['nu'],fd['SAXC']/fd['SAXC'][0],color='darkblue')
    ax.annotate('SA fix',(0.46,5.7),color='darkblue',rotation=20,fontsize=12)
    ax.plot(rd['nu'],rd['SAXC']/fd['SAXC'][0],color='darkblue',linestyle='--')
    ax.annotate('SA rel',(.81,1.62),color='darkblue',rotation=30,fontsize=12)

    ax.plot(fd['nu'],fd['NSAXC']/fd['NSAXC'][0],color='darkorange')
    ax.annotate('NSA fix',(.53,3.54),color='darkorange',rotation=10,fontsize=12)
    ax.plot(rd['nu'],rd['NSAXC']/fd['NSAXC'][0],color='darkorange',linestyle='--')
    ax.annotate('NSA rel',(.58,1.44),xytext=(.65,2.1),color='darkorange',rotation=0,\
        fontsize=12,arrowprops={'color': 'darkorange', 'arrowstyle':'->'})

    ax.plot(fd['nu'],fd['JSFXC']/fd['JSFXC'][0],color='darkgreen')
    ax.annotate('JS fix',(.83,.4),color='darkgreen',rotation=0,fontsize=12)
    ax.plot(rd['nu'],rd['JSFXC']/fd['JSFXC'][0],color='darkgreen',linestyle='--')
    ax.annotate('JS rel',(.91,1.1),color='darkgreen',rotation=0,fontsize=12)

    ax.plot(fd['nu'],fd['AR2']/fd['AR2'][0],color='purple')
    ax.annotate('Ar$_2$ fix',(.33,2.18),color='purple',rotation=30,fontsize=12)
    ax.plot(rd['nu'],rd['AR2']/fd['AR2'][0],color='purple',linestyle='--')
    ax.annotate('Ar$_2$ rel',(.82,5.0),color='purple',rotation=0,fontsize=12)

    ax.set_xlim(fd['nu'][0],fd['nu'][-1])
    ax.hlines(1.0,*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    ax.set_xlabel('$\\nu$',fontsize=14)
    ax.set_ylabel('$\\frac{\\mathrm{Norm~ RMSPE}}{\\mathrm{SCAN~ Norm~ RMSPE}}$',\
        fontsize=14, rotation='horizontal')
    ax.yaxis.set_label_coords(0.0,1.03)

    ax.xaxis.set_minor_locator(MultipleLocator(0.05))
    ax.xaxis.set_major_locator(MultipleLocator(0.2))

    ax.yaxis.set_minor_locator(MultipleLocator(0.2))
    ax.yaxis.set_major_locator(MultipleLocator(1))

    plt.savefig('./SCAN_beta_nu_norms1.pdf',dpi=600,bbox_inches='tight')
    #plt.show()

    plt.cla()
    plt.clf()
    plt.close()

if pnorm2:

    fig,ax = plt.subplots(figsize=(6,4))

    ax.plot(fd['nu'],np.abs(fd['LZBX PE']/fd['LZBX PE'][0]),\
        color='darkblue')
    ax.annotate('LZ $B_\\mathrm{x}$ fix',(.5,82),color='darkblue',fontsize=12)
    ax.plot(rd['nu'],np.abs(rd['LZBXpe']/fd['LZBX PE'][0]),\
        color='darkblue',linestyle='--')
    ax.annotate('LZ $B_\\mathrm{x}$ rel',(.59,1.15),color='darkblue',fontsize=12)

    ax.plot(fd['nu'],np.abs(fd['LZBC PE']/fd['LZBC PE'][0]),\
        color='darkorange')
    ax.annotate('LZ $B_\\mathrm{c}$ fix',(.09,.11),color='darkorange',fontsize=12)
    ax.plot(rd['nu'],np.abs(rd['LZBCpe']/fd['LZBC PE'][0]),\
        color='darkorange',linestyle='--')
    ax.annotate('LZ $B_\\mathrm{c}$ rel',(.61,7.17),color='darkorange',fontsize=12)

    ax.set_xlim(fd['nu'][0],fd['nu'][-1])
    ax.set_ylim(1.e-2,2.e2)
    ax.hlines(1.0,*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    ax.set_yscale('log')
    ax.set_xlabel('$\\nu$',fontsize=14)
    ax.set_ylabel('$\\left| \\frac{\\mathrm{Norm~ RMSPE}}{\\mathrm{SCAN~ Norm~ RMSPE}}\\right|$',\
        fontsize=14, rotation='horizontal')
    ax.yaxis.set_label_coords(0.02,1.03)

    ax.xaxis.set_minor_locator(MultipleLocator(0.05))
    ax.xaxis.set_major_locator(MultipleLocator(0.2))

    #ax.yaxis.set_major_locator(LogLocator(base=10.0,numticks=4))
    #ax.yaxis.set_minor_locator(LogLocator(base=10.0,subs=[.2,.4,.6,.8],numticks=14))
    #ax.yaxis.set_minor_formatter(NullFormatter())

    plt.savefig('./SCAN_beta_nu_norms2.pdf',dpi=600,bbox_inches='tight')
    #plt.show()

    plt.cla()
    plt.clf()
    plt.close()

if ppar:
    fig,ax = plt.subplots(figsize=(6,4))

    par_order = ['K1','C1X','C2X','DX','B3X','C1C','C2C','DC']
    #clist = plt.cm.rainbow(np.linspace(0.0,1.0,len(par_order)))
    clist = ['darkblue','darkorange','darkgreen','tab:red','purple','k','teal']
    lsl = ['-','--','-.',':']

    jpar = 0
    for ipar,apar in enumerate(par_order):

        ax.plot(rd['nu'],rd[apar]/scan_vals[apar],color=clist[ipar%len(clist)],\
            linestyle=lsl[jpar])
        if ipar == len(clist)-1:
            jpar += 1

        mod_d = {'K1': [(.77,.7),0], 'DX': [(.54,3.25),30], 'C1X': [(.9,.73),0],
            'C2C': [(.65,1.2),0], 'DC': [(.9,1.11),0], 'B3X': [(.54,1.5),0],
            'C2X' : [(.6,.47),0], 'C1C': [(.75,.96,.79,1.25),0]
        }
        if len(mod_d[apar][0]) == 2:
            ax.annotate(apar,mod_d[apar][0],rotation=mod_d[apar][1],\
                color=clist[ipar%len(clist)])
        elif len(mod_d[apar][0]) == 4:
            ax.annotate(apar,(mod_d[apar][0][0],mod_d[apar][0][1]),rotation=mod_d[apar][1],\
                color=clist[ipar%len(clist)],xytext=(mod_d[apar][0][2],mod_d[apar][0][3]),\
                arrowprops={'color': clist[ipar%len(clist)], 'arrowstyle': '->', 'lw':1})

    ax.set_xlim(rd['nu'][0],rd['nu'][-1])
    ax.set_yscale('log')
    ax.hlines(1.0,*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    ax.hlines(1.0/0.174/scan_vals['DX'],*ax.get_xlim(),color='gray',linestyle=':',linewidth=1)
    ax.set_xlabel('$\\nu$',fontsize=14)
    ax.set_ylabel('$\\frac{\\mathrm{Rel~ Param}}{\\mathrm{SCAN~ Param}}$',\
        fontsize=14, rotation='horizontal')
    ax.yaxis.set_label_coords(0.02,1.03)

    ax.xaxis.set_minor_locator(MultipleLocator(0.05))
    ax.xaxis.set_major_locator(MultipleLocator(0.2))

    plt.savefig('./SCAN_beta_nu_rel_param.pdf',dpi=600,bbox_inches='tight')
    #plt.show()
