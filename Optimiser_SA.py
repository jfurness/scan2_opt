import Settings
import numpy as np
import os
from math import pi, e
from time import time
from H_ex import set_a1x, set_b1c, set_ec0_lda_pars_scan2, chi2_chi4_b1c_adaptive, set_original_SCAN, set_c_in_ms0_h0x
# from Jellium import filter_jellium, filter_jellium_SCAN2, get_err_jellies_for_scan, get_err_jellies_for_scan2
from Atomics import get_rmse
from TestSet import assess_ae6_for_scan2, assess_bh6_for_scan2, assess_ae6_for_scan, assess_bh6_for_scan
from Ar2_pscf import assess_ar2, jsextr
from Jellium import Jelly
from scipy.optimize import minimize,differential_evolution
from Integrators import test_linear_composition, make_linear_energy_components, get_Ex_for_atoms, get_Ec_for_atoms, get_Ec_non_colinear
from Densities import Atom, GridGenerator
import Benchmarks
from DownhillIE import run_min
import h5py
import SCAN2
from itertools import product
from glob import glob
#from Compensated_opt import opt_main
from Parameters import get_pretty_coefficients

from multiprocessing import Pool
from functools import partial
from int_ex import get_lzlx_coef
from int_ec import get_lzlc_coef

"""
Main program

We actually enter much lower, after all function definitions have been made
"""

# check we are running the right version of TURBOMOLE
# tm = os.environ['TURBODIR']
# if tm != "/home/jfurness/TURBOMOLE":
#     raise SystemExit("Wrong Turbomole in bash profile!")

def search_SCAN2(xparams, cparams, dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, ar2_dens):
    """
    Core optimisation for SCAN2
    """

    print("\n-\tXC Parameter Optimisation")
    print("-\t------------------------")
    # call routines to filter X/C IE function parameters by atomic X/C energies
    # At this point they are independent and stored in xolist and colist for x/c respectively

    # Divert for downhill optimisations
    if Settings.Fint == "TWOBETA":
        xparams["K1"] = Settings.K1_MIN
        run_min(dens, non_spherical_dens, ae6_dens, xparams, cparams, None, None, None)
        return

    # Lists of keys for linear parameters. ALL MUST BE LINEAR
    XP = ["CAX","CBX","CCX","CDX"]
    CP = ["CAC","CBC","CCC","CDC"]

    stime = time()

    # Create filtered list of all linear parameters
    params = get_pretty_coefficients(xparams)

    k1_l = np.arange(Settings.K1_MIN, Settings.K1_MAX, Settings.K1_STEP)

    if Settings.Fint == "JPLAP":
        p1_l = np.arange(Settings.XREG_MIN, Settings.XREG_MAX, Settings.XREG_STEP)
        if Settings.FX_is_FC:
            p2_l = [None]
        else:
            p2_l = np.arange(Settings.CREG_MIN, Settings.CREG_MAX, Settings.CREG_STEP)
    elif Settings.Fint == "SMOOTH":
        p1_l = np.arange(Settings.ETA_MIN, Settings.ETA_MAX, Settings.ETA_STEP)
        if Settings.FX_is_FC:
            p2_l = [None]
        else:
            if Settings.X_C_DIFF_RANGE:
                p2_l = np.arange(Settings.C_ETA_MIN, Settings.C_ETA_MAX, Settings.C_ETA_STEP)
            else:
                p2_l = np.arange(Settings.ETA_MIN, Settings.ETA_MAX, Settings.ETA_STEP)
    elif Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
        p1_l = [None]   # We need at least a single element in each
        p2_l = [None]   # to enter the loop. Despite no NL parameters

    # Create an outer product in all the non-linear parameters
    nl_params = list(product(p1_l, p2_l))

    # Setup the jellium objects
    jellies = []
    if Settings.DO_JELLIUM:
        for rs in Settings.JELLIUM_RS_LIST:
            jellies.append(Jelly(rs))

    # Now we have the major loop over all these sets.
    for k in k1_l:
        print("\n==========================================\n")
        xparams['K1'] = k
        print("K1 is                 :", xparams['K1'])
        assert xparams['K1'] <= 0.174, "K1 is out of bounds!"

        if Settings.Fint == "JPLAP" or Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
            if Settings.X_C_DIFF_RANGE:
                lin_params_x = check_non_positivity(params[0], xparams)
                lin_params_c = params[1]
            else:
                lin_params_c = []+params
                lin_params_x = check_non_positivity(params, xparams)
        else:
            lin_params = params

        # This diverts us into a downhill optimisation of eta_x, eta_c
        # run_min(dens, non_spherical_dens, ae6_dens, xparams, cparams, lin_params_x, lin_params_c, filter_x_diff_c)

        for nlp in nl_params:
            print("\n------------------------------------------\n")

            if Settings.Fint == "JPLAP":
                xparams['FREG'] = nlp[0]
                if Settings.FX_is_FC:
                    cparams['FREG'] = nlp[0]
                else:
                    cparams['FREG'] = nlp[1]
                print("XREG is               :", xparams['FREG'])
                print("CREG is               :", cparams['FREG'])
            elif Settings.Fint == "SMOOTH":
                xparams['XETA'] = nlp[0]
                if Settings.FX_is_FC:
                    cparams['CETA'] = nlp[0]
                else:
                    cparams['CETA'] = nlp[1]
                print("XETA is               :", xparams['XETA'])
                print("CETA is               :", cparams['CETA'])

            if Settings.H1X == "REGULARISED":
                xparams['B1X'] = b1x
            elif Settings.H1X == "ORIGINAL":
                xparams['B1X'] = 0.156632
            elif Settings.H1X == "ORDER2":
                xparams['B1X'] = 0.0
                xparams['B2X'] = 0.0
                xparams['B3X'] = 0.0
                xparams['B4X'] = 0.0

            print("Using IE func         :", Settings.Fint)
            print("fx is the same as fc  :", Settings.FX_is_FC)
            print("Exchange h1x is       :", Settings.H1X)
            print("GX in exchange is     :", Settings.GX)
            print("Correlation is        :", Settings.CORRELATION)
            print("Exchange filter is    :", Settings.ERR_TOL_X)
            print("Correlation filter is :", Settings.ERR_TOL_C)
            print("XC filter is          :", Settings.ERR_TOL_XC)

            print("\nGenerating integrated components...")
            lin_exl, lin_ecl = make_linear_energy_components(dens, xparams, cparams, XP, CP)
            nsp_lin_exl, nsp_lin_ecl = make_linear_energy_components(non_spherical_dens, xparams, cparams, XP, CP, non_colin=True)

            if Settings.DO_AE6:
                ae6_lin_exl, ae6_lin_ecl = make_linear_energy_components(ae6_dens, xparams, cparams, XP, CP, non_colin=True)
            else:
                ae6_lin_exl, ae6_lin_ecl = ([], [])

            if Settings.DO_BH6:
                bh6_lin_exl, bh6_lin_ecl = make_linear_energy_components(bh6_dens, xparams, cparams, XP, CP, non_colin=True)
            else:
                bh6_lin_exl, bh6_lin_ecl = ([], [])

            if Settings.DO_AR2:
                ar2_lin_exl, ar2_lin_ecl = make_linear_energy_components(ar2_dens, xparams, cparams, XP, CP, non_colin=True)
            else:
                ar2_lin_exl, ar2_lin_ecl = ([], [])

            if Settings.DO_JELLIUM:
                for j in jellies:
                    j.make_linear_energy_components(SCAN2.getscan_x, SCAN2.getscan_c, xparams, cparams, XP, CP)


            # Setup output file
            directory = Settings.WORK_DIR
            nstr = "XisC_" if Settings.FX_is_FC else "XnoC_"
            nstr += Settings.Fint[:4]+"_"
            nstr += Settings.H1X[:4]+"_"
            nstr += Settings.GX[:4]+"_"
            nstr += Settings.CORRELATION[:4]+"_"
            if Settings.Fint == "JPLAP":
                nstr = directory+nstr+"k{:}_rx{:}_rc{:}.csv".format(xparams['K1'], xparams['FREG'], cparams['FREG'])
            elif Settings.Fint == "SMOOTH":
                nstr = directory+nstr+"k{:}_etax{:}_etac{:}.csv".format(xparams['K1'], xparams['XETA'], cparams['CETA'])
            else:
                nstr = directory+nstr+"k{:}.csv".format(xparams['K1'])
            print("Saving results to: ", nstr)

            with open(nstr, 'w') as ofile:
                tstr = ",Interpolation,fx = fc,h0x,h1x,gx,correlation,"
                tstr += "CAX,CBX,CCX,CDX,CAC,CBC,CCC,CDC,"
                tstr += "k1,b1x,b2x,b3x,b4x,a1x,b1c,b2c,b3c,"
                if Settings.Fint == "JPLAP":
                    tstr += "xreg,creg,"
                elif Settings.Fint == "SMOOTH":
                    tstr += "etax,etac,"
                if Settings.DO_AE6:
                    tstr += "AE6 ME,AE6 MAE,"
                if Settings.DO_BH6:
                    tstr += "BH6 ME,BH6 MAE,"
                tstr += "Atoms X,Atoms C,Atoms XC,"
                tstr += "NSP X,NSP C,NSP XC,"
                if Settings.DO_AR2:
                    tstr += ("Ar2_{:},"*len(Benchmarks.AR2_SEPS)).format(*Benchmarks.AR2_SEPS)
                if Settings.DO_JELLIUM:
                    tstr += "Jellium X,Jellium C,Jellium XC,"
                tstr += ("{:},"*len(Benchmarks.BM_LIST)).format(*Benchmarks.BM_LIST)
                tstr += ("{:},"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*Benchmarks.NON_SPHERICAL_LIST)
                if Settings.DO_AE6:
                    tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
                    tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
                if Settings.DO_BH6:
                    tstr += ("{:},"*len(Benchmarks.BH6_LIST)).format(*Benchmarks.BH6_LIST)
                    tstr += ("{:},"*len(Benchmarks.BH6_TESTS)).format(*Benchmarks.BH6_TESTS)
                if Settings.DO_JELLIUM:
                    tstr += ("Jellium XC {:},"*len(Settings.JELLIUM_RS_LIST)).format(*Settings.JELLIUM_RS_LIST)
                ofile.write(tstr+"\n")

                # We are now ready to search in the linear parameters
                # How we go about this depends if x is c or not.
                # So split off to separate functions.
                if Settings.FX_is_FC:
                    print("Trying {:} parameters.".format(len(lin_params_x)))
                    filter_x_is_c(lin_params_x, xparams, cparams, XP, CP, lin_exl, lin_ecl, nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl, bh6_lin_exl, bh6_lin_ecl, ar2_lin_exl, ar2_lin_ecl, jellies, ofile)
                else:
                    filter_x_diff_c(lin_params_x, lin_params_c, xparams, cparams, XP, CP, lin_exl, lin_ecl, nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl, bh6_lin_exl, bh6_lin_ecl, ar2_lin_exl, ar2_lin_ecl, jellies, ofile)

    rtime = time() - stime

    print("\n===================")
    print("k1 search komplete!")
    print("===================")
    print("Identified {:} possible candidates in {:.3f}s.".format(len(candidates), rtime))

    #print "-\tFound {:} X candidates and {:} C candidates in {:.3f}s".format(len(xolist), len(colist), rtime)
    # Now use Jellium surfaces as a filter to remove failing combinations from xolist and colist.
    # jl_x, jl_c contain the sets that pass.
    # X and C are still independent at this point
    # print "\n-\tJellium XC Filter"
    # print "-\t------------------------"
    # stime = time()
    # jl_x, jl_c = filter_jellium_SCAN2(xparams, cparams, XP, CP, xolist, colist, dens)
    # rtime = time() - stime
    # print "-\tFound {:} X candidates and {:} C candidates in {:.3f}s".format(len(jl_x), len(jl_c), rtime)

    # # We must decide on a set of X and C to use in combination. This is potentially a huge task as
    # # the outer product jl_x (X) jl_c can be huge.
    # best_comb = get_best_xc_combined(dens, non_spherical_dens, ha_dens, jl_x, jl_c, xparams, cparams, XP, CP)

    # Currently, this is the end of the filtering, so we return the whole list.
    # TODO: Add further norms to better filter list down.
    # return jl_x, jl_c
    return candidates


def filter_x_is_c(params, xparams, cparams, XP, CP,
        lin_exl, lin_ecl, nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl, bh6_lin_exl, bh6_lin_ecl, ar2_lin_exl, ar2_lin_ecl, jellies,
        ofile):
    tot = len(params)

    n_passed = 0

    best_atom_xc = 1e20
    best_nsp_xc = 1e20
    best_ae6_mae = 1e20
    best_bh6_mae = 1e20
    best_ar2_max = 1e20

    for p in tqdm(params, total=tot, ascii=True, disable=Settings.NO_PROG):
        for i, n in enumerate(XP):
            xparams[n] = p[i]
        for i, n in enumerate(CP):
            cparams[n] = p[i]


        # Spherical atom exchange
        exl, err_x, rms_x = get_rmse(lin_exl, xparams, XP, Benchmarks.BM_X, correlation=False)
        if Settings.ERR_TOL_X is not None and abs(rms_x) > Settings.ERR_TOL_X:
            # Failed at Exchange check
            continue

        # Spherical atom correlation
        ecl, err_c, rms_c = get_rmse(lin_ecl, cparams, CP, Benchmarks.BM_C, correlation=True)
        if Settings.ERR_TOL_C is not None and abs(rms_c) > Settings.ERR_TOL_C:
            # Failed at correlation check
            continue

        # Spherical atom exchange+correlation
        excl = exl + ecl
        err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
        rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))
        if rms_xc < best_atom_xc:
                best_atom_xc = rms_xc
        if Settings.ERR_TOL_XC is not None and abs(rms_xc) > Settings.ERR_TOL_XC:
            # Failed at the combined XC filter, if it was set
            continue

        # Non-spherical atom exchange
        nsp_exl, nsp_err_x, nsp_rms_x = get_rmse(nsp_lin_exl, xparams, XP, Benchmarks.NON_SPHERICAL_X, correlation=False)
        if Settings.NSP_ERR_TOL_X is not None and abs(nsp_rms_x) > Settings.NSP_ERR_TOL_X:
            continue

        # Non-spherical atom correlation
        nsp_ecl, nsp_err_c, nsp_rms_c = get_rmse(nsp_lin_ecl, cparams, CP, Benchmarks.NON_SPHERICAL_C, correlation=True)
        if Settings.NSP_ERR_TOL_C is not None and abs(nsp_rms_c) > Settings.NSP_ERR_TOL_C:
            continue

        # Non-spherical atom exchange-correlation
        nsp_excl = nsp_exl + nsp_ecl
        nsp_err_xc = 100*(Benchmarks.NON_SPHERICAL_XC - nsp_excl)/np.abs(Benchmarks.NON_SPHERICAL_XC)
        nsp_rms_xc = np.sqrt(np.mean(np.array(nsp_err_xc)**2))
        if nsp_rms_xc < best_nsp_xc:
            best_nsp_xc = nsp_rms_xc

        if Settings.DO_AE6:
            # AE6 Test set
            ae6_err, ae6_etot = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
            ae6_me = np.mean(ae6_err)
            ae6_mae = np.mean(np.abs(ae6_err))

            if ae6_mae < best_ae6_mae:
                best_ae6_mae = ae6_mae

            if abs(ae6_mae) > abs(Settings.TOL_AE6):
                # Failed at AE6 test
                continue

        if Settings.DO_BH6:
            bh6_err, bh6_etot = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP)
            bh6_me = np.mean(bh6_err)
            bh6_mae = np.mean(np.abs(bh6_err))

            if bh6_mae < best_bh6_mae:
                best_bh6_mae = bh6_mae

            if Settings.TOL_BH6 is not None and bh6_mae > Settings.TOL_BH6:
                # Failed at BH6 test
                continue

        if Settings.DO_AR2:
            ar2_err = assess_ar2(ar2_lin_exl, ar2_lin_ecl, xparams, cparams, XP, CP)
            ar2_max = np.max(np.abs(ar2_err))

            if ar2_max < best_ar2_max:
                best_ar2_max = ar2_max

            if Settings.TOL_AR2 is not None and ar2_max > Settings.TOL_AR2:
                # Failed at Ar2 test
                continue

        if Settings.DO_JELLIUM:
            jellium_exl = []
            jellium_ecl = []
            jerr_xs = []
            jerr_cs = []
            jerr_xcs = []
            for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                ex = jellies[i].get_Ex_from_lin(xparams, XP)
                jellium_exl.append(ex)
                jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
                ec = jellies[i].get_Ec_from_lin(cparams, CP)
                jellium_ecl.append(ec)
                jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ec)/abs(Benchmarks.JELLIUM_C[i]))
                exc = jellium_exl[i] + ec
                jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

            jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
            jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
            jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_xcs)**2))

            if Settings.TOL_JELLIUM_X is not None and jellium_rms_c > Settings.TOL_JELLIUM_C:
                # Failed at Jellium correlation test
                continue
            if Settings.TOL_JELLIUM_C is not None and jellium_rms_c > Settings.TOL_JELLIUM_C:
                # Failed at Jellium correlation test
                continue
            if Settings.TOL_JELLIUM_XC is not None and jellium_rms_xc > Settings.TOL_JELLIUM_XC:
                # Failed at Jellium exchange-correlation test
                continue

        # Passed all filters! Record to file

        ost = ",{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H1X, Settings.GX, Settings.CORRELATION)
        ost += "{:},{:},{:},{:},".format(xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'])
        ost += "{:},{:},{:},{:},".format(cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC'])
        ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
        if Settings.Fint == "SMOOTH" :
            ost += "{:},{:},".format(xparams['XETA'],cparams['CETA'])
        elif Settings.Fint == "JPLAP" :
            ost += "{:},{:},".format(xparams['FREG'],cparams['FREG'])
        if Settings.DO_AE6:
            ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
        if Settings.DO_BH6:
            ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x, rms_c, rms_xc)
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(nsp_rms_x, nsp_rms_c, nsp_rms_xc)
        if Settings.DO_AR2:
            ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
        if Settings.DO_JELLIUM:
            ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(jellium_rms_x, jellium_rms_c, jellium_rms_xc)
        ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
        ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*nsp_err_xc)
        if Settings.DO_AE6:
            ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
            ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
        if Settings.DO_BH6:
            ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
            ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
        if Settings.DO_JELLIUM:
            ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xcs)
        ofile.write(ost+"\n")
        n_passed += 1

    print("We have {:} sets that passed all filters.".format(n_passed))
    print("Best Atomic XC was ", best_atom_xc)
    print("Best NSP XC was ", best_nsp_xc)
    if Settings.DO_AE6:
        print("Best AE6 MAE was   ", best_ae6_mae)
    if Settings.DO_BH6:
        print("Best BH6 MAE was   ", best_bh6_mae)
    if Settings.DO_AR2:
        print("Best Ar2 max was   ", best_ar2_max)
    return


def filter_x_diff_c(x_list, c_list, xparams, cparams, XP, CP,
        lin_exl, lin_ecl, nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl, bh6_lin_exl, bh6_lin_ecl, ar2_lin_exl, ar2_lin_ecl, jellies,
        ofile):
    tot = len(c_list)
    candidates = []

    print("Initial lists are {:} X and {:} C.".format(len(x_list), len(c_list)))

    print("Pre Filtering exchange list by spherical atom")
    txl = []
    for xp in x_list:
        for i, n in enumerate(XP):
            xparams[n] = xp[i]
        exl, err_x, rms_x = get_rmse(lin_exl, xparams, XP, Benchmarks.BM_X, correlation=False)
        if Settings.ERR_TOL_X is not None and abs(rms_x) > Settings.ERR_TOL_X:
            # If an exchange filter was set, we failed it
            continue
        xp.append(exl)
        xp.append(rms_x)
        txl.append(xp)

    print("{:} passed spherical exchange of original {:}".format(len(txl), len(x_list)))
    x_list = txl

    if Settings.DO_JELLIUM:
        print("Pre Filtering exchange list by jellium surface")
        txl = []
        for xp in x_list:
            for i, n in enumerate(XP):
                xparams[n] = xp[i]
            jerr_xs = []
            jellium_exl = []
            for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                ex = jellies[i].get_Ex_from_lin(xparams, XP)
                jellium_exl.append(ex)
                jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
            jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
            if Settings.TOL_JELLIUM_X is not None and jellium_rms_x < Settings.TOL_JELLIUM_X:
                    # Failed at Jellium corrleation test
                    continue
            xp.append(jellium_exl)
            xp.append(jellium_rms_x)
            txl.append(xp)

        print("{:} remain from original {:}".format(len(txl), len(x_list)))

    x_list = txl

    print("Total to do is now {:}".format(len(txl)*len(c_list)))
    if len(x_list) == 0:
        print("All failed exchange filters.")
        return

    n_passed = 0

    best_atom_xc = 1e20
    best_nsp_xc = 1e20
    best_ae6_mae = 1e20
    best_bh6_mae = 1e20
    best_ar2_max = 1e20

    for cp in tqdm(c_list, total=tot, ascii=True, miniters=1, disable=Settings.NO_PROG):
        for i, n in enumerate(CP):
            cparams[n] = cp[i]
        ecl, err_c, rms_c = get_rmse(lin_ecl, cparams, CP, Benchmarks.BM_C, correlation=True)
        if Settings.ERR_TOL_C is not None and abs(rms_c) > Settings.ERR_TOL_C:
            # If a correlation filter was set, we failed it
            continue
        # Non-spherical atom correlation
        nsp_ecl, nsp_err_c, nsp_rms_c = get_rmse(nsp_lin_ecl, cparams, CP, Benchmarks.NON_SPHERICAL_C, correlation=True)
        if Settings.NSP_ERR_TOL_C is not None and abs(nsp_rms_c) > Settings.NSP_ERR_TOL_C:
            continue

        for xp in x_list:
            for i, n in enumerate(XP):
                xparams[n] = xp[i]

            if Settings.DO_JELLIUM:
                exl = xp[-4]
                rms_x = xp[-3]
                jellium_exl = xp[-2]
                jellium_rms_x = xp[-1]
            else:
                # No offset
                exl = xp[-2]
                rms_x = xp[-1]

            excl = exl + ecl
            err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
            rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))
            if rms_xc < best_atom_xc:
                best_atom_xc = rms_xc
            if Settings.ERR_TOL_XC is not None and abs(rms_xc) > Settings.ERR_TOL_XC:
                # Failed at the combined XC filter, if it was set
                continue

            # Now non-spherical exchange and exchange-correlation
            nsp_exl, nsp_err_x, nsp_rms_x = get_rmse(nsp_lin_exl, xparams, XP, Benchmarks.NON_SPHERICAL_X, correlation=False)
            if Settings.NSP_ERR_TOL_X is not None and abs(nsp_rms_x) > Settings.NSP_ERR_TOL_X:
                continue
            nsp_excl = nsp_exl + nsp_ecl
            nsp_err_xc = 100*(Benchmarks.NON_SPHERICAL_XC - nsp_excl)/np.abs(Benchmarks.NON_SPHERICAL_XC)
            nsp_rms_xc = np.sqrt(np.mean(np.array(nsp_err_xc)**2))
            if nsp_rms_xc < best_nsp_xc:
                best_nsp_xc = nsp_rms_xc

            if Settings.DO_AE6:
                # Now test AE6,
                ae6_err, ae6_etot = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
                ae6_me = np.mean(ae6_err)
                ae6_mae = np.mean(np.abs(ae6_err))

                if ae6_mae < best_ae6_mae:
                    best_ae6_mae = ae6_mae

                if Settings.TOL_AE6 is not None and abs(ae6_mae) > abs(Settings.TOL_AE6):
                    # Failed at AE6 Test filter
                    continue

            if Settings.DO_BH6:
                bh6_err, bh6_etot = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP)
                bh6_me = np.mean(bh6_err)
                bh6_mae = np.mean(np.abs(bh6_err))

                if bh6_mae < best_bh6_mae:
                    best_bh6_mae = bh6_mae

                if Settings.TOL_BH6 is not None and bh6_mae > Settings.TOL_BH6:
                    # Failed at BH6 test
                    continue

            if Settings.DO_AR2:
                ar2_err = assess_ar2(ar2_lin_exl, ar2_lin_ecl, xparams, cparams, XP, CP)

                ar2_max = np.max(np.abs(ar2_err))

                if ar2_max < best_ar2_max:
                    best_ar2_max = ar2_max

                if Settings.TOL_AR2 is not None and ar2_max > Settings.TOL_AR2:
                    # Failed at Ar2 test
                    continue

            if Settings.DO_JELLIUM:
                jellium_ecl = []
                jerr_cs = []
                jerr_xcs = []
                for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                    ec = jellies[i].get_Ec_from_lin(cparams, CP)
                    jellium_ecl.append(ec)
                    jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ec)/abs(Benchmarks.JELLIUM_C[i]))
                    exc = jellium_exl[i] + ec
                    jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

                jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
                jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_xcs)**2))

                if Settings.TOL_JELLIUM_C is not None and jellium_rms_c > Settings.TOL_JELLIUM_C:
                    # Failed at Jellium correlation test
                    continue
                if Settings.TOL_JELLIUM_XC is not None and jellium_rms_xc > Settings.TOL_JELLIUM_XC:
                    # Failed at Jellium exchange-correlation test
                    continue

            # Passed all filters, write to file!

            ost = ",{:},{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H0X, Settings.H1X, Settings.GX, Settings.CORRELATION)
            ost += "{:},{:},{:},{:},".format(xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'])
            ost += "{:},{:},{:},{:},".format(cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC'])
            ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
            if Settings.Fint == "SMOOTH" :
                ost += "{:},{:},".format(xparams['XETA'],cparams['CETA'])
            elif Settings.Fint == "JPLAP" :
                ost += "{:},{:},".format(xparams['FREG'],cparams['FREG'])
            if Settings.DO_AE6:
                ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
            if Settings.DO_BH6:
                ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
            ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x, rms_c, rms_xc)
            ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(nsp_rms_x, nsp_rms_c, nsp_rms_xc)
            if Settings.DO_AR2:
                ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
            if Settings.DO_JELLIUM:
                ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(jellium_rms_x, jellium_rms_c, jellium_rms_xc)
            ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
            ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*nsp_err_xc)
            if Settings.DO_AE6:
                ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
                ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
            if Settings.DO_BH6:
                ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
                ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
            if Settings.DO_JELLIUM:
                ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xcs)
            ofile.write(ost+"\n")
            n_passed += 1

    print("We have {:} sets that passed all filters.".format(n_passed))
    print("Best Atomic XC was ", best_atom_xc)
    print("Best NSP XC was ", best_nsp_xc)
    if Settings.DO_AE6:
        print("Best AE6 MAE was   ", best_ae6_mae)
    if Settings.DO_BH6:
        print("Best BH6 MAE was   ", best_bh6_mae)
    if Settings.DO_AR2:
        print("Best Ar2 max was   ", best_ar2_max)

    return best_ae6_mae

def fast_min(params, xparams, cparams, XP, CP,
        lin_exl, lin_ecl,nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl, bh6_lin_exl,
        bh6_lin_ecl, ar2_lin_exl, ar2_lin_ecl, jellies, ofile):
    max_step = 100 # maximum number of iterations to refine parameter set
    max_rec = 5 # maximum recursions to improve set if fx != fc
    max_prec = 1.0e-1 # maximum final precision of parameters A, B,...
    fling = 4 # how large of a grid should we make around a "good" point

    A_stp = [(max_prec - Settings.A_STEP)*i/(1.0*max_step) + Settings.A_STEP for i in range(max_step+1)] # step sizes for A
    B_stp = [(max_prec - Settings.B_STEP)*i/(1.0*max_step) + Settings.B_STEP for i in range(max_step+1)] # for B, etc.
    C_stp = [(max_prec - Settings.C_STEP)*i/(1.0*max_step) + Settings.C_STEP for i in range(max_step+1)]
    D_stp = [(max_prec - Settings.D_STEP)*i/(1.0*max_step) + Settings.D_STEP for i in range(max_step+1)]
    steps = [[A_stp[i],B_stp[i],C_stp[i],D_stp[i]] for i in range(max_step+1)]

    def get_grid(pars,steps,it,xx=True):
        if xx: # if we're doing exchange, look at first half of best_par
            mv = 0
        else: # if correlation, look at second half
            mv = 4
        a_l = [pars[0 + mv] + steps[it-1][0]*fling*i for i in range(-fling*int(steps[it-1][0]/steps[it][0]),fling*int(steps[it-1][0]/steps[it][0])+1)]
        b_l = [pars[1 + mv] + steps[it-1][1]*fling*i for i in range(-fling*int(steps[it-1][1]/steps[it][1]),fling*int(steps[it-1][1]/steps[it][1])+1)]
        c_l = [pars[2 + mv] + steps[it-1][2]*fling*i for i in range(-fling*int(steps[it-1][2]/steps[it][2]),fling*int(steps[it-1][2]/steps[it][2])+1)]
        if Settings.FLT_HLF:
            d_l = [1.0]
        else:
            d_l = [pars[3 + 4*mv] + steps[it-1][3]*fling*i for i in range(-fling*int(steps[it-1][3]/steps[it][3]),fling*int(steps[it-1][3]/steps[it][3])+1)]
        ugrid = product(a_l, b_l, c_l, d_l)
        tot = len(a_l)*len(b_l)*len(c_l)*len(d_l)
        par = beauty_filter(ugrid,tot)
        if xx: # for exchange, need to ensure that fx is bounded
            return check_non_positivity(par, xparams)
        else: # not so for c
            return par

    def get_dat(xparams, cparams, XP, CP, lin_exl, lin_ecl,nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl):
        exl, err_x, rms_x = get_rmse(lin_exl, xparams, XP, Benchmarks.BM_X, correlation=False)
        ecl, err_c, rms_c = get_rmse(lin_ecl, cparams, CP, Benchmarks.BM_C, correlation=True)

        excl = exl + ecl
        err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
        rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

        nsp_exl,nsp_x,nsp_rms_x = get_rmse(nsp_lin_exl, xparams, XP, Benchmarks.NON_SPHERICAL_X, correlation=False)
        nsp_ecl,nsp_c,nsp_rms_c = get_rmse(nsp_lin_ecl, cparams, CP, Benchmarks.NON_SPHERICAL_C, correlation=True)
        nsp_excl = nsp_exl + nsp_ecl
        nsp_err_xc = 100*(Benchmarks.NON_SPHERICAL_XC - nsp_excl)/np.abs(Benchmarks.NON_SPHERICAL_XC)
        nsp_rms_xc = np.sqrt(np.mean(np.array(nsp_err_xc)**2))

        if Settings.DO_JELLIUM:
            jellium_exl = []
            jellium_ecl = []
            jerr_xs = []
            jerr_cs = []
            jerr_xcs = []
            for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                ex = jellies[i].get_Ex_from_lin(xparams, XP)
                jellium_exl.append(ex)
                jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
                ec = jellies[i].get_Ec_from_lin(cparams, CP)
                jellium_ecl.append(ec)
                jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ec)/abs(Benchmarks.JELLIUM_C[i]))
                exc = jellium_exl[i] + ec
                jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

            jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
            jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
            jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_xcs)**2))
        else:
            jellium_rms_x,jellium_rms_c,jellium_rms_xc = 0.0,0.0,0.0
            jerr_xcs = []

        rms = rms_xc + nsp_rms_xc + jellium_rms_xc

        return rms,rms_x,rms_c,rms_xc,nsp_rms_x,nsp_rms_c,nsp_rms_xc,err_xc,nsp_err_xc,jellium_rms_x,jellium_rms_c,jellium_rms_xc,jerr_xcs

    best_xc = 1e20
    for j in range(len(steps)) :
        if len(params) == 0 and j == 0:
            continue
        elif len(params) == 0 and j > 0:
            par = get_grid(best_par,steps,j)
        else:
            par = params
            params = []
        tot = len(par)

        for p in tqdm(par, total=tot, ascii=True, disable=Settings.NO_PROG):
            for i, n in enumerate(XP):
                xparams[n] = p[i]
            for i, n in enumerate(CP):
                cparams[n] = p[i]
            rms,rms_x,rms_c,rms_xc,nsp_rms_x,nsp_rms_c,nsp_rms_xc,err_xc,nsp_err_xc,jellium_rms_x,jellium_rms_c,jellium_rms_xc,jerr_xcs = get_dat(xparams, cparams, XP, CP, lin_exl, lin_ecl,nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl)

            if rms < best_xc:
                best_xc = rms
                best_atom = [rms_x,rms_c,rms_xc]
                best_nsp = [nsp_rms_x,nsp_rms_c,nsp_rms_xc]
                best_jell = [jellium_rms_x,jellium_rms_c,jellium_rms_xc]
                best_par = [xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'],cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC']]
                best_err_xc = err_xc
                best_err_nsp = nsp_err_xc
                best_err_jell = jerr_xcs
                if Settings.DO_AE6:
                    ae6_err, ae6_etot = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
                    ae6_me = np.mean(ae6_err)
                    ae6_mae = np.mean(np.abs(ae6_err))
                if Settings.DO_BH6:
                    bh6_err, bh6_etot = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP)
                    bh6_me = np.mean(bh6_err)
                    bh6_mae = np.mean(np.abs(bh6_err))
                if Settings.DO_AR2:
                    ar2_err = assess_ar2(ar2_lin_exl, ar2_lin_ecl, xparams, cparams, XP, CP)
                    ar2_max = np.max(np.abs(ar2_err))

    ost = ",{:},{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H0X, Settings.H1X, Settings.GX, Settings.CORRELATION)
    ost += ("{:},"*len(best_par)).format(*best_par)
    ost += ("{:},"*9).format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    if Settings.Fint == "SMOOTH" :
        ost += "{:},{:},".format(xparams['XETA'],cparams['CETA'])
    elif Settings.Fint == "JPLAP" :
        ost += "{:},{:},".format(xparams['FREG'],cparams['FREG'])
    if Settings.DO_AE6:
        ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
    if Settings.DO_BH6:
        ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
    ost += ("{:.4f}%,{:.4f}%,{:.4f}%,").format(*best_atom)
    ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(*best_nsp)
    if Settings.DO_AR2:
        ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
    if Settings.DO_JELLIUM:
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(best_jell[0], best_jell[1], best_jell[2])
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*best_err_xc)
    ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*best_err_nsp)
    if Settings.DO_AE6:
        ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
    if Settings.DO_BH6:
        ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
    if Settings.DO_JELLIUM:
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*best_err_jell)
    ofile.write(ost+"\n")

    fling *= 8
    print("Finished x=c, now adjusting x and c sequentially")
    print("Best so far were...")
    print("Best Atomic XC was ", best_atom[2])
    print("Best Non-spherical atomic XC was ", best_nsp[2])
    if Settings.DO_JELLIUM:
        print("Best jellium XC was", best_jell[2])
    if Settings.DO_AE6:
        print("Best corresponding AE6 MAE was   ", ae6_mae)
    if Settings.DO_BH6:
        print("Best corresponding BH6 MAE was   ", bh6_mae)
    if Settings.DO_AR2:
        print("Best corresponding Ar2 max was   ", ar2_max)

    cnt_brk = 0
    kill = 0
    old_best_xc = 5e20

    for ii in range(2*max_rec):

        if kill == 1 :
            break
        if ii%2 == 0:
            do_x = False
        else :
            do_x = True

        for j in range(1,len(steps)) :
            if abs(old_best_xc - best_xc) < old_best_xc*max_prec :
                cnt_brk += 1
            old_best_xc = best_xc
            pars = get_grid(best_par,steps,j,xx=do_x)
            if not do_x :
                for i, n in enumerate(XP):
                    xparams[n] = best_par[i]
            else :
                for i, n in enumerate(CP):
                    cparams[n] = best_par[i+4]
            for p in tqdm(pars, total=len(pars), ascii=True, disable=Settings.NO_PROG):
                if not do_x :
                    for i, n in enumerate(CP):
                        cparams[n] = p[i]
                else :
                    for i, n in enumerate(XP):
                        xparams[n] = p[i]
                rms,rms_x,rms_c,rms_xc,nsp_rms_x,nsp_rms_c,nsp_rms_xc,err_xc,nsp_err_xc,jellium_rms_x,jellium_rms_c,jellium_rms_xc,jerr_xcs = get_dat(xparams, cparams, XP, CP, lin_exl, lin_ecl,nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl)
                if rms < best_xc:
                    best_xc = rms
                    best_atom = [rms_x,rms_c,rms_xc]
                    best_nsp = [nsp_rms_x,nsp_rms_c,nsp_rms_xc]
                    best_par = [xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'],cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC']]
                    best_err_xc = err_xc
                    best_err_nsp = nsp_err_xc
                    best_jell = [jellium_rms_x,jellium_rms_c,jellium_rms_xc]
                    best_err_jell = jerr_xcs

                    if Settings.DO_AE6:
                        ae6_err, ae6_etot = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
                        ae6_me = np.mean(ae6_err)
                        ae6_mae = np.mean(np.abs(ae6_err))
                    if Settings.DO_BH6:
                        bh6_err, bh6_etot = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP)
                        bh6_me = np.mean(bh6_err)
                        bh6_mae = np.mean(np.abs(bh6_err))
                    if Settings.DO_AR2:
                        ar2_err = assess_ar2(ar2_lin_exl, ar2_lin_ecl, xparams, cparams, XP, CP)
                        ar2_max = np.max(np.abs(ar2_err))
            if cnt_brk >= 5:
                kill = 1
                break


    ost = ",{:},{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H0X, Settings.H1X, Settings.GX, Settings.CORRELATION)
    ost += ("{:},"*len(best_par)).format(*best_par)
    ost += ("{:},"*9).format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    if Settings.Fint == "SMOOTH" :
        ost += "{:},{:},".format(xparams['XETA'],cparams['CETA'])
    elif Settings.Fint == "JPLAP" :
        ost += "{:},{:},".format(xparams['FREG'],cparams['FREG'])
    if Settings.DO_AE6:
        ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
    if Settings.DO_BH6:
        ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
    ost += ("{:.4f}%,{:.4f}%,{:.4f}%,").format(*best_atom)
    ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(*best_nsp)
    if Settings.DO_AR2:
        ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
    if Settings.DO_JELLIUM:
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(best_jell[0], best_jell[1], best_jell[2])
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*best_err_xc)
    ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*best_err_nsp)
    if Settings.DO_AE6:
        ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
    if Settings.DO_BH6:
        ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
    if Settings.DO_JELLIUM:
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*best_err_jell)
    ofile.write(ost+"\n")

    print("Best Atomic XC was ", best_atom[2])
    print("Best Non-spherical atomic XC was ", best_nsp[2])
    if Settings.DO_JELLIUM:
        print("Best jellium XC was", best_jell[2])
    if Settings.DO_AE6:
        print("Best corresponding AE6 MAE was   ", ae6_mae)
    if Settings.DO_BH6:
        print("Best corresponding BH6 MAE was   ", bh6_mae)
    if Settings.DO_AR2:
        print("Best corresponding Ar2 max was   ", ar2_max)
    print('This took {:} recursions'.format(ii+1))

    return

def generate_density(alist):
    """
    Utility function to generate and arrange the orbitals for
    the atoms in alist (strings of atomic symbols).
    """
    out = []
    MIN_DEN = 1e-10
    n, r, wt = GridGenerator.make_grid(200)
    for a in map(Atom, alist):
        d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
        idxs = d0 + d1 > MIN_DEN  # Filter all those with small densities
        # Later functions expect density, |grad|, tau, spherical integration weight.
        # spin 0 then 1 for each quantity
        grid = 4*pi*wt[idxs]*r[idxs]**2

        assert max( np.dot(l0[idxs], grid), np.dot(l1[idxs], grid)) < 1e-3, \
            "Laplacian integral failed for {:}".format(a)

        out.append(np.stack([d0[idxs], d1[idxs], g0[idxs], g1[idxs], t0[idxs], t1[idxs], grid, g0[idxs]+g1[idxs], l0[idxs], l1[idxs]], axis=1))
    return out

def read_QUEST_densities(fnames):
    """
    Reads density from QUEST output format.
    Used to read full DFT grid for non-spherical atoms.

    Note! Extra total gradient as non-colinear density gradients
    are aparent!
    """
    olist = []
    for f in fnames:
        print("   - Reading ",f)
        with h5py.File(f, 'r') as inp:
            rho = np.array(inp['rho'])
            idxs = rho[:,0]+rho[:,1] > 1e-12  # Filter out really small densities
            grd = np.array(inp['grd'])
            vabs = lambda a,b: np.sum(np.multiply(a,b),axis=0)
            ga = grd[:,0:3]
            gb = grd[:,4:7]
            gaa = np.array(list(map(vabs, ga, ga))) # |grad|^2 spin 0
            gbb = np.array(list(map(vabs, gb, gb))) # |grad|^2 spin 1
            gab = np.array(list(map(vabs, ga, gb)))
            gtt = gaa + gbb + 2*gab
            tau = np.array(inp['tau'])
            xyz = np.array(inp['xyz'])

            out = np.zeros((rho[idxs].shape[0], 10))
            out[:,0] = rho[idxs,0]
            out[:,1] = rho[idxs,1]
            out[:,2] = np.sqrt(gaa[idxs])
            out[:,3] = np.sqrt(gbb[idxs])
            out[:,4] = tau[idxs,0]
            out[:,5] = tau[idxs,1]
            try:
                out[:,6] = xyz[idxs,3]  # Grid weights
            except IndexError:
                # Weights are missing so assume this is a spherical integal
                print("No integration weights found in ", f, " assuming spherical")
                out[:,6] = np.ones_like(rho[idxs,0])
            out[:,7] = np.sqrt(gtt[idxs])

            if Settings.LOAD_LAP:
                lap = np.array(inp['lap'])
                out[:,8] = lap[idxs,0]
                out[:,9] = lap[idxs,1]

                # Test laplacian correctness
                lapint = max(np.dot(out[:,8], out[:,6]), np.dot(out[:,9], out[:,6]))
                assert lapint < 1e-3, \
                    "Laplacian integral failed for {:} at {:}".format(f, lapint)

            olist.append(out)

    return olist

def read_QUEST_restricted_densities(fnames):
    """
    Reads density from QUEST output format for spin restricted format.
    Used to read hollow atoms.

    Note! Extra total gradient as non-colinear density gradients
    are aparent!
    """
    olist = []
    for f in fnames:
        print("   - Reading ",f)
        with h5py.File(f, 'r') as inp:
            rho = np.array(inp['rho'])
            idxs = rho[:,0] > 1e-12  # Filter out really small densities
            grd = np.array(inp['grd'])
            vabs = lambda a,b: np.sum(np.multiply(a,b),axis=0)
            ga = grd[:,0:3]
            gaa = np.array(list(map(vabs, ga, ga))) # |grad|^2 spin 0
            tau = np.array(inp['tau'])
            xyz = np.array(inp['xyz'])

            out = np.zeros((rho[idxs].shape[0], 10))
            out[:,0] = rho[idxs,0]/2.0
            out[:,1] = rho[idxs,0]/2.0

            out[:,2] = np.sqrt(gaa[idxs])/2.0
            out[:,3] = np.sqrt(gaa[idxs])/2.0
            out[:,4] = tau[idxs,0]/2.0
            out[:,5] = tau[idxs,0]/2.0
            try:
                out[:,6] = xyz[idxs,3]
            except IndexError:
                out[:,6] = 4*pi*xyz[idxs,2]**2*(xyz[1,2]-xyz[0,2])
            out[:,7] = np.sqrt(gaa[idxs])

            if Settings.LOAD_LAP:
                lap = np.array(inp['lap'])
                out[:,8] = lap[idxs,0]/2.0
                out[:,9] = lap[idxs,0]/2.0

                # Test laplacian correctness
                assert max( np.dot(out[:,8], out[:,6]), np.dot(out[:,9], out[:,6])) < 1e-3, \
                    "Laplacian integral failed for {:}".format(f)
            else:
                try:
                    lap = np.array(inp['lap'])
                    print("WARNING: Laplacian data is present but we are NOT keeping it!")
                except KeyError:
                    # It really wasn't there. No worries then unless we need it.
                    if Settings.Fint == "JPLAP" or Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH":
                        raise SystemExit("ERROR: Not loading Laplacian but JPLAP chosen!!")

            olist.append(out)

    return olist

def read_ar2_densities(flist):
    """
    We have to keep track of both basis set and separation for the
    basis set extrapolation. So this requires a bit more care.
    """
    odict = {}
    for f in flist:
        try:
            tmp = f.split("/")[-1].split("_")
            print(tmp)
            sep = tmp[1]
            bas = tmp[2].strip(".out.plot")
        except IndexError:
            # We are probably loading an atom file, check that
            assert "Z.out.plot" in f, "We are loading a strange file: "+f

            sep = "0.0"
            bas = tmp[1].strip(".out.plot")

        odict[sep+bas] = read_QUEST_restricted_densities([f])

    return odict


def test_ACGGA(cparams):
    old = Settings.Fint
    Settings.Fint = "ZEROS"

    names = ["He", "Li", "N", "Ne", "Na", "P", "Ar", "K", "Cr", "Cu", "As", "Kr", "Xe"]
    dens = []
    r, h = np.linspace(0.001, 10.0, 1000, retstep=True)
    MIN_DEN = 1e-10

    for n in names:
        a = Atom(n)
        d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
        idxs = d0 + d1 > MIN_DEN  # Filter all those with small densities
        dens.append(np.stack([d0[idxs], d1[idxs], g0[idxs], g1[idxs], t0[idxs], t1[idxs], 4*h*pi*r[idxs]**2, g0[idxs]+g1[idxs], l0[idxs], l1[idxs]], axis=1))

    n_elec = [2, 3, 7, 10, 11, 15, 18, 19, 24, 29, 33, 36, 54]
    bm = [0.0225, 0.0188, 0.0274, 0.0367, 0.0355, 0.0371, 0.0412, 0.0406, 0.043, 0.0484, 0.0494, 0.0512, 0.0562]

    ecl = get_Ec_for_atoms(dens, cparams)
    for i, m in enumerate(names):
        print(m, "Ec/N: {:.12f}".format(ecl[i]/n_elec[i]), "err: {:.3f}".format(ecl[i]/n_elec[i] + bm[i]))
    Settings.Fint = old

def test_SCAN_original(xparams, cparams, ae6_dens):
    xparams['K1'] = 0.065
    xparams['B1X'] = 0.156632
    xparams['B2X'] = 0.12083
    xparams['B3X'] = 0.5
    xparams['B4X'] = 0.2218
    xparams['A1X'] = 4.9479
    xparams['C1X'] = 0.667
    xparams['C2X'] = 0.8
    xparams['DX'] = 1.24

    cparams['B1C'] = 0.0285764
    cparams['B2C'] = 0.0889
    cparams['B3C'] = 0.125541
    cparams['C1C'] = 0.64
    cparams['C2C'] = 1.5
    cparams['DC'] = 0.7

    Settings.Fint = "ORIGINAL"
    Settings.H1X = "ORIGINAL"
    Settings.GX = "ORIGINAL"
    Settings.CORRELATION = "ORIGINAL"

    exl = get_Ex_for_atoms(ae6_dens, xparams)
    ecl = get_Ec_non_colinear(ae6_dens, cparams)

    err, etot = assess_ae6_for_scan2(None, None, xparams, cparams, [], [], exl=exl, ecl=ecl)
    mae = np.mean(np.abs(err))
    print("Error was {:}".format(mae))
    assert abs(mae - 3.135) <= 1e-3, "ERROR: SCAN original AE6 did not match known value"

    raise SystemExit("Test passed successfully.")

def fdiff_o6(ftab,step):

    fdc = np.asarray([-49/20,6,-15/2,20/3,-15/4,6/5,-1/6])/step
    cdc = np.asarray([-1/60,3/20,-3/4,0.,3/4,-3/20,1/60])/step

    nx = ftab.shape[0]
    df = np.zeros(nx)
    for ix in range(3):
        # forwards difference
        df[ix] = np.dot(fdc,ftab[ix:ix+7])
    for ix in range(3,nx-4):
        # central difference
        df[ix] = np.dot(cdc,ftab[ix-3:ix+4])
    for ix in range(nx-3,nx):
        # backwards difference
        df[ix] = np.dot(-fdc,ftab[ix:ix-7:-1])

    return df

def preprocess_ief_derivs(pars,a):

    if Settings.Fint in ['ORIGINAL','R2','R2reopt']:
        # we already know these are gtg
        return True#, pars
    xps = {}
    cps = {}
    for ikey,akey in enumerate(Settings.PARBDS):
        if Settings.PARBDS[akey][0] == 'x':
            xps[akey] = pars[ikey]
        elif Settings.PARBDS[akey][0] == 'c':
            cps[akey] = pars[ikey]
        elif Settings.PARBDS[akey][0] == 'xc':
            xps[akey] = pars[ikey]
            cps[akey] = pars[ikey]

    dfx,dfc = SCAN2.scan2_ief_derivs(a,xps,cps)
    return np.all(dfx <= 0.0) and np.all(dfc <= 0.0)#, pars


def test_vxc_sl_non_pos(pars, xparams, cparams, dens, rgrid, h):

    xps = {}
    cps = {}
    for ikey,akey in enumerate(Settings.PARBDS):
        if Settings.PARBDS[akey][0] == 'x':
            xps[akey] = pars[ikey]
        elif Settings.PARBDS[akey][0] == 'c':
            cps[akey] = pars[ikey]
        elif Settings.PARBDS[akey][0] == 'xc':
            xps[akey] = pars[ikey]
            cps[akey] = pars[ikey]

    for akey in xparams:
        if akey not in Settings.PARBDS:
            xps[akey] = xparams[akey]
    for akey in cparams:
        if akey not in Settings.PARBDS:
            cps[akey] = cparams[akey]

    _, dexcdn, _, dexcdgn, _ = SCAN2.get_scan2_xc_w_sl_derivs(xps, cps, dens[:,0], dens[:,1], dens[:,2], dens[:,3], dens[:,4], dens[:,5], dens[:,6], dens[:,7], dens[:,8], only_0=True)

    vxc_sl = dexcdn - fdiff_o6(dexcdgn,h) - 2/rgrid*dexcdgn

    return np.all(vxc_sl <= 0.0)#, pars


def get_errors_simple(pars, xparams, cparams, full_dens, lz_dens, ns_dens,\
    jellies,ar2_dens,only_reqd=False):

    xps = {}
    cps = {}
    for ikey,akey in enumerate(Settings.PARBDS):
        if Settings.PARBDS[akey][0] == 'x':
            xps[akey] = pars[ikey]
        elif Settings.PARBDS[akey][0] == 'c':
            cps[akey] = pars[ikey]
        elif Settings.PARBDS[akey][0] == 'xc':
            xps[akey] = pars[ikey]
            cps[akey] = pars[ikey]

    for akey in xparams:
        if akey not in Settings.PARBDS:
            xps[akey] = xparams[akey]
    for akey in cparams:
        if akey not in Settings.PARBDS:
            cps[akey] = cparams[akey]

    wtodo = {'SA': True, 'NSA': True, 'LZBX': True, 'LZBC': True,
    'JS': Settings.DO_JELLIUM, 'AR2': Settings.DO_AR2}
    if only_reqd:
        for anorm in wtodo:
            if wtodo[anorm]:
                if Settings.NORMWG[anorm] < 1.e-10:
                    wtodo[anorm] = False

    outd = {'SAXC': 0.0, 'NSAXC': 0.0, 'LZBX': 0.0, 'LZBC': 0.0, 'LZBXpe': 0.0, 'LZBCpe':0.0, 'JSFXC': 0.0, 'SRES': 0.0, 'AR2': 0.0}
    for akey in xps:
        if akey == 'XETA':
            outd['ETA'] = xps['XETA']
        else:
            outd[akey] = xps[akey]
    for akey in cps:
        if akey != 'CETA':
            outd[akey] = cps[akey]

    # spherical atoms
    if wtodo['SA']:
        exl = get_Ex_for_atoms(full_dens, xps)
        ecl = get_Ec_for_atoms(full_dens, cps)

        num_xc = [ecl[i] + exl[i] - Benchmarks.BM_XC[i] for i in range(len(ecl))]
        err_xc = [100*(ecl[i] + exl[i] - Benchmarks.BM_XC[i])/np.abs(Benchmarks.BM_XC[i]) for i in range(len(ecl))]
        outd['SAXC'] = np.sqrt(np.mean(np.array(err_xc)**2))

    # Non-spherical atoms
    if wtodo['NSA']:
        exl_nsp = get_Ex_for_atoms(ns_dens, xps)
        ecl_nsp = get_Ec_non_colinear(ns_dens, cps)

        num_xc_nsp = [ecl_nsp[i] + exl_nsp[i] - Benchmarks.NON_SPHERICAL_XC[i] for i in range(len(ecl_nsp))]
        err_xc_nsp = [100*(ecl_nsp[i] + exl_nsp[i] - Benchmarks.NON_SPHERICAL_XC[i])/np.abs(Benchmarks.NON_SPHERICAL_XC[i]) for i in range(len(ecl_nsp))]
        outd['NSAXC'] = np.sqrt(np.mean(np.array(err_xc_nsp)**2))

    if wtodo['LZBX']:
        _, outd['LZBX'] = get_lzlx_coef(xps, lz_dens)
        outd['LZBXpe'] = 100*((Benchmarks.LZ_X_B - outd['LZBX'])/abs(Benchmarks.LZ_X_B))

    if wtodo['LZBC']:
        outd['LZBC'] = get_lzlc_coef(cps, lz_dens)
        outd['LZBCpe'] = 100*((Benchmarks.LZ_C_B - outd['LZBC'])/abs(Benchmarks.LZ_C_B))

    if wtodo['JS']:
        jerr_xcs = []
        for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
            ex = jellies[i].eval_x_func(SCAN2.getscan_x, xps)
            ec = jellies[i].eval_c_func(SCAN2.getscan_c, cps)
            exc = ex + ec
            jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

        outd['JSFXC'] = np.sqrt(np.mean(np.array(jerr_xcs)**2))

    if wtodo['AR2']:
        exl_ar2 = get_Ex_for_atoms(ar2_dens, xps)*Benchmarks.EH_TO_KCAL
        ecl_ar2 = get_Ec_for_atoms(ar2_dens, cps)*Benchmarks.EH_TO_KCAL
        exc_ar2 = exl_ar2 + ecl_ar2
        etot = {}

        for i, m in enumerate(Benchmarks.AR2_LIST):
            etot[m] = exc_ar2[i] + Benchmarks.AR2_NO_XC[m]

        seps = ["1.6", "1.8", "2.0"]

        cbs_a = jsextr(np.array([2,3,4]), np.array([etot['ArDZ'], etot['ArTZ'], etot['ArQZ']]))

        ar2_err = []
        for k,s in enumerate(seps):
            cbs_e = jsextr(np.array([2,3,4]), np.array([etot[s+'DZ'], etot[s+'TZ'], etot[s+'QZ']]))
            ar2_err.append(cbs_e - 2*cbs_a - Benchmarks.AR2_BM[s])
            outd['AR2'] += 100*abs(ar2_err[k])/(len(seps)*Benchmarks.AR2_BM[s])

    outd['SRES'] = Settings.NORMWG['SA']*abs(outd['SAXC']) + \
    Settings.NORMWG['NSA']*abs(outd['NSAXC'])+ Settings.NORMWG['LZBX']*abs(outd['LZBXpe']) \
        + Settings.NORMWG['LZBX']*abs(outd['LZBCpe']) + Settings.NORMWG['JS']*abs(outd['JSFXC']) \
        + Settings.NORMWG['AR2']*abs(outd['AR2'])
    #if 'DP' in Settings.PARBDS:
    #    outd['SRES'] += (0.1/xps['DP'])**2
    #if 'K1' in Settings.PARBDS:
    #    outd['SRES'] += 1/xps['K1']

    return outd

def get_elapse_time_hms(dts):
    dt = [dts,0.0,0.0]
    nh = 60**2
    if dt[0] >= nh:
        dt[2] = dt[0]//nh
        dt[0] -= dt[2]*nh
    if dt[0] >= 60:
        dt[1] = dt[0]//60
        dt[0] -= dt[1]*60
    return dt[2],dt[1],dt[0]

def wrap_res_for_sa(pars,vtest,dtest,obj):#(pars,xparams,cparams,vtest,dtest,obj):

    res = 1e20

    dresult = dtest(pars)

    if dresult:

        vresult = vtest(pars)
        if vresult:
            res = obj(pars)['SRES']
            """
            xps = {}
            cps = {}
            for ikey,akey in enumerate(Settings.PARBDS):
                if Settings.PARBDS[akey][0] == 'x':
                    xps[akey] = pars[ikey]
                elif Settings.PARBDS[akey][0] == 'c':
                    cps[akey] = pars[ikey]
                elif Settings.PARBDS[akey][0] == 'xc':
                    xps[akey] = pars[ikey]
                    cps[akey] = pars[ikey]

            for akey in xparams:
                if akey not in Settings.PARBDS:
                    xps[akey] = xparams[akey]
            for akey in cparams:
                if akey not in Settings.PARBDS:
                    cps[akey] = cparams[akey]

            # spherical atoms
            exl = get_Ex_for_atoms(full_dens, xps)
            ecl = get_Ec_for_atoms(full_dens, cps)

            num_xc = [ecl[i] + exl[i] - Benchmarks.BM_XC[i] for i in range(len(ecl))]
            err_xc = [100*(ecl[i] + exl[i] - Benchmarks.BM_XC[i])/np.abs(Benchmarks.BM_XC[i]) for i in range(len(ecl))]
            sa_rms_pe = np.sqrt(np.mean(np.array(err_xc)**2))

            js_rms_pe = 0.0
            if Settings.DO_JELLIUM:
                jerr_xcs = []
                for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                    ex = jellies[i].eval_x_func(SCAN2.getscan_x, xps)
                    ec = jellies[i].eval_c_func(SCAN2.getscan_c, cps)
                    exc = ex + ec
                    jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

                js_rms_pe = np.sqrt(np.mean(np.array(jerr_xcs)**2))

            res = Settings.NORMWG['SA']*sa_rms_pe + Settings.NORMWG['JS']*js_rms_pe
            """

    return res

def wrap_for_sa(bds,vtest,dtest,obj):#(bds,xparams,cparams,vtest,dtest,full_dens,jellies):
    #tres = dual_annealing(wrap_res_for_sa,bds,
    #    args=(vtest,dtest,obj),no_local_search=True)
    tres = differential_evolution(wrap_res_for_sa,bds,args=(vtest,dtest,obj),\
        polish=False,workers=Settings.NPROC,updating='deferred')
    saout = np.zeros(len(Settings.PARBDS.keys())+1)
    saout[0] = tres.fun
    saout[1:] = tres.x

    return saout

def lin_opt(nl_pars_x, nl_pars_c, sa_dens, nsa_dens, jellies):

    nnorm = len(sa_dens) + len(nsa_dens)
    if Settings.DO_JELLIUM:
        nnorm += len(jellies)

    res_x_v = np.zeros(nnorm)
    res_c_v = np.zeros(nnorm)

    cmat_x = np.zeros((nnorm,Settings.N_LIN_PARS_X))
    cmat_c = np.zeros((nnorm,Settings.N_LIN_PARS_C))

    cxs = ['C{:}X'.format(2+i) for i in range(Settings.N_LIN_PARS_X)]
    ccs = ['C{:}C'.format(2+i) for i in range(Settings.N_LIN_PARS_C)]

    sa_lin_x, sa_lin_c = make_linear_energy_components(sa_dens, nl_pars_x, \
        nl_pars_c, cxs, ccs)

    k = 0
    for i in range(len(sa_dens)):

        for j,xkey in enumerate(cxs):
            cmat_x[k,j] = sa_lin_x[xkey][i]
        res_x_v[k] = Benchmarks.BM_X[i] - sa_lin_x['NULL'][i]

        for j,ckey in enumerate(ccs):
            cmat_c[k,j] = sa_lin_c[ckey][i]
        res_c_v[k] = Benchmarks.BM_C[i] - sa_lin_c['NULL'][i]

        k += 1

    nsa_lin_x, nsa_lin_c = make_linear_energy_components(nsa_dens, nl_pars_x, \
        nl_pars_c, cxs, ccs)

    for i in range(len(nsa_dens)):

        for j,xkey in enumerate(cxs):
            cmat_x[k,j] = nsa_lin_x[xkey][i]
        res_x_v[k] = Benchmarks.NON_SPHERICAL_X[i] - nsa_lin_x['NULL'][i]

        for j,ckey in enumerate(ccs):
            cmat_c[k,j] = nsa_lin_c[ckey][i]
        res_c_v[k] = Benchmarks.NON_SPHERICAL_C[i] - nsa_lin_c['NULL'][i]

        k += 1

    if Settings.DO_JELLIUM:

        for ajell in jellies:
            ajell.make_linear_energy_components(SCAN2.getscan_x, SCAN2.getscan_c,\
                nl_pars_x, nl_pars_c, cxs, ccs)

        for i in range(len(jellies)):

            for j,xkey in enumerate(cxs):
                cmat_x[k,j] = jellies[i].lin_exl[xkey]
            res_x_v[k] = Benchmarks.JELLIUM_X[i] - jellies[i].lin_exl['NULL']

            for j,ckey in enumerate(ccs):
                cmat_c[k,j] = jellies[i].lin_ecl[ckey]
            res_c_v[k] = Benchmarks.JELLIUM_C[i] - nsa_lin_c['NULL'][i]

            k += 1

    x_lin_opt,ssr_x,_,_ = np.linalg.lstsq(cmat_x,res_x_v,rcond=None)
    for j,xkey in enumerate(cxs):
        nl_pars_x[xkey] = x_lin_opt[j]

    c_lin_opt,ssr_c,_,_ = np.linalg.lstsq(cmat_c,res_c_v,rcond=None)
    for j,ckey in enumerate(ccs):
        nl_pars_c[ckey] = c_lin_opt[j]

    lz_dens = []
    for iatom,atom in enumerate(['Ne','Ar','Kr','Xe']):
        for iat,anat in enumerate(Benchmarks.BM_LIST):
            if anat == atom:
                lz_dens.append(sa_dens[iat])
                break
    sres = get_errors_simple([0], nl_pars_x, nl_pars_c, sa_dens, lz_dens, nsa_dens,\
        jellies,[None],only_reqd=False)
    x = []
    for i in range(Settings.N_LIN_PARS_X+3):
        x.append(nl_pars_x['C{:}X'.format(i+1)])
    print(x)
    print(sres)
    exit()
    #print(test_linear_composition(sa_dens, nl_pars_x, nl_pars_c, cxs, ccs, non_colin=False))
    #print(test_linear_composition(nsa_dens, nl_pars_x, nl_pars_c, cxs, ccs, non_colin=True))

    return nl_pars_x, nl_pars_c

def runopt():
    """
    Main optimisation function.

    Sets up working parameter dictionary
    then generates/reads necessary densities
    then sets parameters for limiting GGA-like parts of the functional from constraints
    finally, loops over an ascending range of k1 parameters.

    Original SCAN stops at the lowest k1 that passes the filters,
    SCAN2 doesn't seem to fail before k1 is large enough to break constraints.
    TODO: create more filters to better find k1 in SCAN2
    """

    # Working dictionary of parameters.
    # These change throughout the run and are not meant as permanent records.
    # These values are only placeholders, check SCAN2.py or SCAN.py for current 'best'

    xparams = dict(
        K1 = 0.065,
        # A1 = 5.579960559993676,
        A1 = 4.9479,
        CONX = 1.0,
        CAX = -8.0,
        CBX = 19.5,
        CCX = -11.5,
        CDX = 1.0,
        FREG = 1.0,
        XETA = 0.703703704, #20.0/27.0,

        ETA = 0.01,
        FX0 = -0.667,
        DP = 0.361,
        DA = 1.05,

        C1X = 0.667,
        C2X= 0.8,
        DX = 1.24,

        CXBX = 1.0,
        B1X = 0.156632,
        B2X = 0.12083,
        B3X = 0.5,
        B4X = 0.2218,

        C = 0.28771,

        H1DX = 0.001
    )

    cparams = dict(
        CONC = 1.0,
        CAC = -6.0,
        CBC = 0.361,
        CCC = -5.2,
        CDC = 1.0,
        FREG = 1.0,
        CETA = 0.703703704, #20.0/27.0,

        ETA = 0.01,
        FC0 = -0.64,
        DP = 0.361,

        C1C = 0.64,
        C2C = 1.5,
        DC = 0.7,

        B1C = 0.0285764,
        B2C = 0.0889,
        B3C = 0.125541,
        B4C = 2.3631,

        C = 0.28771,

        H1DC1 = 1.0,
        H1DC2 = 0.0,

        F1C = -0.8
    )

    if not os.path.isdir('./fits/'):
        os.system('mkdir ./fits/')

    stime = time()
    print("\nGenerating rare gas densities...")
    lz_dens = generate_density(["Ne", "Ar", "Kr", "Xe"])
    fullist = Benchmarks.BM_LIST
    full_dens = generate_density(fullist)
    He_dens = generate_density(["He"])

    print("Reading non-spherical densities")
    non_spherical_dens = read_QUEST_densities([x+".out.plot" for x in Benchmarks.NON_SPHERICAL_LIST])

    #print("Reading hollow-atom densities")
    ha_dens = None# read_QUEST_restricted_densities(["Ne2p_hftCQZ.out.plot"])

    if Settings.SCAN2_DENS:
        rest_list_ae6 = ["set_dens/SCAN2/"+x+".Rscan2.out.plot" for x in Benchmarks.REST_AE6]
        unrest_list_ae6 = ["set_dens/SCAN2/"+x+".Uscan2.out.plot" for x in Benchmarks.UNREST_AE6]
        rest_list_bh6 = ["set_dens/SCAN2/"+x+".Rscan.out.plot" for x in Benchmarks.REST_BH6]
        unrest_list_bh6 = ["set_dens/SCAN2/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_BH6]
        ar2_root = "set_dens/SCAN2/Ar2/"
    else:
        rest_list_ae6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_AE6]
        unrest_list_ae6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_AE6]
        rest_list_bh6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_BH6]
        unrest_list_bh6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_BH6]
        ar2_root = "set_dens/SCAN/Ar2/"

    ar2_list = []
    for f in Benchmarks.AR2_LIST:
        sep = f[:-2]
        bas = f[-2:]
        if sep == "Ar":
            ar2_list.append(ar2_root+"Ar_"+bas+".out.plot")
        else:
            ar2_list.append(ar2_root+"Ar2_"+sep+"_"+bas+".out.plot")

    print("Reading test set densities")

    if Settings.DO_AE6:
        ae6_dens = read_QUEST_restricted_densities(rest_list_ae6) + read_QUEST_densities(unrest_list_ae6)
    else:
        ae6_dens = []

    if Settings.DO_BH6:
        bh6_dens = read_QUEST_restricted_densities(rest_list_bh6) + read_QUEST_densities(unrest_list_bh6)
    else:
        bh6_dens = []

    if Settings.DO_AR2:
        ar2_dens = read_QUEST_restricted_densities(ar2_list)
    else:
        ar2_dens = []

    if Settings.EVALUATE_PUBLISHED_SCAN:
        eval_scan(full_dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, ar2_dens)

    # Setup the jellium objects
    jellies = []
    if Settings.DO_JELLIUM:
        for rs in Settings.JELLIUM_RS_LIST:
            jellies.append(Jelly(rs))
    #opt_main(He_dens, full_dens, lz_dens, non_spherical_dens, jellies, ae6_dens, bh6_dens, ar2_dens, xparams, cparams)

    # Set parameters in g0 functional
    #if Settings.GX == "ORIGINAL" and Settings.H1X == "ORIGINAL":
    #    xparams['A1'] = 4.9479
    #else:
    a1x = set_a1x(xparams)
    xparams['A1'] = a1x

    #if Settings.BETARS == 'revTPSS' and Settings.GX == "ORIGINAL":
    #    cparams['B1C'] = 0.0285764
    #    cparams['B2C'] = 0.0889
    #    cparams['B3C'] = 0.125541
    #else:
    cparams['B1C'],cparams['B2C'],cparams['B3C'] = set_ec0_lda_pars_scan2(cparams, xparams, He_dens)
    # Helium exchange energy
    print("Correlation Parameters")
    print("B1C:   ",cparams['B1C'])
    print("B2C:   ",cparams['B2C'])
    print("B3C:   ",cparams['B3C'])
    if Settings.CORRELATION == "ACGGA":
        print("CHI_2: ",cparams['CHI_2'])
        print("CHI_42:",cparams['CHI_42'])
    He_ex = get_Ex_for_atoms(He_dens, xparams)[0]
    He_ec = get_Ec_for_atoms(He_dens, cparams)[0]
    print("HELIUM EXCHANGE:    ",He_ex)
    print("HELIUM CORRELATION: ",He_ec)
    print("HELIUM XC:          ",He_ex+He_ec)

    # Check that we definitely hit this constraint!
    assert abs(He_ex+He_ec + 1.068) < 1e-3, "Failed to meet He XC energy bound, {:} != -1.068".format(He_ex+He_ec)

    # test_SCAN_original(xparams, cparams, ae6_dens)

    # Begin the full search that happens in this function
    #search_SCAN2(xparams, cparams, full_dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, ar2_dens)

    """
    In SCAN 2:
        CAX = fx'(0)
        CBX = dp, controls rate of rise of hx1(p)
        CCX = dx = - lim a-->infty fx(a)

        CAC = fc'(0)
        CBC = dc = - lim a-->infty fc(a)
    """

    pard = {}
    nstep = {}
    max_dx = 5.74712643678161 # = 1/.174

    bdsl = []
    npars = len(Settings.PARBDS.keys())
    key_order = []

    for ikey,akey in enumerate(Settings.PARBDS):
        key_order.append(akey)
        if akey == 'DX':
            bdsl.append((Settings.PARBDS[akey][1],min(max_dx,Settings.PARBDS[akey][2])))
        else:
            bdsl.append((Settings.PARBDS[akey][1],Settings.PARBDS[akey][2]))

    # parameters that are kept constant through fitting
    if Settings.K1_STEP_SEARCH:
        ckeys = ['K1','A1','B1C','B2C','B3C']
    else:
        ckeys = ['A1','B1C','B2C','B3C']

    for akey in ckeys:
        key_order.append(akey)

    norml = ['SAXC','NSAXC', 'LZBX','LZBXpe','LZBC','LZBCpe']
    if Settings.DO_JELLIUM:
        norml.append('JSFXC')
    if Settings.DO_AR2:
        norml.append('AR2')
    norml.append('SRES')

    for anorm in norml:
        key_order.append(anorm)

    nr = 4000
    hr, hrstep = np.linspace(0.01,8.0,nr,retstep=True)

    h_at_dens = np.zeros((nr,9))
    hn = np.exp(-2*hr)/pi
    hgn = -2*hn
    h_at_dens[:,0] = hn
    h_at_dens[:,2] = hgn
    h_at_dens[:,4] = hgn
    h_at_dens[:,5] = hgn**2/(8*hn)


    wrap_vxc_h_at = partial(test_vxc_sl_non_pos,xparams=xparams,cparams=cparams,\
        dens=h_at_dens,rgrid=hr,h=hrstep)

    if Settings.ISOORB == 'B':
        atmp = np.arange(0.0,1.0,.01)
    else:
        atmp = np.arange(0.0,100,.01)
    wrap_ief_derivs = partial(preprocess_ief_derivs,a=atmp)

    wrap_get_errors_simple = partial(get_errors_simple,xparams=xparams, \
        cparams=cparams, full_dens=full_dens, lz_dens=lz_dens, \
        ns_dens=non_spherical_dens, jellies=jellies, ar2_dens=ar2_dens, \
        only_reqd=False)

    if Settings.NORMWG['LZBX'] > 1.e-10 or Settings.NORMWG['LZBC'] > 1.e-10:
        tmp_lz_dens = lz_dens
    else:
        tmp_lz_dens = [None]

    if Settings.NORMWG['JS'] > 1.e-10 and Settings.DO_JELLIUM:
        tmp_js_dens = jellies
    else:
        tmp_js_dens = [None]

    if Settings.NORMWG['AR2'] > 1.e-10 and Settings.DO_AR2:
        tmp_ar2_dens = ar2_dens
    else:
        tmp_ar2_dens = [None]

    sa_obj = partial(get_errors_simple,xparams=xparams, \
        cparams=cparams, full_dens=full_dens, lz_dens=tmp_lz_dens, \
        ns_dens=non_spherical_dens, jellies=tmp_js_dens, ar2_dens=tmp_ar2_dens, \
        only_reqd=True)

    wrap_sa = partial(wrap_for_sa,vtest=wrap_vxc_h_at, dtest=wrap_ief_derivs, \
        obj=sa_obj)

    bres = np.zeros(npars+1)
    bres[0] = 1e20
    avgs = np.zeros(npars)
    stddevs = np.zeros(npars)

    tstr = ('{:}, '*(len(key_order)-1) + '{:} \n').format(*key_order)

    nrun = Settings.NRUN
    if Settings.K1_STEP_SEARCH:
        k1_l = np.arange(0.01,0.174,0.005)
        k1_l = np.append(k1_l,0.174)
    else:
        k1_l = [0.065]
    for xparams['K1'] in k1_l:
        if Settings.K1_STEP_SEARCH:
            print('==================\n    k1 = {:}   \n=================='.format(xparams['K1']))

        wgts = np.zeros(nrun)

        tres_l = []
        for irun in range(nrun):

            tmp_str_to_p = 'Beginning run {:} of {:}'.format(1+irun,nrun)
            if irun > 0:
                dtime = get_elapse_time_hms(time()-time_r1)
                if dtime[0] > 0:
                    tmp_str_to_p += '; previous run took {:} hr; {:} min ; {:.0f} sec'.format(*dtime)
                else:
                    tmp_str_to_p += '; previous run took {:} min ; {:.0f} sec'.format(*dtime[1:])
            print(tmp_str_to_p)
            time_r1 = time()

            tres_l.append(wrap_sa(bdsl))

        if Settings.POST_REF:
            opt_bounds = np.zeros((npars,2))
            opt_bounds[:,0] = 1e20
            opt_bounds[:,1] = -1e20

        for irun in range(nrun):
            wgts[irun] = tres_l[irun][0]
            for ipar in range(npars):
                avgs[ipar] += tres_l[irun][1+ipar]*wgts[irun]
                stddevs[ipar] += tres_l[irun][1+ipar]**2*wgts[irun]
                if Settings.POST_REF:
                    opt_bounds[ipar,0] = min(opt_bounds[ipar,0],tres_l[irun][1+ipar])
                    opt_bounds[ipar,1] = max(opt_bounds[ipar,1],tres_l[irun][1+ipar])
            if tres_l[irun][0] < bres[0]:
                bres = tres_l[irun][:]
            tmp = wrap_get_errors_simple(tres_l[irun][1:])
            for ikey,akey in enumerate(key_order):
                if ikey < len(key_order)-1:
                    tstr += '{:}, '.format(tmp[akey])
                else:
                    tstr += '{:} \n'.format(tmp[akey])

        wgt_norm = np.sum(wgts)
        avgs /= wgt_norm
        #for ipar in range(npars):
        #    avgs[ipar] = max(bdsl[ipar][0],min(bdsl[ipar][1],avgs[ipar]))
        stddevs = (np.maximum(1.e-20,stddevs/wgt_norm - avgs**2))**(0.5)

        tstr += 'BEST of {:} runs \n--------------\n'.format(nrun)

        tmp = wrap_get_errors_simple(bres[1:])
        for akey in Settings.PARBDS:
            print(akey,tmp[akey])
        for ikey,akey in enumerate(key_order):
            if ikey < len(key_order)-1:
                tstr += '{:}, '.format(tmp[akey])
            else:
                tstr += '{:} \n'.format(tmp[akey])

        tstr += 'AVERAGE of {:} runs \n--------------\n'.format(nrun)
        for ipar in range(npars):
            # uncrt tries to estimate which digit of precision is uncertain, and round to the next highest digit
            uncrt = int(abs(np.ceil(np.log10(stddevs[ipar]))))
            #avgs[ipar] = round(avgs[ipar],uncrt)
        tmp = wrap_get_errors_simple(avgs)
        if tmp['SRES'] < bres[0]:
            bres[1:] = avgs

        for ikey,akey in enumerate(key_order):
            if ikey < len(key_order)-1:
                tstr += '{:}, '.format(tmp[akey])
            else:
                tstr += '{:} \n'.format(tmp[akey])

        tstr += 'STD DEV of {:} runs \n--------------\n'.format(nrun)
        tstr += ('{:}, '*(npars-1) + '{:}').format(*stddevs)

        if Settings.POST_REF:
            print('Refining parameters')
            tstr += '\n\n'
            tstr += 'REFINED SEARCH\n'
            #tstr += ('{:}, '*(len(key_order)-1) + '{:} \n').format(*key_order)

            bds_l = []
            for ipar in range(npars):
                lbd = min(avgs[ipar]-stddevs[ipar],opt_bounds[ipar,0])
                ubd = max(avgs[ipar]+stddevs[ipar],opt_bounds[ipar,1])
                bds_l.append((lbd,ubd))

            ref_res = minimize(wrap_res_for_sa,avgs, method='Nelder-Mead', bounds=bds_l, \
                args = (wrap_vxc_h_at, wrap_ief_derivs, sa_obj))
            if bres[0] > ref_res.fun:
                bres[0] = ref_res.fun
                bres[1:] = ref_res.x

            tstr += 'Overall BEST \n--------------\n'

            tmp = wrap_get_errors_simple(bres[1:])
            for akey in Settings.PARBDS:
                print(akey,tmp[akey])
            for ikey,akey in enumerate(key_order):
                if ikey < len(key_order)-1:
                    tstr += '{:}, '.format(tmp[akey])
                else:
                    tstr += '{:} \n'.format(tmp[akey])

        tstr += '\n'
        with open('./fits/sc2_devol_safety.csv','w+') as ofl:
            ofl.write(tstr)

    modstr = '_IEF='+Settings.Fint+'_H1X='+Settings.H1X+'_GX='+Settings.GX+'_IND='\
        +Settings.ISOORB+'_C='+Settings.CORRELATION+'_BRS='+Settings.BETARS
    le_fichier = './fits/sc2_devol'+modstr+'.csv'
    ofl = open(le_fichier,'w+')
    ofl.write(tstr)
    ofl.close()

    os.system('rm ./fits/sc2_devol_safety.csv')

    return

    """
    workerbees = Pool(processes=Settings.NPROC)

    print('==============================\nBegin simulated annealing with {:} simulatenous runs'.format(Settings.NPROC))
    tmp = workerbees.map(wrap_sa,[bdsl for i in range(Settings.NPROC)])
    sapars = np.zeros((Settings.NPROC,7))
    sares = np.zeros(Settings.NPROC)
    for i in range(Settings.NPROC):
        sapars[i] = tmp[i][1:]
        sares[i] = tmp[i][0]

    npars = 1

    for ikey,akey in enumerate(Settings.PARBDS.keys()):
        mean = np.sum(sapars[:,ikey])/Settings.NPROC
        var = np.sum(sapars[:,ikey]**2)/Settings.NPROC
        stddev = (max(0.0,var - mean**2))**(0.5)
        print('{:}: mean value = {:}, var = {:}, stddev = {:}'.format(akey,mean,var,stddev))
        nstep = 5 + int(stddev/abs(mean)/.1)
        if akey == 'DX':
            pard[akey] = np.linspace(0.9*sapars[:,ikey].min(),min(max_dx,1.1*sapars[:,ikey].max()),nstep)
        else:
            pard[akey] = np.linspace(0.9*sapars[:,ikey].min(),1.1*sapars[:,ikey].max(),nstep)
        npars *= nstep

    tindmin = np.argmin(sares)
    print('Best set so far, res={:} :'.format(sares[tindmin]))
    for ikey,akey in enumerate(Settings.PARBDS.keys()):
        print('{:} = {:}'.format(akey,sapars[tindmin][ikey]))

    print('==============================\n Moving onto grid search\n{:} total parameter sets'.format(npars))

    print('Begin preprocessing\n')

    print('Testing nonpositivity of the XC potential for the hydrogen atom')
    tmplog = workerbees.map(wrap_vxc_h_at,product(pard['ETA'],pard['K1'],pard['FX0'],\
        pard['DP'],pard['DX'],pard['FC0'],pard['DC']))
    olist1 = []
    for itmp in range(npars):
        lgood,ps = tmplog[itmp]
        if lgood:
            olist1.append(ps)
    for itmp in range(Settings.NPROC):
        # make sure we include the simulated annealing results!
        olist1.append(sapars[itmp])
    opars1 = len(olist1)

    time1 = time()
    dth,dtm,dts = get_elapse_time_hms(time1-stime)
    print('{:} ({:.2f}%) parameter sets passed vxc preprocessing test in {:} hr, {:} min, {:.4f} sec\n'.format(opars1-Settings.NPROC,100*(opars1-Settings.NPROC)/npars,dth,dtm,dts))
    if opars1 == 0:
        raise SystemExit('No parameter sets to fit!')


    print('Testing interpolation function derivatives')
    tmplog = workerbees.map(wrap_ief_derivs,olist1)
    olist2 = []
    for itmp in range(opars1):
        lgood,ps = tmplog[itmp]
        if lgood:
            olist2.append(ps)
    opars2 = len(olist2)

    time2 = time()
    dth,dtm,dts = get_elapse_time_hms(time2-time1)
    print('{:} ({:.2f}%) remaining parameter sets passed interpolation derivative preprocessing test in {:} hr, {:} min, {:.4f} sec\n'.format(opars2,100*opars2/opars1,dth,dtm,dts))
    if opars2 == 0:
        raise SystemExit('No parameter sets to fit!')

    print('Moving on to core fitting')
    tmpout = workerbees.map(wrap_get_errors_simple,olist2)
    workerbees.close()

    time3 = time()

    outdat = np.zeros((opars2,len(key_order)))
    for itmp,tmpd in enumerate(tmpout):
        outdat[itmp] = [tmpd[akey] for akey in key_order]

    outdat = outdat[np.argsort(outdat[:,-1])]
    ofl = h5py.File('scan2_param.hdf5','w')
    for ikey,akey in enumerate(Settings.PARBDS):
        ofl.create_dataset(akey,data=outdat[ikey,:])
    ofl.attrs['sorted']=True
    ofl.close()
    #np.savetxt('scan2_param.csv',np.asarray(outdat),delimiter=',',header='eta, k1, fx0, dp, dx, fc0, dc, Sph At RMS XC, Non Sph At RMS XC, LZ Bx, LZ Bx PE, LZ Bc, LZ Bc PE, Jell Surf Form RMS XC, Res')

    dth,dtm,dts = get_elapse_time_hms(time3-time2)
    print('Log file written. Core fitting took {:} hr, {:} min, {:.4f} sec\n'.format(dth,dtm,dts))

    dth,dtm,dts = get_elapse_time_hms(time3-stime)
    print('Total runtime: {:} hr, {:} min, {:.4f} sec \n'.format(dth,dtm,dts))

    return
    """

def eval_scan(full_dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, ar2_dens):
    """
    This function does nothing more than evaluate the published SCAN for all the test cases we have.

    No optimisation.

    Nothing.
    """

    print("\nReporting Published SCAN")
    print("========================\n")

    xparams = dict(
        K1 = 0.065,
        A1 = 4.9479,

        C1X = 0.667,
        C2X= 0.8,
        DX = 1.24,

        CXBX = 1.0,
        B1X = 0.156632,
        B2X = 0.12083,
        B3X = 0.5,
        B4X = 0.2218,

        ETA = 1.e-3
    )

    cparams = dict(
        C1C = 0.64,
        C2C = 1.5,
        DC = 0.7,

        B1C = 0.0285764,
        B2C = 0.0889,
        B3C = 0.125541,
        B4C = 2.3631,

        ETA = 1.e-2,
        DP = 0.361
    )

    Settings.H0X = "ORIGINAL"
    Settings.H1X = "ASCAN"
    Settings.GX = "ORIGINAL"
    Settings.Fint = "ASCAN"
    Settings.ISOORB = 'BA'
    Settings.CORRELATION = "R2"  # Watch out! This doesn't seem to be enough
    Settings.BETARS = 'ACGGA'
    #SCAN2.AFACTOR = 0.1
    #SCAN2.BFACTOR = 0.1778
    #SCAN2.CFACTOR = None
    #SCAN2.DFACTOR = None

    # Setup the jellium objects
    jellies = []
    if Settings.DO_JELLIUM:
        for rs in Settings.JELLIUM_RS_LIST:
            jellies.append(Jelly(rs))

    directory = Settings.WORK_DIR
    nstr = "PUBLISHED_SCAN.csv"
    with open(nstr, 'w') as ofile:
        tstr = ""
        if Settings.DO_AE6:
            tstr += "AE6 ME,AE6 MAE,"
        if Settings.DO_BH6:
            tstr += "BH6 ME,BH6 MAE,"
        tstr += "Atoms X,Atoms C,Atoms XC,"
        tstr += "NSP X,NSP C,NSP XC,"
        tstr += 'LZ BX, LZ BX PE, LZ BC, LZ BC PE,'
        if Settings.DO_AR2:
            tstr += ("Ar2_{:},"*len(Benchmarks.AR2_SEPS)).format(*Benchmarks.AR2_SEPS)
            tstr += 'Ar2 MAPE,'
        if Settings.DO_JELLIUM:
            tstr += "Jellium X,Jellium C,Jellium XC,"
        tstr += ("{:},"*len(Benchmarks.BM_LIST)).format(*Benchmarks.BM_LIST)
        tstr += ("{:},"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*Benchmarks.NON_SPHERICAL_LIST)
        if Settings.DO_AE6:
            tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
            tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
        if Settings.DO_BH6:
            tstr += ("{:},"*len(Benchmarks.BH6_LIST)).format(*Benchmarks.BH6_LIST)
            tstr += ("{:},"*len(Benchmarks.BH6_TESTS)).format(*Benchmarks.BH6_TESTS)
        if Settings.DO_JELLIUM:
            tstr += ("Jellium XC {:},"*len(Settings.JELLIUM_RS_LIST)).format(*Settings.JELLIUM_RS_LIST)
        ofile.write(tstr+"\n")

        #print(tstr)

        # Spherical atoms
        exl = get_Ex_for_atoms(full_dens, xparams)
        ecl = get_Ec_for_atoms(full_dens, cparams)

        lzats = ["Ne", "Ar", "Kr", "Xe"]
        jat = 0
        ind = [0 for at in lzats]
        for iat,at in enumerate(Benchmarks.BM_LIST):
            if at == lzats[jat]:
                ind[jat] = iat
                jat += 1

        _, lzbx = get_lzlx_coef({}, None,exlist_in=exl[ind])
        lzbc = get_lzlc_coef({}, None,eclist_in=ecl[ind])
        lzbx_pe = 100*((Benchmarks.LZ_X_B - lzbx)/abs(Benchmarks.LZ_X_B))
        lzbc_pe = 100*((Benchmarks.LZ_C_B - lzbc)/abs(Benchmarks.LZ_C_B))

        num_x = [exl[i] - Benchmarks.BM_X[i] for i in range(len(exl))]
        err_x = [100*(exl[i] - Benchmarks.BM_X[i])/np.abs(Benchmarks.BM_X[i]) for i in range(len(exl))]
        rms_x = np.sqrt(np.mean(np.array(err_x)**2))

        num_c = [ecl[i] - Benchmarks.BM_C[i] for i in range(len(ecl))]
        err_c = [100*(ecl[i] - Benchmarks.BM_C[i])/np.abs(Benchmarks.BM_C[i]) for i in range(len(ecl))]
        rms_c = np.sqrt(np.mean(np.array(err_c)**2))

        num_xc = [ecl[i] + exl[i] - Benchmarks.BM_XC[i] for i in range(len(ecl))]
        err_xc = [100*(ecl[i] + exl[i] - Benchmarks.BM_XC[i])/np.abs(Benchmarks.BM_XC[i]) for i in range(len(ecl))]
        rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

        # Non-spherical atoms
        exl_nsp = get_Ex_for_atoms(non_spherical_dens, xparams)
        ecl_nsp = get_Ec_non_colinear(non_spherical_dens, cparams)

        num_x_nsp = [exl_nsp[i] - Benchmarks.NON_SPHERICAL_X[i] for i in range(len(exl_nsp))]
        err_x_nsp = [100*(exl_nsp[i] - Benchmarks.NON_SPHERICAL_X[i])/np.abs(Benchmarks.NON_SPHERICAL_X[i]) for i in range(len(exl_nsp))]
        rms_x_nsp = np.sqrt(np.mean(np.array(err_x_nsp)**2))

        num_c_nsp = [ecl_nsp[i] - Benchmarks.NON_SPHERICAL_C[i] for i in range(len(ecl_nsp))]
        err_c_nsp = [100*(ecl_nsp[i] - Benchmarks.NON_SPHERICAL_C[i])/np.abs(Benchmarks.NON_SPHERICAL_C[i]) for i in range(len(ecl_nsp))]
        rms_c_nsp = np.sqrt(np.mean(np.array(err_c_nsp)**2))

        num_xc_nsp = [ecl_nsp[i] + exl_nsp[i] - Benchmarks.NON_SPHERICAL_XC[i] for i in range(len(ecl_nsp))]
        err_xc_nsp = [100*(ecl_nsp[i] + exl_nsp[i] - Benchmarks.NON_SPHERICAL_XC[i])/np.abs(Benchmarks.NON_SPHERICAL_XC[i]) for i in range(len(ecl_nsp))]
        rms_xc_nsp = np.sqrt(np.mean(np.array(err_xc_nsp)**2))

        if Settings.DO_AE6:
            ae6_err, ae6_etot = assess_ae6_for_scan(ae6_dens, xparams, cparams)
            ae6_me = np.mean(ae6_err)
            ae6_mae = np.mean(np.abs(ae6_err))

        if Settings.DO_BH6:
            bh6_err, bh6_etot = assess_bh6_for_scan(bh6_dens, xparams, cparams)
            bh6_me = np.mean(bh6_err)
            bh6_mae = np.mean(np.abs(bh6_err))

        if Settings.DO_AR2:
            exl_ar2 = get_Ex_for_atoms(ar2_dens, xparams)*Benchmarks.EH_TO_KCAL
            ecl_ar2 = get_Ec_for_atoms(ar2_dens, cparams)*Benchmarks.EH_TO_KCAL
            exc_ar2 = exl_ar2 + ecl_ar2
            etot = {}

            for i, m in enumerate(Benchmarks.AR2_LIST):
                etot[m] = exc_ar2[i] + Benchmarks.AR2_NO_XC[m]

            seps = ["1.6", "1.8", "2.0"]

            cbs_a = jsextr(np.array([2,3,4]), np.array([etot['ArDZ'], etot['ArTZ'], etot['ArQZ']]))

            ar2_err = []
            ar2_mape = 0.0
            for k,s in enumerate(seps):
                cbs_e = jsextr(np.array([2,3,4]), np.array([etot[s+'DZ'], etot[s+'TZ'], etot[s+'QZ']]))
                ar2_err.append(cbs_e - 2*cbs_a - Benchmarks.AR2_BM[s])
                ar2_mape += 100*abs(ar2_err[k])/(len(seps)*Benchmarks.AR2_BM[s])

        if Settings.DO_JELLIUM:
            jellium_exl = []
            jellium_ecl = []
            jerr_xs = []
            jerr_cs = []
            jerr_xcs = []
            for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                ex = jellies[i].eval_x_func(SCAN2.getscan_x, xparams)
                jellium_exl.append(ex)
                jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
                ec = jellies[i].eval_c_func(SCAN2.getscan_c, cparams)
                jellium_ecl.append(ec)
                jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ec)/abs(Benchmarks.JELLIUM_C[i]))
                exc = jellium_exl[i] + ec
                jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

            jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
            jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
            jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_xcs)**2))

        ost = ""
        if Settings.DO_AE6:
            ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
        if Settings.DO_BH6:
            ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x, rms_c, rms_xc)
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x_nsp, rms_c_nsp, rms_xc_nsp)
        ost += ("{:.4f},{:.4f}%,"*2).format(lzbx,lzbx_pe,lzbc,lzbc_pe)
        if Settings.DO_AR2:
            ost += ("{:.3f},"*(len(Benchmarks.AR2_SEPS)+1)).format(*ar2_err,ar2_mape)
        if Settings.DO_JELLIUM:
            ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(jellium_rms_x, jellium_rms_c, jellium_rms_xc)
        ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
        ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*err_xc_nsp)
        if Settings.DO_AE6:
            ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
            ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
        if Settings.DO_BH6:
            ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
            ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
        if Settings.DO_JELLIUM:
            ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xcs)
        ofile.write(ost+"\n")

        #print(ost)

    raise SystemExit("SCAN report saved to: "+nstr)

# Finally, trigger to begin the program if this is main.
if __name__ == "__main__":
    runopt()
