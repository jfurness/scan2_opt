import numpy as np
import matplotlib.pyplot as plt
from os import system,path

from Benchmarks import BM_LIST
from Settings import JELLIUM_RS_LIST
from Jellium import Jelly
from Densities import Atom, AtomData


if not path.isdir('./iso_orb_figs/rg_atoms'):
    system('mkdir -p ./iso_orb_figs/rg_atoms')

if not path.isdir('./iso_orb_figs/sph_atoms'):
    system('mkdir -p ./iso_orb_figs/sph_atoms')

if not path.isdir('./iso_orb_figs/js'):
    system('mkdir -p ./iso_orb_figs/js')

min_dens = 1.e-14

def rare_gas_plots():

    for tatom in ['Ne','Ar','Kr','Xe']:

        nelec = AtomData.N_test[tatom.upper()][0] + AtomData.N_test[tatom.upper()][1]
        r = np.linspace(1.e-2/nelec,50,20000)

        tobj = Atom(tatom)
        d0, d1, g0, g1, t0, t1, _, _ = tobj.get_densities(r)

        n = d0 + d1
        mask = n >= min_dens

        gn = (g0 + g1)[mask]
        tau = (t0 + t1)[mask]

        n = n[mask]
        r = r[mask]

        tauw = gn**2/(8*n)
        tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

        p = 3/5*tauw/tau0

        tmtw = tau - tauw
        tmtw[tmtw<0.0] = 0.0

        alpha = tmtw/tau0
        elf = 1/(1 + alpha**2)

        beta = tmtw/(tau + tau0)

        fig,ax = plt.subplots(figsize=(6,4))
        ax.plot(r,elf,color='darkblue')
        ax.plot(r,1-beta,color='darkorange',linestyle='--')
        ax.plot(r,p,color='darkgreen',linestyle=':')

        ax.set_ylim([-.01,1.01])
        ax.set_xlim([1.e-2,r.max()])
        ax.set_xscale('log')

        iloc = np.argmin(np.abs(r - 6))
        ax.annotate('ELF',(r[iloc],.02+elf[iloc]),color='darkblue',fontsize=14)
        if tatom == 'Ne':
            xypos_bet = (r[iloc]-.5,.92-beta[iloc])
        else:
            xypos_bet = (r[iloc],.94-beta[iloc])
        ax.annotate('$1-\\beta$',xypos_bet,color='darkorange',fontsize=14)
        iloc = np.argmin(np.abs(r - 1.5e-2))
        ax.annotate('$p$',(r[iloc],-.06+p[iloc]),color='darkgreen',fontsize=14)

        ax.annotate('{:}'.format(tatom),(ax.get_xlim()[0]+1.e-3,0.0),\
            fontsize=20,annotation_clip=False)

        ax.hlines(0.5,*ax.get_xlim(),color='gray')

        ax.set_xlabel('$r$ (bohr)',fontsize=12)
        #plt.title('ELF (blue) and $1-\\beta$ (orange); {:} atom'.format(tatom),fontsize=12)

        #plt.show() ; exit()
        plt.savefig('./iso_orb_figs/rg_atoms/{:}_atom.pdf'.format(tatom),dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()
        plt.close()

def sph_atom_plots():

    for tatom in BM_LIST:

        fig,ax = plt.subplots(2,1,figsize=(6,6))

        if tatom in ['Ne','Ar','Kr','Xe']:
            continue

        nelec = AtomData.N_test[tatom.upper()][0] + AtomData.N_test[tatom.upper()][1]
        r = np.linspace(1.e-2/nelec,50,20000)

        tobj = Atom(tatom)
        atdens = np.zeros((r.shape[0],6))
        atdens[:,0], atdens[:,1], atdens[:,2], atdens[:,3], atdens[:,4], \
            atdens[:,5], _, _ = tobj.get_densities(r)

        mask = atdens[:,0] + atdens[:,1] >= min_dens
        r = r[mask]

        for i in range(2):

            n = atdens[mask,i]
            gn = atdens[mask,2+i]
            tau = atdens[mask,4+i]

            tauw = gn**2/(8*n)
            tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

            p = 3/5*tauw/tau0

            tmtw = tau - tauw
            tmtw[tmtw<0.0] = 0.0

            alpha = tmtw/tau0
            elf = 1/(1 + alpha**2)

            beta = tmtw/(tau + tau0)

            #elf2 = (1 - beta)**2/((1 - beta)**2 + beta**2)
            if i == 0:
                modstr = '\\uparrow'
            elif i == 1:
                modstr = '\\downarrow'

            ax[i].plot(r,elf,color='darkblue',linestyle='-',\
                label='ELF$_{:}$'.format(modstr))
            ax[i].plot(r,1-beta,color='darkorange',linestyle='--',\
                label='$(1-\\beta_{:})$'.format(modstr))
            ax[i].plot(r,p,color='darkgreen',linestyle=':')

            #ax[i].plot(r,elf2,color='darkgreen',linestyle='-.',\
            #    label='$(1-\\beta_{:})$'.format(modstr))
            ax[i].set_ylabel('${:}$-spin'.format(modstr),fontsize=12)

            ax[i].set_ylim([-.01,1.01])
            ax[i].set_xlim([1.e-2,r.max()])
            ax[i].set_xscale('log')

            if i == 0:
                if tatom in ['Li','Na','K','Cu','Cr']:
                    posd_elf = {'Li': (9.7,.4), 'Na': (2.9,.14), 'K': (4.4,.3),
                        'Cr': (5.2, .2), 'Cu': (3.5, .17)}
                    posd_bet = {'Li': (5.7,.9), 'Na': (.03,.88), 'K': (.015,.85),
                        'Cr': (.015,.83), 'Cu': (.013,.81)}
                    xypos_elf = posd_elf[tatom]
                    xypos_bet = posd_bet[tatom]
                else:
                    iloc = np.argmin(np.abs(r - 6))
                    xypos_elf = (r[iloc],.02+elf[iloc])
                    xypos_bet = (r[iloc],.92-beta[iloc])
                ax[i].annotate('ELF',xypos_elf,color='darkblue',fontsize=14)
                ax[i].annotate('$1-\\beta$',xypos_bet,color='darkorange',fontsize=14)
                iloc = np.argmin(np.abs(r - 1.5e-2))
                ax[i].annotate('$p$',(r[iloc],-.06+p[iloc]),color='darkgreen',fontsize=14)

            ax[i].hlines(0.5,*ax[i].get_xlim(),color='gray')

        ax[1].annotate('{:}'.format(tatom),(ax[1].get_xlim()[0]+1.e-3,0.0),\
            fontsize=20,annotation_clip=False)

        #ax.legend(fontsize=12,loc='lower left')
        ax[1].set_xlabel('$r$ (bohr)',fontsize=12)
        #plt.suptitle('ELF (blue) and $1-\\beta$ (orange); {:} atom'.format(tatom),fontsize=12)

        #plt.show() #; exit()
        plt.savefig('./iso_orb_figs/sph_atoms/{:}_atom.pdf'.format(tatom),dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()
        plt.close()

    return

def js_plots():

    for rs0 in JELLIUM_RS_LIST:
        tobj = Jelly(rs0)

        n = tobj.d
        gn = tobj.g
        tau = 2*tobj.tau_0

        tauw = gn**2/(8*n)
        tau0 = 3/10*(3*np.pi**2*n)**(2/3)*n

        p = 3/5*tauw/tau0

        tmtw = tau - tauw
        tmtw[tmtw<0.0] = 0.0

        alpha = tmtw/tau0
        elf = 1/(1 + alpha**2)

        beta = tmtw/(tau + tau0)

        fig,ax = plt.subplots(figsize=(6,4))

        kf0 = (9*np.pi/4)**(1/3)/rs0
        lf0 = 2*np.pi/kf0

        zs = tobj.r
        ax.plot(zs,elf,color='darkblue')
        ax.plot(zs,1-beta,color='darkorange',linestyle='--')
        ax.plot(zs,p,color='darkgreen',linestyle=':')

        ax.set_ylim([0,1])
        ax.set_xlim([zs.min(),zs.max()])

        iloc = np.argmin(np.abs(zs - .5))
        ax.annotate('ELF',(zs[iloc],.02+elf[iloc]),color='darkblue',fontsize=14)
        ax.annotate('$1-\\beta$',(zs[iloc],.94-beta[iloc]),color='darkorange',fontsize=14)
        iloc = np.argmin(np.abs(p - .2))

        ax.annotate('$p$',(-.2+zs[iloc],p[iloc]),color='darkgreen',fontsize=14)

        ax.annotate('$\\overline{r_\\mathrm{s}}='+'{:}$'.format(rs0),\
            (ax.get_xlim()[0]+2.e-2,0.025), fontsize=20,annotation_clip=False)

        ax.hlines(0.5,*ax.get_xlim(),color='gray')

        ax.set_xlabel('$z/\\overline{\\lambda}_\\mathrm{F}$',fontsize=12)
        #plt.title('ELF (blue) and $1-\\beta$ (orange); $r_\\mathrm{s}'+'={:}$ jellium surface'.format(rs0),fontsize=12)

        #plt.show() ; exit()
        plt.savefig('./iso_orb_figs/js/rs_{:}_js.pdf'.format(rs0),dpi=600,bbox_inches='tight')

        plt.cla()
        plt.clf()
        plt.close()

    return

if __name__ == "__main__":

    rare_gas_plots()
    #sph_atom_plots()
    #js_plots()
