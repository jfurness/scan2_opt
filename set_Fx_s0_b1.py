import numpy as np
import SCAN2
import matplotlib.pyplot as plt

xparams = dict(
    K1 = 0.086,
    A1 = 5.57996055999,
    CAX = -4.0,
    CBX = 60.9,
    CCX = -51.1012745764,
    CDX = 1.0,
    MUAK = 10.0/81.0,
    K0 = 0.1740,
    B1X = 1.0,
    B2X = 0.12083,
    B3X = 0.5,
    B4X = 1.0,
    AX = -0.7385587663820224058842300326808360,
    INTERP = "POLY",
    # Parameters for old interpolation
    C1X = 1.0,
    C2X= 0.7,
    DX = 1.24
)

s2 = np.linspace(0, 25, 500, endpoint=True)
beta = np.linspace(0,1, 500,endpoint=True)

# gb, gs = np.meshgrid(beta, s2)
# Fx = SCAN2.scanFx(xparams, gs, gb)

# print SCAN2.scanFx(xparams, np.array([0.0]), np.array([1.0]))
# levels = [0, 0.01, 1.0, 1.25]
# cs = plt.contourf(gb, gs, Fx)
# plt.colorbar(cs)
# plt.show()

# plt.plot(np.sqrt(s2), SCAN2.scanFx(xparams, s2, np.zeros(s2.shape)),label="0")
# plt.plot(np.sqrt(s2), SCAN2.scanFx(xparams, s2, 0.5*np.ones(s2.shape)),label="0.5")
# plt.plot(np.sqrt(s2), SCAN2.scanFx(xparams, s2, np.ones(s2.shape)),label="1")
foo = []
for i in np.linspace(0, 1.0, 20, endpoint=True):
    a, = plt.plot(np.sqrt(s2), SCAN2.scanFx(xparams, s2, i*np.ones(s2.shape)),label=str(i),c=(1.0-i, 0, i))
    foo.append(a)

plt.legend([foo[0], foo[-1]], ["$\\beta = 0$", "$\\beta = 1$"])
plt.ylabel("$F_x$")
plt.xlabel("$s$")
plt.ylim([0,1.2])
plt.xlim([0,5])
plt.show()