import numpy as np
from Jellium import Jelly
from scipy.optimize import least_squares,minimize
from functools import partial

jellium_ref_x = {2: 2624.0, 3: 526.0, 4: 157.0, 6: 22.0 }
pi = np.pi
rs_l = [2,3,4,6]

def hx1_tanh(p,ba,k,eta=0.01):

    cp = 10/81
    tfac = (2/3 + 3*eta/4)
    cpp = tfac*(146/2025*tfac - 73/405)
    cpa = 73*eta/1500 - 511/13500
    caa = 73/5000

    hx1 = 1 + k[0]*np.tanh(cp*p/k[0]) + k[1]*np.tanh(cpp*p**2/k[1]) \
        + k[2]*np.tanh(cpa*(ba - 1)*p/k[2]) + k[3]*np.tanh(caa*(ba-1)**2/k[3])
    #hx1 = 1 + k[0]*np.tanh(p) + k[1]*np.tanh(p**2) \
    #    + k[2]*np.tanh((ba - 1)*p) + k[3]*np.tanh((ba-1)**2)

    return hx1

def ex_den_hx1_unp(n,gn,tau,k,eta=0.01):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3*kf*n/(4*pi)
    p = (gn/(2*kf*n))**2

    tau_W = gn**2/(8*n)
    tau_TF = 0.3*kf**2*n

    ba = np.maximum(0.0, (tau - tau_W)/(tau_TF + eta*tau_W))

    hx1 = hx1_tanh(p,ba,k,eta=eta)

    return exlda*hx1

def fit_obj(k,jellies,eta=0.01,rscalar = True):

    exl = np.zeros(len(rs_l))
    for irs,rs in enumerate(rs_l):
        tobj = jellies[irs]
        iwg = tobj.weights*tobj.hat*tobj.CFACT
        tmsk = tobj.d >= 1e-20
        gdens = np.abs(tobj.g)
        ked = 2*tobj.tau_0

        iwg = tobj.weights*tobj.hat*tobj.CFACT
        tmsk = tobj.d >= 1e-20

        exden = ex_den_hx1_unp(tobj.d,gdens,ked,k,eta=eta)
        itrgd = exden*iwg

        exuni = -3.0*tobj.kF0/(4.0*pi)
        sxuni = np.dot(tobj.d*exuni, iwg)
        exl[irs] = np.sum(itrgd[tmsk]) - sxuni - jellium_ref_x[rs]

    if rscalar:
        return np.sum(np.abs(exl))
    else:
        return exl

def fit_hx1(eta=0.01):

    Ex_mat = np.zeros((len(rs_l),5))

    jells = []
    for irs,rs in enumerate(rs_l):
        jells.append(Jelly(rs))
    fobj = partial(fit_obj,jellies=jells,eta=eta)

    cp = 10/81
    tfac = (2/3 + 3*eta/4)
    cpp = tfac*(146/2025*tfac - 73/405)
    cpa = 73*eta/1500 - 511/13500
    caa = 73/5000

    tsum = (cp + abs(cpp) + abs(cpa) + caa)/.174
    c0 = [cp/tsum,cpp/tsum,cpa/tsum,caa/tsum]

    res = minimize(fobj,c0)
    for y in res.x:
        print(y)
    print(res)
    print(fit_obj(res.x,jells,eta=eta,rscalar = False))
    exit()

    for irs,rs in enumerate(rs_l):
        tobj = Jelly(rs)

        iwg = tobj.weights*tobj.hat*tobj.CFACT
        tmsk = tobj.d >= 1e-20
        gdens = np.abs(tobj.g)
        ked = 2*tobj.tau_0

        iwg = tobj.weights*tobj.hat*tobj.CFACT
        tmsk = tobj.d >= 1e-20

        for i in range(5):
            k = np.zeros(4)
            if i > 0:
                k[i-1] = 1.0
            exden = ex_den_hx1_unp(tobj.d,gdens,ked,k,eta=eta)
            itrgd = exden*iwg
            Ex_mat[irs,i] = np.sum(itrgd[tmsk]) - Ex_mat[irs,0]

        exuni = -3.0*tobj.kF0/(4.0*pi)
        sxuni = np.dot(tobj.d*exuni, iwg)
        Ex_mat[irs,0] -= sxuni

    def tmpobj(k):
        errl = np.zeros(len(rs_l))
        for irs,rs in enumerate(rs_l):
            Ex = Ex_mat[irs,0] + np.dot(k,Ex_mat[irs,1:])
            print(Ex)
            errl[irs] = Ex - jellium_ref_x[rs]
        return errl

    cp = 10/81
    tfac = (2/3 + 3*eta/4)
    cpp = tfac*(146/2025*tfac - 73/405)
    cpa = 73*eta/1500 - 511/13500
    caa = 73/5000
    print([cp,cpp,cpa,caa])
    print(tmpobj([cp,cpp,cpa,caa]))
    print(tmpobj(np.zeros(4)))

    res = least_squares(tmpobj,np.zeros(4))
    print(res)
    return

if __name__ == "__main__":

    fit_hx1(eta = 0.01)
