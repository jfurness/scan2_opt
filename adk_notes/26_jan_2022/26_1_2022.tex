\documentclass[12pt]{article}

\usepackage[margin=1in]{geometry}

\usepackage{amsmath,amssymb,bm,graphicx,caption,subcaption,pifont,multirow}

\newcommand{\sux}{_\mathrm{x}}
\newcommand{\suc}{_\mathrm{c}}
\newcommand{\ba}{\overline{\alpha}}
\newcommand{\rs}{r_\mathrm{s}}

\begin{document}

\title{Rational polynomial vs. Chebyshev}
\author{ADK}
\date{\today}

\maketitle

One possible interpolation function is a simple rational polynomial in $x \geq 0$,
\begin{equation}
  f^\text{JPP}(x) = \sum_{i=1}^M c_i \left(\frac{1-x}{1+x} \right)^i. \label{eq:jpp_poly}
\end{equation}
The number of linear parameters $c_i$ can be determined by the number of constraints placed on $f(x)$.
For example, we can see how Eq. \ref{eq:jpp_poly} compares to the TASK interpolation function, which satisfies
\begin{align}
  f^\text{TASK}(0) &= 1 \\
  f^\text{TASK}(1) &= 0 \\
  \frac{d f^\text{TASK}}{dx}(1) &= f_1 \equiv \frac{1}{2f_a}\left[-f_b - \sqrt{f_b^2 - 4 f_a f_c} \right] \approx -1.206307 \\
  f_a &=\frac{400\kappa_0}{243}\\
  f_b &= \frac{200 + 6\kappa_0}{729}\\
  f_c &= -\frac{511}{6075} \\
  & \nonumber \\
  \frac{d^2 f^\text{TASK}}{dx^2}(1) &= f_2 \equiv \frac{73}{2500\kappa_0} - \frac{1}{50}f\sux'(1) \approx 0.191942
  & \nonumber \\
  f^\text{TASK}(\infty) &= d\sux \equiv -3,
\end{align}
with $\kappa_0=0.174$.

TASK uses an expansion in rational Chebyshev polynomials
\begin{equation}
  R_n(x) \equiv T_n\left(\frac{1-x}{1+x} \right)
\end{equation}
where the Chebyshev polynomials can be found as $T_n = \cos(n \arccos(x))$, or by the recurrence relations
\begin{align}
  T_0 &= 1 \\
  T_1 &= x \\
  T_{n+1}(x) &= 2T_n(x) - T_{n-1}(x), \qquad n \geq 1.
\end{align}
See NIST's Digital Library of Mathematical Functions Sec. 18.9 for these recurrence relations (https://dlmf.nist.gov/18.9).
Then the TASK interpolation function is
\begin{align}
  f\sux^\mathrm{TASK}(x) &= \sum_{i=0}^4 b_i R_i(x) \label{eq:ftask}  \\
  [b_0,b_1,b_2,b_3,b_4] &= [-0.628591,-2.10315,-0.5,0.103153,0.128591].
\end{align}

\begin{figure}
  \centering
  \includegraphics[width=0.7\columnwidth]{f_jpp_task.pdf}
  \caption{Comparison of Eq. \ref{eq:jpp_poly}, evaluated on the TASK parameters, and the original TASK expression, Eq. \ref{eq:ftask}.
  The new expression is essentially identical to TASK.
  \label{fig:task_ief_log}}
\end{figure}

To generate the new parameters from the 4 TASK constraints [$f^\text{JPP}(1)=0$ already], we compute
\begin{align}
  \frac{d f^\text{JPP}}{dx}(x) &= \sum_{i=1}^5 i c_i \left(\frac{1-x}{1+x} \right)^{i-1} \frac{-2}{(1+x)^2} \\
  \frac{d f^\text{JPP}}{dx}(x) &= -2\sum_{i=1}^5 i c_i\frac{(1-x)^{i-1}}{(1+x)^{i+1}} \\
  & \nonumber \\
  \frac{d^2 f^\text{JPP}}{dx^2}(x) &= 2\sum_{i=1}^5 i c_i \left[(i-1)
    \frac{(1-x)^{i-2}}{(1+x)^{i+1}} + (i+1)\frac{(1-x)^{i-1}}{(1+x)^{i+2}} \right] \\
  \frac{d^2 f^\text{JPP}}{dx^2}(x) &= 2\sum_{i=1}^5 i c_i\frac{(1-x)^{i-2}}{(1+x)^{i+2}}
    \underbrace{\left[(i-1)(1+x) + (i+1)(1-x)\right]}_{=2(i-x)} \\
  \frac{d^2 f^\text{JPP}}{dx^2}(x) &= 4\sum_{i=1}^5 i(i-x) c_i\frac{(1-x)^{i-2}}{(1+x)^{i+2}}.
\end{align}
Thus we have
\begin{align}
  f^\text{JPP}(0) &= \sum_{i=1}^4 c_i = 1 \\
  \frac{d f^\text{JPP}}{dx}(1) &= -\frac{1}{2}c_1 = f_1 \\
  \frac{d^2 f^\text{JPP}}{dx^2}(1) &= \frac{1}{2}(c_1 + c_2) = f_2 \\
  f^\text{JPP}(\infty) &= \sum_{i=1}^4 (-1)^i c_i = -d.
\end{align}
Solving these yields
\begin{align}
  c_1 &= -2 f_1 \\
  c_2 &= 2(f_2 + f_1 ) \\
  c_4 &= \frac{1-d}{2} - 2(f_1 + f_2) \\
  c_3 &= d + 2(f_2 + 2 f_1) + c_4.
\end{align}
As shown in Figs. \ref{fig:task_ief_log} and \ref{fig:task_ief}, the rational polynomial of \ref{eq:jpp_poly} and the Chebyshev rational polynomial are identical up to the precision of the TASK parameters (about 5 to 6 digits of precision).

\begin{figure}
  \centering
  \includegraphics[width=0.7\columnwidth]{f_comp_task.pdf}
  \caption{Comparison of the smooth $f(x)$ (blue) and Eq. \ref{eq:jpp_poly} (green) evaluated on the TASK parameters, and the original TASK expression (orange), Eq. \ref{eq:ftask}.
  \label{fig:task_ief}}
\end{figure}


\end{document}
