\documentclass[12pt]{article}

\usepackage[margin=1in]{geometry}

\usepackage{amsmath,amssymb,bm,graphicx,multirow}
\usepackage{xcolor,xspace}

\newcommand{\ba}{\overline{\alpha}}
\newcommand{\rs}{r_\mathrm{s}}
\newcommand{\kf}{k_\mathrm{F}}
\newcommand{\sux}{_\mathrm{x}}
\newcommand{\suc}{_\mathrm{c}}
\newcommand{\sus}{_\mathrm{s}}
\newcommand{\nup}{n_{\uparrow}}
\newcommand{\ndn}{n_{\downarrow}}

\begin{document}

\title{$\beta$-dependent meta-GGAs}
\author{ADK}
\date{\today}
\maketitle

\section{Exchange}

The iso-orbital indicator
\begin{equation}
  \beta = \frac{\tau - \tau_W}{\tau + \tau_0}
\end{equation}
where $\tau_0 = 3\kf^2 n/10$ and $\tau_W = 5p\tau_0/3$, has a second order, spin-unpolarized gradient expansion
\begin{equation}
  2\beta = 1 + \frac{10}{9}q - \frac{85}{54}p + \mathcal{O}(\nabla^4).
\end{equation}
Suppose now that the exchange enhancement factor is
\begin{equation}
  F\sux(p,\beta) = \{ h\sux(p) + f\sux(\beta)[h\sux^0 - h\sux(p)]\}g\sux(p)
\end{equation}
where each component has a Taylor series
\begin{align}
  h\sux(p) &= 1 + h\sux'(0)p + \mathcal{O}(\nabla^4) \\
  g\sux(p) &= 1 + g\sux'(0)p + \mathcal{O}(\nabla^4) \\
  f\sux(\beta) &= f\sux'(1/2)(\beta - 1/2) + \mathcal{O}(\nabla^4)\\
    &= f\sux'(1/2) \left[ \frac{5}{9}q - \frac{85}{108}p \right] + \mathcal{O}(\nabla^4).
    \nonumber
\end{align}
Let $h\sux^0 = 1 + \kappa_0$.
Then the overall exchange enhancement factor has a gradient expansion
\begin{align}
  F\sux(p,\beta) &= \left\{ 1 + h\sux'(0)p +
    f\sux'(1/2) \left[ \frac{5}{9}q - \frac{85}{108}p \right]
    \cdot \left[\kappa_0 - h\sux'(0)p \right] \right\}
    \left[1 +  g\sux'(0)p \right] + \mathcal{O}(\nabla^4) \\
  &= 1 + \left[h\sux'(0) + g\sux'(0) - \frac{85}{108}f\sux'(1/2)\kappa_0 \right]p
    + \frac{5}{9} f\sux'(1/2)\kappa_0 q + \mathcal{O}(\nabla^4).
\end{align}
We can eliminate the term linear in $q$ by expressing it as
\begin{equation}
  q = \frac{1}{3}p + n^{-4/3} \nabla \cdot\left[\frac{\nabla n}{4(3\pi^2)^{2/3}n^{1/3}} \right].
\end{equation}
Thus
\begin{align}
  F\sux(p,\beta) &= 1 + \left[h\sux'(0) + g\sux'(0) +
      \left(\frac{5}{27} - \frac{85}{108} \right)f\sux'(1/2)\kappa_0 \right]p
       \\
  & + n^{-4/3} \nabla \cdot\left[ \frac{5f\sux'(1/2)\kappa_0}{36(3\pi^2)^{2/3}} \frac{\nabla n}{n^{1/3}} \right]
    + \mathcal{O}(\nabla^4) \nonumber \\
  &= \widetilde{F}\sux + n^{-4/3}\nabla \cdot \bm{G}\sux, \\
  \widetilde{F}\sux &= 1 + \left[h\sux'(0) + g\sux'(0) - \frac{65}{108}f\sux'(1/2)\kappa_0 \right]p
    + \mathcal{O}(\nabla^4) \\
  \bm{G}\sux &= \frac{5f\sux'(1/2)\kappa_0}{36(3\pi^2)^{2/3}} \frac{\nabla n}{n^{1/3}}.
\end{align}
Thus to recover the second-order gradient expansion for exchange, we set
\begin{equation}
  h\sux'(0) + g\sux'(0) - \frac{65}{108}f\sux'(1/2)\kappa_0 = \mu \equiv \frac{10}{81}.
\end{equation}
For convenience, we define a model parameter $\delta \leq 1$ such that
\begin{align}
  h\sux'(0) &= \delta \mu \\
  f\sux'(1/2) &= \frac{108\left[ (\delta - 1)\mu + g\sux'(0) \right]}{65\kappa_0}.
\end{align}
$g\sux'(0)$ is to be determined (indirectly) from the hydrogen atom exchange energy constraint.
We can reuse the pole-free expression for $h\sux'(0)$ used for the $\ba$-dependent meta-GGA
\begin{equation}
  h\sux(p) = \frac{1 + \mu p + (1 + \kappa_1)p^2/d_{p2}^4}{1 + [\mu - h\sux'(0)]p + p^2/d_{p2}^4}.
  \label{eq:hx_rat_poly}
\end{equation}
This expression reduces to a constant only when $\delta = 0$, making $h\sux'(0)=0$, \emph{and} $\kappa_1 = 0$.
$d_{p2}$ is a model parameter.

For the interpolation function, we exploit the boundedness of $\beta$ in a simple polynomial
\begin{align}
  f\sux(\beta) &= \sum_{i=1}^M c_{i\mathrm{x}}(2\beta-1)^i, \\
  f\sux'(\beta) &= 2 \left[ c_{1\mathrm{x}} + \sum_{i=2}^M i c_{i\mathrm{x}}(2\beta-1)^{i-1} \right].
\end{align}
The number $M$ of parameters $c_{i\mathrm{x}}$ is determined by how many constraints are placed on the interpolation function.
We always need $f\sux(0)=1$, and we constrain $f\sux'(1/2)$ to recover the second-order gradient expansion.
To improve norm performance, we will also constrain $f\sux'(0)$ and $f\sux(1)$,
\begin{align}
  f\sux(0) &= \sum_{i=1}^4 (-1)^i c_{i\mathrm{x}} = 1 \label{eq:fx0_cons} \\
  f\sux'(0) &= 2 \sum_{i=1}^4 i (-1)^i c_{i\mathrm{x}} \\
  f\sux'(1/2) &= 2 c_{1\mathrm{x}} \\
  f\sux(1) &= \sum_{i=1}^4 c_{i\mathrm{x}} = - d\sux. \label{eq:fx1_cons}
\end{align}
These equations can be solved as
\begin{align}
  c_{1\mathrm{x}} &= \frac{1}{2}f\sux'(1/2) \\
  c_{3\mathrm{x}} &= -\frac{1}{2} \left(1 + d\sux + 2c_{1\mathrm{x}} \right) \\
  c_{2\mathrm{x}} &= \frac{1}{4} \left[ f\sux'(0) + 7 + 4 c_{1\mathrm{x}} - d\sux \right] \\
  c_{4\mathrm{x}} &= \frac{1}{2} \left( 1 - d\sux - 2 c_{2\mathrm{x}} \right).
\end{align}
Following this framework, more constraints can easily be added and removed to the model.
While adding more constraints can prove tedious, the equations are linear and can be solved numerically.
For example, we can rewrite Eqs. \ref{eq:fx0_cons}--\ref{eq:fx1_cons} as
\begin{equation}
  \begin{pmatrix}
    -1 & 1 & -1 & 1 \\
    -1 & 2 & -3 & 4 \\
    1 & 0 & 0 & 0 \\
    1 & 1 & 1 & 1
  \end{pmatrix}
  \begin{pmatrix} c_{1\mathrm{x}} \\ c_{2\mathrm{x}} \\ c_{3\mathrm{x}} \\ c_{4\mathrm{x}} \end{pmatrix}
  = \begin{pmatrix} 1 \\ f\sux'(0)/2 \\ f\sux'(1/2)/2 \\ -d\sux \end{pmatrix}
\end{equation}

\section{Correlation}

We derived elsewhere that the spin-resolved $\beta(\nup,\ndn)$ has a second-order gradient expansion
\begin{align}
  \beta(\nup,\ndn) = \frac{1}{2} + \frac{5}{9 d\sus(\zeta)}q - \frac{85}{108 d\sus(\zeta)}p
    + \frac{5}{54(1-\zeta^2)}\sigma^2 + \mathcal{O}(\nabla^4)
\end{align}
where $\sigma = |\nabla \zeta|/(2\kf)$.
This expression presumes that the Weizs\"acker kinetic energy is not spin-resolved.
Fully spin-resolving $\tau_W$ will only change the coefficient of $\sigma^2$, which will be irrelevant later on in this derivation.

Suppose we write the correlation energy density as
\begin{align}
  \varepsilon\suc(\rs,\zeta,p,\ba) &= \varepsilon\suc^1(\rs,\zeta,p) + f\suc(\beta)
  [ \varepsilon\suc^0(\rs,\zeta,p) - \varepsilon\suc^1(\rs,\zeta,p)],
\end{align}
where each component has Taylor series
\begin{align}
  \varepsilon\suc^0(\rs,\zeta,p) &= \varepsilon\suc^{\text{LDA0}}(\rs)g\suc(\zeta) + \chi_{\infty}g\suc(\zeta) p + \mathcal{O}(\nabla^4) \\
  \varepsilon\suc^1(\rs,\zeta,p) &= \varepsilon\suc^{\text{LSDA1}}(\rs,\zeta) + \beta(\rs)\phi^3(\zeta)t^2
  - \gamma \phi^3(\zeta)w_1 D\suc p + \mathcal{O}(\nabla^4) \\
  f\suc(\beta) &= \frac{1}{2}f\suc'(1/2)(2\beta - 1) + \mathcal{O}(\nabla^4) \\
    &= f\suc'(1/2) \left[ \frac{5}{9 d\sus(\zeta)}q - \frac{85}{108 d\sus(\zeta)}p
      + \frac{5}{54(1-\zeta^2)}\sigma^2  \right] + \mathcal{O}(\nabla^4).
    \nonumber
\end{align}
Then the correlation energy density gradient expansion is
\begin{align}
  \varepsilon\suc(\rs,\zeta,p) &= \varepsilon\suc^{\text{LSDA1}}(\rs,\zeta)
    + \beta(\rs)\phi^3(\zeta)t^2 - \gamma \phi^3(\zeta)w_1 D\suc p \nonumber \\
  & + f\suc'(1/2) \left[ \frac{5}{9 d\sus(\zeta)}q - \frac{85}{108 d\sus(\zeta)}p
    + \frac{5}{54(1-\zeta^2)}\sigma^2  \right]  \nonumber \\
  & \times \left[ \varepsilon\suc^{\text{LDA0}}(\rs)g\suc(\zeta) + \chi_{\infty}g\suc(\zeta) p
    - \varepsilon\suc^{\text{LSDA1}}(\rs,\zeta) - \beta(\rs)\phi^3(\zeta)t^2
    + \gamma \phi^3(\zeta)w_1 D\suc p \right] + \mathcal{O}(\nabla^4) \nonumber \\
  &= \varepsilon\suc^{\text{LSDA1}}(\rs,\zeta) + \beta(\rs)\phi^3(\zeta)t^2
    - \left\{ \frac{85f\suc'(1/2)}{108 d\sus(\zeta)}
      \left[\varepsilon\suc^{\text{LSDA0}} - \varepsilon\suc^{\text{LSDA1}} \right]
     + \gamma \phi^3(\zeta)w_1 D\suc \right\} p \nonumber \\
  & + \frac{5f\suc'(1/2)}{54(1-\zeta^2)}\left[\varepsilon\suc^{\text{LSDA0}} - \varepsilon\suc^{\text{LSDA1}} \right] \sigma^2
  + \frac{5 f\suc'(1/2) }{9 d\sus(\zeta)}\left[\varepsilon\suc^{\text{LSDA0}} - \varepsilon\suc^{\text{LSDA1}} \right] q + \mathcal{O}(\nabla^4),
\end{align}
where $\varepsilon\suc^{\text{LSDA0}} \equiv \varepsilon\suc^{\text{LDA0}}(\rs)g\suc(\zeta)$.
To eliminate the term linear in $q$, we employ the same integration-by-parts trick as in r$^2$SCAN,
\begin{align}
  & \frac{\varepsilon\suc^{\text{LSDA0}}-\varepsilon\suc^{\text{LSDA1}}}{d\sus(\zeta)}q =
  \left\{ \frac{2}{3} \frac{\varepsilon\suc^{\text{LSDA0}}-\varepsilon\suc^{\text{LSDA1}}}{d\sus(\zeta)}
  + \frac{\rs}{3d\sus(\zeta)}
  \left[ \frac{\partial \varepsilon\suc^{\text{LSDA0}}}{\partial \rs}
  - \frac{\partial \varepsilon\suc^{\text{LSDA1}}}{\partial \rs} \right]
  \right\} p \\
  & - \frac{\nabla n \cdot \nabla \zeta}{4(3\pi^2)^{2/3} n^{5/3}}
  \frac{\partial}{\partial \zeta}
  \left[ \frac{\varepsilon\suc^{\text{LSDA0}}-\varepsilon\suc^{\text{LSDA1}}}{d\sus(\zeta)} \right]
  + n^{-1} \nabla \cdot \left\{ \frac{\varepsilon\suc^{\text{LSDA0}}-\varepsilon\suc^{\text{LSDA1}}}{d\sus(\zeta)} \frac{\nabla n}{4(3\pi^2)^{2/3} n^{2/3}} \right\} \nonumber.
\end{align}
Substituting for $q$,
\begin{align}
  \varepsilon\suc(\rs,\zeta,p) &=
    \varepsilon\suc^{\text{LSDA1}}(\rs,\zeta) + \beta(\rs)\phi^3(\zeta)t^2
    - \left\{ \frac{5 f\suc'(1/2)}{12 d\sus(\zeta)}
      \left[\varepsilon\suc^{\text{LSDA0}} - \varepsilon\suc^{\text{LSDA1}} \right] \right. \\
  & \left. - \frac{5 f\suc'(1/2) }{27 d\sus(\zeta)}\rs
     \left[ \frac{\partial \varepsilon\suc^{\text{LSDA0}}}{\partial \rs}
     - \frac{\partial \varepsilon\suc^{\text{LSDA1}}}{\partial \rs} \right]
     + \gamma \phi^3(\zeta)w_1 D\suc \right\} p \nonumber \\
  & - \frac{5 f\suc'(1/2) [\nabla n \cdot \nabla \zeta]}{36(3\pi^2)^{2/3} n^{5/3}}
  \frac{\partial}{\partial \zeta}
  \left[ \frac{\varepsilon\suc^{\text{LSDA0}}-\varepsilon\suc^{\text{LSDA1}}}{d\sus(\zeta)} \right]
  + \frac{5f\suc'(1/2)}{54(1-\zeta^2)}\left[\varepsilon\suc^{\text{LSDA0}} - \varepsilon\suc^{\text{LSDA1}} \right] \sigma^2 \nonumber \\
  & + n^{-1}\nabla \cdot \bm{G}\suc + \mathcal{O}(\nabla^6) \nonumber \\
  \bm{G}\suc &=  \frac{\varepsilon\suc^{\text{LSDA0}}-\varepsilon\suc^{\text{LSDA1}}}{d\sus(\zeta)}
    \frac{5f\suc'(1/2)\, \nabla n}{36(3\pi^2)^{2/3} n^{2/3}}.
\end{align}
Thus to recover the second-order gradient expansion for correlation, we take $\nabla \zeta \approx 0$ and
\begin{align}
  D\suc &= \frac{f\suc'(1/2)}{\gamma d\sus(\zeta) \phi^3(\zeta)w_1} \left\{
    \frac{5}{27} \rs
    \left[ \frac{\partial \varepsilon\suc^{\text{LSDA0}}}{\partial \rs}
    - \frac{\partial \varepsilon\suc^{\text{LSDA1}}}{\partial \rs} \right]
    - \frac{5}{12} \left[\varepsilon\suc^{\text{LSDA0}} - \varepsilon\suc^{\text{LSDA1}} \right]
  \right\},
\end{align}
which is identical to the r$^2$SCAN expression with the replacements
\begin{align*}
  \Delta f_{\mathrm{c}2} & \to f\suc'(1/2) \\
  20/27 & \to 5/27 \\
  -45/27 & \to -5/12,
\end{align*}
owing to the different interpolation function used, and different gradient expansion of the iso-orbital indicator used.

For the correlation interpolation function $f\suc(\beta)$, we can use the same set of constraints and equations for exchange, but independent parameters $f\suc'(0)$, $f\suc'(1/2)$, and $d\suc$.

\end{document}
