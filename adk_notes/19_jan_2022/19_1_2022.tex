\documentclass[12pt]{article}

\usepackage[margin=1in]{geometry}

\usepackage{amsmath,amssymb,bm,graphicx,caption,subcaption,pifont,multirow}
\usepackage{xcolor,xspace}

\newcommand{\sux}{_\mathrm{x}}
\newcommand{\suc}{_\mathrm{c}}
\newcommand{\ba}{\overline{\alpha}}
\newcommand{\rs}{r_\mathrm{s}}
\newcommand{\rrscan}{r$^2$SCAN\xspace}

\begin{document}

\title{Trying out different expressions for SCAN 2}
\author{ADK}
\date{\today}

\maketitle

\tableofcontents

\section{Smoother interpolation functions, same functionality}

The interpolation function $f(x)$, where $x$ is an iso-orbital indicator, is used to describe the local chemical environment.
SCAN chooses the piecewise
\begin{equation}
  f(x;c_1,c_2,d) = \left\{ \begin{array}{lr}
    \exp\left[ -c_1\frac{x}{1-x} \right], & x < 1 \\
    -d \exp\left[ \frac{c_2}{1-x} \right], & x > 1
  \end{array} \right. .
\end{equation}
This expression has obvious downsides, which rSCAN attempts to remedy as
\begin{equation}
  f(x;c_1,c_2,d,\bm{b}) = \left\{ \begin{array}{lr}
    \sum_{i=0}^7 b_i x^i, & x < 2.5 \\
    -d \exp\left[ \frac{c_2}{1-x} \right], & x > 2.5
  \end{array} \right. .
\end{equation}
The constants $\bm{b}$ are determined from the following 8 constraints
\begin{align}
  f(0;c_1,c_2,d,\bm{b}) &= 1 \\
  \frac{\partial f}{\partial x}(0;c_1,c_2,d,\bm{b}) &=
    \frac{\partial }{\partial x} \left\{ \exp\left[ -c_1\frac{x}{1-x} \right] \right\}_{x=0}
    = -c_1 \\
  \frac{\partial^2 f}{\partial x^2}(0;c_1,c_2,d,\bm{b}) &=
    \frac{\partial^2 }{\partial x^2} \left\{ \exp\left[ -c_1\frac{x}{1-x} \right] \right\}_{x=0}
    = c_1(c_1-2) \\
  f(1;c_1,c_2,d,\bm{b}) &= 0 \\
  \frac{\partial^m f}{\partial x^m}(2.5;c_1,c_2,d,\bm{b}) &=
    \frac{\partial^m }{\partial x^m} \left\{ -d \exp\left[ \frac{c_2}{1-x} \right] \right\}_{x=2.5},
    \qquad m \in \{1,2,3,4\}.
\end{align}
This produces reasonable smooth interpolation functions, but has the downside of imperfect smoothness at the matching point, and the correlation interpolation function ends up not being a monotone decreasing function of $x$.

To deduce a smoother interpolation function, we look at the asymptotics of the SCAN interpolation function.
Let $y = 1/x$; it's easy to see that SCAN's
\begin{equation}
  f(x;c_1,c_2,d) = \left\{ \begin{array}{lr}
    \exp\left[ -c_1\frac{x}{1-x} \right], & x < 1 \\
    -d \exp\left[ -\frac{c_2 y}{y-1} \right], & x > 1
  \end{array} \right.
\end{equation}
so we need only figure out the low-order Taylor series of
\begin{align}
  \exp\left[ -c\frac{x}{1-x} \right] &= \exp\left[ -c x(1 + x + x^2 +...) \right] \\
  &= \left( 1 - c x + \frac{c^2}{2}x^2 - ...\right)\left(1 - c x^2 + ... \right) \\
  &= 1 - c x + \frac{c(c - 2)}{2} x^2 + ...
\end{align}
Then
\begin{align}
  \lim_{x\to 0} f(x;c_1,c_2,d) &= 1 - c_1 x + \frac{c_1}{2}\left(c_1 - 2 \right) x^2 + ..., \\
  \lim_{x\to \infty} f(x;c_1,c_2,d) &= -d + \frac{d c_2}{x} - \frac{d c_2(c_2 - 2)}{2x^2} + ...
\end{align}
An expression that recovers the asymptotics to linear order in $x$, to leading order in $1/x$, and satisfies $f(1)=0$ and $f(0)=1$, is
\begin{equation}
  f(x;c_0,c_1,d) = \frac{1 + c_1 x - (1 + c_1) x^2}{1 + (c_0 + c_1) x + (1+c_1)x^2/d}. \label{eq:fsmth}
\end{equation}
Note that
\begin{align}
  \lim_{x\to 0} f(x;c_0,c_1,d) &= 1 - c_0 x + ... \\
  \lim_{x\to \infty} f(x;c_0,c_1,d) &= -d + \frac{-d c_0  + (1 + d)c_1}{1+c_1} \frac{1}{x} + ...
\end{align}
so we may make contact with the SCAN asymptotics by setting
\begin{equation}
  c_1 = \frac{c^\text{SCAN}_2 - d^\text{SCAN} c^\text{SCAN}_0}{1 + d^\text{SCAN} - c^\text{SCAN}_2}.
\end{equation}
Naively using these parameters gives quite close behavior to the rSCAN interpolation function for exchange with smoother first derivatives, as seen in Figs. \ref{fig:ief_x_comp} and \ref{fig:ief_c_comp}.
The fit can be improved by more judiciously choosing $c_0$ and $c_1$ for exchange, but not as much as for correlation.

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fx_comp_interp}
    \caption{}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fx_comp_interp_deriv}
    \caption{}
  \end{subfigure}
  \caption{Comparison of the new smooth (SMTH) X interpolation function with the SCAN and rSCAN interpolation functions (left), and their first derivatives (right).
  \label{fig:ief_x_comp}}
\end{figure}


\begin{figure}
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fc_comp_interp}
    \caption{}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fc_comp_interp_deriv}
    \caption{}
  \end{subfigure}
  \caption{Comparison of the new smooth (SMTH) C interpolation function with the SCAN and rSCAN interpolation functions (left), and their first derivatives (right).
  \label{fig:ief_c_comp}}
\end{figure}

Note that (dropping the parameter dependence)
\begin{align}
  f'(x) &= \frac{c_1 - 2(1+c_1)x - [c_0 + c_1 + 2(1+c_1)x/d]f(x)}{1 + (c_0 + c_1) x + (1+c_1)x^2/d} \\
  f'(1) &= -d\frac{2+c_1}{1 + d + d c_0 + (1+d)c_1} \\
  c_1 &= -\frac{2d + (1 + d + d c_0)f'(1)}{d + (1 + d) f'(1)} \label{eq:c1_from_c0}
\end{align}
and moreover
\begin{align}
  f''(x) &= \frac{ -2(1 + c_1) - 2(1+c_1)f(x)/d - 2[c_0 + c_1 + 2(1+c_1)x/d]f'(x)}{1 + (c_0 + c_1) x + (1+c_1)x^2/d} \\
  f''(1) &= \frac{-2d(1 + c_1) - 2[c_0 + c_1 + 2(1+c_1)x/d]f'(1)}{1 + d c_0 + (1 + d)c_1} \\
  c_0 &= -\frac{2d + 4 f'(1) + (1+d)f''(1) + \{(1+d)f''(1) + 2d[1+f'(1)] + 4 f'(1)\}c_1}{2d f'(1) + f''(1)}
    \label{eq:c0_from_c1}
\end{align}
where we have used $f(1)=0$ by construction.
Equation \ref{eq:c1_from_c0} can be substituted into Eq. \ref{eq:c0_from_c1} to show that
\begin{equation}
  c_0 = \frac{2 d + 4 (1 + d) f'(1) + 2 (1 + d) [f'(1)]^2 + (1 + d) f''(1)}{d f''(1) - 2 [f'(1)]^2},
  \label{eq:c0_from_derivs}
\end{equation}
which can then be inserted in Eq. \ref{eq:c1_from_c0} to find $c_1$.
Thus, given constraints $f'(1) \equiv f_1$ and $f''(1) \equiv f_2$, as for the fourth-order gradient expansion, Eq. \ref{eq:c0_from_derivs} can be used to constrain Eq. \ref{eq:fsmth} with one free parameter $d$.

As an example of this, consider the TASK interpolation function, which satisfies
\begin{align}
  f\sux'(1) &= \frac{1}{2f_a}\left[-f_b - \sqrt{f_b^2 - 4 f_a f_c} \right] \approx -1.206307 \\
  f_a &=\frac{400\kappa_0}{243}\\
  f_b &= \frac{200 + 6\kappa_0}{729}\\
  f_c &= -\frac{511}{6075} \\
  & \nonumber \\
  f\sux''(1) &= \frac{73}{2500\kappa_0} - \frac{1}{50}f\sux'(1) \approx 0.191942
  & \nonumber \\
  d\sux &= -3,
\end{align}
with $\kappa_0=0.174$.
Using these values in Eqs. \ref{eq:c0_from_derivs} and Eq. \ref{eq:c1_from_c0}, we find $c_0^\text{TASK} \approx 0.381975$ and $c_1^\text{TASK}\approx -0.113720$.
Figure \ref{fig:task_ief} shows that this expression gives essentially identical behavior to the TASK interpolation function,
\begin{align}
  f\sux^\mathrm{TASK}(x) &= b_0 + b_1\frac{x-1}{x+1} + b_2 \frac{x^2 - 6x + 1}{(x + 1)^2} \label{eq:ftask}  \\
    & + b_3 \frac{x^3 - 15x^2 + 15x - 1}{(x+1)^3} + b_4 \frac{x^4 - 28x^3 + 70x^2 - 28x + 1}{(x + 1)^4}  \nonumber \\
  [b_0,b_1,b_2,b_3,b_4] &= [-0.628591,-2.10315,-0.5,0.103153,0.128591],
\end{align}
but is smoother and less byzantine.

\begin{figure}
  \centering
  \includegraphics[width=0.7\columnwidth]{f_comp_task.pdf}
  \caption{Comparison of Eq. \ref{eq:fsmth}, evaluated on the TASK parameters, and the original TASK expression, Eq. \ref{eq:ftask}.
  The new expression is essentially identical to TASK for most values of $x$, but is much smoother.
  \label{fig:task_ief}}
\end{figure}


\section{Optimized r$^2$SCAN}

If only the second-order expansion is needed, Eq. \ref{eq:c1_from_c0} can be used to constrain $f'(1)$, and Eq. \ref{eq:fsmth} has two free parameters, $c_0$ and $d$.
For an exchange enhancement factor of the form,
\begin{align}
  F\sux(p,\ba) &= \{ h\sux(p) + f\sux(\ba)[1 + \kappa_0 - h\sux(p)]\} g\sux(p) \\
  g\sux(p) &= 1 - \exp[-a\sux p^{-1/4}] \\
  \ba &= \frac{\tau - \tau_\text{W}}{\tau_\text{UEG} + \eta \tau_\text{W}},
\end{align}
we derived
\begin{equation}
  h\sux'(0) - \frac{5(4 + 9\eta)\kappa_0}{27}f\sux'(1) = \mu \equiv \frac{10}{81}
\end{equation}
to recover the second-order gradient expansion for exchange.
Let $\delta \leq 1$ be a parameter, then
\begin{align}
  h\sux'(0) &= \frac{10}{81}\delta \\
  f\sux'(1) &= -\frac{2(1-\delta)}{3(4+9\eta)\kappa_0}.
\end{align}
Note that $\delta < 0$ is permissible, as this keeps $f\sux'(1) < 0$, but $\delta > 1$ is not.
In anticipation of recovering the fourth-order gradient expansion, where seemingly $h\sux'(0)<0$ and $h\sux''(0)<0$ is needed, we propose a few options for $h\sux(p)$ than can accommodate $h\sux'(0) <0$.

The most well-tested option is taken from r$^2$SCAN
\begin{align}
  h\sux(p) &= 1 + \kappa_1 - \frac{\kappa_1}{1 + x(p)/\kappa_1}
    = \frac{1 + (1+\kappa_1) x(p)/\kappa}{1 + x(p)/\kappa} \label{eq:r2_rev_hx} \\
  x(p) &= \left\{\mu + \exp\left[- p^2/d_{p2}^4\right][h\sux'(0) - \mu]\right\} p.
\end{align}
This expression interpolates between gradient coefficient $h\sux'(0)$ when $p^{1/2} \ll d_{p2}$, and $\mu$ when $p^{1/2} \gtrsim d_{p2}$.
We know this expression works well, but it comes at the price of three fit parameters, $\delta$, $d_{p2}$ and $\kappa_1$ (two extra parameters than SCAN).
We also have to carefully check for poles.

An expression which is guaranteed to be pole-free, and which is functionally identical to Eq. \ref{eq:r2_rev_hx}, is
\begin{equation}
  h\sux(p) = \frac{1 + \mu p + (1 + \kappa_1)p^2/d_{p2}^4}{1 + [\mu - h\sux'(0)]p + p^2/d_{p2}^4}.
  \label{eq:hx_rat_poly}
\end{equation}
This expression is pole-free because $h\sux'(0) < \mu$ to recover the second-order gradient expansion.

For the correlation component, we keep the r$^2$SCAN energy densities unchanged, except using the more correct acGGA expression for the gradient coefficient,
\begin{equation}
  \beta(\rs) = \beta(0)\frac{1 + 0.5 \rs + 0.083335 \rs^2}{1 + 0.5 \rs + 0.148165 \rs^2},
\end{equation}
and Eq. \ref{eq:fsmth} for the interpolation function (three fit parameters, same as SCAN).

Overall this model, which we call optimized-r$^2$SCAN (R2OPT), has two extra fit parameters than SCAN -- $\eta$ and $d_{p2}$, but effectively the same number as r$^2$SCAN.
$\delta$ eliminates the arbitrariness in the rSCAN interpolation function by setting its derivative at $\ba=1$ to an optimal value.
Table \ref{tab:fpars} compares the parameters used in SCAN, r$^2$SCAN, and R2OPT, as well as their purpose.

\begin{table}
  \centering
  \begin{tabular}{cccccc} \hline
    & Parameter & Controls & SCAN & r$^2$SCAN & R2OPT \\ \hline
    \multirow{6}{*}{X} & $a_{1\mathrm{x}}$ & Nonuniform scaling & \checkmark & \checkmark & \checkmark \\
    & $\kappa_1$ & $h\sux(\infty)$ & \checkmark & \checkmark & \checkmark \\
    & $c_{1\mathrm{x}}$ & $f\sux'(0)$ & \checkmark & \checkmark & \checkmark ($c_{0\mathrm{x}}$) \\
    & $c_{2\mathrm{x}}$ & shape of $f\sux(x>1)$ & \checkmark & \checkmark & \ding{55} \\
    & $\delta$ & $f\sux'(1)$ & \ding{55} & \ding{55} & \checkmark \\
    & $d\sux$ & $f\sux(\infty)$ & \checkmark & \checkmark & \checkmark \\ \hline
    \multirow{8}{*}{C} & $b_{1\mathrm{c}}$ & $E\suc$ 2$e^-$, $Z\to\infty$ ion & \checkmark & \checkmark & \checkmark \\
    & $b_{2\mathrm{c}}$ & $E\suc$ He atom & \checkmark & \checkmark & \checkmark \\
    & $b_{3\mathrm{c}}$ & 2$e^-$ Lieb-Oxford bound & \checkmark & \checkmark & \checkmark \\
    & $b_{4\mathrm{c}}$ & 1$e^-$ Lieb-Oxford bound & \checkmark & \checkmark & \checkmark \\
    & $c_{1\mathrm{c}}$ & $f\suc'(0)$ & \checkmark & \checkmark & \checkmark ($c_{0\mathrm{c}}$) \\
    & $c_{2\mathrm{c}}$ & shape of $f\suc(x>1)$ & \checkmark & \checkmark & \ding{55} \\
    & $c_{1\mathrm{c}}$ & $f\suc'(1)$ & \ding{55} & \ding{55} & \checkmark \\
    & $d\suc$ & $f\sux(\infty)$ & \checkmark & \checkmark & \checkmark \\ \hline
    \multirow{2}{*}{XC} & $\eta$ & $\ba$ regularization & \ding{55} & \checkmark & \checkmark \\
    & $d_{p2}$ & gradient expansion cutoff & \ding{55} & \checkmark & \checkmark \\ \hline
    & Total & & 12 & 14 & 14 \\ \hline
  \end{tabular}
  \caption{Comparison of the parameters fitted to appropriate norms in SCAN, r$^2$SCAN, and R2OPT.
  If a parameter is used, it is indicated with a \checkmark symbol, and a \ding{55} symbol if it is not.
  A parameter in parenthesis indicates that the parameter is used with a different name.
  The leftmost column indicates whether a parameter is used only in the exchange functional (X), the correlation functional (C), or is shared (XC).
  \label{tab:fpars}}
\end{table}

\subsection{Fitting R2OPT}

After trial and error, minimizing the objective function
\begin{align}
  \sigma &= \frac{1}{0.1583}\text{RMSPE(SA)} + \frac{1}{0.3352}\text{RMSPE(NSA)}
    + \frac{1}{5}\text{PE(LZ}~B\sux) + \frac{1}{5}\text{PE(LZ}~B\suc) \\
    &+ \frac{1}{3}\text{RMSPE(JS)} + \text{RMSPE(Ar}_2) \nonumber
\end{align}
appears to obtain balanced performance across the appropriate norms: the root-mean-squared (RMS) percentage errors (PE) in the
\begin{itemize}
  \item Spherical atoms (SA): Ne, Ar, Kr, Xe
  \item Non-spherical atoms (NSA): B, C, O, F
  \item Large-$Z$ coefficients $B\sux$ and $B\suc$ extrapolated from SA,
  \begin{align}
    E\sux &= -A\sux^\text{LDA} Z^{5/3} + B\sux Z +... \\
    E\suc &= -A\suc^\text{LDA} Z \ln Z + B\suc Z +...
  \end{align}
  \item Jellium surface formation energies for uniform in-plane densities $\overline{\rs}=2,3,4,6$
  \item Ar$_2$ atomization energies at three separations (1.6, 1.8, and 2 \AA{}) extrapolated from three increasingly dense GTO grids (from double to quadruple zeta, using SCAN densities).
\end{itemize}
Because the parameter space is very large, we minimize the coefficients within a neighborhood of the \rrscan values using a stochastic (non-deterministic) differential evolution method.
A stochastic method tends to be faster when the parameter space is large, and can avoid shallow local minima better than most deterministic methods (non-greedy search).

The bottleneck is the Ar$_2$ energy determination.
Removing Ar$_2$ from the objective function (and not evaluating it until a parameter set has been determined) decreases the time per run from about 1.5 hours to 10 minutes.

After two runs of the differential evolution method, the standard deviations of the parameters are used to define the bounding box for a deterministic Nelder-Mead simplex algorithm to refine the parameters.
The starting guess is the average values of the parameters.
Both the averages and standard deviations are weighted by the residuum errors $\sigma$.
Because we start the deterministic method so close to a minimum, it converges very rapidly.

The R2OPT parameters determined this way are
\begin{align}
  \eta &= 0.0007617150406229218 \\
  d_{p2} &= 0.2928748721008113 \\
  \kappa_1 &= 0.02267187541150674 \\
  c_{0\mathrm{x}} &= 0.23255698536393887 \\
  d\sux &= 0.9515270384726153 \\
  \delta &= 0.0287736922890905 \\
  c_{0\mathrm{c}} &= 0.14962422714920232 \\
  c_{1\mathrm{c}} &= -0.07112475601915894 \\
  d\suc &= 0.5191039248962888,
\end{align}
with norm errors RMSPE(SA) = 0.105\%, RMSPE(NSA) = 0.141\%, PE(LZ $B\sux$) = $(1.6\times 10^{-5})$\%, PE(LZ $B\suc$) = 4.385\%, RMSPE(JS) = 2.897\%, and RMSPE(Ar$_2$) = 0.535\%.
However, all that glitters is not gold.

\subsection{Tests of R2OPT in real systems}

As has been documented, SCAN has problems describing alkali metals, in addition to transition metals.
Thus we propose augmenting the LC20 set of solids with three more light alkalis: K, Cs, and Rb.
This LC23 set of solids provides a good benchmark of solid state performance.
Table \ref{tab:lc23} presents error statistics in the cubic lattice constants $a_0$, equilibrium bulk modiuli $B_0$, and cohesive energies $E_0$ for SCAN, \rrscan, and R2OPT, broken into subcategories.

\begin{table}
  \centering
  \begin{tabular}{lrrr} \hline
    MAE & SCAN & \rrscan & R2OPT \\ \hline
    $a_0$ (metals, \AA{}) & 0.051 & 0.066 & 0.063 \\
    $a_0$ (alk., \AA{}) & 0.095 & 0.114 & 0.121 \\
    $a_0$ (insul., \AA{}) & 0.024 & 0.026 & 0.027 \\
    $a_0$ (total, \AA{}) & 0.039 & 0.049 & 0.048 \\
    $a_0$ (LC20, \AA{}) & 0.024 & 0.029 & 0.027 \\ \hline
    $B_0$ (metals, GPa) & 4.3 & 3.6 & 10.2 \\
    $B_0$ (alk., GPa) & 0.5 & 0.4 & 0.4 \\
    $B_0$ (insul., GPa) & 3.9 & 3.4 & 5.0 \\
    $B_0$ (total, GPa) & 4.1 & 3.5 & 7.9 \\
    $B_0$ (LC20, GPa) & 4.7 & 4.0 & 9.0 \\ \hline
    $E_0$ (metals, eV/atom) & 0.15 & 0.13 & 0.29 \\
    $E_0$ (alk., eV/atom) & 0.08 & 0.09 & 0.08 \\
    $E_0$ (insul., eV/atom) & 0.07 & 0.08 & 0.15 \\
    $E_0$ (total, eV/atom) & 0.12 & 0.11 & 0.23 \\
    $E_0$ (LC20, eV/atom) & 0.12 & 0.11 & 0.24 \\ \hline
  \end{tabular}
  \caption{Mean absolute errors (MAE) for the LC23 set of solids for SCAN, \rrscan, and R2OPT.
  \label{tab:lc23}}
\end{table}

R2OPT matches \rrscan in accuracy for lattice constants, but is much worse for bulk moduli $B=V E''(V)$ and cohesive energies.
Thus the shape of the R2OPT binding energy curves is not nearly as right as SCAN or \rrscan.

For molecules, R2OPT performs between \rrscan and SCAN, as seen in Table \ref{tab:ae6} for the AE6 set.
The R2OPT errors are systematically towards overbinding.

\begin{table}
  \centering
  \begin{tabular}{lrrr} \hline
    & SCAN & \rrscan & R2OPT \\ \hline
    ME (kcal/mol) & 0.271 & 1.329 & 3.575 \\
    MAE (kcal/mol) & 4.320 & 2.870 & 3.575 \\ \hline
  \end{tabular}
  \caption{AE6 mean absolute errors (MAE).
  The R2OPT errors are systematic.
  \label{tab:ae6}}
\end{table}

\textcolor{red}{Recommendation: $\kappa_1$ is likely too small in this model.
Refit using fixed $\kappa_1$ in a wide range, see if it's possible to obtain similar norm accuracy for larger $\kappa_1$.
All other parameters are reasonably close to the \rrscan values.}

\section{The fourth-order expansion}

To recover the fourth-order gradient expansion for exchange, we can keep the correlation functional from before.
Fortunately, by fixing the values of $h\sux'(0)$, $h\sux''(0)$, $f\sux'(1)$, and $f\sux''(1)$, we remove two fit parameters from the optimized r$^2$SCAN model: $\delta$ [which controls $f\sux'(1)$] and $c_{0\mathrm{x}}$ [which controls $f\sux'(0)$].
This brings the total number of fit parameters, without further simplification, to six, the same number as in SCAN: $\eta$, $\kappa_1$, $d\sux$, $c_{0\mathrm{c}} = f\suc'(0)$, $c_{1\mathrm{c}}$, and $d\suc$.

The clearest generalization to the fourth-order expansion is to use the same interpolation for both $p$ and $p^2$ in $x(p)$,
\begin{equation}
  x(p) = \left\{\mu + \exp\left[- p^2/d_{p2}^4\right][h\sux'(0) - \mu]\right\} p
    + \left\{\frac{[h\sux'(0)]^2}{\kappa_1} + \frac{h\sux''(0)}{2}\exp\left[- p^2/d_{p2}^4\right]\right\} p^2
\end{equation}
which has asymptotic behavior $x(p)\sim \mu p + [h\sux'(0)p]^2/\kappa_1$.
This adds no new parameters to the R2OPT model.

An interesting possibility, which removes one fit parameter, is to fix $\kappa_1$ to recover the right $h\sux''(0)$ for the fourth-order expansion.
From Eq. \ref{eq:r2_rev_hx}
\begin{align}
  h\sux(p) &= 1 + h\sux'(0)p - \frac{[h\sux'(0)]^2}{\kappa_1}p^2 + ...
\end{align}
which would require $\kappa_1 = -[h\sux'(0)]^2/[2 h\sux''(0)]$, which when $\eta=0.001$, makes $\kappa_1 \approx 0.0117426$, much smaller than the SCAN value 0.065.
\emph{However,} the minimally-modified r$^2$SCAN model, which sought the fewest number of changes to r$^2$SCAN to yield the fourth-order expansion, yielded a similarly small $\kappa_1 \approx 0.013641$.
So we can't rule this out.

Note also that $\kappa_1$ increases with increasing $\eta$, consistent with the fourth-order gradient expansion constraint.
Using this, the maximum possible value of $\eta$ such that $\kappa_1 = 0.174$ is roughly 0.42005.

To recover the fourth-order gradient expansion, $h\sux'(0)<0$ and $h\sux''(0)<0$, making poles a practical concern.
We can extend Eq. \ref{eq:hx_rat_poly} to accommodate the higher-order gradient expansion
\begin{equation}
  h\sux(p) = \frac{1 + \mu p + \mu[\mu - h\sux'(0)]p^2 +  (1 + \kappa_1)p^3/d_{p2}^6}
  {1 + [\mu - h\sux'(0)]p + \{[\mu - h\sux'(0)]^2 - h\sux''(0)/2\} p^2 + p^3/d_{p2}^6}.
  \label{eq:hx_rat_poly_4}
\end{equation}
The coefficients of $p^2$ in the numerator and denominator are chosen to cancel erroneous terms in the series expansion of $h\sux(p)$ and restricted to be positive.
To see this, consider a generalized
\begin{align}
  h(p) &= \frac{1 + \mu p + b p^2 +  (1 + \kappa_1)p^3/d_{p2}^6}
  {1 + [\mu - h\sux'(0)]p + c p^2 + p^3/d_{p2}^6} \\
  &= [1 + \mu p + b p^2 +  (1 + \kappa_1)p^3/d_{p2}^6]\big\{ 1 - [\mu - h\sux'(0)]p + \{[\mu - h\sux'(0)]^2 - c\} p^2 + \mathcal{O}(\nabla^6)\big\} \\
  &= 1 + h\sux'(0)p + \{ b - \mu[\mu - h\sux'(0)] + [\mu - h\sux'(0)]^2 - c \}p^2 + \mathcal{O}(\nabla^6) \\
  &\implies b - \mu[\mu - h\sux'(0)] - \frac{h\sux''(0)}{2} +  [\mu - h\sux'(0)]^2 - c = 0.
\end{align}

\end{document}
