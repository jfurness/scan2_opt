import numpy as np
import matplotlib.pyplot as plt

scan_x_pars = {'C1':  0.667, 'C2': 0.8,'D': 1.24}
scan_c_pars = {'C1':  0.64, 'C2': 1.5,'D': .7}

def scan_ief_lower(x,sps):
    ief = np.exp(-sps['C1']*x/(1-x))
    dief = -ief*sps['C1']/(1-x)**2
    return ief,dief

def scan_ief_upper(x,sps):
    omx = 1-x
    ief = -sps['D']*np.exp(sps['C2']/omx)
    d1 = ief*sps['C2']/omx**2
    return ief,d1

def scan_interp(x,sps):
    ief = np.zeros(x.shape)
    dief = np.zeros(x.shape)
    lmsk = x < 1
    umsk = x > 1
    ief[lmsk],dief[lmsk] = scan_ief_lower(x[lmsk],sps)
    ief[umsk],dief[umsk] = scan_ief_upper(x[umsk],sps)
    return ief,dief

def rscan_interp(x,wief):
    if wief.lower() == 'x':
        cl = np.asarray([1.0,-0.667,-0.4445555,-0.663086601049, 1.451297044490, \
            -0.887998041597, 0.234528941479,-0.023185843322])
        sps = scan_x_pars
    elif wief.lower() == 'c':
        cl = np.asarray([1.0,-0.64,-0.4352,-1.535685604549,3.061560252175,\
            -1.915710236206,0.516884468372,-0.051848879792])
        sps = scan_c_pars

    ief = np.zeros(x.shape)
    dief = np.zeros(x.shape)
    lmask = x <= 2.5
    umask = x > 2.5

    xm = x[lmask]
    for i in range(8):
        ief[lmask] += cl[i]*xm**i
    for i in range(1,8):
        dief[lmask] += i*cl[i]*xm**(i-1)

    ief[umask],dief[umask] = scan_ief_upper(x[umask],sps)
    return ief,dief

def smooth_interp(x,c):
    ief_den = 1 + (c[0] + c[1])*x + (1 + c[1])*x**2/c[2]
    ief = (1 + c[1]*x - (1 + c[1])*x**2)/ief_den
    dief = (c[1] - 2*(1+c[1])*x - (c[0] + c[1] + 2*(1+c[1])*x/c[2])*ief)/ief_den
    return ief,dief

def get_pars_from_g1g2(g1,g2,eta,k0):

    fa = 100/243*(4 + 9*eta)*k0
    fb = 200/729 - 20*(1 + k0)*g1/9 + 2*k0/243 + 98*eta*k0/27
    fc = 73*(8 + 9*eta)/6075 - 73/405

    rcnd = fb**2 - 4*fa*fc
    if rcnd < 0:
        return 1e20,1e20,1e20,1e20
    rcnd = rcnd**(0.5)
    f1 = np.asarray([(-fb - rcnd)/(2*fa), (-fb + rcnd)/(2*fa)])
    f2 = 73/(2500*k0) - f1/50
    h1 = 10/81 - g1 + 5*(4 + 9*eta)*k0*f1/27
    h2 = -g2 - 2*(h1*g1 + 25*(8+9*eta)**2*k0*f2/1458 \
        + 5*(8 +9*eta)*(h1*f1 - k0*f1*g1)/27 + (6 + 75*eta*(8 + 9*eta))*k0*f1/243)

    if f1[0] <= 0:
        return f1[0],f2[0],h1[0],h2[0]
    elif f2[0] <= 0:
        return f1[1],f2[1],h1[1],h2[1]
    else:
        return 1e20,1e20,1e20,1e20

def task_interp(x):
    bnu = [-0.628591,-2.10315,-0.5,0.103153,0.128591]
    ief = np.zeros_like(x)
    dief = np.zeros_like(x)
    for i in range(5):
        f,df = cheb_rtnl_poly(x,i)
        ief += bnu[i]*f
        dief += bnu[i]*df
    return ief, dief

def cheb_rtnl_poly(x,n):
    """
        Chebyshev polynomial T_n(x) = cos(n * arccos(x))

        Rational Chebyshev polynomial R_n(x) = T_n((x-1)/(x+1))
    """
    if n == 0:
        ri = 1
        dri = 0.0
    elif n == 1:
        ri = (x - 1)/(x + 1)
        dri =2.0/(x + 1.0)**2
    elif n == 2:
        ri = (x**2 - 6*x + 1)/(x + 1)**2
        dri = 8*(x - 1.0)/(x + 1.0)**3
    elif n == 3:
        ri = (x**3 - 15*x**2 + 15*x - 1)/(x + 1)**3
        dri = 6*(3*x**2 - 10*x + 3.0)/(x + 1.0)**4
    elif n == 4:
        ri = (x**4 - 28*x**3 + 70*x**2 - 28*x + 1)/(x + 1)**4
        dri = 32*(x**3 - 7*x**2 + 7*x - 1.0)/(x + 1.0)**5
    else:
        arg = n*np.arccos((x - 1.0)/(x + 1.0))
        ri = np.cos(arg)
        dri = i*np.sin(arg)/( (x + 1.0)*max(1.e-12,x**(0.5)) )

    return ri,dri

def ratpoly(x,c):
    t = (1-x)/(1+x)
    dt = -2/(1+x)**2
    f = np.zeros_like(x)
    df = np.zeros_like(x)
    for i in range(len(c)):
        f += c[i]*t**(i+1)
        df += (i+1)*c[i]*t**i*dt
    return f,df

if __name__ == "__main__":

    """
    def kappa_from_eta(n):
        f1,f2,h1,h2 = get_pars_from_g1g2(0.0,0.0,n,0.174)
        return -2*h1**2/h2

    def obj(n):
        return 0.174 - kappa_from_eta(n)

    from scipy.optimize import bisect
    eta_max = bisect(obj,0.4,0.5,xtol=1.e-15)
    print(eta_max,kappa_from_eta(eta_max))
    eta_max_r = round(eta_max,5)
    print(eta_max_r,kappa_from_eta(eta_max_r))

    etal = np.linspace(0.001,eta_max,5000)
    kl = np.zeros_like(etal)
    for ieta,eta in enumerate(etal):
        kl[ieta] = kappa_from_eta(eta)

    fig,ax = plt.subplots(figsize=(8,6))
    ax.plot(etal,kl,linewidth=2,color='darkblue')
    ax.set_xlim([etal[0],etal[-1]])
    ax.set_ylim([kl[0],0.174])
    ax.tick_params('both',labelsize=16)
    ax.set_xlabel('$\eta$',fontsize=16)
    ax.set_ylabel("$[h_\\mathrm{x}'(0)]^2/h_\\mathrm{x}''(0)$",fontsize=16)
    plt.savefig('./kappa_eta.pdf',bbox_inches='tight',dpi=600)
    exit()
    """

    """
    f1,f2,h1,h2 = get_pars_from_g1g2(0.0,0.0,0.1218,0.174)
    print(f1,f2,h1,h2)
    print(-2*h1**2/h2)
    exit()
    """

    #"""
    x = np.linspace(0.0,5.0,2000)

    for w in ['x','c']:

        if w == 'x':
            scanps = scan_x_pars
        elif w == 'c':
            scanps = scan_c_pars

        new_pars = [scanps['C1'],(scanps['C2']-scanps['D']*scanps['C1'])/(1 + scanps['D'] - scanps['C2']),scanps['D']]
        #print(new_pars[1])
        ief,dief = smooth_interp(x,new_pars)

        #tmp = np.array([1.e6])
        #print(smooth_interp(tmp,new_pars),scan_interp(tmp,scanps),rscan_interp(tmp,w))

        sief,sdief = scan_interp(x,scanps)
        rief,rdief = rscan_interp(x,w)
        #print(dief[0],sdief[0],rdief[0])
        #print(dief[-1],sdief[-1],rdief[-1])

        fig,ax = plt.subplots(figsize=(8,6))
        ax.plot(x,ief,label='SMTH',linewidth=2,color='darkblue')
        ax.plot(x,sief,label='SCAN',linewidth=2,color='darkorange',linestyle='--')
        ax.plot(x,rief,label='rSCAN',linewidth=2,color='darkgreen',linestyle='-.')
        ax.set_xlim([x[0],x[-1]])
        plt.hlines(0.0,x[0],x[-1],color='gray',linewidth=2)
        ax.legend(fontsize=16)
        ax.tick_params('both',labelsize=16)
        ax.set_xlabel('$x$',fontsize=16)
        ax.set_ylabel('$f_{\\mathrm{'+w+ '} }(x)$',fontsize=16)
        plt.savefig('./f'+w+ '_comp_interp.pdf',bbox_inches='tight',dpi=600)

        plt.close()
        fig,ax = plt.subplots(figsize=(8,6))

        ax.plot(x,dief,label='SMTH',linewidth=2,color='darkblue')
        ax.plot(x,sdief,label='SCAN',linewidth=2,color='darkorange',linestyle='--')
        ax.plot(x,rdief,label='rSCAN',linewidth=2,color='darkgreen',linestyle='-.')
        ax.set_xlim([x[0],x[-1]])
        plt.hlines(0.0,x[0],x[-1],color='gray',linewidth=2)
        ax.legend(fontsize=16)
        ax.tick_params('both',labelsize=16)
        ax.set_xlabel('$x$',fontsize=16)
        ax.set_ylabel('$d f_{\\mathrm{'+w+ "} }/dx$",fontsize=16)
        plt.savefig('./f'+w+ '_comp_interp_deriv.pdf',bbox_inches='tight',dpi=600)

        plt.close()
    exit()
    #"""

    x = np.linspace(0.0,5.0,2000)
    fig,ax = plt.subplots(figsize=(8,6))

    f1,f2,_,_ = get_pars_from_g1g2(0.0,0.0,0.0,0.174)
    #print(f1,f2)

    d = 3
    cf0 = (2*d + 4*(1 + d)*f1 + 2*(1 + d)*f1**2 + (1 + d)*f2)/(d*f2 - 2*f1**2)
    cf1 = -(2*d + (1 + d + cf0*d)*f1)/(d + (1 + d)*f1)
    print(cf0,cf1)
    ief,dief = smooth_interp(x,[cf0,cf1,d])
    tief,tdief = task_interp(x)

    c4 = (1 - d)/2 - 2*(f1 + f2)
    c3 = d + 2*(f2 + 2*f1) + c4
    fjpp,dfjpp = ratpoly(x,[-2*f1, 2*(f2 + f1), c3,c4])
    """
    ax.plot(x,np.log10(np.abs(fjpp-tief)))
    ax.set_xlim([x[0],x[-1]])
    ax.set_ylim([-9,-5])
    ax.tick_params('both',labelsize=16)
    ax.set_xlabel('$x$',fontsize=16)
    ax.set_ylabel('$\\log_{10}|f^\\mathrm{JPP}(x) - f^\\mathrm{TASK}(x)|$',fontsize=16)
    #plt.show() ; exit()
    plt.savefig('./f_jpp_task.pdf',bbox_inches='tight',dpi=600)
    exit()
    """

    ax.plot(x,ief,label='$f^\mathrm{SMTH}_{\\mathrm{x}}$',linewidth=2,color='darkblue')
    ax.plot(x,tief,label='$f^\mathrm{TASK}_{\\mathrm{x}}$',linewidth=2,color='darkorange')
    #ax.plot(x,fjpp,label='$f^\mathrm{JPP}_{\\mathrm{x}}$',linewidth=2,color='darkgreen')
    ax.plot(x,dief,label='$df^\mathrm{SMTH}_{\\mathrm{x}}/dx$',linewidth=2,color='darkblue',linestyle='-.')
    ax.plot(x,tdief,label='$df^\mathrm{SMTH}_{\\mathrm{x}}/dx$',linewidth=2,color='darkorange',linestyle='-.')
    #ax.plot(x,dfjpp,label='$f^\mathrm{JPP}_{\\mathrm{x}}$',linewidth=2,color='darkgreen',linestyle='-.')

    ax.set_xlim([x[0],x[-1]])
    plt.hlines(0.0,x[0],x[-1],color='gray',linewidth=2)
    ax.legend(fontsize=16,ncol=2)
    ax.tick_params('both',labelsize=16)
    ax.set_xlabel('$x$',fontsize=16)
    #ax.set_ylabel('$d f_{\\mathrm{'+w+ "} }/dx$",fontsize=16)
    #plt.show()
    plt.savefig('./f_comp_task.pdf',bbox_inches='tight',dpi=600)
