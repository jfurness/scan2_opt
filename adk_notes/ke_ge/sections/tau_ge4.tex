\section{Modified kinetic energy density-gradient expansion \label{sec:ke_ge4}}

From M. Brack, B.K. Jennings, and Y. Chu \cite{brack1976}, the spin-unpolarized kinetic energy density has a density gradient expansion
\begin{align}
  \tau &= \frac{3}{10}\kf^2 n + \frac{1}{6}\lan + \frac{1}{72}\frac{|\gn|^2}{n} + \frac{1}{4320 (3\pi^2)^{2/3}}\left[ 12 \frac{\nn(\lan)}{n^{2/3}} - 30\frac{\gn \cdot \n(\lan)}{n^{5/3}} - 14\frac{(\lan)^2}{n^{5/3}} \right. \nonumber \\
  & \left. - 7\frac{\nn|\gn|^2}{n^{5/3}} + \frac{140}{3}\frac{|\gn|^2\lan}{n^{8/3}} + \frac{92}{3}\frac{\gn \cdot \n|\gn|^2}{n^{8/3}} - 48\frac{|\gn|^4}{n^{11/3}} \right]
  \label{eq:tau_ge_o4_unsimp}
\end{align}
where $\kf = (3\pi^2 n)^{1/3}$.
This expression has limited applicability in standard codes, where $n$, $\gn$, and (sometimes) $\lan$ are calculated, but rarely are the derivatives of $\gn$ and $\lan$.
Thus we will seek a modified gradient expansion that integrates to the same kinetic energy, but is solely in terms of the variables
\begin{align}
  \tau_0 &= \frac{3}{10}\kf^2 n \\
  p &= \left[\frac{\gn}{2\kf n} \right]^2 = \frac{3}{40}\frac{|\gn|^2}{\tau_0 n} \\
  q &= \frac{\lan}{4\kf^2 n} = \frac{3}{40} \frac{\lan}{\tau_0}.
\end{align}
To do this, we will make use of the divergence theorem,
\begin{equation}
  \int_{\Omega} \n \cdot \bm{f} d^3 r = \int_{\text{bdy}\, \Omega} \bm{f} \cdot d\bm{S},
\end{equation}
where $\Omega$ is a volume and $\text{bdy}\, \Omega$ its bounding surface.
Provided that $\bm{f}$ vanishes at infinity sufficiently quickly, the integral of $\n \cdot \bm{f}$ vanishes.

The third, fifth, and seventh term in brackets do not need any substantive work to be expressed in terms of $p$ and $q$.
All others do; since no term is special, we will move left to right.
We will also make extensive use of the identity
\begin{equation}
  \n \cdot (f \bm{g}) = \n f \cdot \bm{g} + f \n \cdot \bm{g}.
\end{equation}

\vspace{5mm}
First term:
\begin{align}
  n^{-2/3}\nn(\lan) &= n^{-2/3} \n \cdot \n(\lan) \\
  &= \n \cdot [n^{-2/3} \n(\lan)] - \n(\lan) \cdot \n(n^{-2/3}) \\
  &= \n \cdot [n^{-2/3} \n(\lan)] + \frac{2}{3} n^{-5/3} \n(\lan) \cdot \gn \\
  &= \n \cdot \left[ n^{-2/3} \n(\lan)+ \frac{2}{3} n^{-5/3} \lan \gn \right] - \frac{2}{3}\lan ~\gn \cdot \n(n^{-5/3}) - \frac{2}{3}n^{-5/3}(\lan)^2 \\
  &= \n \cdot \left[ n^{-2/3} \n(\lan)+ \frac{2}{3} n^{-5/3} \lan \gn \right] + \frac{10}{9}\frac{\lan |\gn|^2}{n^{8/3}} -\frac{2}{3}\frac{(\lan)^2}{n^{5/3}}
\end{align}

\vspace{5mm}
Second term:
\begin{align}
  n^{-5/3} \gn \cdot \n(\lan) &= n^{-5/3}\n \cdot [\gn \lan ] - n^{-5/3} (\lan)^2 \\
  &= \n \cdot[ n^{-5/3} \gn \lan] - \lan \gn \cdot \n(n^{-5/3}) - n^{-5/3} (\lan)^2 \\
  &= \n \cdot[ n^{-5/3} \gn \lan] + \frac{5}{3} \frac{\lan |\gn|^2}{n^{8/3}} - \frac{(\lan)^2}{n^{5/3}}.
\end{align}

\vspace{5mm}
Fourth term:
\begin{align}
  n^{-5/3} \nn |\gn|^2 &= \n \cdot [ n^{-5/3} \n |\gn|^2] - \n |\gn|^2 \cdot \n(n^{-5/3}) \\
  &= \n \cdot [ n^{-5/3} \n |\gn|^2] + \frac{5}{3} n^{-8/3} \n |\gn|^2 \cdot \gn \\
  &= \n \cdot \left[ n^{-5/3} \n |\gn|^2 + \frac{5}{3} n^{-8/3} \gn |\gn|^2 \right] - \frac{5}{3}n^{-8/3} \lan |\gn|^2 - \frac{5}{3}|\gn|^2 \gn \cdot \n(n^{-8/3}) \\
  &= \n \cdot \left[ n^{-5/3} \n |\gn|^2 + \frac{5}{3} n^{-8/3} \gn |\gn|^2 \right] - \frac{5}{3} \frac{\lan |\gn|^2}{n^{8/3}} + \frac{40}{9}\frac{|\gn|^4}{n^{11/3}}.
\end{align}

\vspace{5mm}
Sixth term:
\begin{align}
  n^{-8/3}\gn \cdot \n|\gn|^2 &= \n \cdot[n^{-8/3}\gn |\gn|^2 ] - n^{-8/3}|\gn|^2\lan - |\gn|^2 \gn \cdot \n(n^{-8/3}) \\
  &= \n \cdot[n^{-8/3}\gn |\gn|^2 ] - \frac{|\gn|^2\lan}{n^{8/3}} + \frac{8}{3}\frac{|\gn|^4}{n^{11/3}}.
\end{align}

\clearpage

We can now put these terms together; focusing only on the square bracketed term,
\begin{align}
  [...] &= \underbrace{ 12 \n \cdot \left[ n^{-2/3} \n(\lan)+ \frac{2}{3} n^{-5/3} \lan \gn \right] + \frac{40}{3}\frac{\lan |\gn|^2}{n^{8/3}} -8\frac{(\lan)^2}{n^{5/3}} }_{1^{\text{st}} ~\text{term}} \nonumber \\
  & - \underbrace{ 30 \n \cdot[ n^{-5/3} \gn \lan] - 50 \frac{\lan |\gn|^2}{n^{8/3}} + 30\frac{(\lan)^2}{n^{5/3}}}_{2^{\text{nd}} ~\text{term}} - \underbrace{14\frac{(\lan)^2}{n^{5/3}} }_{3^{\text{rd}} ~\text{term}} \nonumber \\
  & - \underbrace{7\n \cdot \left[ n^{-5/3} \n |\gn|^2 + \frac{5}{3} n^{-8/3} \gn |\gn|^2 \right] + \frac{35}{3} \frac{\lan |\gn|^2}{n^{8/3}} - \frac{280}{9}\frac{|\gn|^4}{n^{11/3}}}_{4^{\text{th}} ~\text{term}} + \underbrace{\frac{140}{3}\frac{|\gn|^2\lan}{n^{8/3}}}_{5^{\text{th}} ~\text{term}} \nonumber \\
  & + \underbrace{ \frac{92}{3}\n \cdot[n^{-8/3}\gn |\gn|^2 ] - \frac{92}{3}\frac{|\gn|^2\lan}{n^{8/3}} + \frac{736}{9}\frac{|\gn|^4}{n^{11/3}}}_{6^{\text{th}} ~\text{term}} - \underbrace{48\frac{|\gn|^4}{n^{11/3}}}_{7^{\text{th}} ~\text{term}} \\
  & \nonumber \\
  &= -9\frac{\lan |\gn|^2}{n^{8/3}} +8  \frac{(\lan)^2}{n^{5/3}} + \frac{8}{3}\frac{|\gn|^4}{n^{11/3}} \nonumber \\
  & + \n \cdot\left[12 \frac{\n(\lan)}{n^{2/3}} -22 \frac{\lan \gn}{n^{5/3}} -7 \frac{\n |\gn|^2}{n^{5/3}} + 19 \frac{\gn |\gn|^2}{n^{8/3}} \right].
\end{align}
Then
\begin{align}
  \tau &= \frac{3}{10}\kf^2 n + \frac{1}{6}\lan + \frac{1}{72}\frac{|\gn|^2}{n} + \frac{1}{4320 (3\pi^2)^{2/3}}\left\{ -9\frac{\lan |\gn|^2}{n^{8/3}} +8  \frac{(\lan)^2}{n^{5/3}} + \frac{8}{3}\frac{|\gn|^4}{n^{11/3}} \right. \nonumber \\
  & \left. +\n \cdot\left[12 \frac{\n(\lan)}{n^{2/3}} -22 \frac{\lan \gn}{n^{5/3}} -7 \frac{\n |\gn|^2}{n^{5/3}} + 19 \frac{\gn |\gn|^2}{n^{8/3}}\right] \right\} \\
  & \nonumber \\
  \frac{\tau}{\tau_0} &= 1 + \frac{20}{9} q + \frac{5}{27} p + \frac{1}{1296}\left\{ -9\frac{\lan |\gn|^2}{\kf^4 n^2} + 8 \frac{(\lan)^2}{\kf^2 n^2} + \frac{8}{3}\frac{|\gn|^4}{\kf^2 n^2} \right\} \nonumber \\
  & + \frac{1}{4320(3\pi^2)^{2/3}\tau_0}\n \cdot\left[12 \frac{\n(\lan)}{n^{2/3}} -22 \frac{\lan \gn}{n^{5/3}} -7 \frac{\n |\gn|^2}{n^{5/3}} + 19 \frac{\gn |\gn|^2}{n^{8/3}}\right] \\
  & \nonumber \\
  \frac{\tau}{\tau_0} &= 1 + \frac{20}{9} q + \frac{5}{27} p + \frac{8}{81} q^2 - \frac{1}{9} p q + \frac{8}{243} p^2  \nonumber \\
  & + \frac{1}{4320(3\pi^2)^{2/3}\tau_0}\n \cdot\left[12 \frac{\n(\lan)}{n^{2/3}} -22 \frac{\lan \gn}{n^{5/3}} -7 \frac{\n |\gn|^2}{n^{5/3}} + 19 \frac{\gn |\gn|^2}{n^{8/3}}\right]
    \label{eq:tau_ge_o4}.
\end{align}
For the purposes of the second half of this note, we will define
\begin{align}
  \tau_2 &= \tau_0 \left[\frac{20}{9} q + \frac{5}{27} p \right] \label{eq:tau_2} \\
  \tau_4 &= \tau_0 \left[ \frac{8}{81} q^2 - \frac{1}{9} p q + \frac{8}{243} p^2 \right] \label{eq:tau_4} \\
  \widetilde{\tau}_4 &= \frac{1}{4320(3\pi^2)^{2/3}}\n \cdot\left[12 \frac{\n(\lan)}{n^{2/3}} -22 \frac{\lan \gn}{n^{5/3}} -7 \frac{\n |\gn|^2}{n^{5/3}} + 19 \frac{\gn |\gn|^2}{n^{8/3}}\right],
    \label{eq:tau_4_div}
\end{align}
so that $\tau = \tau_0 + \tau_2 + \tau_4 + \widetilde{\tau}_4 + \mathcal{O}(|\gn|^6)$.

Perdew and Constantin \cite{perdew2007} also derived an expression for $\tau$ in the slowly-varying limit. Their work assumes that the term $\widetilde{\tau}_4$ vanishes under integration of $\tau$, however their Eqs. 7 and 8 provide an identical cut-off gradient expansion for $\tau$ as Eqs. \ref{eq:tau_ge_o4}--\ref{eq:tau_4_div},
\begin{equation}
  \tau^{\text{PC}}(n,p,q) = \tau_0\left[1 + \frac{5}{27}p + \frac{20}{9}q + \frac{8}{81} q^2 - \frac{1}{9} p q + \frac{8}{243} p^2 \right] + \mathcal{O}(|\gn|^6).
\end{equation}
It's worth noting that the term linear in $q$ would also vanish under direct integration of $\tau$.


\subsection{Spin resolution}

For the correlation energy, we need to spin-resolve the kinetic energy density for arbitrary spin-polarization $\nup$ and $\ndn$, such that $n = \nup + \ndn$ and $\zeta = (\nup - \ndn)/n$.
The kinetic energy density can be spin-resolved using the local spin-scaling relationship \cite{oliver1979}
\begin{equation}
  \tau(\nup,\ndn) = \frac{1}{2}[\tau(2\nup) + \tau(2\ndn)].
\end{equation}
Because the correlation energy gradient expansion is known only to second order, we will ignore the fourth-order gradient expansion terms in $\tau$ in this section.
It is also most convenient to restart from Eq. \ref{eq:tau_ge_o4_unsimp} for each spin channel, rather than Eq. \ref{eq:tau_ge_o4}.

As
\begin{align}
  2\nup &= n(1+\zeta) \\
  2\ndn &= n(1-\zeta),
\end{align}
we find that
\begin{align}
  \n [ n(1\pm \zeta)] &= (1\pm \zeta)\n n \pm n \n \zeta \\
  |\n [ n(1\pm \zeta)]|^2 &= (1 \pm \zeta)^2 |\gn|^2
    \pm 2(1 \pm \zeta)n \n \zeta \cdot \n n + n^2 |\n \zeta|^2 \\
  \n^2 [n(1 \pm \zeta)] &= (1 \pm \zeta)\lan \pm 2 \gn \cdot \n \zeta \pm n \n^2 \zeta.
\end{align}
Then Eq. \ref{eq:tau_ge_o4_unsimp} can be written, up to fourth-order, for each spin channel as
\begin{align}
  \tau(2\nup) &= \frac{3}{10}\kf^2 n (1 + \zeta)^{5/3}
    + \frac{1}{6}\left[(1 + \zeta)\lan + 2 \gn \cdot \n \zeta + n \n^2 \zeta \right]\\
    &+ \frac{1}{72}\left[(1 + \zeta) \frac{|\gn|^2}{n} + 2\gn \cdot \n \zeta
    + \frac{n}{1+\zeta} |\n \zeta|^2 \right] + \mathcal{O}(|\gn|^4), \nonumber \\
  \tau(2\ndn) &= \frac{3}{10}\kf^2 n (1 - \zeta)^{5/3}
    + \frac{1}{6}\left[(1 - \zeta)\lan - 2 \gn \cdot \n \zeta - n \n^2 \zeta \right]\\
    &+ \frac{1}{72}\left[(1 - \zeta) \frac{|\gn|^2}{n} - 2\gn \cdot \n \zeta
    + \frac{n}{1-\zeta} |\n \zeta|^2 \right] + \mathcal{O}(|\gn|^4). \nonumber
\end{align}
Then
\begin{align}
  \tau(\nup,\ndn) &= \frac{3}{10}\kf^2 n d\sus(\zeta) + \frac{1}{6}\lan
    + \frac{1}{72} \frac{|\gn|^2}{n} + \frac{1}{72} \frac{n |\n \zeta|^2}{1-\zeta^2}
    + \mathcal{O}(|\gn|^4) \label{eq:tau_ge_o2_sr} \\
  &= \tau_0(n)d\sus(\zeta)\left[1 + \frac{20}{9d\sus(\zeta)}q + \frac{5}{27d\sus(\zeta)} p
    + \frac{5}{27d\sus(\zeta)(1-\zeta^2)}\sigma^2 \right] + \mathcal{O}(|\gn|^4) \\
  d\sus(\zeta) &= \frac{1}{2}\left[(1 + \zeta)^{5/3} + (1 - \zeta)^{5/3} \right] \\
  \bm{\sigma} &= \frac{\n \zeta}{2 \kf}.
\end{align}
