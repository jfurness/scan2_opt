
Let's start from a generalized r$^2$SCAN-like model, and see how many changes would be needed to recover the fourth-order gradient expansion for exchange.

\subsection{Gradient expansion and spin resolution of $\ba$}

The r$^2$SCAN iso-orbital indicator (assuming that the parameters still have to be determined) is $\ba$,
\begin{equation}
  \ba = \frac{\tau - \tau_W}{\tau_0 + \eta \tau_W}, \qquad \tau_W = \frac{5}{3}\tau_0~p.
\end{equation}
We will make use of the Taylor expansion
\begin{equation}
  (1 + a x)^b = 1 + a b x + \frac{b(b-1)a^2}{2}x^2 + \mathcal{O}(x^3)
\end{equation}
to derive the gradient expansion of $\ba$.
When $b=-1$, $(1 + a x)^{-1} = 1 - a x + a^2 x^2 + \mathcal{O}(x^3)$.
In the slowly-varying limit, $\ba$ tends to
\begin{align}
  \ba &\to \frac{\tau_0 + \tau_2 + \tau_4 + \widetilde{\tau}_4 - \frac{5}{3}\tau_0~p}{\tau_0 + \frac{5}{3}\eta\tau_0~p} \\
  \ba & \to \left[1 + \frac{20}{9} q - \frac{40}{27} p + \tau_4 + \widetilde{\tau}_4 + \mathcal{O}{|\gn|^6} \right]\left[ 1 + \frac{5\eta}{3}p\right]^{-1} \\
  \ba & \to \left[1 + \frac{20}{9} q - \frac{40}{27} p + \tau_4 + \widetilde{\tau}_4 + \mathcal{O}{|\gn|^6} \right]\left[ 1 - \frac{5\eta}{3}p + \frac{25\eta^2}{9}p^2 + \mathcal{O}(|\gn|^6)\right] \\
  \ba & \to 1 + \frac{20}{9}q - \frac{5(8 + 9\eta)}{27}p + \frac{8}{81} q^2 - \frac{3 + 100\eta}{27} p q + \frac{8 + 75 \eta (8 + 9 \eta)}{243} p^2 + \frac{\widetilde{\tau}_4}{\tau_0} + \mathcal{O}(|\gn|^6).
  \label{eq:ba_ge_o4}
\end{align}
Why are we still retaining $\widetilde{\tau}_4$?
We anticipate needing to compute $(1-\ba)n(\br)\varepsilon^{\text{UEG}}\sx$, with $\varepsilon^{\text{UEG}}\sx=-3\kf/(4\pi)$, and needing to perform a successive integration by parts.
However, only the second-order terms in the gradient expansion of $\ba$ appear in the fourth-order expansions of
\begin{align}
  (\ba -1)^2 &= \frac{400}{81}q^2 - \frac{200 (8 + 9\eta)}{243}p q
    + \frac{25(8 + 9\eta)^2}{729}p^2 + \mathcal{O}(|\gn|^6) \label{eq:ge_ba_m_1_sq} \\
  (\ba-1)p &= \frac{20}{9} p q - \frac{5(8 + 9\eta)}{27}p^2 + \mathcal{O}(|\gn|^6). \label{eq:ge_ba_m_1_p}
\end{align}
These quantities will be needed to compute the gradient expansion of the exchange enhancement factor.

Note that, to second order, we can approximately invert Eq. \ref{eq:ba_ge_o4} to express $q$ in terms of $p$ and $\ba$ as
\begin{equation}
  q \approx \frac{9}{20}(\ba - 1) + \frac{8+9\eta}{12}p
    = \frac{9}{20}(\ba - 1) + \left(\frac{2}{3} + \frac{3}{4}\eta \right)p.
\end{equation}

To obtain spin resolution of $\ba$ from Eq. \ref{eq:tau_ge_o2_sr} for $\tau(\nup,\ndn)$, we either need to resolve each kinetic energy density in $\ba$, or make the choice in SCAN \cite{sun2015} and \rrscan \cite{furness2020}, which does not spin-resolve $\tau_W$.
For the latter choice,
\begin{align}
  \ba(\nup,\ndn) &= \frac{\tau(\nup,\ndn) - \tau_W(n)}{\tau_0(n)d\sus(\zeta) + \eta \tau_W(n)} \\
  &= \left[ 1 + \frac{20}{9d\sus(\zeta)}q - \frac{40}{27d\sus(\zeta)} p
    + \frac{5}{27d\sus(\zeta)(1-\zeta^2)}\sigma^2 \right]
    \cdot \left[1 + \frac{5\eta}{3d\sus(\zeta)}p \right]^{-1} + \mathcal{O}(|\gn|^4) \\
  &= 1 + \frac{20}{9d\sus(\zeta)} q - \frac{5(8 + 9\eta)}{27d\sus(\zeta)}p
    + \frac{5}{27d\sus(\zeta)(1-\zeta^2)}\sigma^2 + \mathcal{O}(|\gn|^4).
\end{align}
In the former choice, suggested by Mej\'ia-Rodr\'iguez and Trickey \cite{mejia2017},
\begin{align}
  \tau_W(\nup,\ndn) = \tau_0(n)\left[\frac{5}{3}p + \frac{5}{3(1-\zeta^2)}\sigma^2 \right]
\end{align}
\begin{align}
  \ba_\text{FSR}(\nup,\ndn) &= \frac{\tau(\nup,\ndn) - \tau_W(\nup,\ndn)}{\tau_0(n)d\sus(\zeta) + \eta \tau_W(\nup,\ndn)} \\
  &= 1 + \frac{20}{9d\sus(\zeta)} q - \frac{5(8 + 9\eta)}{27d\sus(\zeta)}p
    - \frac{40}{27d\sus(\zeta)(1-\zeta^2)}\sigma^2 + \mathcal{O}(|\gn|^4),
\end{align}
where ``FSR'' stands for ``full spin resolution.''

\subsection{$\ba$-dependent exchange enhancement factor}

The overall enhancement factor of this new meta-GGA will be similar to r$^2$SCAN,
\begin{equation}
  F\sx(p,\ba) = \{h\sx(p) + f\sx(\ba)[h\sx^0 - h\sx(p)]\}g\sx(p).
\end{equation}
$h\sx^0=1 + \kappa_0$, with $\kappa_0 = 0.174$, is a constant.
We have assumed that $h\sx$ has no $\ba$-dependence, as in r$^2$SCAN but not SCAN,
\begin{equation}
  h\sx(p\to 0) = 1 + h_1 p + \frac{h_2}{2} p^2 + \mathcal{O}(|\gn|^6),
\end{equation}
where $h_1 \equiv h\sx'(0)$ and $h_2 \equiv h\sx''(0)$ for ease of writing.
Analogously,
\begin{equation}
  g\sx(p\to0) = 1 + g_1 p + \frac{g_2}{2} p^2 + \mathcal{O}(|\gn|^6),
\end{equation}
with $g_1 \equiv g\sx'(0)$ and $g_2 \equiv g\sx''(0)$.
Note that, in SCAN and r$^2$SCAN, $g\sx(p\to 0)\to 1 + \mathcal{O}(|\gn|^{\infty})$, because every term in the Taylor series of $g\sx(p)$ beyond order $p^0$ is zero.
In this work, we will pursue a few alternative constructions, including those with an analytic $g\sx(p)$ that may have a low-order Taylor series in $p$, like the Made Very Simple (MVS) meta-GGA.
Suppose the interpolation function $f\sx$ satisfies $f\sx(\ba=1)=0$, and has a Taylor series
\begin{equation}
  f\sx(\ba\to 1) = f_1(\ba-1) + \frac{f_2}{2}(\ba-1)^2 + \mathcal{O}(|\gn|^6),
\end{equation}
where $f_1 \equiv f\sx'(1)$ and $f_2 \equiv f\sx''(1)$.

\iffalse
Inserting the gradient expansion of $\ba$ into $f\sx$,
\begin{align}
  f\sx(\ba) &\to \frac{20}{9}f\sx'(1) q - \frac{5}{27}f\sx'(1)(8 + 9\eta)p + \frac{8}{81}[f\sx'(1) + 25 f\sx''(1)] q^2 \nonumber \\
  & - \frac{1}{27}\left[(3  + 100\eta)f\sx'(1) + \frac{100}{9}(8 + 9\eta)f\sx''(1) \right]p q  \nonumber \\
  & + \frac{1}{243}\left\{[8 + 75(8 + 9\eta)] f\sx'(1) + \frac{25}{6}(8 + 9\eta)^2 f\sx''(1)\right\} p^2 +\frac{\widetilde{\tau}_4}{\tau_0} + \mathcal{O}(|\gn|^6).
\end{align}
\fi

Before we insert the gradient expansion of $\ba$, let's write the Taylor series of $F\sx$ in $p$ and $\ba-1$,
\begin{align}
  F\sx &= \left\{1 + h_1 p + \frac{h_2}{2} p^2
    + \left[f_1(\ba-1) + \frac{f_2}{2}(\ba-1)^2\right]
    \left[\kappa_0 - h_1 p - \frac{h_2}{2} p^2 \right] \right\} \\
    & \times \left\{ 1 + g_1 p + \frac{g_2}{2} p^2 \right\} + \mathcal{O}(|\gn|^6) \nonumber \\
  F\sx &= 1 + (h_1 + g_1)p + \kappa_0 f_1(\ba - 1)
    + \left(\frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1 \right)p^2
    + [\kappa_0 f_1 g_1 - h_1 f_1]p(\ba - 1) \label{eq:fx_taylor} \\
    & + \frac{\kappa_0 f_2}{2}(\ba-1)^2 +\mathcal{O}(|\gn|^6). \nonumber
\end{align}
Now insert Eq. \ref{eq:ge_ba_m_1_sq} for the gradient expansion of $(\ba - 1)^2$ and Eq. \ref{eq:ge_ba_m_1_p} for the gradient expansion of $(\ba-1)p$,
\begin{align}
  F\sx &= 1 + (h_1 + g_1)p + \kappa_0 f_1(\ba - 1) \\
    & + \left[\frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1
    + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
    - \frac{5(8+9\eta)}{27}(\kappa_0 f_1 g_1 - h_1 f_1) \right] p^2 \nonumber \\
    & - \left[ \frac{20}{9}h_1 f_1 - \frac{20}{9}\kappa_0 f_1 g_1
    + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2 \right] p q
    + \frac{200\kappa_0}{81}f_2 q^2 +\mathcal{O}(|\gn|^6). \nonumber
\end{align}
Finally, let's insert the fourth-order gradient expansion of $\ba-1$,
\begin{align}
  F\sx &= 1 + \left[ h_1 + g_1 - \frac{5(8+9\eta)\kappa_0}{27}f_1 \right]p
    + \frac{20}{9}\kappa_0 f_1 q \\
    & + \left[\frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1
    + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
    + \frac{5(8+9\eta)}{27}( h_1 f_1 - \kappa_0 f_1 g_1)
    + \frac{8 + 75 \eta (8 + 9 \eta)}{243}\kappa_0 f_1 \right] p^2 \nonumber \\
    & - \left[ \frac{20}{9}h_1 f_1 - \frac{20}{9}\kappa_0 f_1 g_1
    + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2
    + \frac{3+100\eta}{27} \kappa_0 f_1 \right] p q
    + \frac{8\kappa_0}{81}\left[f_1 + 25f_2 \right] q^2 \nonumber \\
    & + \kappa_0 f_1 \frac{\widetilde{\tau}_4}{\tau_0} +\mathcal{O}(|\gn|^6). \nonumber
\end{align}

\iffalse
Then the gradient expansion of $F\sx$ will be
\begin{align}
  F\sx & \to 1 + h\sx'(0) p + \frac{1}{2}h\sx''(0) p^2 + \left\{\frac{20}{9}f\sx'(1) q - \frac{5}{27}f\sx'(1)(8 + 9\eta)p + \frac{8}{81}[f\sx'(1) + 25 f\sx''(1)] q^2 \right. \nonumber \\
  & - \frac{1}{27}\left[(3  + 100\eta)f\sx'(1) + \frac{100}{9}(8 + 9\eta)f\sx''(1) \right]p q  \nonumber \\
  & \left. + \frac{1}{243}\left[[8 + 75(8 + 9\eta)] f\sx'(1) + \frac{25}{6}(8 + 9\eta)^2 f\sx''(1)\right] p^2 +\frac{\widetilde{\tau}_4}{\tau_0}\right\}[h\sx^0 - 1 - h\sx'(0) p - \frac{1}{2}h\sx''(0) p^2] \nonumber \\
  & + \mathcal{O}(|\gn|^6) \\
  F\sx & \to 1 + \frac{20\kappa_0}{9}f\sx'(1) q + \left[h\sx'(0) - \frac{5(8 + 9\eta)}{27}\kappa_0 f\sx'(1) \right]p + \kappa_0 \frac{8}{81} \left[ f\sx'(1) + 25 f\sx''(1) \right]q^2 \nonumber \\
  & - \left[\kappa_0 f\sx'(1)\frac{3+100\eta}{27} + \frac{20}{9}h\sx'(0)f\sx'(1) + \kappa_0 f\sx''(1)\frac{100(8+9\eta)}{243} \right]p q \nonumber \\
  & + \left[\frac{h\sx''(0)}{2} +\kappa_0 f\sx'(1)\frac{8 + 75 \eta (8 + 9 \eta)}{243} + f\sx'(1)h\sx'(0)\frac{5(8 + 9\eta)}{27} + \kappa_0 f\sx''(1)\frac{25}{1458}(8 + 9 \eta)^2 \right]p^2 \nonumber \\
  & + \kappa_0 f\sx'(1)\frac{\widetilde{\tau}_4}{\tau_0} + \mathcal{O}(|\nabla n|^6).
\end{align}
\fi

\subsection{Simplifying this expression: divergence theorem}

Just as was done in r$^2$SCAN, we can simplify the expanded $F\sx$ using the divergence theorem.
Given that the exchange energy is
\begin{equation}
  E\sx[n] = \int d^3r~F\sx n(\br) \varepsilon^{\text{UEG}}\sx(n) = -\frac{3}{4\pi}(3\pi^2)^{1/3}\int d^3r~F\sx n^{4/3}(\br),
\end{equation}
we can eliminate the term linear in $q$ in favor of a term linear in $p$,
\begin{align}
  n^{4/3}q &= \frac{1}{4(3\pi^2)^{2/3}}n^{-1/3}\lan \\
  &= \n \cdot\left[\frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}} \right] + \frac{1}{3}\frac{|\gn|^2}{4(3\pi^2)^{2/3}n^{4/3}} \\
  &= \n \cdot\left[\frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}} \right] + n^{4/3}\frac{1}{3}p .
    \label{eq:q_to_p_div_thm}
\end{align}

Let $\widetilde{F}\sx$ be the exchange enhancement factor after application of the divergence theorem,
\begin{align}
  E\sx &= \int_{\Omega} F\sx \varepsilon^{\text{UEG}}\sx(n) d^3 r = \int_{\Omega} \left[ \widetilde{F}\sx  + n^{-4/3}\nabla \cdot \bm{G}\sx \right] \varepsilon^{\text{UEG}}\sx(n) d^3 r \\
  & = \int_{\Omega} \widetilde{F}\sx  \varepsilon^{\text{UEG}}\sx(n)d^3 r  - \frac{3}{4\pi}(3\pi^2)^{1/3} \underbrace{\int_{\text{bdy}\, \Omega} \bm{G}\sx \cdot d\bm{S}}_{=0}.
\end{align}
Because the volume integral of the gauge function $\bm{G}\sx$ vanishes, an exchange functional using either $F\sx$ or $\widetilde{F}\sx$ would yield the same exchange energy, but different energy densities.

What should the bounding surface $\text{bdy}\, \Omega$ be? For typical metals, which have valence densities that are slowly-varying, we will see that the gradient expansion terms, including those of $\bm{G}\sx$, are likely finite everywhere. However, we will also see that $\bm{G}\sx$ diverges in the exponentially-decaying density tail of an atom. Molecules, wide-gap insulating solids, and solids with some defects also have regions of exponential decay: the tail of the molecule, the interstices of insulators, and within vacancies of solids. See E. Ospadov \textit{et al.} \cite{ospadov2018} for a discussion of atoms and molecules, and Kaplan \textit{et al.} \cite{kaplan2021} for a discussion of solids.
Therefore the bounding surface should be the Kohn-Sham turning surface, if it exists, and can be taken to be a surface at infinity otherwise.

When the gradient expansion is generalized, as in a meta-GGA, the divergent contributions from both $\widetilde{F}\sx$ and $\n \cdot \bm{G}\sx$ are cut off outside the turning surface. Therefore discarding the gauge term is reasonable, as its surface integral (outside the turning surface) would vanish after the cutoff procedure.

Then,
\begin{align}
  F\sx &= 1 + \left[ h_1 + g_1 - \frac{5(4 + 9\eta)\kappa_0}{27}f_1 \right]p
    + \frac{8\kappa_0}{81}\left[f_1 + 25f_2 \right] q^2 \\
    & + \left[\frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1
    + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
    + \frac{5(8+9\eta)}{27}( h_1 f_1 - \kappa_0 f_1 g_1)
    + \frac{8 + 75 \eta (8 + 9 \eta)}{243}\kappa_0 f_1 \right] p^2 \nonumber \\
    & - \left[ \frac{20}{9}h_1 f_1 - \frac{20}{9}\kappa_0 f_1 g_1
    + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2
    + \frac{3+100\eta}{27} \kappa_0 f_1 \right] p q \nonumber \\
    & + \kappa_0 f\sx'(1)\frac{\widetilde{\tau}_4}{\tau_0}
    + n^{-4/3}\n \cdot \left[\frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}} \right]
    + \mathcal{O}(|\nabla n|^6).
\end{align}
We're almost at a workable expression for $F\sx$. The last term to consider involves $\widetilde{\tau}_4$,
\begin{align}
  n^{4/3}\frac{\widetilde{\tau}_4}{\tau_0} &= \frac{10}{3}\cdot \frac{1}{4320(3\pi^2)^{4/3}} n^{-1/3} \n \cdot\left[12 \frac{\n(\lan)}{n^{2/3}} -22 \frac{\lan \gn}{n^{5/3}} -7 \frac{\n |\gn|^2}{n^{5/3}} + 19 \frac{\gn |\gn|^2}{n^{8/3}}\right].
\end{align}
Let's work through these as before. It might help to keep in mind that
\begin{align}
  p^2 &= \frac{|\gn|^4}{16(3 \pi^2)^{4/3}n^{16/3}} \implies \frac{|\gn|^4}{n^4} = 16(3 \pi^2)^{4/3} p^2 \, n^{4/3} \\
  p q &= \frac{\lan |\gn|^2}{16(3 \pi^2)^{4/3}n^{13/3}} \implies \frac{\lan |\gn|^2}{n^3} = 16(3 \pi^2)^{4/3} pq \, n^{4/3} \\
  q^2 &= \frac{(\lan)^2}{16(3 \pi^2)^{4/3}n^{10/3}} \implies \frac{(\lan)^2}{n^2} = 16(3 \pi^2)^{4/3} q^2 \, n^{4/3}.
\end{align}
First term:
\begin{align}
  n^{-1/3}\n \cdot [ n^{-2/3} \n(\lan)] &= \n \cdot [n^{-1}\n (\lan) ] - n^{-2/3} \n( \lan) \cdot \n(n^{-1/3}) \\
  &= \n \cdot [n^{-1}\n( \lan) ] + \frac{1}{3}n^{-2}\n(\lan)\cdot \gn \\
  &= \n \cdot \left[n^{-1}\n( \lan) + \frac{1}{3}n^{-2}\lan \gn \right] - \frac{1}{3}\lan \n \cdot(n^{-2}\gn)\\
  &= \n \cdot \left[n^{-1}\n( \lan) + \frac{1}{3}n^{-2}\lan \gn \right] - \frac{1}{3}\frac{(\lan)^2}{n^2} + \frac{2}{3}\frac{\lan |\gn|^2}{n^3} \\
  &= \n \cdot \left[n^{-1}\n( \lan) + \frac{1}{3}n^{-2}\lan \gn \right] + \frac{16(3\pi^2)^{4/3}}{3} \left[2 p q - q^2 \right]n^{4/3}.
\end{align}

Second term:
\begin{align}
  n^{-1/3}\n \cdot [ n^{-5/3} \lan \gn] &= \n \cdot [ n^{-2} \lan \gn] - n^{-5/3} \lan \gn \cdot \n(n^{-1/3}) \\
  &= \n \cdot [ n^{-2} \lan \gn] + \frac{1}{3}\frac{\lan |\gn|^2}{n^3} \\
  &= \n \cdot [ n^{-2} \lan \gn] + \frac{16(3\pi^2)^{4/3}}{3} p q \, n^{4/3}.
\end{align}

Third term:
\begin{align}
  n^{-1/3}\n \cdot [ n^{-5/3} \n |\gn|^2] &= \n \cdot [ n^{-2} \n |\gn|^2] - n^{-5/3} \n |\gn|^2 \cdot \n(n^{-1/3}) \\
  &= \n \cdot [ n^{-2} \n |\gn|^2] + \frac{1}{3}n^{-3}\gn \cdot \n |\gn|^2 \\
  &= \n \cdot \left[ n^{-2} \n |\gn|^2 + \frac{1}{3}n^{-3}|\gn|^2\gn \right] - \frac{1}{3}|\gn|^2 \n \cdot (n^{-3}\gn)\\
  &= \n \cdot \left[ n^{-2} \n |\gn|^2 + \frac{1}{3}n^{-3}|\gn|^2\gn \right] - \frac{1}{3}\frac{\lan |\gn|^2}{n^3} + \frac{|\gn|^4}{n^4} \\
  &= \n \cdot \left[ n^{-2} \n |\gn|^2 + \frac{1}{3}n^{-3}|\gn|^2\gn \right] + \frac{16(3\pi^2)^{4/3}}{3}\left[3p^2 - p q\right]n^{4/3}
\end{align}

Fourth term:
\begin{align}
  n^{-1/3}\n \cdot [n^{-8/3}|\gn|^2  \gn ] &= \n \cdot [n^{-3}|\gn|^2 \gn ] - n^{-8/3}|\gn|^2 \gn \cdot \n(n^{-1/3}) \\
  &= \n \cdot [n^{-3}|\gn|^2 \gn ] + \frac{1}{3}\frac{|\gn|^4}{n^4} \\
  &= \n \cdot [n^{-3}|\gn|^2 \gn ] + \frac{16(3\pi^2)^{4/3}}{3} p^2 \, n^{4/3}.
\end{align}

Putting these terms together yields
\begin{align}
  n^{4/3}\frac{\widetilde{\tau}_4}{\tau_0} &= \frac{1}{243} \left[ 12(2 pq - q^2) - 22 pq - 7 (3 p^2 - pq) + 19 p^2 \right]n^{4/3} \nonumber \\
  & + \frac{1}{1296(3\pi^2)^{4/3}} \n \cdot \bigg[ 12 n^{-1}\n( \lan) + 4 n^{-2}\lan \gn - 22 n^{-2} \lan \gn -7 n^{-2} \n |\gn|^2  \nonumber \\
  & \left. - \frac{7}{3}n^{-3}|\gn|^2\gn + 19 n^{-3}|\gn|^2 \gn \right],
  & \nonumber \\
  \frac{\widetilde{\tau}_4}{\tau_0} &= -\frac{1}{243}\left[12 q^2 - 9 p q + 2 p^2 \right] + \frac{n^{-4/3}}{1296(3\pi^2)^{4/3}} \n \cdot \left[12 \frac{\n( \lan)}{n} - 18\frac{\lan \gn}{n^2} \right. \nonumber \\
  & \left. -7\frac{\n |\gn|^2}{n^2} + \frac{50}{3}\frac{|\gn|^2 \gn}{n^3}  \right].
    \label{eq:t4_tilde_div_thm}
\end{align}
And we may rewrite $F\sx$ as
\begin{align}
  F\sx &= 1 + \left[ h_1 + g_1 - \frac{5(4 + 9\eta)\kappa_0}{27}f_1 \right]p
    + \frac{8\kappa_0}{81}\left[f_1 + 25f_2 \right] q^2 \\
    & + \left[\frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1
    + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
    + \frac{5(8+9\eta)}{27}( h_1 f_1 - \kappa_0 f_1 g_1)
    + \frac{8 + 75 \eta (8 + 9 \eta)}{243}\kappa_0 f_1 \right] p^2 \nonumber \\
    & - \left[ \frac{20}{9}h_1 f_1 - \frac{20}{9}\kappa_0 f_1 g_1
    + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2
    + \frac{3+100\eta}{27} \kappa_0 f_1 \right] p q
     -\frac{ \kappa_0 f_1}{243}\left[12 q^2 - 9 p q + 2 p^2 \right] \nonumber \\
    & + n^{-4/3}\n \cdot \left\{\frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}}
    \frac{1}{1296(3\pi^2)^{4/3}}\left[ 12 \frac{\n( \lan)}{n} - 18\frac{\lan \gn}{n^2}
    -7\frac{\n |\gn|^2}{n^2} + \frac{50}{3}\frac{|\gn|^2 \gn}{n^3}\right] \right\} \nonumber \\
    & + \mathcal{O}(|\nabla n|^6). \nonumber
\end{align}
After simplification,
\begin{align}
  F\sx &= 1 + \left[ h_1 + g_1 - \frac{5(4 + 9\eta)\kappa_0}{27}f_1 \right]p
    + \frac{4\kappa_0}{81}\left[f_1 + 50 f_2 \right] q^2 \\
    & + \left[\frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1
    + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
    + \frac{5(8+9\eta)}{27}( h_1 f_1 - \kappa_0 f_1 g_1)
    + \frac{6 + 75 \eta (8 + 9 \eta)}{243}\kappa_0 f_1 \right] p^2 \nonumber \\
    & - \left[ \frac{20}{9}h_1 f_1 - \frac{20}{9}\kappa_0 f_1 g_1
    + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2
    + \frac{2+100\eta}{27} \kappa_0 f_1 \right] p q \nonumber \\
    & + n^{-4/3}\n \cdot \left\{\frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}}
    + \frac{1}{1296(3\pi^2)^{4/3}}\left[ 12 \frac{\n( \lan)}{n} - 18\frac{\lan \gn}{n^2}
    -7\frac{\n |\gn|^2}{n^2} + \frac{50}{3}\frac{|\gn|^2 \gn}{n^3}\right] \right\} \nonumber \\
    & + \mathcal{O}(|\nabla n|^6). \nonumber
\end{align}

\iffalse
\begin{align}
  F\sx &= 1 + \left[h\sx'(0) - \frac{5(4 + 9\eta)}{27}\kappa_0 f\sx'(1) \right]p + \kappa_0 \frac{4}{81} \left[ f\sx'(1) + 50 f\sx''(1) \right]q^2 \nonumber \\
  & - \left[\kappa_0 f\sx'(1)\frac{2+100\eta}{27} + \frac{20}{9}h\sx'(0)f\sx'(1) + \kappa_0 f\sx''(1)\frac{100(8+9\eta)}{243} \right]p q \nonumber \\
  & + \left[\frac{h\sx''(0)}{2} +\kappa_0 f\sx'(1)\frac{6 + 75 \eta (8 + 9 \eta)}{243} + f\sx'(1)h\sx'(0)\frac{5(8 + 9\eta)}{27} + \kappa_0 f\sx''(1)\frac{25}{1458}(8 + 9 \eta)^2 \right]p^2 \nonumber \\
  & +  n^{-4/3}\n \cdot \left\{\frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}} + \frac{1}{1296(3\pi^2)^{4/3}}\left[ 12 \frac{\n( \lan)}{n} - 18\frac{\lan \gn}{n^2} -7\frac{\n |\gn|^2}{n^2} \right. \right. \nonumber \\
  & \left. \left. + \frac{50}{3}\frac{|\gn|^2 \gn}{n^3}\right] \right\} + \mathcal{O}(|\nabla n|^6).
\end{align}
\fi

Now we can write an expression for the integrated-by-parts enhancement factor $\widetilde{F}\sx$ that yields the same integrated exchange energy and exchange potential as $F\sx$, but a different exchange energy density
\begin{align}
  \widetilde{F}\sx &= 1 + \left[ h_1 + g_1 - \frac{5(4 + 9\eta)\kappa_0}{27}f_1 \right]p
    + \frac{4\kappa_0}{81}\left[f_1 + 50 f_2 \right] q^2 \nonumber \\
    & + \left[\frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1
    + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
    + \frac{5(8+9\eta)}{27}( h_1 f_1 - \kappa_0 f_1 g_1)
    + \frac{6 + 75 \eta (8 + 9 \eta)}{243}\kappa_0 f_1 \right] p^2 \nonumber \\
    & - \left[ \frac{20}{9}h_1 f_1 - \frac{20}{9}\kappa_0 f_1 g_1
    + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2
    + \frac{2+100\eta}{27} \kappa_0 f_1 \right] p q + \mathcal{O}(|\nabla n|^6), \label{eq:fx_final}
%  1 + \left[h\sx'(0) - \frac{5(4 + 9\eta)}{27}\kappa_0 f\sx'(1) \right]p + \kappa_0 \frac{4}{81} \left[ f\sx'(1) + 50 f\sx''(1) \right]q^2 \nonumber \\
%  & - \left[\kappa_0 f\sx'(1)\frac{2+100\eta}{27} + \frac{20}{9}h\sx'(0)f\sx'(1) + \kappa_0 f\sx''(1)\frac{100(8+9\eta)}{243} \right]p q \nonumber \\
%  & + \left[\frac{h\sx''(0)}{2} +\kappa_0 f\sx'(1)\frac{6 + 75 \eta (8 + 9 \eta)}{243} + f\sx'(1)h\sx'(0)\frac{5(8 + 9\eta)}{27} + \kappa_0 f\sx''(1)\frac{25}{1458}(8 + 9 \eta)^2 \right]p^2 + \mathcal{O}(|\nabla n|^6), \label{eq:fx_final}
\end{align}
and for completeness, the gauge function
\begin{align}
  \bm{G}\sx &= \frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}} + \frac{1}{1296(3\pi^2)^{4/3}}\left[ 12 \frac{\n( \lan)}{n} - 18\frac{\lan \gn}{n^2} -7\frac{\n |\gn|^2}{n^2} + \frac{50}{3}\frac{|\gn|^2 \gn}{n^3}\right] \nonumber \\
    & + \mathcal{O}(|\nabla n|^6).
\end{align}
