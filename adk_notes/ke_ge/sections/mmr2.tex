
This section gives numeric details of an r$^2$SCAN-style model for the enhancement factor,
\begin{equation}
  F\sx(p,\bar\alpha) = \{ h\sx^1(p) + f\sx(\bar\alpha)[h\sx^0 - h\sx^1(p)]\}g\sx(p),
\end{equation}
where $g\sx'(0)=g\sx''(0)=0$.
Then the parameters $h\sx'(0)$, $h\sx''(0)$, $f\sx'(1)$, and $f''\sx(1)$ are determined by Eqs. \ref{eq:dhx_roots}, \ref{eq:ddhx_roots}, \ref{eq:dfx_roots}, and \ref{eq:ddfx_roots}, respectively.

\subsection{Exchange}

For now, we keep our ansatz for $h\sx^1$,
\begin{equation}
  h\sx^1(p) = \frac{1 + h_1 p + h_2 p^2/2 + (1 + \kappa_1)(p/d_p)^3}{1 + (p/d_p)^3}.
\end{equation}
However, we need to modify the rSCAN interpolation function.
Since we want to make as few modifications as possible to r$^2$SCAN, consider the following piecewise function
\begin{equation}
  f\sx(x) = \left\{ \begin{array}{lr} (1 - x)\sum_{i=0}^9 b_{i\mathrm{x}} x^i, & x\leq 2.5 \\
  -d\sx \exp\left[\frac{c_{2\mathrm{x}}}{1-x} \right], & x > 2.5 \end{array} \right.,
\end{equation}
with $d\sx=1.24$ and $c_{2\mathrm{x}} = 0.8$ taken from the SCAN interpolation function.
There are 10 parameters in this model, although its easy to see that $b_0=1$.
We have explicitly separated-out the factor of $(1-x)$ because the rSCAN parameters, to the precision stated in their paper, make $|f\sx(1)| \lesssim 10^{-12}$ instead of zero.
We take the seven constraints of the rSCAN interpolation function, and add to these the constraints that $f\sx'(1)=f_1$ and $f\sx''(1)=f_2$.
Then the nine constraints are
\begin{align}
  f\sx'(0) &= \frac{df\sx^\text{SCAN}}{d\alpha}(0) = c_{1\mathrm{x}} = -0.667 \\
  f\sx''(0) &= \frac{d^2f\sx^\text{SCAN}}{d\alpha^2}(0) = c_{1\mathrm{x}}(2 + c_{1\mathrm{x}}) \\
  f\sx'(1) &= f_1 \\
  f\sx''(1) &= f_2 \\
  f\sx(x_0) &= f\sx^\text{SCAN}(x_0) \\
  \frac{d^j f\sx}{dx^j}(x_0) &= \frac{d^j f\sx^\text{SCAN}}{d\alpha^j }(x_0), \qquad j = 1,2,3,4.
\end{align}
The matching point $x_0=2.5$ is taken from rSCAN, again for compatibility, and \(c_{1\mathrm{x}} = -0.667\) from SCAN.

These constraints define a matrix equation that is easily solved for the coefficients as a function of $\eta$ and $x_0$.
For example, when $\eta=0.223658$, the optimal coefficients are
\begin{align}
  \bm{b}\sx = [& 1,0.3329999999981638,-0.11155550000181476,\\
      & -3.9177837175577244,12.331100554139594,-16.07458519620982,\nonumber \\
      & 10.780481492224087,-3.931921174658157,0.7443705881042744,\nonumber \\
      & -0.05747598610828107] \nonumber
\end{align}
The new interpolation function is plotted alongside the SCAN and rSCAN interpolation functions in Fig. \ref{fig:ief_x_comp}.
The (b) panel of that figure plots their first derivatives.

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fx_comp_interp}
    \caption{}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fx_comp_interp_deriv}
    \caption{}
  \end{subfigure}
  \caption{Comparison of the new piecewise polynomial X interpolation function with the SCAN and rSCAN interpolation functions (left), and their first derivatives (right).
  Here, $\eta=0.223658$.
  \label{fig:ief_x_comp}}
\end{figure}

We now have three free parameters to fit: $\eta$, $\kappa_1$, and $d_p$, which are shared with r$^2$SCAN correlation.
The fitting routine determines the interpolation function coefficients at each value of $\eta$.

\subsection{Correlation}

For the reasons outlined previously, we also re-optimize the rSCAN correlation interpolation function.
\textbf{In addition, we must refit the rSCAN interpolation function because it is not a monotone decreasing function.}
It can be seen from Fig. \ref{fig:ief_c_deriv_comp} that the rSCAN interpolation function is increasing when its argument is roughly between 1.64 and 1.76.

The structure is identical to the X interpolation function,
\begin{equation}
  f\mc(x) = \left\{ \begin{array}{lr} (1 - x)\sum_{i=0}^7 b_{i\mathrm{c}} x^i, & x\leq 2.5 \\
  -d\mc \exp\left[\frac{c_{2\mathrm{c}}}{1-x} \right], & x > 2.5 \end{array} \right.,
\end{equation}
as this guarantees that $f\mc(1)=0$ identically.
$d\mc=0.7$ and $ c_{2\mathrm{c}}=1.5$ are taken from SCAN; the arbitrary matching point is taken from rSCAN.
As we cannot constrain the interpolation function to recover the gradient expansion in the same way as for exchange, we have two fewer constraints than for exchange
\begin{align}
  f\mc'(0) &= \frac{df\mc^\text{SCAN}}{d\alpha}(0) = c_{1\mathrm{c}} = -0.64 \\
  f\mc''(0) &= \frac{d^2f\mc^\text{SCAN}}{d\alpha^2}(0) = c_{1\mathrm{c}}(2 + c_{1\mathrm{c}}) \\
  f\mc(x_0) &= f\mc^\text{SCAN}(x_0) \\
  \frac{d^j f\mc}{dx^j}(x_0) &= \frac{d^j f\mc^\text{SCAN}}{d\alpha^j }(x_0), \qquad j = 1,2,3,4. \\
\end{align}
\(c_{1\mathrm{c}} = -0.64\) is taken from SCAN, and $x_0=2.5$ from rSCAN.
The optimal coefficients are
\begin{align}
  \bm{b}\mc = [& 1,0.3599999999999802,-0.07520000000001449,\\
    & -2.9102780325567696,3.5297025324388795,-1.7124523194678987,\nonumber \\
    & 0.38449334136225327,-0.0332644461570117261], \nonumber
\end{align}
which have no dependence on the fitted parameters.

The new C interpolation function is plotted alongside the SCAN and rSCAN interpolation functions in Fig. \ref{fig:ief_c_comp}.
The (b) panel of that figure plots their first derivatives.

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fc_comp_interp}
    \caption{}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{fc_comp_interp_deriv}
    \caption{
    \label{fig:ief_c_deriv_comp}}
  \end{subfigure}
  \caption{Comparison of the new piecewise polynomial C interpolation function with the SCAN and rSCAN interpolation functions (left), and their first derivatives (right).
  Note that the rSCAN interpolation function is not a monotonically decreasing function of its argument.
  Its derivative takes a local maximum when $x \approx 1.698292$.
  \label{fig:ief_c_comp}}
\end{figure}


\subsection{The fitted model}

The following parameters are determined by running 20 differential evolution optimizations of the three parameters, using the target
\begin{equation}
  \sigma = \text{SAXC}/0.1 + \text{JSFXC}/5
\end{equation}
with SAXC the spherical atom XC root-mean-squared percent error (RMSPE), and JSFXC the jellium surface formation XC energy RMSPE.
Differential evolution is a stochastic method, so the results can differ even when the starting conditions are the same.
Within the packaged optimization routine, a ``polish'' option (a grid search or other refinement method) is used to help reduce the arbitrariness of the final, stochastically-determined parameters.
We then take the best of the 20 runs.

Using the non-analytic $g\sx = 1 - e^{-a\sx/p^{1/4}}$, the average value of $\eta = 0.223658$ (standard deviation $3.13\times 10^{-6}$).
$\kappa_1 = 0.013640$ (standard deviation $4.0 \times 10^{-7}$) and $d_p = 0.078155$ (standard deviation $3.17 \times 10^{-6}$) are the statistical averages of the other two parameters.
To six digits of precision, the ``best'' parameters were
\begin{align}
  \eta^\text{opt} &= 0.223658 \\
  \kappa_1^\text{opt} &= 0.013641 \\
  d_p^\text{opt} &= 0.078158.
\end{align}
From the value of $\eta$,
\begin{align}
  f_1 &= -1.095631 \\
  f_2 &= 0.189729 \\
  h_1 &= -0.088821 \\
  h_2 &= -0.201446.
\end{align}
Note that, unlike SCAN or r$^2$SCAN, $h\sx(p)$ has \textit{negative} first and second derivatives at $p=0$.
This is similar to TASK, but unlike most other non-empirical meta-GGAs.
I suspect that this has made satisfying the appropriate norms more challenging.

The norm scores are given below in Table \ref{tab:norms_gx_ori}, compared to SCAN.
This form is more accurate than SCAN for the atomic norms and jellium surfaces, but only incrementally more accurate.

\begin{table}[ht]
  \centering
  \begin{tabular}{crrrrr} \hline\hline
    Functional & SAXC XC RMSPE & NSAXC RMSPE & LZ $B\sx$ PE & LZ $B\mc$ PE & JSFXC RMSPE \\ \hline
    SCAN & 0.16 & 0.34 & -0.08 & 0.01 & 6.25 \\
    r$^2$ reopt, NAG & 0.07 & 0.27 & -3.63 & 1.88 & 4.77 \\
    r$^2$ reopt, AG & 0.07 & 0.29 & -3.66 & -0.47 & 4.89 \\ \hline\hline
  \end{tabular}
  \caption{Comparison of norm scores.
  SAXC = spherical atom XC, NSAXC = non-spherical atom XC.
  LZ = large-$Z$ atoms; $B\sx$ is the leading-order correction to LDA exchange, and $B\mc$ the leading-order correction to correlation for an atom of large, finite $Z$.
  JSFXC = jellium surface formation XC energies.
  RMSPE = root-mean-squared percent error, and PE = percent error.
  NAG = non-analytic $g\sx(p)$, AG = analytic $g\sx(p)$.
  \label{tab:norms_gx_ori}}
\end{table}

Note that if we instead use the analytic $g\sx(p) = [1 + (p/a\sx)^2]^{-1/8}$, we get similar performance but with a smaller value of $\eta$, and hence a larger slope at $\ba =1$:
\begin{align}
  \eta &= 0.12902858154786187 \\
  \kappa_1 &= 0.01725609034858679 \\
  d_p &= 0.07977304708578874 \\
  f_1 &= -1.134107 \\
  f_2 &= 0.190498 \\
  h_1 &= -0.065153 \\
  h_2 &= -0.186566.
\end{align}

These fitted models have two unusual, and perhaps undesirable, features: $\kappa_1$ and $d_p$ are both very small.
For practical purposes, the smallness of $d_p$ is more relevant, as this makes the energy density vary more rapidly as a function of $p$.
To try to make $d_p$ larger, we can add a penalty to the objective function, $1/d_p$.
The optimal parameters ($\eta=0.001$, $\kappa = 0.056$, $d_p=0.287$) give norm scores that are above the targets: SAXC = 0.17\% and JSFXC = 5.60\%.
