\section{Summary of Equations}

\begin{align}
  \ba(n) &= \frac{\tau - \tau_W(n)}{\tau_0(n) + \eta \tau_W(n)} \qquad \text{(spin-unpolarized, for exchange)} \\
  \ba(\nup,\ndn) &= \frac{\tau - \tau_W(n)}{\tau_0(n)d_s(\zeta) + \eta \tau_W(n)}  \qquad \text{(spin-dependent, for correlation)} \\
  \tau_W(n) &= \frac{|\gn|^2}{8n} \\
  \tau_0(n) &= \frac{3}{10}\kf(n)^2 n \\
  d_s(\zeta) &= [(1 + \zeta)^{5/3} + (1 - \zeta)^{5/3}]/2 \\
  \zeta &= \frac{\nup - \ndn}{n}\\
  \kf(n) &= (3\pi^2 n)^{1/3} \\
  p(n) &= \left[\frac{|\gn|}{2 \kf n}\right]^2.
\end{align}
$\eta$ is a fit parameter used to ensure that the semilocal part of the KS potential
is nonpositive for the H atom.

\subsection{Exchange}

\begin{align}
  E\sx[\nup,\ndn] &= \frac{1}{2}\left(E\sx[2\nup] + E\sx[2\ndn] \right)\\
  E\sx[n] &= \int F\sx(p,\ba)\varepsilon^{\text{LDA}}\sx(n) n \, d^3 r \\
  \varepsilon^{\text{LDA}}\sx(n) &= -\frac{3}{4\pi}(3\pi^2 n)^{1/3} \\
  F\sx(p,\ba) &= \left\{h\sx^1(p) + f\sx(\ba)[h\sx^0 - h\sx^1(p)] \right\}g\sx(p) \\
  h\sx^0 &= 1 + k_0 = 1.174 \\
  g\sx(p) &= 1 - \exp\left[-\frac{a\sx}{p^{1/4}} \right] \\
  & \nonumber \\
  f\sx(\ba) &= (1-\ba)\frac{1 + c_1 \ba + c_2 \ba^2 + c_3 \ba^3
    + d\sx d_{\ba\text{x}} \ba^4 }{1 +  d_{\ba\text{x}} \ba^5} \\
  c_1 &= 1 + f_{\text{x}0} \\
  c_2 &= -5 + d_{\ba\text{x}} d\sx - 2 f_{\text{x}0} + (2d_{\ba\text{x}}-3) f_1
    + (1 + d_{\ba\text{x}})\frac{f_2}{2} \\
  c_3 &= 3 - 2 d_{\ba\text{x}} d\sx + f_{\text{x}0} + (2 - 3 d_{\ba\text{x}}) f_1
    - (1 + d_{\ba\text{x}})\frac{f_2}{2} \\
  & \nonumber \\
  h\sx^1(p) &= \frac{1 + h_1 p + h_2 p^2 + (1 + \kappa_1)(p/d_p)^3}{1 + (p/d_p)^3}
\end{align}

Model parameters:
\begin{itemize}
  \item $a\sx$, fitted to hydrogen atom exchange energy. Could use SCAN
   value of 4.9479 or refine this further.
  \item $d\sx = \lim_{\ba \to \infty}f\sx(\ba)$, fitted to norms.
   Initial guess $d\sx=1.24$.
  \item $f_{\text{x}0} \equiv f\sx'(0)$, initial guess $f_{\text{x}0}=-0.667$.
  \item $ d_{\ba\text{x}}$, controls rate of increase of $f\sx$.
    May not require fitting; initial guess $d_{\ba\text{x}} = 1$.
  \item $\kappa_1$, fitted to norms. Initial guess $\kappa_1 = 0.065$.
  \item $d_p$, fitted to norms.
    Controls rate of increase of $h\sx^1(p)$, may not require fitting; initial guess $d_p = 1$.
\end{itemize}

For a given set of model parameters, the following parameters are determined (not fitted) as:
\begin{align}
  f_1 &= f\sx'(1) = \frac{1}{2f_a}\left[-f_b \pm \sqrt{f_b^2 - 4 f_a f_c} \right] \\
  f_a &=\frac{100(4 + 9 \eta)\kappa_0}{243}\\
  f_b &= \frac{200 + 6\kappa_0(1 + 441 \eta)}{729}\\
  f_c &= \frac{657\eta - 511}{6075} \\
  & \nonumber \\
  f_2 &= f\sx''(1) = \frac{73}{2500\kappa_0} - \frac{1}{50}f_1 \\
  & \nonumber \\
  h_1 &= h\sx'(0) = \frac{10}{81} + \frac{5(4 + 9\eta)}{27}\kappa_0 f_1 \\
  h_2 &= h\sx''(0) = - 2\left\{ \kappa_0 f_1\frac{6 + 75 \eta (8 + 9 \eta)}{243} + f_1 h_1\frac{5(8 + 9\eta)}{27} \right. \nonumber \\
    & \left. + \kappa_0 f_2\frac{25}{1458}(8 + 9 \eta)^2 \right\}.
\end{align}
Recommend taking the negative root solution of $f_1$ to increase bandgaps, as in TASK.

\subsection{Correlation}


\begin{align}
  E\mc[\nup,\ndn] &= \int \varepsilon\mc(\rs,\zeta,p,\ba) n\, d^3 r \\
  \varepsilon\mc(\rs,\zeta,p,\ba) &= \varepsilon\mc^1(\rs,\zeta,p) + f\mc(\ba)[\varepsilon\mc^1(\rs,\zeta,p) - \varepsilon\mc^0(\rs,\zeta,p)] \\
  f\mc(\ba) &= (1- \ba)^3\frac{1 + (3 + f_{\text{c}0})\ba + d\mc d_{\ba\text{c}} \ba^2}{1 + d_{\ba\text{c}} \ba^5}.
\end{align}
Alarm bells might be going off: why are we returning to the $(1-\ba)^3$ form of $f\mc(\ba)$?
There are three well-motivated reasons:
\begin{enumerate}
  \item The density-gradient coefficient for correlation is of opposite sign to that of exchange.
  This contribution may reduce the overall bandgap.

  This can be seen by comparing bandgaps computed with SCAN X + SCAN C to those computed with  SCAN X + PW92, as has been done by Neupane \textit{et al.} \cite{neupane2021}.
  See, in particular, their Fig. 3 and accompanying discussion in Sec. IV.A.
  That the bandgaps predicted with TASK X + SCAN C are not much smaller than those predicted by TASK X + PW92 C is important here.

  In principle, a meta-GGA implemented in a generalized Kohn-Sham scheme can predict both accurate total energies and accurate band gaps, see Perdew \textit{et al.} \cite{perdew2017}.

  \item While the $\zeta$-dependence of SCAN is not quite right, the corrections needed in r$^2$SCAN to recover the second-order gradient expansion seem to make this even less correct.
  This is seen in the magnetic moments of transition metals; r$^2$SCAN increases the magnetization more than the already-too-large SCAN values.
  \item Exchange is more important than correlation in high-density regions, thus it is more important to reduce oscillations in the exchange functional than the correlation functional.
  The oscillations in the KS potential will still be reduced by virtue of exchange being smoother.
\end{enumerate}

$\ba=1$ equations:
\begin{align}
  \varepsilon\mc^1(\rs,\zeta,p) &= \varepsilon\mc^{\text{PW92}}(\rs,\zeta) + H_1(\rs,\zeta,p) \\
  H_1(\rs,\zeta,p) &= \gamma \phi(\zeta)^3 \ln \{1 + w_1(\rs,\zeta)[1 - g(A t^2)] \} \\
  \phi(\zeta) &= [(1 + \zeta)^{2/3} + (1 - \zeta)^{2/3}]/2 \\
  w_1(\rs,\zeta) &= \exp\left[-\frac{\varepsilon\mc^{\text{PW92}}(\rs,\zeta)}{\gamma \phi(\zeta)^3} \right] -1 \\
  A(\rs,\zeta) &= \frac{\beta(\rs)}{\gamma w_1(\rs,\zeta)} \\
  t(\rs,\zeta,p) &= \left(\frac{3\pi^2}{16}\right)^{1/3} \frac{p^{1/2}}{\phi(\zeta)\rs^{1/2}} \\
  g(y) &= [1 + 4y + 10y^2]^{-1/4} \\
  \beta(\rs) &= \beta(0)\frac{1 + 0.5\rs + 0.083335\rs^2}{1 + 0.5\rs + 0.148165\rs^2} \\
  \beta(0) &= 0.066724526517184.\\
  \gamma &= (1 - \ln 2)/\pi^2.
\end{align}
The form of $\beta(\rs)$ is taken from the acGGA, and gives an improved fit to the Hu-Langreth expression at small $\rs$, and allows for perfect cancellation between the second-order exchange and correlation gradient coefficients at large $\rs$, as in the revTPSS expression used in SCAN and r$^2$SCAN,
\[
  \beta^{\text{revTPSS}}(\rs) = \beta(0)\frac{1 + 0.1 \rs}{1 + 0.1778\rs}.
\]
Note also that $g(y)$ has changed to make the interpolation more PBE-like,
\[
  1 - g(y) \approx y + \mathcal{O}(y^3).
\]
A similar change was suggested by revSCAN.

$\ba=0$ equations:
\begin{align}
  \varepsilon\mc^0(\rs,\zeta,p) &= \left\{\varepsilon\mc^{\text{LDA0}}(\rs,\zeta) + H_0(\rs,\zeta,p) \right\}G\mc(\zeta) \\
  G\mc(\zeta) &= \{1 - b_{\text{4c}}[d\sx(\zeta)-1] \}(1 - \zeta^{12}) \\
  b_{\text{4c}} &= \frac{3 h\sx^0}{4} \left(\frac{3}{2\pi} \right)^{2/3}\frac{b_{\text{3c}}}{b_{\text{2c}}} \approx 2.363025 \\
  d\sx(\zeta) &= [(1 + \zeta)^{4/3} + (1 - \zeta)^{4/3}]/2 \\
  \varepsilon\mc^{\text{LDA0}}(\rs,\zeta) &= -\frac{b_{\text{1c}}}{1 + b_{\text{2c}}\rs^{1/2} + b_{\text{3c}}\rs} \\
  H_0(\rs,p) &= b_{\text{1c}} \ln\{ 1 + w_0(\rs) [1 - g_\infty(p)]\} \\
  g_{\infty}(p) &= (1 + 4\chi_{\infty} p)^{-1/4} \\
  w_0(\rs) &= \exp\left[-\frac{\varepsilon\mc^{\text{LDA0}}(\rs)}{b_{\text{1c}}}\right] - 1 \\
  \chi_{\infty} &= \lim_{\zeta \to 0}\left\{ \lim_{\rs \to \infty} \frac{\beta(\rs)}{\gamma \exp[-\varepsilon_{\text{xc}}^{\text{LSDA}}/(\gamma \phi^3)] - 1}\frac{t^2}{p} \right\} \nonumber \\
  & = \left(\frac{3\pi^2}{16}\right)^{2/3} \frac{\beta(\infty)}{\{ 0.9-3[3/(16\pi)]^{2/3}\}} \approx 0.128029
\end{align}
If we used the revTPSS $\beta(\rs)$, we could retain the SCAN values of $b_{\text{1c}} = 0.0285764$, $b_{\text{1c}} = 0.0889$, and $b_{\text{1c}} = 0.125541$.
However, using the acGGA parameterization of $\beta(\rs)$, these will need to be refitted.
It's worth noting that $b_{\text{1c}}$ is fitted to the correlation energy of an $N=2$, $Z\to \infty$ ion, $-0.0467$ hartree.
This is a ``Bohr atom'', where both electrons reside in the 1s shell, so
\[
  n_{\text{Bohr}}(r) = \frac{2 Z^3}{\pi} e^{-2 Z r},
\]
which is a two-electron hydrogen atom density subject to uniform scaling, $r \to Z r$.
$\rs \to 0$ and $\zeta=0$ for this system, so $w_0(\infty)=e-1$.
Then, as $p$ is invariant under uniform scaling,
\begin{align}
  E\mc[n_{\text{Bohr}}] &= -b_{\text{1c}} \int \left\{ 1 - \ln[1 + (e-1)[1 - g_{\infty}(p)]\, ] \right\} \frac{2}{\pi} e^{-2 R}\, d^3(R) = -0.0467 \\
  E\mc[n_{\text{Bohr}}] &= b_{\text{1c}} \int \ln\left[1 + \frac{1-e}{e}g_{\infty}(p)\right] \frac{2}{\pi} e^{-2 R}\, d^3(R) = -0.0467 \\
  p(R) &= \frac{e^{4R/3}}{(6\pi)^{2/3} } \nonumber,
\end{align}
where $R = Z r$.

$b_{\text{3c}}$ is determined by asserting that the exchange correlation energy of any two-electron system is bounded from below by $1.67082E\sx^{\text{LDA}}$:
\begin{align}
  \min[\varepsilon_{\text{xc}}] &= h\sx^0 \varepsilon\sx^{\text{LDA}} + \min[\varepsilon\mc^{\text{LDA0}}(\rs,\zeta)] = 1.67082\varepsilon\sx^{\text{LDA}} \\
  \frac{b_{\text{1c}}}{b_{\text{3c}}} &= (1.67082 - h\sx^0)\frac{3}{4\pi}\left(\frac{9\pi}{4} \right)^{1/3}.
\end{align}
Then $b_{\text{2c}}$ is determined by fitting to the Helium atom exchange-correlation energy, $-1.068$ hartree.

The model parameters are:
\begin{itemize}
  \item $ f_{\text{c}0}\equiv f\mc'(0)$, initial guess $ f_{\text{c}0}=-0.64$.
  \item $d_{\ba\text{c}}$, controls rate of increase of $f\mc$.
    May not require fitting, initial guess $d_{\ba\text{c}}=1$.
  \item $d\mc = \lim_{\ba \to \infty}f\mc(\ba)$, fitted to norms.
  Initial guess: SCAN value of 0.7.
  %\item $d_{\text{c}\ba}$, controls the rate of increase of $f\mc(\ba)$. May not
  %require fitting; initial guess $d_{\text{c}\ba}=1$.
\end{itemize}
