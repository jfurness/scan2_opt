\subsection{TASK}

Our expression for $\widetilde{F}\sx$, Eq. \ref{eq:fx_final}, should agree with TASK's enhancement factor \cite{aschebrock2019}, at least up to a gauge, in the limit that $\eta \to 0$.
This is because TASK uses $\alpha = \ba(\eta=0)$, and has $g_1=g_2=0$.
Then Eq. \ref{eq:fx_final} reduces to
\begin{align}
  \lim_{\eta\to 0}\widetilde{F}\sx &= 1
  + \left[h_1 - \frac{20}{27}\kappa_0 f_1 \right]p
  + \kappa_0 \frac{4}{81} \left[ f_1 + 50 f_2 \right]q^2
   - \left[\frac{20}{9}h_1 f_1 + \frac{800}{243}\kappa_0 f_2
  + \frac{2}{27}\kappa_0 f_1 \right]p q \nonumber \\
  & + \left[\frac{h_2}{2} + \kappa_0 f_2 \frac{800}{729}
  + f_1 h_1 \frac{40}{27} +\kappa_0 f_1 \frac{6}{243} \right]p^2
  + \mathcal{O}(|\nabla n|^6).
\end{align}
In Ref. \cite{aschebrock2019}, the following notation is used
\begin{equation}
  F^{\text{TASK}}\sx \to 1 + \mu_s p + \mu_{\alpha}(\alpha -1) + C_s p^2 + C_{s\alpha} p(\alpha-1) + C_{\alpha}(\alpha-1)^2 + \mathcal{O}(|\gn|^6).
\end{equation}
To make contact with our gradient expansion, we use Eq. \ref{eq:fx_taylor} with $g_1=g_2=0$
\begin{align}
  F\sx &= 1 + h_1 p + \kappa_0 f_1(\alpha - 1) + \frac{h_2}{2} p^2
     - h_1 f_1 p(\alpha - 1) + \frac{\kappa_0 f_2}{2}(\ba-1)^2
     + \mathcal{O}(|\gn|^6)
\end{align}
Then we can equate
\begin{align}
  h_1 &= \mu_s\\
  f_1 &= \mu_{\alpha} /\kappa_0  \\
  h_2 &= 2 C_s\\
  f_2 &= 2C_{\alpha}/\kappa_0 \\
  h_1 f_1 &= -C_{s\alpha}
\end{align}
so that
\begin{align}
  \lim_{\eta\to 0}\widetilde{F}\sx &= 1 + \left[\mu_s - \frac{20}{27} \mu_{\alpha} \right]p + \frac{4}{81} \left[ \mu_{\alpha} + 100 C_{\alpha} \right]q^2 + \left[-\frac{2}{27}\mu_{\alpha} + \frac{20}{9}C_{s\alpha} - \frac{1600}{243} C_{\alpha} \right]p q \nonumber \\
  & + \left[C_s + \frac{2}{81}\mu_{\alpha} - \frac{40}{27}C_{s\alpha} + \frac{1600}{729}C_{\alpha} \right]p^2 + \mathcal{O}(|\nabla n|^6).
\end{align}
This is identical to Eq. (A7) of Ref. \cite{aschebrock2019}, therefore it is likely that our expression for $\widetilde{F}\sx$ is correct for $\eta>0$.


\subsection{Higher order gradient expansion of r$^2$SCAN}

Eq. \ref{eq:fx_final} is general enough that it can tell us what r$^2$SCAN estimates the fourth-order gradient expansion coefficients to be.
We need only plug in the appropriate values from r$^2$SCAN,
\begin{align}
  f\sx'(1) &= \sum_{i=1}^7 i c_{i\text{x}} \approx -0.935300 \\
  f\sx''(1) &= \sum_{i=2}^7 i(i-1) c_{i\text{x}} \approx 0.850036 \\
  h\sx'(0) &= \frac{5(4 +9 \eta )}{27 } (h\sx^0-1)f\sx'(1) + \mu_{\text{AK}} \approx 0.002636 \\
  h\sx''(0) &= -\frac{2h\sx'(0)^2}{\kappa_1} \approx -0.000214 \\
  g\sx'(0) &= 0 \\
  g\sx''(0) &= 0.
\end{align}
into Eq. \ref{eq:fx_final}
\begin{align}
  \widetilde{F}\sx^\text{r$^2$SCAN} &= 1 + \left[ h_1 - \frac{5(4 + 9\eta)\kappa_0}{27}f_1 \right]p
    + \frac{4\kappa_0}{81}\left[f_1 + 50 f_2 \right] q^2 \\
    & + \left[\frac{h_2}{2} + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
    + \frac{5(8+9\eta)}{27}h_1 f_1
    + \frac{6 + 75 \eta (8 + 9 \eta)}{243}\kappa_0 f_1 \right] p^2 \nonumber \\
    & - \left[ \frac{20}{9}h_1 f_1 + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2
    + \frac{2+100\eta}{27} \kappa_0 f_1 \right] p q + \mathcal{O}(|\nabla n|^6), \nonumber
\end{align}
By design, r$^2$SCAN captures the coefficient of $p$ exactly, $\mu_{\text{AK}}$.

\begin{table}[h]
  \centering
\begin{tabular}{ccrrr} \hline
  Coefficient & Exact & Exact (numerical) & r$^2$SCAN value & Percent error \\ \hline
  $q^2$ & 146/2025 & 0.072099 & 0.357164 & 395.4 \\
  $pq$ & -73/405 & -0.180247 & -0.469346 & 160.4 \\
  $p^2$ & 0 & 0.000000 & 0.154493 & \\ \hline
\end{tabular}
\caption{r$^2$SCAN fourth-order gradient expansion coefficients for exchange.
  \label{tab:r2scan_gex4_coeffs}}
\end{table}

From Table \ref{tab:r2scan_gex4_coeffs}, we see that r$^2$SCAN overestimates the magnitude of the fourth-order terms, but finds the correct sign.
The development of r$^2$SCAN introduced an important construction principle: smoothness.
We intuited that r$^2$SCAN was unlikely to recover an accurate higher-order gradient expansion, but the way that we attempted to correct this in r$^4$SCAN resulted in a less-smooth, less-accurate functional.

r$^4$SCAN is still an important milestone, as it lets us probe the limitations of meta-GGAs.
r$^4$SCAN is smoother than SCAN, but the contortions needed to produce the right fourth-order gradient expansion harm the accuracy and stability of r$^2$SCAN.

The construction of a meta-GGA outlined below lets us keep the smoothness of r$^2$SCAN and -- hopefully -- its accuracy.

\subsection{Higher order gradient expansion of the meta-GGA Made Very Simple}

The fourth-order gradient expansion of the meta-GGA Made Very Simple (MVS) \cite{sun2015a} can also be deduced from Eq. \ref{eq:fx_final}.
In MVS, $\eta=0$ and
\begin{align}
  h\sx(p) &= 1 \\
  h_1 &= 0 \\
  h_2 &= 0 \\
  & \nonumber \\
  f\sx(\alpha) &= \frac{1-\alpha}{[(1 + c_e \alpha^2)^2 + c_1 \alpha^4]^{1/4}} \\
  f_1 &= -0.957854 \\
  f_2 &= 2.990509 \\
  & \nonumber \\
  g\sx(p) &= [1 + bp^2]^{-1/8}\\
  g_1 &= 0 \\
  g_2 &= -0.005825.
\end{align}
The parameters are $c_e = -1.6665$, $c_1 = [20k_0/(27\mu_\text{AK})]^4 - (1 + c_e)^2$, and $b = 0.0233$.
Now plug these values into Eq. \ref{eq:fx_final},
\begin{align}
  \widetilde{F}\sx^\text{MVS} &= 1  - \frac{20\kappa_0}{27}f_1 p
    + \frac{4\kappa_0}{81}\left[f_1 + 50 f_2 \right] q^2 \\
    & + \left[\frac{g_2}{2} + \frac{800\kappa_0}{729}f_2
    + \frac{6}{243}\kappa_0 f_1 \right] p^2
    - \left[ \frac{800 \kappa_0}{243}f_2 + \frac{2}{27} \kappa_0 f_1 \right] p q
    + \mathcal{O}(|\nabla n|^6). \nonumber
\end{align}
Table \ref{tab:mvs_gex4_coeffs} presents the values and errors of these coefficients.
Note that the coefficient of $p$ is constrained to the exact value in MVS.
Like r$^2$SCAN, MVS finds the right sign for the coefficients, but highly-incorrect values.
MVS makes larger errors than does r$^2$SCAN in the values of the coefficients.

\begin{table}[h]
  \centering
\begin{tabular}{ccrrr} \hline
  Coefficient & Exact & Exact (numerical) & MVS value & Percent error \\ \hline
  $q^2$ & 146/2025 & 0.072099 & 1.276581 & 1670.6 \\
  $pq$ & -73/405 & -0.180247 & -1.700736 & 843.6 \\
  $p^2$ & 0 & 0.000000 & 0.563999 & \\ \hline
\end{tabular}
  \caption{MVS fourth-order gradient expansion coefficients for exchange.
    \label{tab:mvs_gex4_coeffs}}
\end{table}
