\subsection{Alternate iso-orbital indicators}

\subsubsection{Gradient expansion and spin resolution of $\beta$}

Furness and Sun \cite{furness2019} suggested
\begin{equation}
  \beta = \frac{\tau - \tau_W}{\tau + \tau_0}
    = \frac{\ba(1 + 5\eta p/3)}{1 + \ba(1 + 5\eta p/3) + 5p/3}
    = \frac{\alpha}{1 + \alpha + 5p/3}
\end{equation}
as a numerically more-stable iso-orbital indicator than $\alpha$.
Using the same gradient expansion for $\tau$, Eqs. \ref{eq:tau_ge_o4} and \ref{eq:tau_4_div},
\begin{align}
  \beta &= \frac{1}{2} \left[1 + \frac{20}{9}q - \frac{40}{27} p
    + \frac{8}{81} q^2 - \frac{1}{9} p q + \frac{8}{243} p^2
    + \frac{\widetilde{\tau}_4}{\tau_0} + \mathcal{O}(\nabla^6) \right] \\
  & \cdot \left[1 + \frac{10}{9}q + \frac{5}{54} p
    + \frac{4}{81} q^2 - \frac{1}{18} p q + \frac{4}{243} p^2
    + \frac{1}{2} \frac{\widetilde{\tau}_4}{\tau_0} + \mathcal{O}(\nabla^6) \right]^{-1}
    \nonumber \\
  & = \frac{1}{2} \left[1 + \frac{20}{9}q - \frac{40}{27} p
    + \frac{8}{81} q^2 - \frac{1}{9} p q + \frac{8}{243} p^2
    + \frac{\widetilde{\tau}_4}{\tau_0} + \mathcal{O}(\nabla^6) \right] \\
  & \cdot \left[1 - \frac{10}{9}q - \frac{5}{54} p
    + \frac{96}{81} q^2 + \frac{127}{486} p q - \frac{23}{2916} p^2
    - \frac{1}{2} \frac{\widetilde{\tau}_4}{\tau_0}  + \mathcal{O}(\nabla^6) \right]
    \nonumber \\
  &= \frac{1}{2}\left[1 + \frac{10}{9}q - \frac{85}{54}p
    -\frac{32}{27}q^2 + \frac{773}{486} p q + \frac{473}{2916} p^2
    + \frac{1}{2} \frac{\widetilde{\tau}_4}{\tau_0}
  \right] + \mathcal{O}(\nabla^6) \label{eq:beta_ge_o4} \\
  &= \frac{1}{2} + \frac{5}{9}q - \frac{85}{108}p
    -\frac{16}{27}q^2 + \frac{773}{972} p q + \frac{473}{5832} p^2
    + \frac{1}{4} \frac{\widetilde{\tau}_4}{\tau_0} + \mathcal{O}(\nabla^6).
\end{align}
To second order, we can approximately invert Eq. \ref{eq:beta_ge_o4} to express $q$ in terms of $p$ and $\beta$ as
\begin{equation}
  q \approx \frac{9}{10}(2\beta - 1) + \frac{17}{12}p.
\end{equation}
The gradient expansion of the spin-resolved $\beta$ can be expressed either with partial spin resolution
\begin{align}
  \beta(\nup,\ndn) &= \frac{\tau(\nup,\ndn) - \tau_W(n)}{\tau(\nup,\ndn) + \tau_0(n)d\sus(\zeta)} \\
  &= \frac{1}{2} + \frac{5}{9 d\sus(\zeta)}q - \frac{85}{108 d\sus(\zeta)}p
    + \frac{5}{54(1-\zeta^2)}\sigma^2 +\mathcal{O}(|\gn|^4),
\end{align}
or with full spin resolution
\begin{align}
  \beta_\text{FSR}(\nup,\ndn) &=
    \frac{\tau(\nup,\ndn) - \tau_W(\nup,\ndn)}{\tau(\nup,\ndn) + \tau_0(n)d\sus(\zeta)} \\
  &= \frac{1}{2} + \frac{5}{9 d\sus(\zeta)}q - \frac{85}{108 d\sus(\zeta)}p
    - \frac{85}{54(1-\zeta^2)}\sigma^2 +\mathcal{O}(|\gn|^4).
\end{align}


\subsubsection{$\beta$-dependent exchange enhancement factor}

To find an expression for the enhancement factor of a $\beta$-dependent meta-GGA, we'll use a slightly different notation to distinguish from the $\ba$-dependent meta-GGA.
Let the exchange enhancement factor be
\begin{equation}
  F\sx(p,\beta) = \{ h\sx(p) + j\sx(\beta)[h\sx^0 - h\sx(p)] \} g\sx(p)
\end{equation}
where each component has a Taylor series
\begin{align}
  h\sx(p) &= 1 + h\sx'(0)p + \frac{h\sx''(0)}{2}p^2 + \mathcal{O}(p^3) \\
    & \equiv 1 + h_1 p + \frac{h_2}{2}p^2 + \mathcal{O}(p^3) \nonumber \\
  g\sx(p) &= 1 + g\sx'(0)p + \frac{g\sx''(0)}{2}p^2 + \mathcal{O}(p^3) \\
    & \equiv 1 + g_1 p + \frac{g_2}{2}p^2 + \mathcal{O}(p^3) \nonumber  \\
  j\sx(\beta) &= j\sx'(1/2)\left(\beta - \frac{1}{2} \right)
    + \frac{1}{2}j\sx''(1/2)\left(\beta - \frac{1}{2} \right)^2
    + \mathcal{O}\left(\beta - \frac{1}{2} \right)^3 \\
    & \equiv j_1(2\beta - 1) + \frac{j_2}{2}(2\beta-1)^2 + \mathcal{O}(2\beta-1)^3,
    \nonumber
\end{align}
where $j_1 \equiv j\sx'(1/2)/2$ and $j_2 \equiv j\sx''(1/2)/4$.
Using these replacements, Eq. \ref{eq:fx_taylor} can be used to write the overall exchange enhancement factor to fourth-order
\begin{align}
  F\sx &= 1 + (h_1 + g_1)p + \kappa_0 j_1(2\beta - 1)
    + \frac{1}{2} \left(h_2 + g_2 + 2 h_1 g_1 \right)p^2
    + [\kappa_0 j_1 g_1 - h_1 j_1]p(2\beta - 1) \\
    & + \frac{\kappa_0 j_2}{2}(2\beta-1)^2 + \mathcal{O}(|\gn|^6). \nonumber
\end{align}
We make similar replacements as before,
\begin{align}
  (2\beta - 1)p &= \frac{10}{9}p q - \frac{85}{54}p^2  + \mathcal{O}(|\gn|^6) \\
  (2\beta - 1)^2 &= \frac{100}{81}q^2 - \frac{850}{243}p q + \frac{7225}{2916}p^2 + \mathcal{O}(|\gn|^6),
\end{align}
yielding
\begin{align}
  F\sx &= 1 + (h_1 + g_1)p + \kappa_0 j_1(2\beta - 1)
    + \frac{1}{2} \left\{ h_2 + g_2 + 2 h_1 g_1
      - \frac{85}{27} [\kappa_0 j_1 g_1 - h_1 j_1] + \frac{7225}{2916} \kappa_0 j_2
    \right\}p^2 \nonumber \\
    & + \left\{ \frac{10}{9}[\kappa_0 j_1 g_1 - h_1 j_1] - \frac{425}{243} \kappa_0 j_2 \right\} p q
     + \frac{50}{81} \kappa_0 j_2 q^2 + \mathcal{O}(|\gn|^6).
\end{align}
Last, insert Eq. \ref{eq:beta_ge_o4} for the term linear in $2\beta-1$,
\begin{align}
  F\sx &= 1 + \frac{10}{9}\kappa_0 j_1 q + \left(h_1 + g_1 - \frac{85}{54}\kappa_0 j_1 \right)p
    + \left(\frac{50}{81} j_2 - \frac{32}{27}j_1\right)\kappa_0 q^2 \\
  & + \left\{ \frac{10}{9}[\kappa_0 j_1 g_1 - h_1 j_1] - \frac{425}{243} \kappa_0 j_2
    + \frac{773}{486}\kappa_0 j_1 \right\} p q
    + \frac{\kappa_0 j_1}{2} \frac{\widetilde{\tau}_4}{\tau_0} \nonumber \\
  & + \frac{1}{2} \left\{ h_2 + g_2 + 2 h_1 g_1
    - \frac{85}{27} [\kappa_0 j_1 g_1 - h_1 j_1] + \frac{7225}{2916} \kappa_0 j_2
    + \frac{473}{1458}\kappa_0 j_1 \right\} p^2 \nonumber
    + \mathcal{O}(|\gn|^6).
\end{align}
Mercifully, we can reuse Eq. \ref{eq:q_to_p_div_thm} to eliminate the term linear in $q$ to one linear in $p$, and Eq. \ref{eq:t4_tilde_div_thm} to eliminate $\widetilde{\tau_4}$,
and Eq. \ref{eq:t4_tilde_div_thm} to eliminate $\widetilde{\tau_4}$,
\begin{align}
  F\sx &= 1 + \left(h_1 + g_1 - \frac{65}{54}\kappa_0 j_1 \right)p
    + \left(\frac{50}{81} j_2 - \frac{98}{81}j_1\right)\kappa_0 q^2 \\
  & + \left\{ \frac{10}{9}[\kappa_0 j_1 g_1 - h_1 j_1] - \frac{425}{243} \kappa_0 j_2
    + \frac{391}{243}\kappa_0 j_1 \right\} p q \nonumber \\
  & + \frac{1}{2} \left\{ h_2 + g_2 + 2 h_1 g_1
    - \frac{85}{27} [\kappa_0 j_1 g_1 - h_1 j_1] + \frac{7225}{2916} \kappa_0 j_2
    + \frac{461}{1458}\kappa_0 j_1 \right\} p^2 \nonumber \\
  & + n^{-4/3} \n \cdot\left\{ \frac{10}{9}\kappa_0 j_1 \frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}}
    + \frac{\kappa_0 j_1}{2592(3\pi^2)^{4/3}} \left[12 \frac{\n( \lan)}{n}
    - 18\frac{\lan \gn}{n^2} -7\frac{\n |\gn|^2}{n^2} \right. \right. \nonumber \\
  & \left. \left. + \frac{50}{3}\frac{|\gn|^2 \gn}{n^3} \right]
  \right\} + \mathcal{O}(|\gn|^6). \nonumber
\end{align}
We then identify the integrated by parts gradient expansion of $F\sx$,
\begin{align}
  \widetilde{F}\sx &= 1 + \left(h_1 + g_1 - \frac{65}{54}\kappa_0 j_1 \right)p
    + \left(\frac{50}{81} j_2 - \frac{98}{81}j_1\right)\kappa_0 q^2 \\
  & + \left\{ \frac{10}{9}[\kappa_0 j_1 g_1 - h_1 j_1] - \frac{425}{243} \kappa_0 j_2
    + \frac{391}{243}\kappa_0 j_1 \right\} p q \nonumber \\
  & + \frac{1}{2} \left\{ h_2 + g_2 + 2 h_1 g_1
    - \frac{85}{27} [\kappa_0 j_1 g_1 - h_1 j_1] + \frac{7225}{2916} \kappa_0 j_2
    + \frac{461}{1458}\kappa_0 j_1 \right\} p^2 \nonumber
\end{align}
which can be used to constrain the derivatives of $h\sx$, $g\sx$, and $j\sx$ to recover the second or fourth-order gradient expansion for exchange, and the gauge function
\begin{align}
  \bm{G}_{\beta\mathrm{x}} &= \frac{10}{9}\kappa_0 j_1 \frac{\gn}{4(3\pi^2)^{2/3}n^{1/3}}
    + \frac{\kappa_0 j_1}{2592(3\pi^2)^{4/3}} \left[12 \frac{\n( \lan)}{n}
    - 18\frac{\lan \gn}{n^2} -7\frac{\n |\gn|^2}{n^2} \right. \nonumber \\
  & \left. + \frac{50}{3}\frac{|\gn|^2 \gn}{n^3} \right]
\end{align}
such that $F\sx = \widetilde{F}\sx + n^{-4/3} \n \cdot \bm{G}_{\beta\mathrm{x}}$.
