\contentsline {section}{\numberline {1}Modified kinetic energy density-gradient expansion }{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Spin resolution}{5}{subsection.1.1}%
\contentsline {section}{\numberline {2}Relationship to enhancement factors }{6}{section.2}%
\contentsline {subsection}{\numberline {2.1}Gradient expansion and spin resolution of $\overline {\alpha }$}{6}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}$\overline {\alpha }$-dependent exchange enhancement factor}{7}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Simplifying this expression: divergence theorem}{8}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Alternate iso-orbital indicators}{12}{subsection.2.4}%
\contentsline {subsubsection}{\numberline {2.4.1}Gradient expansion and spin resolution of $\beta $}{12}{subsubsection.2.4.1}%
\contentsline {subsubsection}{\numberline {2.4.2}$\beta $-dependent exchange enhancement factor}{13}{subsubsection.2.4.2}%
\contentsline {section}{\numberline {3}Comparisons to other functionals}{14}{section.3}%
\contentsline {subsection}{\numberline {3.1}TASK}{14}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Higher order gradient expansion of r$^2$SCAN}{15}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Higher order gradient expansion of the meta-GGA Made Very Simple}{16}{subsection.3.3}%
\contentsline {section}{\numberline {4}The fourth-order gradient expansion for exchange }{17}{section.4}%
\contentsline {subsection}{\numberline {4.1}r$^2$SCAN-style meta-GGA}{18}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}MVS-style meta-GGA}{19}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Compromise approach}{19}{subsection.4.3}%
\contentsline {section}{\numberline {5}The minimally-modified r$^2$SCAN model}{20}{section.5}%
\contentsline {subsection}{\numberline {5.1}Exchange}{22}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Correlation}{22}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}The fitted model}{24}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Note about XC potential for H atom}{25}{subsection.5.4}%
\contentsline {section}{\numberline {6}Fitting procedure, and initial guess for parameters}{26}{section.6}%
\contentsline {section}{\numberline {7}Summary of Equations}{27}{section.7}%
\contentsline {subsection}{\numberline {7.1}Exchange}{28}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Correlation}{29}{subsection.7.2}%
