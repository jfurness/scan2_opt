\documentclass[12pt]{article}

\usepackage[margin=0.75in]{geometry}
\usepackage{amsmath,amssymb,bm,graphicx,caption,subcaption}
\usepackage{xspace}
\usepackage[linktocpage=true]{hyperref}

\numberwithin{equation}{section}

\newcommand{\rs}{r_{\mathrm{s}}}
\newcommand{\kf}{k_{\mathrm{F}}}
\newcommand{\n}{\nabla}
\newcommand{\nn}{\nabla^2}
\newcommand{\gn}{\n n}
\newcommand{\lan}{\nn n}
\newcommand{\ba}{\overline{\alpha}}
\newcommand{\br}{\bm{r}}
\newcommand{\sx}{_{\mathrm{x}}}
\newcommand{\sxc}{_{\mathrm{xc}}}
\newcommand{\sus}{_\mathrm{s}}
\newcommand{\mc}{_{\mathrm{c}}}
\newcommand{\nup}{n_{\uparrow}}
\newcommand{\ndn}{n_{\downarrow}}
\newcommand{\rrscan}{r$^2$SCAN\xspace}

\begin{document}


\title{The kinetic energy density-gradient expansion and enhancement factors}
\author{Aaron Kaplan}
\date{\today}

\maketitle

\tableofcontents

\section*{The Takeaway}

r$^2$SCAN, at present, seems to be nearly as accurate as SCAN, for a generally much lower computational cost. That has already made it appealing to research groups with high throughput workflows. From a computational perspective, r$^2$SCAN is optimized sufficiently.

From a theoretical perspective, we should aim to restore the fourth-order density gradient expansion for exchagne to r$^2$SCAN. It may even be possible to retain the structure of $h\sx^1(p)$ from r$^2$SCAN and only modify the interpolation function $f\sx(\ba)$ to accomplish this.

Section \ref{sec:ke_ge4} lays the ground-work to show how the unwieldy fourth-order gradient expansion for the kinetic energy density can be rewritten in terms of polynomials in the dimensionless gradient $p$ and Laplacian $q$, plus a divergence term.

Section \ref{sec:fx} demonstrates how that divergence term may be eliminated in favor of $p$ and $q$, yielding an integrated-by-parts expression for $F\sx$ \emph{only} in terms of a polynomial in $p$ and $q$.

Section \ref{sec:new_fns} links this polynomial to the gradient expansion for exchange, and proposes new, completely smooth interpolation functions that could be used in a successor to r$^2$SCAN.

\input{./sections/tau_ge4.tex}

\section{Relationship to enhancement factors \label{sec:fx}}

\input{./sections/ba_ge_fx.tex}

\input{./sections/beta_ge_fx.tex}

\section{Comparisons to other functionals}

\input{./sections/comp_to_dfas.tex}

\section{The fourth-order gradient expansion for exchange \label{sec:new_fns}}

The terms in $\widetilde{F}\sx$ are constrained by the known gradient expansion,
\begin{equation}
  F\sx \to 1 + \mu_{\text{AK}}p + \frac{146}{2025}q^2 - \frac{73}{405}p q + D p^2 + \mathcal{O}(|\gn|^6).
\end{equation}
where $\mu_{\text{AK}} = 10/81$ and $D \approx 0$. Thus the coefficients in $\widetilde{F}\sx$ are constrained by
\begin{align}
  \mu_{\text{AK}}  &= h_1 + g_1 - \frac{5(4 + 9\eta)\kappa_0}{27}f_1  \label{eq:cons_p} \\
  \frac{146}{2025}&= \frac{4\kappa_0}{81}\left[f_1 + 50 f_2 \right] \label{eq:cons_qq}\\
  \frac{73}{405} &= \frac{20}{9}h_1 f_1 - \frac{20}{9}\kappa_0 f_1 g_1
  + \frac{100 (8 + 9\eta)\kappa_0}{243}f_2
  + \frac{2+100\eta}{27} \kappa_0 f_1 \label{eq:cons_pq}\\
  D &= \frac{h_2}{2} + \frac{g_2}{2} + h_1 g_1
  + \frac{25(8 + 9\eta)^2\kappa_0}{1458}f_2
  + \frac{5(8+9\eta)}{27}( h_1 f_1 - \kappa_0 f_1 g_1) & \label{eq:cons_pp} \\
  & \quad + \frac{6 + 75 \eta (8 + 9 \eta)}{243}\kappa_0 f_1. \nonumber
\end{align}
This system of equations is underdetermined: we have six free parameters, $\{ f_1,f_2,h_1,h_2,g_1,g_2\}$, but only four equations linking them.
We then have a few options:
\begin{enumerate}
  \item Make an r$^2$SCAN-style meta-GGA, where our free parameters are $\{f_1,f_2,h_1,h_2\}$ and set $g_1=g_2=0$.
  \item Make an MVS-style meta-GGA, where our free parameters are $\{f_1,f_2,g_1,g_2\}$ and set $h_1=h_2=0$.
  \item Pursue a compromise approach.
    This would entail fixing two nonzero values of the $g_i$ and $h_j$, likely by constraints.
    For example, $g_1$ and $g_2$ can be fixed by the constraint(s) of the hydrogen atom and/or quasi-two dimensional electron gas exchange energies.
\end{enumerate}
The next few sections detail how these constructions work in practice

%\clearpage
\subsection{r$^2$SCAN-style meta-GGA}

In an r$^2$SCAN-style meta-GGA,
\begin{equation}
  g\sx(p) = 1 + \mathcal{O}(|\nabla n|^M)
\end{equation}
with $M \geq 6$. Then $g_1=g_2=0$, and the equations \ref{eq:cons_p}--\ref{eq:cons_pp} can be solved as
\begin{align}
  h_1 &= \frac{10}{81} + \frac{5(4 + 9\eta)}{27}\kappa_0 f_1 \label{eq:dhx_roots}\\
  f_2 &= \frac{73}{2500\kappa_0} - \frac{1}{50}f_1 \label{eq:ddfx_roots} \\
  h_2 &= 2D - 2\left\{ \kappa_0 f_1\frac{6 + 75 \eta (8 + 9 \eta)}{243} + f_1 h_1\frac{5(8 + 9\eta)}{27} + \kappa_0 f_2\frac{25}{1458}(8 + 9 \eta)^2 \right\}. \label{eq:ddhx_roots}
\end{align}
Inserting the substitions of Eqs. \ref{eq:dhx_roots} and \ref{eq:ddfx_roots} into Eq. \ref{eq:cons_pq}, we find two possible solutions for $f_1$
\begin{align}
  %0 &= \frac{100(4 + 9 \eta)}{243}[f_1]^2 + \frac{200 + 6\kappa_0(1 + 441 \eta)}{729}f_1 + \frac{657\eta - 511}{6075}\\
  f_1^{\pm} &= \frac{1}{2f_a}\left[-f_b \pm \sqrt{f_b^2 - 4 f_a f_c} \right] \label{eq:dfx_roots}\\
  f_a &=\frac{100(4 + 9 \eta)\kappa_0}{243}\\
  f_b &= \frac{200 + 6\kappa_0(1 + 441 \eta)}{729}\\
  f_c &= \frac{657\eta - 511}{6075}
\end{align}
Note that, for a parameter set to be meaningful, $f_1<0$ to ensure that the interpolation function is monotone decreasing.
In r$^2$SCAN, $f\sx(\ba)$ was determined {\it a priori}, and thus $\eta$ and a parameter in $h\sx(p)$ were determined by fitting, and the rest of $h\sx(p)$ by constraints. Something similar can be effected here: $\eta$ determines $f_1 = f\sx'(1)$ (the roots $f_1^{\pm}$), which in turn determines $f_2$, $h_1$, and $h_2$.

Note however, that for most typical values of $0.001 \leq \eta \leq 0.01$, $h_1<0$.
SCAN and r$^2$SCAN both have $h_1>0$ (although $h_1$ is very small in r$^2$SCAN), whereas TASK has $h_1<0$.
For example, when $\eta=0.001$,
\begin{align}
  f_1 &= -1.205624 \\
  f_2 &= 0.191929 \\
  h_1 &= -0.032284 \\
  h_2 &= -0.177521.
\end{align}
This may make it challenging to satisfy the appropriate norms.

\iffalse
For example, suppose
\begin{equation}
  h\sx(p) = \frac{1 + h_1 p + h_2 p^2 + (1+ \kappa_1)(p/d_p)^3}{1 + (p/d_p)^3} \label{eq:x_pos},
\end{equation}
which has no poles. Note that $h\sx'(0)$ and $h\sx''(0)$ are constrained by Eqs. \ref{eq:dhx_roots} and \ref{eq:ddhx_roots}.
$d_p$ controls how quickly $h\sx$ approaches its asymptotic limit.

We also need to revise the r$^2$SCAN interpolation function by replacing it with a completely smooth, pole-free function such as
\begin{align}
  f\sx(\ba) &= (1-\ba)\frac{1 + c_1 \ba + c_2 \ba^2 + d\sx \ba^3 }{1 + \ba^4}
 \label{eq:ief_simple} \\
 c_1 &= -2 + f_2 + d\sx \\
 c_2 &= 1 -2 f_1 - f_2 - 2d\sx
\end{align}
which satisfies $f(0)=1$, $f(1)=0$, and $f(\infty)=-d\sx$. Note that $f\sx'(1)$ and $f\sx''(1)$ are constrained by Eqs. \ref{eq:dfx_roots} and \ref{eq:ddfx_roots}.

To round out the construction, we could also fit this smooth form to the r$^2$SCAN correlation interpolation function; then no piecewise functions are used. We can take $f\mc'(1) = -0.711402$, $f\mc''(1)=1.66889$, and $d\mc=0.7$ from the r$^2$SCAN interpolation function as input to Eq. \ref{eq:ief_simple}.
\fi


\subsection{MVS-style meta-GGA}

In an MVS-style meta-GGA, $h\sx(p)=1$, then $h_1=h_2=0$ and Eqs. \ref{eq:cons_p}--\ref{eq:cons_pp} can be solved as
\begin{align}
  g_1 &= \frac{10}{81} + \frac{5(4 + 9\eta)}{27}\kappa_0 f_1 \\
  f_2 &= \frac{73}{2500\kappa_0} - \frac{1}{50}f_1 \\
  g_2 &= 2D - 2\left\{ \kappa_0 f_1\frac{6 + 75 \eta (8 + 9 \eta)}{243} - f_1 g_1\frac{5(8 + 9\eta)k_0}{27} + \kappa_0 f_2\frac{25}{1458}(8 + 9 \eta)^2 \right\}  \\
  %0 &= -\frac{100(4 + 9 \eta)k_0^2}{243}[f_1]^2 + \left(\frac{882 \eta}{243} - \frac{194}{729}\right)f_1 + \frac{73(9 \eta - 7)}{6075}\\
  f_1^{(\pm)} &= \frac{1}{2f_a}\left[-f_b \pm \sqrt{f_b^2 - 4 f_a f_c} \right] \label{eq:mvs_f1_sols} \\
  f_a &= -\frac{100(4 + 9 \eta)k_0^2}{243} \\
  f_b &= \frac{882 \eta}{243} - \frac{194}{729}\\
  f_c &= \frac{73(9 \eta - 7)}{6075}.
\end{align}
However, it is readily seen that $f_1$ is complex for many values of $\eta$.
The two possible solutions $f_1^+$ and $f_1^-$ of Eq. \ref{eq:mvs_f1_sols} are plotted in Fig. \ref{fig:mvs_f1} as a function of $\eta$.
The roots of $f_b^2 - 4 f_a f_c$ are
\begin{align}
  \eta_a &= \frac{10772}{729} \\
  \eta_b &= -\frac{48536}{19683} \\
  \eta_c &= - \frac{256700}{531441} \\
  \eta_\pm &= \frac{-\eta_b \pm \sqrt{\eta_b^2 - 4\eta_a\eta_c}}{2\eta_a}
    \approx \left\{ \begin{array}{rl} 0.282566, & + \\ -0.115686, & - \end{array} \right.
\end{align}
Thus only for values of $\eta \geq \eta_+$ are all three parameters real, however, $f_1 >0$ for $\eta \geq \eta_+$.
This is inconsistent with our construction principles, which require $f_1<0$ to have a monotonically decreasing interpolation function.
This model is intended to be instructive only.

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{mvs_style_f1.pdf}
  \caption{First derivative of $f\sx(\ba)$, Eq. \ref{eq:mvs_f1_sols}, in a MVS-style meta-GGA recovering the fourth-order gradient expansion for exchange, as a function of $\eta$.
  \label{fig:mvs_f1}}
\end{figure}

\textbf{We cannot make an MVS-style meta-GGA that recovers the fourth-order gradient expansion for exchange.}


\subsection{Compromise approach}

In a compromise approach, we pick two of the six free parameters to be control parameters, and vary these with $\eta$ to determine the other four parameters according to Eqs. \ref{eq:cons_p}--\ref{eq:cons_pp}.
There are then $6!/(4! \, 2!)=15$ ways to select control parameters.

Realistically, it's easiest to view $g_1$ and $g_2$ as control parameters, because the $g\sx(p)$ function is determined before any value of $\eta$ is input to the routine.
That $\eta$-independent flexibility means that we are better off determining $g\sx(p)$ first, then using its derivatives to constrain $\{f_1,f_2,h_1,h_2\}$.
We consider two possibilities for $g\sx$
\begin{align}
  g\sx(p) &= [1 + a\sx p]^{-1/4} = 1 - \frac{a\sx}{4}p + \frac{5a\sx^2}{32}p^2
    + \mathcal{O}(|\nabla n|^6) \\
  g\sx(p) &= [1 + a\sx p^2]^{-1/8} = 1 - \frac{a\sx}{8}p^2 + \mathcal{O}(|\nabla n|^6),
\end{align}
which are the simplest two ways to enforce the nonuniform scaling constraint for exchange.
$a\sx$ is fitted to the hydrogen atom exchange energy.
In the first example, $a\sx = 0.0274478$, so $g_1 = -0.00686194$ and $g_2 = 0.000235431$.
In the second example, $a\sx = 0.0232781$, so $g_1=0$ and $g_2=-0.00581952$.

When viewed as a function of $g_1$ and $\eta$, the maximum possible value of $h_1$ consistent with the gradient expansion constraint is $h_1=-0.0318862$ when $g_1=-0.0121037$ and $\eta=0$.
\textbf{Therefore, $h_1$ cannot be made positive in this model framework.}


This can also be demonstrated by considering $\{h_1,h_2\}$ as control parameters.
This example shows that it is impossible to have $h_1>0$ and $f_1<0$ while still recovering the fourth-order gradient expansion for exchange, at least in the framework established here.
Figure \ref{fig:f1_var_h1_eta} shows this for $\eta \in \{0.0,0.001,0.01\}$.
The results, and general shapes of the possible solutions $f_1^\pm$, are not so sensitive to $\eta$ when $\eta \leq 0.01$.

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{compromise_f1_eta=0.0.pdf}
    \caption{$\eta=0$, i.e. $\ba \to \alpha$.
    \label{fig:f1_var_h1_eta0}}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{compromise_f1_eta=0.001.pdf}
    \caption{
    $\eta=0.001$, as it is in r$^2$SCAN.
    \label{fig:f1_var_h1_eta001}}
  \end{subfigure}

  \begin{subfigure}[b]{0.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{compromise_f1_eta=0.01.pdf}
    \caption{$\eta=0.01$.
    \label{fig:f1_var_h1_eta01}}
  \end{subfigure}
  \caption{Using $h_1$ and $h_2=0$ as control parameters, we see that there is no possible value of $h_1>0$ such that $f_1<0$, and importantly, with $f_1$ real-valued.
  Note that the value of $h_2$ only modulates $g_2$.
  \label{fig:f1_var_h1_eta}}
\end{figure}

\section{The minimally-modified r$^2$SCAN model}

\input{./sections/mmr2.tex}

\subsection{Note about XC potential for H atom}

For a fully spin-polarized one-electron system, we can compute the XC potential of a meta-GGA without needing to compute the OEP.
That is because $\tau = \tau_W = |\nabla n|^2/(8n)$ for such a system.
Then, if the system is fully spin-polarized along the positive $\hat{\bm{z}}$ axis,
\begin{equation}
  v\sxc^{(\uparrow)}(\bm{r}) = \frac{\partial e\sxc}{\partial n}
  - \nabla \cdot \left(\frac{\nabla n}{|\nabla n|}\frac{\partial e\sxc}{\partial |\nabla n|}  \right),
\end{equation}
where
\begin{align}
  \frac{\partial e\sxc}{\partial n} &= \frac{\partial e\sxc}{\partial n}\bigg|_{\tau\to \tau_W} +  \left(\frac{\partial e\sxc}{\partial \tau}\right)_{\tau\to \tau_W} \frac{\partial \tau_W}{\partial n} \\
  \frac{\partial e\sxc}{\partial |\nabla n|} &= \frac{\partial e\sxc}{\partial |\nabla n|}\bigg|_{\tau\to \tau_W} + \left(\frac{\partial e\sxc}{\partial \tau}\right)_{\tau\to \tau_W} \frac{\partial \tau_W}{\partial |\nabla n|}.
\end{align}
On the RHS side of the equations, the derivatives are to be evaluated without assuming a value of $\tau$, and then computed by assigning $\tau=\tau_W$.
These are easily evaluated using
\begin{align}
  \frac{\partial \tau_W}{\partial n} &= -\frac{\tau_W}{n} \\
  \frac{\partial \tau_W}{\partial |\nabla n|} &= \frac{|\nabla n|}{4n}.
\end{align}
This seems to remedy the issue where $v\sxc^{(\uparrow)}(\bm{r})$ would be positive at large $r$ using $\eta\approx 0.001$.

This represents one order of limits.
In general, the meta-GGA potential is a non-local operator on the Kohn-Sham orbitals $\phi_{i,\sigma}$
\begin{equation}
  v\sxc^{(\sigma)} \phi_{i,\sigma} = \frac{\partial e\sxc}{\partial n_\sigma}\phi_{i,\sigma}
  - \phi_{i,\sigma} \, \nabla \cdot \left(\frac{\nabla n_\sigma}{|\nabla n_\sigma|}
  \frac{\partial e\sxc}{\partial |\nabla n_\sigma|} \right)\
  - \frac{1}{2}\nabla \cdot \left(\frac{\partial e\sxc}{\partial \tau_\sigma} \nabla \phi_{i,\sigma}\right)
\end{equation}
We now show that this potential reduces to the GGA-level potential described above.
For ease of writing, let $\mu\sxc \equiv \partial e\sxc/\partial \tau$.
For a completely spin-polarized one-electron system, the only occupied orbital is $\phi = n^{1/2}$, and
\begin{align}
  - \frac{1}{2}\nabla \cdot (\mu\sxc \nabla \phi) &=
    - \frac{1}{4}\nabla \cdot (\mu\sxc n^{-1/2}\nabla n) \\
  &= - \frac{1}{4}\nabla \cdot (n^{1/2} \mu\sxc n^{-1} \nabla n) \\
  &= - \frac{1}{4}\nabla (n^{1/2}) \cdot (\mu\sxc n^{-1} \nabla n)
    - \frac{n^{1/2}}{4}\nabla \cdot (\mu\sxc n^{-1} \nabla n) \\
  &= \left[-\mu\sxc \frac{|\nabla n|^2}{8n^2}
    - \frac{1}{4}\nabla \cdot ( \mu\sxc n^{-1} \nabla n) \right]n^{1/2} \\
  &= \left[\mu\sxc \frac{\partial \tau_W}{\partial n}
    - \nabla \cdot \left( \mu\sxc \frac{\partial \tau_W}{\partial \nabla n}
    \right) \right] \phi
\end{align}
This is the anticipated GGA-level contribution to the XC potential (the terms in square brackets) multiplying $\phi$.
Thus the meta-GGA XC potential reduces to a GGA potential for a fully-spin polarized one-electron system.

\section{Fitting procedure, and initial guess for parameters}

Here's how a potential fit might go:
\begin{enumerate}
  \item Select a set of parameters $(\eta,\kappa_1,d\sx)$.
  \item Solve Eq. \ref{eq:dfx_roots} for the two possible roots $f'\sx(1)$. Discard any solution with $f'\sx(1)>0$, or where $f_b^2 - 4 f_a f_c < 0$.
  \item From Eq. \ref{eq:ddfx_roots}, determine $f''\sx(1)$.
  \item From Eq. \ref{eq:dhx_roots}, determine $h\sx'(0)$.
  \item From \ref{eq:ddhx_roots}, determine $h\sx''(0)$.
  \item Compute appropriate norm errors from these possible parameterizations.
\end{enumerate}

Consider then the set $\kappa_1=0.065$, $\eta = 0.001$, and $d=1.24$, unchanged from r$^2$SCAN. These yield $f\sx'(1) =-1.20562$, $f\sx''(1)=0.191929$, $c_p=-0.0322844$, and $c_{pp}=-0.177521$.

Compare this to the r$^2$SCAN value, $df\sx^{\text{r$^2$SCAN}}(1)/d\ba = -0.93530$. We may find a better description of bandgaps with a larger slope near $\ba=1$.

\input{./sections/xc_eqs_first_attempt.tex}

\bibliographystyle{unsrt}
\bibliography{\jobname}


\end{document}
