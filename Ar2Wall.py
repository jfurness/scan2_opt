import os
import numpy as np
from subprocess import call
from scipy import stats
import itertools


def compile_tm(xparams, cparams):
    with open('/home/jfurness/TURBOMOLE/dftlib/scanx.tmplt', 'r') as tmp:
        with open('/home/jfurness/TURBOMOLE/dftlib/xscan.f', 'w') as of:
            s = tmp.read()
            s = s.replace("K1REP", str(xparams['K1']))
            s = s.replace("C1XREP", str(xparams['C1X']))
            s = s.replace("C2XREP", str(xparams['C2X']))
            s = s.replace("DXREP", str(xparams['DX']))
            of.write(s)
    with open('/home/jfurness/TURBOMOLE/dftlib/scanc.tmplt', 'r') as tmp:
        with open('/home/jfurness/TURBOMOLE/dftlib/cscan.f', 'w') as of:
            s = tmp.read()
            s = s.replace("C1CREP", str(cparams['C1C']))
            s = s.replace("C2CREP", str(cparams['C2C']))
            s = s.replace("DCREP", str(cparams['DC']))
            of.write(s)
    call("cd /home/jfurness/TURBOMOLE/dftlib; rm -r em64t-unknown-linux-gnu; make > compile.log 2>&1", shell=True)
    call("cd /home/jfurness/TURBOMOLE/dscf; rm -r em64t-unknown-linux-gnu; make > compile.log 2>&1", shell=True)
    return

def run_tm(r, bas, idx):
    rundir = "/lustre/project/jsun/JFURNESS/Jobs/11_28_SCAN2_updated/{0}/{1}/{2}".format(idx, bas, r)
    call("mkdir /lustre/project/jsun/JFURNESS/Jobs/11_28_SCAN2_updated/{0}".format(idx), shell=True)
    call("mkdir /lustre/project/jsun/JFURNESS/Jobs/11_28_SCAN2_updated/{0}/{1}".format(idx, bas), shell=True)
    base = "/lustre/project/jsun/JFURNESS/Jobs/11_28_SCAN2_updated/base"

    call("source ~/.bash_profile; cd {0}/{1}/{2}; cpc {3}; cd {3}; dscf > dscf.out;".format(base, bas, r, rundir), shell=True)
    return rundir

def analyse_tmout(out):
    with open(out, 'r') as of:
        lines = of.readlines()
        return float([_f for _f in lines[-2].strip().split(' ') if _f][1])*627.509

def jsextr(zs, ens):
    x = np.power(zs,-3.0)
    poly = np.polyfit(x, ens, 1)
    return poly[1]  # Return only intercept

def get_ar2_error(xparams, cparams, run_idx, candidate):
    xparams['K1'] = candidate['K1']
    xparams['C1X'] = candidate['C1X']
    xparams['C2X'] = candidate['C2X']
    xparams['DX'] = candidate['DX']

    cparams['C1C'] = candidate['C1C']
    cparams['C2C'] = candidate['C2C']
    cparams['DC'] = candidate['DC']

    compile_tm(xparams, cparams)

    a = {}
#    for z in ["DZ", "TZ", "QZ", "5Z"]:
    for z in ["DZ", "TZ", "QZ"]:
        rundir = run_tm("atom", z, run_idx)
        a[z] = analyse_tmout("{:}/energy".format(rundir))

#    cbs_a = jsextr(np.array([2,3,4,5]), np.array([a['DZ'], a["TZ"], a["QZ"], a["5Z"]]))
    cbs_a = jsextr(np.array([2,3,4]), np.array([a['DZ'], a["TZ"], a["QZ"]]))

#    res = {"DZ":{}, "TZ":{}, "QZ":{}, "5Z":{}}
    res = {"DZ":{}, "TZ":{}, "QZ":{}}
#    for z, r in itertools.product(["DZ", "TZ", "QZ", "5Z"], ["1.6", "1.8", "2.0"]):
    for z, r in itertools.product(["DZ", "TZ", "QZ"], ["1.6", "1.8", "2.0"]):
        rundir = run_tm(r, z, run_idx)
        en = analyse_tmout("{:}/energy".format(rundir))
        res[z][r] = en
    with open("{:}/details.txt".format(run_idx),"w") as out:
        out.write("K1, {:}\n".format(str(xparams['K1'])))
        out.write("C1X, {:}\n".format(str(xparams['C1X'])))
        out.write("C2X, {:}\n".format(str(xparams['C2X'])))
        out.write("DX, {:}\n".format(str(xparams['DX'])))
        out.write("C1C, {:}\n".format(str(cparams['C1C'])))
        out.write("C2C, {:}\n".format(str(cparams['C2C'])))
        out.write("DC, {:}\n".format(str(cparams['DC'])))

    ref_cc = {"1.6":127764.6*2.8591e-3, "1.8":69341.6*2.8591e-3, "2.0":36100.4*2.8591e-3}
    ref = {"1.6":128236.0*2.8591e-3, "1.8":69631.6*2.8591e-3, "2.0":36270.3*2.8591e-3}

#    op6 = jsextr(np.array([2,3,4,5]), np.array([res['DZ']["1.6"], res["TZ"]["1.6"], res["QZ"]["1.6"], res["5Z"]["1.6"]]))
#    op8 = jsextr(np.array([2,3,4,5]), np.array([res['DZ']["1.8"], res["TZ"]["1.8"], res["QZ"]["1.8"], res["5Z"]["1.8"]]))
#    tp0 = jsextr(np.array([2,3,4,5]), np.array([res['DZ']["2.0"], res["TZ"]["2.0"], res["QZ"]["2.0"], res["5Z"]["2.0"]]))
    op6 = jsextr(np.array([2,3,4]), np.array([res['DZ']["1.6"], res["TZ"]["1.6"], res["QZ"]["1.6"]]))
    op8 = jsextr(np.array([2,3,4]), np.array([res['DZ']["1.8"], res["TZ"]["1.8"], res["QZ"]["1.8"]]))
    tp0 = jsextr(np.array([2,3,4]), np.array([res['DZ']["2.0"], res["TZ"]["2.0"], res["QZ"]["2.0"]]))
    e1p6 = op6 - 2*cbs_a - ref_cc["1.6"]
    e1p8 = op8 - 2*cbs_a - ref_cc["1.8"]
    e2p0 = tp0 - 2*cbs_a - ref_cc["2.0"]

    return e1p6, e1p8, e2p0


def filter_ar2_wall(candidates, xparams, cparams):
    # Filter out the candidates by the Ar2 test. Biggest K1 first.
    # First set of K1 to pass wins! Need a selection method from amongst those though...

    outlist = []

    run_idx = 0
    done = False
    last_k1 = None

    try:
        os.remove('Ar2.dat')
    except OSError:
        # File is absent, no need to remove
        pass

    # Fixes a problem with calculation running out of time not writing file correctly...
    # Write the header
    with open('Ar2.dat', 'a') as of:
        of.write("k1, c1c, c2c, dc, c1x, c2x, dx, 1.6, 1.8, 2.0, MAE\n")

    # Start with largest (last) k1
    nc = len(candidates)
    for trial in reversed(candidates):
        if done and trial['K1'] == last_k1:
            # continue onwards for the rest of these K1 as order in list is arbitrary for other parameters
            break
        print("Running candidate: {:}".format(nc - run_idx))

        errs = get_ar2_error(xparams, cparams, run_idx, trial)

        mae = sum(map(abs,errs))/float(len(errs))

        run_idx += 1

        with open('Ar2.dat', 'a') as of:
            of.write("{:}, {:}, {:}, {:}, {:}, {:}, {:}, ".format(
                trial['K1'], trial['C1C'], trial['C2C'], trial['DC'], trial['C1X'], trial['C2X'], trial['DX']
                ))
            of.write("Ar2: 1.6: {:}, 1.8: {:}, 2.0: {:}, MAE: {:}\n".format(errs[0], errs[1], errs[2], mae))

        if mae < 1.0:
            trial['Ar2_1.6'] = errs[0]
            trial['Ar2_1.8'] = errs[1]
            trial['Ar2_2.0'] = errs[2]
            trial['Ar2_MAE'] = mae
            outlist.append(trial)
            done = True
        last_k1 = trial['K1']

    return outlist
