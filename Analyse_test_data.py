import numpy as np
import matplotlib.pyplot as plt
from Optimiser import read_QUEST_densities, read_QUEST_restricted_densities, generate_density
from Densities import Atom
import Benchmarks
import Settings
from Jellium import Jelly
from math import pi
import glob

def get_from_dat(a, alpha):
    iso_0 = find_iso_sr(2*a[:, 0], 2*a[:, 2], 2*a[:, 4], alpha)
    iso_1 = find_iso_sr(2*a[:, 1], 2*a[:, 3], 2*a[:, 5], alpha)
    iso = find_iso(a[:, 0]+a[:, 1], a[:, 7], a[:, 4]+a[:, 5], (a[:, 0] - a[:, 1])/(a[:, 0] + a[:, 1]), alpha)

    p_0 = 2*a[:, 2]**2/(4*(3*pi**2)**(2.0/3.0)*a[:, 0]**(8.0/3.0))
    p_1 = 2*a[:, 3]**2/(4*(3*pi**2)**(2.0/3.0)*a[:, 1]**(8.0/3.0))
    p_T = 2*a[:, 7]**2/(4*(3*pi**2)**(2.0/3.0)*(a[:, 0]+a[:, 1])**(8.0/3.0))

    return iso_0, iso_1, iso, p_0, p_1, p_T

def get_den_weight(a):
    wt = a[:, 6]

    return wt*2*a[:, 0], wt*2*a[:, 1], wt*(a[:, 0] + a[:, 1])

def find_iso_sr(d, g, t, alpha):
    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d**(5.0/3.0)
    tauw = (g**2)/(8*d)
    if alpha:
        alpha = (t - tauw)/(tau_unif)
        return alpha
    else:
        beta = (t - tauw)/(t + tau_unif)
        return beta

def find_iso(d, g, t, z, alpha):
    ds_zeta = (np.power(1.0 + z, 5.0/3.0) + np.power(1.0 - z, 5.0/3.0))/2.0
    tau_unif = 0.3*np.power(3*pi**2, 2.0/3.0)*np.power(d, 5.0/3.0)*ds_zeta
    tauw = (g**2)/(8*d)
    if alpha:
        alpha = (t - tauw)/(tau_unif)
        return alpha
    else:
        beta = (t - tauw)/(t + tau_unif)
        return beta

def bin_data(a_list, alpha):
    bins_0 = np.zeros((BIN_COUNT))
    bins_1 = np.zeros((BIN_COUNT))
    bins_T = np.zeros((BIN_COUNT))

    for a in a_list:
        beta_0, beta_1, beta_T = get_from_dat(a, alpha)
        dwt_0, dwt_1, dwt_T = get_den_weight(a) 
        for i in range(BIN_COUNT):    
            idx = np.abs(beta_0 - (i+0.5)*BIN_WIDTH) < BIN_WIDTH
            bins_0[i] += np.sum(dwt_0[idx])
            idx = np.abs(beta_1 - (i+0.5)*BIN_WIDTH) < BIN_WIDTH
            bins_1[i] += np.sum(dwt_1[idx])
            idx = np.abs(beta_T - (i+0.5)*BIN_WIDTH) < BIN_WIDTH
            bins_T[i] += np.sum(dwt_T[idx])

    return bins_0, bins_1, bins_T

def bin_ief(a_list):
    bins_0 = np.zeros((BIN_COUNT))
    bins_1 = np.zeros((BIN_COUNT))
    bins_T = np.zeros((BIN_COUNT))

    for a in a_list:
        beta_0, beta_1, beta_T = get_from_dat(a, False)
        func_0 = (1 - 2*beta_0)*(1 -0.874944949404*beta_0 -8.54101325365*beta_0**2 + 17.1122483033*beta_0**3 + 15.7457172108*beta_0**4)
        func_1 = (1 - 2*beta_1)*(1 -0.874944949404*beta_1 -8.54101325365*beta_1**2 + 17.1122483033*beta_1**3 + 15.7457172108*beta_1**4)
        func_T = (1 - 2*beta_T)*(1 -0.874944949404*beta_T -8.54101325365*beta_T**2 + 17.1122483033*beta_T**3 + 15.7457172108*beta_T**4)
        dwt_0, dwt_1, dwt_T = get_den_weight(a)
        for i in range(BIN_COUNT):
            idx = np.abs(func_0 - ((i+0.5)*BIN_WIDTH + BIN_LOW)) < 0.5*BIN_WIDTH
            bins_0[i] += np.sum(dwt_0[idx])
            idx = np.abs(func_1 - ((i+0.5)*BIN_WIDTH + BIN_LOW)) < 0.5*BIN_WIDTH
            bins_1[i] += np.sum(dwt_1[idx])
            idx = np.abs(func_T - ((i+0.5)*BIN_WIDTH + BIN_LOW)) < 0.5*BIN_WIDTH
            bins_T[i] += np.sum(dwt_T[idx])
    return bins_0, bins_1, bins_T

def autolabel(rects, ax):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.1f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom', size = 8)

def plot_bins(bins, fig, name):
    ax = fig.add_subplot(111)
    bin_x = np.linspace(BIN_LOW, BIN_HI, BIN_COUNT)
    rect = ax.bar(bin_x, bins, width=BIN_WIDTH, align="edge", edgecolor='k', color='g')
    autolabel(rect, ax)
    ax.set_xlabel("$f_x(\\beta)$")
    ax.set_ylabel("$\\sum w(\\vec r)n(\\vec r)$")
    ax.set_xlim([BIN_LOW, BIN_HI])
    fig.suptitle(name)
    plt.savefig(DIR+name.replace(" ","_")+".pdf")
    fig.clear()

DIR = "/Users/james/Documents/19_09_24_analysing_norms/"
BIN_COUNT = 21
BIN_LOW = 0.0
BIN_HI = 1.0
BIN_WIDTH = (1/float(BIN_COUNT))*(BIN_HI - BIN_LOW)

fullist = Benchmarks.BM_LIST
spherical = generate_density(fullist)

non_spherical_dens = read_QUEST_densities([x+".out.plot" for x in Benchmarks.NON_SPHERICAL_LIST])

rest_list_ae6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_AE6]
unrest_list_ae6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_AE6]
rest_list_bh6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_BH6]
unrest_list_bh6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_BH6]
ar2_root = "set_dens/SCAN/Ar2/"

# ae6_dens = read_QUEST_restricted_densities(rest_list_ae6) + read_QUEST_densities(unrest_list_ae6)

# bh6_dens = read_QUEST_restricted_densities(rest_list_bh6) + read_QUEST_densities(unrest_list_bh6)

# ar2_dens = read_QUEST_restricted_densities(glob.glob(ar2_root+"*QZ*.plot"))

jellies = []

for rs in Settings.JELLIUM_RS_LIST:
    j = Jelly(rs)
    idx = j.d > 1e-12
    jellies.append(np.array([
            j.d_0[idx], j.d_1[idx],
            j.grd_0[idx], j.grd_1[idx],
            j.tau_0[idx], j.tau_1[idx],
            j.weights[idx],
            j.g[idx],
            j.lap_0[idx], j.lap_1[idx]
        ]
        ).T)

# print "Doing Spherical Atoms..."
# fig = plt.figure()
# sa_bin_0, sa_bin_1, sa_bin_T = bin_data(spherical, True)
# plot_bins(sa_bin_0, fig, "Spherical Atoms 0")
# plot_bins(sa_bin_1, fig, "Spherical Atoms 1")
# plot_bins(sa_bin_T, fig, "Spherical Atoms T")

# print "Doing AE6..."
# fig.clear()
# ae6_bin_0, ae6_bin_1, ae6_bin_T = bin_data(ae6_dens, True)
# plot_bins(ae6_bin_0, fig, "AE6 0")
# plot_bins(ae6_bin_1, fig, "AE6 1")
# plot_bins(ae6_bin_T, fig, "AE6 T")

# print "Doing BH6..."
# fig.clear()
# bh6_bin_0, bh6_bin_1, bh6_bin_T = bin_data(bh6_dens, True)
# plot_bins(bh6_bin_0, fig, "BH6 0")
# plot_bins(bh6_bin_1, fig, "BH6 1")
# plot_bins(bh6_bin_T, fig, "BH6 T")


# print "Doing Ar2..."
# fig.clear()
# ar2_bin_0, ar2_bin_1, ar2_bin_T = bin_data(ar2_dens, True)
# plot_bins(ar2_bin_0, fig, "Ar2 0")
# plot_bins(ar2_bin_1, fig, "Ar2 1")
# plot_bins(ar2_bin_T, fig, "Ar2 T")

# print "Doing Jellium..."
# fig.clear()
# jel_bin_0, jel_bin_1, jel_bin_T = bin_data(jellies, True)
# plot_bins(jel_bin_0, fig, "Jellium Surfaces 0")
# plot_bins(jel_bin_1, fig, "Jellium Surfaces 1")
# plot_bins(jel_bin_T, fig, "Jellium Surfaces T")

# print "Doing Non-Spherical..."
# fig.clear()
# nsa_bin_0, nsa_bin_1, nsa_bin_T = bin_data(non_spherical_dens, True)
# plot_bins(nsa_bin_0, fig, "Non-Spherical Atoms 0")
# plot_bins(nsa_bin_1, fig, "Non-Spherical Atoms 1")
# plot_bins(nsa_bin_T, fig, "Non-Spherical Atoms T")

# plot_bins(sa_bin_0 + ae6_bin_0 + bh6_bin_0 + ar2_bin_0 + jel_bin_0 + nsa_bin_0, fig, "Total 0")
# plot_bins(sa_bin_1 + ae6_bin_1 + bh6_bin_1 + ar2_bin_1 + jel_bin_1 + nsa_bin_1, fig, "Total 1")
# plot_bins(sa_bin_T + ae6_bin_T + bh6_bin_T + ar2_bin_T + jel_bin_T + nsa_bin_T, fig, "Total T")
fig = plt.figure()

print("Doing Sc")
sc = generate_density(["Sc"])
sc_bin_0, sc_bin_1, sc_bin_T = bin_data(sc, False)
plot_bins(sc_bin_0, fig, "Sc 0")
plot_bins(sc_bin_1, fig, "Sc 1")
plot_bins(sc_bin_T, fig, "Sc T")

raise SystemExit

print("Doing Spherical Atoms...")
sa_bin_0, sa_bin_1, sa_bin_T = bin_ief(spherical)
plot_bins(sa_bin_0, fig, "Spherical Atoms 0")
plot_bins(sa_bin_1, fig, "Spherical Atoms 1")
plot_bins(sa_bin_T, fig, "Spherical Atoms T")

print("Doing AE6...")
fig.clear()
ae6_bin_0, ae6_bin_1, ae6_bin_T = bin_ief(ae6_dens)
plot_bins(ae6_bin_0, fig, "AE6 0")
plot_bins(ae6_bin_1, fig, "AE6 1")
plot_bins(ae6_bin_T, fig, "AE6 T")

print("Doing BH6...")
fig.clear()
bh6_bin_0, bh6_bin_1, bh6_bin_T = bin_ief(bh6_dens)
plot_bins(bh6_bin_0, fig, "BH6 0")
plot_bins(bh6_bin_1, fig, "BH6 1")
plot_bins(bh6_bin_T, fig, "BH6 T")


print("Doing Ar2...")
fig.clear()
ar2_bin_0, ar2_bin_1, ar2_bin_T = bin_ief(ar2_dens)
plot_bins(ar2_bin_0, fig, "Ar2 0")
plot_bins(ar2_bin_1, fig, "Ar2 1")
plot_bins(ar2_bin_T, fig, "Ar2 T")

print("Doing Jellium...")
fig.clear()
jel_bin_0, jel_bin_1, jel_bin_T = bin_ief(jellies)
plot_bins(jel_bin_0, fig, "Jellium Surfaces 0")
plot_bins(jel_bin_1, fig, "Jellium Surfaces 1")
plot_bins(jel_bin_T, fig, "Jellium Surfaces T")

print("Doing Non-Spherical...")
fig.clear()
nsa_bin_0, nsa_bin_1, nsa_bin_T = bin_ief(non_spherical_dens)
plot_bins(nsa_bin_0, fig, "Non-Spherical Atoms 0")
plot_bins(nsa_bin_1, fig, "Non-Spherical Atoms 1")
plot_bins(nsa_bin_T, fig, "Non-Spherical Atoms T")

plot_bins(sa_bin_0 + ae6_bin_0 + bh6_bin_0 + ar2_bin_0 + jel_bin_0 + nsa_bin_0, fig, "Total 0")
plot_bins(sa_bin_1 + ae6_bin_1 + bh6_bin_1 + ar2_bin_1 + jel_bin_1 + nsa_bin_1, fig, "Total 1")
plot_bins(sa_bin_T + ae6_bin_T + bh6_bin_T + ar2_bin_T + jel_bin_T + nsa_bin_T, fig, "Total T")

