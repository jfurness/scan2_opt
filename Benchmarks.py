import numpy
import glob
import Settings
# Benchmarks for atomic exchange and correlation energies
EH_TO_KCAL = 627.5096

BM_X = numpy.array([
    # -0.5129*2,   # He
    -0.5936*3,   # Li
    -0.9435*7,   # N
    -1.2105*10,  # Ne
    -1.2739*11,  # Na
    -1.5089*15,  # P
    -1.6764*18,  # Ar
    -1.7193*19,  # K
    -1.9898*24,  # Cr
    -2.2681*29,  # Cu
    -2.4696*33,  # As
    -2.6065*36,  # Kr
    -3.3160*54   # Xe
    ])
BM_C = numpy.array([
    # 0.0210*2,   # He
    -0.0151*3,   # Li
    -0.0269*7,   # N
    -0.0391*10,  # Ne
    -0.0360*11,  # Na
    -0.0361*15,  # P
    -0.0403*18,  # Ar
    -0.0402*19,  # K
    -0.0446*24,  # Cr
    -0.0546*29,  # Cu
    -0.0510*33,  # As
    -0.0514*36,  # Kr
    -0.0556*54   # Xe
    ])
BM_XC = numpy.array(BM_X)+numpy.array(BM_C)

BM_LIST = ["Li", "N", "Ne", "Na", "P", "Ar", "K", "Cr", "Cu", "As", "Kr", "Xe"]

LZ_X_A = 0.24327
LZ_X_B = -0.22586

LZ_C_B = 0.03877

NON_SPHERICAL_X = [ # Currently taken from agGGA paper. Maybe symmetry broken HF is better?
    -0.7485*5,  # B
    -0.8411*6,  # C
    -1.0226*8,  # O
    -1.1115*9   # F
    ]

NON_SPHERICAL_C = [ # Currently taken from agGGA paper. Maybe symmetry broken CCSD(T) is better?
    -0.0250*5,  # B
    -0.0261*6,  # C
    -0.0322*8,  # O
    -0.0361*9   # F
    ]
NON_SPHERICAL_XC = numpy.array(NON_SPHERICAL_X)+numpy.array(NON_SPHERICAL_C)

NON_SPHERICAL_LIST = ["B", "C", "O", "F"]

HA_X = [
    -6.971801808   # HF in t-aug-cc-pCVQZ
    ]

HA_C = [
    -0.259178904   # E[CCSD(T)] - E[HF] in t-aug-cc-pCVQZ
    ]

HA_XC = numpy.array(HA_X)+numpy.array(HA_C)

HA_LIST = ["Ne[2S2,2P6]"]

SET_NO_XC = {}
if Settings.SCAN2_DENS:
    dname = "set_dens/SCAN2/*.out"
else:
    dname = "set_dens/SCAN/*.out"

for f in glob.glob(dname):
    with open(f, 'r') as inp:
        for line in inp:
            if "E_tot" in line:
                etot = float(line.strip().split(' ')[-1])
            if "E_XC" in line:
                exc = float(line.strip().split(' ')[-1])
        SET_NO_XC[f.split('/')[2].split('.')[0]] = etot - exc

REST_AE6 = ["C2H2O2", "C4H8", "C3H4", "SiH4", "SiO"]
UNREST_AE6 = ["H", "C", "O", "Si", "S", "S2"]

REST_BH6 = ["H2", "H2O", "CH4", "H2S"]
UNREST_BH6 = ["H", "O", "OH", "CH3", "HS", "TS_H_OH_O_H2", "TS_OH_CH4_CH3_H2O", "TS_H_H2S_H2_SH"]

AE6_LIST = REST_AE6+UNREST_AE6
BH6_LIST = REST_BH6+UNREST_BH6

AE6_TESTS = ['SiH4', 'SiO', 'S2', 'C3H4', 'C2H2O2', 'C4H8']
BH6_TESTS = ['F_TS_H_OH_O_H2', 'B_TS_H_OH_O_H2', 'F_TS_OH_CH4_CH3_H2O', 'B_TS_OH_CH4_CH3_H2O', 'F_TS_H_H2S_H2_SH', 'B_TS_H_H2S_H2_SH']

AE6_BM = {
    'SiH4': 322.4,
    'SiO': 192.08,
    'S2': 101.67,
    'C3H4': 704.79,
    'C2H2O2': 633.35,
    'C4H8': 1149.01
}
BH6_BM = {
    'F_TS_H_OH_O_H2': 10.7,
    'B_TS_H_OH_O_H2': 13.1,
    'F_TS_OH_CH4_CH3_H2O': 6.7,
    'B_TS_OH_CH4_CH3_H2O': 19.6,
    'F_TS_H_H2S_H2_SH': 3.5,
    'B_TS_H_H2S_H2_SH': 17.3
}

# Ar2 Benchmarks

AR2_SEPS = ["1.6", "1.8", "2.0"]

AR2_BM = {
    "1.6": 127764.6*2.8591e-3,
    "1.8": 69341.6*2.8591e-3,
    "2.0": 36100.4*2.8591e-3
    }

AR2_NO_XC = {}
AR2_SCAN_EN = {}

dname = "./set_dens/SCAN/Ar2/"

for f in glob.glob(dname+"Ar_*.out"):
    with open(f, "r") as inp:
        for line in inp:
            if "E_tot" in line:
                etot = float(line.strip().split(' ')[-1])*EH_TO_KCAL
            if "E_XC" in line:
                exc = float(line.strip().split(' ')[-1])*EH_TO_KCAL
        fname = f.split('/')[-1].split('_')
        bas = fname[-1][:-4]
        AR2_NO_XC["Ar"+bas] = etot - exc
        AR2_SCAN_EN["Ar"+bas] = etot

for f in glob.glob(dname+"Ar2*.out"):
    with open(f, 'r') as inp:
        for line in inp:
            if "E_tot" in line:
                etot = float(line.strip().split(' ')[-1])*EH_TO_KCAL
            if "E_XC" in line:
                exc = float(line.strip().split(' ')[-1])*EH_TO_KCAL
        fname = f.split('/')[-1].split('_')
        sep = fname[-2]
        bas = fname[-1][:-4]
        AR2_NO_XC[sep+bas] = etot - exc
        AR2_SCAN_EN[sep+bas] = etot

AR2_LIST = list(AR2_NO_XC.keys())

if Settings.JELLIUM_REF_DATA == "TD-DFT":
    JELLIUM_X = numpy.array([2624.0, 526.0, 157.0, 22.0])
    JELLIUM_XC = numpy.array([3466.0, 797.0, 278.0, 58.0])
elif Settings.JELLIUM_REF_DATA == "QMC":
    JELLIUM_X = numpy.array([2624.0, 526.0, 157.0, 22.0]) # <- TD-DFT X
    JELLIUM_XC = numpy.array([3392.0, 768.0, 261.0, 53.0])
JELLIUM_C = JELLIUM_XC - JELLIUM_X