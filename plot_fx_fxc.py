import numpy as np
import matplotlib.pyplot as plt
from os import system

import Settings
from SCAN2 import scanFx,getscan_c,getscan_x
pi = np.pi

clist = ['darkblue','darkorange','darkgreen','darkred','black']
linlist = ['-','--','-.',':']

def exlda(rs):
    return -(3/(4*pi))**2*(9*pi/4)**(1/3)/rs**4

def plot_fx(dir='./'):
    s_l = 10**(np.linspace(-3,2,5000))
    alpha_l = [0.0,0.5,1.0,5.0,1e6]

    fig, ax = plt.subplots(figsize=(6,4))
    bds = [1e20,-1e20]
    for ialpha,alpha in enumerate(alpha_l):
        fx = scanFx(Settings.PLOT_PARS_X, s_l**2, alpha)
        bds[0] = min(bds[0],fx.min())
        bds[1] = max(bds[1],fx.max())
        ax.plot(s_l,fx,label='$\\alpha={:}$'.format(alpha),color=clist[ialpha],\
            linestyle=linlist[ialpha%len(linlist)])
        ax.annotate('$\\alpha={:}$'.format(alpha),(s_l[2]+1.e-3,fx[2]+.015),fontsize=12,\
            color=clist[ialpha])

    ax.set_xlim([s_l[0],s_l[-1]])
    ax.hlines(1.174,*ax.get_xlim(),color='gray',linestyle=':')
    ax.set_ylim([0.95*bds[0],1.1*bds[1]])

    ax.set_xlabel('$s$',fontsize=12)
    ax.set_xscale('log')
    ax.set_ylabel('$F_\\mathrm{x}(p,\\alpha)$',fontsize=12)
    ax.tick_params('both',labelsize=12)

    #ax.legend(fontsize=14)
    #plt.show() ;  exit()
    plt.savefig(dir+'fx.pdf',dpi=600,bbox_inches='tight')

    return

def plot_fxc(dir='./'):

    s_l = 10**(np.linspace(-3,2,5000))
    alpha_l = np.array([0.0,1.0,1e6])#np.array([0.0,0.5,1.0,5.0,1e6])
    zeta_l = [0.0,1.0]
    rs_l = np.linspace(0.1,100,2000)

    s,rs = np.meshgrid(s_l,rs_l)

    ex_lda = exlda(rs)

    n = 3/(4*pi*rs**3)
    gn = 2*(3*pi**2)**(1/3)*n**(4/3)*s
    tau0 = 3/10*(3*pi**2*n)**(2/3)*n
    tauw = 5/3*s**2*tau0

    lap = np.zeros_like(s)

    for alpha in alpha_l:

        tau = tau0*alpha + tauw

        for zeta in zeta_l:

            fig,ax = plt.subplots(figsize=(6,6))
            #fxc = np.zeros_like(s)

            n_up = (1 + zeta)*n/2
            gn_up = (1 + zeta)*n/2 # assume |grad zeta| = 0
            tau_up = (1 + zeta)*tau/2 # hey it's an approximation, you know?

            n_dn = (1 - zeta)*n/2
            gn_dn = (1 - zeta)*n/2 # assume |grad zeta| = 0
            tau_dn = (1 - zeta)*tau/2

            ex = getscan_x(Settings.PLOT_PARS_X, n_up, n_dn, gn_up, gn_dn, tau_up,\
                tau_dn, lap, lap, only_0=(abs(zeta)==1.0), only_Fx=False, unp=(zeta==0.0))

            ec = getscan_c(Settings.PLOT_PARS_C, n, gn, tau, lap, zeta)
            fxc = (ex + ec)/ex_lda

            """
            i = np.unravel_index(np.argmin(fxc),s.shape)
            print(s[i],rs[i],fxc[i])
            i = np.unravel_index(np.argmax(fxc),s.shape)
            print(s[i],rs[i],fxc[i])
            """

            plt.contourf(s,rs,fxc)

            ax.set_yscale('log')
            ax.set_xscale('log')
            ax.set_xlim([s_l[0],s_l[-1]])
            ax.set_ylim([rs_l[0],rs_l[-1]])

            ax.set_xlabel('$s$',fontsize=14)
            ax.set_ylabel('$r_\\mathrm{s}$',fontsize=14)
            ax.tick_params('both',labelsize=12)

            plt.title('$F_\\mathrm{xc}(s,r_\\mathrm{s},'+'\\alpha={:},\\zeta={:})$'\
                .format(alpha,zeta),fontsize=14)
            plt.colorbar()
            plt.savefig(dir+'fxc_a={:}_z={:}.pdf'.format(alpha,zeta),dpi=600,bbox_inches='tight')

            plt.cla()
            plt.clf()
            plt.close()
            #plt.show()
            #exit()

    return


if __name__=="__main__":

    dirstr = './enh/enh_IEF='+Settings.Fint+'_H1X='+Settings.H1X+'_GX='+Settings.GX+'_IND='\
        +Settings.ISOORB+'_C='+Settings.CORRELATION+'_BRS='+Settings.BETARS+'/'

    system('rm -rf {:}'.format(dirstr))
    system('mkdir -p {:}'.format(dirstr))
    plot_fx(dir=dirstr)
    plot_fxc(dir=dirstr)
