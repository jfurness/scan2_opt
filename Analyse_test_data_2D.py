import numpy as np
import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
from Optimiser import read_QUEST_densities, read_QUEST_restricted_densities, generate_density
from Densities import Atom
import Benchmarks
import Settings
from Jellium import Jelly
from math import pi
import glob

def get_from_dat(a, alpha):
    iso_0 = find_iso_sr(2*a[:, 0], 2*a[:, 2], 2*a[:, 4], alpha)
    iso_1 = find_iso_sr(2*a[:, 1], 2*a[:, 3], 2*a[:, 5], alpha)
    iso = find_iso(a[:, 0]+a[:, 1], a[:, 7], a[:, 4]+a[:, 5], (a[:, 0] - a[:, 1])/(a[:, 0] + a[:, 1]), alpha)

    p_0 = 2*a[:, 2]**2/(4*(3*pi**2)**(2.0/3.0)*a[:, 0]**(8.0/3.0))
    p_1 = 2*a[:, 3]**2/(4*(3*pi**2)**(2.0/3.0)*a[:, 1]**(8.0/3.0))
    p_T = 2*a[:, 7]**2/(4*(3*pi**2)**(2.0/3.0)*(a[:, 0]+a[:, 1])**(8.0/3.0))

    return iso_0, iso_1, iso, p_0, p_1, p_T

def get_den_weight(a):
    wt = a[:, 6]

    return wt*2*a[:, 0], wt*2*a[:, 1], wt*(a[:, 0] + a[:, 1])

def find_iso_sr(d, g, t, alpha):
    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d**(5.0/3.0)
    tauw = (g**2)/(8*d)
    if alpha:
        alpha = (t - tauw)/(tau_unif)
        return alpha
    else:
        beta = (t - tauw)/(t + tau_unif)
        return beta

def find_iso(d, g, t, z, alpha):
    ds_zeta = (np.power(1.0 + z, 5.0/3.0) + np.power(1.0 - z, 5.0/3.0))/2.0
    tau_unif = 0.3*np.power(3*pi**2, 2.0/3.0)*np.power(d, 5.0/3.0)*ds_zeta
    tauw = (g**2)/(8*d)
    if alpha:
        alpha = (t - tauw)/(tau_unif)
        return alpha
    else:
        beta = (t - tauw)/(t + tau_unif)
        return beta

def bin_data(a_list, alpha):
    bins_T = np.zeros((ISO_BIN_COUNT, P_BIN_COUNT))

    for a in a_list:
        beta_0, beta_1, beta_T, p_0, p_1, p_T = get_from_dat(a, alpha)
        dwt_0, dwt_1, dwt_T = get_den_weight(a) 
        for i, b in enumerate(beta_T):
            b_idx = None
            for bbin in range(ISO_BIN_COUNT):
                if abs(b - (bbin+0.5)*ISO_BIN_WIDTH) < ISO_BIN_WIDTH:
                    b_idx = bbin
                    break
            
            p_idx = None
            if p_T[i] > P_BIN_HI:
                p_idx = P_BIN_COUNT-1
            else:
                for pbin in range(P_BIN_COUNT):
                    if abs(p_T[i] - (pbin+0.5)*P_BIN_WIDTH) < P_BIN_WIDTH:
                        p_idx = pbin
                        break

            if b_idx is not None and p_idx is not None:
                bins_T[b_idx, p_idx] += dwt_T[i]
            else:
                print("HERE")
                raise SystemExit

    return bins_T

def autolabel(rects, ax):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.1f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom', size = 8)

def plot_bins_3D(bins, fig, name):
    ax = fig.add_subplot(311)
    bin_b = np.arange(ISO_BIN_LOW, ISO_BIN_HI, ISO_BIN_WIDTH)

    bin_p = np.arange(P_BIN_LOW, P_BIN_HI, P_BIN_WIDTH)
    # bin_p = np.hstack([np.arange(P_BIN_LOW, P_BIN_HI, ISO_BIN_WIDTH),[P_BIN_HI+P_BIN_WIDTH]])

    gX, gY = np.meshgrid(bin_p, bin_b)
    # x, y = gX.ravel(), gY.ravel()
    # width = ISO_BIN_WIDTH
    # depth = P_BIN_WIDTH
    # bins = np.reshape(bins, ISO_BIN_COUNT*(P_BIN_COUNT+1))

    # rect = ax.bar3d(x, y, np.zeros_like(bins), width, depth, bins, shade=True, edgecolor='k', color='g')
    # ax.plot(bin_b, np.sum(bins, axis=1))
    # cs = ax.contourf(gX, gY, bins)
    cs = ax.imshow(bins, aspect=P_BIN_HI/ISO_BIN_HI, origin="lower", extent=(P_BIN_LOW, P_BIN_HI, ISO_BIN_LOW, ISO_BIN_HI), interpolation='nearest')
    fig.colorbar(cs)
    ax.set_ylabel("$\\beta$")
    ax.set_xlabel("$p$")
    ax.set_ylim([ISO_BIN_LOW, ISO_BIN_HI])
    ax.set_xlim([P_BIN_LOW, P_BIN_HI])
    ax.set_xticks([0, 1.0, 2.0, 3.0])
    ax.set_xticklabels(["0.0", "1.0", "2.0", "$\\geq 3$"])

    ax = fig.add_subplot(312)
    ax.bar(bin_b, np.sum(bins, axis=1), width=ISO_BIN_WIDTH, align="edge", edgecolor='k', color='b')
    ax.set_xlim([ISO_BIN_LOW, ISO_BIN_HI])
    ax.set_xlabel("$\\beta$")

    ax = fig.add_subplot(313)
    ax.bar(bin_p, np.sum(bins, axis=0), width=P_BIN_WIDTH, align="edge", edgecolor='k', color='r')
    ax.set_xlim([P_BIN_LOW, P_BIN_HI])
    ax.set_xlabel("$p$")
    ax.set_xticks([0, 1.0, 2.0, 3.0])
    ax.set_xticklabels(["0.0", "1.0", "2.0", "$\\geq 3$"])

    fig.suptitle(name)
    fig.tight_layout(rect=[0,0,1.0,0.95])
    # plt.show()
    plt.savefig(DIR+name.replace(" ","_")+".pdf")
    fig.clear()

DIR = "/Users/james/Documents/19_09_24_analysing_norms/with_p/"
ISO_BIN_COUNT = 21
ISO_BIN_LOW = 0.0
ISO_BIN_HI = 1.0
ISO_BIN_WIDTH = (1/float(ISO_BIN_COUNT))*(ISO_BIN_HI - ISO_BIN_LOW)

P_BIN_COUNT = 21
P_BIN_LOW = 0.0
P_BIN_HI = 3.0
P_BIN_WIDTH = (1/float(P_BIN_COUNT))*(P_BIN_HI - P_BIN_LOW)
print(P_BIN_WIDTH)

fullist = Benchmarks.BM_LIST
spherical = generate_density(fullist)

non_spherical_dens = read_QUEST_densities([x+".out.plot" for x in Benchmarks.NON_SPHERICAL_LIST])

rest_list_ae6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_AE6]
unrest_list_ae6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_AE6]
rest_list_bh6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_BH6]
unrest_list_bh6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_BH6]
ar2_root = "set_dens/SCAN/Ar2/"

ae6_dens = read_QUEST_restricted_densities(rest_list_ae6) + read_QUEST_densities(unrest_list_ae6)

bh6_dens = read_QUEST_restricted_densities(rest_list_bh6) + read_QUEST_densities(unrest_list_bh6)

ar2_dens = read_QUEST_restricted_densities(glob.glob(ar2_root+"*QZ*.plot"))

jellies = []

for rs in Settings.JELLIUM_RS_LIST:
    j = Jelly(rs)
    idx = j.d > 1e-12
    jellies.append(np.array([
            j.d_0[idx], j.d_1[idx],
            j.grd_0[idx], j.grd_1[idx],
            j.tau_0[idx], j.tau_1[idx],
            j.weights[idx],
            j.g[idx],
            j.lap_0[idx], j.lap_1[idx]
        ]
        ).T)

print("Doing Spherical Atoms...")
fig = plt.figure(figsize=(5,11))
sa_bin_T = bin_data(spherical, False)
plot_bins_3D(sa_bin_T, fig, "Spherical Atoms T")

print("Doing AE6...")
fig.clear()
ae6_bin_T = bin_data(ae6_dens, False)
plot_bins_3D(ae6_bin_T, fig, "AE6 T")

print("Doing BH6...")
fig.clear()
bh6_bin_T = bin_data(bh6_dens, False)
plot_bins_3D(bh6_bin_T, fig, "BH6 T")


print("Doing Ar2...")
fig.clear()
ar2_bin_T = bin_data(ar2_dens, False)
plot_bins_3D(ar2_bin_T, fig, "Ar2 T")

print("Doing Jellium...")
fig.clear()
jel_bin_T = bin_data(jellies, False)
plot_bins_3D(jel_bin_T, fig, "Jellium Surfaces T")

print("Doing Non-Spherical...")
fig.clear()
nsa_bin_T = bin_data(non_spherical_dens, False)
plot_bins_3D(nsa_bin_T, fig, "Non-Spherical Atoms T")

plot_bins_3D(sa_bin_T + ae6_bin_T + bh6_bin_T + ar2_bin_T + jel_bin_T + nsa_bin_T, fig, "Total T")

# print "Doing Sc"
# sc = generate_density(["Sc"])
# sc_bin_0, sc_bin_1, sc_bin_T = bin_data(sc, False)
# plot_bins(sc_bin_0, fig, "Sc 0")
# plot_bins(sc_bin_1, fig, "Sc 1")
# plot_bins(sc_bin_T, fig, "Sc T")

raise SystemExit