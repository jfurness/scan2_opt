import numpy as np
import Settings
from Integrators import get_Ex_from_lin, get_Ec_from_lin
import Benchmarks

def jsextr(zs, ens):
    x = np.power(zs,-3.0)
    poly = np.polyfit(x, ens, 1)
    return poly[1]  # Return only intercept

def get_energies_from_input_densities():
    print("Getting Ar2 values from input densities.")
    cbs_a = jsextr(np.array([2,3,4]), np.array([Settings.AR2_SCAN_EN['ArDZ'], Settings.AR2_SCAN_EN["ArTZ"], Settings.AR2_SCAN_EN["ArQZ"]]))
    out = {}
    for k in ["1.6","1.8","2.0"]:
        cbs = jsextr(np.array([2,3,4]), np.array([Settings.AR2_SCAN_EN[k+'DZ'], Settings.AR2_SCAN_EN[k+"TZ"], Settings.AR2_SCAN_EN[k+"QZ"]]))
        print(k, cbs - 2*cbs_a, Settings.AR2_BM[k], (cbs - 2*cbs_a - Settings.AR2_BM[k]))
        out[k] = cbs - 2*cbs_a
    return out

def assess_ar2(ar2_lin_exl, ar2_lin_ecl, xparams, cparams, XP, CP):
    assert Settings.DO_AR2, "We didn't load the AE6 densities. We can't evaluate them"

    exl = get_Ex_from_lin(ar2_lin_exl, xparams, XP)*Benchmarks.EH_TO_KCAL
    ecl = get_Ec_from_lin(ar2_lin_ecl, cparams, CP)*Benchmarks.EH_TO_KCAL

    exc = exl + ecl

    etot = {}

    for i, m in enumerate(Benchmarks.AR2_LIST):
        etot[m] = exc[i] + Benchmarks.AR2_NO_XC[m]

    seps = ["1.6", "1.8", "2.0"]

    cbs_a = jsextr(np.array([2,3,4]), np.array([etot['ArDZ'], etot['ArTZ'], etot['ArQZ']]))

    err_l = []
    for s in seps:
        cbs_e = jsextr(np.array([2,3,4]), np.array([etot[s+'DZ'], etot[s+'TZ'], etot[s+'QZ']]))
        err_l.append(cbs_e - 2*cbs_a - Benchmarks.AR2_BM[s])

    return err_l
