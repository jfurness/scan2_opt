from int_ex import opt_c1x_lzlx, get_lzlx_coef, get_Ex_for_atoms
import scipy
import numpy as np
from SCAN2 import interp_gradient
from tqdm import tqdm
from itertools import product

"""
OLD MODULE

We don't fit by the extrapolations any more, so probably safe to ignore this stuff.

"""

def find_c2x_then_c1x(dens, xparams):
    nc2x = 30
    xolist = []

    C2X_START = 0.7
    C2X_RANGE = 0.9

    for i in range(nc2x):
        xparams['C2X'] = C2X_START + i*C2X_RANGE/nc2x

        x_res = {'ncount': 0, 'success': False, 'axcoef':None, 'aerror':None, 'bxcoef': None, 'berror':None, 'C1X':None, 'C2X':None, 'DX':None}
        opt_c1x_lzlx(xparams, x_res, dens, False)

        if x_res['success']:
            xolist.append(x_res)

    return xolist


def x_global_opt(dens, xparams):
    print("Running Nelder-Mead from (1, 1, 1)...")
    res = scipy.optimize.minimize(obj_func, (1, 1), args = (dens, xparams), method='Nelder-Mead', options={'fatol':0.01,'xatol':10})
    print('Best was {:}, {:}, {:} with {:}%'.format(xparams['CAX'], xparams['CBX'], xparams['CCX'], res.fun))   

def obj_func(x, dens, xparams):
    xparams['CAX'] = x[0]
    xparams['CBX'] = x[1]
    xparams['CCX'] = 0.24 - x[0] - x[1]
    d = {}
    err_a, err_b = list(map(abs, get_lzlx_coef(xparams, d, dens, True)))
    print("Trying {:}, {:}, {:} got ({:},{:})".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], err_a, err_b))
    return (err_a+err_b)/2.0


def constrained_search(dens, xparams):
    ERR_TOL = 0.1

    A_MAX = 6
    A_MIN = -6
    A_STEP = 0.125

    B_MIN = 50
    B_MAX = 70
    B_STEP = 0.125

    # F1X_MIN = -1
    # F1X_MAX = -0
    # F1X_STEP = 0.025

    C_MIN = 0
    C_MAX = 20
    C_STEP = 0.25

    beta = np.arange(0,1,0.001)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta2*beta2
    om2b2 = (1-2*beta)**2
    om2b3 = (1-2*beta)**3

    caxs = np.arange(A_MIN, A_MAX, A_STEP)
    cbxs = np.arange(B_MIN, B_MAX, B_STEP)
    ccxs = np.arange(C_MIN, C_MAX, C_STEP)
    # f1xs = np.arange(F1X_MIN, F1X_MAX, F1X_STEP)
    f1xs = [-1.24]
    tot = caxs.shape[0]*cbxs.shape[0]*ccxs.shape[0]
    print("combinations: ",tot)
    possible = product(caxs, cbxs, ccxs, f1xs)

    candidates = []
    mono = []
    d = {}
    for p in tqdm(possible, total=tot):
        # Numerically check monotonicity
        # ccx = -(p[2] + 1 + p[0] + p[1]) SIMPLE POLYNOMIAL!
        # ccx = -((1 + p[0] + p[1])/p[2] + 1)
        a, b, c, f1 = p
        cdx = -(a + b + c**2*f1 + 2*c*f1 + f1 + 1)/f1
        # if np.all(interp_gradient(beta, beta2, beta3, p[0], p[1], ccx) <= 0): SIMPLE POLYNOMIAL
        # if np.all(pade_grad(beta, beta2, beta4, om2b2, om2b3, p[0], p[1], ccx) <= 0):
        if np.all(mod_grad(beta, beta2, beta3, beta4, om2b2, om2b3, a, b, c, cdx) <= 0):
            mono.append(p)

    print("{:} monotonic combinations found".format(len(mono)))
    print("Testing with large Z limit exchange...")
    # Check the remainders for large Z exchange limit
    err_b_min = 1e20
    err_a_min = 1e20

    for p in tqdm(mono):
        xparams['CAX'] = p[0]
        xparams['CBX'] = p[1]
        # xparams['CCX'] = -(p[2] + 1 + p[0] + p[1]) SIMPLE POLYNOMIAL!
        # xparams['CCX'] = -((1 + xparams['CAX'] + xparams['CBX'])/p[2] + 1)
        xparams['CCX'] = p[2]
        xparams['CDX'] = -(xparams['CAX'] + xparams['CBX'] + xparams['CCX']**2*p[3] + 2*xparams['CCX']*p[3] + p[3] + 1)/p[3]
        err_a, err_b = get_lzlx_coef(xparams, d, dens, True)
        if abs(err_a) <= ERR_TOL and abs(err_b) <= ERR_TOL:
            candidates.append({'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'CDX':xparams['CDX'], 'err_a':err_a, 'err_b':err_b})
                # print "ACCEPTED A: {:}, B: {:}, C: {:}. Error: a = {:}%, b = {:}%.".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], err_a, err_b)
        if abs(err_b) < abs(err_b_min) and abs(err_a) < abs(err_a_min):
            best = {'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'CDX':xparams['CDX'], 'err_a':err_a, 'err_b':err_b}
            err_b_min = err_b
            err_a_min = err_a
    print("Best A: {:}, B: {:}, C: {:}, D: {:}. Error: a = {:}%, b = {:}%.".format(best['CAX'], best['CBX'], best['CCX'], best['CDX'], best['err_a'], best['err_b']))
    for s in ["CAX","CBX","CCX","CDX"]:
        xparams[s] = best[s]
    exlist = get_Ex_for_atoms(dens, xparams, True)
    for p in zip(["He", "Ne", "Ar", "Kr", "Xe"], exlist):
        print(p)
    return candidates

def matching_scan_interp(dens, xparams):
    C1X_MIN = 0.2
    C1X_MAX = 0.8
    C1X_STEP = 0.05

    C2X_MIN = 0.1
    C2X_MAX = 1.0
    C2X_STEP = 0.05

    B1X_MIN = 0.2
    B1X_MAX = 1.0
    B1X_STEP = 0.1

    D1X_MIN = 0.5
    D1X_MAX = 1.5
    D1X_STEP = 0.1

    ERR_TOL = 0.1

    d = {}  # Dummy dictionary
    err_b_min = 1e20
    err_a_min = 1e20
    abse = 1e20

    # Generate possible combinations
    c1xs = np.arange(C1X_MIN, C1X_MAX, C1X_STEP)
    c2xs = np.arange(C2X_MIN, C2X_MAX, C2X_STEP)
    b1xs = np.arange(B1X_MIN, B1X_MAX, B1X_STEP)
    d1xs = np.arange(D1X_MIN, D1X_MAX, D1X_STEP)
    raw = list(product(c1xs, c2xs, b1xs, d1xs))
    candidates = []

    for p in tqdm(raw):
        c1x = p[0]
        c2x = p[1]
        xparams['B1X'] = p[2]
        if xparams['INTERP'] == "OLD":
            d1x = 1.24
        else:
            # d1x = 31*np.exp(c2x)/25.0 # enforces same ending point as SCAN
            d1x = p[3]
        xparams['C1X'] = c1x
        xparams['C2X'] = c2x
        xparams['DX'] = d1x
        err_a, err_b = get_lzlx_coef(xparams, d, dens, True)
        if abs(err_a) <= ERR_TOL and abs(err_b) <= ERR_TOL:
            candidates.append({'C1X':xparams['C1X'], 'C2X':xparams['C2X'], 'DX':xparams['DX'], 'B1X':xparams['B1X'], 'err_a':err_a, 'err_b':err_b})
                # print "ACCEPTED A: {:}, B: {:}, C: {:}. Error: a = {:}%, b = {:}%.".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], err_a, err_b)
        # if abs(err_b) < abs(err_b_min) and abs(err_a) < abs(err_a_min):
        #     best = {'C1X':xparams['C1X'], 'C2X':xparams['C2X'], 'DX':xparams['DX'], 'err_a':err_a, 'err_b':err_b}
        #     err_b_min = err_b
        #     err_a_min = err_a
        if abs(err_b) + abs(err_a) < abse:
            best = {'C1X':xparams['C1X'], 'C2X':xparams['C2X'], 'DX':xparams['DX'], 'B1X':xparams['B1X'], 'err_a':err_a, 'err_b':err_b}
            abse = abs(err_b) + abs(err_a)
    print("Best c1x: {:}, c2z: {:}, d1x: {:}, b1x: {:}. Error: a = {:}%, b = {:}%.".format(best['C1X'], best['C2X'], best['DX'], best['B1X'], best['err_a'], best['err_b']))

    exlist = get_Ex_for_atoms(dens, xparams, True)
    for p in zip(["He", "Ne", "Ar", "Kr", "Xe"], exlist):
        print(p)

    return candidates

def smooth_fit(dens, xparams):
    # res = scipy.optimize.minimize(smooth_obj, (0.667, 0.8, 1.24), args=(dens, params), method="Nelder-Mead")
    xparams['K1'] = 0.067
    xparams['CAX'] = 3.15
    xparams['CBX'] = 1.35
    xparams['B1X'] = 1.0
    xparams['DX'] = 0.1
    # exlist = get_Ex_for_atoms(dens, xparams, True)
    # for p in zip(["He", "Ne", "Ar", "Kr", "Xe"], exlist):
    #     print p
    # d = {}
    # err_a, err_b = get_lzlx_coef(xparams, d, dens, True)
    # print "err a", err_a, "err_b", err_b
    # raise SystemExit()

    mon = np.linspace(0,1,num=100)
    if xparams['INTERP'] == "SMOOTH":
        mon = mon*2
    MAX_CAX = 3.0
    caxs = np.arange(0.00, MAX_CAX, 0.1)
    cbxs = np.arange(0, 3.0, 0.1)
    dxs = np.arange(0.1,2.0,0.1)
    b1xs = [1.0]
    ERR_TOL = 0.1
    d = {}  # Dummy dictionary
    err_b_min = 1e20
    err_a_min = 1e20
    abse = 1e20
    poss = list(product(caxs, cbxs, dxs))
    out = []
    for p in tqdm(poss):
        cbx = abs(p[1])
        cax = max(p[0], -2*np.sqrt(cbx))
        dx = abs(p[2])

        last = 1.0
        fail = False
        for r in mon:
            num = (1 - r)**3*(1 + cax*r + cbx*r**2)
            den = (1 + r**3)*(1 + (cbx/dx)*r**2)
            here = num/den
            if here > last:
                fail = True
                break
            else:
                last = here
        if fail:
            continue
        xparams['CAX'] = cax
        xparams['CBX'] = cbx
        xparams['DX'] = dx

        err_a, err_b = get_lzlx_coef(xparams, d, dens, True)
        if abs(err_a) <= ERR_TOL and abs(err_b) <= ERR_TOL:
            candidates.append({'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'DX':xparams['DX'], 'B1X':xparams['B1X'], 'err_a':err_a, 'err_b':err_b})
        if abs(err_b) + abs(err_a) < abse:
            best = {'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'DX':xparams['DX'], 'B1X':xparams['B1X'], 'err_a':err_a, 'err_b':err_b}
            abse = abs(err_b) + abs(err_a)
    print("Best A: {:}, B: {:}, d1x: {:}, b1x: {:}. Error: a = {:.2f}%, b = {:.2f}%.".format(best['CAX'], best['CBX'], best['DX'], best['B1X'], best['err_a'], best['err_b']))

    exlist = get_Ex_for_atoms(dens, xparams, True)
    for p in zip(["He", "Ne", "Ar", "Kr", "Xe"], exlist):
        print(p)
    raise SystemExit

def smooth_NM(dens, xparams):
    xparams['CAX'] = 3.15
    xparams['CBX'] = 1.35
    xparams['B1X'] = 1.0
    xparams['DX'] = 0.1
    res = scipy.optimize.minimize(smooth_obj, (3.15, 13.35, 1.24, 1.0), args=(dens, xparams), method='Nelder-Mead',options={'fatol':0.01})
    best = {'CAX':max(res.x[0], -2*np.sqrt(abs(res.x[1]))), 'CBX':abs(res.x[1]), 'DX':abs(res.x[2]), 'K1':abs(res.x[3])}
    print("Best A: {:}, B: {:}, d1x: {:}, k1: {:}.".format(best['CAX'], best['CBX'], best['DX'], best['K1']))
    for k in list(best.keys()):
        xparams[k] = best[k]

    exlist = get_Ex_for_atoms(dens, xparams, True)
    for p in zip(["He", "Ne", "Ar", "Kr", "Xe"], exlist):
        print(p)
    d = {}
    err_a, err_b = get_lzlx_coef(xparams, d, dens, True)
    print("err a {:.3f}%, err_b {:.3f}%".format(err_a, err_b))

    raise SystemExit

def smooth_obj(p, dens, params):
    params['CBX'] = abs(p[1])
    params['CAX'] = max(p[0], -2*np.sqrt(params['CBX']))
    params['DX'] = abs(p[2])
    params['K1'] = abs(p[3])

    d = {}
    err_a, err_b = get_lzlx_coef(params, d, dens, True)
    print("Tried A: {:2.4f}, B: {:2.4f}, DX: {:2.4f}, K1: {:2.4f}, got a: {:.4f}%, b: {:.4f}%".format(params['CAX'], params['CBX'], params['DX'], params['K1'], abs(err_a), abs(err_b))) 
    return max(abs(err_a), 0.1) + max(abs(err_b), 0.1)