import numpy as np
from math import pi, e
import Settings

AX = -0.7385587663820224058842300326808360
UAK = 10.0/81.0
BETA_RS_0 = 0.066725
BETA_MB = 0.066725
F0 = -0.9
if Settings.CORRELATION == "ACGGA" or Settings.CORRELATION == "COMPENSATED":
    AFACTOR = 0.5
    BFACTOR = 1.0
    CFACTOR = 0.16667
    DFACTOR = 0.29633
elif Settings.CORRELATION == "ORIGINAL" or Settings.CORRELATION == "MAXLOC" or Settings.CORRELATION == "MGG2":
    AFACTOR = 0.1
    BFACTOR = 0.1778

GAMMA = 0.031090690869654895034940863712730
C_TILDE = 1.467
P_TAU = 4.5 # Note the sup mat of acGGA paper uses 1/4.5 and multiplies
C1_CONST = 0.046644
KAILD = 0.128026
K0 = 0.174
UKU = 1.0

DEFAULT_X_PARAMS = dict(
    K1 = 0.065,
    A1 = 4.9479,

    CONX = 1.0,
    CAX = -4.5,
    CBX = 60.0,
    CCX = -33.467995,
    CDX = -22.032005,

    CXBX = 1.0,

    B1X = 0.156632,
    B2X = 0.12083,
    B3X = 0.5,
    B4X = 0.2218,

    C1X = 0.667,
    C2X= 0.8,
    DX = 1.24
)

DEFAULT_C_PARAMS = dict(
    CONC = 1.0,
    CAC = -6.0,
    CBC = 10.9,
    CCC = -5.2,
    CDC = 0.0,

    C1C = 0.64,
    C2C = 1.5,
    DC = 0.7,

    B1C = 0.0285764,
    B2C = 0.0889,
    B3C = 0.125541
)


def getscan_x(params, d0, d1, g0, g1, t0, t1, l0, l1, only_0=False, only_Fx=False):
    # First spin 0

    # construct LDA exchange energy density
    exunif_0 = AX*(2*d0)**(1.0/3.0)
    exlda_0 = exunif_0*(2*d0)

    # and enhancement factor
    Fx0 = get_scanFx(params, 2*d0, 2*g0, 2*t0, 2*l0)

    Ex_0 = exlda_0*Fx0

    if only_0:  # Test for zero spin 1 density i.e. H atom
        return Ex_0/2.0
    if not np.any(d1):
        return Ex_0/2.0

    # Now spin 1

    # construct LDA exchange energy density
    exunif_1 = AX*(2*d1)**(1.0/3.0)
    exlda_1 = exunif_1*(2*d1)

    # and enhancement factor
    Fx1 = get_scanFx(params, 2*d1, 2*g1, 2*t1, 2*l1)

    # Fx1 = scanFx(params, p, beta)
    Ex_1 = exlda_1*Fx1

    if only_Fx:
        return Fx0, Fx1
    return (Ex_0 + Ex_1)/2.0

def get_scanFx(params, rho, drho, tau, lap):
    p = drho**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))
    q = lap/(4*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0))

    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0)
    tauw = (drho**2)/(8*rho)
    if Settings.Fint == "REG":
        alpha = (tau - tauw)/(tau_unif + 1e-4)
    else:
        alpha = (tau - tauw)/tau_unif

    if Settings.Fint == "SMOOTH":
        ap = (40.0/27.0 + params["XETA"])*p
        bq = 20.0/9.0*q
        x_s = np.ones_like(alpha)
        idxs = alpha < 1e5
        x_s[idxs] = alpha[idxs]*(1.0 + ap[idxs]/np.sqrt(1.0 + 4*ap[idxs]**2))*(1.0 - bq[idxs]/np.sqrt(1.0 + 4*bq[idxs]**2))
        beta_s = x_s/(1.0 + x_s + params["XETA"]*p)
        Fx = scanFx(params, p, q, alpha, beta_s)

    elif Settings.Fint == "VSMTH":
        ap = 2.0*40.0/27.0*p
        bq = 2*20.0/9.0*q
        t1 = 1.0 + ap/np.sqrt(1.0 + ap**2)
        t2 = 1.0 - bq/np.sqrt(1.0 + bq**2)
        x_s = np.ones_like(alpha)
        idxs = alpha < 1e5
        x_s[idxs] = alpha[idxs]*np.sqrt(t1[idxs])*np.sqrt(t2[idxs])
        beta_vs = x_s/(1.0 + x_s)
        Fx = scanFx(params, p, q, alpha, beta_vs)
    else:
        beta = (tau - tauw)/(tau + tau_unif)
        Fx = scanFx(params, p, q, alpha, beta)

    return Fx

def scanFx(params, p, q, alpha, beta, delta_x=False):
    # make HX0
    if Settings.H0X == "MS0":
        hx0 = ms0_h0x(params, p)

    elif Settings.H0X == "ORIGINAL":
        hx0 = 1.0 + K0
    elif Settings.H0X == "MGG2":
        ku = (4.0 * beta * (1.0 - beta))**UKU
        hx0 = 1.0 + K0 - K0*ku/(1.0 + UAK*p/K0)
    else:
        raise SystemExit("Typo in Settings.H0X! > {:}".format(Settings.H0X))

    # make HX1
    if Settings.H1X == "ORIGINAL":
        cfb4 = UAK**2/params['K1'] - 0.112654
        p2 = p**2
        oma = 1.0 - alpha
        oma2 = oma**2
        wfac = cfb4*p2*np.exp(-abs(cfb4)*p/UAK)
        vfac = params['B1X']*p + params['B2X']*oma*np.exp(-params['B3X']*oma2)
        yfac = UAK*p + wfac + vfac**2
        hx1 = 1.0 + params['K1'] - params['K1']/(1.0 + yfac/params['K1'])
    elif Settings.H1X == "REGULARISED":
        p2 = p**2
        b12 = params['B1X']**2
        qt = (9.0/10.0)*(2*beta - 1) + 17.0/12.0*p
        qt2 = qt*qt
        xfac = 146.0/2025.0*qt2/(1+qt2/b12)
        xfac -= 73.0/405.0*qt*p/(np.sqrt(1+qt2/b12)*np.sqrt(1 + p2/b12))
        xfac += 1.0/params['K1']*UAK**2*p2/(1 + p2/b12)
        xfac *= 16*beta**2*(1-beta)**2
        xfac += 10.0/81.0*p
        hx1 = 1.0 + params['K1'] - params['K1']/(1.0 + xfac/params['K1'])
    elif Settings.H1X == "ORDER2":
        xfac = 10.0/81.0*p
        hx1 = 1.0 + params['K1'] - params['K1']/(1.0 + xfac/params['K1'])
    elif Settings.H1X == "COMPENSATED":
        mup = 10.0/81.0*p
        qt = (9.0/10.0)*(2*beta - 1) + 17.0/12.0*p
        delfx1 = (1 - 2*beta)*(1 + params['CAX']/2.0 + params['CBX']/4.0 + params['CCX']/8.0 + params['CDX']/16.0)
        delfx2 = -(params['CAX']/2.0 + params['CBX']/2.0 + 3.0/8.0*params['CCX'] + params['CDX']/4.0)*(1 - 2*beta)**2
        delx1 = -K0*delfx1
        delx2 = 146.0/2025.0*qt**2 - 73.0/405.0*p*qt + delfx1*delx1 + delfx1*mup - K0*delfx2 + 1.0/params['K1']*(delx1 + mup)**2
        delx = delx1 + delx2
        xfac = mup

        # xfac += delx/np.sqrt(1.0 + (delx/params['H1DX'])**2)

        idx = (1e-10 < beta) & (beta < 1.0-1e-10)
        expo = -params['H1DX']*p**2
        expo[idx] += -params['H1DX']*4*(1 - 2*beta[idx])**2/(beta[idx]*(1 - beta[idx]))
        ufid = expo > -100
        damp = np.zeros_like(xfac)
        damp[ufid] = np.exp(expo[ufid])
        xfac[ufid] += delx[ufid]*damp[ufid]
        if delta_x:
            return xfac
        hx1 = 1.0 + params['K1'] - params['K1']/(1.0 + xfac/params['K1'])
    else:
        raise SystemExit("Typo in Settings.H1X! > {:}".format(Settings.H1X))

    # FA
    if Settings.Fint == "BETA":
        FA = (1 - 2*beta)**3*(params['CONX'] + params['CAX']*beta + params['CBX']*beta**2 + params['CCX']*beta**3 + params['CDX']*beta**4)
    elif Settings.Fint == "ORIGINAL":
        FA = np.zeros(alpha.shape)
        oma = 1-alpha
        FA[alpha < 1.0] = np.exp(-params['C1X']*alpha[alpha < 1.0]/oma[alpha < 1.0])
        FA[alpha > 1.0] = -params['DX']*np.exp(params['C2X']/oma[alpha > 1.0])
    elif Settings.Fint == "ZEROS":
        FA = np.zeros(alpha.shape)
    elif Settings.Fint == "ONES":
        FA = np.ones(alpha.shape)
    elif Settings.Fint == "TWOBETA":
        FA = np.zeros(beta.shape)
        tb = 2*beta
        omtb = 1-tb
        FA[tb < 1.0] = np.exp(-params['C1X']*tb[tb < 1.0]/omtb[tb < 1.0])
        FA[tb > 1.0] = -params['DX']*np.exp(params['C2X']/omtb[tb > 1.0])
    elif Settings.Fint == "JPLAP":
        FA = eval_jplap(beta, p, q, params['CAX'], params['CBX'], params['CCX'], params['CDX'], params['FREG'])
    elif Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
        FA = eval_smoothlap(beta, params['CAX'], params['CBX'], params['CCX'], params['CDX'])
    elif Settings.Fint == "PADE":
        FA = eval_pade(beta, params['CAX'], params['CBX'], params['CCX'], params['CDX'])
    elif Settings.Fint == "REG":
        idx = (alpha > 1e-13) & (alpha < 2.5)
        FA = np.ones_like(alpha)
        co = [-0.023185843322, 0.234528941479, -0.887998041597, 1.451297044490, -0.663086601049, -0.4445555, -0.667]
        for i in range(1,8):
            FA[idx] += co[i-1]*pow(alpha[idx], 8-i)
        idx = alpha >= 2.5
        FA[idx] = -params['DX']*np.exp(params['C2X']/(1.0 - alpha[idx]))

    # elif Settings.Fint == "CRUDE":
    #     FA = 0.0
    #     for i in range(Settings.N_PARAMETERS+1):
    #         FA += params['X'+str(i)]*np.power(beta, i)
    #     FA *= (1 - 2*beta)
    else:
        raise SystemError("ERROR: Typo in Fint")

    # gx
    p14 = p**(1.0/4.0)
    gx = np.ones(p.shape)
    idxs = p > 1e-12
    if Settings.GX == "ORIGINAL":
        gx[idxs] = -np.expm1(-params['A1']/p14[idxs])
    elif Settings.GX == "FOURTH":
        gx[idxs] = -np.expm1(-0.444/p14[idxs] - params['A1']/np.sqrt(p[idxs]))

    # Fx1
    Fx1 = hx1 + FA*(hx0 - hx1)

    # Fx
    return Fx1*gx

def get_single_orbital_exchange(params, d, g, beta=None):
    rho = 2*d
    drho = 2*g

    exunif = AX*rho**(1.0/3.0)
    exlda = exunif*rho

    p = drho**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))

    if Settings.H0X == "MS0":
        hx0 = ms0_h0x(params, p)

    elif Settings.H0X == "ORIGINAL":
        hx0 = 1.0 + K0
    elif Settings.H0X == "MGG2":
        ku = (4.0 * beta * (1.0 - beta))**UKU
        hx0 = 1.0 + K0 - K0*ku/(1.0 + UAK*p/K0)
    else:
        raise SystemExit("Typo in Settings.H0X! > {:}".format(Settings.H0X))

    # gx
    p14 = p**(1.0/4.0)
    gx = np.ones(p.shape)
    idxs = p > 1e-16
    if Settings.GX == "ORIGINAL":
        gx[idxs] = -np.expm1(-params['A1']/p14[idxs])
    elif Settings.GX == "FOURTH":
        gx[idxs] = -np.expm1(-0.444/p14[idxs] - params['A1']/np.sqrt(p[idxs]))
    else:
        print("Unrecognised GX setting ", Settings.GX)
        raise ValueError

    return exlda*hx0*gx/2.0


def getscan_c(params, dT, gT, tT, lT, zeta):
    """
    This follows the ugly Fortran of the original optimisation program. Sorry.

    Note: g0 and g1 are absolute value of gradients
    """

    gTT = gT**2
    tauw = gTT/(8*dT)
    s = np.abs(gT)/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(dT, 4.0/3.0))
    idxs = gTT <= 1e-16
    s[idxs] = 0.0
    p = s*s
    q = lT/(4*(3*pi**2)**(2.0/3.0)*dT**(5.0/3.0))


    ds_zeta = (np.power(1.0 + zeta, 5.0/3.0) + np.power(1.0 - zeta, 5.0/3.0))/2.0
    if Settings.Fint == "REG":
        tau0 = (0.3*np.power(3*pi**2, 2.0/3.0)*np.power(dT, 5.0/3.0) + 1e-4)*ds_zeta
    else:
        tau0 = 0.3*np.power(3*pi**2, 2.0/3.0)*np.power(dT, 5.0/3.0)*ds_zeta

    alpha = (tT - tauw)/tau0

    if Settings.Fint == "SMOOTH":
        ap = (40.0/27.0 + params["CETA"])*p
        bq = 20.0/9.0*q
        x_s = np.ones_like(alpha)
        idxs = alpha < 1e5
        x_s[idxs] = alpha[idxs]*(1.0 + ap[idxs]/np.sqrt(1.0 + 4*ap[idxs]**2))*(1.0 - bq[idxs]/np.sqrt(1.0 + 4*bq[idxs]**2))
        beta = x_s/(1.0 + x_s + params["CETA"]*p)
    elif Settings.Fint == "VSMTH" :
        ap = 2.0*40.0/27.0*p
        bq = 2*20.0/9.0*q
        t1 = 1.0 + ap/np.sqrt(1.0 + ap**2)
        t2 = 1.0 - bq/np.sqrt(1.0 + bq**2)
        x_s = np.ones_like(alpha)
        idxs = alpha < 1e5
        x_s[idxs] = alpha[idxs]*np.sqrt(t1[idxs])*np.sqrt(t2[idxs])
        beta = x_s/(1.0 + x_s)
    else:
        beta = (tT - tauw)/(tT + tau0)


    if Settings.Fint == "BETA":
        f_int = (1 - 2*beta)**3*(params['CONC'] + params['CAC']*beta + params['CBC']*beta**2 + params['CCC']*beta**3 + params['CDC']*beta**4)
    elif Settings.Fint == "ORIGINAL":
        f_int = np.zeros(alpha.shape)
        oma = 1-alpha
        f_int[alpha < 1.0] = np.exp(-params['C1C']*alpha[alpha < 1.0]/oma[alpha < 1.0])
        f_int[alpha > 1.0] = -params['DC']*np.exp(params['C2C']/(oma[alpha > 1.0]))
    elif Settings.Fint == "ONES":
        f_int = np.ones(beta.shape)
    elif Settings.Fint == "ZEROS":
        f_int = np.zeros(beta.shape)
    elif Settings.Fint == "TWOBETA":
        f_int = np.zeros(beta.shape)
        tb = 2*beta
        omtb = 1-tb
        f_int[tb < 1.0] = np.exp(-params['C1C']*tb[tb < 1.0]/omtb[tb < 1.0])
        f_int[tb > 1.0] = -params['DC']*np.exp(params['C2C']/omtb[tb > 1.0])
    elif Settings.Fint == "JPLAP":
        f_int = eval_jplap(beta, p, q, params['CAC'], params['CBC'], params['CCC'], params['CDC'], params['FREG'])
    elif Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
        f_int = eval_smoothlap(beta, params['CAC'], params['CBC'], params['CCC'], params['CDC'])
    elif Settings.Fint == "PADE":
        f_int = eval_pade(beta, params['CAC'], params['CBC'], params['CCC'], params['CDC'])
    elif Settings.Fint == "REG":
        idx = (alpha > 1e-13) & (alpha < 2.5)
        f_int = np.ones_like(alpha)
        co = [-0.051848879792, 0.516884468372, -1.915710236206, 3.061560252175, -1.535685604549, -0.4352, -0.64]
        for i in range(1,8):
            f_int[idx] += co[i-1]*pow(alpha[idx], 8-i)
        idx = alpha >= 2.5
        f_int[idx] = -params['DC']*np.exp(params['C2C']/(1.0 - alpha[idx]))
    else:
        raise SystemExit("ERROR: Typo in Fint Setting")

    dthrd = np.exp(np.log(dT)*1.0/3.0)
    rs = (0.75/pi)**(1.0/3.0)/dthrd

    if Settings.CORRELATION == "ORIGINAL":
        eppgga0 = corgga_0_ori(params, rs, s, zeta)
        eppgga1 = corgga_1_ori(params, rs, s, zeta)
    elif Settings.CORRELATION == "ACGGA":
        eppgga0 = corgga_0_acgga(params, rs, s, zeta)
        eppgga1 = corgga_1_acgga(params, rs, s, zeta)
    elif Settings.CORRELATION == "MAXLOC" :
        eppgga0, eppgga1 = corgga_maxloc(params, rs, s, zeta)
    elif Settings.CORRELATION == "MGG2" :
        eppgga0, eppgga1 = corgga_mgga2(params, rs, s, zeta, beta)
    elif Settings.CORRELATION == "COMPENSATED":
        eppgga0 = corgga_0_ori(params, rs, s, zeta)
        eppgga1 = corgga_1_comp_v5(params, rs, s, beta, zeta)

    epp = eppgga1 + f_int*(eppgga0 - eppgga1)

    return dT*epp

def get_single_orbital_correlation(params, dT, gT, zeta):
    gTT = gT**2
    tauw = gTT/(8*dT)
    s = np.abs(gT)/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(dT, 4.0/3.0))
    idxs = gTT <= 1e-16
    s[idxs] = 0.0

    dthrd = np.exp(np.log(dT)*1.0/3.0)
    rs = (0.75/pi)**(1.0/3.0)/dthrd

    if Settings.CORRELATION == "ORIGINAL" or Settings.CORRELATION == "COMPENSATED":
        eppgga0 = corgga_0_ori(params, rs, s, zeta)
    elif Settings.CORRELATION == "ACGGA":
        eppgga0 = corgga_0_acgga(params, rs, s, zeta)
    else:
        print("ERROR: Single orbital correlation funciton not set")
        raise NotImplementedError
    return dT*eppgga0

def corgga_1_comp(params, rs, s, beta, zeta, maxbound=False, minbound=False):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    phi3 = phi**3
    EC_lsda0 = lsda_0(params, rs)
    EC_lsda1 = lsda_1(rs, zeta)

    # w1 = np.expm1(-EC_lsda1/(phi3*GAMMA))
    sqrtrs = np.sqrt(rs)
    beta_num = 1.0 + AFACTOR*rs*(BFACTOR + CFACTOR*rs)
    beta_den = 1.0 + AFACTOR*rs*(1.0 + DFACTOR*rs)
    beta_rs = BETA_RS_0*beta_num/beta_den

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    gc_zeta = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)

    w = np.expm1(-EC_lsda1/(phi3*GAMMA))  # Updated
    t = (3*pi**2/16.0)**(1.0/3.0)*s/(phi*sqrtrs)
    y = beta_rs*t**2/(GAMMA*w)
    p_t = (1.0 + t/P_TAU)/(1.0 + C_TILDE*t/P_TAU)

    if maxbound:
        del_y = params['H1DC']
    elif minbound:
        del_y = -params['H1DC']
    elif params['H1DC'] == 0.0:
        del_y = 0.0
    else:
        del_y = -(1 - 2*beta)*(1 + params['CAC']/2.0 + params['CBC']/4.0 + params['CCC']/8.0 + params['CDC']/16.0)*(EC_lsda0*gc_zeta - EC_lsda1)/(GAMMA*phi3)
        del_y = del_y/np.sqrt(1 + (del_y/params['H1DC'])**2)

    Xc = beta_rs*t**2/GAMMA + del_y

    g = Xc*(1 + y)/(1 + y + y**2)

    h1c = GAMMA*phi3*np.log(1 + g)

    return (EC_lsda1 + h1c)

def corgga_1_comp_v4(params, rs, s, beta, zeta, sumis=None):
    dthrd = rs/(0.75/pi)**(1.0/3.0)
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    afix_T = np.sqrt(pi/4.0)*np.power(9.0*pi/4.0, 1.0/6.0)

    T = afix_T*s/np.sqrt(rs)/phi
    FK = (3.0*pi**2)**(1.0/3.0)*dthrd
    sk = np.sqrt(4.0*FK/pi)

    EC_lsda1, _ = corpbe_rtpss(rs, zeta, T, phi, sk)
    EC_lsda0 = lsda_0(params, rs)


    beta_num = 1.0 + AFACTOR*rs
    beta_den = 1.0 + BFACTOR*rs
    beta_rs = BETA_MB*beta_num/beta_den

    phi3 = phi**3
    pon = -EC_lsda1/(phi3*GAMMA)
    w = np.expm1(pon)

    A = beta_rs/(GAMMA*w)
    y = A*T**2

    if sumis is None:
        # sumis = 1 + params['CAC']/2.0 + params['CBC']/4.0 + params['CCC']/8.0 + params['CDC']/16.0
        sumis = (1 + params['CAC']/2.0 + params['CBC']/4.0)/((1 + params['CCC']/4.0)**2 + params['CDC']/16.0)
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    gc_zeta = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    del_ec2 = -(1 - 2*beta)*sumis*(EC_lsda0*gc_zeta - EC_lsda1)/(GAMMA*phi3*w)
    if params['H1DC'] > 0.0:
        del_y = del_ec2/np.sqrt(1 + (w*del_ec2/params['H1DC'])**2)
    else:
        del_y = 0.0
    f_g = (1.0 - del_y)/np.power(1.0 + 4.0*y, 0.25)

    hcore = 1.0 + w*(1.0 - f_g)
    ah = GAMMA*phi**3
    H = ah*np.log(hcore)

    return EC_lsda1+H

def corgga_1_comp_v5(params, rs, s, beta, zeta, sumis=None):
    dthrd = rs/(0.75/pi)**(1.0/3.0)
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    afix_T = np.sqrt(pi/4.0)*np.power(9.0*pi/4.0, 1.0/6.0)

    T = afix_T*s/np.sqrt(rs)/phi
    FK = (3.0*pi**2)**(1.0/3.0)*dthrd
    sk = np.sqrt(4.0*FK/pi)

    EC_lsda1, _ = corpbe_rtpss(rs, zeta, T, phi, sk)
    EC_lsda0 = lsda_0(params, rs)

    beta_num = 1.0 + AFACTOR*rs
    beta_den = 1.0 + BFACTOR*rs
    beta_rs = BETA_MB*beta_num/beta_den

    phi3 = phi**3
    pon = -EC_lsda1/(phi3*GAMMA)
    w = np.expm1(pon)

    A = beta_rs/(GAMMA*w)
    y = A*T**2

    if sumis is None:
        sumis = 1 + params['CAC']/2.0 + params['CBC']/4.0 + params['CCC']/8.0 + params['CDC']/16.0
        # sumis = (1 + params['CAC']/2.0 + params['CBC']/4.0)/((1 + params['CCC']/4.0)**2 + params['CDC']/16.0)
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    gc_zeta = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    del_ec2 = -(1 - 2*beta)*sumis*(EC_lsda0*gc_zeta - EC_lsda1)/(GAMMA*phi3*w)

    idx = (1e-10 < beta) & (beta < 1.0)
    damp = np.zeros_like(del_ec2)
    expo = np.zeros_like(del_ec2)
    expo[idx] = -params['H1DC1']*4*(1 - 2*beta[idx])**2/(beta[idx]*(1 - beta[idx]))
    expidx = idx & (expo > -100)
    damp[expidx] = np.exp(expo[expidx])
    if params['H1DC2'] > 0.0:
        del_ec2 /= np.sqrt(1 + (w*del_ec2/params['H1DC2'])**2)
    else:
        del_ec2 = 0.0
    del_y = del_ec2*damp

    f_g = (1.0 - del_y)/np.power(1.0 + 4.0*y, 0.25)

    hcore = 1.0 + w*(1.0 - f_g)
    ah = GAMMA*phi**3
    H = ah*np.log(hcore)

    return EC_lsda1+H

def corgga_0_acgga(params, rs, s, zeta):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    EC_lsda0 = lsda_0(params, rs)

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)

    g_0 = 1.0/(1.0 + params['CHI_2']*s**2 + params['CHI_42']*s**4)

    w = np.expm1(-EC_lsda0/params['B1C'])

    H_0 = params['B1C']*np.log(1.0 + w*(1.0 - g_0))

    return (EC_lsda0 + H_0)*Gc

def corgga_0_acgga_rs_0(params, s, zeta):
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    ginf_s = 1.0/(1.0 + params['CHI_2']*s**2 + params['CHI_42']*s**4)
    lim = -params['B1C'] + params['B1C']*np.log(1 + (e - 1)*(1 - ginf_s))
    return lim*Gc

def corgga_0_acgga_rs_inf_s_inf(params, zeta):
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    print(np.min(Gc))
    print(np.min(dx_zeta))
    print(-params['B1C']/params['B3C'])
    print(params['CHI_42'])
    print(np.all(-params['B1C']/params['B3C']*Gc/params['CHI_42'] <= 0.0))
    return -params['B1C']/params['B3C']*Gc/params['CHI_42']

def corgga_0_acgga_rs_inf(params, s, zeta):
    """
    This has rs dependence in the limit:

    lim_{rs->inf} ec0 = c_inf^0/rs

    Also called c_inf^0 in the notes

    """

    ginf_s = 1.0/(1.0 + params['CHI_2']*s**2 + params['CHI_42']*s**4)
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    return -params['B1C']/params['B3C']*ginf_s*Gc

def corgga_1_comp_rs_inf(params, s, zeta, beta, sumis=None):
    """
    Again, we have rs dependence in the limit:

    lim_{rs->inf} ec1 = c_inf^1/rs

    also called c_inf^1 in notes
    """

    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    phi3 = phi**3
    beta_rs_inf = CFACTOR*BETA_RS_0/DFACTOR
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    cx_zeta = -3.0/(4*pi)*(9*pi/4.0)**(1.0/3.0)*dx_zeta
    f0 = -0.9

    lim_y = beta_rs_inf*phi*(3*pi**2/16.0)**(2.0/3.0)*s**2
    if sumis is None:
        sumis = (1 + params['CAC']/2.0 + params['CBC']/4.0)/((1 + params['CCC']/4.0)**2 + params['CDC']/16.0)
        # lim_dely = -(1 - 2*beta)*(1 + params['CAC']/2.0 + params['CBC']/4.0 + params['CCC']/8.0 + params['CDC']/16.0)
    else:
        lim_dely = -(1 - 2*beta)*sumis

    lim_dely *= (-params['B1C']/params['B3C']*Gc - cx_zeta - f0)

    gy_inf = (1 + params['CHI_2']*s**2)/(1 + params['CHI_2']*s**2 + params['CHI_42']*s**4)

    return f0 - cx_zeta + (lim_y - lim_dely)*gy_inf

def corgga_1_rs_inf_db(params, s, zeta, beta, sumis=None):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    phi3 = phi**3
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    cx_zeta = -3.0/(4*pi)*(9*pi/4.0)**(1.0/3.0)*dx_zeta
    f0 = -0.9
    beta_rs_inf = CFACTOR*BETA_RS_0/DFACTOR

    if sumis is None:
        del_beta = -(1 - 2*beta)*(1 + params['CAC']/2.0 + params['CBC']/4.0 + params['CCC']/8.0 + params['CDC']/16.0)
    else:
        del_beta = -(1 - 2*beta)*sumis

    chi_inf = beta_rs_inf*phi*(3*pi**2/16.0)**(2.0/3.0)/(cx_zeta - f0)

    lim_dely = -chi_inf*s**2
    # lim_dely += (params['B1C']*Gc/(params['B3C']*(cx_zeta - f0)) - 1.0)*del_beta/np.sqrt(1 + (del_beta/params['DBETA'])**2)
    lim_dely += params['DBETA']*np.exp(-s**2/1.0)
    lim_dely *= (1 + chi_inf*s**2)/(1 + chi_inf*s**2 + chi_inf**2*s**4)

    return (f0 - cx_zeta)*(1 + lim_dely)

def corgga_1_rs_inf_v3(params, s, beta, zeta, sumis=None):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    if sumis is None:
        del_beta = -(1 - 2*beta)*(1 + params['CAC']/2.0 + params['CBC']/4.0 + params['CCC']/8.0 + params['CDC']/16.0)
    else:
        del_beta = -(1 - 2*beta)*sumis

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    cx_zeta = -3.0/(4*pi)*(9*pi/4.0)**(1.0/3.0)*dx_zeta
    f0 = -0.9

    beta_rs_inf = CFACTOR*BETA_RS_0/DFACTOR
    chi_inf = beta_rs_inf*phi*(3*pi**2/16.0)**(2.0/3.0)/(cx_zeta - f0)

    del_ec2_inf = del_beta*(1 - params['B1C']*Gc/(params['B3C']*(cx_zeta - f0)))

    con_del_ec2_inf = del_ec2_inf/np.sqrt(1 + (del_ec2_inf/params['DC2'])**2)

    print(con_del_ec2_inf, del_beta, chi_inf)

    c1_inf = (f0 - cx_zeta)/(1 + chi_inf*s**2 + chi_inf**2*s**4)
    if params['DC1'] > 0.0:
        c1_inf *= 1 - (1 + chi_inf*s**2)*con_del_ec2_inf*np.exp(-chi_inf*s**2/params['DC1'])
        # c1_inf *= 1 - con_del_ec2_inf

    return c1_inf

def corgga_1_rs_inf_v4(params, s, beta, zeta, sumis=None):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    if sumis is None:
        del_beta = -(1 - 2*beta)*(1 + params['CAC']/2.0 + params['CBC']/4.0 + params['CCC']/8.0 + params['CDC']/16.0)
    else:
        del_beta = -(1 - 2*beta)*sumis

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - params['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    cx_zeta = -3.0/(4*pi)*(9*pi/4.0)**(1.0/3.0)*dx_zeta
    f0 = -0.9
    # beta_rs_inf = CFACTOR*BETA_RS_0/DFACTOR
    # chi_inf = beta_rs_inf*phi*(3*pi**2/16.0)**(2.0/3.0)/(cx_zeta - f0)
    chi_inf = 0.128026

    del_ec2_inf = del_beta*(1 - params['B1C']*Gc/(params['B3C']*(cx_zeta - f0)))
    # return (f0 - cx_zeta)*(1 - del_ec2_inf)/(1 + chi_inf*s**2 + chi_inf**2*s**4)
    return (f0 - cx_zeta)*(1 - del_ec2_inf)/(1 + 4*chi_inf*s**2)**(1/4.0)



def corgga_1_comp_rs_inf_s_inf(params, zeta):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    beta_rs_inf = CFACTOR*BETA_RS_0/DFACTOR
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    cx_zeta = -3.0/(4*pi)*(9*pi/4.0)**(1.0/3.0)*dx_zeta
    f0 = -0.9
    return -(cx_zeta - f0)/(beta_rs_inf*phi*(3*pi**2/16.0)**(2.0/3.0)/(cx_zeta - f0))**2

def corgga_1_comp_rs_0(params, s, zeta):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    xi = (3*pi**2/16.0)**(2.0/3.0)/C_TILDE
    xi *= np.exp(-C1_CONST/GAMMA)*BETA_RS_0/GAMMA
    xsp2 = xi*(s/phi)**2
    glim_yt = 1 + 1.0/(xsp2 + xsp2**2)
    return -GAMMA*phi**3*np.log(glim_yt)

def corgga_1_acgga(params, rs, s, zeta):
    """
    This is the acGGA+ of Cancio 2018 (JCP)
    """

    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    phi3 = phi**3

    sqrtrs = np.sqrt(rs)

    EC_lsda1 = lsda_1(rs, zeta)  # Agrees with example

    beta_num = 1.0 + AFACTOR*rs*(BFACTOR + CFACTOR*rs)
    beta_den = 1.0 + AFACTOR*rs*(1.0 + DFACTOR*rs)
    beta_rs = BETA_RS_0*beta_num/beta_den

    w = np.expm1(-EC_lsda1/(phi3*GAMMA))  # Updated
    t = (3*pi**2/16.0)**(1.0/3.0)*s/(phi*sqrtrs)
    p_t = (1.0 + t/P_TAU)/(1.0 + C_TILDE*t/P_TAU)
    A = BETA_RS_0/(GAMMA*w)
    t_til = t*np.sqrt(p_t*beta_rs/BETA_RS_0)
    y = A*t_til**2
    g = (1.0 + y)/(1.0 + y + y**2)
    hcore = 1.0 + BETA_RS_0/GAMMA*t_til**2*g
    H = GAMMA*phi3*np.log(hcore)

    return EC_lsda1+H



def corgga_0_ori(params, rs, s, zeta):
    # _0 does not refer to spin in function name

    sqrt_rs = np.sqrt(rs)
    f1 = 1.0 + params['B2C']*sqrt_rs + params['B3C']*rs
    ec0_lda = -params['B1C']/f1

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0

    gc_zeta = (1.0 - 2.3631*(dx_zeta - 1.0)) * (1.0 - pow(zeta, 12))
    w0 = np.expm1(-ec0_lda/params['B1C'])

    gf_inf = 1.0/(1.0 + 4.0*KAILD*s**2)**(1.0/4.0)

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = params['B1C']*np.log(hcore0)

    EPPGGA = (ec0_lda + h0)*gc_zeta

    return EPPGGA

def corgga_1_ori(params, rs, s, zeta):
    dthrd = rs/(0.75/pi)**(1.0/3.0)
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    afix_T = np.sqrt(pi/4.0)*np.power(9.0*pi/4.0, 1.0/6.0)

    T = afix_T*s/np.sqrt(rs)/phi
    FK = (3.0*pi**2)**(1.0/3.0)*dthrd
    sk = np.sqrt(4.0*FK/pi)

    EC, _ = corpbe_rtpss(rs, zeta, T, phi, sk)

    beta_num = 1.0 + AFACTOR*rs
    beta_den = 1.0 + BFACTOR*rs
    beta = BETA_MB*beta_num/beta_den

    phi3 = phi**3
    pon = -EC/(phi3*GAMMA)
    w = np.expm1(pon)

    A = beta/(GAMMA*w)
    V = A*T**2

    f_g = 1.0/np.power(1.0 + 4.0*V, 0.25)

    hcore = 1.0 + w*(1.0 - f_g)
    ah = GAMMA*phi**3
    H = ah*np.log(hcore)

    return EC+H

def corgga_maxloc(params, rs, s, zeta):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    sqrt_rs = np.sqrt(rs)

    f1 = 1.0 + params['B2C']*sqrt_rs + params['B3C']*rs
    ec0_lda = -params['B1C']/f1

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0

    if Settings.H0X == "ORIGINAL" or Settings.H0X == "MGG2":
        h0 = 1.174
    elif Settings.H0X == "MS0":
        h0 = 1.0 + K0 - K0/(1.0 + params['C']/K0)
    bfac = h0*(3.0/4.0*(3.0/(2.0*pi))**(2.0/3.0))*params['B3C']/params['B1C']
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - pow(zeta, 12))

    EC0 = gc_zeta*ec0_lda

    dthrd = rs/(0.75/pi)**(1.0/3.0)
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    afix_T = np.sqrt(pi/4.0)*np.power(9.0*pi/4.0, 1.0/6.0)

    T = afix_T*s/np.sqrt(rs)/phi
    FK = (3.0*pi**2)**(1.0/3.0)*dthrd
    sk = np.sqrt(4.0*FK/pi)

    EC1, _ = corpbe_rtpss(rs, zeta, T, phi, sk)

    beta_num = 1.0 + AFACTOR*rs
    beta_den = 1.0 + BFACTOR*rs
    beta = BETA_MB*beta_num/beta_den

    phi3 = phi**3
    ah = GAMMA*phi3
    pon = -EC1/ah
    w = np.expm1(pon)

    A = beta/(GAMMA*w)

    V = A*T**2

    # NB: need to pick either the polynomial f_g (older, violates ec0 >= ec1),
    # or the exponential one (newer, violates ec0 >= ec1 for intermediate s as implemented)
    # increasing cfac only slightly, 0.05 - 0.1, will resolve this, but worsen performance
    cfac = 1.0 - (np.expm1((-EC1 + EC0)/ah))/w
    f_g = (1.0 + cfac*V**2)/(1.0 + V + V**2)
    #cfac = params['B1C']/params['B3C']/0.4335
    #f_g = cfac + (1.0 - cfac)*np.exp(-V/(1.0-cfac))

    hcore = 1.0 + w*(1.0 - f_g)
    H1 = ah*np.log(hcore)

    return EC0, EC1 + H1

def corgga_mgga2(params, rs, s, zeta, beta):
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    sqrt_rs = np.sqrt(rs)
    f1 = 1.0 + params['B2C']*sqrt_rs + params['B3C']*rs
    ec0_lda = -params['B1C']/f1

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0

    if Settings.H0X == "ORIGINAL" or Settings.H0X == "MGG2":
        h0 = 1.174
    elif Settings.H0X == "MS0":
        h0 = 1.0 + K0 - K0/(1.0 + params['C']/K0)
    bfac = h0*(3.0/4.0*(3.0/(2.0*pi))**(2.0/3.0))*params['B3C']/params['B1C']
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - pow(zeta, 12))

    EC0 = gc_zeta*ec0_lda

    dthrd = rs/(0.75/pi)**(1.0/3.0)
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    afix_T = np.sqrt(pi/4.0)*np.power(9.0*pi/4.0, 1.0/6.0)

    T = afix_T*s/np.sqrt(rs)/phi
    FK = (3.0*pi**2)**(1.0/3.0)*dthrd
    sk = np.sqrt(4.0*FK/pi)

    EC1, _ = corpbe_rtpss(rs, zeta, T, phi, sk)

    beta_num = 1.0 + AFACTOR*rs
    beta_den = 1.0 + BFACTOR*rs
    bbbeta = BETA_MB*beta_num/beta_den

    phi3 = phi**3
    ah = GAMMA*phi3
    pon = -EC1/ah
    w = np.expm1(pon)

    A = bbbeta/(GAMMA*w)

    V = A*T**2

    ccg = 1.0 - (np.expm1((EC0 - EC1)/ah))/w
    f_g = (1.0 + ccg*V**2)/(1.0 + V + V**2)

    hcore = 1.0 + w*(1.0 - f_g)
    H1 = ah*np.log(hcore)

    ku = (4.0* beta * (1.0 - beta))**UKU

    eps1 = EC1 + H1
    eps0 = eps1 + (1.0 - ku)*(EC0 - eps1)

    return eps0, eps1


def corpbe_rtpss(rs, zeta, T, phi, sk):
    GAM = 0.51984209978974632953442121455650  # 2^(4/3)-2
    FZZ = 8.0/(9.0*GAM)
    gamma = 0.031090690869654895034940863712730  # (1-log(2))/pi^2
    bet_mb = 0.066724550603149220
    sqrt_rs = np.sqrt(rs)

    EU = gcor2(0.03109070, 0.213700, 7.59570, 3.58760, 1.63820, 0.492940, sqrt_rs)
    EP = gcor2(0.015545350, 0.205480, 14.11890, 6.19770, 3.36620, 0.625170, sqrt_rs)
    ALFM = gcor2(0.01688690, 0.111250, 10.3570, 3.62310, 0.880260, 0.496710, sqrt_rs)

    ALFC = -ALFM
    Z4 = zeta**4

    # LDA part of the energy
    F = ((1.0 + zeta)**(4.0/3.0) + (1.0 - zeta)**(4.0/3.0) - 2.0)/GAM
    EC = EU*(1.0 - F*zeta**4) + EP*F*Z4 - ALFM*F*(1.0 - Z4)/FZZ

    # PBE correction
    bet = bet_mb*(1.0 + 0.1*rs)/(1.0 + 0.1778*rs)

    delt = bet/gamma
    phi3 = phi**3
    pon = -EC/(phi3*gamma)
    B = delt/(np.exp(pon) - 1.0)
    B2 = B**2
    T2 = T**2
    T4 = T**4
    Q4 = 1.0 + B*T2
    Q5 = 1.0 + B*T2 + B2*T4
    H = phi3*(bet/delt)*np.log(1.0 + delt*Q4*T2/Q5)   # Non-local part of correlation

    return EC, H


def gcor2(A, A1, B1, B2, B3, B4, sqrtrs):
    Q0 = -2.0*A*(1.0 + A1*sqrtrs*sqrtrs)
    Q1 = 2.0*A*sqrtrs*(B1 + sqrtrs*(B2 + sqrtrs*(B3 + B4*sqrtrs)))
    Q2 = np.log(1.0 + 1.0/Q1)
    GG = Q0*Q2
    return GG

def eval_jplap(beta, p, q, A, B, C, D, reg):
    assert reg >= 1.0, "Invalid value for IE function regularising parameter"
    t1 = (1 - 2*beta)*(1 + A*beta + B*beta**2 + C*beta**3 + D*beta**4)
    t21 = 16*(1.0 + A/2.0 + B/4.0 + C/8.0 + D/16.0)*(beta**(2.0))*(1 - beta)**(2.0)
    t22 = -85.0/54.0*p/np.sqrt(1 + p**2/reg**2) + 10.0/9.0*q/np.sqrt(1 + q**2/reg**2)
    return t1 + t21*t22

def eval_smoothlap(beta, A, B, C, D):
    return (1 - 2*beta)*(1 + A*beta + B*beta**2 + C*beta**3 + D*beta**4)

def interp_gradient_poly(beta, beta2, beta3, A, B, C, D):
    return (1 - 2*beta)**3*(A + 2*B*beta + 3*beta2*C + 4*beta3*D) - 6*(1 - 2*beta)**2*(1 + A*beta + B*beta2 + beta3*C + beta3*beta*D)

def interp_gradient_jplap(beta, beta2, beta3, beta4, pp, qq, A, B, C, D):
    t1 = (1 - 2*beta)*(A + 2*B*beta + 3*C*beta2 + 4*D*beta3) - 2*(1 + A*beta + B*beta2 + C*beta3 + D*beta4)
    t21 = 32*(1.0 + A/2.0 + B/4.0 + C/8.0 + D/16.0)*beta*(1 - beta)*(1 - 2*beta)
    t22 = -85.0/54.0*pp + 10.0/9.0*qq
    return t1 + t21*t22

def interp_grad2_jplap(beta, beta2, beta3, pp, qq, A, B, C, D):
    t1 = (1 - 2*beta)*(2*B + 6*C*beta + 12*D*beta2) - 4*(A + 2*B*beta + 3*C*beta2 + 4*D*beta3)
    t21 = 32*(1 - 6*beta + 6*beta2)
    t22 = -85.0/54.0*pp + 10.0/9.0*qq
    return t1 + t21*t22

def interp_gradient_smooth(beta, beta2, beta3, beta4, A, B, C, D):
    return (1 - 2*beta)*(A + 2*B*beta + 3*C*beta2 + 4*D*beta3) - 2*(1 + A*beta + B*beta2 + C*beta3 + D*beta4)

def interp_grad2_smooth(beta, beta2, beta3, A, B, C, D):
    return (1 - 2*beta)*(2*B + 6*C*beta + 12*D*beta2) - 4*(A + 2*B*beta + 3*C*beta2 + 4*D*beta3)


def eval_pade(beta, A, B, C, D):
    return (1 - 2*beta)*(1 + A*beta + B*beta**2)/((1 + C*beta**2)**2 + D*beta**4)

def interp_d1_pade(beta, beta2, beta3, beta4, A, B, C, D):
    t1 = (1 - 2*beta)*(1 + A*beta + B*beta2)*(4*D*beta3 + 4*C*beta*(1 + C*beta2))/(D*beta4 + (1 + C*beta2)**2)**2
    t2 = (1 -2*beta)*(A+2*B*beta)/(D*beta4 + (1 + C*beta2)**2)
    t3 = 2*(1 + A*beta + B*beta2)/(D*beta4 + (1 + C*beta2)**2)
    return -t1 + t2 - t3

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Beginning XCFun style SCAN implementation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def getscan_x_both(params, d0, d1, g0, g1, t0, t1, weights):
    e0 = getscan_x(params, 2*d0, 2*g0, 2*t0, weights)
    e1 = getscan_x(params, 2*d1, 2*g1, 2*t1, weights)
    return 0.5*(e0 + e1)


def getscan_c_both(params, d0, d1, g0, g1, t0, t1, weights):
    zeta = (d0 - d1)/(d0 + d1)
    e0 = getscan_c(params, d0, d1, np.abs(g0), np.abs(g1), t0, t1, zeta, weights)
    return e0


def eps_c_0_high_dens_zeta_0(params, s):
    """
    Assuming zeta = 0
    """

    cx = -(3.0/(4.0*pi))*(9.0*pi/4.0)**(1.0/3.0)
    beta_inf = BETA_RS_0*AFACTOR/BFACTOR
    chi_inf = (3.0*pi**2/16.0)**(2.0/3.0)*beta_inf/(cx - F0)  # checked
    g_inf = 1.0/np.power(1.0 + 4*chi_inf*s**2, 1.0/4.0)
    return params['B1C']*np.log(1.0 - g_inf*np.expm1(1)/np.exp(1))


def eps_c_0_high_dens_zeta_0(params, s, chi_42=None):
    """
    Assuming zeta = 0

    In this, chi_42 is set by b1c, it is a free parameter later and in the main functional
    """


    chi_0_zeta_0 = BETA_RS_0*(3*pi**2/16.0)**(2.0/3.0)/(C_TILDE*GAMMA)*np.exp(-C1_CONST/GAMMA)
    if chi_42 is None:
        print("SETTING CHI_42 from B1C")
        chi_42 = params['B1C']/GAMMA*(e - 1)/e*chi_0_zeta_0**2
    else:
        foo = params['B1C']/GAMMA*(e - 1)/e*chi_0_zeta_0**2

    chi_2 = GAMMA/params['B1C']*chi_0_zeta_0**(-3)*chi_42**2*(e - 1)/e

    # chi = chi_0_zeta_0
    g_0 = 1.0/(1.0 + chi_2*s**2 + chi_42*s**4)
    return params['B1C']*np.log(1 - g_0*(e-1)/e)

def eps_c_1_high_dens_zeta_0(params, s):
    chi_0_zeta_0 = BETA_RS_0*(3*pi**2/16.0)**(2.0/3.0)/(C_TILDE*GAMMA)*np.exp(-C1_CONST/GAMMA)
    chi = chi_0_zeta_0
    g_0 = 1.0/(1.0 + chi*s**2 + chi**2*s**4)
    return GAMMA*np.log(1 - g_0)


def working_eps_c_0(params, s):
    """
    Minimal epsilon_c 0 required for working.
    """

    g_0 = 1.0/(1.0 + params['CHI_2']*s**2 + params['CHI_42']*s**4)

    return params['B1C']*np.log(1 - g_0*(e-1)/e)

def lsda_0(params, rs):
    sqrtrs = np.sqrt(rs)
    EC_lsda0 = -params['B1C']/(1.0 + params['B2C']*sqrtrs + params['B3C']*rs)
    return EC_lsda0

def lsda_1(rs, zeta):
    GAM = 0.5198421  # 2^(4/3)-2
    FZZ = 1.709921
    sqrt_rs = np.sqrt(rs)

    EU = gcor2(0.03109070, 0.213700, 7.59570, 3.58760, 1.63820, 0.492940, sqrt_rs)
    EP = gcor2(0.015545350, 0.205480, 14.11890, 6.19770, 3.36620, 0.625170, sqrt_rs)
    ALFM = gcor2(0.01688690, 0.111250, 10.3570, 3.62310, 0.880260, 0.496710, sqrt_rs)

    # ALFC = -ALFM
    Z4 = zeta**4

    # LDA part of the energy
    F = ((1.0 + zeta)**(4.0/3.0) + (1.0 - zeta)**(4.0/3.0) - 2.0)/GAM
    EC = EU*(1.0 - F*Z4) + EP*F*Z4 - ALFM*F*(1.0 - Z4)/FZZ

    return EC

def eps_c_0_high_dens_zeta_0_original(params, s):
    """
    Assuming zeta = 0
    """

    cx = -(3.0/(4.0*pi))*(9.0*pi/4.0)**(1.0/3.0)
    beta_inf = BETA_RS_0*AFACTOR/BFACTOR
    chi_inf = (3.0*pi**2/16.0)**(2.0/3.0)*beta_inf/(cx - F0)  # checked
    g_inf = 1.0/np.power(1.0 + 4*chi_inf*s**2, 1.0/4.0)
    return params['B1C']*np.log(1.0 - g_inf*np.expm1(1)/np.exp(1))



def ms0_h0x(params, p):
    """
    h0x for ms0
    """
    return 1 + K0 - K0/(1+((10.0/81.0)*p+params['C'])/K0)
