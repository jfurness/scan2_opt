import matplotlib.pyplot as plt
import numpy as np

def get_func(beta, p):
    func = 1.0
    for i in range(1, len(p)+1):
        func += p[i-1]*beta**i
    func *= (1 - 2*beta)
    return func

previous_good = [-1.6, -10, 46.5, -30.5]

negative = [-0.938568829,    -8.350725202,    16.69840956, 15.50070379]

params25 = [ -1.43192946,  -2.52140548,   2.47163193,   5.45320492,  11.73953297,
   5.7004713 ,  -8.47002815,  -0.83524881, -16.71575815,  -7.94973787,
   2.43076337,  -3.02394517,   6.52993602,  -3.52518411,  -5.45222297,
  -2.4954013 ,   1.25263748,  19.42944905,   9.60436292,  -5.36431614,
   6.67698685,   7.30470314,  -5.17345622,  -8.8785607,   -5.75709301]

params7 = [-2.237299841, 0.449762565, 2.779056591, -8.99E-07,   0.002268028, 0.002795321, 0.001693309]

params13 = [-0.79903157, -4.19767344, -11.84481596, 32.15596516, 57.71564026, \
-45.24645626, -48.93234418, 37.75413129, -99.59586221, 62.07268402, \
-38.00871823, 36.10660137, 76.12252771, 9.422603, -61.72524438]

print((-(1+np.sum(params25))))

beta = np.linspace(0.0, 1.0, 500)
func25 = get_func(beta, params25)
func7 = get_func(beta, params7)
func13 = get_func(beta, params13)
funcneg = get_func(beta, negative)
funcprev = get_func(beta, previous_good)


fig = plt.figure()
ax = fig.add_subplot(111)

ax.axhline(c = 'gray', ls=':')
ax.set_xlim([0, 1.0])
ax.set_ylim([-2, 1])
ax.plot(beta, funcprev, label="simple 4")
ax.plot(beta, funcneg, label="4 (unbounded)")
ax.plot(beta, func7, label="7")
ax.plot(beta, func13, label="13")
ax.plot(beta, func25, label="25")

ax.legend()
ax.set_xlabel("$\\beta$")
ax.set_ylabel("$f_x(\\beta)$")

plt.show()

