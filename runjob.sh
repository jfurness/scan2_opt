#!/bin/bash
#SBATCH --job-name=scan2opt
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kaplan@temple.edu
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=248
#SBATCH --mem=50gb
#SBATCH --time=06:00:00
#SBATCH --output=scan2_fit_log.txt

python3 Optimiser.py
