import numpy as np
import matplotlib.pyplot as plt

def ori(a):
    out = np.zeros(a.shape)

    c1x = 0.667
    c2x = 0.8
    dx = 1.24
    oma = 1-a
    out[a < 1.0] = np.exp(-c1x*a[a < 1.0]/oma[a < 1.0])
    out[a > 1.0] = -dx*np.exp(c2x/oma[a > 1.0])
    return out

def match(beta):
    out = np.zeros(beta.shape)
    tb = 2*beta
    c1x = 0.65
    c2x = 0.2
    dx = 1.4
    oma = 1-tb
    out[tb < 1.0] = np.exp(-c1x*tb[tb < 1.0]/oma[tb < 1.0])
    out[tb > 1.0] = -dx*np.exp(c2x/oma[tb > 1.0])
    return out

def bet(beta, A, B, C):
    return (1-2*beta)**3*(1+A*beta + B*beta**2 + C*beta**3)

def four(beta, A, B, C, D):
    return (1-2*beta)**3*(1+A*beta + B*beta**2 + C*beta**3 + D*beta**4)


def newpol(beta):
    A = -0.0
    B = 63.125
    C = 63
    return (1 - 2*beta)**3*(1 + A*beta + B*beta**2)/(1 + C*beta**4)

def mod(beta):
    A = -5.5
    B = 65.5
    C = 0.0
    D = 48.19
    return (1 - 2*beta)**3*(1 + A*beta + B*beta**2)/((1 + C*beta**2)**2 + D*beta**4)

def smooth(beta):
    cax = 2.9
    cbx = 0.0
    dx = 0.1
    tb = 2*beta
    num = (1 - tb)**3*(1 + cax*tb + cbx*tb**2)
    den = (1 + tb**3)*(1 + (cbx/dx)*tb**2)
    f_beta = num/den
    return f_beta

def smooth_alpha(alpha):
    cax = 4.1618
    cbx = 1.5433
    dx = 0.09632
    num = (1 - alpha)**3*(1 + cax*alpha + cbx*alpha**2)
    den = (1 + alpha**3)*(1 + (cbx/dx)*alpha**2)
    f_beta = num/den
    return f_beta
inp = np.linspace(0, 1, 200)

# with open('IE_x_filter.csv', 'r') as f:
#     for i, line in enumerate(f):
#         if i != 0:
#             bits = map(float,line.strip().split(','))
#             A = bits[0]
#             B = bits[1]
#             C = bits[2]
#             fx = bet(inp, A, B, C)
#             mu = i/12.0
#             if i%2 == 0:
#                 plt.plot(inp, fx, c=(mu,0,1-mu),label='A:{:},B:{:},C:{:}'.format(A,B,C))
#             else: 
#                 plt.plot(inp, fx, c=(mu,0,1-mu),ls=":",label='A:{:},B:{:},C:{:}'.format(A,B,C))
# plt.xlim([0,1])
# plt.legend()
# plt.show()
# raise SystemExit
xA = [-4.6, -6.0, -5.2]
xB = [64, 60, 68]
xC = [-56, -28, -20]
xD = [10, 58, -32]

xA = [ -0.185]
xB = [133.282]
xC = [-457.582]
xD = [508.533]

cA = [-10.2, -10.8, -6.8]
cB = [38, 42, 26]
cC = [-60, -58, 58]
cD = [34, 56, 50]

cA = [-5.577]
cB = [168.427]
cC = [-2.383]
cD = [-10.285]

fig = plt.figure()
ax = fig.add_subplot(121)
ax2 = fig.add_subplot(122)

cs = ['r','g','b']
lss= ["-",'--',':']

# plt.plot(inp,f_al, label='$f(2\\alpha)$')
for i, a in enumerate(["Spherical"]):#, "Non-Spherical", "Hollow"]):
    ax.plot(inp, four(inp, xA[i], xB[i], xC[i], xD[i]), label="Exchange", c=cs[i], ls = lss[i])
    ax2.plot(inp, four(inp, cA[i], cB[i], cC[i], cD[i]), label="Correlation", c=cs[i], ls = lss[i])
# plt.plot(inp, mab, label='$f(\\beta)_{\mathrm{SCAN}}$')
# plt.plot(inp, smo, label='$f(\\beta)_{\mathrm{SMOOTH}}$')
# plt.plot(inp, sma, label='$f(2\\alpha)_{\mathrm{SMOOTH}}$')
ax.hlines(0.0, 0, 1,linestyles='--')
ax2.hlines(0.0, 0, 1,linestyles='--')
ax.vlines(0.5, -7, 1.1,linestyles='--',colors='gray')
ax2.vlines(0.5, -7, 1.1,linestyles='--',colors='gray')
# plt.title("Cor. A: {:}, B: {:}, C: {:}, D: {:} $f(\\beta = 1) = {:}$\nEx. A: {:}, B: {:}, C: {:}, D: {:} $f(\\beta = 1) = {:}$".format(cA, cB, cC, cD, four(1, cA, cB, cC, cD), xA, xB, xC, xD, four(1, xA, xB, xC, xD)))
ax.set_xlim([0,1])
ax2.set_xlim([0,1])
ax.set_ylim([-7, 2.1])
ax2.set_ylim([-7, 2.1])
ax.set_xticks([0.0, 0.2, 0.4, 0.5, 0.6, 0.8, 1.0])
ax2.set_xticks([0.0, 0.2, 0.4, 0.5, 0.6, 0.8, 1.0])
ax.legend()
ax2.legend()
plt.show()

