

subroutine get_pars_from_g1g2(g1,g2,eta,k0,f1,f2,h1,h2)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: g1,g2,eta,k0
  real(dp), intent(out) :: f1,f2,h1,h2

  real(dp) :: fa,fb,fc,rcnd

  fa = 100._dp/243._dp*(4._dp + 9*eta)*k0
  fb = 200._dp/729._dp - 20._dp*(1._dp + k0)*g1/9._dp + 2._dp*k0/243._dp &
  &    + 98._dp*eta*k0/27._dp
  fc = 73._dp*(8._dp + 9*eta)/6075._dp - 73._dp/405._dp

  rcnd = fb**2 - 4*fa*fc
  if (rcnd < 0) then
    f1 = 1.d20
    f2 = 1.d20
    h1 = 1.d20
    h2 = 1.d20
  end if

  rcnd = rcnd**(0.5)
  if ((-fb - rcnd)/(2*fa) <= 0._dp ) then
    f1 = (-fb - rcnd)/(2*fa)
  else if ((-fb + rcnd)/(2*fa) <= 0._dp) then
    f1 = (-fb + rcnd)/(2*fa)
  else
    f1 = 1.d20
    f2 = 1.d20
    h1 = 1.d20
    h2 = 1.d20
    return
  end if

  f2 = 73._dp/(2500._dp*k0) - f1/50._dp
  h1 = 10._dp/81._dp - g1 + 5._dp*(4._dp + 9*eta)*k0*f1/27._dp
  h2 = -g2 - 2._dp*(h1*g1 + 25._dp*(8._dp + 9*eta)**2*k0*f2/1458._dp &
  &    + 5._dp*(8._dp + 9*eta)*(h1*f1 - k0*f1*g1)/27._dp + (6._dp  &
  &    + 75._dp*eta*(8._dp + 9*eta))*k0*f1/243._dp)

end subroutine get_pars_from_g1g2

subroutine spinf(z,n,f,df)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: z,n
  real(dp), intent(out) :: f,df

  real(dp) :: opz,omz

  opz = min(2._dp,max(0._dp,1._dp+z))
  omz = min(2._dp,max(0._dp,1._dp-z))
  f = (opz**n + omz**n)/2._dp
  df = n*(opz**(n-1) - omz**(n-1))/2._dp

end subroutine spinf

subroutine min_mod_r2_revinterp(x,cfs,c2,d,nc,f,df)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  integer, intent(in) :: nc
  real(dp), intent(in) :: x, cfs(nc),c2,d
  real(dp), intent(out) :: f,df

  integer :: ic
  real(dp), parameter :: x_match = 2.5_dp
  real(dp) :: omx

  omx = 1._dp - x
  if (x <= x_match) then
    f = cfs(1)
    df = -nc*cfs(nc)*x**(nc-1)
    do ic = 1,nc-1
      f = f + cfs(1+ic)*x**ic
      df = df + ic*(cfs(1+ic) - cfs(ic) )*x**(ic-1)
    end do
    f = f*omx
  else
    f = -d*exp(c2/omx)
    df = f*c2/omx**2
  end if


end subroutine min_mod_r2_revinterp


subroutine min_mod_r2_x(n0,n1,gn0,gn1,t0,t1,ex,dexdn0,dexdn1,dexdgn0,dexdgn1, &
  &      dexdt0, dexdt1)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n0,n1,gn0,gn1,t0,t1
  real(dp), intent(out) :: ex,dexdn0,dexdn1,dexdgn0,dexdgn1,dexdt0, dexdt1

  real(dp) :: ex0,ex1

  ex = 0._dp ; ex0 = 0._dp ; ex1 = 0._dp
  dexdn0 = 0._dp ; dexdn1 = 0._dp
  dexdgn0 = 0._dp ; dexdgn1 = 0._dp
  dexdt0 = 0._dp ; dexdt1 = 0._dp

  if (n0 > 0._dp) call min_mod_r2_x_unp(2*n0,2*gn0,2*t0,ex0,dexdn0,dexdgn0,dexdt0)
  if (n1 > 0._dp) call min_mod_r2_x_unp(2*n1,2*gn1,2*t1,ex1,dexdn1,dexdgn1,dexdt1)

  ex = (ex0 + ex1)/2._dp

end subroutine min_mod_r2_x


subroutine min_mod_r2_x_unp(n,gn,t,ex,dexdn,dexdgn,dexdt)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n,gn,t
  real(dp), intent(out) :: ex,dexdn,dexdgn,dexdt

  real(dp), parameter :: pi = 3.1415926535897932385_dp
  real(dp), parameter :: eta = 0.129_dp

  real(dp) :: kf, kf2, p, ba, ba_den, d_p_dn, d_p_dgn, d_ba_dn, d_ba_dgn,d_ba_dt
  real(dp) :: t0, d_t0_dn, tw, d_tw_dn, d_tw_dgn
  real(dp) :: fx, d_fx_dp, d_fx_dba, exlda, d_exlda_dn

  kf = (3*pi**2*n)**(1._dp/3._dp)
  kf2 = kf*kf
  exlda = -3._dp/(4._dp*pi)*kf*n
  d_exlda_dn = -kf/pi

  p = (gn/(2*kf*n))**2
  d_p_dn = -8._dp/3._dp*p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  tw = gn**2/(8._dp*n)
  d_tw_dn = -tw/n
  d_tw_dgn = gn/(4._dp*n)

  t0 = 0.3_dp*kf2*n
  d_t0_dn = kf2/2

  ba_den = t0 + eta*tw
  ba = max(0._dp,t - tw)/ba_den
  d_ba_dn = -(d_tw_dn + (d_t0_dn + eta*d_tw_dn)*ba)/ba_den
  d_ba_dgn = -(1._dp + eta*ba)*d_tw_dgn/ba_den
  d_ba_dt = 1._dp/ba_den

  call min_mod_r2_x_enhance(p,ba,fx,d_fx_dp,d_fx_dba)

  ex = exlda*fx
  dexdn = d_exlda_dn*fx + exlda*(d_fx_dp*d_p_dn + d_fx_dba*d_ba_dn)
  dexdgn = exlda*(d_fx_dp*d_p_dgn + d_fx_dba*d_ba_dgn)
  dexdt = exlda*d_fx_dba*d_ba_dt

end subroutine min_mod_r2_x_unp


subroutine min_mod_r2_x_enhance(p,ba,fx,dfxdp,dfxdba)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: p,ba
  real(dp), intent(out) :: fx,dfxdp,dfxdba

  real(dp), parameter :: hx0 = 1.174_dp, c1x = 0.667_dp, c2x = 0.8_dp, dx = 1.24_dp
  real(dp), parameter :: k1 = 0.0173_dp, dp3 = (0.0798_dp)**3, a1x = 6.5543010899064695_dp

  real(dp), parameter :: f1 = -1.1341067299654735_dp, f2 = 0.19049822655333248_dp
  real(dp), parameter :: h1 = -0.0651537656797633_dp, h2 = -0.18656618386850496_dp
  real(dp), parameter :: cfx(10) = (/ 1._dp,0.3330000000014095_dp,&
  &        -0.11155549999856908_dp,-3.5829688591062783_dp,11.821456262207933_dp,&
  &       -15.858852272635602_dp,10.822176662775393_dp,-3.9914555447608073_dp,&
  &         0.7614202183506578_dp,-0.05911423686003714_dp /)

  real(dp) :: gx, d_gx_dp, hx1, hx1_den, d_hx1_dp, ief, d_ief_dba,fxint

  gx = 1._dp/(1._dp + (p/a1x)**2)**(0.125_dp)
  d_gx_dp = -0.25_dp*p/a1x**2*gx**9

  hx1_den = 1._dp + p**3/dp3
  hx1 = (1._dp + h1*p + h2*p**2/2._dp + (1._dp+k1)*p**3/dp3)/hx1_den
  d_hx1_dp = (h1 + h2*p + 3*(1._dp+k1)*p**2/dp3 - 3*p**2/dp3*hx1)/hx1_den

  call min_mod_r2_revinterp(ba,cfx,c2x,dx,10,ief,d_ief_dba)

  fxint = hx1 + ief*(hx0 - hx1)
  fx = fxint*gx
  dfxdp = d_hx1_dp*(1._dp-ief)*gx + fxint*d_gx_dp
  dfxdba = d_ief_dba*(hx0 - hx1)*gx

end subroutine min_mod_r2_x_enhance



subroutine min_mod_r2_c(nu,nd,gn,t,ec,d_ec_dnu,d_ec_dnd,d_ec_dgn,d_ec_dt)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: nu,nd,gn,t
  real(dp), intent(out) :: ec,d_ec_dnu,d_ec_dnd,d_ec_dgn,d_ec_dt

  real(dp), parameter :: pi = 3.1415926535897932385_dp

  real(dp), parameter :: c2c = 1.5_dp, dc = 0.7_dp

  real(dp), parameter :: cfc(8) = (/ 1._dp,0.3599999999999802_dp,&
  &       -0.07520000000001449_dp,-2.9102780325567696_dp,3.5297025324388795_dp,&
  &       -1.7124523194678987_dp,0.38449334136225327_dp,-0.033264446157011726_dp /)
  real(dp), parameter :: eta = 0.129_dp, dfc1 = -sum(cfc)

  real(dp) :: n, rs, d_rs_dn, kf, kf2, z, d_z_dnu, d_z_dnd, p, d_p_dn, d_p_dgn
  real(dp) :: t0, t0_unp, dsz, d_dsz_dz, d_t0_dn, d_t0_dz, tw, d_tw_dn, d_tw_dgn
  real(dp) :: ba, ba_den, d_ba_dn, d_ba_dz, d_ba_dgn, d_ba_dt, ief, omief, d_ief_dba
  real(dp) :: eps0, d_eps0_drs, d_eps0_dz, d_eps0_dp, eps1, d_eps1_drs
  real(dp) :: d_eps1_dz, d_eps1_dp, epsc, d_epsc_drs, d_epsc_dz, d_epsc_dp
  real(dp) :: d_epsc_dba, d_ec_dn, spind

  n = nu + nd

  kf = (3*pi**2*n)**(1._dp/3._dp)
  kf2 = kf*kf
  rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)
  d_rs_dn = -rs/(3._dp*n)

  z = (nu - nd)/n
  z = min(max(-0.99999999999990_dp,z),0.99999999999990_dp)
  d_z_dnu = 2._dp*nd/n**2
  d_z_dnd = -2._dp*nu/n**2

  p = (gn/(2*kf*n))**2
  d_p_dn = -8._dp/3._dp*p/n
  d_p_dgn = gn/(2*(kf*n)**2)

  tw = gn**2/(8._dp*n)
  d_tw_dn = -tw/n
  d_tw_dgn = gn/(4._dp*n)

  call spinf(z,5._dp/3._dp,dsz,d_dsz_dz)
  t0_unp = 0.3_dp*kf2*n
  t0 = t0_unp*dsz
  d_t0_dn = kf2*dsz/2._dp
  d_t0_dz = t0_unp*d_dsz_dz

  ba_den = t0 + eta*tw
  ba = max(0._dp,t - tw)/ba_den
  d_ba_dn = -(d_tw_dn + (d_t0_dn + eta*d_tw_dn)*ba)/ba_den
  d_ba_dz = - d_t0_dz*ba/ba_den
  d_ba_dgn = -(1._dp + eta*ba)*d_tw_dgn/ba_den
  d_ba_dt = 1._dp/ba_den

  call min_mod_r2_revinterp(ba,cfc,c2c,dc,8,ief,d_ief_dba)
  omief = 1._dp - ief

  call min_mod_r2_c_core(rs, z, p, eta, dfc1, eps0, d_eps0_drs, d_eps0_dz, &
    &        d_eps0_dp, eps1, d_eps1_drs, d_eps1_dz, d_eps1_dp)

  epsc = eps1*omief + ief*eps0
  d_epsc_drs = d_eps1_drs*omief + ief*d_eps0_drs
  d_epsc_dz = d_eps1_dz*omief + ief*d_eps0_dz
  d_epsc_dp = d_eps1_dp*omief + ief*d_eps0_dp
  d_epsc_dba = d_ief_dba*(eps0 - eps1)

  ec = n*epsc

  d_ec_dn = epsc + n*(d_epsc_drs*d_rs_dn + d_epsc_dp*d_p_dn + d_epsc_dba*d_ba_dn)
  spind = n*(d_epsc_dz + d_epsc_dba*d_ba_dz)
  d_ec_dnu = d_ec_dn + spind*d_z_dnu
  d_ec_dnd = d_ec_dn + spind*d_z_dnd

  d_ec_dgn = n*(d_epsc_dp*d_p_dgn + d_epsc_dba*d_ba_dgn)

  d_ec_dt = n*d_epsc_dba*d_ba_dt

end subroutine min_mod_r2_c


subroutine min_mod_r2_c_core(rs, z, p, eta, dfc1, eps0, d_eps0_drs, d_eps0_dz, &
  &        d_eps0_dp, eps1, d_eps1_drs, d_eps1_dz, d_eps1_dp)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: rs, z, p, eta, dfc1
  real(dp), intent(out) :: eps0, d_eps0_drs, d_eps0_dz, d_eps0_dp
  real(dp), intent(out) :: eps1, d_eps1_drs, d_eps1_dz, d_eps1_dp

  real(dp), parameter :: pi = 3.1415926535897932385_dp
  real(dp), parameter :: gamma = (1._dp - log(2._dp))/pi**2
  real(dp), parameter :: ct2 = (3._dp*pi**2/16._dp)**(2._dp/3._dp)

  real(dp), parameter :: b1c = 0.028576417228595823_dp, b2c = 0.12372377054435257_dp
  real(dp), parameter :: b3c = 0.12554127065950266_dp
  real(dp), parameter :: b4c = 1.174_dp*(3._dp/4._dp &
  &         *(3._dp/(2._dp*pi))**(2._dp/3._dp))*b3c/b1c

  real(dp), parameter :: beta_0 = 0.066724526517184_dp, beta_a = 0.5_dp
  real(dp), parameter :: beta_b = 1._dp, beta_c = 0.16667_dp, beta_d = 0.29633_dp
  real(dp), parameter :: beta_inf = beta_0*beta_c/beta_d

  real(dp), parameter :: chi_inf = (3*pi**2/16._dp)**(2._dp/3._dp)*beta_inf&
  &         /(0.9_dp - 3._dp*(3._dp/(16._dp*pi))**(2._dp/3._dp))

  real(dp), parameter :: dp4 = (0.0798_dp)**4

  real(dp) :: rsh, f1, d_f1_drs, d_f1_drs2, ec0_lda, d_ec0lda_drs, d_ec0lda_drs2
  real(dp) :: dxz, d_dxz_dz, gcz, d_gcz_dz, gc1, gc2
  real(dp) :: ecl0, d_ecl0_drs, d_ecl0_dz, d_ecl0_drs2, d_ecl0_drsdz
  real(dp) :: w0, d_w0_drs, gf_inf, d_gf_inf_dp
  real(dp) :: hcore0, h0, d_h0_drs, d_h0_dp

  real(dp) :: brs_den, brs, d_brs_drs, phi, d_phi_dz, gp3
  real(dp) :: t2, d_t2_drs, d_t2_dz, d_t2_dp
  real(dp) :: ecl1, d_ecl1_drs, d_ecl1_dz, d_ecl1_drs2, d_ecl1_drsdz
  real(dp) :: w1, wexp, d_w1_drs, d_w1_dz, aa, y, d_y_drs, d_y_dz, d_y_dp
  real(dp) :: dsz, d_dsz_dz, dyfac, d_dyfac_drs, d_dyfac_dz
  real(dp) :: dyc, d_dyc_drs, d_dyc_dz, damp, dydamp, d_dydamp_dp
  real(dp) :: dy, d_dy_drs, d_dy_dz, d_dy_dp
  real(dp) :: gt, omgt, dgt, d_gt_drs, d_gt_dz, d_gt_dp
  real(dp) :: hcore1, h1, d_h1_drs, d_h1_dz, d_h1_dp

!===============================================================================
!             alpha_bar = 0 energy density
!===============================================================================

  rsh = rs**(0.5_dp)

  f1 = 1._dp + b2c*rsh + b3c*rs
  d_f1_drs = b2c/(2*rsh) + b3c
  d_f1_drs2 = -b2c/(4*rsh**3)

  ec0_lda = -b1c/f1
  d_ec0lda_drs = b1c*d_f1_drs/f1**2
  d_ec0lda_drs2 = -2*d_ec0lda_drs*d_f1_drs/f1 + b1c*d_f1_drs2/f1**2

  call spinf(z, 4._dp/3._dp, dxz, d_dxz_dz)
  gc1 = 1._dp - b4c*(dxz - 1._dp)
  gc2 = 1._dp - z**(12)
  gcz = gc1*gc2
  d_gcz_dz = -b4c*d_dxz_dz*gc2 - 12*z**(11)*gc1

  ecl0 = ec0_lda*gcz
  d_ecl0_drs = d_ec0lda_drs*gcz
  d_ecl0_dz = ec0_lda*d_gcz_dz
  d_ecl0_drs2 = d_ec0lda_drs2*gcz
  d_ecl0_drsdz = d_ec0lda_drs*d_gcz_dz

  w0 = exp(-ec0_lda/b1c) - 1._dp
  d_w0_drs = -d_ec0lda_drs/b1c*(w0 + 1._dp)

  gf_inf = 1._dp/(1._dp + 4._dp*chi_inf*p)**(1._dp/4._dp)
  d_gf_inf_dp = -chi_inf*gf_inf**5

  hcore0 = 1._dp + w0*(1._dp - gf_inf)
  h0 = b1c*log(hcore0)
  d_h0_drs = b1c*d_w0_drs*(1._dp - gf_inf)/hcore0
  d_h0_dp = -b1c*w0*d_gf_inf_dp/hcore0

  eps0 = (ec0_lda + h0)*gcz
  d_eps0_drs = (d_ec0lda_drs + d_h0_drs)*gcz
  d_eps0_dz = (ec0_lda + h0)*d_gcz_dz
  d_eps0_dp = d_h0_dp*gcz

!===============================================================================
!             alpha_bar = 1 energy density
!===============================================================================

  brs_den = 1._dp + beta_a*rs*(1 + beta_d*rs)
  brs = beta_0*(1._dp + beta_a*rs*(beta_b + beta_c*rs) )/brs_den
  d_brs_drs = beta_a*( beta_0*(beta_b + 2*beta_c*rs) &
  &         - (1._dp + 2*beta_d*rs)*brs )/brs_den

  call spinf(z, 2._dp/3._dp, phi, d_phi_dz)
  gp3 = gamma*phi**3

  t2 = ct2*p/(phi**2*rs)
  d_t2_drs = -t2/rs
  d_t2_dz = -2*t2*d_phi_dz/phi
  d_t2_dp = ct2/(phi**2*rs)

  call ec_pw92_w_extra_derivs(rs,z,ecl1,d_ecl1_drs, d_ecl1_dz,d_ecl1_drs2,&
  &    d_ecl1_drsdz)

  wexp = exp(-ecl1/gp3)
  w1 = wexp - 1._dp
  d_w1_drs = -d_ecl1_drs/gp3*wexp
  d_w1_dz = (-d_ecl1_dz + 3*ecl1*d_phi_dz/phi)*wexp/gp3

  aa = brs/(gamma*w1)
  y = aa*t2
  d_y_drs = ( d_brs_drs/(gamma*w1) - d_w1_drs*aa/w1 )*t2 + aa*d_t2_drs
  d_y_dz = -d_w1_dz*aa*t2/w1 + aa*d_t2_dz
  d_y_dp = aa*d_t2_dp

  call spinf(z, 5._dp/3._dp, dsz, d_dsz_dz)

  dyfac = dfc1/(27._dp*dsz*gp3*w1)
  d_dyfac_drs = -dyfac*d_w1_drs/w1
  d_dyfac_dz = -dyfac*(d_dsz_dz/dsz + 3*d_phi_dz/phi + d_w1_dz/w1)

  dyc = 20*rs*(d_ecl0_drs - d_ecl1_drs) - 45*eta*(ecl0 - ecl1)
  d_dyc_drs = (20._dp - 45*eta)*(d_ecl0_drs - d_ecl1_drs) + 20*rs*(d_ecl0_drs2 - d_ecl1_drs2)
  d_dyc_dz = 20*rs*(d_ecl0_drsdz - d_ecl1_drsdz) - 45*eta*(d_ecl0_dz - d_ecl1_dz)

  damp = exp(-p**2/dp4)
  dydamp = p*damp
  d_dydamp_dp = (1._dp - 2*p**2/dp4)*damp

  dy = dyfac*dyc*dydamp
  d_dy_drs = d_dyfac_drs*dyc*dydamp + dyfac*d_dyc_drs*dydamp
  d_dy_dz = d_dyfac_dz*dyc*dydamp + dyfac*d_dyc_dz*dydamp
  d_dy_dp = dyfac*dyc*d_dydamp_dp

  gt = 1._dp/(1._dp + 4*(y - dy))**(0.25_dp)
  omgt = 1._dp - gt
  dgt = -gt**5
  d_gt_drs = dgt*(d_y_drs - d_dy_drs)
  d_gt_dz = dgt*(d_y_dz - d_dy_dz)
  d_gt_dp = dgt*(d_y_dp - d_dy_dp)

  hcore1 = 1._dp + w1*omgt
  h1 = gp3*log(hcore1)
  d_h1_drs = gp3*(d_w1_drs*omgt - w1*d_gt_drs)/hcore1
  d_h1_dz = 3*h1*d_phi_dz/phi + gp3*(d_w1_dz*omgt - w1*d_gt_dz)/hcore1
  d_h1_dp = -gp3*w1*d_gt_dp/hcore1

  eps1 = ecl1 + h1
  d_eps1_drs = d_ecl1_drs + d_h1_drs
  d_eps1_dz = d_ecl1_dz + d_h1_dz
  d_eps1_dp = d_h1_dp

end subroutine min_mod_r2_c_core


subroutine ec_pw92_w_extra_derivs(rs,z,ec,d_ec_drs, d_ec_dz,d_ec_drs2,d_ec_drsdz)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: rs,z
  real(dp), intent(out) :: ec,d_ec_drs, d_ec_dz,d_ec_drs2,d_ec_drsdz

  real(dp), parameter :: fz_den = (2._dp**(4._dp/3._dp)-2._dp),fdd0 = 8._dp/(9*fz_den)
  real(dp), parameter :: unp_pars(6) = (/0.031091_dp,0.21370_dp,7.5957_dp,&
  &         3.5876_dp,1.6382_dp,0.49294_dp /)
  real(dp), parameter :: pol_pars(6) = (/ 0.015545_dp,0.20548_dp,14.1189_dp,&
  &         6.1977_dp,3.3662_dp,0.62517_dp /)
  real(dp), parameter :: alp_pars(6) = (/ 0.016887_dp,0.11125_dp,10.357_dp,&
  &         3.6231_dp,0.88026_dp,0.49671_dp /)

  real(dp) :: ec0,d_ec0_drs,d_ec0_drs2, ec1,d_ec1_drs,d_ec1_drs2
  real(dp) :: ac,d_ac_drs,d_ac_drs2, fz, d_fz_dz, z4, fzz4, dxz, d_dxz_dz

  call spinf(z, 4._dp/3._dp, dxz, d_dxz_dz)
  fz = 2._dp*(dxz - 1._dp)/fz_den
  d_fz_dz = 2._dp*d_dxz_dz/fz_den

  z4 = z**4
  fzz4 = fz*z4

  call ec_pw92_g_w_2nd_derivs(rs,unp_pars,ec0,d_ec0_drs,d_ec0_drs2)
  call ec_pw92_g_w_2nd_derivs(rs,pol_pars,ec1,d_ec1_drs,d_ec1_drs2)
  call ec_pw92_g_w_2nd_derivs(rs,alp_pars,ac,d_ac_drs,d_ac_drs2)

  ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

  d_ec_drs = d_ec0_drs*(1._dp - fzz4) + d_ec1_drs*fzz4 - d_ac_drs/fdd0*(fz - fzz4)
  d_ec_dz = -ac*d_fz_dz/fdd0 + (4*fz*z**3 + d_fz_dz*z4)*(ac/fdd0 + ec1 - ec0)

  d_ec_drs2 = d_ec0_drs2*(1._dp - fzz4) + d_ec1_drs2*fzz4 - d_ac_drs2/fdd0 &
  &         *(fz - fzz4)
  d_ec_drsdz = -d_ac_drs*d_fz_dz/fdd0 + (4*fz*z**3 + d_fz_dz*z4)*(d_ac_drs/fdd0 &
  &          + d_ec1_drs - d_ec0_drs)

end subroutine ec_pw92_w_extra_derivs



subroutine ec_pw92_g_w_2nd_derivs(rs,pars,g,dg,ddg)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: rs, pars(6)
  real(dp), intent(out) :: g,dg,ddg

  real(dp) :: rsh,q0,dq0,q1,dq1,ddq1,q2,dq2,ddq2

  rsh = rs**(0.5_dp)

  q0 = -2*pars(1)*(1._dp + pars(2)*rs)
  dq0 = -2*pars(1)*pars(2)

  q1 = 2*pars(1)*(pars(3)*rsh + pars(4)*rs + pars(5)*rs*rsh + pars(6)*rs*rs )
  dq1 = pars(1)*(pars(3)/rsh + 2*pars(4) + 3*pars(5)*rsh + 4*pars(6)*rs )
  ddq1 = pars(1)*(-0.5_dp*pars(3)/rsh**3 + 3._dp/2._dp*pars(5)/rsh + 4*pars(6) )

  q2 = log(1._dp + 1._dp/q1)
  dq2 = -dq1/(q1**2 + q1)
  ddq2 = (dq1**2*(1._dp + 2*q1)/(q1**2 + q1) - ddq1)/(q1**2 + q1)

  g = q0*q2
  dg = dq0*q2 + q0*dq2
  ddg = 2*dq0*dq2 + q0*ddq2

end subroutine ec_pw92_g_w_2nd_derivs
