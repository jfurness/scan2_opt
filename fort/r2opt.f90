
subroutine spinf(z,n,f,df)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: z,n
  real(dp), intent(out) :: f,df

  real(dp) :: opz,omz

  opz = min(2._dp,max(0._dp,1._dp+z))
  omz = min(2._dp,max(0._dp,1._dp-z))
  f = (opz**n + omz**n)/2._dp
  df = n*(opz**(n-1) - omz**(n-1))/2._dp

end subroutine spinf


subroutine smth_interp(x,c0,c1,d,f,df)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: x,c0,c1,d
  real(dp), intent(out) :: f,df

  real(dp) :: fden,fnum,x2,opc1

  x2 = x*x
  opc1 = 1._dp + c1
  fden = 1._dp + (c0 + c1)*x + opc1*x2/d
  fnum = 1._dp + c1*x - opc1*x2

  f = fnum/fden
  df = (c1 - 2._dp*opc1*x - (c0 + c1 + 2._dp*opc1*x/d)*f)/fden

end subroutine smth_interp


subroutine r2opt_x(n0,n1,gn0,gn1,t0,t1,ex,dexdn0,dexdn1,dexdgn0,dexdgn1,&
  &    dexdt0,dexdt1)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n0,n1,gn0,gn1,t0,t1
  real(dp), intent(out) :: ex,dexdn0,dexdn1,dexdgn0,dexdgn1,dexdt0,dexdt1

  real(dp) :: ex0,ex1

  call r2opt_x_unp(2._dp*n0,2._dp*gn0,2._dp*t0,ex0,dexdn0,dexdgn0,dexdt0)
  call r2opt_x_unp(2._dp*n1,2._dp*gn1,2._dp*t1,ex1,dexdn1,dexdgn1,dexdt1)

  ex = (ex0 + ex1)/2._dp

end subroutine r2opt_x


subroutine r2opt_x_unp(n,gn,t,ex,dexdn,dexdgn,dexdt)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n,gn,t
  real(dp), intent(out) :: ex,dexdn,dexdgn,dexdt

  real(dp), parameter :: eta = 0.000761715040622921_dp
  real(dp), parameter :: pi = 3.141592653589793238_dp

  real(dp) :: kf,kf2,exlda,d_exlda_dn,p,d_p_dn,d_p_dgn
  real(dp) :: tw, d_tw_dn, d_tw_dgn, t0, d_t0_dn
  real(dp) :: ba_den,ba,d_ba_dn,d_ba_dgn,d_ba_dt
  real(dp) :: fx,d_fx_dp,d_fx_dba


  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  kf2 = kf*kf
  exlda = -3._dp/(4._dp*pi)*kf*n
  d_exlda_dn = -kf/pi

  p = (gn/(2._dp*kf*n))**2
  d_p_dn = -8._dp/3._dp*p/n
  d_p_dgn = gn/(2._dp*(kf*n)**2)

  tw = gn**2/(8._dp*n)
  d_tw_dn = -tw/n
  d_tw_dgn = gn/(4._dp*n)

  t0 = 0.3_dp*kf2*n
  d_t0_dn = kf2/2._dp

  ba_den = t0 + eta*tw
  ba = max(0._dp,t - tw)/ba_den
  d_ba_dn = -(d_tw_dn + (d_t0_dn + eta*d_tw_dn)*ba)/ba_den
  d_ba_dgn = -(1._dp + eta*ba)*d_tw_dgn/ba_den
  d_ba_dt = 1._dp/ba_den

  call r2opt_x_enhance(p,ba,fx,d_fx_dp,d_fx_dba)

  ex = exlda*fx
  dexdn = d_exlda_dn*fx + exlda*(d_fx_dp*d_p_dn + d_fx_dba*d_ba_dn)
  dexdgn = exlda*(d_fx_dp*d_p_dgn + d_fx_dba*d_ba_dgn)
  dexdt = exlda*d_fx_dba*d_ba_dt


end subroutine r2opt_x_unp


subroutine r2opt_x_enhance(p,ba,fx,dfxdp,dfxdba)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: p,ba
  real(dp), intent(out) :: fx,dfxdp,dfxdba

  real(dp), parameter :: eta = 0.000761715040622921_dp, cdp = 0.292874872100811_dp
  real(dp), parameter :: k1 = 0.0226718754115067_dp, f0 = 0.232556985363938_dp
  real(dp), parameter :: dx = 0.951527038472615_dp, delta = 0.0287736922890905_dp
  real(dp), parameter :: a1 = 4.94869866000824_dp

  real(dp), parameter :: k0 = 0.174_dp, hx0 = 1.174_dp, mu = 10._dp/81._dp
  real(dp), parameter :: f1 = 27._dp*(delta-1._dp)*mu/(5._dp*(4._dp + 9._dp*eta)*k0)
  real(dp), parameter :: h1 = mu*delta, dp4 = cdp**4
  real(dp), parameter :: c1 = -(f1 + dx*(2._dp + f1*(1._dp + f0)))/(f1 + dx*(f1 + 1._dp))

  real(dp) :: gexp,gx,dgxdp, pscl2, hx1_num, hx1_den, hx1, dhx1dp
  real(dp) :: ief, diefdba, fx_int

  gexp = 0._dp
  if (p > 1.d-16) gexp = exp(-a1/p**(0.25_dp))

  gx = 1._dp - gexp
  dgxdp = -0.25_dp*a1*gexp/p**(1.25_dp)

  pscl2 = p**2/dp4
  hx1_num = 1._dp + mu*p + (1._dp  + k1)*pscl2
  hx1_den = 1._dp + (mu-h1)*p + pscl2
  hx1 = hx1_num/hx1_den
  dhx1dp = (mu + 2._dp*(1._dp + k1)*p/dp4 - (mu - h1 + 2._dp*p/dp4)*hx1)/hx1_den

  call smth_interp(ba,f0,c1,dx,ief,diefdba)

  fx_int = hx1 + ief*(hx0 - hx1)
  fx = fx_int*gx

  dfxdp = dhx1dp*(1._dp-ief)*gx + fx_int*dgxdp
  dfxdba = diefdba*(hx0 - hx1)*gx

end subroutine r2opt_x_enhance



subroutine r2opt_c(n0,n1,gn,t,ec,decdn0,decdn1, decdgn, decdt)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: n0,n1,gn,t
  real(dp), intent(out) :: ec,decdn0,decdn1,decdgn,decdt

  real(dp), parameter :: eta = 0.000761715040622921_dp, cdp = 0.292874872100811_dp
  real(dp), parameter :: c0c = 0.149624227149202_dp, c1c = -0.0711247560191589_dp
  real(dp), parameter :: dc = 0.519103924896288_dp

  real(dp), parameter :: pi = 3.141592653589793238_dp
  real(dp), parameter :: dfc1 = (c1c - 2._dp*(1._dp + c1c))/(1._dp &
  &             + c0c + c1c + (1._dp+c1c)/dc)

  real(dp) :: n, zeta, d_zeta_dn0, d_zeta_dn1, rs, d_rs_dn
  real(dp) :: kf, kf2, p, d_p_dn, d_p_dgn, tw, d_tw_dn, d_tw_dgn
  real(dp) :: t0_unp,t0, d_t0_dn, d_t0_dz, dsz, d_dsz_dz
  real(dp) :: ba_den, ba, d_ba_dn, d_ba_dz , d_ba_dgn, d_ba_dt
  real(dp) :: ec0,d_ec0_drs,d_ec0_dz,d_ec0_dp, ec1, d_ec1_drs, d_ec1_dz, d_ec1_dp
  real(dp) :: ief, d_ief_dba, omief
  real(dp) :: epsc, d_epsc_drs, d_epsc_dz, d_epsc_dp, d_epsc_da
  real(dp) :: d_ec_dn, spind

  n = n0 + n1
  zeta = min(max((n0 - n1)/n,-0.99999999999990_dp),0.99999999999990_dp)
  d_zeta_dn0 = 2._dp*n1/n**2
  d_zeta_dn1 = -2._dp*n0/n**2

  rs = (3._dp/(4._dp*pi*n))**(1._dp/3._dp)
  d_rs_dn = -rs/(3._dp*n)

  kf = (3._dp*pi**2*n)**(1._dp/3._dp)
  kf2 = kf*kf

  p = (gn/(2._dp*kf*n))**2
  d_p_dn = -8._dp/3._dp*p/n
  d_p_dgn = gn/(2._dp*(kf*n)**2)

  tw = gn**2/(8._dp*n)
  d_tw_dn = -tw/n
  d_tw_dgn = gn/(4._dp*n)

  call spinf(zeta,5._dp/3._dp,dsz,d_dsz_dz)
  t0_unp = 0.3_dp*kf2*n
  t0 = t0_unp*dsz
  d_t0_dn = 0.5_dp*kf2*dsz
  d_t0_dz = t0_unp*d_dsz_dz

  ba_den = t0 + eta*tw
  ba = max(0._dp, t - tw)/ba_den
  d_ba_dn = -(d_tw_dn + (d_t0_dn + eta*d_tw_dn)*ba)/ba_den
  d_ba_dz = -d_t0_dz*ba/ba_den
  d_ba_dgn = -(1._dp + eta*ba)*d_tw_dgn/ba_den
  d_ba_dt = 1._dp/ba_den

  call r2opt_c_gga(rs,zeta,p,eta,dfc1,ec0,d_ec0_drs,d_ec0_dz,d_ec0_dp, ec1,&
  &                d_ec1_drs, d_ec1_dz, d_ec1_dp)

  call smth_interp(ba,c0c,c1c,dc,ief,d_ief_dba)
  omief = 1._dp - ief

  epsc = ec1*omief + ief*ec0

  d_epsc_drs = d_ec1_drs*omief + ief*d_ec0_drs
  d_epsc_dz = d_ec1_dz*omief + ief*d_ec0_dz
  d_epsc_dp = d_ec1_dp*omief + ief*d_ec0_dp
  d_epsc_da = d_ief_dba*(ec0 - ec1)

  ec = n*epsc

  d_ec_dn = epsc + n*(d_epsc_drs*d_rs_dn + d_epsc_dp*d_p_dn &
  &       + d_epsc_da*d_ba_dn)
  spind = n*(d_epsc_dz + d_epsc_da*d_ba_dz)

  decdn0 = d_ec_dn + spind*d_zeta_dn0
  decdn1 = d_ec_dn + spind*d_zeta_dn1

  decdgn = n*(d_epsc_dp*d_p_dgn + d_epsc_da*d_ba_dgn)

  decdt = n*d_epsc_da*d_ba_dt

end subroutine r2opt_c


subroutine r2opt_c_gga(rs,z,p,eta,dfc1,eps0,d_eps0_drs,d_eps0_dz,d_eps0_dp, &
  &                    eps1, d_eps1_drs, d_eps1_dz, d_eps1_dp)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: rs,z,p,eta, dfc1
  real(dp), intent(out) :: eps0,d_eps0_drs,d_eps0_dz,d_eps0_dp
  real(dp), intent(out) :: eps1, d_eps1_drs, d_eps1_dz, d_eps1_dp


  real(dp), parameter :: pi = 3.1415926535897932385_dp
  real(dp), parameter :: gamma = (1._dp - log(2._dp))/pi**2
  real(dp), parameter :: ct2 = (3._dp*pi**2/16._dp)**(2._dp/3._dp)

  real(dp), parameter :: cdp = 0.292874872100811_dp

  real(dp), parameter :: b1c = 0.0285764172285958_dp, b2c = 0.107547000252794_dp
  real(dp), parameter :: b3c = 0.125541270659502_dp
  real(dp), parameter :: b4c = 1.174_dp*(3._dp/4._dp &
  &         *(3._dp/(2._dp*pi))**(2._dp/3._dp))*b3c/b1c

  real(dp), parameter :: beta_0 = 0.066724526517184_dp, beta_a = 0.5_dp
  real(dp), parameter :: beta_b = 1._dp, beta_c = 0.16667_dp, beta_d = 0.29633_dp
  real(dp), parameter :: beta_inf = beta_0*beta_c/beta_d

  real(dp), parameter :: chi_inf = ct2*beta_inf/(0.9_dp - 3._dp*(3._dp/(16._dp*pi))**(2._dp/3._dp))

  real(dp), parameter :: dp4 = cdp**4

  real(dp) :: rsh, f1, d_f1_drs, d_f1_drs2, ec0_lda, d_ec0lda_drs, d_ec0lda_drs2
  real(dp) :: dxz, d_dxz_dz, gcz, d_gcz_dz, gc1, gc2
  real(dp) :: ecl0, d_ecl0_drs, d_ecl0_dz, d_ecl0_drs2, d_ecl0_drsdz
  real(dp) :: w0exp, w0, d_w0_drs, gf_inf, omgf_inf, d_omgf_inf_dp
  real(dp) :: hcore0, h0, d_h0_drs, d_h0_dp

  real(dp) :: brs_den, brs, d_brs_drs, phi, d_phi_dz, gp3
  real(dp) :: t2, d_t2_drs, d_t2_dz, d_t2_dp
  real(dp) :: ecl1, d_ecl1_drs, d_ecl1_dz, d_ecl1_drs2, d_ecl1_drsdz
  real(dp) :: w1, wexp, d_w1_drs, d_w1_dz, aa, y, d_y_drs, d_y_dz, d_y_dp
  real(dp) :: dsz, d_dsz_dz, dyfac, d_dyfac_drs, d_dyfac_dz
  real(dp) :: dyc, d_dyc_drs, d_dyc_dz, damp, dydamp, d_dydamp_dp
  real(dp) :: dy, d_dy_drs, d_dy_dz, d_dy_dp
  real(dp) :: gt, omgt, dgt, d_omgt_drs, d_omgt_dz, d_omgt_dp
  real(dp) :: hcore1, h1, d_h1_drs, d_h1_dz, d_h1_dp

!===============================================================================
!             alpha_bar = 0 energy density
!===============================================================================

  rsh = rs**(0.5_dp)

  f1 = 1._dp + b2c*rsh + b3c*rs
  d_f1_drs = b2c/(2._dp*rsh) + b3c
  d_f1_drs2 = -b2c/(4._dp*rsh**3)

  ec0_lda = -b1c/f1
  d_ec0lda_drs = b1c*d_f1_drs/f1**2
  d_ec0lda_drs2 = -2._dp*d_ec0lda_drs*d_f1_drs/f1 + b1c*d_f1_drs2/f1**2

  call spinf(z, 4._dp/3._dp, dxz, d_dxz_dz)
  gc1 = 1._dp - b4c*(dxz - 1._dp)
  gc2 = 1._dp - z**(12)
  gcz = gc1*gc2
  d_gcz_dz = -b4c*d_dxz_dz*gc2 - 12._dp*z**(11)*gc1

  ecl0 = ec0_lda*gcz
  d_ecl0_drs = d_ec0lda_drs*gcz
  d_ecl0_dz = ec0_lda*d_gcz_dz
  d_ecl0_drs2 = d_ec0lda_drs2*gcz
  d_ecl0_drsdz = d_ec0lda_drs*d_gcz_dz

  w0exp = exp(-ec0_lda/b1c)
  w0 = w0exp - 1._dp
  d_w0_drs = -d_ec0lda_drs/b1c*w0exp

  gf_inf = 1._dp/(1._dp + 4._dp*chi_inf*p)**(1._dp/4._dp)
  omgf_inf = 1._dp - gf_inf
  d_omgf_inf_dp = chi_inf*gf_inf**5

  hcore0 = 1._dp + w0*omgf_inf
  h0 = b1c*log(hcore0)
  d_h0_drs = b1c*d_w0_drs*omgf_inf/hcore0
  d_h0_dp = b1c*w0*d_omgf_inf_dp/hcore0

  eps0 = (ec0_lda + h0)*gcz
  d_eps0_drs = (d_ec0lda_drs + d_h0_drs)*gcz
  d_eps0_dz = (ec0_lda + h0)*d_gcz_dz
  d_eps0_dp = d_h0_dp*gcz

!===============================================================================
!             alpha_bar = 1 energy density
!===============================================================================

  brs_den = 1._dp + beta_a*rs*(1._dp + beta_d*rs)
  brs = beta_0*(1._dp + beta_a*rs*(beta_b + beta_c*rs) )/brs_den
  d_brs_drs = beta_a*( beta_0*(beta_b + 2._dp*beta_c*rs) &
  &         - (1._dp + 2._dp*beta_d*rs)*brs )/brs_den

  call spinf(z, 2._dp/3._dp, phi, d_phi_dz)
  gp3 = gamma*phi**3

  t2 = ct2*p/(phi**2*rs)
  d_t2_drs = -t2/rs
  d_t2_dz = -2._dp*t2*d_phi_dz/phi
  d_t2_dp = ct2/(phi**2*rs)

  call ec_pw92_w_extra_derivs(rs,z,ecl1,d_ecl1_drs, d_ecl1_dz,d_ecl1_drs2,&
  &    d_ecl1_drsdz)

  wexp = exp(-ecl1/gp3)
  w1 = wexp - 1._dp
  d_w1_drs = -d_ecl1_drs/gp3*wexp
  d_w1_dz = (-d_ecl1_dz + 3._dp*ecl1*d_phi_dz/phi)*wexp/gp3

  aa = brs/(gamma*w1)
  y = aa*t2
  d_y_drs = ( d_brs_drs/(gamma*w1) - d_w1_drs*aa/w1 )*t2 + aa*d_t2_drs
  d_y_dz = -d_w1_dz*aa*t2/w1 + aa*d_t2_dz
  d_y_dp = aa*d_t2_dp

  call spinf(z, 5._dp/3._dp, dsz, d_dsz_dz)

  dyfac = dfc1/(27._dp*dsz*gp3*w1)
  d_dyfac_drs = -dyfac*d_w1_drs/w1
  d_dyfac_dz = -dyfac*(d_dsz_dz/dsz + 3*d_phi_dz/phi + d_w1_dz/w1)

  dyc = 20._dp*rs*(d_ecl0_drs - d_ecl1_drs) - 45._dp*eta*(ecl0 - ecl1)
  d_dyc_drs = (20._dp - 45._dp*eta)*(d_ecl0_drs - d_ecl1_drs) &
  &         + 20._dp*rs*(d_ecl0_drs2 - d_ecl1_drs2)
  d_dyc_dz = 20._dp*rs*(d_ecl0_drsdz - d_ecl1_drsdz) &
  &        - 45._dp*eta*(d_ecl0_dz - d_ecl1_dz)

  damp = exp(-p**2/dp4)
  dydamp = p*damp
  d_dydamp_dp = (1._dp - 2._dp*p**2/dp4)*damp

  dy = dyfac*dyc*dydamp
  d_dy_drs = (d_dyfac_drs*dyc + dyfac*d_dyc_drs)*dydamp
  d_dy_dz = (d_dyfac_dz*dyc + dyfac*d_dyc_dz)*dydamp
  d_dy_dp = dyfac*dyc*d_dydamp_dp

  gt = 1._dp/(1._dp + 4._dp*(y - dy))**(0.25_dp)
  omgt = 1._dp - gt
  dgt = gt**5
  d_omgt_drs = dgt*(d_y_drs - d_dy_drs)
  d_omgt_dz = dgt*(d_y_dz - d_dy_dz)
  d_omgt_dp = dgt*(d_y_dp - d_dy_dp)

  hcore1 = 1._dp + w1*omgt
  h1 = gp3*log(hcore1)
  d_h1_drs = gp3*(d_w1_drs*omgt + w1*d_omgt_drs)/hcore1
  d_h1_dz = 3._dp*h1*d_phi_dz/phi + gp3*(d_w1_dz*omgt + w1*d_omgt_dz)/hcore1
  d_h1_dp = gp3*w1*d_omgt_dp/hcore1

  eps1 = ecl1 + h1
  d_eps1_drs = d_ecl1_drs + d_h1_drs
  d_eps1_dz = d_ecl1_dz + d_h1_dz
  d_eps1_dp = d_h1_dp

end subroutine r2opt_c_gga



subroutine ec_pw92_w_extra_derivs(rs,z,ec,d_ec_drs, d_ec_dz,d_ec_drs2,d_ec_drsdz)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: rs,z
  real(dp), intent(out) :: ec,d_ec_drs, d_ec_dz,d_ec_drs2,d_ec_drsdz

  real(dp), parameter :: fz_den = (2._dp**(4._dp/3._dp)-2._dp),fdd0 = 8._dp/(9*fz_den)
  real(dp), parameter :: unp_pars(6) = (/0.031091_dp,0.21370_dp,7.5957_dp,&
  &         3.5876_dp,1.6382_dp,0.49294_dp /)
  real(dp), parameter :: pol_pars(6) = (/ 0.015545_dp,0.20548_dp,14.1189_dp,&
  &         6.1977_dp,3.3662_dp,0.62517_dp /)
  real(dp), parameter :: alp_pars(6) = (/ 0.016887_dp,0.11125_dp,10.357_dp,&
  &         3.6231_dp,0.88026_dp,0.49671_dp /)

  real(dp) :: ec0,d_ec0_drs,d_ec0_drs2, ec1,d_ec1_drs,d_ec1_drs2
  real(dp) :: ac,d_ac_drs,d_ac_drs2, fz, d_fz_dz, z4, fzz4, dxz, d_dxz_dz

  call spinf(z, 4._dp/3._dp, dxz, d_dxz_dz)
  fz = 2._dp*(dxz - 1._dp)/fz_den
  d_fz_dz = 2._dp*d_dxz_dz/fz_den

  z4 = z**4
  fzz4 = fz*z4

  call ec_pw92_g_w_2nd_derivs(rs,unp_pars,ec0,d_ec0_drs,d_ec0_drs2)
  call ec_pw92_g_w_2nd_derivs(rs,pol_pars,ec1,d_ec1_drs,d_ec1_drs2)
  call ec_pw92_g_w_2nd_derivs(rs,alp_pars,ac,d_ac_drs,d_ac_drs2)

  ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

  d_ec_drs = d_ec0_drs*(1._dp - fzz4) + d_ec1_drs*fzz4 - d_ac_drs/fdd0*(fz - fzz4)
  d_ec_dz = -ac*d_fz_dz/fdd0 + (4*fz*z**3 + d_fz_dz*z4)*(ac/fdd0 + ec1 - ec0)

  d_ec_drs2 = d_ec0_drs2*(1._dp - fzz4) + d_ec1_drs2*fzz4 - d_ac_drs2/fdd0 &
  &         *(fz - fzz4)
  d_ec_drsdz = -d_ac_drs*d_fz_dz/fdd0 + (4._dp*fz*z**3 + d_fz_dz*z4)*(d_ac_drs/fdd0 &
  &          + d_ec1_drs - d_ec0_drs)

end subroutine ec_pw92_w_extra_derivs



subroutine ec_pw92_g_w_2nd_derivs(rs,pars,g,dg,ddg)

  implicit none
  integer, parameter :: dp = selected_real_kind(15, 307)

  real(dp), intent(in) :: rs, pars(6)
  real(dp), intent(out) :: g,dg,ddg

  real(dp) :: rsh,q0,dq0,q1,dq1,ddq1,q2,dq2,ddq2

  rsh = rs**(0.5_dp)

  q0 = -2._dp*pars(1)*(1._dp + pars(2)*rs)
  dq0 = -2._dp*pars(1)*pars(2)

  q1 = 2._dp*pars(1)*(pars(3)*rsh + pars(4)*rs + pars(5)*rs*rsh + pars(6)*rs*rs )
  dq1 = pars(1)*(pars(3)/rsh + 2._dp*pars(4) + 3._dp*pars(5)*rsh + 4._dp*pars(6)*rs )
  ddq1 = pars(1)*(-0.5_dp*pars(3)/rsh**3 + 3._dp/2._dp*pars(5)/rsh + 4*pars(6) )

  q2 = log(1._dp + 1._dp/q1)
  dq2 = -dq1/(q1**2 + q1)
  ddq2 = (dq1**2*(1._dp + 2*q1)/(q1**2 + q1) - ddq1)/(q1**2 + q1)

  g = q0*q2
  dg = dq0*q2 + q0*dq2
  ddg = 2._dp*dq0*dq2 + q0*ddq2

end subroutine ec_pw92_g_w_2nd_derivs
