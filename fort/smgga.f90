

subroutine SMGGA_X(n0,n1,gn0,gn1,t0,t1,ex,dexdn0,dexdn1,dexdgn0,dexdgn1,&
  &    dexdt0,dexdt1)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: n0,n1,gn0,gn1,t0,t1
  real(kdp), intent(out) :: ex,dexdn0,dexdn1,dexdgn0,dexdgn1,dexdt0,dexdt1

  real(kdp) :: ex0,ex1

  call SMGGA_X_UNP(2._kdp*n0,2._kdp*gn0,2._kdp*t0,ex0,dexdn0,dexdgn0,dexdt0)
  call SMGGA_X_UNP(2._kdp*n1,2._kdp*gn1,2._kdp*t1,ex1,dexdn1,dexdgn1,dexdt1)

  ex = (ex0 + ex1)/2._kdp

end subroutine SMGGA_X


subroutine SMGGA_X_UNP(n,gn,t,ex,dexdn,dexdgn,dexdt)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: n,gn,t
  real(kdp), intent(out) :: ex,dexdn,dexdgn,dexdt

  real(kdp), parameter :: pi = 3.141592653589793238_kdp

  real(kdp) :: kf,kf2,exlda,d_exlda_dn,p,d_p_dn,d_p_dgn
  real(kdp) :: tw, d_tw_dn, d_tw_dgn, t0, d_t0_dn
  real(kdp) :: b_den,b,d_b_dn,d_b_dgn,d_b_dt
  real(kdp) :: fx,d_fx_dp,d_fx_db

  kf = (3._kdp*pi**2*n)**(1._kdp/3._kdp)
  kf2 = kf*kf
  exlda = -3._kdp*kf*n/(4._kdp*pi)
  d_exlda_dn = -kf/pi

  p = (gn/(2._kdp*kf*n))**2
  d_p_dn = -8._kdp/3._kdp*p/n
  d_p_dgn = gn/(2._kdp*(kf*n)**2)

  tw = gn**2/(8._kdp*n)
  d_tw_dn = -tw/n
  d_tw_dgn = gn/(4._kdp*n)

  t0 = 0.3_kdp*kf2*n
  d_t0_dn = kf2/2._kdp

  b_den = t + t0
  b = min( 1._kdp, max(0._kdp,t - tw)/b_den )
  d_b_dn = -(d_tw_dn + d_t0_dn*b)/b_den
  d_b_dgn = -d_tw_dgn/b_den
  d_b_dt = (1._kdp - b)/b_den

  call SMGGA_Fx(p,b,fx,d_fx_dp,d_fx_db)

  ex = exlda*fx
  dexdn = d_exlda_dn*fx + exlda*(d_fx_dp*d_p_dn + d_fx_db*d_b_dn)
  dexdgn = exlda*(d_fx_dp*d_p_dgn + d_fx_db*d_b_dgn)
  dexdt = exlda*d_fx_db*d_b_dt


end subroutine SMGGA_X_UNP


subroutine SMGGA_Fx(p,b,fx,d_fx_dp,d_fx_db)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: p,b
  real(kdp), intent(out) :: fx,d_fx_dp,d_fx_db

  real(kdp) :: tbm1, pbas, d_pbas_dp, ief, d_ief_db, p14, pexp
  real(kdp) :: hx1, d_hx1_dp, gx, d_gx_dp, tmp, dtmp, fx_core

  integer :: i

  ! exchange beta = 0 parameters
  real(kdp), parameter :: a1x = 4.948698694710879_kdp, hx0 = 1.174_kdp

  ! exchange interpolation parameters
  real(kdp), parameter :: cax(6) = (/-0.0544982221979261_kdp, &
    &  0.2822239261540784_kdp, -11.403659801266858_kdp, -14.94642456380148_kdp,&
    &  8.365467294936067_kdp, 12.571509909118685_kdp /)

  ! exchange hx1 parameters
  real(kdp), parameter :: cbx(3) = (/ 0.11204244025200226_kdp, &
    & -0.1105621488363453_kdp, 0.012211985454419344_kdp /)

  tbm1 = 2._kdp*b - 1._kdp

  ief = 0._kdp
  d_ief_db = 0._kdp
  tmp = tbm1
  dtmp = 2._kdp
  do i = 1,6
    ief = ief + cax(i)*tmp
    d_ief_db = d_ief_db + i*cax(i)*dtmp
    tmp = tmp*tbm1
    dtmp = dtmp*tbm1
  end do

  pbas = tanh(p)
  d_pbas_dp = 1._kdp - pbas**2

  tmp = pbas
  dtmp = d_pbas_dp
  hx1 = 1._kdp
  d_hx1_dp = 0._kdp
  do i = 1,3
    hx1 = hx1 + cbx(i)*tmp
    d_hx1_dp = d_hx1_dp + i*cbx(i)*dtmp
    tmp = tmp*pbas
    dtmp = dtmp*pbas
  end do

  gx = 1._kdp
  d_gx_dp = 0._kdp
  if (p > 1.d-16) then
    p14 = p**(0.25_kdp)
    pexp = exp(-a1x/p14)
    gx = 1._kdp - pexp
    d_gx_dp = -(0.25_kdp)*a1x*pexp/(p14*p)
  end if

  fx_core = hx1 + ief*(hx0 - hx1)
  fx = fx_core*gx
  d_fx_dp = d_hx1_dp*(1._kdp - ief)*gx + fx_core*d_gx_dp
  d_fx_db = d_ief_db*(hx0 - hx1)

end subroutine SMGGA_Fx


subroutine SMGGA_C(n0,n1,gn,t,ec,decdn0,decdn1, decdgn, decdt)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: n0,n1,gn,t
  real(kdp), intent(out) :: ec,decdn0,decdn1,decdgn,decdt

  real(kdp), parameter :: pi = 3.141592653589793238_kdp

  ! correlation interpolation parameters
  real(kdp), parameter :: cac(4) = (/ -13.72428034860716_kdp, &
    &   -25.000805325013786_kdp, 12.371817656085426_kdp, 24.648342632492053_kdp /)

  real(kdp) :: n, zeta, d_zeta_dn0, d_zeta_dn1, rs, d_rs_dn, kf, kf2
  real(kdp) :: p, d_p_dn, d_p_dgn, tw, d_tw_dn, d_tw_dgn
  real(kdp) :: dsz,d_dsz_dz, t0_unp, t0, d_t0_dn, d_t0_dz
  real(kdp) :: b_den, b, d_b_dn, d_b_dgn, d_b_dz, d_b_dt

  real(kdp) :: tbm1, tmp, dtmp, ief, d_ief_db

  real(kdp) :: ec0, d_ec0_drs, d_ec0_dz, d_ec0_dp
  real(kdp) :: ec1, d_ec1_drs, d_ec1_dz, d_ec1_dp
  real(kdp) :: omief, epsc, d_epsc_drs, d_epsc_dz, d_epsc_dp, d_epsc_db
  real(kdp) :: d_ec_dn, spind

  integer :: i

  n = n0 + n1
  zeta = min(max((n0 - n1)/n,-0.99999999999990_kdp),0.99999999999990_kdp)
  d_zeta_dn0 = 2._kdp*n1/n**2
  d_zeta_dn1 = -2._kdp*n0/n**2

  rs = (3._kdp/(4._kdp*pi*n))**(1._kdp/3._kdp)
  d_rs_dn = -rs/(3._kdp*n)

  kf = (3._kdp*pi**2*n)**(1._kdp/3._kdp)
  kf2 = kf*kf

  p = (gn/(2._kdp*kf*n))**2
  d_p_dn = -8._kdp/3._kdp*p/n
  d_p_dgn = gn/(2._kdp*(kf*n)**2)

  tw = gn**2/(8._kdp*n)
  d_tw_dn = -tw/n
  d_tw_dgn = gn/(4._kdp*n)

  call spinf(zeta,5._kdp/3._kdp,dsz,d_dsz_dz)
  t0_unp = 0.3_kdp*kf2*n
  t0 = t0_unp*dsz
  d_t0_dn = 0.5_kdp*kf2*dsz
  d_t0_dz = t0_unp*d_dsz_dz

  b_den = t + t0
  b = min( 1._kdp, max(0._kdp,t - tw)/b_den )
  d_b_dn = -(d_tw_dn + d_t0_dn*b)/b_den
  d_b_dz = -d_t0_dz*b/b_den
  d_b_dgn = -d_tw_dgn/b_den
  d_b_dt = (1._kdp - b)/b_den

  tbm1 = 2._kdp*b - 1._kdp

  ief = 0._kdp
  d_ief_db = 0._kdp
  tmp = tbm1**3
  dtmp = 2._kdp*tbm1**2
  do i = 1,4
    ief = ief + cac(i)*tmp
    d_ief_db = d_ief_db + (2._kdp+i)*cac(i)*dtmp
    tmp = tmp*tbm1
    dtmp = dtmp*tbm1
  end do

  call SMGGA_CGGA0(rs,zeta,p,ec0,d_ec0_drs,d_ec0_dz,d_ec0_dp)
  call SMGGA_CGGA1(rs,zeta,p,ec1,d_ec1_drs,d_ec1_dz,d_ec1_dp)

  omief = 1._kdp - ief
  epsc = ec1*omief + ief*ec0

  d_epsc_drs = d_ec1_drs*omief + ief*d_ec0_drs
  d_epsc_dz = d_ec1_dz*omief + ief*d_ec0_dz
  d_epsc_dp = d_ec1_dp*omief + ief*d_ec0_dp
  d_epsc_db = d_ief_db*(ec0 - ec1)

  ec = n*epsc

  d_ec_dn = epsc + n*(d_epsc_drs*d_rs_dn + d_epsc_dp*d_p_dn &
  &       + d_epsc_db*d_b_dn)
  spind = n*(d_epsc_dz + d_epsc_db*d_b_dz)

  decdn0 = d_ec_dn + spind*d_zeta_dn0
  decdn1 = d_ec_dn + spind*d_zeta_dn1

  decdgn = n*(d_epsc_dp*d_p_dgn + d_epsc_db*d_b_dgn)

  decdt = n*d_epsc_db*d_b_dt

end subroutine SMGGA_C

subroutine SMGGA_CGGA0(rs,z,p,ec0,d_ec0_drs,d_ec0_dz,d_ec0_dp)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: rs,z,p
  real(kdp), intent(out) :: ec0,d_ec0_drs,d_ec0_dz,d_ec0_dp

  real(kdp), parameter :: pi = 3.141592653589793238_kdp

  ! correlation second-order gradient expansion parameters
  real(kdp), parameter :: beta_mb = 0.066725_kdp, beta_ca = 0.5_kdp
  real(kdp), parameter :: beta_cb = 1._kdp, beta_cc = 0.16667_kdp
  real(kdp), parameter :: beta_cd = 0.29633_kdp
  real(kdp), parameter :: beta_inf = beta_mb*beta_cb/beta_cd

  ! beta = 0 correlation LDA parameters
  real(kdp), parameter :: b1c = 0.028576442357136363_kdp
  real(kdp), parameter :: b2c = 0.10754697760569154_kdp
  real(kdp), parameter :: b3c = 0.12554138105363932_kdp

  ! beta = 0 correlation GGA parameters
  real(kdp), parameter :: chi_inf = (3._kdp*pi**2/16._kdp)**(2._kdp/3._kdp) &
    &   *beta_inf/(0.9_kdp - 3._kdp*(3._kdp/(16._kdp*pi))**(2._kdp/3._kdp))
  real(kdp), parameter :: hx0 = 1.174_kdp
  real(kdp), parameter :: b4c = hx0* &
    & (3._kdp/4._kdp*(3._kdp/(2._kdp*pi))**(2._kdp/3._kdp))*b3c/b1c

  real(kdp) :: rsh, ecl0, ecl0_den, d_ecl0_drs, dxz, d_dxz_dz
  real(kdp) :: gcz1, gcz2, gcz, d_gcz_dz
  real(kdp) :: w0exp, w0, d_w0_drs, ginf, d_ginf_dp
  real(kdp) :: h0core, h0, d_h0_drs, d_h0_dp

  rsh = rs**(0.5_kdp)
  ecl0_den = 1._kdp + b2c*rsh + b3c*rs
  ecl0 = -b1c/ecl0_den
  d_ecl0_drs = -ecl0*(0.5_kdp*b2c/rsh + b3c)/ecl0_den

  call spinf(z,4._kdp/3._kdp,dxz,d_dxz_dz)
  gcz1 = 1._kdp - b4c*(dxz - 1._kdp)
  gcz2 = 1._kdp - z**(12)
  gcz = gcz1*gcz2
  d_gcz_dz = -b4c*d_dxz_dz*gcz2 - 12._kdp*z**(11)*gcz1

  w0exp = exp(-ecl0/b1c)
  w0 = w0exp - 1._kdp
  d_w0_drs = -d_ecl0_drs*w0exp/b1c

  ginf = (1._kdp + 4._kdp*chi_inf*p)**(-0.25_kdp)
  d_ginf_dp = -chi_inf*ginf**5

  h0core = 1._kdp + w0*(1._kdp - ginf)
  h0 = b1c*log(h0core)

  d_h0_drs = b1c*d_w0_drs*(1._kdp - ginf)/h0core
  d_h0_dp = -b1c*w0*d_ginf_dp/h0core

  ec0 = (ecl0 + h0)*gcz
  d_ec0_drs = (d_ecl0_drs + d_h0_drs)*gcz
  d_ec0_dz = (ecl0 + h0)*d_gcz_dz
  d_ec0_dp = d_h0_dp*gcz

end subroutine SMGGA_CGGA0


subroutine SMGGA_CGGA1(rs,z,p,ec1,d_ec1_drs,d_ec1_dz,d_ec1_dp)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: rs,z,p
  real(kdp), intent(out) :: ec1,d_ec1_drs,d_ec1_dz,d_ec1_dp

  real(kdp), parameter :: pi = 3.141592653589793238_kdp
  real(kdp), parameter :: gamma = (1._kdp - log(2._kdp))/pi**2
  real(kdp), parameter :: cfact = (3._kdp*pi**2/16._kdp)**(2._kdp/3._kdp)
  ! correlation second-order gradient expansion parameters
  real(kdp), parameter :: beta_mb = 0.066725_kdp, beta_ca = 0.5_kdp
  real(kdp), parameter :: beta_cb = 1._kdp, beta_cc = 0.16667_kdp
  real(kdp), parameter :: beta_cd = 0.29633_kdp
  real(kdp), parameter :: beta_inf = beta_mb*beta_cb/beta_cd

  real(kdp) :: ecl1, d_ecl1_drs, d_ecl1_dz, phi,d_phi_dz, gp3, d_gp3_dz
  real(kdp) :: aa, d_aa_drs, d_aa_dz, t2, d_t2_drs, d_t2_dz, d_t2_dp
  real(kdp) :: w1exp, w1, d_w1_drs, d_w1_dz
  real(kdp) :: brs, d_brs_drs, brs_num, brs_den
  real(kdp) :: gt, dgt, d_gt_drs, d_gt_dz, d_gt_dp
  real(kdp) :: y, d_y_drs, d_y_dz, d_y_dp
  real(kdp) :: h1core, h1, d_h1_drs, d_h1_dz, d_h1_dp

  call spinf(z,2._kdp/3._kdp,phi,d_phi_dz)
  gp3 = gamma*phi**3
  d_gp3_dz = 3._kdp*gamma*phi**2*d_phi_dz

  call EPS_C_PW92(rs,z,ecl1,d_ecl1_drs, d_ecl1_dz)

  t2 = cfact*p/(phi**2*rs)
  d_t2_drs = -t2/rs
  d_t2_dz = -2._kdp*d_phi_dz*t2/phi
  d_t2_dp = cfact/(phi**2*rs)

  w1exp = exp(-ecl1/gp3)
  w1 = w1exp - 1._kdp
  d_w1_drs = -d_ecl1_drs*w1exp/gp3
  d_w1_dz = ( -d_ecl1_dz + 3._kdp*ecl1*d_phi_dz/phi )*w1exp/gp3

  brs_num = 1._kdp + beta_ca*rs*(beta_cb + beta_cc*rs)
  brs_den = 1._kdp + beta_ca*rs*(1._kdp + beta_cd*rs)
  brs = beta_mb*brs_num/brs_den
  d_brs_drs = beta_ca*( (beta_cb + 2._kdp*beta_cc*rs )*beta_mb &
 &          - (1._kdp + 2._kdp*beta_cd*rs)*brs )/brs_den

  aa = brs/(gamma*w1)
  d_aa_drs = d_brs_drs/(gamma*w1) - d_w1_drs*aa/w1
  d_aa_dz = - d_w1_dz*aa/w1

  y = aa*t2
  d_y_drs = d_aa_drs*t2 + aa*d_t2_drs
  d_y_dz = d_aa_dz*t2 + aa*d_t2_dz
  d_y_dp = aa*d_t2_dp

  gt = (1._kdp + 4._kdp*y)**(-0.25_kdp)
  dgt = -gt**5
  d_gt_drs = dgt*d_y_drs
  d_gt_dz = dgt*d_y_dz
  d_gt_dp = dgt*d_y_dp

  h1core = 1._kdp + w1*(1._kdp - gt)
  h1 = gp3*log(h1core)
  d_h1_drs = gp3*(d_w1_drs*(1._kdp - gt) - w1*d_gt_drs )/h1core
  d_h1_dz = gp3*(d_w1_dz*(1._kdp - gt) - w1*d_gt_dz )/h1core &
 &        + d_gp3_dz*log(h1core)
  d_h1_dp = -gp3*w1*d_gt_dp/h1core

  ec1 = ecl1 + h1
  d_ec1_drs = d_ecl1_drs + d_h1_drs
  d_ec1_dz = d_ecl1_dz + d_h1_dz
  d_ec1_dp = d_h1_dp

end subroutine SMGGA_CGGA1

subroutine EPS_C_PW92(rs,z,ec,d_ec_drs, d_ec_dz)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: rs,z
  real(kdp), intent(out) :: ec,d_ec_drs, d_ec_dz

  real(kdp), parameter :: fz_den = (2._kdp**(4._kdp/3._kdp) - 2._kdp)
  real(kdp), parameter :: fdd0 = 8._kdp/(9._kdp*fz_den)

  real(kdp) :: ec0, d_ec0_drs, ec1, d_ec1_drs, ac, d_ac_drs
  real(kdp) :: fz, d_fz_dz, z4, fzz4, dxz, d_dxz_dz

  call spinf(z, 4._kdp/3._kdp, dxz, d_dxz_dz)
  fz = 2._kdp*(dxz - 1._kdp)/fz_den
  d_fz_dz = 2._kdp*d_dxz_dz/fz_den

  z4 = z**4
  fzz4 = fz*z4

  call gc_PW92(rs, 0.031091_kdp, 0.21370_kdp, 7.5957_kdp, 3.5876_kdp, 1.6382_kdp, &
  &         0.49294_kdp,ec0,d_ec0_drs)
  call gc_PW92(rs, 0.015545_kdp, 0.20548_kdp, 14.1189_kdp, 6.1977_kdp, 3.3662_kdp, &
  &         0.62517_kdp,ec1,d_ec1_drs)
  call gc_PW92(rs, 0.016887_kdp, 0.11125_kdp, 10.357_kdp, 3.6231_kdp, 0.88026_kdp,&
  &         0.49671_kdp,ac,d_ac_drs)

  ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

  d_ec_drs = d_ec0_drs*(1._kdp - fzz4) + d_ec1_drs*fzz4 - d_ac_drs/fdd0*(fz - fzz4)
  d_ec_dz = -ac*d_fz_dz/fdd0 + (4._kdp*fz*z**3 + d_fz_dz*z4)*(ac/fdd0 + ec1 - ec0)

end subroutine EPS_C_PW92


subroutine gc_PW92(rs, A, alp, b1, b2, b3, b4, g, dg)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: rs, A, alp, b1, b2, b3, b4
  real(kdp), intent(out) :: g, dg

  real(kdp) :: rsh,q0,dq0,q1,dq1,q2,dq2

  rsh = rs**(0.5_kdp)

  q0 = -2._kdp*A*(1._kdp + alp*rs)
  dq0 = -2._kdp*A*alp

  q1 = 2._kdp*A*(b1*rsh + b2*rs + b3*rs*rsh + b4*rs*rs )
  dq1 = A*(b1/rsh + 2._kdp*b2 + 3._kdp*b3*rsh + 4._kdp*b4*rs )

  q2 = log(1._kdp + 1._kdp/q1)
  dq2 = -dq1/(q1**2 + q1)

  g = q0*q2
  dg = dq0*q2 + q0*dq2

end subroutine gc_PW92


subroutine spinf(z,n,f,df)

  implicit none
  integer, parameter :: kdp = selected_real_kind(15, 307)

  real(kdp), intent(in) :: z,n
  real(kdp), intent(out) :: f,df

  real(kdp) :: opz,omz

  opz = min(2._kdp,max(0._kdp,1._kdp+z))
  omz = min(2._kdp,max(0._kdp,1._kdp-z))
  f = (opz**n + omz**n)/2._kdp
  df = n*(opz**(n-1) - omz**(n-1))/2._kdp

end subroutine spinf
