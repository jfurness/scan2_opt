import numpy as np
import SCAN2
import matplotlib.pyplot as plt
from mayavi import mlab

def wrap_smoothbeta(p, q, alpha, eta, A, B, C, D):
    ap = (40.0/27.0 + eta)*p
    bq = 20.0/9.0*q
    x_s = alpha*(1.0 + ap/np.sqrt(1.0 + 4*ap**2))*(1.0 - bq/np.sqrt(1.0 + 4*bq**2))
    beta_s = x_s/(1.0 + x_s + eta*p)

    return (1 - 2*beta_s)*(1 + A*beta_s + B*beta_s**2 + C*beta_s**3 + D*beta_s**4)
    



alpha = 1.0

ETA = 0.07407407407407407
A = -0.5
B = -1
C = 13
D = -7

gp, gq = np.mgrid[0.0:9.0:0.01, -9.0:9.0:0.02]

f = np.array(wrap_smoothbeta(np.ravel(gp), np.ravel(gq), alpha, ETA, A, B, C, D))
gf = f.reshape(gp.shape)


s = mlab.surf(gp, gq, gf)

mlab.xlabel("p")
mlab.ylabel("q")
mlab.zlabel("f(beta_smooth)")

mlab.show()