import numpy as np
#from functools import partial
from scipy.optimize import least_squares, bisect, minimize_scalar, minimize
import h5py # for QUEST densities
import matplotlib.pyplot as plt
from os import system,path
from itertools import product

# for getting spherical Hartree-Fock densities
import Settings
from Densities import Atom, GridGenerator
import Benchmarks
from Jellium import Jelly
from int_ex import get_coeff_from_ex
from int_ec import get_coeff_from_ec

if not path.isdir('./series_figs/'):
    system('mkdir ./series_figs')

# Load defaults with r2 / SCAN values
# This is what the routine starts with, just so all variables are properly initialized
xpars_default = {'A1': 4.9479, 'K1': 0.065, 'C1X': 0.667, 'C2X': 0.8, 'DX': 1.24,
    'DP': 0.361, 'CP': 0.243, 'ETA': 0.01, 'B3X': 0.5, 'f1x': -0.9353000875519996}

cpars_default = {'B1C': 0.0285764, 'B2C': 0.0889, 'B3C': 0.125541, 'C1C': 0.64,
    'C2C': 1.5, 'DC': 0.7, 'ETA': 0.01, 'DP': 0.361, 'f1c': -0.7114023342889984}

# used to say if a calculation is unrestricted (None), restricted ('UNP'), or one-electron ('OES')
atom_rest = {}
for at in Benchmarks.BM_LIST+Benchmarks.NON_SPHERICAL_LIST:
    atom_rest[at] = None
for at in ['Ne','Ar','Kr','Xe']:
    atom_rest[at] = 'UNP'

imin_x = 1
imin_c = 1

"""
    Constants
"""
al0 = np.linspace(0.0,20.0,1000)
ptmp0 = al0
al_ref, ptmp = np.meshgrid(al0,ptmp0)

pi = np.pi
GAMMA = (1.0 - np.log(2.0))/pi**2

MIN_DEN = 1e-10
K0 = 0.174
MU_GE2 = 10/81

up = 10/81
upp = 0.0
uqq = 146/2025
uqqh = uqq**(0.5)
upq = -5*uqq/2

BETA_MB = 0.066725
if Settings.BETARS == 'revTPSS':
    AFACTOR = 0.1778
    BFACTOR = 0.1/AFACTOR
    CFACTOR = 0.0
    DFACTOR = 0.0
    BETA_INF = BETA_MB*BFACTOR
elif Settings.BETARS == 'ACGGA':
    AFACTOR = 0.5
    BFACTOR = 1.0
    CFACTOR = 0.16667
    DFACTOR = 0.29633
    BETA_INF = BETA_MB*CFACTOR/DFACTOR

def beta_gec(rs):
    beta_num = 1 + AFACTOR*rs*(BFACTOR + CFACTOR*rs)
    beta_den = 1 + AFACTOR*rs*(1 + DFACTOR*rs)
    return BETA_MB*beta_num/beta_den

cpars_default['CHI_inf'] = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))

"""
    Series interpolation function, can be used for X or C.
    Expects to get a limit for f(beta=1) = ief_inf
"""

def dief(b,pars,imin=1,ind='BA'):

    if ind == 'B':
        tbm1 = 2*b - 1
        dt = 2
    elif ind == 'BA':
        tbm1 = (b - 1)/(b + 1)
        dt = 2/(b + 1)**2

    tmp = tbm1**(imin-1)
    dief = np.zeros_like(b)
    for i in range(len(pars)):
        dief += (i+imin)*pars[i]*tmp
        tmp *= tbm1
    return dief*dt

def get_ief_pars(b_pars,ief_inf,f1,imin=1):

    Mb = len(b_pars)+3
    a = np.zeros(Mb)

    a[0] = 2*f1
    a[1:-2] = b_pars
    s1 = 0.0
    for i in range(Mb-2):
        s1 += (-1)**(i+imin)*a[i]

    s1 = (-1)**(Mb+imin-1)*(1 - s1)
    s2 = -(ief_inf + np.sum(a[:-2]))

    a[-1] = 0.5*(s2 + s1)
    a[-2] = 0.5*(s2 - s1)

    return a

"""
    Exchange subroutines
"""

def series_SCAN_Fx(p,ba,ax,xpars,ind='BA',whx1='r2SCAN'):

    f1 = xpars['f1x']#2*ax[0]
    if ind == 'BA':
        ya = (ba - 1)/(ba + 1)
        cba = 9/20*uqqh
        cp = (9*xpars['ETA'] - 7)*uqqh/12
        amo = ba - 1

        dh1 = 5*(4 + 9*xpars['ETA'])*K0*f1/27

    elif ind == 'B':
        ya = 2*ba - 1
        cba = 9/10*uqqh
        cp = uqqh/6
        amo = 2*ba - 1

        dh1 = 65*K0*f1/108

    tmp = np.zeros_like(ya)
    tmp[:] = ya
    ief_x = np.zeros_like(ba)

    for i in range(len(ax)):
        ief_x += ax[i]*tmp
        tmp *= ya

    k1 = xpars['K1']
    if whx1 == 'SCAN':
        # 16*up**2/(25*uqq) #
        b4 = up**2/k1 - 25*uqq/16
        #x = up*p + (cp*p + cba*amo)**2
        #x = up*p*(1 + b4/(2*up)*p)**2 + (cp*p + cba*(ba - 1))**2
        ba_damp = np.exp(-xpars['B3X']*amo**2)
        x = up*p + b4*p**2*np.exp(-abs(b4)*p/up) + (cp*p + cba*amo*ba_damp)**2
        #x = (up*p + (cp*p + cba*(ba - 1))**2 + up**2*p**2/k1 )/(1 + 25*uqq/(16*up)*p)
        hx1 = 1 + k1 - k1/(1 + x/k1)

    elif whx1 == 'r2SCAN':

        x = (up + dh1*np.exp(-p**2/xpars['DP']**4))*p
        hx1 = 1 + k1 - k1/(1 + x/k1)

    gx = np.ones_like(p)
    pmsk = p > 1.e-16
    gx[pmsk] = -np.expm1(-xpars['A1']/p[pmsk]**(0.25))

    hx0 = 1 + K0

    return (hx1 + ief_x*(hx0 - hx1))*gx

def e_x_series_SCAN_unp(n,gn,tau,ax,xpars,ind='BA'):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3*kf*n/(4*pi)
    p = (gn/(2*kf*n))**2

    tau_W = gn**2/(8*n)
    tau_TF = 0.3*kf**2*n
    if ind == 'B':
        ba = np.minimum(1.0,np.maximum(0.0,(tau - tau_W)/(tau + tau_TF)))

    elif ind == 'BA':
        ba = np.maximum(0.0, (tau - tau_W)/(tau_TF + xpars['ETA']*tau_W))

    Fx = series_SCAN_Fx(p,ba,ax,xpars,ind=ind)

    return exlda*Fx

def e_x_series_SCAN(n0,n1,g0,g1,t0,t1,lpars,xpars,typ=None,ind='BA'):

    ax = get_ief_pars(lpars,xpars['DX'],xpars['f1x'],imin=imin_x)

    ex0 = e_x_series_SCAN_unp(2*n0,2*g0,2*t0,ax,xpars,ind=ind)
    if typ is None:
        ex1 = e_x_series_SCAN_unp(2*n1,2*g1,2*t1,ax,xpars,ind=ind)
        ex = (ex0 + ex1)/2
    elif typ == 'UNP':
        # spin-unpolarized system
        ex = ex0
    elif typ == 'OES':
        # fully spin-polarized, one-electron system with zero spin-down density
        ex = ex0/2
    return ex

def get_Ex_atoms(dens,lpars,xpars,typ=None,ind='BA'):
    ex_dens = e_x_series_SCAN(dens[:,0],dens[:,1],dens[:,2],dens[:,3],dens[:,4],\
        dens[:,5], lpars,xpars,typ=typ,ind=ind)
    ex_dens[dens[:, 0] + dens[:, 1] < MIN_DEN] = 0.0

    return np.dot(dens[:,6],ex_dens)

"""
    Ix(ij) matrix elements of Ex
"""

def get_Ix_unp(n,gn,tau,wg,xpars,rank,mask=None,ind='BA'):

    Ix = np.zeros(rank)
    if mask is None:
        mask = np.ones(p.shape,dtype=bool)

    def fint(pars):
        wexden = wg*e_x_series_SCAN_unp(n,gn,tau,pars,xpars,ind=ind)
        return np.sum(wexden[mask])

    for i in range(rank):
        tarr = np.zeros(rank-1)
        if i > 0:
            tarr[i-1] = 1.0
        Ix[i] = fint(tarr) - Ix[0]

    return Ix

def get_Ix(n0,n1,g0,g1,t0,t1,wg,xpars,rank,typ=None,ind='BA'):

    msk = n0 + n1 >= MIN_DEN
    Ix0 = get_Ix_unp(2*n0,2*g0,2*t0,wg,xpars,rank,mask=msk,ind=ind)
    if typ is None:
        Ix1 = get_Ix_unp(2*n1,2*g1,2*t1,wg,xpars,rank,mask=msk,ind=ind)
        Ix = (Ix0 + Ix1)/2
    elif typ == 'UNP':
        # spin-unpolarized system
        Ix = Ix0
    elif typ == 'OES':
        # fully spin-polarized, one-electron system with zero spin-down density
        Ix = Ix0/2

    return Ix

def get_Ex_from_Ix(Ix,lpars,xpars):

    ax = get_ief_pars(lpars,xpars['DX'],xpars['f1x'],imin=imin_x)
    Ex = Ix[0] + np.dot(ax,Ix[1:])

    return Ex

"""
    Correlation subroutines
"""

def spinf(z,n):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    return (opz**n + omz**n)/2.0

def eps_c_pw92(rs,z,unp=False):

    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """
    rsh = rs**(0.5)

    def g(v):
        q0 = -2*v[0]*(1 + v[1]*rs)
        q1 = 2*v[0]*(v[2]*rsh + v[3]*rs + v[4]*rs*rsh + v[5]*rs*rs)
        logt = np.log(1 + 1/q1)
        return q0*logt

    unp_pars = [0.031091,0.21370,7.5957,3.5876,1.6382,0.49294]
    ec0 = g(unp_pars)
    if unp:
        return ec0

    pol_pars = [0.015545,0.20548,14.1189,6.1977,3.3662,0.62517]
    alp_pars = [0.016887,0.11125,10.357,3.6231,0.88026,0.49671]

    fz_den = 2**(4/3)-2
    fdd0 = 8/9/fz_den
    dxz = spinf(z,4/3)
    fz = 2*(dxz - 1)/fz_den

    ec1 = g(pol_pars)
    ac = g(alp_pars)
    z4 = z**4
    fzz4 = fz*z4
    ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

    return ec

def ec_pw92_for_r2c(rs,z):

    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """

    rsh = rs**(0.5)
    def g(v):

        q0 = -2*v[0]*(1 + v[1]*rs)
        dq0 = -2*v[0]*v[1]

        q1 = 2*v[0]*(v[2]*rsh + v[3]*rs + v[4]*rs*rsh + v[5]*rs*rs)
        dq1 = v[0]*(v[2]/rsh + 2*v[3] + 3*v[4]*rsh + 4*v[5]*rs)

        q2 = np.log(1 + 1/q1)
        dq2 = -dq1/(q1**2 + q1)

        g = q0*q2
        dg = dq0*q2 + q0*dq2

        return g,dg

    unp_pars = [0.031091,0.21370,7.5957,3.5876,1.6382,0.49294]
    pol_pars = [0.015545,0.20548,14.1189,6.1977,3.3662,0.62517]
    alp_pars = [0.016887,0.11125,10.357,3.6231,0.88026,0.49671]

    fz_den = 0.5198420997897464#(2**(4/3)-2)
    fdd0 = 1.7099209341613653#8/9/fz_den
    dxz = spinf(z,4/3)
    fz = 2*(dxz - 1)/fz_den

    ec0, d_ec0_drs = g(unp_pars)
    ec1, d_ec1_drs = g(pol_pars)
    ac, d_ac_drs = g(alp_pars)
    z4 = z**4
    fzz4 = fz*z4
    ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

    d_ec_drs = d_ec0_drs*(1 - fzz4) + d_ec1_drs*fzz4 - d_ac_drs/fdd0*(fz - fzz4)

    return ec, d_ec_drs

def epsc01_r2scan(rs,p,z,df1,cps,ind='BA'):

    sqrt_rs = np.sqrt(rs)
    f1 = 1.0 + cps['B2C']*sqrt_rs + cps['B3C']*rs
    ec0_lda = -cps['B1C']/f1

    dx_zeta = spinf(z,4/3)
    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*cps['B3C']/cps['B1C']
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - pow(z, 12))

    d_ec0_drs = gc_zeta*cps['B1C']/f1**2 * (0.5*cps['B2C']/rs**(0.5) + cps['B3C'])

    w0 = np.expm1(-ec0_lda/cps['B1C'])

    chi_inf = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))
    gf_inf = 1.0/(1.0 + 4.0*chi_inf*p)**(1.0/4.0)

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = cps['B1C']*np.log(hcore0)

    eps0 = (ec0_lda + h0)*gc_zeta

    brs = beta_gec(rs)

    phi = spinf(z,2/3)
    gp3 = GAMMA*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)

    ecl,d_ecl_drs = ec_pw92_for_r2c(rs,z)
    w1 = np.expm1(-ecl/gp3)
    aa = brs/GAMMA/w1
    y = aa*t2

    ds_zeta = spinf(z,5.0/3.0)

    if ind == 'BA':
        fac1 = 20/27
        fac2 = -45/27*cps['ETA']
    elif ind == 'B':
        fac1 = 5/27
        fac2 = -5/12

    dy_damp = p*np.exp(-(p/cps['DP']**2)**2)
    dy_fac = df1/(ds_zeta*gp3*w1)*(fac1*rs*(d_ec0_drs - d_ecl_drs) \
        +fac2*(ec0_lda*gc_zeta - ecl) )
    delta_y = dy_fac*dy_damp

    if np.any(y - delta_y < -0.01):
        tmpr = 1e20*np.ones_like(p)
        return tmpr, tmpr
    gt = 1.0/(1.0 + 4*(y - delta_y))**(0.25)


    h = gp3*np.log(1 + w1*(1 - gt))

    return eps0, ecl + h

def cgga0(rs,z,p,cps):

    f1 = 1.0 + cps['B2C']*rs**(0.5) + cps['B3C']*rs
    ec0_lda = -cps['B1C']/f1

    dx_zeta = spinf(z,4/3)
    hx0 = 1.174
    b4c = hx0*(3/4*(3/(2*pi))**(2/3))*cps['B3C']/cps['B1C']
    gc_zeta = (1.0 - b4c*(dx_zeta - 1.0)) * (1.0 - z**(12))

    w0 = np.expm1(-ec0_lda/cps['B1C'])

    gf_inf = 1.0/(1.0 + 4.0*cps['CHI_inf']*p)**(1.0/4.0)

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = cps['B1C']*np.log(hcore0)

    eps0 = (ec0_lda + h0)*gc_zeta

    return eps0


def cgga1(rs,z,p,cps,unp=False):

    phi = spinf(z,2/3)
    gp3 = GAMMA*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)

    ecl = eps_c_pw92(rs,z,unp=unp)
    w1 = np.expm1(-ecl/gp3)
    aa = beta_gec(rs)/GAMMA/w1
    y = aa*t2

    gt = 1.0/(1.0 + 4*y)**(0.25)

    h = gp3*np.log(1 + w1*(1 - gt))

    eps1 = ecl + h

    return eps1

def epsc(n0,n1,gn,tau,ac,cpars,unp=False,ind='BA'):

    n = n0 + n1
    zeta = np.minimum(1.0,np.maximum(-1.0,(n0 - n1)/n))

    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2
    p[gn**2 <= 1e-16] = 0.0

    tau_W = gn**2/(8*n)
    tau_TF = 0.3*kf**2*n
    if ind == 'B':
        beta = np.minimum(1.0,np.maximum(0.0,(tau - tau_W)/(tau + tau_TF)))
        ya = 2*beta - 1
    elif ind == 'BA':
        ba = np.maximum(0.0,(tau - tau_W)/(tau + cpars['ETA']*tau_W))
        ya = (ba - 1)/(ba + 1)

    tmp = np.zeros_like(ya)
    tmp[:] = ya
    ief_c = np.zeros_like(ya)

    for i in range(len(ac)):
        ief_c += ac[i]*tmp
        tmp *= ya

    #eps_c0 = cgga0(rs,zeta,p,cpars)
    #eps_c1 = cgga1(rs,zeta,p,cpars,unp=unp)
    eps_c0, eps_c1 = epsc01_r2scan(rs,p,zeta,cpars['f1c'],cpars,ind=ind)
    eps_c = eps_c1*(1 - ief_c) + eps_c0*ief_c

    return n*eps_c

def get_Ec_atoms(dens,lpars,cpars,typ=None,ind='BA'):
    ac = get_ief_pars(lpars,cpars['DC'],cpars['f1c'],imin=imin_c)
    ec_dens = epsc(dens[:,0],dens[:,1],dens[:,7],dens[:,4]+dens[:,5],ac,cpars,\
        unp=(typ=='UNP'),ind=ind)
    ec_dens[dens[:, 0] + dens[:, 1] < MIN_DEN] = 0.0
    return np.dot(dens[:,6],ec_dens)

def get_Ic(n0,n1,gn,tau,wg,cpars,rank,typ=None,mask=None,ind='BA'):

    n = n0 + n1
    if mask is None:
        msk = n > MIN_DEN
    else:
        msk = mask

    Ic = np.zeros(rank)

    def fint(pars):
        wecden = wg*epsc(n0,n1,gn,tau,pars,cpars,unp=(typ=='UNP'),ind=ind)
        return np.sum(wecden[msk])

    for i in range(rank):
        tarr = np.zeros(rank-1)
        if i > 0:
            tarr[i-1] = 1.0
        Ic[i] = fint(tarr) - Ic[0]

    return Ic


def get_Ec_from_Ic(Ic,lpars,cpars):
    ac = get_ief_pars(lpars,cpars['DC'],cpars['f1c'],imin=imin_c)
    Ec = Ic[0] + np.dot(ac,Ic[1:])
    return Ec

"""
    Auxiliary fitting routines
"""

def bracket(func,x0,dx,nstep=1000):
    osgn = np.sign(func(x0))
    a = x0
    success = False
    for ix in range(nstep):
        b = a + dx
        csgn = np.sign(func(b))
        if osgn*csgn < 0.0:
            success = True
            break
        a = b

    if not success:
        print('WARNING: could not bracket root')

    return a,b,success

def fit_h_atom():

    rmin = 1.e-6
    rmax = 1.e2
    Nr = 5000
    rho,drho = np.linspace(np.log(rmin),np.log(rmax),Nr,retstep=True)
    r = np.exp(rho)
    wg = 4*pi*r**3*drho

    n = np.exp(-2*r)/pi
    agn = 2*n
    tau = agn**2/(8*n)

    h_ex_exact = -0.3125

    exlda_sr = -2**(1/3)*3*(3*pi**2*n)**(1/3)*n/(4*pi)
    hx0 = 1 + K0
    ex0 = hx0*exlda_sr
    p_sr = 2**(-2/3)*(agn/(2*(3*pi**2*n)**(1/3)*n))**2
    pmsk = p_sr > 1.e-16
    pm_sr = p_sr[pmsk]

    def hobj(cc):
        gx = np.ones_like(p_sr)
        gx[pmsk] = -np.expm1(-cc/pm_sr**(0.25))
        exden = ex0*gx
        return h_ex_exact - np.sum(wg*exden)

    ca,cb,csuc = bracket(hobj,4.8,5.1,nstep=1000)
    if not csuc:
        print('Could not bracket root for A1 (hydrogen atom) param')
        return 1e20
    a1 = bisect(hobj,ca,cb)

    return a1

def set_ec0_lda_pars_scan2(cps,xps,He_dens,ind='BA'):

    # Grid
    h = 0.001
    r = np.arange(0.0, 35.0, h)

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    d = 2/pi*np.exp(-2*r)
    s = np.exp(2*r/3)/(6*pi)**(1/3)

    td = {akey: cps[akey] for akey in cps}

    def tmpf(b1c):
        td['B1C'] = b1c
        return cgga0(0.0,0.0,s**2,td)

    c_den = tmpf(1.0)
    Ec = 4*pi*h*np.sum(r**2*d*c_den)
    cps['B1C'] = -0.0467/Ec
    print("Optimal B1C:", cps['B1C'])
    c_den_opt = tmpf(cps['B1C'])
    Ec_opt = np.sum(4*pi*r**2*h*d*c_den_opt)
    print("Is consistent? ", abs(Ec_opt + 0.0467) < 1e-16)

    cps['B3C'] = cps['B1C']*4*pi/3*(4/(9*pi))**(1/3)/(0.67082 - 0.174)
    He_ex = get_Ex_atoms(He_dens, [], xps,ind=ind)

    td = {akey: cps[akey] for akey in cps}

    def tobj(b2c):
        td['B2C'] = b2c
        He_ec = get_Ec_atoms(He_dens,[],td,typ='UNP',ind=ind)
        err = abs(He_ex + He_ec + 1.068)
        return err

    b2c_res = minimize_scalar(tobj, bracket=(0.07,0.09))

    if b2c_res.fun > 1e-9:
        print("Failed on b2c")
        return None
    cps['B2C'] = abs(b2c_res.x)

    return cps['B1C'],cps['B2C'], cps['B3C']

"""
    For generating initial guesses of params
"""

def get_rpoly_coeff(c1,c2,d,x,osmth=3):

    b0 = 1
    b1 = 1-c1
    b2 = c1*(c1 - 2)/2 + b1

    """
        rank x rank coefficient matrix to enforce
            d^j f / d x^j = d^j f_SCAN / d x^j,    j = 0,1,2,...,(rank-1)
        at matching point x

        rank = osmth+1
    """
    rank = osmth+1

    bmat = np.zeros((rank,rank))
    for i in range(rank):
        j = 3 + i
        bmat[0,i] = x**j - x**(j+1)
        bmat[1,i] = j*x**(j-1) - (j+1)*x**j
        bmat[2,i] = j*(j-1)*x**(j-2) - (j+1)*j*x**(j-1)
        if i == 0:
            bmat[3,i] = 6 - 24*x
            if osmth == 4:
                bmat[4,i] = -24
        else:
            bmat[3,i] = j*(j-1)*(j-2)*x**(j-3) - (j+1)*j*(j-1)*x**(j-2)
            if osmth == 4:
                bmat[4,i] = j*(j-1)*(j-2)*(j-3)*x**(j-4) - (j+1)*j*(j-1)*(j-2)*x**(j-3)

    """
        zeroth to fourth derivatives of SCAN x > 1 interpolation function
    """
    res = np.zeros(rank)
    omx = 1-x
    ief = -d*np.exp(c2/omx)
    d1 = ief*c2/omx**2
    d2 = (d1 + 2*ief/omx )*c2/omx**2
    d3 = (d2 + 2*d1/omx + 2*ief/omx**2)*c2/omx**2 + 2*d2/omx
    if osmth == 4:
        res[4] = (d3 + 2*d2/omx + 4*d1/omx**2 + 4*ief/omx**3)*c2/omx**2 \
            + 2*(d2 + 2*d1/omx + 2*ief/omx**2)*c2/omx**3 \
            + 2*d3/omx + 2*d2/omx**2

    """
        RHS of matrix equation
    """
    x2 = x*x
    res[0] = ief - (b0*(1-x) + b1*(x - x2) + b2*(x2 - x2*x))
    res[1] = d1 - (-b0 + b1*(1-2*x) + b2*(2*x - 3*x2))
    res[2] = d2 - (-2*b1 + 2*b2*(1 - 3*x))
    res[3] = d3 + 6*b2

    bl = np.zeros(3+rank)
    bl[:3] = [b0,b1,b2]
    bl[3:] = np.linalg.solve(bmat,res)

    return bl

def rief(x,bv,c2,d,rderiv=False,x0=2.5):
    nlp = len(bv)
    f = np.zeros_like(x)
    xmsk = x <= x0
    xm = x[xmsk]
    for i in range(nlp):
        f[xmsk] += bv[i]*xm**i
    f[xmsk] *= (1-xm)

    if rderiv:
        df = -bv[0]*np.ones_like(x)
        for i in range(1,nlp):
            df += bv[i]*(i - (i+1)*xm)*xm**(i-1)

    xmsk = x>x0
    xm = x[xmsk]
    f[xmsk] = -d*np.exp(c2/(1-xm))
    if rderiv:
        df[xmsk] = c2/(1-xm)**2*f[xmsk]
        return f,df

    return f

def scan_ief(x,c1,c2,d,rderiv=False):
    f = np.zeros_like(x)

    xmsk = x < 1
    xm = x[xmsk]
    f[xmsk] = np.exp(-c1*xm/(1-xm))
    if rderiv:
        df = np.zeros_like(x)
        df[xmsk] = -c1/(1-xm)**2*f[xmsk]

    xmsk = x > 1
    xm = x[xmsk]
    f[xmsk] = -d*np.exp(c2/(1-xm))
    if rderiv:
        df[xmsk] = c2/(1-xm)**2*f[xmsk]
        return f,df

    return f

def poly_ief(y,cb,ief_inf,f1,imin=1):

    nps = len(cb)+3
    ca = np.zeros(nps)

    ca[0] = 2*f1
    ca[1:-2] = cb
    s1 = 0.0
    for i in range(nps-2):
        s1 += (-1)**(i+imin)*ca[i]

    s1 = (-1)**(nps+imin-1)*(1 - s1)
    s2 = -(ief_inf + np.sum(ca[:-2]))

    ca[-1] = 0.5*(s2 + s1)
    ca[-2] = 0.5*(s2 - s1)

    tmp = np.zeros_like(y)
    tmp[:] = y**imin

    ief = np.zeros_like(y)

    for i in range(len(ca)):
        ief += ca[i]*tmp
        tmp *= y

    return ca,ief

def get_starting_pars(Nbx,Nbc,ind='BA',eta=None):

    xrpc = get_rpoly_coeff(xpars_default['C1X'],xpars_default['C2X'],\
        xpars_default['DX'],2.5,osmth=3)

    crpc = get_rpoly_coeff(cpars_default['C1C'],cpars_default['C2C'],\
        cpars_default['DC'],2.5,osmth=3)

    if ind == 'BA':
        alpl = np.linspace(0.0,5.0,5000)
        y = (alpl - 1)/(alpl + 1)
    elif ind == 'B':
        betl = np.linspace(0.0,1.0,5000)
        y = 2*betl - 1
        alpl = betl/(1 - betl)

    ief_x = rief(alpl,xrpc,xpars_default['C2X'],xpars_default['DX'],x0=2.5)

    def tobj(cb):
        _, tiefx = poly_ief(y,cb,xpars_default['DX'],xpars_default['f1x'],imin=imin_x)
        return tiefx - ief_x
    if Nbx == 0:
        lps = []
    else:
        fps = least_squares(tobj,np.zeros(Nbx))
        lps = fps.x
    cax, pief_x = poly_ief(y,lps,xpars_default['DX'],xpars_default['f1x'],imin=imin_x)

    if imin_c == 3:
        ief_c = scan_ief(alpl,cpars_default['C1C'],cpars_default['C2C'],\
            cpars_default['DC'],rderiv=False)
    elif imin_c == 1:
        ief_c = rief(alpl,crpc,cpars_default['C2C'],cpars_default['DC'],x0=2.5)

    def tobj(cb):
        _, tiefc = poly_ief(y,cb,cpars_default['DC'],cpars_default['f1c'],imin=imin_c)
        return tiefc - ief_c

    if Nbc == 0:
        lps = []
    else:
        fps = least_squares(tobj,np.zeros(Nbc))
        lps = fps.x
    cac, pief_c = poly_ief(y,lps,cpars_default['DC'],cpars_default['f1c'],imin=imin_c)

    """
    diefx = -np.sum(xrpc)
    diefc = -np.sum(crpc)
    print(diefx,xpars_default['f1x'],dief(np.ones(1),cax,imin=1,ind='BA')[0])
    print(diefc,cpars_default['f1c'],dief(np.ones(1),cac,imin=1,ind='BA')[0])

    plt.plot(alpl,ief_x)
    plt.plot(alpl,pief_x)
    plt.show()
    #exit()

    plt.plot(alpl,ief_c)
    plt.plot(alpl,pief_c)

    plt.show()
    exit()
    #"""
    return cax,cac

"""
    Density related stuff ripped from Optimiser.py
    Thanks James!
"""

def generate_density(alist):
    """
    Utility function to generate and arrange the orbitals for
    the atoms in alist (strings of atomic symbols).
    """
    out = []
    n, r, wt = GridGenerator.make_grid(200)
    for a in map(Atom, alist):
        d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
        idxs = d0 + d1 > MIN_DEN  # Filter all those with small densities
        # Later functions expect density, |grad|, tau, spherical integration weight.
        # spin 0 then 1 for each quantity
        grid = 4*pi*wt[idxs]*r[idxs]**2

        assert max( np.dot(l0[idxs], grid), np.dot(l1[idxs], grid)) < 1e-3, \
            "Laplacian integral failed for {:}".format(a)

        out.append(np.stack([d0[idxs], d1[idxs], g0[idxs], g1[idxs], t0[idxs], \
            t1[idxs], grid, g0[idxs]+g1[idxs], l0[idxs], l1[idxs]], axis=1))
    return out

def read_QUEST_densities(fnames):
    """
    Reads density from QUEST output format.
    Used to read full DFT grid for non-spherical atoms.

    Note! Extra total gradient as non-colinear density gradients
    are aparent!
    """
    olist = []
    for f in fnames:
        print("   - Reading ",f)
        with h5py.File(f, 'r') as inp:
            rho = np.array(inp['rho'])
            idxs = rho[:,0]+rho[:,1] > 1e-12  # Filter out really small densities
            grd = np.array(inp['grd'])
            vabs = lambda a,b: np.sum(np.multiply(a,b),axis=0)
            ga = grd[:,0:3]
            gb = grd[:,4:7]
            gaa = np.array(list(map(vabs, ga, ga))) # |grad|^2 spin 0
            gbb = np.array(list(map(vabs, gb, gb))) # |grad|^2 spin 1
            gab = np.array(list(map(vabs, ga, gb)))
            gtt = gaa + gbb + 2*gab
            tau = np.array(inp['tau'])
            xyz = np.array(inp['xyz'])

            out = np.zeros((rho[idxs].shape[0], 10))
            out[:,0] = rho[idxs,0]
            out[:,1] = rho[idxs,1]
            out[:,2] = np.sqrt(gaa[idxs])
            out[:,3] = np.sqrt(gbb[idxs])
            out[:,4] = tau[idxs,0]
            out[:,5] = tau[idxs,1]
            try:
                out[:,6] = xyz[idxs,3]  # Grid weights
            except IndexError:
                # Weights are missing so assume this is a spherical integal
                print("No integration weights found in ", f, " assuming spherical")
                out[:,6] = np.ones_like(rho[idxs,0])
            out[:,7] = np.sqrt(gtt[idxs])

            if Settings.LOAD_LAP:
                lap = np.array(inp['lap'])
                out[:,8] = lap[idxs,0]
                out[:,9] = lap[idxs,1]

                # Test laplacian correctness
                lapint = max(np.dot(out[:,8], out[:,6]), np.dot(out[:,9], out[:,6]))
                assert lapint < 1e-3, \
                    "Laplacian integral failed for {:} at {:}".format(f, lapint)

            olist.append(out)

    return olist

def read_QUEST_restricted_densities(fnames):
    """
    Reads density from QUEST output format for spin restricted format.

    Note! Extra total gradient as non-colinear density gradients
    are aparent!
    """
    olist = []
    for f in fnames:
        print("   - Reading ",f)
        with h5py.File(f, 'r') as inp:
            rho = np.array(inp['rho'])
            idxs = rho[:,0] > 1e-12  # Filter out really small densities
            grd = np.array(inp['grd'])
            vabs = lambda a,b: np.sum(np.multiply(a,b),axis=0)
            ga = grd[:,0:3]
            gaa = np.array(list(map(vabs, ga, ga))) # |grad|^2 spin 0
            tau = np.array(inp['tau'])
            xyz = np.array(inp['xyz'])

            out = np.zeros((rho[idxs].shape[0], 10))
            out[:,0] = rho[idxs,0]/2.0
            out[:,1] = rho[idxs,0]/2.0

            out[:,2] = np.sqrt(gaa[idxs])/2.0
            out[:,3] = np.sqrt(gaa[idxs])/2.0
            out[:,4] = tau[idxs,0]/2.0
            out[:,5] = tau[idxs,0]/2.0
            try:
                out[:,6] = xyz[idxs,3]
            except IndexError:
                out[:,6] = 4*pi*xyz[idxs,2]**2*(xyz[1,2]-xyz[0,2])
            out[:,7] = np.sqrt(gaa[idxs])

            if Settings.LOAD_LAP:
                lap = np.array(inp['lap'])
                out[:,8] = lap[idxs,0]/2.0
                out[:,9] = lap[idxs,0]/2.0

                # Test laplacian correctness
                assert max( np.dot(out[:,8], out[:,6]), np.dot(out[:,9], out[:,6])) < 1e-3, \
                    "Laplacian integral failed for {:}".format(f)
            else:
                try:
                    lap = np.array(inp['lap'])
                    print("WARNING: Laplacian data is present but we are NOT keeping it!")
                except KeyError:
                    continue

            olist.append(out)

    return olist

"""
    Residuum errors
"""

def jsextr(zs, ens):
    # used to extrapolate to CBS from finite zeta basis sets
    x = np.power(zs,-3.0)
    poly = np.polyfit(x, ens, 1)
    return poly[1]  # Return only intercept

def get_errors(Ix_d,Ic_d,xpars,cpars,lxpars,lcpars,only_obj=False,\
    penalize_deriv=False,ind='BA'):

    resd = {
        'OBJ': 0.0, 'SA XC MAPE': 0.0, 'SA X MAPE': 0.0, 'SA C MAPE': 0.0,
        'NSA XC MAPE': 0.0, 'NSA X MAPE': 0.0, 'NSA C MAPE': 0.0,
        'LZ Bx': 0.0, 'LZ Bx PE': 0.0, 'LZ Bc': 0.0, 'LZ Bc PE': 0.0,
        'JS XC MAPE': 0.0, 'JS X MAPE': 0.0, 'JS C MAPE': 0.0,
        'AR2 XC MAPE': 0.0
        }
    wgts = {'SA': 1/0.15, 'NSA': 1/0.3, 'LZ': 1.0/0.1, 'JS': 1/3.0, 'AR2': 1/1.5}
    norm_pe = []
    #for sep in Benchmarks.AR2_SEPS:
    #    resd['AR2 '+sep] = 0.0

    n_sa = len(Benchmarks.BM_LIST)
    sa_x = np.zeros(n_sa)
    sa_c = np.zeros(n_sa)
    lz_ats = ['Ne','Ar','Kr','Xe']
    lz_x_d = {}
    lz_c_d = {}
    for iat,at in enumerate(Benchmarks.BM_LIST):
        sa_x[iat] = get_Ex_from_Ix(Ix_d[at],lxpars,xpars)
        sa_c[iat] = get_Ec_from_Ic(Ic_d[at],lcpars,cpars)
        if at in lz_ats:
            lz_x_d[at] = sa_x[iat]
            lz_c_d[at] = sa_c[iat]

    sa_xc = sa_x + sa_c
    sa_xc_pe = 100*(sa_xc-Benchmarks.BM_XC)/(Benchmarks.BM_XC)
    sa_x_pe = 100*(sa_x-Benchmarks.BM_X)/(Benchmarks.BM_X)
    sa_c_pe = 100*(sa_c-Benchmarks.BM_C)/(Benchmarks.BM_C)
    for ierr,err in enumerate(sa_xc_pe):
        norm_pe.append(err*wgts['SA'])
        #norm_pe.append(sa_x_pe[ierr]*wgts['SA'])
        #norm_pe.append(sa_c_pe[ierr]*wgts['SA'])

    resd['SA XC MAPE'] = np.sum( np.abs(sa_xc_pe) )/n_sa
    resd['SA X MAPE'] = np.sum( np.abs(sa_x_pe) )/n_sa
    resd['SA C MAPE'] = np.sum( np.abs(sa_c_pe) )/n_sa

    _, resd['LZ Bx'] = get_coeff_from_ex([lz_x_d['Ne'],lz_x_d['Ar'],lz_x_d['Kr'],lz_x_d['Xe']])
    resd['LZ Bx PE'] = 100*(resd['LZ Bx'] - Benchmarks.LZ_X_B)/(Benchmarks.LZ_X_B)
    norm_pe.append(resd['LZ Bx PE']*wgts['LZ'])

    resd['LZ Bc'] = get_coeff_from_ec([lz_c_d['Ne'],lz_c_d['Ar'],lz_c_d['Kr'],lz_c_d['Xe']])
    resd['LZ Bc PE'] = 100*(resd['LZ Bc'] - Benchmarks.LZ_C_B)/(Benchmarks.LZ_C_B)
    norm_pe.append(resd['LZ Bc PE']*wgts['LZ'])

    n_nsa = len(Benchmarks.NON_SPHERICAL_LIST)
    nsa_x = np.zeros(n_nsa)
    nsa_c = np.zeros(n_nsa)

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        nsa_x[iat] = get_Ex_from_Ix(Ix_d[at],lxpars,xpars)
        nsa_c[iat] = get_Ec_from_Ic(Ic_d[at],lcpars,cpars)

    nsa_xc = nsa_x + nsa_c

    nsa_xc_pe = 100*(nsa_xc-Benchmarks.NON_SPHERICAL_XC)/(Benchmarks.NON_SPHERICAL_XC)

    tmp = np.array(Benchmarks.NON_SPHERICAL_X)
    nsa_x_pe = 100*(nsa_x - tmp)/tmp
    tmp = np.array(Benchmarks.NON_SPHERICAL_C)
    nsa_c_pe = 100*(nsa_c - tmp)/tmp
    for ierr,err in enumerate(nsa_xc_pe):
        norm_pe.append(err*wgts['NSA'])
        #norm_pe.append(nsa_x_pe[ierr]*wgts['NSA'])
        #norm_pe.append(nsa_c_pe[ierr]*wgts['NSA'])

    resd['NSA XC MAPE'] = np.sum( np.abs(nsa_xc_pe) )/n_nsa
    resd['NSA X MAPE'] = np.sum( np.abs(nsa_x_pe) )/n_nsa
    resd['NSA C MAPE'] = np.sum( np.abs(nsa_c_pe) )/n_nsa

    if Settings.DO_JELLIUM:

        n_js = len(Settings.JELLIUM_RS_LIST)
        sigx = np.zeros(n_js)
        sigc = np.zeros(n_js)

        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):
            rs_str = str(rs)
            sigx[irs] = get_Ex_from_Ix(Ix_d[rs_str],lxpars,xpars) \
                - Ix_d[rs_str+'_unif']
            sigc[irs] = get_Ec_from_Ic(Ic_d[rs_str],lcpars,cpars) \
                - Ic_d[rs_str+'_unif']

        sigxc = sigx + sigc

        js_xc_pe = 100*(sigxc - Benchmarks.JELLIUM_XC)/Benchmarks.JELLIUM_XC

        js_x_pe = 100*(sigx - Benchmarks.JELLIUM_X)/Benchmarks.JELLIUM_X
        js_c_pe = 100*(sigc - Benchmarks.JELLIUM_C)/Benchmarks.JELLIUM_C
        for ierr,err in enumerate(js_xc_pe):
            norm_pe.append(err*wgts['JS'])
            #norm_pe.append(js_x_pe[ierr]*wgts['JS'])
            #norm_pe.append(js_c_pe[ierr]*wgts['JS'])

        resd['JS XC MAPE'] = np.sum( np.abs(js_xc_pe) )/n_js
        resd['JS X MAPE'] = np.sum( np.abs(js_x_pe) )/n_js
        resd['JS C MAPE'] = np.sum( np.abs(js_c_pe) )/n_js

    if Settings.DO_AR2:

        n_ar2 = len(Benchmarks.AR2_LIST)
        exl_ar2 = np.zeros(n_ar2)
        ecl_ar2 = np.zeros(n_ar2)

        for i,sep in enumerate(Benchmarks.AR2_LIST):
            # conversion to kcal/mol already included in Ix_d and Ic_d
            exl_ar2[i] = get_Ex_from_Ix(Ix_d[sep],lxpars,xpars)
            ecl_ar2[i] = get_Ec_from_Ic(Ic_d[sep],lcpars,cpars)

        exc_ar2 = exl_ar2 + ecl_ar2
        etot = {}

        for i, sep in enumerate(Benchmarks.AR2_LIST):
            etot[sep] = exc_ar2[i] + Benchmarks.AR2_NO_XC[sep]

        cbs_a = jsextr(np.array([2,3,4]), np.array([etot['ArDZ'], etot['ArTZ'], etot['ArQZ']]))

        ar2_err = []
        n_seps = len(Benchmarks.AR2_SEPS)
        for i,sep in enumerate(Benchmarks.AR2_SEPS):
            cbs_e = jsextr(np.array([2,3,4]), np.array([etot[sep+'DZ'], etot[sep+'TZ'], etot[sep+'QZ']]))
            ar2_err.append(cbs_e - 2*cbs_a - Benchmarks.AR2_BM[sep])
            norm_pe.append(100*ar2_err[i]/Benchmarks.AR2_BM[sep]*wgts['AR2'])
            resd['AR2 XC MAPE'] += 100*abs(ar2_err[i])/(n_seps*Benchmarks.AR2_BM[sep])

    resd['OBJ'] = Settings.NORMWG['SA']*abs(resd['SA XC MAPE']) \
        + Settings.NORMWG['NSA']*abs(resd['NSA XC MAPE']) \
        + Settings.NORMWG['LZBX']*abs(resd['LZ Bx PE']) \
        + Settings.NORMWG['LZBX']*abs(resd['LZ Bc PE']) \
        + Settings.NORMWG['JS']*abs(resd['JS XC MAPE']) \
        + Settings.NORMWG['AR2']*abs(resd['AR2 XC MAPE'])

    if only_obj:
        #return resd['OBJ']
        if penalize_deriv:
            if ind == 'B':
                ceta = None
            elif ind == 'BA':
                ceta = xpars['ETA']
            diefx = dief(al_ref,get_ief_pars(lxpars,xpars['DX'],xpars['f1x'],imin=imin_x),\
                imin=imin_x,ind=ind)
            nbadx = diefx[np.abs(diefx)>5.0].shape[0]#diefx[diefx>0.0].shape[0]
            norm_pe.append(nbadx)
            diefc = dief(al_ref,get_ief_pars(lcpars,cpars['DC'],cpars['f1c'],imin=imin_c),\
                imin=imin_c,ind=ind)
            nbadc = diefc[np.abs(diefc)>5.0].shape[0]#diefc[diefc>0.0].shape[0]
            norm_pe.append(nbadc)

        ax = get_ief_pars(lxpars,xpars['DX'],xpars['f1x'],imin=imin_x)
        fxtmp = series_SCAN_Fx(ptmp,al_ref,ax,xpars,ind=ind)
        nbad_fx = 100*fxtmp[fxtmp<0.0].size
        norm_pe.append(nbad_fx)

        return norm_pe#sum(norm_pe)

    return resd

"""
    Core Optimization
"""

def fx_non_neg_test(ax,xpars,ind='BA'):
    fxtmp = series_SCAN_Fx(ptmp,al_ref,ax,xpars,ind=ind)
    return np.any(fxtmp < 0.0)

def validate_implementation(xpars,cpars,full_dens,non_spherical_dens,jellies,ar2_dens):

    # no free pars. sufficient for testing
    xrank = 4
    crank = 4

    Ix_d = {}
    Ic_d = {}

    ind = 'BA'

    for iat,at in enumerate(Benchmarks.BM_LIST):
        tdens = full_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at],ind=ind)
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at],ind=ind)

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        tdens = non_spherical_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at],ind=ind)
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at],ind=ind)

    if Settings.DO_JELLIUM:
        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):
            tobj = jellies[irs]

            iwg = tobj.weights*tobj.hat*tobj.CFACT
            tmsk = tobj.d >= 1e-20
            rs_str = str(rs)
            gdens = np.abs(tobj.g)
            ked = 2*tobj.tau_0

            Ix_d[rs_str] = get_Ix_unp(tobj.d,gdens,ked,iwg,xpars,xrank,mask=tmsk,\
                ind=ind)

            exuni = -3.0*tobj.kF0/(4.0*pi)
            sxuni = np.dot(tobj.d*exuni, iwg)
            Ix_d[rs_str + '_unif'] = sxuni

            Ic_d[rs_str] = get_Ic(tobj.d_0,tobj.d_1,gdens,ked,\
                iwg,cpars,crank,typ='UNP',mask=tmsk,ind=ind)

            Ic_d[rs_str + '_unif'] = np.dot(tobj.d*tobj.ECPW92(), iwg)

    if Settings.DO_AR2:
        for i in range(len(Benchmarks.AR2_LIST)):
            tdens = ar2_dens[i]
            nm = Benchmarks.AR2_LIST[i]
            Ix_d[nm] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
                tdens[:,5],tdens[:,6],xpars,xrank,typ='UNP',ind=ind)*Benchmarks.EH_TO_KCAL
            Ic_d[nm] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
                tdens[:,6],cpars,crank,typ='UNP',ind=ind)*Benchmarks.EH_TO_KCAL

    tstr = 'Atom, Ex linear, Ex full, Diff X, Ec linear, Ec full, Diff C\n'
    for iat,at in enumerate(Benchmarks.BM_LIST):
        ex = get_Ex_atoms(full_dens[iat],[],xpars,typ=atom_rest[at],ind=ind)
        iex = get_Ex_from_Ix(Ix_d[at],[],xpars)

        ec = get_Ec_atoms(full_dens[iat],[],cpars,typ=atom_rest[at],ind=ind)
        iec = get_Ec_from_Ic(Ic_d[at],[],cpars)
        tstr += ('{:},'*6 + '{:}\n').format(at,iex,ex,ex-iex,iec,ec,ec-iec)

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        ex = get_Ex_atoms(non_spherical_dens[iat],[],xpars,typ=atom_rest[at],ind=ind)
        iex = get_Ex_from_Ix(Ix_d[at],[],xpars)

        ec = get_Ec_atoms(non_spherical_dens[iat],[],cpars,typ=atom_rest[at],ind=ind)
        iec = get_Ec_from_Ic(Ic_d[at],[],cpars)

        tstr += ('{:},'*6 + '{:}\n').format(at,iex,ex,ex-iex,iec,ec,ec-iec)

    if Settings.DO_JELLIUM:
        tstr += '\nJellium Surface rs, sig_x lin, sig_x full, diff x, sig_c lin, sig_c full, diff c\n'
        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):

            rs_str = str(rs)
            sx_lin = get_Ex_from_Ix(Ix_d[rs_str],[],xpars)-Ix_d[rs_str+'_unif']

            def xfun(ps,n0,n1,g0,g1,t0,t1,l0,l1):
                return e_x_series_SCAN(n0,n1,g0,g1,t0,t1,[],xpars,typ='UNP')
            sx_full = jellies[irs].eval_x_func(xfun, {})

            sc_lin = get_Ec_from_Ic(Ic_d[rs_str],[],cpars) - Ic_d[rs_str+'_unif']

            def cfun(ps,n,gn,t,lap,zeta):
                ac = get_ief_pars([],cpars['DC'],cpars['f1c'],imin=imin_c)
                return epsc(n/2,n/2,gn,t,ac,cpars,unp=True)

            sc_full = jellies[irs].eval_c_func(cfun, {})

            tstr += ('{:},'*6 + '{:}\n').format(rs,sx_lin,sx_full,sx_full-sx_lin,\
                sc_lin,sc_full,sc_full-sc_lin)

    if Settings.DO_AR2:
        tstr += '\nAr2, Ex lin, Ex full, diff x, Ec lin, Ec full, diff c\n'
        for i,sep in enumerate(Benchmarks.AR2_LIST):
            ex = get_Ex_atoms(ar2_dens[i],[],xpars,typ='UNP',ind=ind)*Benchmarks.EH_TO_KCAL
            iex = get_Ex_from_Ix(Ix_d[sep],[],xpars)

            ec = get_Ec_atoms(ar2_dens[i],[],cpars,typ='UNP',ind=ind)*Benchmarks.EH_TO_KCAL
            iec = get_Ec_from_Ic(Ic_d[sep],[],cpars)
            tstr += ('{:},'*6 + '{:}\n').format(sep,iex,ex,ex-iex,iec,ec,ec-iec)

    with open('./test_series_scan.csv','w+') as tfl:
        tfl.write(tstr)

    return


def plot_fitted_funcs(cax,cac,ind='BA'):

    fig, ax = plt.subplots(figsize=(6,4))

    if ind == 'B':
        bl = np.linspace(0.0,1.0,2000)
        ya = 2*bl - 1
        dt = 2
    elif ind == 'BA':
        al = np.linspace(0.0,5.0,2000)
        ya = (al - 1)/(al + 1)
        dt = 2/(1 + al)**2

    tmp = ya**3
    dtmp = ya**2*dt

    ief_x = np.zeros_like(ya)
    dief_x = np.zeros_like(ya)
    for i in range(len(cax)):
        ief_x += cax[i]*tmp
        tmp *= ya
        dief_x += (i+3)*cax[i]*dtmp
        dtmp *= ya
    if ind == 'B':
        ax.plot(bl,ief_x,color='darkblue',label='$f_\\mathrm{x}(\\beta)$')
        ax.plot(bl,dief_x,color='darkorange',label="$f_\\mathrm{x}'(\\beta)$")
        ax.set_xlim([0.0,1.0])
        ax.set_xlabel('$\\beta$',fontsize=12)

    elif ind == 'BA':
        ax.plot(al,ief_x,color='darkblue',label='$f_\\mathrm{x}(\\overline{\\alpha})$')
        ax.plot(al,dief_x,color='darkorange',label="$f_\\mathrm{x}'(\\overline{\\alpha})$")
        ax.set_xlim([0.0,al.max()])
        ax.set_xlabel('$\\overline{\\alpha}$',fontsize=12)

    ax.legend(fontsize=12)
    ax.hlines(0.0,*ax.get_xlim(),linewidth=1,color='k')
    plt.show()

    plt.cla()
    plt.clf()
    plt.close()

    fig, ax = plt.subplots(figsize=(6,4))

    tmp = ya**3
    dtmp = dt*ya**2
    ief_c = np.zeros_like(ya)
    dief_c = np.zeros_like(ya)
    for i in range(len(cac)):
        ief_c += cac[i]*tmp
        tmp *= ya
        dief_c += (i+3)*cac[i]*dtmp
        dtmp *= ya

    if ind == 'B':
        ax.plot(bl,ief_c,color='darkblue',label='$f_\\mathrm{c}(\\beta)$')
        ax.plot(bl,dief_c,color='darkorange',label="$f_\\mathrm{c}'(\\beta)$")
        ax.set_xlim([0.0,1.0])
        ax.set_xlabel('$\\beta$',fontsize=12)

    elif ind == 'BA':
        ax.plot(al,ief_c,color='darkblue',label='$f_\\mathrm{c}(\\overline{\\alpha})$')
        ax.plot(al,dief_c,color='darkorange',label="$f_\\mathrm{c}'(\\overline{\\alpha})$")
        ax.set_xlim([0.0,al.max()])
    ax.set_xlabel('$\\overline{\\alpha}$',fontsize=12)

    ax.legend(fontsize=12)
    ax.hlines(0.0,*ax.get_xlim(),linewidth=1,color='k')

    plt.show()

    plt.cla()
    plt.clf()
    plt.close()

    return

def get_Ix_Ic(xrank,crank,xpars,cpars,sa_dens,nsa_dens,jellies,ar2_dens,ind='BA'):

    Ix_d = {}
    Ic_d = {}

    for iat,at in enumerate(Benchmarks.BM_LIST):
        tdens = sa_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at],ind=ind)
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at],ind=ind)

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        tdens = nsa_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at],ind=ind)
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at],ind=ind)

    if Settings.DO_JELLIUM:
        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):
            tobj = jellies[irs]

            iwg = tobj.weights*tobj.hat*tobj.CFACT
            tmsk = tobj.d >= 1e-20
            rs_str = str(rs)
            gdens = np.abs(tobj.g)
            ked = 2*tobj.tau_0

            Ix_d[rs_str] = get_Ix_unp(tobj.d,gdens,ked,\
                iwg,xpars,xrank,mask=tmsk,ind=ind)

            exuni = -3.0*tobj.kF0/(4.0*pi)
            sxuni = np.dot(tobj.d*exuni, iwg)
            Ix_d[rs_str + '_unif'] = sxuni

            Ic_d[rs_str] = get_Ic(tobj.d_0,tobj.d_1,gdens,ked,\
                iwg,cpars,crank,typ='UNP',mask=tmsk,ind=ind)

            Ic_d[rs_str + '_unif'] = np.dot(tobj.d*tobj.ECPW92(), iwg)

    if Settings.DO_AR2:
        for i in range(len(Benchmarks.AR2_LIST)):
            tdens = ar2_dens[i]
            nm = Benchmarks.AR2_LIST[i]
            Ix_d[nm] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
                tdens[:,5],tdens[:,6],xpars,xrank,typ='UNP',ind=ind)*Benchmarks.EH_TO_KCAL
            Ic_d[nm] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
                tdens[:,6],cpars,crank,typ='UNP',ind=ind)*Benchmarks.EH_TO_KCAL

    return Ix_d, Ic_d

def opt_fixed_nl_pars(partup,xpars,cpars,cons_pars,sa_dens,nsa_dens,jellies,\
    ar2_dens,cpbds=None, Ix_ref = {}, Ic_ref = {}, iguess=[],\
    penalize_derivs=False,ind='BA'):

    Ncons = len(cons_pars)

    Nbx, Nbc = partup

    if len(Ix_ref.keys()) == 0 or len(Ic_ref.keys()) == 0:
        xrank = Nbx+4
        crank = Nbc+4

        Ix_d, Ic_d = get_Ix_Ic(xrank,crank,xpars,cpars,sa_dens,nsa_dens,jellies,\
            ar2_dens,ind=ind)

    else:
        Ix_d = Ix_ref
        Ic_d = Ic_ref

    def wrap_obj(comb_pars):
        bx_pars = comb_pars[:Nbx]
        bc_pars = comb_pars[Nbx:Nbx+Nbc]
        conp = comb_pars[Nbx+Nbc:]
        for ipar,apar in enumerate(cons_pars):
            if apar in xpars:
                xpars[apar] = conp[ipar]
            if apar in cpars:
                cpars[apar] = conp[ipar]

        tobj = get_errors(Ix_d,Ic_d,xpars,cpars,bx_pars,bc_pars,\
            only_obj=True, penalize_deriv=penalize_derivs,ind=ind)

        return tobj

    lbds = []
    ubds = []
    for i in range(Nbx+Nbc):
        lbds.append(-np.inf)
        ubds.append(np.inf)

    if cpbds is not None:
        for abd in cpbds:
            lbds.append(abd[0])
            ubds.append(abd[1])
    else:
        for i in range(Ncons):
            lbds.append(-np.inf)
            ubds.append(np.inf)

    if len(iguess) == 0:
        x0 = []
        for i in range(Nbx+Nbc):
            x0.append(1.0)
        for ipar,apar in enumerate(cons_pars):
            if apar in xpars:
                x0.append(xpars[apar])
            if apar in cpars:
                x0.append(cpars[apar])
    else:
        x0 = [x for x in iguess]

    lsrd = least_squares(wrap_obj,x0,bounds=(lbds,ubds))
    #lsrd = minimize(wrap_obj,x0,bounds=[(lbds[i],ubds[i]) for i in range(len(lbds))])

    ax = lsrd.x[:Nbx]
    ac = lsrd.x[Nbx:Nbx+Nbc]
    conp = lsrd.x[Nbx+Nbc:]
    for ipar,apar in enumerate(cons_pars):
        if apar in xpars:
            xpars[apar] = conp[ipar]
        if apar in cpars:
            cpars[apar] = conp[ipar]

    rsd = get_errors(Ix_d,Ic_d,xpars,cpars,ax,ac,only_obj=False,ind=ind)
    return lsrd.x, rsd


def main_opt(nlpar_l,clpar_l,Nbx=0,Nbc=0,validate_lin=False,nlp_bounds=None,\
    bounds=None,ramp=True, penalize_derivs=False,ind='BA',opt_nlp=False):

    # first step, load in defaults for parameters
    xpars = {akey: xpars_default[akey] for akey in xpars_default}
    cpars = {akey: cpars_default[akey] for akey in cpars_default}

    # Now enforce helium atom constraint on correlation energy
    He_dens = generate_density(["He"])

    full_dens = generate_density(Benchmarks.BM_LIST)

    non_spherical_dens = read_QUEST_densities([x+".out.plot" for x in Benchmarks.NON_SPHERICAL_LIST])

    jellies = []
    if Settings.DO_JELLIUM:
        for rs in Settings.JELLIUM_RS_LIST:
            jellies.append(Jelly(rs))

    ar2_list = []
    ar2_dens = []
    if Settings.DO_AR2:

        if Settings.SCAN2_DENS:
            ar2_root = "set_dens/SCAN2/Ar2/"
        else:
            ar2_root = "set_dens/SCAN/Ar2/"

        for f in Benchmarks.AR2_LIST:
            sep = f[:-2]
            bas = f[-2:]
            if sep == "Ar":
                ar2_list.append(ar2_root+"Ar_"+bas+".out.plot")
            else:
                ar2_list.append(ar2_root+"Ar2_"+sep+"_"+bas+".out.plot")

        ar2_dens = read_QUEST_restricted_densities(ar2_list)


    """
        Use this to test whether the series meta-GGA energies are computed correctly
    """
    if validate_lin:
        validate_implementation(xpars,cpars,full_dens,non_spherical_dens,\
            jellies,ar2_dens)
        exit()

    # now enforce Hydrogen atom constraint on exchange energy
    xpars['A1'] = fit_h_atom()
    print('Hydrogen atom constraint:\n      A1 = {:}'.format(xpars['A1']))

    cpars['B1C'], cpars['B2C'], cpars['B3C'] = set_ec0_lda_pars_scan2(cpars,xpars,\
        He_dens[0],ind=ind)
    tstr = 'Two-electron correlation parameters:\n      B1C = {:}\n'.format(cpars['B1C'])
    tstr += '      B2C = {:}\n      B3C = {:}'.format(cpars['B2C'], cpars['B3C'])

    print(tstr)

    """
        Only generate the biggest Ix and Ic dicts once, rather than reevaluate
        them for each possible combo
    """
    xrank = Nbx+4
    crank = Nbc+4

    #Ix_d, Ic_d = get_Ix_Ic(xrank,crank,xpars,cpars,full_dens, non_spherical_dens, \
    #    jellies, ar2_dens)

    if ramp:
        pspace = product(np.arange(0,Nbx+1,1),np.arange(0,Nbc+1,1))
    else:
        pspace = [(Nbx,Nbc)]

    if bounds is not None:
        tbds = [tmp for tmp in bounds]
    else:
        tbds = [(None,None) for i in clpar_l]

    ostr = 'Nbx, Nbc'
    for akey in nlpar_l:
        ostr += ', {:}'.format(akey)
    for akey in clpar_l:
        ostr += ', {:}'.format(akey)

    ostr += ', X IEF Monotonic decr.'
    for i in range(Nbx+3):
        ostr += ', a_x{:}'.format(1+i)

    ostr += ', Non-negative Fx, C IEF Monotonic decr.'
    for i in range(Nbc+3):
        ostr += ', a_c{:}'.format(3+i)

    loptd = {}

    if not opt_nlp:
        # if non-linear pars are fixed across all runs, don't need to re-evaluate
        # linear integrals
        Ix_d, Ic_d = get_Ix_Ic(xrank,crank,xpars,cpars,full_dens, non_spherical_dens, \
            jellies, ar2_dens,ind=ind)

    for ipv,pv in enumerate(pspace):

        ibx,ibc = pv

        if opt_nlp:

            def downhill_nlp(nlpv):
                if not hasattr(nlpv,'__len__'):
                    nlpv = [nlpv]
                for ipv,apv in enumerate(nlpar_l):
                    if apv in xpars:
                        xpars[apv] = nlpv[ipv]
                    if apv in cpars:
                        cpars[apv] = nlpv[ipv]

                cax0,cac0 = get_starting_pars(ibx,ibc,ind=ind,eta=xpars['ETA'])
                x0 = []
                for i in range(1,len(cax0)-2):
                    x0.append(cax0[i])
                for i in range(1,len(cac0)-2):
                    x0.append(cac0[i])
                for apar in clpar_l:
                    if apar in xpars:
                        val = xpars[apar]
                    elif apar in cpars:
                        val = cpars[apar]
                    x0.append(val)

                _, resd = opt_fixed_nl_pars((ibx,ibc), xpars, cpars, clpar_l,\
                    full_dens, non_spherical_dens, jellies, ar2_dens, cpbds=bounds,\
                    Ix_ref = {}, Ic_ref = {}, iguess=x0,\
                    penalize_derivs=penalize_derivs,ind=ind)
                print(nlpv,resd['OBJ'])
                return resd['OBJ']

            if len(nlpar_l) == 1:

                metd = 'brent'
                tbds = (None,None)
                if nlp_bounds is not None:
                    tbds = nlp_bounds[0]
                    metd = 'bounded'
                tmpres = minimize_scalar(downhill_nlp,bounds=tbds,method=metd)
                if tmpres.x in xpars:
                    xpars[apv] = tmpres.x
                if tmpres.x in cpars:
                    cpars[apv] = tmpres.x
            else:
                tbds = [(None, None) for apar in nlp_bounds]
                if nlp_bounds is not None:
                    tbds = nlp_bounds
                nlp0 = []
                for apar in nlpar_l:
                    if apar in xpars:
                        val = xpars[apar]
                    elif apar in cpars:
                        val = cpars[apar]
                    nlp0.append(1.05*val)
                tmpres = minimize(downhill_nlp,nlp0,method='L-BFGS-B',bounds = tbds)
                for ipv,apv in enumerate(nlpar_l):
                    if apv in xpars:
                        xpars[apv] = tmpres.x[ipv]
                    if apv in cpars:
                        cpars[apv] = tmpres.x[ipv]

            if not tmpres.success:
                print(tmpres)

            Ix_d, Ic_d = get_Ix_Ic(ibx+4,ibc+4,xpars,cpars,full_dens, \
                non_spherical_dens, jellies, ar2_dens,ind=ind)

        Ix_tmp = {}
        for akey in Ix_d:
            if akey[-5:] == '_unif':
                Ix_tmp[akey] = Ix_d[akey]
            else:
                Ix_tmp[akey] = Ix_d[akey][:ibx+4]

        Ic_tmp = {}
        for akey in Ic_d:
            if akey[-5:] == '_unif':
                Ic_tmp[akey] = Ic_d[akey]
            else:
                Ic_tmp[akey] = Ic_d[akey][:ibc+4]

        cax0,cac0 = get_starting_pars(ibx,ibc,ind=ind,eta=xpars['ETA'])
        x0 = []
        for i in range(1,len(cax0)-2):
            x0.append(cax0[i])
        for i in range(1,len(cac0)-2):
            x0.append(cac0[i])
        for apar in clpar_l:
            if apar in xpars:
                val = xpars[apar]
            elif apar in cpars:
                val = cpars[apar]
            x0.append(val)

        lin_pars, resd = opt_fixed_nl_pars((ibx,ibc), xpars, cpars, clpar_l,\
            full_dens, non_spherical_dens, jellies, ar2_dens, cpbds=bounds,\
            Ix_ref = Ix_tmp, Ic_ref = Ic_tmp, iguess=x0,\
            penalize_derivs=penalize_derivs,ind=ind)

        nl_pars_fit = lin_pars[ibx+ibc:]
        for ipar,apar in enumerate(clpar_l):
            if apar in xpars:
                xpars[apar] = nl_pars_fit[ipar]
            if apar in cpars:
                cpars[apar] = nl_pars_fit[ipar]

        tmpstr = '{:},{:}'.format(ibx,ibc)
        loptd[tmpstr] = { 'AX': lin_pars[:ibx], 'AC': lin_pars[ibx:ibx+ibc] }
        for apar in clpar_l:
            if apar in xpars:
                loptd[tmpstr][apar] = xpars[apar]
            elif apar in cpars:
                loptd[tmpstr][apar] = cpars[apar]

        ax = get_ief_pars(lin_pars[:ibx],xpars['DX'],xpars['f1x'],imin=imin_x)
        ac = get_ief_pars(lin_pars[ibx:ibx+ibc],cpars['DC'],cpars['f1c'],imin=imin_c)

        if ipv == 0:
            for akey in resd:
                ostr += ', {:}'.format(akey)
            ostr += '\n'

        cres = resd['OBJ']
        tstr = '{:}, {:}'.format(*pv)
        for akey in nlpar_l:
            if akey in xpars:
                val = xpars[akey]
            elif akey in cpars:
                val = cpars[akey]
            tstr += ', {:}'.format(val)

        for val in nl_pars_fit:
            tstr += ', {:}'.format(val)

        tstr += ', {:}'.format(np.all(dief(al_ref,ax,imin=imin_x)<0.0))
        for i in range(len(ax)):
            tstr += ', {:}'.format(ax[i])
        for i in range(len(ax),Nbx+3):
            tstr += ', '

        tstr += ', {:}'.format(not fx_non_neg_test(ax,xpars,ind=ind))

        tstr += ', {:}'.format(np.all(dief(al_ref,ac,imin=imin_c)<0.0))
        for i in range(len(ac)):
            tstr += ', {:}'.format(ac[i])
        for i in range(len(ac),Nbc+3):
            tstr += ', '

        for akey in resd:
            tstr += ', {:}'.format(resd[akey])
        tstr += '\n'

        ostr += tstr
        with open('./fits/series_SCAN.csv','w+') as tfl:
            tfl.write(ostr)

    return

if __name__ == "__main__":

    """
    cax = [0.9995425119895545, 1.000279391685005, -2.1230489726832507, -1.1237858523787012]
    cac = [1.0000091143538343, 0.999982392837519, -1.8499641196736945, -0.8499373981573792]
    #print(fx_non_neg_test(cax,xpars_default,ind='BA'))
    #exit()
    plot_fitted_funcs(cax,cac)
    exit()
    #"""

    main_opt(['K1'],['f1x','DX','f1c','DC'],Nbx=2,Nbc=2,validate_lin=False,\
        bounds = [(-10.0,0.0),(0.0,1/.174),(-10.0,0.0),(0.0,5.0)],ramp=False,\
        penalize_derivs=False,ind='BA',opt_nlp=False,nlp_bounds=[(0.02,0.174)])
