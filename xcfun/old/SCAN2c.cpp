#include "functional.hpp"
#include "constants.hpp"
#include "SCAN2_eps.hpp"

template<class num>
static num SCAN2c(const densvars<num> &d)
{
  num eps_c = SCAN2_eps::SCAN2_C(d); 
 
  return eps_c;
}

FUNCTIONAL(XC_SCAN2_C) = {
  "SCAN2 Correlation functional",
  "SCAN2 Correlation functional.\n",
  XC_DENSITY | XC_GRADIENT | XC_KINETIC,
  ENERGY_FUNCTION(SCAN2c)
  XC_A_B_GAA_GAB_GBB_TAUA_TAUB,
  XC_PARTIAL_DERIVATIVES,
};
