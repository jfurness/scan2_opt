#include "functional.hpp"
#include "constants.hpp"
#include "SCAN2_eps.hpp"

template<class num>
static num SCAN2X(const densvars<num> &d)
{
  num Fx_a;
  num Fx_b;
  Fx_a = SCAN2_eps::SCAN2_X(2.0*d.a, 4.0*d.gaa, 2.0*d.taua); 
  num epsxunif_a = SCAN2_eps::fx_unif2(2*d.a);
  Fx_b = SCAN2_eps::SCAN2_X(2.0*d.b, 4.0*d.gbb, 2.0*d.taub); 
  num epsxunif_b = SCAN2_eps::fx_unif2(2*d.b);

  return 0.5*(Fx_a*epsxunif_a + Fx_b*epsxunif_b);
}

FUNCTIONAL(XC_SCAN2_X) = {
  "SCAN2 exchange functional",
  "SCAN2 exchange functional.\n"
  "Sun, J.; Ruzsinszky, A.; Perdew J.\n"
  "Strongly Constrained and Appropriately Normed Semilocal Density Functional\n"
  "J. P. Phys. Rev. Lett. 2015, 115 (3), 036402\n"
  "Implemented by James Furness\n",
  XC_DENSITY | XC_GRADIENT | XC_KINETIC,
  ENERGY_FUNCTION(SCAN2X)
  XC_A_B_GAA_GAB_GBB_TAUA_TAUB,
  XC_PARTIAL_DERIVATIVES,
};
