from math import pi, e
import numpy as np
from scipy.optimize import minimize_scalar, minimize
import SCAN2
import itertools
from Atomics import wrap_get_Ec
from Integrators import get_Ex_for_atoms, get_Ec_for_atoms
import Settings
import matplotlib.pyplot as plt

def set_a1x(params):
    print("Determining a1x by hydrogen Exchange...")
    low_a1x = 0.0
    high_a1x = 4.0

    # Grid
    h = 0.001
    r = np.arange(0.0, 35.0, h)

    # Make density
    exponent = 2.0
    N = (exponent**3/(8.0*pi))
    d0 = N*np.exp(-exponent*r)
    g0 = np.abs(-N*exponent*np.exp(-exponent*r))
    t0 = 0.5*(np.sqrt(N)*0.5*exponent*np.exp(-0.5*exponent*r))**2
    l0 = N*exponent**2*np.exp(-exponent*r)

    if Settings.H0X == "MGG2":
        tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*(2*d0)**(5.0/3.0)
        tauw = ((2*g0)**2)/(16*d0)
        beta = (2*t0 - tauw)/(2*t0 + tau_unif)
    else:
        beta = None

    #  Run Scipy bisection search for minial H X error
    res = minimize_scalar(wrap_x_func, bracket=(low_a1x, high_a1x), args=(params, d0, g0, beta, r, h))

    print("\nFinal a1x found: {:}".format(res.x))
    print("Error:           {:}".format(res.fun))

    return res.x

def wrap_x_func(a1x, params, d0, g0, beta, r, h):
    """
    Wrapper funciton to set parameter and evaluate error
    Adapted for scipy's calling structure
    """

    params['A1'] = a1x
    scan_x_den = SCAN2.get_single_orbital_exchange(params, d0, g0)#, beta=beta)
    scan_ex = np.sum(4*pi*r**2*h*scan_x_den)
    # scano_x_den = SCAN2.getscan_x(params, d0, np.zeros(d0.shape), g0, np.zeros(d0.shape), t0, np.zeros(d0.shape), l0, np.zeros(d0.shape), only_0=True)
    # scano_ex = np.sum(4*pi*r**2*h*scano_x_den)
    # raise SystemExit
    obj = abs(scan_ex + 0.3125)
    print("Tried {:10.8f}, got {:9.8e}, ({:.4f})".format(a1x, obj, scan_ex))
    return obj

def set_ec0_lda_pars_scan2(cparams,xparams,He_dens):

    # Grid
    h = 0.001
    r = np.arange(0.0, 35.0, h)

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    d = 2/pi*np.exp(-2*r)
    s = np.exp(2*r/3)/(6*pi)**(1/3)
    func = SCAN2.eps_c_0_high_dens_zeta_0

    cparams['B1C'] = 1.0
    c_den = func(cparams, s)
    Ec = 4*pi*h*np.sum(r**2*d*c_den)
    cparams['B1C'] = -0.0467/Ec
    print("Optimal B1C:", cparams['B1C'])
    c_den_opt = func(cparams, s)
    Ec_opt = np.sum(4*pi*r**2*h*d*c_den_opt)
    print("Is consistent? ", abs(Ec_opt + 0.0467) < 1e-16)

    cparams['B3C'] = cparams['B1C']*4*pi/3*(4/(9*pi))**(1/3)/(0.67082 - 0.174)
    He_ex = get_Ex_for_atoms(He_dens, xparams)[0]
    b2c_res = minimize_scalar(wrap_get_Ec, args=(cparams, He_dens, He_ex), bracket=(0.07,0.09))

    if b2c_res.fun > 1e-9:
        print("Failed on b2c")
        return None
    cparams['B2C'] = abs(b2c_res.x)

    return cparams['B1C'],cparams['B2C'], cparams['B3C']


def set_b1c(params):
    low_b1c = 0.03109069
    high_b1c = 0.04109069

    # Grid
    h = 0.001
    r = np.arange(0.0, 35.0, h)

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    exponent = 1.0
    N = (exponent**3/(8.0*pi))
    d = 2*N*np.exp(-exponent*r)
    g = np.abs(-2*N*exponent*np.exp(-exponent*r))
    s = g/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(d, 4.0/3.0))

    func = SCAN2.eps_c_0_high_dens_zeta_0

    ANALYTIC = not do_SCAN2  # We can no longer algebraically determine B1C, must optimise

    if ANALYTIC:
        params['B1C'] = 1.0
        c_den = func(params, s)
        Ec = np.sum(4*pi*r**2*h*d*c_den)
        b1c = -0.0467/Ec
        print("\nIntegrated energy @B1C = 1.0 is", Ec, " (-0.0467 target)")
        print("Optimal B1C should is:", b1c)
        params['B1C'] = b1c
        c_den_opt = func(params, s)
        Ec_opt = np.sum(4*pi*r**2*h*d*c_den_opt)
        print("Is consistent? ", abs(Ec_opt + 0.0467) < 1e-16)
        return b1c

    else:
        print("\nGenerating guess b1c with Chi_4^2 from equality.")
        set_chi_42 = True
        # Scipy bisection search
        res = minimize_scalar(wrap_c_func, args=(params, d, s, r, h, func, set_chi_42), bracket=(low_b1c, high_b1c))
        b1c = res.x
        print("\n Found b1c = {:} with error: {:}".format(b1c, res.fun))

    print("\n\"Guess\" b1c is:", b1c)

    params['B1C'] = b1c
    chi_0_zeta_0 = params['BETA_RS_0']*(3*pi**2/16.0)**(2.0/3.0)/(params['C_TILDE']*params['GAMMA'])*np.exp(-params['C1_CONST']/params['GAMMA'])
    params['CHI_42'] = params['B1C']/(params['GAMMA'])*(e - 1)/e*chi_0_zeta_0**2

    c_den_0 = SCAN2.eps_c_0_high_dens_zeta_0(params, s, chi_42=params['CHI_42'])
    Ec_0 = np.sum(4*pi*r**2*h*d*c_den_0)

    c_den_1 = SCAN2.eps_c_1_high_dens_zeta_0(params, s)
    Ec_1 = np.sum(4*pi*r**2*h*d*c_den_1)

    print("Ec_0:",Ec_0)
    print("Ec_1:",Ec_1)
    # We must ensure ec1 < ec0
    print("is ec1 < ec0? ", check_e1c_lt_e0c(params, 0.0))

    lowerchi_42 = 0.001
    upperchi_42 = 0.3
    if check_e1c_lt_e0c(params, 0.0):
        print("Guess value is acceptable!")
    else:
        print("Bisection search to find lowest acceptable chi_4^2.")
        optchi_42, opt_b1c = bisection_search(params, lowerchi_42, upperchi_42, 1e-6, 'CHI_42', r, d, h, s, func)
        print("Best chi_42 is ", optchi_42, "up from", lowerchi_42)

        # for b1c in np.linspace(0.0, upperb1c, 100):
        #     params['B1C'] = b1c
        #     print "B1C: {:}, {:}".format(b1c, check_e1c_lt_e0c(params, 0.0))
    params['CHI_42'] = optchi_42

    c_den_0 = SCAN2.eps_c_0_high_dens_zeta_0(params, s, chi_42=params['CHI_42'])
    Ec_0 = np.sum(4*pi*r**2*h*d*c_den_0)

    c_den_1 = SCAN2.eps_c_1_high_dens_zeta_0(params, s)
    Ec_1 = np.sum(4*pi*r**2*h*d*c_den_1)

    print("\nB1C and CHI_4^2 SUMARRY")
    print("========================")
    print("B1C = ", opt_b1c)
    print("CHI_4^2", optchi_42)
    print("Ec_0:", Ec_0, "BM:", -0.0467, "Err ({:.3f}%)".format(100*(Ec_0 + 0.0467)/(-0.0467)))
    print("Ec_1:",Ec_1)
    print("is ec1 < ec0? ", check_e1c_lt_e0c(params, 0.0))

    return b1c

def wrap_c_func(b1c, params, d, s, r, h, func, set_chi_42):
    params['B1C'] = abs(b1c)

    if set_chi_42:
        c_den = func(params, s)
    else:
        c_den = func(params, s, chi_42=params['CHI_42'])
    Ec = np.sum(4*pi*r**2*h*d*c_den)
    obj = abs(Ec + 0.0467)
    # print "Tried {:10.8f}, got {:9.8e}".format(b1c, obj)
    return obj

def check_e1c_lt_e0c(params, tol, s):
    ec0 = SCAN2.working_eps_c_0(params, s)
    ec1 = SCAN2.eps_c_1_high_dens_zeta_0(params, s)
    # check with float fudge factor
    # print "With CHI_42 = {:}, Max diff {:} at s = {:}".format(params['CHI_42'], np.max(diff), s[diff.argmax()])
    # if not np.all(ec1 <= ec0):

    #     print (s[(ec1 > ec0)])[0], (ec1[(ec1 > ec0)])[0], (ec0[(ec1 > ec0)])[0]
    return np.all(ec1 <= ec0)

def check_e1c_lt_e0c_rs1(params, tol, s):
    rs = 1.0
    ec0 = SCAN2.corgga_0_acgga(params, rs, s, np.zeros(s.shape))
    ec1 = SCAN2.corgga_1_acgga(params, rs, s, np.zeros(s.shape))
    # check with float fudge factor
    # print "With CHI_42 = {:}, Max diff {:} at s = {:}".format(params['CHI_42'], np.max(diff), s[diff.argmax()])
    return np.all(ec1 <= ec0)


def check_lsda1_lt_lsda0(params, tol, rs):
    lsda0 = SCAN2.lsda_0(params, rs)
    lsda1 = SCAN2.lsda_1(rs, np.zeros(rs.shape))
    return np.all(lsda1 <= lsda0)

def bisection_search(params, low_x, high_x, tol, key, r, d, h, s, func):
    """
    Recursive bisection search to locate largest acceptable x
    """
    print("===================")
    ran = (high_x-low_x)
    if ran < tol:
        print("Result between {:} and {:}, finished! Taking upper...".format(low_x, high_x))

        print("Optimising b1c for new chi_42 = ",high_x)
        set_chi_42 = False
        params['CHI_42'] = high_x
        low_b1c = 0.03109069
        high_b1c = 0.04109069
        res = minimize_scalar(wrap_c_func, args=(params, d, s, r, h, func, set_chi_42), bracket=(low_b1c, high_b1c))
        opt_b1c = res.x
        params['B1C'] = opt_b1c
        c_den_0 = SCAN2.eps_c_0_high_dens_zeta_0(params, s, chi_42=params['CHI_42'])
        Ec_0 = np.sum(4*pi*r**2*h*d*c_den_0)
        print("With b1c = ",opt_b1c,", Ec_0:", Ec_0, "Err ({:.3f}%)".format(100*(Ec_0 + 0.0467)/(-0.0467)))

        return high_x, opt_b1c

    x = low_x + ran*0.5

    print("Optimising b1c for new chi_42 = ", x)
    set_chi_42 = False
    params['CHI_42'] = x
    low_b1c = 0.03109069
    high_b1c = 0.04109069
    res = minimize_scalar(wrap_c_func, args=(params, d, s, r, h, func, set_chi_42), bracket=(low_b1c, high_b1c))
    opt_b1c = res.x
    chi_0_zeta_0 = params['BETA_RS_0']*(3*pi**2/16.0)**(2.0/3.0)/(params['C_TILDE']*params['GAMMA'])*np.exp(-params['C1_CONST']/params['GAMMA'])
    foo = params['B1C']/params['GAMMA']*(e - 1)/e*chi_0_zeta_0**2
    print("CHI_42 is {:} [Equality is {:} from B1C ({:})]".format(x, foo, opt_b1c))
    params['B1C'] = opt_b1c
    c_den_0 = SCAN2.eps_c_0_high_dens_zeta_0(params, s, chi_42=params['CHI_42'])
    Ec_0 = np.sum(4*pi*r**2*h*d*c_den_0)
    print("With b1c = ",opt_b1c,", Ec_0:", Ec_0, "Err ({:.3f}%)".format(100*(Ec_0 + 0.0467)/(-0.0467)))

    if check_e1c_lt_e0c(params, 1e-17):
        print(x, "too large.")
        out_chi_42, opt_b1c = bisection_search(params, low_x, x, tol, key, r, d, h, s, func)
    else:
        print(x, "too small.")
        out_chi_42, opt_b1c = bisection_search(params, x, high_x, tol, key, r, d, h, s, func)
    return out_chi_42, opt_b1c



def chi2_chi4_b1c_search(cparams, xparams, He_dens):
    """
    Old Chi2 and chi_4 search function

    this uses a dense global grid to minimise error.

    """
    # Grid
    h = 0.001
    r = np.arange(0.0, 35.0, h)

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    exponent = 1.0
    N = (exponent**3/(8.0*pi))
    d = 2*N*np.exp(-exponent*r)
    g = np.abs(-2*N*exponent*np.exp(-exponent*r))
    s = g/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(d, 4.0/3.0))

    # Helium exchange energy
    He_ex = get_Ex_for_atoms(He_dens, xparams, True)[0]

    # X Print correlation and exchange energy of He. For SCAN as well.

    # 1) Generate document of equations
    # 2) Detail parameters as we have them, Ec_ rs->0 2e
    # 3) Free minimize (b1c, b2c) against He_xc to get new set of parameters.
    # 4) Add to document.

    # This is adaptive for the number of decimal places we want in (chi_2, chi_42) iteratively refining
    chi_2_target = 1.0
    chi_42_target = 1.0
    for i in range(1, 2):
        step = 10**(-i)
        min_chi2 = chi_2_target - 5*step
        min_chi42 = chi_42_target - 5*step
        duples = itertools.product(chi_2, chi_42)

    chi_2 = np.arange(min_chi2, max_chi2, step_chi2)
    chi_42 = np.arange(min_chi42, max_chi42, step_chi42)
    # chi_42 = chi_2**2

    duples = itertools.product(chi_2, chi_42)
    # pool = Pool()

    valid_set = [is_valid_set(cparams, xparams, t, d, s, r, h, He_dens, He_ex) for t in duples]

    valid_set = [_f for _f in valid_set if _f]
    # valid_set = filter(None, map(lambda t: is_valid_set(cparams, xparams, t[0], t[1], d, s, r, h, He_dens, He_ex), itertools.izip(chi_2, chi_42)))

    valid_set = [t for t in valid_set if t['CHI_42'] > t['AN_CHI_42']]
    # valid_set = filter(lambda t: t['B2C'] > 0.0, valid_set)
    # valid_set = sorted(valid_set, key = lambda t: abs(t['B1C'] - cparams['GAMMA']))
    valid_set = sorted(valid_set, key = lambda t: t['Exc[He]_err'])

    print("\nPARAMETER SEARCH (found {:})".format(len(valid_set)))
    print("================\n")
    print("\nHelium Exchange: {:}\n".format(He_ex))
    for v in valid_set:
        print("CHI_2 = {:.6f}({:.6f}), CHI_42 = {:.6f}({:.6f}), B1C = {:.6f}({:.6f}), B2C = {:.6f}({:.4f}), B3C = {:.6f}, He_c = {:}".format(v['CHI_2'], v['AN_CHI_2'], v['CHI_42'], v['AN_CHI_42'], v['B1C'], v['Ec_err'], v['B2C'], v['Exc[He]_err'], v['B3C'], v['He_c']))


def chi2_chi4_b1c_adaptive(cparams, xparams, He_dens):
    """
    Better search routine that recursively determines parameters
    to higher precision by increasing grid density around best currently known point.
    """

    # This isn't changed by IE function. So while work is ongoing there we can skip the optimisation
    # # and just return the values we know it will find. Eventually we should re run this when all else is set.
    # print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    # print "----->    SKIPPING OPTIMISATION FOR BREVITY    <-----"
    # print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

    # return {'CHI_2':0.0, 'CHI_42':0.445000, 'B1C':0.040982, 'B2C':0.100689, 'B3C':0.180040}

    # Grid
    h = 0.01
    r = np.arange(0.0, 15.0, h)

    rs_check = np.logspace(-2, 5, 250)
    s_check = np.logspace(-2, 5, 250)

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    exponent = 1.0
    N = (exponent**3/(8.0*pi))
    d = 2*N*np.exp(-exponent*r)
    g = np.abs(-2*N*exponent*np.exp(-exponent*r))
    s = g/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(d, 4.0/3.0))
    idxs = d > 1e-10
    d = d[idxs]
    g = g[idxs]
    s = s[idxs]
    r = r[idxs]

    # Helium exchange energy
    He_ex = get_Ex_for_atoms(He_dens, xparams)[0]

    print("\nPARAMETER SEARCH")
    print("================\n")
    print("\nHelium Exchange: {:}\n".format(He_ex))

    # This is adaptive for the number of decimal places we want in (chi_2, chi_42) iteratively refining

    # for i in range(1, SIGNIFICANT_FIGURES):  # Starting precision, final precision in decimal places
    step = 0.005
    min_chi2 = 0.0
    max_chi2 = 1.0
    min_chi42 = 0.0
    max_chi42 = 1.0
    print("Searching chi_2 = [{:}, {:}], chi_42 = [{:}, {:}] in steps of {:}".format(min_chi2, max_chi2, min_chi42, max_chi42, step))

    chi_2 = np.arange(min_chi2, max_chi2, step)
    chi_42 = np.arange(min_chi42, max_chi42, step)
    duples = itertools.product(chi_2, chi_42)

    valid_set = []
    for t in tqdm(duples,total=len(chi_2)*len(chi_42),disable=Settings.NO_PROG):
        res = is_valid_set(cparams, xparams, t, d, s, r, h, He_dens, He_ex, rs_check, s_check)
        if res is not None and res['CHI_42'] > res['AN_CHI_42']:
            valid_set.append(res)

    assert len(valid_set) > 0, "No parameters passed filters :'("

    valid_set = sorted(valid_set, key=lambda t: t['B1C'])

    print("\n===============")
    print("DONE")
    with open("valid_ec0.csv","w") as out:
        out.write("CHI_2,CHI_42,B1C,B2C,B3C\n")
        for v in valid_set:
            out.write("{:.6f},{:.6f},{:.6f},{:.6f},{:.6f}\n".format(v['CHI_2'], v['CHI_42'], v['B1C'], v['B2C'], v['B3C']))
    print("\nHelium Exchange:     {:}".format(He_ex))
    print("Helium correlation:  {:}".format(valid_set[0]['He_c']))
    print("Helium xc:           {:}".format(He_ex + valid_set[0]['He_c']))
    return valid_set[0]

def is_valid_set(cparams, xparams, chis, d, s, r, h, He_dens, He_ex, param_sum):
    """
    Check that the parameters fit two constraints:
    (1) Ec of 2e high density limit = -0.0467 Eh
    (2) eps_0 > eps_1 for all s
    """

    # First find b1c such that (1) is obeyed
    cparams['CHI_2'] = chis[0]
    cparams['CHI_42'] = chis[1]
    # cparams['CHI_2'] = 0.0
    # cparams['CHI_42'] = 0.444228
    res_tol = 1e-5
    if cparams['CHI_2'] < 0 or cparams['CHI_42'] < 0:
        return None
    B1C_PREC = 4
    try:
        res = minimize_scalar(b1c_objective, args=(cparams, d, s, r, h), tol=res_tol, options={'xtol':10**-B1C_PREC})
    except ValueError:
        print("Couldn't find B1C for CHI_2 {:}, CHI_42 {:}".format(cparams['CHI_2'], cparams['CHI_42']))
        return None
    # b1c = round(res.x, 4)
    b1c = res.x
    if res.fun > res_tol or b1c < 0:
        return None

    cparams['B1C'] = b1c
    par = 3.0/(4.0*pi)*(9.0*pi/4.0)**(1.0/3.0)
    fac = (1.67082-1.174)*par
    cparams['B3C'] = cparams['B1C']/fac

    b2c_res = minimize_scalar(wrap_get_Ec, args=(cparams, He_dens, He_ex), bracket=(0.07,0.09), tol=res_tol)

    if b2c_res.fun > res_tol:
        return None
    cparams['B2C'] = abs(b2c_res.x)

    # Now check (2)
    tol = 0.0
    if check_e1c_lt_e0c_rs_many(cparams, tol, param_sum):
        chi_0_zeta_0 = SCAN2.BETA_RS_0*(3*pi**2/16.0)**(2.0/3.0)/(SCAN2.C_TILDE*SCAN2.GAMMA)*np.exp(-SCAN2.C1_CONST/SCAN2.GAMMA)
        test = SCAN2.GAMMA/cparams['B1C']*chi_0_zeta_0**(-3)*cparams['CHI_42']**2*(e-1)/e
        fourth = cparams['B1C']/SCAN2.GAMMA*chi_0_zeta_0*(e - 1)/e

        ec = get_Ec_for_atoms(He_dens, cparams)[0]


        return {'CHI_2': cparams['CHI_2'], 'CHI_42': cparams['CHI_42'],
                'AN_CHI_2':test, 'AN_CHI_42':fourth,
                'Ec_err':res.fun, 'Exc[He]_err':b2c_res.fun, 'He_c':ec,
                'H1DC':cparams['H1DC'],
                'B1C':cparams['B1C'], 'B2C':cparams['B2C'], 'B3C':cparams['B3C']
                }
    else:
        return None

def b1c_objective(b1c, params, d, s, r, h):
    params['B1C'] = b1c
    dthrd = d**(1.0/3.0)
    rs = (0.75/pi)**(1.0/3.0)/dthrd
    # if Settings.CORRELATION == "COMPENSATED" and False:
    #     c_den = SCAN2.corgga_0_acgga(params, rs, s, np.zeros_like(r))
    # else:
    c_den = SCAN2.working_eps_c_0(params, s)

    Ec = np.sum(4*pi*r**2*h*d*c_den)
    obj = abs(Ec + 0.0467)
    # print "B1C {:} Ec {:}, obj {:}".format(b1c, Ec, obj)
    return obj

def check_e1c_lt_e0c_rs_many(params, tol, param_sum):
    """
    Try many rs now.
    """
    out = True
    lrs = np.logspace(-4, 4, 100)
    s = np.logspace(-2, 3, 200)
    zeta = 0.0
    beta = 0.0  # Beta = 1 is worst case
    tol = 0.0

    ec0_inf = SCAN2.corgga_0_acgga_rs_inf(params, s, zeta)
    ec1_inf = SCAN2.corgga_1_rs_inf_v4(params, s, beta, zeta, sumis=param_sum)

    if np.any(ec1_inf > ec0_inf):
        return False

    # Skip rs -> 0 for now
    # ec0_0 = SCAN2.corgga_0_acgga_rs_0(params, s, zeta)
    # ec1_0 = SCAN2.corgga_1_comp_rs_0(params, s, zeta)

    # if np.any(ec1_0 > ec0_0):
    #     return False
    # else:
    #     return True

    PLOT = False

    for rs in lrs:
        if Settings.CORRELATION == "ORIGINAL":
            ec0 = SCAN2.corgga_0_ori(params, rs, s, zeta)
            ec1 = SCAN2.corgga_1(params, rs, s, zeta)
        elif Settings.CORRELATION == "COMPENSATED":
            ec0 = SCAN2.corgga_0_ori(params, rs, s, zeta)
            ec1 = SCAN2.corgga_1_comp_v4(params, rs, s, zeta, sumis=param_sum)
        if np.any(ec1 > ec0 + tol):
            # rs=10
            if PLOT:
                ls = np.logspace(-2, 2, 1000)
                ec0 = SCAN2.corgga_0(params, rs, ls, 0.0)

                # ec1o = SCAN2.corgga_1_ori(params, np.ones_like(ls)*rs, ls, 0.0)
                ec1 = SCAN2.corgga_1_comp_v4(params, rs, ls, 1.0, 0.0, sumis=param_sum)
                # ec1OLD = SCAN2.OLD_corgga_1_comp(params, np.ones_like(ls)*rs, ls, np.zeros_like(ls), maxbound=True)

                fig = plt.figure()
                ax1 = fig.add_subplot(111)
                ax1.semilogx(ls, ec0, label="$\\epsilon_{c}^{0}$", c='green')
                ax1.semilogx(ls, ec1ac, label="$\\epsilon_{c}^{1(\\mathrm{acGGA+})}}$", ls=':', c='orangered')
                # ax1.semilogx(ls, ec1o, label="$\\epsilon_{c}^{1}$ ORI")
                ax1.semilogx(ls, ec1, label="$\\epsilon_{c}^{1(\\mathrm{comp})}}$ $d_c = $"+str(params['H1DC']), ls="--", c='darkblue')
                # ax1.semilogx(ls, ec1min, ls="--", c='darkblue')
                # ax1.fill_between(ls, ec1min, ec1, alpha=0.15, color='darkblue')
                # ax1.semilogx(ls, ec1OLD, label="$\\epsilon_{c}^{1}$ OLD", ls=":")
                ax1.set_xlabel("s")
                ax1.set_xlim([1e-2,1e2])

                # ax2 = fig.add_subplot(212)
                # ax2.semilogx(ls, ec1 - ec1ac, label="$\\epsilon_{c}^{1\\mathrm{COMP}} - \\epsilon_{c}^{1\\mathrm{AC}}$")
                # ax2.hlines(0, np.min(ls), np.max(ls), colors='gray', linestyles=":")
                fig.suptitle("$r_s$: {:}, $\\chi_2$: {:.3f}, $\\chi_4^2$: {:.3f},\nB1C: {:.3f}, B2C: {:.3f}, B3C: {:.3f}".format(rs, params['CHI_2'], params['CHI_42'], params['B1C'], params['B2C'], params['B3C']))

                ax1.legend()
                # ax2.legend()
                # ax2.set_xlabel("s")
                plt.show()
                raise SystemExit
            out = False
            break

    return out


def set_original_SCAN(cparams, xparams, He_dens):
    """
    This repeats the analysis with the original SCAN functional,
    to confirm the validity of the code.
    """
    # Grid
    h = 0.001
    r = np.arange(0.0, 35.0, h)

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    exponent = 1.0
    N = (exponent**3/(8.0*pi))
    d = 2*N*np.exp(-exponent*r)
    g = np.abs(-2*N*exponent*np.exp(-exponent*r))
    s = g/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(d, 4.0/3.0))

    # Helium exchange energy
    He_ex = get_Ex_for_atoms(He_dens, xparams)[0]

    print("\nPARAMETER SEARCH")
    print("================\n")
    print("\nHelium Exchange: {:}\n".format(He_ex))

    res = minimize_scalar(original_b1c_obj, args=(cparams, d, s, r, h))
    print("b1c found as: ", res.x, "with error", res.fun)
    cparams['B1C'] = res.x
    par = 3.0/(4.0*pi)*(9.0*pi/4.0)**(1.0/3.0)
    fac = (1.67082-1.174)*par
    cparams['B3C'] = cparams['B1C']/fac
    print("b3c found as: ", cparams['B3C'])

    b2c_res = minimize_scalar(wrap_get_Ec, args=(cparams, He_dens, He_ex), bracket=(0.07,0.08))
    cparams['B2C'] = abs(b2c_res.x)
    ec = get_Ec_for_atoms(He_dens, cparams)[0]
    print("b2c found as: ", cparams['B2C'], "with error", res.fun)
    # cparams['B2C'] = 0.0889
    # ec_old = get_Ec_for_atoms(He_dens, cparams, False)[0]
    # print "Ec[He] with our value is:", ec
    # print "Ec[He] with old value is:", ec_old, "Err:", abs(He_ex + ec_old+ 1.068)
    return dict(B1C=cparams['B1C'], B2C=cparams['B2C'], B3C=cparams['B3C'])

def original_b1c_obj(b1c, params, d, s, r, h):
    params['B1C'] = b1c
    c_den = SCAN2.eps_c_0_high_dens_zeta_0_original(params, s)
    Ec = np.sum(4*pi*r**2*h*d*c_den)
    obj = abs(Ec + 0.0467)
    return obj


def set_c_in_ms0_h0x(xparams):
    exponent = 2.0
    h = 0.001
    r = np.arange(0.0, 35.0, h)
    N = (exponent**3/(8.0*pi))
    d = N*np.exp(-exponent*r)
    g = np.abs(-N*exponent*np.exp(-exponent*r))
    t = 0.5*(np.sqrt(N)*0.5*exponent*np.exp(-0.5*exponent*r))**2
    l = N*exponent**2*np.exp(-exponent*r)

    func = SCAN2.getscan_x

    #  Run Scipy bisection search for minial H X error
    C_START = 0.28771
    A1X_START = 4.9479
    res = minimize_scalar(constrained_ms0_obj, (4.0, 10.0), args=(xparams, d, g, t, l, r, h))
    # res = minimize(ms0_h0x_obj, (C_START, A1X_START), args=(xparams, d, g, t, l, r, h), method='Nelder-Mead')

    # print "\nFinal a1x found: {:}".format(abs(res.x[1]))
    # print "Final c found:   {:}".format(abs(res.x[0]))
    print("\nFinal a1x found: {:}".format(abs(res.x)))
    a1 = abs(res.x)
    ct2 = 0.426*np.sqrt(a1*(-0.038+0.001*a1 + 0.038*np.exp(1.130*a1)))/a1
    c = -0.237 + ct2
    print("Final c found:   {:}".format(c))
    print("Error:           {:}".format(np.sqrt(res.fun)))

    xparams['C'] = c
    xparams['A1'] = a1

    eps_x = SCAN2.get_single_orbital_exchange(xparams, d, g)

    print("Ex was ", np.sum(4*pi*r**2*h*eps_x))

    assert np.sum(4*pi*r**2*h*eps_x) + -0.3125 < 1e-4, "Not good enough."

    return (xparams['C'], xparams['A1'])


def ms0_h0x_obj(par, xparams, d, g, t, l, r, h):
    xparams['C'] = abs(par[0])
    xparams['A1'] = abs(par[1])
    scan_x_den = SCAN2.getscan_x(xparams, d, np.zeros(d.shape), g, np.zeros(d.shape), t, np.zeros(d.shape), l, np.zeros(d.shape), only_0=True)
    ex = np.sum(4*pi*r**2*h*scan_x_den)
    obj = abs(ex - (-0.3125))**2
    print("Trying a1x = {:10.8f}, c = {:10.8f}, got {:9.8e}".format(xparams['A1'], xparams['C'], ex))

    return obj


def constrained_ms0_obj(par, xparams, d, g, t, l, r, h):
    a1 = abs(par)
    xparams['A1'] = a1

    ct2 = 0.426*np.sqrt(a1*(-0.038+0.001*a1 + 0.038*np.exp(1.130*a1)))/a1
    c = -0.237 + ct2
    assert c >= 0, "ERROR: Do we allow negative c? cp {:}, cm {:}".format(cp, cm)

    xparams['C'] = c
    scan_x_den = SCAN2.get_single_orbital_exchange(xparams, d, g)
    ex = np.sum(4*pi*r**2*h*scan_x_den)
    obj = abs(ex - (-0.3125))**2
    print("Trying a1x = {:10.8f}, c = {:10.8f}, got {:9.8e}".format(xparams['A1'], xparams['C'], ex))

    return obj
