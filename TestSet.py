import numpy as np
import h5py
from itertools import product
from Integrators import get_Ex_for_atoms, get_Ec_for_atoms, get_Ec_non_colinear, get_Ex_from_lin, get_Ec_from_lin
import SCAN2
import Benchmarks
from Atomics import print_full_report
from scipy.optimize import minimize
from random import shuffle
import Settings

def load_parameter_set(fname):
    print("Loading parameters...")
    ugrid = []
    with open(fname, 'r') as inp:
        for line in inp:
            try:
                bits = list(map(float, [_f for _f in line.strip().split(',') if _f]))
                ugrid.append([bits[2:6], bits[6:10]])
            except ValueError:
                pass
    return ugrid

def fit_by_ae6_bh6(dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, xparams, cparams, XP, CP, inplist):
    # test_set_nm(dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, xparams, cparams, XP, CP)

    print("ACTUAL K1 : {:}, B1X : {:}".format(xparams['K1'], xparams['B1X']))

    if Settings.Fint == "TWOBETA" or Settings.Fint == "ORIGINAL":
        candidates = fit_original_IE(dens, non_spherical_dens, ha_dens,  ae6_dens, bh6_dens, xparams, cparams, inplist)
        return candidates

    print("Generating Linear energy integrals...")
    ae6_lin_exl, ae6_lin_ecl = make_linear_energy_components(ae6_dens, xparams, cparams, XP, CP, non_colin=True)
    if Settings.DO_BH6:
        bh6_lin_exl, bh6_lin_ecl = make_linear_energy_components(bh6_dens, xparams, cparams, XP, CP, non_colin=True)

    # test_linear_composition(ae6_dens, xparams, cparams, XP, CP, non_colin=True)
    # raise SystemExit

    # xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'] = (-2.0, 52.0, 58, -60)
    # cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'] = (-10.2, 38.0, -60.0, 34.0)

    # ae6_lin_exl, ae6_lin_ecl = make_linear_energy_components(ae6_dens, xparams, cparams, XP, CP, non_colin=True)
    # bh6_lin_exl, bh6_lin_ecl = make_linear_energy_components(bh6_dens, xparams, cparams, XP, CP, non_colin=True)

    # ae6_err = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
    # bh6_err = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP)
    # print "AE6 ME", np.mean(ae6_err), "MAE", np.mean(np.abs(ae6_err))
    # print "BH6 ME", np.mean(bh6_err), "MAE", np.mean(np.abs(bh6_err))

    # err = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, SCAN2.DEFAULT_X_PARAMS, SCAN2.DEFAULT_C_PARAMS, XP, CP)
    # err = assess_ae6_for_scan(ae6_dens, SCAN.DEFAULT_X_PARAMS, SCAN.DEFAULT_C_PARAMS)
    # print "AE6 ME", np.mean(err), "MAE", np.mean(np.abs(err))
    # err = assess_bh6_for_scan(bh6_dens, SCAN.DEFAULT_X_PARAMS, SCAN.DEFAULT_C_PARAMS)
    # print "BH6 ME", np.mean(err), "MAE", np.mean(np.abs(err))
    # # raise SystemExit
    # print "==============="
    # err = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, SCAN2.DEFAULT_X_PARAMS, SCAN2.DEFAULT_C_PARAMS, XP, CP)

    # test_linear_composition(ae6_dens, xparams, cparams, XP, CP, non_colin=True)
    # raise SystemExit

    candidates = []

    print("Testing Parameters...")

    ae6_min = 1e20
    bh6_min = 1e20

    n_tried = 0

    best_ae6_mae = 1e20
    best_bh6_mae = 1e20

    ngood = 0
    directory = "/Users/james/Desktop/"
    nstr = "XisC_" if Settings.FX_is_FC else "XnoC_"
    nstr += Settings.Fint[:4]+"_"
    nstr += Settings.H1X[:4]+"_"
    nstr += Settings.GX[:4]+"_"
    nstr += Settings.CORRELATION[:4]+"_"
    nstr = directory+nstr+"k{:}_b1x{:}.csv".format(xparams['K1'], xparams['B1X'])
    with open(nstr, 'w') as out:
        tstr = ",Interpolation,fx = fc,h1x,gx,correlation,"
        tstr += "CAX,CBX,CCX,CDX,CAC,CBC,CCC,CDC,"
        tstr += "k1,b1x,b2x,b3x,b4x,a1x,b1c,b2c,b3c,"
        tstr += "AE6 ME,AE6 MAE,Atoms X,Atoms C,Atoms XC,"
        tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
        tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
        out.write(tstr+"\n")

        for p in tqdm(inplist, total=len(inplist), ascii=True, disable=Settings.NO_PROG):
            for i, n in enumerate(CP):
                cparams[n] = p[n]
            for i, n in enumerate(XP):
                xparams[n] = p[n]

            ae6_err, ae6_etot = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
            ae6_me = np.mean(ae6_err)
            ae6_mae = np.mean(np.abs(ae6_err))

            if Settings.DO_BH6:
                bh6_err, bh6_etot = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP)
                bh6_me = np.mean(bh6_err)
                bh6_mae = np.mean(np.abs(bh6_err))

            if abs(ae6_mae) < abs(best_ae6_mae):
                change = True
                best_ae6_mae = ae6_mae
                best_ae6 = {'ae6_me':ae6_me,'ae6_mae': ae6_mae, 'ae6_err':ae6_err, 'ae6_etot':ae6_etot}
                if Settings.DO_BH6:
                    best_ae6['bh6_me'] = bh6_me
                    best_ae6['bh6_mae'] = bh6_mae
                for n in XP:
                    best_ae6[n] = xparams[n]
                for n in CP:
                    best_ae6[n] = cparams[n]

            if Settings.DO_BH6 and abs(bh6_mae) < abs(best_bh6_mae):
                best_bh6_mae = bh6_mae
                best_bh6 = {'ae6_me':ae6_me,'ae6_mae': ae6_mae,'bh6_me':bh6_me,'bh6_mae': bh6_mae}
                for n in XP:
                    best_bh6[n] = xparams[n]
                for n in CP:
                    best_bh6[n] = cparams[n]

            if abs(ae6_mae) < abs(Settings.TOL_AE6):
                ost = ",{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H1X, Settings.GX, Settings.CORRELATION)
                ost += "{:},{:},{:},{:},".format(xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'])
                ost += "{:},{:},{:},{:},".format(cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC'])
                ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
                ost += "{:.3f},{:.3f},{:.4f}%,{:.4f}%,{:.4f}%,".format(ae6_me, ae6_mae, p['rms_x'], p['rms_c'], p['rms_xc'])
                ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
                ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
                of.write(ost)
        #     tmp = {'ae6_me':ae6_me,'ae6_mae': ae6_mae,'bh6_me':bh6_me,'bh6_mae': bh6_mae}
        #     for n in XP:
        #         tmp[n] = xparams[n]
        #     for n in CP:
        #         tmp[n] = cparams[n]
        #     for m in Benchmarks.BH6_LIST:
        #         tmp[m] = bh6_etot[m]
        #     for m in Benchmarks.AE6_LIST:
        #         tmp[m] = ae6_etot[m]
        #     tmp['K1'] = xparams['K1']
        #     tmp['B1X'] = xparams['B1X']
        #     # tmp['err_x'] = p[0]['err_x']
        #     # tmp['err_c'] = p[1]['err_c']
        #     candidates.append(tmp)
        #     ngood += 1
    # print "{:} parameters tested, {:}({:.3f}%) were too ugly.".format(n_tried, n_ugly, 100*n_ugly/n_tried)
    print("Found {:<5} candidates.".format(len(candidates)))
    print("Best AE6 MAE :          CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(best_ae6['CAX'], best_ae6['CBX'], best_ae6['CCX'], best_ae6['CDX']))
    print("                        CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(best_ae6['CAC'], best_ae6['CBC'], best_ae6['CCC'], best_ae6['CDC']))
    print("                   AE6: ME: {:.3f}, MAE: {:.3f}".format(best_ae6['ae6_me'], best_ae6['ae6_mae']))
    if Settings.DO_BH6:
        print("                   BH6: ME: {:.3f}, MAE: {:.3f}".format(best_ae6['bh6_me'], best_ae6['bh6_mae']))
    for a in XP:
        xparams[a] = best_ae6[a]
    for a in CP:
        cparams[a] = best_ae6[a]
    print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams, exact=False)
    exl = get_Ex_for_atoms(dens, xparams, do_SCAN2=True)
    ecl = get_Ec_for_atoms(dens, cparams, do_SCAN2=True)
    bm_x = Benchmarks.BM_X
    bm_c = Benchmarks.BM_C
    bm_xc = Benchmarks.BM_XC
    num_x = [exl[i] - bm_x[i] for i in range(len(exl))]
    err_x = [100*(exl[i] - bm_x[i])/np.abs(bm_x[i]) for i in range(len(exl))]
    rms_x = np.sqrt(np.mean(np.array(err_x)**2))

    num_c = [ecl[i] - bm_c[i] for i in range(len(ecl))]
    err_c = [100*(ecl[i] - bm_c[i])/np.abs(bm_c[i]) for i in range(len(ecl))]
    rms_c = np.sqrt(np.mean(np.array(err_c)**2))

    num_xc = [ecl[i] + exl[i] - bm_xc[i] for i in range(len(ecl))]
    err_xc = [100*(ecl[i] + exl[i] - bm_xc[i])/np.abs(bm_xc[i]) for i in range(len(ecl))]
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

    print("For table")
    print("==================")
    tstr = ",Interpolation,fx = fc,h1x,gx,correlation,"
    ost = ",{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H1X, Settings.GX, Settings.CORRELATION)
    tstr += "Ax,Bx,Cx,Ac,Bc,Cc,"
    ost += "{:},{:},{:},{:},{:},{:},".format(xparams['CAX'],xparams['CBX'],xparams['CCX'],cparams['CAC'],cparams['CBC'],cparams['CCC'])
    tstr += "k1,b1x,a1x,b1c,b2c,b3c,"
    ost += "{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    tstr += "AE6 ME,AE6 MAE,Atoms X,Atoms C,Atoms XC,"
    ost += "{:.3f},{:.3f},{:.4f}%,{:.4f}%,{:.4f}%,".format(best_ae6['ae6_me'], best_ae6['ae6_mae'], rms_x, rms_c, rms_xc)
    tstr += ("{:},"*len(Benchmarks.BM_LIST)).format(*Benchmarks.BM_LIST)
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
    tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
    ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[best_ae6['ae6_etot'][x] for x in Benchmarks.AE6_LIST])
    tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
    ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*best_ae6['ae6_err'])
    print(tstr)
    print(ost)
    print("==================")

    if Settings.DO_BH6:
        print("Best BH6 MAE :          CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(best_bh6['CAX'], best_bh6['CBX'], best_bh6['CCX'], best_bh6['CDX']))
        print("                        CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(best_bh6['CAC'], best_bh6['CBC'], best_bh6['CCC'], best_bh6['CDC']))
        print("                   AE6: ME: {:.3f}, MAE: {:.3f}".format(best_bh6['ae6_me'], best_bh6['ae6_mae']))
        print("                   BH6: ME: {:.3f}, MAE: {:.3f}".format(best_bh6['bh6_me'], best_bh6['bh6_mae']))
        for a in XP:
            xparams[a] = best_bh6[a]
        for a in CP:
            cparams[a] = best_bh6[a]
        print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams, exact=False)

    return candidates
    # raise SystemExit

def assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP, exl=None, ecl=None):
    if exl is None:
        exl = get_Ex_from_lin(bh6_lin_exl, xparams, XP)
    if ecl is None:
        ecl = get_Ec_from_lin(bh6_lin_ecl, cparams, CP)

    exc = exl + ecl

    etot = {}

    for i, m in enumerate(Benchmarks.BH6_LIST):
        etot[m] = exc[i] + Benchmarks.SET_NO_XC[m]
        # print m, etot[m]

    mols = np.array([
        etot['TS_H_OH_O_H2'] - etot['OH'] - etot['H'],
        etot['TS_H_OH_O_H2'] - etot['O'] - etot['H2'],
        etot['TS_OH_CH4_CH3_H2O'] - etot['OH'] - etot['CH4'],
        etot['TS_OH_CH4_CH3_H2O'] - etot['CH3'] - etot['H2O'],
        etot['TS_H_H2S_H2_SH'] - etot['H'] - etot['H2S'],
        etot['TS_H_H2S_H2_SH'] - etot['H2'] - etot['HS']
    ])

    err = []
    for i, m in enumerate(Benchmarks.BH6_TESTS):
        err.append(mols[i]*Benchmarks.EH_TO_KCAL - Benchmarks.BH6_BM[m])
        # print m, err[-1]
    return err, etot

def assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP, exl=None, ecl=None):
    assert Settings.DO_AE6, "We didn't load the AE6 densities. We can't evaluate them"

    if exl is None:
        exl = get_Ex_from_lin(ae6_lin_exl, xparams, XP)
    if ecl is None:
        ecl = get_Ec_from_lin(ae6_lin_ecl, cparams, CP)

    exc = exl + ecl

    etot = {}

    for i, m in enumerate(Benchmarks.AE6_LIST):
        etot[m] = exc[i] + Benchmarks.SET_NO_XC[m]
        # print m, etot[m]

    mols = np.array([
        etot['Si'] + 4*etot['H'] - etot['SiH4'],
        etot['Si'] + etot['O'] - etot['SiO'],
        2*etot['S'] - etot['S2'],
        3*etot['C'] + 4*etot['H'] - etot['C3H4'],
        2*etot['C'] + 2*etot['H'] + 2*etot['O'] - etot['C2H2O2'],
        4*etot['C'] + 8*etot['H'] - etot['C4H8']
    ])

    err = []
    for i, m in enumerate(Benchmarks.AE6_TESTS):
        err.append(mols[i]*Benchmarks.EH_TO_KCAL - Benchmarks.AE6_BM[m])
        # print m, err[-1]
    return err, etot

def fit_original_IE(dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, xparams, cparams, inplist):
    candidates = []

    print("Testing Parameters...")

    ae6_min = 1e20
    bh6_min = 1e20

    n_tried = 0

    best_ae6_mae = 1e20
    best_bh6_mae = 1e20

    ngood = 0
    for p in tqdm(inplist, total=len(inplist), ascii=True, disable=Settings.NO_PROG):
        for i, n in enumerate(["C1X", "C2X", "DX"]):
            cparams[n] = p[n]
        for i, n in enumerate(["C1C", "C2C", "DC"]):
            xparams[n] = p[n]

        ae6_exl = get_Ex_for_atoms(ae6_dens, xparams, True)
        ae6_ecl = get_Ec_non_colinear(ae6_dens, cparams, True)

        ae6_err, ae6_etot = assess_ae6_for_scan2(None, None, xparams, cparams, None, None, exl=ae6_exl, ecl=ae6_ecl)
        ae6_me = np.mean(ae6_err)
        ae6_mae = np.mean(np.abs(ae6_err))

        if Settings.DO_BH6:
            bh6_exl = get_Ex_for_atoms(bh6_dens, xparams, True)
            bh6_ecl = get_Ec_non_colinear(bh6_dens, cparams, True)
            bh6_err, bh6_etot = assess_bh6_for_scan2(None, None, xparams, cparams, None, None, exl=bh6_exl, ecl=bh6_ecl)
            bh6_me = np.mean(bh6_err)
            bh6_mae = np.mean(np.abs(bh6_err))

        if abs(ae6_mae) < abs(best_ae6_mae):
            change = True
            best_ae6_mae = ae6_mae
            best_ae6 = {'ae6_me':ae6_me,'ae6_mae': ae6_mae, 'ae6_err':ae6_err, 'ae6_etot':ae6_etot}
            if Settings.DO_BH6:
                best_ae6['bh6_me'] = bh6_me
                best_ae6['bh6_mae'] = bh6_mae
            for n in ["C1X", "C2X", "DX"]:
                best_ae6[n] = xparams[n]
            for n in ["C1C", "C2C", "DC"]:
                best_ae6[n] = cparams[n]

        if Settings.DO_BH6 and abs(bh6_mae) < abs(best_bh6_mae):
            best_bh6_mae = bh6_mae
            best_bh6 = {'ae6_me':ae6_me,'ae6_mae': ae6_mae,'bh6_me':bh6_me,'bh6_mae': bh6_mae}
            for n in ["C1X", "C2X", "DX"]:
                best_bh6[n] = xparams[n]
            for n in ["C1C", "C2C", "DC"]:
                best_bh6[n] = cparams[n]

        if (not Settings.DO_BH6 and abs(ae6_mae) < Settings.TOL_AE6) or (Settings.DO_BH6 and (abs(ae6_mae) < Settings.TOL_AE6 and abs(bh6_mae) < Settings.TOL_BH6)):
            tmp = {'ae6_me':ae6_me,'ae6_mae': ae6_mae, 'ae6_err':ae6_err, 'ae6_etot':ae6_etot}
            for n in ["C1X", "C2X", "DX"]:
                tmp[n] = xparams[n]
            for n in ["C1C", "C2C", "DC"]:
                tmp[n] = cparams[n]
            candidates.append(tmp)

    print("Found {:<5} candidates.".format(len(candidates)))
    print("Best AE6 MAE :          C1X {:}, C2X {:}, DX {:}".format(best_ae6['C1X'], best_ae6['C2X'], best_ae6['DX']))
    print("                        C1C {:}, C2C {:}, DC {:}".format(best_ae6['C1C'], best_ae6['C2C'], best_ae6['DC']))
    print("                   AE6: ME: {:.3f}, MAE: {:.3f}".format(best_ae6['ae6_me'], best_ae6['ae6_mae']))

    for a in ["C1X", "C2X", "DX"]:
        xparams[a] = best_ae6[a]
    for a in ["C1C", "C2C", "DC"]:
        cparams[a] = best_ae6[a]
    print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams, exact=False)
    exl = get_Ex_for_atoms(dens, xparams, do_SCAN2=True)
    ecl = get_Ec_for_atoms(dens, cparams, do_SCAN2=True)
    bm_x = Benchmarks.BM_X
    bm_c = Benchmarks.BM_C
    bm_xc = Benchmarks.BM_XC
    num_x = [exl[i] - bm_x[i] for i in range(len(exl))]
    err_x = [100*(exl[i] - bm_x[i])/np.abs(bm_x[i]) for i in range(len(exl))]
    rms_x = np.sqrt(np.mean(np.array(err_x)**2))

    num_c = [ecl[i] - bm_c[i] for i in range(len(ecl))]
    err_c = [100*(ecl[i] - bm_c[i])/np.abs(bm_c[i]) for i in range(len(ecl))]
    rms_c = np.sqrt(np.mean(np.array(err_c)**2))

    num_xc = [ecl[i] + exl[i] - bm_xc[i] for i in range(len(ecl))]
    err_xc = [100*(ecl[i] + exl[i] - bm_xc[i])/np.abs(bm_xc[i]) for i in range(len(ecl))]
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

    print("For table")
    print("==================")
    tstr = ",Interpolation,fx = fc,h1x,gx,correlation,"
    ost = ",{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H1X, Settings.GX, Settings.CORRELATION)
    tstr += "C1X,C2X,DX,C1C,C2C,DC,"
    ost += "{:},{:},{:},{:},{:},{:},".format(xparams['C1X'],xparams['C2X'],xparams['DX'],cparams['C1C'],cparams['C2C'],cparams['DC'])
    tstr += "k1,b1x,a1x,b1c,b2c,b3c,"
    ost += "{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    tstr += "AE6 ME,AE6 MAE,Atoms X,Atoms C,Atoms XC,"
    ost += "{:.3f},{:.3f},{:.4f}%,{:.4f}%,{:.4f}%,".format(best_ae6['ae6_me'], best_ae6['ae6_mae'], rms_x, rms_c, rms_xc)
    tstr += ("{:},"*len(Benchmarks.BM_LIST)).format(*Benchmarks.BM_LIST)
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
    tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
    ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[best_ae6['ae6_etot'][x] for x in Benchmarks.AE6_LIST])
    tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
    ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*best_ae6['ae6_err'])
    print(tstr)
    print(ost)
    print("==================")

def test_set_nm(dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, xparams, cparams, XP, CP):
    beta = np.arange(0, 1, 0.002)
    beta2 = beta**2
    beta3 = beta**3

    beta_05 = np.arange(0, 0.5, 0.002)
    beta2_05 = beta_05**2
    beta3_05 = beta_05**3

    beta_10 = np.arange(0.5, 1.0, 0.002)
    beta2_10 = beta_10**2
    beta3_10 = beta_10**3

    beta_mess = (beta, beta2, beta3, beta_05, beta2_05, beta3_05, beta_10, beta2_10, beta3_10)

    print("\nBeginning outer minimization...\n")
    res = minimize(k_b_obj,
        (0.01, 2.0),
        args=(dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, xparams, cparams, XP, CP, beta_mess),
        method='Nelder-Mead',
        options={'xatol':0.001, 'fatol':0.001}
        )
    print("--------------------------------------------------------------------------------------------------")
    xparams['K1'], xparams['B1X'] = res.x

    print("{:}, {:}, | {:}, {:}, {:}, {:}, {:}, {:}, {:}, {:}, {:.3f}".format(xparams['K1'], xparams['B1X'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'], res.fun))
    print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams)

    raise SystemExit

def k_b_obj(p_kb, dens, non_spherical_dens, ha_dens, ae6_dens, bh6_dens, xparams, cparams, XP, CP, beta_mess):
    xparams['K1'] = abs(p_kb[0])
    xparams['B1X'] = abs(p_kb[1])

    ae6_lin_exl, ae6_lin_ecl = make_linear_energy_components(ae6_dens, xparams, cparams, XP, CP, non_colin=True)
    bh6_lin_exl, bh6_lin_ecl = make_linear_energy_components(bh6_dens, xparams, cparams, XP, CP, non_colin=True)

    xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'] = (0.75, 52.5, -60, 17.5)
    cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'] = (0.75, 52.5, -60, 17.5)

    res = minimize(ie_obj,
            (xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']),
            args=(ae6_lin_exl, ae6_lin_ecl, bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP, beta_mess),
            method='Nelder-Mead',
            options={'xatol':0.001, 'fatol':0.001, 'adaptive':True})
    print("{:}, {:}, | {:}, {:}, {:}, {:}, {:}, {:}, {:}, {:}, {:.3f}".format(xparams['K1'], xparams['B1X'], res.x[0], res.x[1], res.x[2], res.x[3], res.x[4], res.x[5], res.x[6], res.x[7], res.fun))
    xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'] = res.x
    return float("{:.3f}".format(res.fun))


def ie_obj(p_ie, ae6_lin_exl, ae6_lin_ecl, bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP, beta_mess):
    xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'] = p_ie

    # beta, beta2, beta3, beta_05, beta2_05, beta3_05, beta_10, beta2_10, beta3_10 = beta_mess
    # if not beauty_filter(, beta, beta2, beta3, beta_05, beta2_05, beta3_05, beta_10, beta2_10, beta3_10):
    #     return 1e10

    ae6_err = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
    bh6_err = assess_bh6_for_scan2(bh6_lin_exl, bh6_lin_ecl, xparams, cparams, XP, CP)

    ae6_mae = np.mean(np.abs(ae6_err))
    bh6_mae = np.mean(np.abs(bh6_err))

    print("\t - {:}, {:}, {:}, {:}, {:}, {:}, {:}, {:}, {:.3f}, {:.3f}, {:.3f}".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'], ae6_mae, bh6_mae, ae6_mae + bh6_mae))

    return ae6_mae + bh6_mae

def assess_ae6_for_scan(ae6_dens, xparams, cparams):
    exl = get_Ex_for_atoms(ae6_dens, xparams)
    ecl = np.hstack([get_Ec_for_atoms(ae6_dens[:len(Benchmarks.REST_AE6)], cparams), get_Ec_non_colinear(ae6_dens[len(Benchmarks.REST_AE6):], cparams)])

    exc = exl + ecl
    etot = {}
    for i, m in enumerate(Benchmarks.AE6_LIST):
        etot[m] = exc[i] + Benchmarks.SET_NO_XC[m]

    mols = np.array([
        etot['Si'] + 4*etot['H'] - etot['SiH4'],
        etot['Si'] + etot['O'] - etot['SiO'],
        2*etot['S'] - etot['S2'],
        3*etot['C'] + 4*etot['H'] - etot['C3H4'],
        2*etot['C'] + 2*etot['H'] + 2*etot['O'] - etot['C2H2O2'],
        4*etot['C'] + 8*etot['H'] - etot['C4H8']
    ])

    err = []
    for i, m in enumerate(Benchmarks.AE6_TESTS):
        err.append(mols[i]*Benchmarks.EH_TO_KCAL - Benchmarks.AE6_BM[m])

    return err, etot

def assess_bh6_for_scan(bh6_dens, xparams, cparams):
    exl = get_Ex_for_atoms(bh6_dens, xparams)
    ecl = np.hstack([get_Ec_for_atoms(bh6_dens[:len(Benchmarks.REST_BH6)], cparams), get_Ec_non_colinear(bh6_dens[len(Benchmarks.REST_BH6):], cparams)])

    exc = exl + ecl
    etot = {}
    for i, m in enumerate(Benchmarks.BH6_LIST):
        etot[m] = exc[i] + Benchmarks.SET_NO_XC[m]

    mols = np.array([
        etot['TS_H_OH_O_H2'] - etot['OH'] - etot['H'],
        etot['TS_H_OH_O_H2'] - etot['O'] - etot['H2'],
        etot['TS_OH_CH4_CH3_H2O'] - etot['OH'] - etot['CH4'],
        etot['TS_OH_CH4_CH3_H2O'] - etot['CH3'] - etot['H2O'],
        etot['TS_H_H2S_H2_SH'] - etot['H'] - etot['H2S'],
        etot['TS_H_H2S_H2_SH'] - etot['H2'] - etot['HS']
    ])

    err = []
    for i, m in enumerate(Benchmarks.BH6_TESTS):
        err.append(mols[i]*Benchmarks.EH_TO_KCAL - Benchmarks.BH6_BM[m])


    return err, etot
