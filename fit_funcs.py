import numpy as np
import scipy.optimize
from tqdm import tqdm
import matplotlib.pyplot as plt
from itertools import product

def ori(a):
    c1x = 0.667
    c2x = 0.8
    dx = 1.24
    out = np.zeros(a.shape)
    oma = 1-a
    out[a < 1.0] = np.exp(-c1x*a[a < 1.0]/oma[a < 1.0])
    out[a > 1.0] = -dx*np.exp(c2x/oma[a > 1.0])
    return out

def smooth_alpha(alpha, cax, cbx, dx):
    num = (1 - alpha)**3*(1 + cax*alpha + cbx*alpha**2)
    den = (1 + alpha**3)*(1 + (cbx/dx)*alpha**2)
    f_beta = num/den
    return f_beta

def obj(p, inp, h, mon):
    cbx = abs(p[1])
    cax = max(p[0], -2*np.sqrt(cbx))
    dx = abs(p[2])
    old = ori(inp)
    new = smooth_alpha(inp, cax, cbx, dx)
    last = 1.0
    for r in mon:
        here = smooth_alpha(r, cax, cbx, dx)
        if here > last:
            return None
        else:
            last = here

    return np.sum(np.abs(old - new)*h)

def brute():
    inp, h = np.linspace(0.001,1000,num=20000,retstep=True)
    mon = np.linspace(0,2,num=200)
    caxs = np.arange(2.0, 4.0, 0.01)
    cbxs = np.arange(1.0, 2.0, 0.01)
    dx = 1.24

    poss = list(product(caxs, cbxs, [dx]))
    out = []
    for p in tqdm(poss):
        res = obj(p, inp, h, mon)
        if res is not None:
            out.append({'x':p,'fun':res})
    print(out)   
    out = sorted(out, key=lambda x: x['fun'])
    return out[0]

res = brute()
# res = scipy.optimize.minimize(obj, (0.667, 0.8, 1.24), method="Nelder-Mead")
cbx = abs(res['x'][1])
cax = max(res['x'][0], -2*np.sqrt(cbx))
dx = abs(res['x'][2])
print("Diff minimized at A: {0}, B: {1}, Dx: {2} with {3}".format(cax, cbx, dx, res['fun']))
inp = np.linspace(0,5,num=300)

print("Long range (alpha = 10000): old: {:}, new: {:}".format(ori(np.array(10000)), smooth_alpha(np.array(10000), cax, cbx, dx)))

plt.plot(inp, ori(inp), label="$f_{\\mathrm{old}}$")
plt.plot(inp, smooth_alpha(inp, cax, cbx, dx), label="$f_{\\mathrm{new}}$")
plt.legend()
plt.text(3, 0.0, "$A: {:.2f}$\n$B: {:.2f}$\n$d_x: {:.2f}$".format(cax, cbx, dx))
plt.title("Minimizing $\\int_0^{1000} |f_{\\mathrm{old}}(\\alpha) - f_{\\mathrm{new}}(\\alpha)| d\\alpha $")
plt.xlabel("$\\alpha$")
plt.ylabel("$f(\\alpha)$")
plt.show()