import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

dfa_d = {'SCAN': 'SCAN','r2SCAN': 'r$^2$SCAN', 'ASCAN': 'ASCAN'}
cl = ['darkblue','darkorange','tab:green']
lsl = ['-','--','-.']

fig, ax = plt.subplots(2,1,figsize=(4,6))

for idfa,dfa in enumerate(dfa_d):

    wfl = './{:}_BH76_gridtest/{:}_BH76_gridtest.csv'.format(dfa,dfa)
    gs, gpts, bh76_mad, bh76rc_mad = np.transpose(np.genfromtxt(wfl,delimiter=',',\
        skip_header=1))

    ax[0].scatter(gpts/gpts[0],bh76_mad,color=cl[idfa],linestyle=lsl[idfa])
    ax[0].plot(gpts/gpts[0],bh76_mad,color=cl[idfa],linestyle=lsl[idfa])

    ax[1].scatter(gpts/gpts[0],bh76rc_mad,color=cl[idfa],linestyle=lsl[idfa])
    ax[1].plot(gpts/gpts[0],bh76rc_mad,color=cl[idfa],linestyle=lsl[idfa],label=dfa_d[dfa])

for i in range(2):
    ax[i].set_xscale('log')
#ax[1].set_xticks(gpts/gpts[0])

ax[1].legend(fontsize=10)
ax[1].set_xlabel('Relative grid density',fontsize=10)
ax[0].set_ylabel('BH76 MAD (kcal/mol)',fontsize=10)
ax[1].set_ylabel('BH76RC MAD (kcal/mol)',fontsize=10)

ax[0].yaxis.set_minor_locator(MultipleLocator(0.05))
ax[0].yaxis.set_major_locator(MultipleLocator(0.2))

ax[1].yaxis.set_minor_locator(MultipleLocator(0.05))
ax[1].yaxis.set_major_locator(MultipleLocator(0.2))

plt.savefig('./BH76_w_RC_mads.pdf',dpi=600,bbox_inches='tight')
#plt.show()
