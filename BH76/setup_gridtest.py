from setup import setup
from os import system

def setup_BH76_gridtest():

    inp_opts = {
        'gridsize': 0, 'symm': False, 'tol': 1.e-7, 'max_cycle': 500,
        'verbose': 4, 'basis': 'def2-qzvp'
    }

    for dfa in ['SCAN','r2SCAN','ASCAN']:
        wdir = './{:}_BH76_gridtest/'.format(dfa)
        system('rm -rf '+wdir)
        system('mkdir '+wdir)

        tstr = '#!/bin/bash\n\n'
        tstr += 'for u in ./*/ ; do\n'
        tstr += '  cd $u\n  qsub runjob.sh \n  cd ..\n'
        tstr += 'done'
        with open(wdir+'suball.sh','w+') as tfl:
            tfl.write(tstr)

        for inp_opts['gridsize'] in range(1,10,1):

            cdir = wdir + dfa + '_gsize={:}'.format(inp_opts['gridsize'])
            setup(dfa,startclean=True,xclib='XCFun',wfdir = '',dfa_dir=cdir,\
                inp_opts=inp_opts)

if __name__ == "__main__":

    setup_BH76_gridtest()
