import numpy as np
from math import pi

def get_tm_x(d0, d1, g0, g1, t0, t1, only_0=False, only_Fx=False):
    rho = 2*d0
    grd = 2*g0
    tau = 2*t0

    AX = -0.7385587663820224058842300326808360
    exunif_0 = AX*rho**(1.0/3.0)
    exlda_0 = exunif_0*rho

    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0)
    tauw = (grd**2)/(8*rho)
    alpha = (tau - tauw)/tau_unif
    p = grd**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))

    Fx_0 = get_Fx_rr(p, alpha)
    Ex_0 = exlda_0*Fx_0

    if only_0:
        if only_Fx:
            return Fx_0, np.zeros_like(Fx_0)
        else:
            return Ex_0, np.zeros_like(Ex_0)

    exunif_1 = AX*(2*d1)**(1.0/3.0)
    exlda_1 = exunif_1*(2*d1)

    rho = 2*d0
    grd = 2*g0
    tau = 2*t0

    exunif_0 = AX*rho**(1.0/3.0)
    exlda_0 = exunif_0*rho

    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0)
    tauw = (grd**2)/(8*rho)
    alpha = (tau - tauw)/tau_unif
    p = grd**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))

    Fx_1 = get_Fx_rr(p, alpha)
    Ex_1 = exlda_1*Fx_1

    if only_Fx:
        return Fx_0, Fx_1
    else:
        return Ex_0, Ex_1

def get_tm_al_x(d0, d1, g0, g1, t0, t1, A, B, C, only_0=False, only_Fx=False):
    rho = 2*d0
    grd = 2*g0
    tau = 2*t0

    AX = -0.7385587663820224058842300326808360
    exunif_0 = AX*rho**(1.0/3.0)
    exlda_0 = exunif_0*rho

    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0)
    tauw = (grd**2)/(8*rho)
    alpha = (tau - tauw)/tau_unif
    p = grd**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))

    Fx_0 = get_Fx_al_rr(p, alpha, A, B, C)
    Ex_0 = exlda_0*Fx_0

    if only_0:
        if only_Fx:
            return Fx_0, np.zeros_like(Fx_0)
        else:
            return Ex_0, np.zeros_like(Ex_0)

    exunif_1 = AX*(2*d1)**(1.0/3.0)
    exlda_1 = exunif_1*(2*d1)

    rho = 2*d0
    grd = 2*g0
    tau = 2*t0

    exunif_0 = AX*rho**(1.0/3.0)
    exlda_0 = exunif_0*rho

    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0)
    tauw = (grd**2)/(8*rho)
    alpha = (tau - tauw)/tau_unif
    p = grd**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))

    Fx_1 = get_Fx_al_rr(p, alpha, A, B, C)
    Ex_1 = exlda_1*Fx_1

    if only_Fx:
        return Fx_0, Fx_1
    else:
        return Ex_0, Ex_1

def get_Fdme(d, g, t, tau_unif, p):
    LAMBDA = 0.6866
    BETA = 79.873
    y = (2*LAMBDA - 1.0)**2*p
    f = (1 + 10*(70*y/27.0) + BETA*y**2)**(1.0/10.0)
    k = 3*(LAMBDA**2 - LAMBDA + 1.0/2.0)
    R = 1 + 595*(2*LAMBDA - 1)**2*p/54.0
    tauw = (g**2)/(8*d)
    # R -= (t - k*(t - tau_unif - tauw/9.0))/tau_unif
    R -= (t - k*(t - tau_unif - g**2/(72.0*d)))/tau_unif
    return 1/f**2 + 7*R/(9.0*f**4)

def get_Fdme_rr(p, alpha):
    LAMBDA = 0.6866
    BETA = 79.873
    y = (2*LAMBDA - 1.0)**2*p
    f = (1 + 10*(70*y/27.0) + BETA*y**2)**(1.0/10.0)
    k = 3*(LAMBDA**2 - LAMBDA + 1.0/2.0)
    R = 1 + 595*(2*LAMBDA - 1)**2*p/54.0 - (k*(1-alpha) + alpha + 5*(1 - 8*k/9)*p/3.0)
    return 1/f**2 + 7*R/(9.0*f**4)

def get_Fx(d, g, t, tauw, tau_unif, p, alpha):
    
    w = ((tauw/t)**2 + 3*(tauw/t)**3)/(1.0 + (tauw/t)**3)**2

    return w*get_Fdme(d, g, t, tau_unif, p) + (1.0 - w)*get_Fsc(t, p, tauw, alpha)

def get_Fx_rr(p, alpha):
    z = 1/(1.0 + 3*alpha/(5*p))
    w = (z**2 + 3*z**3)/(1.0 + z**3)**2
    return w*get_Fdme_rr(p, alpha) + (1.0 - w)*get_Fsc_rr(p, alpha)

def get_Fx_al_rr(p, alpha, A, B, C):
    beta = alpha/(alpha + 5*p/3.0 + 1.0)
    # w = (1 - 2*beta)**3*(1 + A*beta + B*beta**2 + C*beta**3)
    w = (1-2*beta)**3*np.exp(-C*beta**2)
    return w*get_Fdme_rr(p, alpha) + (1.0 - w)*get_Fsc_rr(p, alpha)

def get_Fsc(t, p, tauw, alpha):
    qt = 9.0/20.0*(alpha - 1.0) + 2*p/3.0
    fsc = (1.0 + 10*((10.0/81.0 + 50*p/729.0)*p + 146*qt**2/2025.0 \
            - (73*qt/405.0)*(3*tauw/(5*t))*(1-tauw/t)))**(1.0/10.0)
    return fsc

def get_Fsc_rr(p, alpha):
    z = 1/(1 + 3*alpha/(5*p))
    qt = 9.0/20.0*(alpha - 1.0) + 2*p/3.0
    fsc = (1.0 + 10*((10.0/81.0 + 50*p/729.0)*p + 146*qt**2/2025.0 \
            - (73*qt/405.0)*(3/5.0*z)*(1-z)))**(1.0/10.0)
    return fsc

