import numpy as np
from itertools import product
import Settings
import SCAN2
import time
from multiprocessing import Pool
import matplotlib.pyplot as plt

def get_pretty_coefficients(xparams):
#    try:
#        xset = []
#        with open("xparams.csv", "r") as inp:
#            lines = inp.readlines()
#            for l in lines[1:]:
#                xset.append(map(float,l.strip().split(',')))
#        cset = []
#        with open("cparams.csv", "r") as inp:
#            lines = inp.readlines()
#            for l in lines[1:]:
#                cset.append(map(float,l.strip().split(',')))
#        return (xset, cset)
#    except IOError:
#        pass

    a_l = np.arange(Settings.A_MIN, Settings.A_MAX, Settings.A_STEP)
    b_l = np.arange(Settings.B_MIN, Settings.B_MAX, Settings.B_STEP)

    if Settings.F1_MIN is None:
        F1_MIN = -(2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)
    else:
        F1_MIN = Settings.F1_MIN

    if Settings.Fint == "JPLAP" or Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
        f1_l = np.arange(Settings.C_MIN, Settings.C_MAX, Settings.C_STEP)
    else:
        f1_l = np.arange(F1_MIN, Settings.F1_MAX, Settings.F1_STEP)
    if Settings.FLT_HLF :
        d_l = np.ones(1) # dummy array so that len(d_l) > 0
    else:
        d_l = np.arange(Settings.D_MIN, Settings.D_MAX, Settings.D_STEP)

    tot = a_l.shape[0]*b_l.shape[0]*f1_l.shape[0]*d_l.shape[0]

    print("Filtering exchange for beauty... {:} expect ~{:}s".format(tot, tot/20000))
    # params = beauty_filter(ugrid, tot)
    stime = time.time()
    p = Pool(4)
    xset = [_f for _f in p.map(fast_check_x, product(a_l, b_l, f1_l, d_l)) if _f]
    p.terminate()
    print("Took {:}s, got {:} sets".format(time.time() - stime, len(xset)))
    if Settings.X_C_DIFF_RANGE:
        a_l_c = np.arange(Settings.C_A_MIN, Settings.C_A_MAX, Settings.C_A_STEP)
        b_l_c = np.arange(Settings.C_B_MIN, Settings.C_B_MAX, Settings.C_B_STEP)
        c_l_c = np.arange(Settings.C_C_MIN, Settings.C_C_MAX, Settings.C_C_STEP)
        if Settings.FLT_HLF :
            d_l_c = np.arange(1,1.1,0.5) # dummy array so that len(d_l_c) > 0
        else:
            d_l_c = np.arange(Settings.C_D_MIN, Settings.C_D_MAX, Settings.C_D_STEP)

        tot = a_l_c.shape[0]*b_l_c.shape[0]*c_l_c.shape[0]*d_l_c.shape[0]

        print("X and C have different ranges. Filtering C... {:} expect ~{:}s".format(tot, tot/20000))
        stime = time.time()
        p = Pool(4)
        cset = [_f for _f in p.map(fast_check_c, product(a_l_c, b_l_c, c_l_c, d_l_c)) if _f]
        # cset = filter(None, map(fast_check_c, product(a_l_c, b_l_c, c_l_c, d_l_c)))
        p.terminate()
        print("Took {:}s, got {:} sets".format(time.time() - stime, len(cset)))
        with open("xparams.csv", 'w') as out:
            out.write("CAX,CBX,CCX,CDX\n")
            for s in xset:
                out.write("{:},{:},{:},{:}\n".format(*s))

        with open("cparams.csv", 'w') as out:
            out.write("CAC,CBC,CCC,CDC\n")
            for s in xset:
                out.write("{:},{:},{:},{:}\n".format(*s))

        return (xset, cset)
    else:
        return params


def fast_check_c(p):
    beta = np.arange(0, 1, 0.002)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta**4

    grad_sum = 1 + p[0]/2.0 + p[1]/4.0 + p[2]/8.0 +p[3]/16.0
    if grad_sum > 1.0 or grad_sum < 0.0:
        return None

    d1 = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, p[0], p[1], p[2], p[3])
    if np.any(d1 > 0.0):
        return None

    d2 = SCAN2.interp_grad2_smooth(beta, beta2, beta3, p[0], p[1], p[2], p[3])
    last = d2[0]
    count = 0
    for i in range(1, d2.shape[0]):
        if last*d2[i] < 0:  # Only possible if prev or current is negative but not both
            count += 1
            if count > 1:
                break
    if count > 1:
        return None
    return p

def fast_check_x(p):
    beta = np.arange(0, 1, 0.002)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta**4

    d1 = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, p[0], p[1], p[2], p[3])
    if np.any(d1 > 0.0):
        return None

    d2 = SCAN2.interp_grad2_smooth(beta, beta2, beta3, p[0], p[1], p[2], p[3])
    last = d2[0]
    count = 0
    for i in range(1, d2.shape[0]):
        if last*d2[i] < 0:  # Only possible if prev or current is negative but not both
            count += 1
            if count > 1:
                break
    if count > 1:
        return None

    return p

def beauty_filter(ugrid, tot):
    beta = np.arange(0, 1, 0.002)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta**4

    params = []
    n_ugly = 0
    n_2dugly = 0
    for p in tqdm(ugrid, total=tot, ascii=True, disable=Settings.NO_PROG):
        if Settings.PARAMETER_SUM_CONSTRAINT is not None and \
                abs(1 + p[0]/2.0 + p[1]/4.0 + p[2]/8.0 +p[3]/16.0) > Settings.PARAMETER_SUM_CONSTRAINT:
            continue

        l = list(p)
        if Settings.FLT_HLF:
            l[3] = -2.0*(8.0 + 4*l[0] + 2*l[1] + l[2])
        if Settings.Fint == "BETA":
            l[2] = -(p[2] + 1 + p[0] + p[1])
            d1 = SCAN2.interp_gradient_poly(beta, beta2, beta3, l[0], l[1], l[2], l[3])
        elif Settings.Fint == "JPLAP":
            pp = np.arange(0, 1, 0.002)
            qq = np.arange(-1, 1, 0.004)
            d1 = SCAN2.interp_gradient_jplap(beta, beta2, beta3, beta4, pp, qq, l[0], l[1], l[2], l[3])
        elif Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
            d1 = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, l[0], l[1], l[2], l[3])
        else:
            raise SystemExit("ERROR: Inappropriate assessment of beauty!")

        if np.any(d1 > 0.0):
            n_ugly += 1
            continue  # Reject the ugly!
        if Settings.Fint == "JPLAP" or Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
            if Settings.Fint == "JPLAP":
                d2 = SCAN2.interp_grad2_jplap(beta, beta2, beta3, pp, qq, l[0], l[1], l[2], l[3])
            elif Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
                d2 = SCAN2.interp_grad2_smooth(beta, beta2, beta3, l[0], l[1], l[2], l[3])
            last = d2[0]
            count = 0
            for i in range(1, d2.shape[0]):
                if last*d2[i] < 0:  # Only possible if prev or current is negative but not both
                    count += 1
                    if count > 1:
                        n_2dugly += 1
                        break
            if count > 1:
                continue
        params.append(l)
    print("{:} were too ugly ({:.3f}%)".format(n_ugly, 100*n_ugly/tot))
    if Settings.Fint == "JPLAP" or Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
        print("{:} failed 2nd derivative ({:.3f}%)".format(n_2dugly, 100*n_2dugly/tot))
    print("{:} remain ({:.3f}%)".format(len(params), 100*len(params)/tot))

    return params

def check_non_positivity(params, xparams):
    out = []
    if Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
        min_fbeta1 = -5.47413
    else:
        min_fbeta1 = -(2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)
    print("Checking non-positvity...")
    n_ugly = 0
    for l in tqdm(params, ascii=True, disable=Settings.NO_PROG):
        if Settings.Fint == "JPLAP":
            beta_1 = SCAN2.eval_jplap(1.0, 0.0, 0.0, l[0],l[1],l[2],l[3],xparams['FREG'])
        elif Settings.Fint == "SMOOTH" or Settings.Fint == "VSMTH" or Settings.Fint == "CRUDE":
            beta_1 = SCAN2.eval_smoothlap(1.0, l[0],l[1],l[2],l[3])

        if beta_1 < min_fbeta1:
            n_ugly += 1
            continue
        out.append(l)
    print("{:} failed non-positivity ({:.3f}%)".format(n_ugly, 100*n_ugly/len(params)))
    print("{:} remain...".format(len(out)))
    return out
