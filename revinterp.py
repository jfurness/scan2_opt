import numpy as np

scan_x_pars = {'C1':  0.667, 'C2': 0.8,'D': 1.24}
scan_c_pars = {'C1':  0.64, 'C2': 1.5,'D': .7}
match_pt = 2.5

pard = {'c': np.asarray([1,0.3599999999999802,-0.07520000000001449,
-2.9102780325567696,3.5297025324388795,-1.7124523194678987,
0.38449334136225327,-0.0332644461570117261])}

def get_scan2_pars(eta):
    K0 = .174
    fa = 100*(4 + 9*eta)*K0/243
    fb = (200 + 6*K0*(1 + 441*eta))/729
    fc = (657*eta - 511)/6075

    f1 = (-fb - (fb**2 - 4*fa*fc)**(0.5) )/(2*fa)
    f2 = 73/(2500*K0) - f1/50
    h1 = 10/81 + 5*(4 + 9*eta)*K0*f1/27
    h2 = -2*(K0*f1*(6 + 75*eta*(8 + 9*eta))/243 + f1*h1*5*(8 + 9*eta)/27 + K0*f2*25/1458*(8 + 9*eta)**2)
    return f1,f2,h1,h2

def scan_ief_lower(x,sps):
    ief = np.exp(-sps['C1']*x/(1-x))
    dief = -ief*sps['C1']/(1-x)**2
    ddief = ief*sps['C1']/(1-x)**4*(sps['C1'] - 2 + 2*x)
    return ief,dief,ddief

def scan_ief_upper(x,sps):
    omx = 1-x
    ief = -sps['D']*np.exp(sps['C2']/omx)
    d1 = ief*sps['C2']/omx**2
    d2 = (d1 + 2*ief/omx )*sps['C2']/omx**2
    d3 = (d2 + 2*d1/omx + 2*ief/omx**2)*sps['C2']/omx**2 \
        + 2*d2/omx
    d4 = (d3 + 2*d2/omx + 4*d1/omx**2 + 4*ief/omx**3)*sps['C2']/omx**2 \
        + 2*(d2 + 2*d1/omx + 2*ief/omx**2)*sps['C2']/omx**3 \
        + 2*d3/omx + 2*d2/omx**2
    return ief,d1,d2,d3,d4

def scan_interp(x,sps,ret_deriv=False):
    ief = np.zeros(x.shape)
    dief = np.zeros(x.shape)
    lmsk = x < 1
    umsk = x > 1
    ief[lmsk],dief[lmsk],_ = scan_ief_lower(x[lmsk],sps)
    ief[umsk],dief[umsk],_,_,_ = scan_ief_upper(x[umsk],sps)
    if ret_deriv:
        return ief,dief
    else:
        return ief

def rscan_interp(x,wief,ret_deriv=False):
    if wief.lower() == 'x':
        cl = np.asarray([1.0,-0.667,-0.4445555,-0.663086601049, 1.451297044490, \
            -0.887998041597, 0.234528941479,-0.023185843322])
        sps = scan_x_pars
    elif wief.lower() == 'c':
        cl = np.asarray([1.0,-0.64,-0.4352,-1.535685604549,3.061560252175,\
            -1.915710236206,0.516884468372,-0.051848879792])
        sps = scan_c_pars

    ief = np.zeros(x.shape)
    dief = np.zeros(x.shape)
    lmask = x <= 2.5
    umask = x > 2.5

    xm = x[lmask]
    for i in range(8):
        ief[lmask] += cl[i]*xm**i
    for i in range(1,8):
        dief[lmask] += i*cl[i]*xm**(i-1)

    ief[umask],dief[umask],_,_,_ = scan_ief_upper(x[umask],sps)
    if ret_deriv:
        return ief,dief
    else:
        return ief

def dpoly(x,n):
    df = np.zeros(n.shape)
    nmask = n>0
    df[nmask] = n[nmask]*x**(n[nmask]-1)
    return df

def poly_interp_mat_coeff(x,nc):

    i = np.arange(1,nc,1)
    f = (1 - x)*x**i

    df1 = dpoly(x,i) - dpoly(x,i+1)
    df2 = i*dpoly(x,i-1) - (i+1)*dpoly(x,i)
    df3 = i* ( (i-1)*dpoly(x,i-2) - (i+1)*dpoly(x,i-1) )
    df4 = i*(i-1)*( (i-2)*dpoly(x,i-3) - (i+1)*dpoly(x,i-2) )

    return f,df1,df2,df3,df4

def get_coeff_mat(wex,xpars={}):

    if wex.lower() == 'x':
        nc = 10
        spars = scan_x_pars

    elif wex.lower() == 'c':
        nc = 8
        spars = scan_c_pars

    ind = 0
    cmat = np.zeros((nc-1,nc-1))
    res = np.zeros(nc-1)

    # match SCAN X first and second derivs for alpha = 0

    _,cmat[ind],cmat[ind+1],_,_ = poly_interp_mat_coeff(0.0,nc)
    _,res[ind],res[ind+1] = scan_ief_lower(0.0,spars)
    res[ind] += 1

    ind += 2

    if wex.lower() == 'x':
        # fix first and second derivs at alpha = 1
        _,cmat[ind],cmat[ind+1],_,_ = poly_interp_mat_coeff(1.0,nc)
        #res[ind],res[ind+1],_,_ = get_scan2_pars(eta)
        res[ind] = xpars['f1'] + 1
        res[ind+1] = xpars['f2']
        ind += 2

    # continuity up to fourth derivs with SCAN X alpha > 1 interpolation function
    # at alpha = 2.5, for consistency with r2 SCAN C
    cmat[ind],cmat[ind+1],cmat[ind+2],cmat[ind+3],cmat[ind+4] = \
        poly_interp_mat_coeff(match_pt,nc)
    res[ind],res[ind+1],res[ind+2],res[ind+3],res[ind+4] = \
        scan_ief_upper(match_pt,spars)
    res[ind] += match_pt-1
    res[ind+1] += 1.0

    return cmat,res

def get_coeffs(wex,xpars={},use_stored=False):

    if use_stored and wex in pard:
        return pard[wex]

    cmat,res = get_coeff_mat(wex,xpars=xpars)
    #print('Coefficient matrix:')
    #for i in range(cmat.shape[0]):
    #    print(cmat[i])

    pars = np.zeros(res.shape[0]+1)
    pars[0] = 1.0
    pars[1:] = np.linalg.solve(cmat,res)

    """
    if min_x_d2:
        xt = np.linspace(0.0,match_pt,2000)
        ddief = np.zeros_like(xt)
        for i in range(2,nc):
            ddief += i*(i-1)*(c[i] - c[i-1])*xt**(i-2)
        nsgnchg = 0
        ddiefsgn = np.sign(ddief)
        ddiefsgn = ddiefsgn[np.abs(ddiefsgn)>0.0]
        osgn = ddiefsgn[0]
        for i in range(1,ddiefsgn.shape[0]):
            if ddiefsgn[i]*osgn < 0.0:
                nsgnchg += 1
        if nsgnchg <= 2.0:
            return pars
    """
    #print(np.dot(cmat,pars)-res)
    #exit()

    return pars

def new_interp(x,c,wex,ret_deriv=False):
    nc = c.shape[0]
    if wex.lower() == 'x':
        sps = scan_x_pars
    elif wex.lower() == 'c':
        sps = scan_c_pars

    ief = np.zeros(x.shape)
    dief = np.zeros(x.shape)

    lmask = x <= match_pt
    umask = x > match_pt
    xm = x[lmask]
    for i in range(nc):
        ief[lmask] += c[i]*xm**i
    ief[lmask] *= (1-xm)

    ief[umask] = -sps['D']*np.exp(sps['C2']/(1-x[umask]))

    if ret_deriv:
        """
            ief_lower(x) = c[0] + sum_(i=1)^Nc { c[i] - c[i-1] } x^i - c[Nc] x^(Nc+1)
        """
        dief[lmask] = -nc*c[nc-1]*xm**(nc-1)
        for i in range(1,nc):
            dief[lmask] += i*(c[i] - c[i-1])*xm**(i-1)

        dief[umask] = ief[umask]*sps['C2']/(1-x[umask])**2

    return ief, dief

def smooth_interp(x,c,ret_deriv=False):
    ief_den = 1 + (c[0] + c[1])*x + (1 + c[1])*x**2/c[2]
    ief = (1 + c[1]*x - (1 + c[1])*x**2)/ief_den
    if ret_deriv:
        dief = (c[1] - 2*(1+c[1])*x - (c[0] + c[1] + 2*(1+c[1])*x/c[2])*ief)/ief_den
        return ief,dief
    return ief

if __name__=="__main__":

    ta1 = np.linspace(0.0,0.5,100)
    ta2 = 1/np.linspace(.01,.2,100)
    obj_x = lambda c,x : np.abs(rscan_interp(x,'c')-\
        smooth_interp(x,[c[0],c[1],scan_c_pars['D']]))
    obj = lambda c : np.append(obj_x(c,ta1),obj_x(c,ta2))
    from scipy.optimize import least_squares
    cpx = least_squares(obj,[scan_c_pars['C1'],scan_c_pars['C2']],\
        bounds=((0,-np.inf),(np.inf,np.inf)))
    print(cpx.x)
    #exit()

    import matplotlib.pyplot as plt
    ta = np.linspace(0.0,5.0,2000)
    nief,dnief=smooth_interp(ta,[cpx.x[0],cpx.x[1],scan_c_pars['D']],ret_deriv=True)
    plt.plot(ta,nief,color='tab:blue',linestyle='-')
    plt.plot(ta,dnief,color='tab:blue',linestyle='--')

    nief,dnief=scan_interp(ta,scan_c_pars,ret_deriv=True)
    plt.plot(ta,nief,color='tab:orange',linestyle='-')
    plt.plot(ta,dnief,color='tab:orange',linestyle='--')

    nief,dnief=rscan_interp(ta,'c',ret_deriv=True)
    plt.plot(ta,nief,color='tab:green',linestyle='-')
    plt.plot(ta,dnief,color='tab:green',linestyle='--')

    plt.show()
    exit()

    eta = 0.223658

    xd = {}
    xd['f1'],xd['f2'],h1,h2 = get_scan2_pars(eta)
    print(xd['f1'],xd['f2'],h1,h2)

    import matplotlib.pyplot as plt
    x = np.linspace(0.0,5.0,2000)

    for w in ['x','c']:

        cl = get_coeffs(w,xpars=xd)
        str = '{:}: ['.format(w.upper())
        for i in range(cl.shape[0]):
            str += '{:},'.format(cl[i])
        str = str[:-1] +']'
        print(str)

        ief,dief = new_interp(x,cl,w,ret_deriv=True)
        if w == 'x':
            scanps = scan_x_pars
        elif w == 'c':
            scanps = scan_c_pars

        sief,sdief = scan_interp(x,scanps,ret_deriv=True)
        rief,rdief = rscan_interp(x,w,ret_deriv=True)

        fig,ax = plt.subplots(figsize=(8,6))
        ax.plot(x,ief,label='New',linewidth=2,color='darkblue')
        ax.plot(x,sief,label='SCAN',linewidth=2,color='darkorange')
        ax.plot(x,rief,label='rSCAN',linewidth=2,color='darkgreen')
        ax.set_xlim([x[0],x[-1]])
        plt.hlines(0.0,x[0],x[-1],color='gray',linewidth=2)
        if w == 'x':
            ax.legend(fontsize=16,title='$\\eta={:}$'.format(eta),title_fontsize=16)
        elif w == 'c':
            ax.legend(fontsize=16)
        ax.tick_params('both',labelsize=16)
        ax.set_xlabel('$x$',fontsize=16)
        ax.set_ylabel('$f_{\\mathrm{'+w+ '} }(x)$',fontsize=16)
        #ax.annotate('$\\eta={:}$'.format(eta),(),fontsize=16)
        plt.savefig('../ke_ge/f'+w+ '_comp_interp.pdf',bbox_inches='tight',dpi=600)

        plt.cla()
        plt.clf()
        fig,ax = plt.subplots(figsize=(8,6))

        ax.plot(x,dief,label='New',linewidth=2,color='darkblue')
        ax.plot(x,sdief,label='SCAN',linewidth=2,color='darkorange')
        ax.plot(x,rdief,label='rSCAN',linewidth=2,color='darkgreen')
        ax.set_xlim([x[0],x[-1]])
        plt.hlines(0.0,x[0],x[-1],color='gray',linewidth=2)
        if w == 'x':
            ax.legend(fontsize=16,title='$\\eta={:}$'.format(eta),title_fontsize=16)
        elif w == 'c':
            ax.legend(fontsize=16)
        ax.tick_params('both',labelsize=16)
        ax.set_xlabel('$x$',fontsize=16)
        ax.set_ylabel('$d f_{\\mathrm{'+w+ "} }/dx$",fontsize=16)
        plt.savefig('../ke_ge/f'+w+ '_comp_interp_deriv.pdf',bbox_inches='tight',dpi=600)

    exit()
