import numpy as np
from H_ex import is_valid_set, set_a1x, set_c_in_ms0_h0x, set_original_SCAN
import Settings
from Parameters import get_pretty_coefficients
from Atomics import get_Ex_for_atoms, get_rmse_nonlin, get_Ec_non_colinear, get_Ec_for_atoms
from itertools import product
from math import pi
import Benchmarks
from scipy.optimize import minimize, minimize_scalar
import SCAN2
import matplotlib.pyplot as plt
from TestSet import assess_ae6_for_scan2, assess_bh6_for_scan2
import time
from int_ex import get_lzlx_coef
from int_ec import get_lzlc_coef
from random import random
import sys
from Ar2_pscf import assess_ar2, jsextr

class SmallValueException(Exception):
    pass

class SimpleResult:
    x = None
    fun = None

def random_IE_params(betah, beta, beta2, beta3, beta4, xparams, cparams):
    print("Making random...")
    while True:
        xparams['CAX'] = random()*10 - 8.0 # ranges from [-8, 2]
        cparams['CAC'] = random()*10 - 8.0 # ranges from [-8, 2]

        for n in ["CB", "CC", "CD"]:
            xparams[n+"X"] = random()*20 - 10.0
            cparams[n+"C"] = random()*20 - 10.0

        d1_x = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
        d1_c = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])
        # d1_x = SCAN2.interp_d1_pade(beta, beta2, beta3, beta4, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
        # d1_c = SCAN2.interp_d1_pade(beta, beta2, beta3, beta4, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])

        oneD_ie = 100*(np.sum(betah*d1_x[d1_x > 0]) + np.sum(betah*d1_c[d1_c > 0]))

        # fx = SCAN2.eval_pade(beta, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
        # fc = SCAN2.eval_pade(beta, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])
        # if np.any(fx > 1) or np.any(fc > 1):
        #     oneD_ie += 100

        # Enforce minimal fx
        minfx = -(2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)
        # fx_min = 100*(max(minfx, -SCAN2.eval_pade(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))-minfx)
        f1 = SCAN2.eval_smoothlap(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
        if f1 > minfx and oneD_ie < 0.01:
            return

def opt_main(He_dens, sa_dens, lz_dens, nsp_dens, jellies, ae6_dens, bh6_dens, ar2_dens, xparams, cparams):
    assert Settings.Fint == "CRUDE" or Settings.Fint == "PADE", "ERROR: IE function {:} not CRUDE".format(Settings.Fint)
    # assert Settings.CORRELATION == "COMPENSATED", "ERROR: Correlation {:} not COMPENSATED".format(Settings.CORRELATION)

    # Run dx = 0, dc = 0 first, see what happens,
    # Then can repeat with dx and dc != 0
    # np.seterr(all='raise')
    xparams['K1'] = 0.174
    xparams['K0'] = 0.174

    # Set single orbital exchange
    if Settings.H0X == "MS0":
        c, a1x = set_c_in_ms0_h0x(xparams)
        xparams['C'] = c
        cparams['C'] = c
        xparams['A1'] = a1x
    else:
        a1x = set_a1x(xparams)
        xparams['A1'] = a1x
        xparams['K0'] = SCAN2.K0

    He_ex = get_Ex_for_atoms(He_dens, xparams)[0]

    param_sum = 1.0

    res = set_original_SCAN(cparams, xparams, He_dens)

    cparams['B1C'] = res['B1C']
    cparams['B2C'] = res['B2C']
    cparams['B3C'] = res['B3C']
    # cparams['H1DC1'] = 1.0
    # cparams['H1DC2'] = 0.1
    # xparams['H1DX'] = 1.0

    # show_for_params(xparams, cparams)

    # plot_many_rs(cparams, param_sum)

    # raise SystemExit


    ie_names = ["CA", "CB", "CC", "CD"]
    # nstr = reduce(lambda x, y: str(x)+"_"+str(y), sys.argv[1:])+".csv"

    # nstr = "RESULTS_compensated.csv"
    nstr = "res_{:}.csv".format(sys.argv[1])
    with open(nstr, 'w') as ofile:
        tstr = ",Interpolation,fx = fc,h0x,h1x,gx,correlation,"
        tstr += "CAX,CBX,CCX,CDX,CAC,CBC,CCC,CDC,"
        tstr += "k1,b1x,b2x,b3x,b4x,a1x,b1c,b2c,b3c,"
        # tstr += "chi_2,chi_42,dx,dc,"
        tstr += "dx,dc1,dc2,"
        if Settings.DO_AE6:
            tstr += "AE6 ME,AE6 MAE,"
        if Settings.DO_BH6:
            tstr += "BH6 ME,BH6 MAE,"
        tstr += "Atoms X,Atoms C,Atoms XC,"
        # tstr += "NSP X,NSP C,NSP XC,"
        if Settings.DO_AR2:
            tstr += ("Ar2_{:},"*len(Benchmarks.AR2_SEPS)).format(*Benchmarks.AR2_SEPS)
        if Settings.DO_JELLIUM:
            tstr += "Jellium X,Jellium C,Jellium XC,"
        tstr += ("{:},"*len(Benchmarks.BM_LIST)).format(*Benchmarks.BM_LIST)
        # tstr += ("{:},"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*Benchmarks.NON_SPHERICAL_LIST)
        if Settings.DO_AE6:
            tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
            tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
        if Settings.DO_BH6:
            tstr += ("{:},"*len(Benchmarks.BH6_LIST)).format(*Benchmarks.BH6_LIST)
            tstr += ("{:},"*len(Benchmarks.BH6_TESTS)).format(*Benchmarks.BH6_TESTS)
        if Settings.DO_JELLIUM:
            tstr += ("Jellium X {:},"*len(Settings.JELLIUM_RS_LIST)).format(*Settings.JELLIUM_RS_LIST)
            tstr += ("Jellium C {:},"*len(Settings.JELLIUM_RS_LIST)).format(*Settings.JELLIUM_RS_LIST)
            tstr += ("Jellium XC {:},"*len(Settings.JELLIUM_RS_LIST)).format(*Settings.JELLIUM_RS_LIST)
        tstr += "LZ_Ax,LZ_Bx,LZ_Bc,obj,"
        ofile.write(tstr+"\n")

        # start = [ -0.04604788,   0.0331932,   -4.15572785,   3.72164732,  23.93345625,
                # -17.70447435,  -1.90668704,   0.92085958,  -2.55137776,  16.67170546]
        # start = [0.0, 0.0, -1.5, -9,  42,  -28, -1.5,   4,   23,  -21]
        # start = [-3.82694636, 3.66125344, -2.61593588, 0.37451508, -3.46752383,  3.15513496, -2.61593588, 0.37451508,]

        best = 1e30
        best_x = None

        # start = map(float, sys.argv[1:])
        # istart = [start]
        # for i in range(len(start)):
        #     s = []+start
        #     if i != 0 and i != 5:
        #         s[i] += 1.0
        #     else:
        #         s[i] += 0.01
        #     istart.append(s)
        # start = [1e-4, 1e-4]+[0.0]*8
        # isimp = [start,
        #     [5e-4, 1e-4]+[0.0]*8,
        #     [1e-4, 5e-4]+[0.0]*8,
        #     [1e-4, 1e-4]+[1.0]+[0.0]*7,
        #     [1e-4, 1e-4]+[0.0]*1+[1.0]+[0.0]*6,
        #     [1e-4, 1e-4]+[0.0]*2+[1.0]+[0.0]*5,
        #     [1e-4, 1e-4]+[0.0]*3+[1.0]+[0.0]*4,
        #     [1e-4, 1e-4]+[0.0]*4+[1.0]+[0.0]*3,
        #     [1e-4, 1e-4]+[0.0]*5+[1.0]+[0.0]*2,
        #     [1e-4, 1e-4]+[0.0]*6+[1.0]+[0.0],
        #     [1e-4, 1e-4]+[0.0]*7+[1.0]
        # ]
        # last_dc = [1e30]

        # res = minimize(dxc_obj, start, method='Nelder-Mead', options={'adaptive':True, 'fatol':1e-2},
        #     args=(xparams, cparams, He_dens, He_ex, sa_dens, lz_dens, jellies, ae6_dens, ofile) )
        # # res = SimpleResult()
        # # res.x = start
        # print "FINAL"
        # print res.x

        # raise SystemExit

        betah = 0.00001
        beta = np.arange(0, 1, betah)
        beta2 = beta**2
        beta3 = beta**3
        beta4 = beta**4

        elapsed = 0.0
        ltime = time.time()
        best = 1e30

        start = [-3.54890905e+00,  7.98010364e+00, -2.48274886e-01,  1.54778731e+00,
        -5.60434308e-01,  5.84717500e+00,  3.34558206e-01, -4.23171149e+00,
        7.06127757e-02,  2.17009347e+00,  5.45341107e-02,  1.76153164e-01]

        cparams['H1DC1'] = 1.0
        xparams['H1DC1'] = 1.0
        xparams['H1DC2'] = 0.1
        res = minimize(dc2_only, start, method="Nelder-Mead", options={'adaptive':True}, args=(xparams, cparams, sa_dens, lz_dens, jellies, ae6_dens, ar2_dens, ofile))

        print(res.x)

        raise SystemExit

        p = res.x

        # xparams['H1DX'] = abs(p[0])
        # cparams['H1DC'] = abs(p[5])
        # xparams['H1DX'] = 0.001
        # cparams['H1DC'] = 0.1
        ie_names = ["CA", "CB", "CC", "CD"]
        for i, n in enumerate(ie_names):
            xparams[n+"X"] = p[i+1]
            cparams[n+"C"] = p[i+6]

        fig = plt.figure(figsize=(5,11))
        ax = fig.add_subplot(211)

        s = np.linspace(0, 10, 400)
        beta = np.linspace(0.0, 0.8, 400)
        gs, gbeta = np.meshgrid(s, beta)

        beta_line = 0.31

        Fx = SCAN2.scanFx(xparams, gs**2, 0.0, 0.0, gbeta, delta_x=False)

        cf = ax.contourf(gs, gbeta, Fx, 20)
        cfb = ax.contour(gs, gbeta, Fx, 20, colors='k')
        ax.clabel(cfb)
        ax.set_xlabel("$s$")
        ax.set_ylabel("$\\beta$")
        ax.set_xlim([s[0], s[-1]])
        ax.set_ylim([beta[0], beta[-1]])
        ax.hlines(beta_line, s[0], s[-1], colors='r', linestyles='--')
        x_deriv = (1 + xparams['CAX']/2.0 + xparams['CBX']/4.0)/((1 + xparams['CCX']/4.0)**2 + xparams['CDX']/16.0)
        fig.suptitle("$k_1$ = {:.3f}, $d_x$ = {:.3f} $\\sum$ {:.3f}\nIE coef = {:.3f}, {:.3f}, {:.3f}, {:.3f}".format(xparams['K1'], xparams['H1DX'], x_deriv, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
        cbar = fig.colorbar(cf)
        cbar.add_lines(cfb)


        Fx_b05 = SCAN2.scanFx(xparams, s**2, 0.0, 0.0, np.full_like(s, beta_line), delta_x=False)
        ax2 = fig.add_subplot(212)
        ax2.plot(s, Fx_b05)
        ax2.set_xlabel("$s$")
        ax2.set_ylabel("$F_x$")
        ax2.set_xlim([s[0], s[-1]])
        ax2.text(0.5, 0.5, "$\\beta = {:}$".format(beta_line), transform=ax2.transAxes)
        # plt.savefig("Final_Fx.pdf")
        plt.tight_layout(rect=[0.0, 0.0, 1.0, 0.95])
        plt.show()

        print("BEST")
        print(best)
        print(best_x)
        raise SystemExit

    dc = 1.0
    param_sum = -1.0

    # res = set_correlation(dc, He_dens, He_ex, xparams, cparams, param_sum)

    # cparams['H1DC'] = dc
    # cparams['CHI_2'] = res['CHI_2']
    # cparams['CHI_42'] = res['CHI_42']
    # cparams['B1C'] = res['B1C']
    # cparams['B2C'] = res['B2C']
    # cparams['B3C'] = res['B3C']

    # plot_many_rs(cparams, param_sum)

    # Generate set of {Ac, Bc, Cc, Dc} and {Ax, Bx, Cx, Dx}
    # Must be different as correlation requires |1 + A/2 + B/4 + C/8 + D/16| <= 0.5
    # for ec1(rs -> infinity) monotonicity in s
    assert Settings.X_C_DIFF_RANGE, "Need to set different ranges for X and C"
    ie_xp, ie_cp = get_pretty_coefficients(xparams)


    # Generate set of {dx} and check Fx_0 > Fx_1 for +ve bound
    dx_list = [_f for _f in [check_Fx_parts(h1dx, xparams) for h1dx in np.arange(Settings.H1DX_MIN, Settings.H1DX_MAX, Settings.H1DX_STEP)] if _f]

    print("Number of dx candidates {:}".format(len(dx_list)))

    # dc_list = filter(None, map(lambda h1dc: set_correlation(h1dc, He_dens, He_ex, xparams, cparams), np.arange(Settings.H1DC_MIN, Settings.H1DC_MAX, Settings.H1DC_STEP)))
    dc_list = []
    try:
        with open("dc_candidates.dat", "r") as inp:
            print("Reading correlation sets...")
            lines = inp.readlines()
            fields = lines[0].strip().split(',')
            for line in lines[1:]:
                bits = list(map(float, line.strip().split(',')))
                di = {}
                for i, f in enumerate(fields):
                    di[f] = bits[i]
                dc_list.append(di)
    except IOError:
        print("File missing, generating correlation sets...")
        with open("dc_candidates.dat", "w") as out:
            out.write("H1DC,B1C,B2C,B3C,CHI_2,CHI_42\n")
            for dc in np.arange(Settings.H1DC_MIN, Settings.H1DC_MAX, Settings.H1DC_STEP):
                res = set_correlation(dc, He_dens, He_ex, xparams, cparams, param_sum)
                if res is None:
                    print("We are done!")
                    break
                else:
                    dc_list.append(res)
                    out.write("{:.3f},{:.3f},{:.3f},{:.3f},{:.3f},{:.3f}\n".format(res['H1DC'], res['B1C'], res['B2C'], res['B2C'], res['CHI_2'], res['CHI_42']))

    ie_names = ["CA", "CB", "CC", "CD"]

    print("Filtering exchange with sperical atoms...")
    xset = []
    for x_cand in tqdm(product(dx_list, ie_xp),total=len(dx_list)*len(ie_xp),disable=Settings.NO_PROG):
        xparams['H1DX'] = x_cand[0]
        xp = x_cand[1]
        for i, n in enumerate(ie_names):
            xparams[n+"X"] = xp[i]

        exl, err_x, rms_x = get_rmse_nonlin(sa_dens, xparams, Benchmarks.BM_X, correlation=False)
        if Settings.ERR_TOL_X is not None and abs(rms_x) > Settings.ERR_TOL_X:
            # If an exchange filter was set, we failed it
            continue

        jellium_exl = None
        jellium_rms_x = None
        if Settings.DO_JELLIUM:
            jerr_xs = []
            jellium_exl = []
            for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                ex = jellies[i].eval_x_func(SCAN2.getscan_x, xparams)
                jellium_exl.append(ex)
                jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
            jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
            if Settings.TOL_JELLIUM_X is not None and jellium_rms_x < Settings.TOL_JELLIUM_X:
                # Failed at Jellium corrleation test
                continue


        xset.append([x_cand, exl, jellium_exl, jellium_rms_x])

    print("{:} exchange parameter sets".format(len(xset)))


    print("Filtering correlation with spherical atoms...")
    cset = []
    with np.errstate(all='raise'):
        for c_cand in tqdm(product(dc_list, ie_cp),total=len(dc_list)*len(ie_cp),disable=Settings.NO_PROG):
            cparams['H1DC'] = c_cand[0]['H1DC']
            cparams['CHI_2'] = c_cand[0]['CHI_2']
            cparams['CHI_42'] = c_cand[0]['CHI_42']
            cparams['B1C'] = c_cand[0]['B1C']
            cparams['B2C'] = c_cand[0]['B2C']
            cparams['B3C'] = c_cand[0]['B3C']
            cp = c_cand[1]
            for i, n in enumerate(ie_names):
                cparams[n+"C"] = cp[i]

            ecl, err_c, rms_c = get_rmse_nonlin(sa_dens, cparams, Benchmarks.BM_C, correlation=True)

            if Settings.ERR_TOL_C is not None and abs(rms_c) > Settings.ERR_TOL_C:
                # If a correlation filter was set, we failed it
                continue

            jellium_ecl = None
            jellium_rms_c = None
            if Settings.DO_JELLIUM:
                jerr_cs = []
                jellium_ecl = []
                for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
                    ec = jellies[i].eval_c_func(SCAN2.getscan_c, cparams)
                    jellium_ecl.append(ec)
                    jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ex)/abs(Benchmarks.JELLIUM_C[i]))
                jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
                if Settings.TOL_JELLIUM_C is not None and jellium_rms_c < Settings.TOL_JELLIUM_C:
                    # Failed at Jellium corrleation test
                    continue
            ae6_ecl = None
            if Settings.DO_AE6:
                ae6_ecl = get_Ec_non_colinear(ae6_dens, cparams)

            cset.append([c_cand, ecl, jellium_ecl, jellium_rms_c, ae6_ecl])
    print("{:} correlation parameter sets".format(len(cset)))

    ie_names = ["CA", "CB", "CC", "CD"]
    nstr = "RESULTS_compensated.csv"
    with open(nstr, 'w') as ofile:
        tstr = ",Interpolation,fx = fc,h0x,h1x,gx,correlation,"
        tstr += "CAX,CBX,CCX,CDX,CAC,CBC,CCC,CDC,"
        tstr += "k1,b1x,b2x,b3x,b4x,a1x,b1c,b2c,b3c,"
        tstr += "chi_2,chi_42,dx,dc,"
        if Settings.DO_AE6:
            tstr += "AE6 ME,AE6 MAE,"
        if Settings.DO_BH6:
            tstr += "BH6 ME,BH6 MAE,"
        tstr += "Atoms X,Atoms C,Atoms XC,"
        # tstr += "NSP X,NSP C,NSP XC,"
        if Settings.DO_AR2:
            tstr += ("Ar2_{:},"*len(Benchmarks.AR2_SEPS)).format(*Benchmarks.AR2_SEPS)
        if Settings.DO_JELLIUM:
            tstr += "Jellium X,Jellium C,Jellium XC,"
        tstr += ("{:},"*len(Benchmarks.BM_LIST)).format(*Benchmarks.BM_LIST)
        # tstr += ("{:},"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*Benchmarks.NON_SPHERICAL_LIST)
        if Settings.DO_AE6:
            tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
            tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
        if Settings.DO_BH6:
            tstr += ("{:},"*len(Benchmarks.BH6_LIST)).format(*Benchmarks.BH6_LIST)
            tstr += ("{:},"*len(Benchmarks.BH6_TESTS)).format(*Benchmarks.BH6_TESTS)
        if Settings.DO_JELLIUM:
            tstr += ("Jellium XC {:},"*len(Settings.JELLIUM_RS_LIST)).format(*Settings.JELLIUM_RS_LIST)
        tstr += "LZ Ax,LZ Bx,LZ Bc,"
        ofile.write(tstr+"\n")

        last_dc = 1e30

        res = minimize(dxc_obj, start, method='Nelder-Mead', options={'adaptive':True},
            args=(xparams, cparams, He_dens, He_ex, sa_dens, ae6_dens, ofile, last_dc) )

        print("FINAL")
        print(res.x)
        print(res.fun)
        raise SystemExit

        final_list = []

        best_ae6 = 1e30

        xiter = tqdm(xset, desc="Best = N/A", disable=Settings.NO_PROG)

        for x_cand in xiter:
            xparams['H1DX'] = x_cand[0][0]
            xp = x_cand[0][1]
            for i, n in enumerate(ie_names):
                xparams[n+"X"] = xp[i]
            exl = x_cand[1]
            bad_x = False
            ae6_exl = get_Ex_for_atoms(ae6_dens, xparams)

            for c_cand in cset:
                cparams['H1DC'] = c_cand[0][0]['H1DC']
                cparams['CHI_2'] = c_cand[0][0]['CHI_2']
                cparams['CHI_42'] = c_cand[0][0]['CHI_42']
                cparams['B1C'] = c_cand[0][0]['B1C']
                cparams['B2C'] = c_cand[0][0]['B2C']
                cparams['B3C'] = c_cand[0][0]['B3C']
                cp = c_cand[0][1]
                for i, n in enumerate(ie_names):
                    cparams[n+"C"] = cp[i]
                ecl = c_cand[1]

                excl = exl + ecl
                err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
                rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

                if Settings.ERR_TOL_XC is not None and abs(rms_xc) > Settings.ERR_TOL_XC:
                    # Failed at the combined XC filter, if it was set
                    continue

                if Settings.DO_JELLIUM:
                    jellium_rms_x = x_cand[3]
                    jellium_rms_c = c_cand[3]
                    jxc = x_cand[2] + c_cand[2]
                    jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_cs)**2))
                    if Settings.TOL_JELLIUM_C is not None and jellium_rms_c < Settings.TOL_JELLIUM_C:
                        # Failed at Jellium corrleation test
                        continue

                ae6_ecl = c_cand[4]
                ae6_err, ae6_etot = assess_ae6_for_scan2(None, None, xparams, cparams, None, None, exl=ae6_exl, ecl=ae6_ecl)
                ae6_me = np.mean(ae6_err)
                ae6_mae = np.mean(np.abs(ae6_err))

                if ae6_mae < best_ae6:
                    xiter.desc = "New best ae6 = {:.3f}".format(ae6_mae)
                    best_ae6 = ae6_mae

                # if ae6_mae > 15.0:
                #     # This exchange is so bad there is probably not a correlation that can fix it
                #     citer.close()
                #     break

                if ae6_mae > Settings.TOL_AE6 and best_ae6 != ae6_mae:
                    continue

                err_x = 100*(Benchmarks.BM_X - exl)/np.abs(Benchmarks.BM_X)
                rms_x = np.sqrt(np.mean(np.array(err_x)**2))
                err_c = 100*(Benchmarks.BM_C - ecl)/np.abs(Benchmarks.BM_C)
                rms_c = np.sqrt(np.mean(np.array(err_c)**2))

                ost = ",{:},{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H0X, Settings.H1X, Settings.GX, Settings.CORRELATION)
                ost += "{:},{:},{:},{:},".format(xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'])
                ost += "{:},{:},{:},{:},".format(cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC'])
                ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
                ost += "{:},{:},{:},{:},".format(cparams['CHI_2'],cparams['CHI_42'],xparams['H1DX'],cparams['H1DC'])
                if Settings.DO_AE6:
                    ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
                if Settings.DO_BH6:
                    ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
                ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x, rms_c, rms_xc)
                # ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(nsp_rms_x, nsp_rms_c, nsp_rms_xc)
                if Settings.DO_AR2:
                    ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
                if Settings.DO_JELLIUM:
                    ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(jellium_rms_x, jellium_rms_c, jellium_rms_xc)
                ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
                # ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*nsp_err_xc)
                if Settings.DO_AE6:
                    ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
                    ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
                if Settings.DO_BH6:
                    ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
                    ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
                # if Settings.DO_JELLIUM:
                    # ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xcs)
                ofile.write(ost+"\n")

    raise SystemExit

def dc2_only(pset, xparams, cparams, sa_dens, lz_dens, jellies, ae6_dens, ar2_dens, ofile):
    nstr = ['CA', 'CB', 'CC', 'CD']
    for i, n in enumerate(nstr):
        xparams[n+'X'] = pset[i]
        cparams[n+'C'] = pset[i+4]

    xparams['K1'] = min(abs(pset[8]), 0.174)
    xparams['H1DX'] = abs(pset[9])
    cparams['H1DC1'] = abs(pset[10])
    cparams['H1DC2'] = abs(pset[11])

    print("==================================")
    print("K1: {:}".format(xparams['K1']))
    print("H1DX: {:}, H1DC1: {:}, H1DC2: {:}".format(xparams['H1DX'], cparams['H1DC1'], cparams['H1DC2']))
    print("CAX: {:}, CBX: {:}, CCX:{:}, CDX: {:}".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    print("CAC: {:}, CBC: {:}, CCC:{:}, CDC: {:}".format(cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))

    # Spherical atoms
    exl, err_x, rms_x = get_rmse_nonlin(sa_dens, xparams, Benchmarks.BM_X, correlation=False)
    ecl, err_c, rms_c = get_rmse_nonlin(sa_dens, cparams, Benchmarks.BM_C, correlation=True)

    excl = exl + ecl
    err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

    # Jellium surface
    jerr_xs = []
    jellium_exl = []
    jerr_cs = []
    jellium_ecl = []
    jerr_xcs = []
    for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
        ex = jellies[i].eval_x_func(SCAN2.getscan_x, xparams)
        ec = jellies[i].eval_c_func(SCAN2.getscan_c, cparams)
        exc = ex + ec
        jellium_exl.append(ex)
        jellium_ecl.append(ec)
        jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
        jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ec)/abs(Benchmarks.JELLIUM_C[i]))
        jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

    jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
    jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
    jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_xcs)**2))

    jellium_obj = max(1.0, jellium_rms_xc)

    # Large Z asymptote
    acoef_x, bcoef_x = get_lzlx_coef(xparams, lz_dens)
    bcoef_c = get_lzlc_coef(cparams, lz_dens)

    err_ax = 100*((Benchmarks.LZ_X_A - acoef_x)/abs(Benchmarks.LZ_X_A))
    err_bx = 100*((Benchmarks.LZ_X_B - bcoef_x)/abs(Benchmarks.LZ_X_B))

    err_bc = 100*((Benchmarks.LZ_C_B - bcoef_c)/abs(Benchmarks.LZ_C_B))

    lz_obj = max(1, np.mean(np.abs([err_ax, err_bx, err_bc]))) + max(1, np.mean(np.abs([err_ax, err_bx])))

    # AE6 test set
    ae6_exl = get_Ex_for_atoms(ae6_dens, xparams)
    ae6_ecl = get_Ec_non_colinear(ae6_dens, cparams)
    ae6_err, ae6_etot = assess_ae6_for_scan2(None, None, xparams, cparams, None, None, exl=ae6_exl, ecl=ae6_ecl)
    ae6_me = np.mean(ae6_err)
    ae6_mae = np.mean(np.abs(ae6_err))

    # Ar2 wall
    if Settings.DO_AR2:
        exl_ar2 = get_Ex_for_atoms(ar2_dens, xparams)*Benchmarks.EH_TO_KCAL
        ecl_ar2 = get_Ec_for_atoms(ar2_dens, cparams)*Benchmarks.EH_TO_KCAL
        exc_ar2 = exl_ar2 + ecl_ar2
        etot = {}

        for i, m in enumerate(Benchmarks.AR2_LIST):
            etot[m] = exc_ar2[i] + Benchmarks.AR2_NO_XC[m]

        seps = ["1.6", "1.8", "2.0"]

        cbs_a = jsextr(np.array([2,3,4]), np.array([etot['ArDZ'], etot['ArTZ'], etot['ArQZ']]))

        ar2_err = []
        for s in seps:
            cbs_e = jsextr(np.array([2,3,4]), np.array([etot[s+'DZ'], etot[s+'TZ'], etot[s+'QZ']]))
            ar2_err.append(cbs_e - 2*cbs_a - Benchmarks.AR2_BM[s])
        ar2_max = np.max(np.abs(ar2_err))
        ar2_obj = ar2_max


    sa_obj = max(1.0, rms_xc/Settings.ERR_TOL_XC)
    ae6_obj = max(Settings.TOL_AE6, ae6_mae) - Settings.TOL_AE6 + ae6_me**2

    betah = 0.00001
    beta = np.arange(0, 1, betah)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta**4

    d1_x = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
    d1_c = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])

    oneD_ie = 100*(np.sum(betah*d1_x[d1_x > 0]) + np.sum(betah*d1_c[d1_c > 0]))
    print("1D obj: {:}".format(oneD_ie))
    # fx = SCAN2.eval_pade(beta, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
    # fc = SCAN2.eval_pade(beta, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])
    # if np.any(fx > 1) or np.any(fc > 1):
    #     oneD_ie += 100

    # Enforce minimal fx
    minfx = (2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)
    # fx_min = 100*(max(minfx, -SCAN2.eval_pade(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))-minfx)
    print("fx(1) = ", -minfx, SCAN2.eval_smoothlap(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    fx_min = 100*(max(minfx, -SCAN2.eval_smoothlap(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])) - minfx)


    obj = max(jellium_rms_x, 0.75) + max(jellium_rms_xc, 0.75) + lz_obj + ae6_obj + oneD_ie + fx_min

    print(pset)
    print("LZ_Ax: {:.3f}%, LZ_Bx: {:.3f}%, LZ_Bc: {:.3f}%, LZ obj: {:.3f}".format(err_ax, err_bx, err_bc, lz_obj))
    if Settings.DO_AR2:
        print("Ar2_obj: {:.3f}".format(ar2_obj))
    print("SA: {:.3f}%, SA_x: {:.3f}%, Jellium: {:.3f}%, Jellium_x: {:.3f}%".format(rms_xc, rms_x, jellium_rms_xc, jellium_rms_x))
    print("AE6: {:.3f} kcal/mol, ME: {:.3f}".format(ae6_mae, ae6_me))
    print("OBJ: {:.3f}".format(obj))

    # Write it all out
    ost = ",{:},{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H0X, Settings.H1X, Settings.GX, Settings.CORRELATION)
    ost += "{:},{:},{:},{:},".format(xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'])
    ost += "{:},{:},{:},{:},".format(cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC'])
    ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    # ost += "{:},{:},{:},{:},".format(cparams['CHI_2'],cparams['CHI_42'],xparams['H1DX'],cparams['H1DC'])
    ost += "{:},{:},{:},".format(xparams['H1DX'],cparams['H1DC1'],cparams['H1DC2'])
    if Settings.DO_AE6:
        ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
    if Settings.DO_BH6:
        ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
    ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x, rms_c, rms_xc)
    # ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(nsp_rms_x, nsp_rms_c, nsp_rms_xc)
    if Settings.DO_AR2:
        ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
    if Settings.DO_JELLIUM:
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(jellium_rms_x, jellium_rms_c, jellium_rms_xc)
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
    # ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*nsp_err_xc)
    if Settings.DO_AE6:
        ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
    if Settings.DO_BH6:
        ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
    if Settings.DO_JELLIUM:
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xs)
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_cs)
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xcs)
    ost += "{:.4f}%,{:.4f}%,{:.4f}%,{:.3f}".format(acoef_x, bcoef_x, bcoef_c, obj)
    ofile.write(ost+"\n")

    return obj

def dxc_obj(p, xparams, cparams, He_dens, He_ex, sa_dens, lz_dens, jellies, ae6_dens, ofile):
    # last_dc is a single entry list to keep a reference rather than a const

    stime = time.time()
    print("==================================")
    # xparams['H1DX'] = abs(p[0])
    cparams['H1DC2'] = abs(p[-1])
    # xparams['CAX'] = ax
    # cparams['CAC'] = ac

    print("H1DX: {:}, H1DC1: {:}, H1DC2: {:}".format(xparams['H1DX'], cparams['H1DC1'], cparams['H1DC2']))
    print("CAX: {:}, CAC: {:}".format(xparams['CAX'], cparams['CAC']))

    ie_names = ["CA", "CB", "CC", "CD"]
    for i, n in enumerate(ie_names):
        xparams[n+"X"] = p[i]
        cparams[n+"C"] = p[i+4]

    print("CAX: {:}, CBX: {:}, CCX:{:}, CDX: {:}".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    print("CAC: {:}, CBC: {:}, CCC:{:}, CDC: {:}".format(cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))

    betah = 0.00001
    beta = np.arange(0, 1, betah)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta**4

    # Calculate correlation condition
    # cc = 100*max(0.0, (1 + cparams['CAC']/2.0 + cparams['CBC']/4.0 + cparams['CCC']/8.0 + cparams['CDC']/16.0) - 1.0)  #Subtract 1 because we want range < 1.0
    cc = 0.0 # 100*max(0.0, (1 + cparams['CAC']/2.0 + cparams['CBC']/4.0)/((1 + cparams['CCC']/4.0)**2 + cparams['CDC']/16.0) - 1.0)  #Subtract 1 because we want range < 1.0

    # Calculate monotonicity metric

    d1_x = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
    d1_c = SCAN2.interp_gradient_smooth(beta, beta2, beta3, beta4, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])
    # d1_x = SCAN2.interp_d1_pade(beta, beta2, beta3, beta4, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
    # d1_c = SCAN2.interp_d1_pade(beta, beta2, beta3, beta4, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])

    oneD_ie = 100*(np.sum(betah*d1_x[d1_x > 0]) + np.sum(betah*d1_c[d1_c > 0]))

    # fx = SCAN2.eval_pade(beta, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
    # fc = SCAN2.eval_pade(beta, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])
    # if np.any(fx > 1) or np.any(fc > 1):
    #     oneD_ie += 100

    # Enforce minimal fx
    minfx = (2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)
    # fx_min = 100*(max(minfx, -SCAN2.eval_pade(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))-minfx)
    print("fx(1) = ", -minfx, SCAN2.eval_smoothlap(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    fx_min = 100*(max(minfx, -SCAN2.eval_smoothlap(1.0, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])) - minfx)

    # Calculate smoothness metric
    # d2_x = SCAN2.interp_grad2_smooth(beta, beta2, beta3, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX'])
    # d2_c = SCAN2.interp_grad2_smooth(beta, beta2, beta3, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC'])
    twoD_ie = 0.000#*(np.sum(d2_x**2*betah) + np.sum(d2_c**2*betah))


    # Spherical atoms
    exl, err_x, rms_x = get_rmse_nonlin(sa_dens, xparams, Benchmarks.BM_X, correlation=False)
    ecl, err_c, rms_c = get_rmse_nonlin(sa_dens, cparams, Benchmarks.BM_C, correlation=True)

    excl = exl + ecl
    err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))


    # Jellium surface
    jerr_xs = []
    jellium_exl = []
    jerr_cs = []
    jellium_ecl = []
    jerr_xcs = []
    for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
        ex = jellies[i].eval_x_func(SCAN2.getscan_x, xparams)
        ec = jellies[i].eval_c_func(SCAN2.getscan_c, cparams)
        exc = ex + ec
        jellium_exl.append(ex)
        jellium_ecl.append(ec)
        jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
        jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ec)/abs(Benchmarks.JELLIUM_C[i]))
        jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

    jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
    jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
    jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_xcs)**2))

    jellium_obj = max(1.0, jellium_rms_xc)

    # Large Z asymptote
    acoef_x, bcoef_x = get_lzlx_coef(xparams, lz_dens)
    bcoef_c = get_lzlc_coef(cparams, lz_dens)

    err_ax = 100*((Benchmarks.LZ_X_A - acoef_x)/abs(Benchmarks.LZ_X_A))
    err_bx = 100*((Benchmarks.LZ_X_B - bcoef_x)/abs(Benchmarks.LZ_X_B))

    err_bc = 100*((Benchmarks.LZ_C_B - bcoef_c)/abs(Benchmarks.LZ_C_B))

    lz_obj = max(1, np.mean(np.abs([err_ax, err_bx, err_bc]))/2.0)

    # AE6 test set
    ae6_exl = get_Ex_for_atoms(ae6_dens, xparams)
    ae6_ecl = get_Ec_non_colinear(ae6_dens, cparams)
    ae6_err, ae6_etot = assess_ae6_for_scan2(None, None, xparams, cparams, None, None, exl=ae6_exl, ecl=ae6_ecl)
    ae6_me = np.mean(ae6_err)
    ae6_mae = np.mean(np.abs(ae6_err))

    sa_obj = max(1.0, rms_xc/Settings.ERR_TOL_XC)
    ae6_obj = max(Settings.TOL_AE6, ae6_mae) - Settings.TOL_AE6

    obj = cc + fx_min + oneD_ie + twoD_ie + sa_obj + jellium_obj + ae6_obj + lz_obj
    print(p)
    print("LZ_Ax: {:.3f}%, LZ_Bx: {:.3f}%, LZ_Bc: {:.3f}%, LZ obj: {:.3f}".format(err_ax, err_bx, err_bc, lz_obj))
    print("cc: {:.3f}, fx(1): {:.3f}, 1D: {:.3f}, 2D: {:.3f}, SA: {:.3f}% ({:.3f}), Jellium: {:.3f}%, AE6: {:.3f} kcal/mol ({:.3f}), OBJ: {:.3f} ({:.3f}s)".format(cc, fx_min, oneD_ie, twoD_ie, rms_xc, sa_obj, jellium_obj, ae6_mae, ae6_obj, obj, time.time() - stime))

    ost = ",{:},{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H0X, Settings.H1X, Settings.GX, Settings.CORRELATION)
    ost += "{:},{:},{:},{:},".format(xparams['CAX'],xparams['CBX'],xparams['CCX'],xparams['CDX'])
    ost += "{:},{:},{:},{:},".format(cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC'])
    ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    # ost += "{:},{:},{:},{:},".format(cparams['CHI_2'],cparams['CHI_42'],xparams['H1DX'],cparams['H1DC'])
    ost += "{:},{:},{:},".format(xparams['H1DX'],cparams['H1DC1'],cparams['H1DC2'])
    if Settings.DO_AE6:
        ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
    if Settings.DO_BH6:
        ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
    ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x, rms_c, rms_xc)
    # ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(nsp_rms_x, nsp_rms_c, nsp_rms_xc)
    if Settings.DO_AR2:
        ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
    if Settings.DO_JELLIUM:
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(jellium_rms_x, jellium_rms_c, jellium_rms_xc)
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
    # ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*nsp_err_xc)
    if Settings.DO_AE6:
        ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
    if Settings.DO_BH6:
        ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
    if Settings.DO_JELLIUM:
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xs)
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_cs)
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xcs)
    ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(acoef_x, bcoef_x, bcoef_c)
    ofile.write(ost+"\n")

    return obj


def seven_parameter_obj(p, xparams, cparams, sa_dens, jellies, ae6_dens, ofile, ecl, jellium_ecl, ae6_ecl):
    # last_dc is a single entry list to keep a reference rather than a const

    stime = time.time()
    print("==================================")

    print("H1DX: {:}, H1DC: {:}".format(xparams['H1DX'], cparams['H1DC']))

    xparams['X0'] = 1.0
    for i, c in enumerate(p):
        xparams["X"+str(i+1)] = c
        print("X{:}: {:}".format(i+1, xparams["X"+str(i+1)]))

    print("CAC: {:}, CBC: {:}, CCC:{:}, CDC: {:}".format(cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))

    betah = 0.0001
    beta = np.arange(0, 1, betah)

    # Calculate monotonicity metric
    d1_x_t1 = 0.0
    for i in range(len(p)):
        d1_x_t1 += (i+1)*xparams['X'+str(i+1)]*np.power(beta, i)
    d1_x_t1 *= (1 - 2*beta)

    d1_x_t2 = 0.0
    for i in range(len(p)+1):
        d1_x_t2 += xparams['X'+str(i)]*np.power(beta, i)
    d1_x_t2 *= 2

    d1_x = d1_x_t1 - d1_x_t2
    oneD_ie = 100*np.sum(betah*d1_x[d1_x > 0])

    # print beta[::1000]
    # print d1_x[::1000]

    # Enforce minimal fx
    # minfx = (2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)
    minfx = -2.0

    fx1_min = 0.0
    for i in range(len(p)+1):
        fx1_min += xparams['X'+str(i)]
    fx1_min *= -1
    fx_min = -100*min((min(minfx, fx1_min)) - minfx, 0.0)

    # Calculate smoothness metric
    # d2_x_t1 = 0.0
    # for i in range(2,len(p)):
    #     d2_x_t1 += i*(i-1)*xparams['X'+str(i)]*np.power(beta, i-2)
    # d2_x_t1 *= (1 - 2*beta)

    # d2_x_t2 = 0.0
    # for i in range(len(p)):
    #     d2_x_t2 += (i+1)*xparams['X'+str(i+1)]*np.power(beta, i)
    # d2_x_t2 *= 4

    # d2_x = d2_x_t1 - d2_x_t2

    twoD_ie = 0.000#*(np.sum(d2_x**2*betah))

    # Spherical atoms
    exl, err_x, rms_x = get_rmse_nonlin(sa_dens, xparams, Benchmarks.BM_X, correlation=False)

    excl = exl + ecl
    err_c = 100*(Benchmarks.BM_C - ecl)/np.abs(Benchmarks.BM_C)
    err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
    rms_c = np.sqrt(np.mean(np.array(err_c)**2))
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

    # Jellium surface
    jerr_xs = []
    jerr_cs = []
    jellium_exl = []
    jerr_xcs = []
    for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
        ex = jellies[i].eval_x_func(SCAN2.getscan_x, xparams)
        ec = jellium_ecl[i]
        exc = ex + ec
        jellium_exl.append(ex)
        jerr_xs.append(100*(Benchmarks.JELLIUM_X[i] - ex)/abs(Benchmarks.JELLIUM_X[i]))
        jerr_cs.append(100*(Benchmarks.JELLIUM_C[i] - ec)/abs(Benchmarks.JELLIUM_C[i]))
        jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

    jellium_rms_x = np.sqrt(np.mean(np.array(jerr_xs)**2))
    jellium_rms_c = np.sqrt(np.mean(np.array(jerr_cs)**2))
    jellium_rms_xc = np.sqrt(np.mean(np.array(jerr_xcs)**2))

    # AE6 test set
    ae6_exl = get_Ex_for_atoms(ae6_dens, xparams)
    ae6_err, ae6_etot = assess_ae6_for_scan2(None, None, xparams, cparams, None, None, exl=ae6_exl, ecl=ae6_ecl)
    ae6_me = np.mean(ae6_err)
    ae6_mae = np.mean(np.abs(ae6_err))

    sa_obj = max(1.0, rms_xc/Settings.ERR_TOL_XC)*10
    ae6_obj = max(Settings.TOL_AE6, ae6_mae) - Settings.TOL_AE6

    obj = fx_min + oneD_ie + twoD_ie + sa_obj + ae6_obj
    print(p)
    print("fx(1): {:.3f}, 1D: {:.3f}, 2D: {:.3f}, SA: {:.3f}% ({:.3f}), AE6: {:.3f} kcal/mol ({:.3f}), OBJ: {:.3f} ({:.3f}s)".format(fx_min, oneD_ie, twoD_ie, rms_xc, sa_obj, ae6_mae, ae6_obj, obj, time.time() - stime))

    ost = ",{:},{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H0X, Settings.H1X, Settings.GX, Settings.CORRELATION)
    for i in range(1, len(p)+1):
        ost += "{:},".format(xparams['X'+str(i)])
    ost += "{:},{:},{:},{:},".format(cparams['CAC'],cparams['CBC'],cparams['CCC'],cparams['CDC'])
    ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    ost += "{:},{:},{:},{:},".format(cparams['CHI_2'],cparams['CHI_42'],xparams['H1DX'],cparams['H1DC'])
    if Settings.DO_AE6:
        ost += "{:.3f},{:.3f},".format(ae6_me, ae6_mae)
    if Settings.DO_BH6:
        ost += "{:.3f},{:.3f},".format(bh6_me, bh6_mae)
    ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(rms_x, rms_c, rms_xc)
    # ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(nsp_rms_x, nsp_rms_c, nsp_rms_xc)
    if Settings.DO_AR2:
        ost += ("{:.3f},"*len(Benchmarks.AR2_SEPS)).format(*ar2_err)
    if Settings.DO_JELLIUM:
        ost += "{:.4f}%,{:.4f}%,{:.4f}%,".format(jellium_rms_x, jellium_rms_c, jellium_rms_xc)
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
    # ost += ("{:+.4f}%,"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*nsp_err_xc)
    if Settings.DO_AE6:
        ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
    if Settings.DO_BH6:
        ost += ("{:.4f},"*len(Benchmarks.BH6_LIST)).format(*[bh6_etot[x] for x in Benchmarks.BH6_LIST])
        ost += ("{:.3f},"*len(Benchmarks.BH6_TESTS)).format(*bh6_err)
    if Settings.DO_JELLIUM:
        ost += ("{:.3f},"*len(Benchmarks.JELLIUM_XC)).format(*jerr_xcs)
    ofile.write(ost+"\n")

    return obj

def set_correlation(h1dc, He_dens, He_ex, xparams, cparams, param_sum):
    cparams['H1DC'] = h1dc

    step = 0.02
    min_chi2 = 0.00
    max_chi2 = 1.0
    min_chi42 = 0.00
    max_chi42 = 1.0

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    h = 0.01
    r = np.arange(0.0, 15.0, h)
    exponent = 2.0
    N = (exponent**3/(8.0*pi))
    d = 2*N*np.exp(-exponent*r)
    g = np.abs(-2*N*exponent*np.exp(-exponent*r))
    s = g/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(d, 4.0/3.0))
    idxs = d > 1e-10
    d = d[idxs]
    g = g[idxs]
    s = s[idxs]
    r = r[idxs]

    chi_2 = np.arange(min_chi2, max_chi2, step)
    chi_42 = np.arange(min_chi42, max_chi42, step)
    duples = product(chi_2, chi_42)

    smallest_b1_set = {'H1DC':h1dc, 'B1C':1e30}

    for t in duples:
        res = is_valid_set(cparams, xparams, t, d, s, r, h, He_dens, He_ex, param_sum)
        if res is None:
            # print "Nothing for CHI_2: {:.3f} CHI_42: {:.3f}".format(t[0], t[1])
            continue
        if res is not None and res['B1C'] < smallest_b1_set['B1C']:
            smallest_b1_set = res
            smallest_b1_set['H1DC'] = cparams['H1DC']

        # print "Found B1C: {:.3f}, B2C: {:.3f}, B3C: {:.3f}, CHI_2: {:.3f} CHI_42: {:.3f}".format(smallest_b1_set['B1C'], smallest_b1_set['B2C'], smallest_b1_set['B3C'], smallest_b1_set['CHI_2'], smallest_b1_set['CHI_42'])

    try:
        cparams['B2C'] = smallest_b1_set['B2C']
        print("Setting correlation for dc = {:}, B1C: {:}, B2C: {:}, B3C: {:}, CHI_2: {:} CHI_42 at {:}".format(smallest_b1_set['H1DC'], smallest_b1_set['B1C'], smallest_b1_set['B2C'], smallest_b1_set['B3C'], smallest_b1_set['CHI_2'], smallest_b1_set['CHI_42']))
    except KeyError:
        print("Found nothing...")
        return None



    # He_c =  get_Ec_for_atoms(He_dens, cparams)[0]

    # print "\nHelium Exchange:     {:}".format(He_ex)
    # print   "Helium correlation:  {:}".format(He_c)
    # print   "Helium xc:           {:}".format(He_ex + He_c)
    # assert abs(He_ex+He_c + 1.068) < 1e-3, "Failed to meet He XC energy bound, {:} != -1.068".format(He_ex+He_c)

    return smallest_b1_set

def plot_Fx_parts(params):

    lb = -2
    ub = 3
    s = np.logspace(lb, ub, 1000)
    p = s**2
    beta = 0.5

    gp = -np.expm1(-params['A1']/np.sqrt(s))
    h0x = (1.0 + SCAN2.K0)*gp

    # Upper
    mup = 10.0/81.0*p
    qt = (9.0/10.0)*(2*beta - 1) + 17.0/12.0*p
    xfac_upper = mup + params['H1DX']
    h1x_upper = 1.0 + params['K1'] - params['K1']/(1.0 + xfac_upper/params['K1'])
    h1x_upper *= gp

    # Lower
    xfac_lower = mup - params['H1DX']
    h1x_lower = 1.0 + params['K1'] - params['K1']/(1.0 + xfac_lower/params['K1'])
    h1x_lower *= gp

    fig = plt.figure()
    ax = fig.gca()
    ax.semilogx(s, h0x, label="$F_x^0$", c='green')
    ax.semilogx(s, h1x_lower,color='darkblue', ls="--")
    ax.semilogx(s, h1x_upper, label="$F_x^1$ $d_x = ${:}".format(params['H1DX']),color='darkblue', ls="--")
    ax.fill_between(s,h1x_lower, h1x_upper, alpha=0.15, color='darkblue')

    ax.set_xlim([10**lb, 10**ub])
    ax.set_xlabel("$s$")
    ax.legend()
    fig.suptitle("$k_1 = {:.3f}$".format(params['K1']))
    plt.show()
    raise SystemExit

def check_Fx_parts(h1dx, params):
    lb = -2
    ub = 3
    s = np.logspace(lb, ub, 1000)
    p = s**2
    beta = 0.5

    gp = -np.expm1(-params['A1']/np.sqrt(s))
    h0x = (1.0 + SCAN2.K0)*gp

    # Upper
    mup = 10.0/81.0*p
    qt = (9.0/10.0)*(2*beta - 1) + 17.0/12.0*p
    xfac_upper = mup + h1dx
    h1x_upper = 1.0 + params['K1'] - params['K1']/(1.0 + xfac_upper/params['K1'])
    h1x_upper *= gp

    if np.all(h0x > h1x_upper):
        return h1dx
    else:
        return None

def plot_eps_c_limits(params):
    print("B1C:   {:.4f}".format(params['B1C']))
    print("B2C:   {:.4f}".format(params['B2C']))
    print("B3C:   {:.4f}".format(params['B3C']))
    print("B4C:   {:.4f}".format(params['B4C']))
    print("CHI_2:   {:.4f}".format(params['CHI_2']))
    print("CHI_42:   {:.4f}".format(params['CHI_42']))

    fig = plt.figure(figsize=(11,5))
    ax = fig.add_subplot(121)

    lb = -2
    ub = 3
    s = np.logspace(lb, ub, 2000)

    paramsum = 0.6

    ec1_inf = SCAN2.corgga_1_comp_rs_inf(params, s, 1.0, 0.0, sumis=paramsum)
    ec1_inf_beta1 = SCAN2.corgga_1_comp_rs_inf(params, s, 1.0, 1.0, sumis=paramsum)
    ec0_inf = SCAN2.corgga_0_acgga_rs_inf(params, s, 1.0)

    ax.semilogx(s, ec0_inf, label="$\\epsilon_{\\mathrm{c}}^0$", c='green')
    ax.semilogx(s, ec1_inf, label="$\\epsilon_{\\mathrm{c}}^1 \\beta = 0$", c='darkblue', ls="--")
    ax.semilogx(s, ec1_inf_beta1, label="$\\epsilon_{\\mathrm{c}}^1 \\beta = 1$", c='darkred', ls="--")
    ax.fill_between(s,ec1_inf, ec1_inf_beta1, alpha=0.15, color='darkblue')
    ax.set_xlabel("$s$")

    # zeta = np.linspace(0, 1, 1000)

    # ec1_inf = SCAN2.corgga_1_comp_rs_inf_s_inf(params, zeta)
    # ec0_inf = SCAN2.corgga_0_acgga_rs_inf_s_inf(params, zeta)

    # ax.plot(zeta, ec0_inf, label="$\\epsilon_{\\mathrm{c}}^0$", c='green')
    # ax.plot(zeta, ec1_inf, label="$\\epsilon_{\\mathrm{c}}^1$", c='darkblue', ls="--")
    ax.legend(loc=4)
    ax.set_title("$r_s\\rightarrow \\infty$\n$\\left|1 + \\frac{A}{2} + \\frac{B}{4} + \\frac{C}{8} + \\frac{D}{16}\\right| = $"+str(paramsum))

    ax.annotate("$\\epsilon_{\\mathrm{c}}^0 > \\epsilon_{\\mathrm{c}}^1:$"+" {:}".format(np.all(ec0_inf > ec1_inf)), xy=(0.05, 0.95), xycoords='axes fraction')

    ax2 = fig.add_subplot(122)

    ec1_0 = SCAN2.corgga_1_comp_rs_0(params, s, 1.0)
    ec0_0 = SCAN2.corgga_0_acgga_rs_0(params, s, 1.0)

    ax2.semilogx(s, ec0_0, label="$\\epsilon_{\\mathrm{c}}^0$", c='green')
    ax2.semilogx(s, ec1_0, label="$\\epsilon_{\\mathrm{c}}^1 \\beta = 0$", c='darkblue', ls="--")
    ax2.annotate("$\\epsilon_{\\mathrm{c}}^0 > \\epsilon_{\\mathrm{c}}^1:$"+" {:}".format(np.all(ec0_0 > ec1_0)), xy=(0.05, 0.95), xycoords='axes fraction')

    ax2.set_xlabel("$s$")

    ax2.set_title("$r_s \\rightarrow 0$")

    ax2.legend(loc=4)

    plt.tight_layout()

    plt.show()

    raise SystemExit

def plot_frac():
    zeta = np.linspace(0, 1, 1000)

    b1c = 0.041
    b3c = 0.180
    b4c = 2.3631

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    cx_zeta = -3.0/(4*pi)*(9*pi/4.0)**(1.0/3.0)*dx_zeta
    f0 = -0.9

    Gc = (1 - b4c*(dx_zeta - 1.0))*(1.0 - zeta**12)

    frac = b1c*Gc/(b3c*(cx_zeta - f0)) - 1.0

    plt.plot(zeta, frac)
    print(np.min(frac), np.max(frac))
    plt.show()
    raise SystemExit

def plot_many_rs(cparams, param_sum):
    cparams['H1DC'] = 10.0
    s = np.logspace(-2, 3, 100)

    lrs = np.logspace(-3, 4, 8)

    beta = np.zeros_like(s)
    zeta = 0.0

    fig = plt.figure(figsize=(10,15))

    hexs = ['#c994c7','#df65b0','#e7298a','#ce1256','#91003f']
    for i, rs in enumerate(lrs):
        ax = fig.add_subplot(420+i+1)

        e0 = SCAN2.corgga_0_ori(cparams, rs, s, beta)
        e1_b0 = SCAN2.corgga_1_comp_v5(cparams, rs, s, beta, zeta, sumis=param_sum)
        e1_b025 = SCAN2.corgga_1_comp_v5(cparams, rs, s, beta+0.49, zeta, sumis=param_sum)
        e1_b05 = SCAN2.corgga_1_comp_v5(cparams, rs, s, beta+0.5, zeta, sumis=param_sum)
        e1_b075 = SCAN2.corgga_1_comp_v5(cparams, rs, s, beta+0.51, zeta, sumis=param_sum)
        e1_b1 = SCAN2.corgga_1_comp_v5(cparams, rs, s, beta+1.0, zeta, sumis=param_sum)

        ax.semilogx(s, e0, label="$\\epsilon_c^0$")
        ax.semilogx(s, e1_b0, label="$\\epsilon_c^1$ $\\beta = 0$", c=hexs[0])
        ax.semilogx(s, e1_b025, c=hexs[1], ls=':')
        ax.semilogx(s, e1_b05, c=hexs[2], ls=':')
        ax.semilogx(s, e1_b075, c=hexs[3], ls=':')
        ax.semilogx(s, e1_b1, label="$\\epsilon_c^1$ $\\beta = 1$", c=hexs[4])

        ax.fill_between(s, e1_b1, e1_b0, alpha=0.1, color='#91003f')

        ax.set_xlabel("$s$")
        ax.text(0.65, 0.15, "$r_s = {:}$\n$\\epsilon_c^0 > \\epsilon_c^1? : ${:}".format(rs, np.all(e0 > e1_b1)), transform=ax.transAxes)
        if i == 0 or i >= 6:
            ax.legend()

    tstr = "$\\beta = "+str(beta[0])+", \\zeta = "+str(zeta)+"$\n"
    tstr += "$d_{c}:"+" {:.3f}$".format(cparams['H1DC'])
    tstr += " $b_1: {:.3f}, b_2: {:.3f}, b_3: {:.3f}$".format(cparams['B1C'], cparams['B2C'], cparams['B3C'])
    tstr += "\n$|1 + A/2 + B/4 +C/8 + D/16| = {:.3f}$".format(param_sum)
    fig.suptitle(tstr)
    plt.tight_layout(rect=[0, 0.0, 1, 0.93])
    plt.savefig("cor_1_v4_rs_many.pdf")

def plot_rs_inf(xparams, cparams, He_ex, He_dens):

    dc = 0.021

    print("Setting Correlation")
    # res = set_correlation(dc, He_dens, He_ex, xparams, cparams)

    print("set ")
    fig = plt.figure()
    ax = fig.add_subplot(111)

    s = np.logspace(-2, 3, 1000)

    beta = 1.0
    zeta = 0.0
    param_sum = -1.0

    ci0 = SCAN2.corgga_0_acgga_rs_inf(cparams, s, zeta)
    ci1 = SCAN2.corgga_1_rs_inf_v4(cparams, s, beta, zeta, sumis=param_sum)
    ci1_b0 = SCAN2.corgga_1_rs_inf_v4(cparams, s, 0.0, zeta, sumis=param_sum)

    hex = [str(i) for i in range(10)]+["a","b","c","d","e","f"]

    for i, dbeta in enumerate(np.linspace(-1e0, 0.0, 5)):
        param_sum = dbeta
        ci1 = SCAN2.corgga_1_rs_inf_v4(cparams, s, beta, zeta, sumis=param_sum)
        ax.semilogx(s, ci1, c="#{:}f2020".format(hex[i*2+5]), label="$C_{\\infty}^1 \\Sigma = "+"{:.3e}".format(param_sum)+"$")
        ci1_b0 = SCAN2.corgga_1_rs_inf_v4(cparams, s, 0.0, zeta, sumis=param_sum)
        ax.semilogx(s, ci1_b0, c="#20{:}f20".format(hex[i*2+5]), ls=":")
    ax.set_xlabel("s")
    ax.semilogx(s, ci0, label="$C_{\\infty}^0$")
    ax.legend(loc=4)



    # cparams['DC2'] = 0.0
    # ci1 = SCAN2.corgga_1_rs_inf_v3(cparams, s, beta, zeta, sumis=param_sum)
    # ax.semilogx(s, ci1, c="g", label="$C_{\\infty}^1 d_{c2} = "+"{:.3e}".format(cparams['DC2'])+"$")


    # beta = 0.0

    # for i, dbeta in enumerate(np.linspace(1e-6, 1e0, 5)):
    #     cparams['DC2'] = dbeta
    #     ci1 = SCAN2.corgga_1_rs_inf_v4(cparams, s, beta, zeta, sumis=param_sum)
    #     ax.semilogx(s, ci1, ls=":", c="#{:}00000".format(hex[i*2+5]), label="$C_{\\infty}^1 d_{c2} = "+"{:.3e}".format(cparams['DC2'])+"$")
    # cparams['DC2'] = 0.0
    # ci1 = SCAN2.corgga_1_rs_inf_v3(cparams, s, beta, zeta, sumis=param_sum)
    # ax.semilogx(s, ci1, ls = ":", c="g", label="$C_{\\infty}^1 d_{c2} = "+"{:.3e}".format(cparams['DC2'])+"$")


    ax.set_xlim([np.min(s), np.max(s)])
    ax.hlines(0.0, np.min(s), np.max(s), colors='gray', linestyles=':')

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    Gc = (1 - cparams['B4C']*(dx_zeta - 1.0))*(1.0 - zeta**12)
    cx_zeta = -3.0/(4*pi)*(9*pi/4.0)**(1.0/3.0)*dx_zeta
    f0 = -0.9
    # ax.text(0.6, 0.6, "$f_0 - C_x(\\zeta) = {:.6f}$".format(f0 - cx_zeta), transform=ax.transAxes)


    tstr = "$\\beta = "+str(beta)+", \\zeta = "+str(zeta)+"$\n"
    tstr += "$d_{c2}:"+" {:.3f}, \\chi_2: {:.3f}, \\chi_4^2: {:.3f}$".format(cparams['DC2'], cparams['CHI_2'], cparams['CHI_42'])
    tstr += " $b_1: {:.3f}, b_2: {:.3f}, b_3: {:.3f}$".format(cparams['B1C'], cparams['B2C'], cparams['B3C'])
    tstr += "\n$|1 + A/2 + B/4 +C/8 + D/16| = {:.3f}$".format(param_sum)
    fig.suptitle(tstr)

    fig.tight_layout(rect=[0, 0.0, 1, 0.85])
    plt.show()

def get_for_point(point, ae6_dens, xparams, cparams, ae6_exl):
    cparams['CAC'] = point[0]
    cparams['CBC'] = point[1]
    cparams['CCC'] = point[2]
    cparams['CDC'] = point[3]

    ae6_ecl = get_Ec_non_colinear(ae6_dens, cparams)
    ae6_err, ae6_etot = assess_ae6_for_scan2(None, None, xparams, cparams, None, None, exl=ae6_exl, ecl=ae6_ecl)
    return np.mean(np.abs(ae6_err))


def show_for_params(xparams, cparams):

    pset = [-3.54890905e+00,  7.98010364e+00, -2.48274886e-01,  1.54778731e+00,
        -5.60434308e-01,  5.84717500e+00,  3.34558206e-01, -4.23171149e+00,
        7.06127757e-02,  2.17009347e+00,  5.45341107e-03,  1.76153164e-01]

    nstr = ['CA', 'CB', 'CC', 'CD']
    for i, n in enumerate(nstr):
        xparams[n+'X'] = pset[i]
        cparams[n+'C'] = pset[i+4]

    xparams['K1'] = min(abs(pset[8]), 0.174)
    xparams['H1DX'] = abs(pset[9])
    cparams['H1DC1'] = abs(pset[10])
    cparams['H1DC2'] = abs(pset[11])

    figIE = plt.figure()
    axIE = figIE.add_subplot(111)

    beta = np.arange(0, 1.0, 0.001)

    axIE.plot(beta, SCAN2.eval_smoothlap(beta, xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']), label="Exchange")
    axIE.plot(beta, SCAN2.eval_smoothlap(beta, cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']), label="Correlation")
    axIE.axhline(color='gray', linestyle=':')

    axIE.legend()

    figFx = plt.figure()
    axFx = figFx.add_subplot(111)

    p = np.arange(0, 9.0, 0.01)

    gp, gbeta = np.meshgrid(p, beta)

    fx = SCAN2.scanFx(xparams, gp, 0.0, 0.0, gbeta)
    cbar = axFx.contourf(gp, gbeta, fx, [0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.174], extend='min')
    axFx.contour(gp, gbeta, fx, [0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.174], colors='k')
    cb = figFx.colorbar(cbar)
    axFx.set_xlabel("$p$")
    axFx.set_ylabel("$\\beta$")

    plt.show()