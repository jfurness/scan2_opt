import numpy as np
import SCAN2
import Benchmarks

def make_linear_energy_components(dens, xparams, cparams, XP, CP, non_colin=False):
    """
    Function to return the integrated components for IE functions in which
    parameters enter linearly.

    An extra 'NULL' entry is included for the portion that is separate from the IE parameters

    The two dicts returned can be supplied as is to the get_E(x/c)_from_lin functions to evaluate
    energy with given parameters by simple multiplication.
    """

    lin_exl = {}
    old_x = {}
    for p in XP:
        old_x[p] = xparams[p]
        xparams[p] = 0.0

    xparams['CONX'] = 0.0
    lin_exl['NULL'] = get_Ex_for_atoms(dens, xparams)
    xparams['CONX'] = 1.0
    lin_exl['CONX'] = 0.0 #get_Ex_for_atoms(dens, xparams) - lin_exl['NULL']
    xparams['CONX'] = 0.0

    for p in XP:
        xparams[p] = 1.0
        lin_exl[p] = get_Ex_for_atoms(dens, xparams) - lin_exl['NULL']
        xparams[p] = 0.0

    xparams['CONX'] = 1.0
    for p in XP:
        xparams[p] = old_x[p]

    # Repeat for correlation
    lin_ecl = {}
    old_c = {}  # So we can restore the original value
    for p in CP:
        old_c[p] = cparams[p]
        cparams[p] = 0.0

    cparams['CONC'] = 0.0
    if non_colin:
        lin_ecl['NULL'] = get_Ec_non_colinear(dens, cparams)
    else:
        lin_ecl['NULL'] = get_Ec_for_atoms(dens, cparams)

    cparams['CONC'] = 1.0
    if non_colin:
        lin_ecl['CONC'] = 0.0#  get_Ec_non_colinear(dens, cparams) - lin_ecl['NULL']
    else:
        lin_ecl['CONC'] = 0.0# get_Ec_for_atoms(dens, cparams) - lin_ecl['NULL']
    cparams['CONC'] = 0.0

    for p in CP:
        cparams[p] = 1.0
        if non_colin:
            lin_ecl[p] = get_Ec_non_colinear(dens, cparams) - lin_ecl['NULL']
        else:
            lin_ecl[p] = get_Ec_for_atoms(dens, cparams) - lin_ecl['NULL']
        cparams[p] = 0.0

    # Restore parameters
    cparams['CONC'] = 1.0
    for p in CP:
        cparams[p] = old_c[p]


    return lin_exl, lin_ecl

def get_Ex_from_lin(lin_exl, xparams, XP):
    """
    Takes 1st list of integrated quantities from make_linear_energy_components and
    returns total energy for the parameters stored in the parameter dictionary.
    """
    out = lin_exl['NULL'] + xparams['CONX']*lin_exl['CONX']
    for n in XP:
        out += xparams[n]*lin_exl[n]
    return out

def get_Ec_from_lin(lin_ecl, cparams, CP):
    """
    Takes 2nd list of integrated quantities from make_linear_energy_components and
    returns total energy for the parameters stored in the parameter dictionary.
    """
    out = lin_ecl['NULL'] + cparams['CONC']*lin_ecl['CONC']
    for n in CP:
        out += cparams[n]*lin_ecl[n]
    return out

def test_linear_composition(dens, xparams, cparams, XP, CP, non_colin=False):
    """
    Simple unit test to validate linear construction of energy from IE function parameters.lin_exl

    Test is such that linear version must match that calculated as a full integral of the energy
    density in the traditional way.

    1st tests each component individually, then does mixed for the default parameters.
    """
    lin_exl, lin_ecl = make_linear_energy_components(dens, xparams, cparams, XP, CP, non_colin=non_colin)
    print("PRE")
    print("CONX {:}, CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(xparams['CONX'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    print("CONC {:}, CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(cparams['CONC'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))
    #str = 'CONX {:}'.format(xparams['CONX'])
    #for axp in XP:
    #    str += '{:} {:}'.format(axp,xparams[axp])
    #str = 'CONC {:}'.format(cparams['CONC'])
    #for acp in CP:
    #    str += '{:} {:}'.format(acp,cparams[acp])


    plist = ['CON', 'CA', 'CB', 'CC', 'CD']

    for p in plist:
        xparams[p+'X'] = 0.0
        cparams[p+'C'] = 0.0

    for p in plist:

        xparams[p+'X'] = 3.0
        cparams[p+'C'] = 3.0

        fresh_x = get_Ex_for_atoms(dens, xparams)
        if non_colin:
            fresh_c = get_Ec_non_colinear(dens, cparams)
        else:
            fresh_c = get_Ec_for_atoms(dens, cparams)

        print("\nIN RC for "+p+": CONX {:}, CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(xparams['CONX'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
        print("IN RC for "+p+": CONC {:}, CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(cparams['CONC'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))
        rc_ex = get_Ex_from_lin(lin_exl, xparams, XP)
        rc_ec = get_Ec_from_lin(lin_ecl, cparams, CP)

        print("      |     Ex lin    |    Ex full    |     Ec lin    |    Ec full   ")
        for i in range(len(dens)):
            print("{:}  | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} ".format(i, rc_ex[i], fresh_x[i], rc_ec[i], fresh_c[i]))

        xparams[p+'X'] = 0.0
        cparams[p+'C'] = 0.0

    # Just for confidence, lets test mixed
    for p in plist:
        xparams[p+'X'] = SCAN2.DEFAULT_X_PARAMS[p+'X']
        cparams[p+'C'] = SCAN2.DEFAULT_C_PARAMS[p+'C']

    fresh_x = get_Ex_for_atoms(dens, xparams)
    if non_colin:
        fresh_c = get_Ec_non_colinear(dens, cparams)
    else:
        fresh_c = get_Ec_for_atoms(dens, cparams)

    print("\nCONX {:}, CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(xparams['CONX'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    print("CONC {:}, CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(cparams['CONC'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))
    rc_ex = get_Ex_from_lin(lin_exl, xparams, XP)
    rc_ec = get_Ec_from_lin(lin_ecl, cparams, CP)
    print("      |     Ex lin    |    Ex full    |     Ec lin    |    Ec full   ")
    for i in range(len(dens)):
        print("{:4}  | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} ".format(i, rc_ex[i], fresh_x[i], rc_ec[i], fresh_c[i]))


def get_Ex_for_atoms(atoms, xparams):
    """
    Takes a list of densities and returns the energy densities for them.
    Then integrates using weights provided to give full energy.

    INPUT:
        atoms: list of lists containing density arrays as
                [density, |Grad|, tau, weights] for spin unresolved
                [density_0, density_1, |grad|_0, |grad|_1, tau_0, tau_1, weights] for resolved
        xparams: working dictionary of parameters to be evaluated
    OUTPUT:
        list of X energies for given atoms
    """

    i = 0
    ex_list = []

    for a in atoms:
        if a.shape[1] == 4:
            # Spin unresolved density
            ex_eps = SCAN2.getscan_x(xparams, a[:, 0], a[:, 0], a[:, 1], a[:, 1], a[:, 2], a[:, 2])

            ex_eps[a[:, 0] + a[:, 0] < 1e-10] = 0.0  # Clamp low density to zero
            ex_list.append(np.dot(a[:, 3], ex_eps))  # Integrate with weights etc.
        else:
            # Spin Resolved                   d0,      d1,      g0,      g1,      t0,      t1,      l0,      l1
            ex_eps = SCAN2.getscan_x(xparams, a[:, 0], a[:, 1], a[:, 2], a[:, 3], a[:, 4], a[:, 5], a[:, 8], a[:, 9])

            ex_eps[a[:, 0] + a[:, 1] < 1e-10] = 0.0  # Clamp low density to zero
            ex_list.append(np.dot(a[:, 6], ex_eps))  # Integrate with weights etc.

        i += 1

    return np.array(ex_list)


def get_Ec_non_colinear(dens, cparams):
    """
    This should be used when the density gradients of spin 0
    and spin 1 are not necessarily colinear.

    Then expects extra 7th element in density array which has total
    density gradient (including cross terms).

    NOTE: This total density gradient != |grad_0| + |grad_1|
    """

    ec_list = []

    for i, a in enumerate(dens):
        ec_eps = SCAN2.getscan_c(cparams, a[:, 0]+a[:, 1], a[:, 7],
                        a[:, 4]+a[:, 5], a[:, 8]+a[:, 9], (a[:, 0] - a[:, 1])/(a[:, 0] + a[:, 1])
                    )

        ec_eps[a[:, 0] + a[:, 1] < 1e-10] = 0.0  # Clamp low density to zero
        ec_list.append(np.dot(a[:, 6], ec_eps))  # Integrate with weights etc.


    return np.array(ec_list)

def get_Ec_for_atoms(atoms, cparams):
    """
    Takes a list of densities and returns the energy densities for them.
    Then integrates using weights provided to give full energy.

    INPUT:
        atoms: list of lists containing density arrays as
                [density, |Grad|, tau, weights] for spin unresolved
                [density_0, density_1, |grad|_0, |grad|_1, tau_0, tau_1, weights] for resolved
        xparams: working dictionary of parameters to be evaluated
    OUTPUT:
        list of C energies for given atoms
    """

    ec_list = []

    for a in atoms:
        # print i
        # print "dens", np.dot(2*a[:, 0], a[:, 3])
        # Zeta is always 0 for closed shell atoms
        if a.shape[1] == 4:
            # Spin unresolved density, final argument is zeta
            ec_eps = SCAN2.getscan_c(cparams, a[:, 0]+a[:, 0], a[:, 1]+a[:, 1],
                    a[:, 2]+a[:, 2], np.zeros(a[:, 0].shape)
                )
            ec_eps[a[:, 0]+a[:, 0] < 1e-10] = 0.0  # Clamp low density to zero
            ec_list.append(np.dot(a[:, 3], ec_eps))  # Integrate with weights etc.
        else:
            # Spin Resolved, final argument is zeta
            # Note, this is asuming colinear density gradients!
            ec_eps = SCAN2.getscan_c(cparams, a[:, 0]+a[:, 1], a[:, 2]+a[:, 3],
                    a[:, 4]+a[:, 5], a[:, 8]+a[:, 9], (a[:, 0] - a[:, 1])/(a[:, 0] + a[:, 1])
                )
            ec_eps[a[:, 0] + a[:, 1] < 1e-10] = 0.0  # Clamp low density to zero
            ec_list.append(np.dot(a[:, 6], ec_eps))  # Integrate with weights etc.


    return np.array(ec_list)
