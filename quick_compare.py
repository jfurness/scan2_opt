import numpy as np
import matplotlib.pyplot as plt



al = np.linspace(0,1,num=200)

for a in np.logspace(0.125, 1, 10):
    y = (1-2*al)**3*(1 + np.exp(a*al))*0.5
    plt.plot(al, y, label = str(a))

# for cbx in np.linspace(0, 2, num=10):
#     plt.plot(al, smooth_alpha(2*al, cax, cbx, dx))
plt.hlines(-1.24, 0, 1,linestyles='--')
plt.vlines(0.5, -2, 1.1,linestyles='--',colors='gray')
plt.legend()
plt.xlim([0,1])
plt.ylim([-2,1.1])
plt.xticks([0.0, 0.2, 0.4, 0.5, 0.6, 0.8, 1.0])
plt.show()