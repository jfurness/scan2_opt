import numpy as np
from functools import partial
from scipy.optimize import least_squares

# for getting spherical Hartree-Fock densities
from Densities import Atom, GridGenerator
from Benchmarks import BM_X, BM_C, BM_XC, BM_LIST

import matplotlib.pyplot as plt

pi = np.pi
gamma = (1.0 - np.log(2.0))/pi**2
beta0 = 0.066725

MIN_DEN = 1e-10

"""
    Exchange ingredients
"""

def series_GGA_Fx(p,b,mu=0.21951,kappa=0.804):
    nprs = len(b)+2
    c = np.zeros(nprs)
    c[0] = kappa
    c[1:-1] = b[:]
    c[-1] = kappa - np.sum(c)

    fx = np.ones_like(p)
    x = mu*p/kappa
    y = x/(1+x)

    tmp = np.zeros_like(y)
    tmp[:] = y
    for i in range(nprs):
        fx += c[i]*tmp
        tmp *= y

    return fx

def series_GGA_unp(n,gn,b,mu=0.21951,kappa=0.804):

    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2

    exlda = -3*kf*n/(4*pi)
    Fx = series_GGA_Fx(p,b,mu=mu,kappa=kappa)
    return exlda*Fx

def series_GGA(n0,n1,g0,g1,b,mu=0.21951,kappa=0.804,typ=None):

    ex0 = series_GGA_unp(2*n0,2*g0,b,mu=mu,kappa=kappa)

    if typ is None:
        ex1 = series_GGA_unp(2*n1,2*g1,b,mu=mu,kappa=kappa)
        ex = (ex0 + ex1)/2
    elif typ == 'UNP':
        # spin-unpolarized system
        ex = ex0
    elif typ == 'OES':
        # fully spin-polarized, one-electron system with zero spin-down density
        ex = ex0/2

    return ex

def get_Ix_unp(n,gn,wg,rank,mu=0.21951,kappa=0.804,mask=None):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3*kf*n/(4*pi)

    p = (gn/(2*kf*n))**2
    x = mu*p/kappa
    y = x/(1+x)

    Ix = np.zeros(rank)
    tmpigrd = wg*exlda

    if mask is None:
        mask = np.ones(p.shape,dtype=bool)

    for i in range(rank):
        Ix[i] = np.sum(tmpigrd[mask])
        tmpigrd *= y

    return Ix

def get_Ix(n0,n1,g0,g1,wg,rank,mu=0.21951,kappa=0.804,typ=None):

    npars = rank+3
    msk = n0 + n1 > MIN_DEN
    Ix0 = get_Ix_unp(2*n0,2*g0,wg,npars,mu=mu,kappa=kappa,mask=msk)
    if typ is None:
        Ix1 = get_Ix_unp(2*n1,2*g1,wg,npars,mu=mu,kappa=kappa,mask=msk)
        Ix = (Ix0 + Ix1)/2
    elif typ == 'UNP':
        # spin-unpolarized system
        Ix = Ix0
    elif typ == 'OES':
        # fully spin-polarized, one-electron system with zero spin-down density
        Ix = Ix0/2

    return Ix

def get_c_from_b(b,kappa=0.804):
    c = np.zeros(len(b)+3)
    c[0] = 1
    c[1] = kappa
    c[2:-1] = b[:]
    c[-1] = kappa - np.sum(c[1:])
    return c

def get_Ex_from_Ix(Ix,b,mu=0.21951,kappa=0.804):
    c = get_c_from_b(b,kappa=kappa)
    return np.dot(c,Ix)


"""
    Correlation ingredients
"""


def spinf(z,n):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    return (opz**n + omz**n)/2.0

def eps_c_pw92(rs,z,unp=False):

    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """
    rsh = rs**(0.5)

    def g(v):
        q0 = -2*v[0]*(1 + v[1]*rs)
        q1 = 2*v[0]*(v[2]*rsh + v[3]*rs + v[4]*rs*rsh + v[5]*rs*rs)
        logt = np.log(1 + 1/q1)
        return q0*logt

    unp_pars = [0.031091,0.21370,7.5957,3.5876,1.6382,0.49294]
    ec0 = g(unp_pars)
    if unp:
        return ec0

    pol_pars = [0.015545,0.20548,14.1189,6.1977,3.3662,0.62517]
    alp_pars = [0.016887,0.11125,10.357,3.6231,0.88026,0.49671]

    fz_den = 2**(4/3)-2
    fdd0 = 8/9/fz_den
    dxz = spinf(z,4/3)
    fz = 2*(dxz - 1)/fz_den

    ec1 = g(pol_pars)
    ac = g(alp_pars)
    z4 = z**4
    fzz4 = fz*z4
    ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

    return ec

def eps_c_pbe(rs,z,p,var='PBE',unp=False):
    """
        PBE: [PBE1996]
        PBEsol: [P2008]
    """
    phi = spinf(z,2/3)
    gp3 = gamma*phi**3
    t2 = (3.0*pi**2/16.0)**(2.0/3.0)*p/(phi**2*rs)

    ecl = eps_c_pw92(rs,z,unp=unp)
    beta = {'PBE': beta0, 'PBEsol': 0.046,
    'revTPSS': beta0*(1 + 0.1*rs)/(1 + 0.1778*rs),
    }

    aa = beta[var]/gamma/np.expm1(-ecl/gp3)
    v = aa*t2
    v2 = v**2

    gt = t2*(1.0 + v)/(1.0 + v + v2)
    hc = 1.0 + beta[var]/gamma*gt

    ec = ecl + gp3*np.log(hc)

    return ec

def ec_dens(n0,n1,gn,var='PBE',unp=False):

    n = n0 + n1
    rs = (3/(4*pi*n))**(1/3)
    zeta = np.minimum(1.0,np.maximum(-1.0,(n0 - n1)/n))
    p = (gn/(2*(3*pi**2*n)**(1/3)*n))**2
    p[gn**2 <= 1e-16] = 0.0

    eps_c = eps_c_pbe(rs,zeta,p,var=var,unp=unp)

    return eps_c*n


"""
    Density related stuff
"""

def generate_density(alist):
    """
    Utility function to generate and arrange the orbitals for
    the atoms in alist (strings of atomic symbols).
    """
    out = []
    n, r, wt = GridGenerator.make_grid(200)
    for a in map(Atom, alist):
        d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
        idxs = d0 + d1 > MIN_DEN  # Filter all those with small densities
        # Later functions expect density, |grad|, tau, spherical integration weight.
        # spin 0 then 1 for each quantity
        grid = 4*pi*wt[idxs]*r[idxs]**2

        assert max( np.dot(l0[idxs], grid), np.dot(l1[idxs], grid)) < 1e-3, \
            "Laplacian integral failed for {:}".format(a)

        out.append(np.stack([d0[idxs], d1[idxs], g0[idxs], g1[idxs], t0[idxs], t1[idxs], grid, g0[idxs]+g1[idxs], l0[idxs], l1[idxs]], axis=1))
    return out

def get_Ec_colin(dens):

    ec = ec_dens(dens[:,0],dens[:,1],dens[:,7],var='PBE',unp=False)
    ec[dens[:, 0] + dens[:, 1] < MIN_DEN] = 0.0

    return np.dot(dens[:,6],ec)

def get_Ex(dens,pars):

    ex = series_GGA(dens[:, 0],dens[:, 1],dens[:, 2],dens[:, 3],pars,\
        mu=0.21951,kappa=0.804,typ=None)
    ex[dens[:, 0] + dens[:, 1] < MIN_DEN] = 0.0

    return np.dot(dens[:,6],ex)


def obj(pars,ec_d,Ix_d,only_res):

    N_BM = len(BM_LIST)
    exc_l = np.zeros(N_BM)
    xc_err_l = np.zeros(N_BM)

    for iat,at in enumerate(BM_LIST):
        exc_l[iat] = ec_d[at] + get_Ex_from_Ix(Ix_d[at],pars,mu=0.21951,kappa=0.804)
        xc_err_l[iat] = 100*(exc_l[iat]/BM_XC[iat] - 1)#exc_l[iat] - BM_XC[iat]

    #res = np.abs(xc_err_l)/N_BM#np.sum(np.abs(xc_err_l))/N_BM

    if only_res:
        return np.abs(xc_err_l)/N_BM
    else:
        return exc_l, xc_err_l, xc_err_l#np.sum(np.abs(xc_err_l))/N_BM

def opt(Npar):

    full_dens = generate_density(BM_LIST)

    ec_d = {}
    Ix_d = {}
    for iat,at in enumerate(BM_LIST):
        tdens = full_dens[iat]
        ec_d[at] = get_Ec_colin(tdens)
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,6],\
            Npar,mu=0.21951,kappa=0.804,typ=None)

    tobj = partial(obj,ec_d=ec_d,Ix_d=Ix_d,only_res=True)

    if Npar == 0:
        ps = []
    else:
        lsrd = least_squares(tobj,np.ones(Npar))
        ps = lsrd.x

    #for iat,at in enumerate(BM_LIST):
    #    print(at,get_Ex(full_dens[iat],ps) - get_Ex_from_Ix(Ix_d[at],ps))

    ens, errs, fres = obj(ps,ec_d,Ix_d,False)
    return ps, ens, errs, fres

if __name__ == "__main__":

    Max_Npar = 5

    pl = 10**np.linspace(-2,3,5000)

    fig,ax = plt.subplots(2,1,figsize=(6,7))
    clist = plt.cm.inferno(np.linspace(0,0.85,Max_Npar+1))
    lsls = ['-','--','-.',':']

    str = 'N pars'
    for i in range(Max_Npar+3):
        str += ', c{:}'.format(i)
    for iat, at in enumerate(BM_LIST):
        str += ', Exc {:} (Ha), XC err {:} (%)'.format(at,at)
    str += ', Obj (Ha) \n'

    res_l = np.zeros(Max_Npar+1)

    for Npar in range(Max_Npar+1):
        opt_pars, xcs, errs, resv = opt(Npar)
        res_l[Npar] = (np.sum(resv**2)/((resv.shape[0] - Npar)**2 + 1))**(0.5)
        cps = get_c_from_b(opt_pars,kappa=0.804)

        str += '{:}'.format(Npar)
        for ipar in range(Npar+3):
            str += ', {:}'.format(cps[ipar])
        for ipar in range(Npar,Max_Npar):
            str += ', '
        for iat, at in enumerate(BM_LIST):
            str += ', {:}, {:}'.format(xcs[iat],errs[iat])
        str += ', {:} \n'.format(res_l[Npar])

        if Npar == 0:
            lbl = '0/PBE'
        else:
            lbl = Npar

        ax[0].plot(pl,series_GGA_Fx(pl,opt_pars),color=clist[Npar],\
            linestyle=lsls[Npar%len(lsls)],label = lbl)

    with open('./fits/series_gga.csv','w+') as tfl:
        tfl.write(str)

    ax[0].legend(fontsize=12,ncol=2)

    ax[0].set_xscale('log')
    ax[0].set_xlim(pl[0],pl[-1])
    ax[0].set_xlabel('$p$',fontsize=12)

    ax[0].hlines(1.804,*ax[0].get_xlim(),color='gray',linestyle=':')

    ax[0].set_ylim(0.0,2.0)
    ax[0].set_ylabel('$F_\\mathrm{x}(p)$',fontsize=12)

    ax[1].plot([i for i in range(Max_Npar+1)],res_l/res_l[0],color='k')
    ax[1].set_xlim(0,Max_Npar)
    ax[1].set_xlabel('$M$',fontsize=12)

    ax[1].set_ylim(0.98*res_l.min()/res_l[0],1.02*res_l.max()/res_l[0])
    ax[1].set_ylabel('GoF($M$)/GoF(0)',fontsize=12)

    #plt.show()
    plt.savefig('./fits/series_gga.pdf',dpi=666,bbox_inches='tight')
