import numpy as np
from math import pi

def getPBEsol_x(foo, d0, d1, g0, g1, t0, t1):
    MUGE = 10.0/81.0
    KAPPA = 0.804
    c = np.power(81.0/(4.0*pi),1.0/3.0)/2.0
    AX = -0.7385587663820224058842300326808360

    rho = 2*d0
    drho = 2*g0
    p = drho**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))
    t1 = 1.0 + MUGE*p/KAPPA
    Fx = 1.0 + KAPPA - KAPPA/t1
    exunif_0 = AX*rho**(1.0/3.0)
    exlda_0 = exunif_0*rho
    Ex_0 = exlda_0*Fx

    rho = 2*d0
    drho = 2*g0
    p = drho**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))
    t1 = 1.0 + MUGE*p/KAPPA
    Fx = 1.0 + KAPPA - KAPPA/t1
    exunif_1 = AX*rho**(1.0/3.0)
    exlda_1 = exunif_1*rho
    Ex_1 = exlda_1*Fx
    return (Ex_0 + Ex_1)/2.0

def getPBEsol_c(foo, d0, d1, g0, g1, t0, t1, zeta):
    dd0 = np.abs(g0)
    dd1 = np.abs(g1)
    dthrd = np.exp(np.log(d0+d1)*1.0/3.0)
    rs = (0.75/pi)**(1.0/3.0)/dthrd
    eps = pw92eps(zeta, rs)
    u = phi(d0, d1)
    gnn = dd0 + 2*dd0*dd1 + dd1
    d2 = np.power(1.0/12.0*np.power(3,5.0/6.0)/np.power(pi,-1.0/6.0),2)
    d2 *= gnn/(u*u*np.power(d0+d1, 7.0/3.0))
    return (d0+d1)*(eps + H(d2, eps, np.power(u,3)))

def A(eps, u3):
    param_gamma = (1-np.log(2.0))/(pi*pi)
    param_beta_accurate  = 0.06672455060314922
    param_beta_gamma = param_beta_accurate/param_gamma
    return param_gamma/np.expm1(-eps/(param_gamma*u3))

def H(d2, eps, u3):
    param_gamma = (1-np.log(2.0))/(pi*pi)
    param_beta_accurate  = 0.06672455060314922
    param_beta_gamma = param_beta_accurate/param_gamma
    d2A = d2*A(eps, u3)
    return param_gamma*u3*np.log(1.0+param_beta_gamma*d2*(1.0+d2A)/(1.0+d2A*(1.0+d2A)))

def pw92eps(zeta, rs):
    t = [[0.0310907, 0.21370, 7.5957 , 3.5876, 1.6382 ,0.49294,1],
        [0.01554535, 0.20548, 14.1189, 6.1977, 3.3662 ,0.62517,1],
        [0.0168869, 0.11125, 10.357 , 3.6231, 0.88026,0.49671,1]]
    c = 8.0/(9.0*(2*np.power(2, 1.0/3.0)-2))
    zeta4 = np.power(zeta, 4)
    omegaval = omega(zeta)
    sqrtrs = np.sqrt(rs)
    e0 = eopt(sqrtrs, t[0])
    return e0 - eopt(sqrtrs,t[2])*omegaval*(1-zeta4)/c + (eopt(sqrtrs,t[1]) - e0)*omegaval*zeta4

def omega(zeta):
    return (ufunc(zeta,4.0/3.0)-2)/(2.0*pow(2,1.0/3.0)-2.0)

def ufunc(x, a):
    return np.power(1+x, a)+np.power(1-x, a)

def eopt(sqrtr, t):
     return -2*t[0]*(1+t[1]*sqrtr*sqrtr)* \
      np.log(1+0.5/(t[0]*(sqrtr*(t[2] \
                  +sqrtr*(t[3] \
                      +sqrtr*(t[4] \
                          +t[5]*sqrtr))))))

def phi(d0, d1):
    n_m13 = np.power(d0+d1, -1.0/3.0)
    a_43 = np.power(d0, 4.0/3.0)
    b_43 = np.power(d1, 4.0/3.0)
    return np.power(2.0,-1.0/3.0)*n_m13*n_m13*(np.sqrt(a_43)+np.sqrt(b_43))
