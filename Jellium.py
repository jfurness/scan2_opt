import numpy
from math import pi
import SCAN
import SCAN2
import PBEsol
from Atomics import get_Ex_for_atoms, get_Ec_for_atoms, print_report
import Settings

class Jelly:
    """
    Container class to hold jellium denisties and evaluate functionals on them.

    instantiated with rs value, X. This value must have a corresponding "LDAX" file
    that holds the orbitals for the density
    """
    r = None  # coordinate
    d = None  # density
    g = None  # gradient
    t = None  # kinetic energy density
    lap = None  # Density laplacian
    hat = None
    friedel = None
    rs = None
    CFACT = 1556920.0

    lin_exl = None  # Dictionary of integrated energy quantities for polynomial IE
    lin_ecl = None  # Same for correlation

    def __init__(self, rs):
        self.rs = rs

        self.d0 = 3.0/(4.0*pi*self.rs**3)
        self.kF0 = 1.0/self.rs*(9.0*pi/4.0)**(1.0/3.0)
        self.eF0 = self.kF0**2/2.0
        self.tau0 = 0.3*self.kF0**2*self.d0

        try:
            with open("LDA"+str(rs), 'r') as inp:
                i = 1
                self.r = []  # coordinate
                self.d = []  # density
                self.g = []  # gradient
                self.t = []  # kinetic energy density
                self.lap = []  # Density laplacian (unused)
                for line in inp:
                    if i == 1:
                        # Read the parameters for unit change and Friedel oscillations
                        self.hat, self.friedel = tuple(map(float, [_f for _f in line.strip().split(' ') if _f]))
                    else:
                        bits = list(map(float, [_f for _f in line.strip().split(' ') if _f]))
                        self.r.append(bits[0])
                        self.d.append(bits[1])
                        self.g.append(bits[2])
                        self.t.append(bits[3] + 0.6*self.eF0*bits[1])
                        self.lap.append(bits[4])
                    i += 1
        except FileNotFoundError:
            print("ERROR: Jellium density file LSD{:} is not found".format(rs))
            raise SystemExit

        self.r = numpy.array(self.r)
        self.d = numpy.array(self.d)
        self.g = numpy.array(self.g)
        self.t = numpy.array(self.t)
        self.lap = numpy.array(self.lap)

        self.npts = self.r.shape[0]

        # Calculate some variables of use for evaluating functionals
        self.t0 = (3.0/10.0)*((3.0*pi**2)**(2.0/3.0))*(self.d0**(2.0/3.0))*self.d
        self.akfl = (3.0*pi**2*self.d)**(1.0/3.0)
        self.sgr = numpy.abs(self.g)/(2.0*self.akfl*self.d)
        self.tw = self.g**2/(8.0*self.d)
        self.tau = self.t + 0.6*self.eF0*self.d
        self.zz = self.tw/self.tau

        self.weights = numpy.ones(self.npts)
        self.weights[[0, -1]] = 0.5

        self.d_0 = self.d*0.5
        self.d_1 = self.d*0.5
        self.grd_0 = self.g*0.5
        self.grd_1 = self.g*0.5
        self.tau_0 = self.t*0.5
        self.tau_1 = self.t*0.5
        self.lap_0 = self.lap*0.5
        self.lap_1 = self.lap*0.5

        self.aakf = (3.0*pi**2*self.d)**(1.0/3.0)
        self.aaks = numpy.sqrt(4.0*self.aakf/pi)
        self.zeta = (self.d_0 - self.d_1)/self.d
        self.phi = 0.5*((1.0 + self.zeta)**(2.0/3.0) + (1.0-self.zeta)**(2.0/3.0))
        self.t = numpy.abs(self.g)/(2*self.aaks*self.d*self.phi)

    def ECPW92(self):
        """
        Determines PW92 correlation for the jellium

        Could be done once and saved...
        """
        pp = 1.0
        AA = 0.031091
        alpha1 = 0.21370
        beta1 = 7.5957
        beta2 = 3.5876
        beta3 = 1.6382
        beta4 = 0.49294
        ECPW920 = -2.0*AA*(1.0 + alpha1*self.rs)*numpy.log(1.0 + 1.0/(2.0*AA*(
                beta1*numpy.sqrt(self.rs) +
                beta2*self.rs +
                beta3*(self.rs**1.5) +
                beta4*(self.rs**(pp+1.0))
            )))

        pp = 1.0
        AA = 0.015545
        alpha1 = 0.20548
        beta1 = 14.1189
        beta2 = 6.1977
        beta3 = 3.3662
        beta4 = 0.62517
        ECPW921 = -2.0*AA*(1.0 + alpha1*self.rs)*numpy.log(1.0 +
                1.0/(2.0*AA*(
                    beta1*numpy.sqrt(self.rs) +
                    beta2*self.rs +
                    beta3*(self.rs**1.5) +
                    beta4*(self.rs**(pp + 1.0))
            )))

        pp = 1.0
        AA = 0.016887
        alpha1 = 0.11125
        beta1 = 10.357
        beta2 = 3.6231
        beta3 = 0.88026
        beta4 = 0.49671

        amalphac = -2.0*AA*(1.0 + alpha1*self.rs)*numpy.log(1.0 +
                1.0/(2.0*AA*(
                    beta1*numpy.sqrt(self.rs) +
                    beta2*self.rs +
                    beta3*(self.rs**1.5) +
                    beta4*(self.rs**(pp + 1.0))
                )))

        f = ((1.0 + self.zeta)**(4.0/3.0) + (1.0 - self.zeta)**(4.0/3.0) - 2.0)/(2.0**(4.0/3.0) - 2.0)

        ECPW92 = ECPW920 - amalphac*f*(1.0 - (self.zeta**4))*(1.0/1.709921) + (ECPW921 - ECPW920)*f*(self.zeta**4)
        return ECPW92

    def eval_func(self, Fx, Fc, Xparams, Cparams):
        sigmaX = self.eval_x_func(Fx, Xparams)
        sigmaC = self.eval_c_func(Fc, Cparams)

        return sigmaX, sigmaC

    def eval_c_func(self, Fc, Cparams):
        Ec = Fc(Cparams, self.d_0+self.d_1, self.grd_0+self.grd_1,
                 self.tau_0+self.tau_1, self.lap_0+self.lap_1, self.zeta)
        Ec[self.d  < 1e-20] = 0.0
        corr = numpy.dot(Ec, self.weights)*self.hat*self.CFACT
        suni = numpy.dot(self.d*self.ECPW92(), self.weights)*self.hat*self.CFACT
        #Ecunif = Fc(Cparams, self.d0*numpy.ones(1), numpy.zeros(1),
        #         self.tau0*numpy.ones(1), numpy.zeros(1), numpy.zeros(1))[0]
        #suni = numpy.dot(self.d*Ecunif, self.weights)*self.hat*self.CFACT
        sigmaC = corr - suni

        return sigmaC

    def eval_x_func(self, Fx, Xparams):
        Ex = Fx(Xparams, self.d_0, self.d_1, self.grd_0, self.grd_1,
                 self.tau_0, self.tau_1, self.lap_0, self.lap_1)
        Ex[self.d  < 1e-20] = 0.0
        exch = numpy.dot(Ex, self.weights)*self.hat*self.CFACT

        exuni = -3.0*self.kF0/(4.0*pi)
        """
        exuni = Fx(Xparams, self.d0/2*numpy.ones(1), self.d0/2*numpy.ones(1),
                 numpy.zeros(1), numpy.zeros(1),self.tau0/2*numpy.ones(1),
                 self.tau0/2*numpy.ones(1), numpy.zeros(1), numpy.zeros(1))[0]/self.d0
        """
        sxuni = numpy.dot(self.d*exuni, self.weights)*self.hat*self.CFACT

        sigmaX = exch - sxuni
        return sigmaX

    def make_linear_energy_components(self, Fx, Fc, xparams, cparams, XP, CP):
        """
        Function to return the integrated components for IE functions in which
        parameters enter linearly.

        An extra 'NULL' entry is included for the portion that is separate from the IE parameters

        The two dicts are filled allowing get_E(x/c)_from_lin functions to evaluate
        energy with given parameters by simple multiplication.
        """
        self.lin_exl = {}
        old_x = {}
        for p in XP:
            old_x[p] = xparams[p]
            xparams[p] = 0.0
        xparams['CONX'] = 0.0
        self.lin_exl['NULL'] = self.eval_x_func(Fx, xparams)
        xparams['CONX'] = 1.0
        self.lin_exl['CONX'] = self.eval_x_func(Fx, xparams) - self.lin_exl['NULL']
        xparams['CONX'] = 0.0

        for p in XP:
            xparams[p] = 1.0
            self.lin_exl[p] = self.eval_x_func(Fx, xparams) - self.lin_exl['NULL']
            xparams[p] = 0.0

        xparams['CONX'] = 1.0
        for p in XP:
            xparams[p] = old_x[p]

        # Repeat for correlation
        self.lin_ecl = {}
        old_c = {}  # So we can restore the original value
        for p in CP:
            old_c[p] = cparams[p]
            cparams[p] = 0.0

        cparams['CONC'] = 0.0
        self.lin_ecl['NULL'] = self.eval_c_func(Fc, cparams)
        cparams['CONC'] = 1.0
        self.lin_ecl['CONC'] = self.eval_c_func(Fc, cparams) - self.lin_ecl['NULL']
        cparams['CONC'] = 0.0

        for p in CP:
            cparams[p] = 1.0
            self.lin_ecl[p] = self.eval_c_func(Fc, cparams) - self.lin_ecl['NULL']
            cparams[p] = 0.0

        # Restore parameters
        cparams['CONC'] = 1.0
        for p in CP:
            cparams[p] = old_c[p]



    def get_Ex_from_lin(self, xparams, XP):
        """
        returns total energy for the parameters stored in the parameter dictionary.
        """
        out = self.lin_exl['NULL'] + 1.0*self.lin_exl['CONX']
        for n in XP:
            out += xparams[n]*self.lin_exl[n]
        return out


    def get_Ec_from_lin(self, cparams, CP):
        """
        returns total energy for the parameters stored in the parameter dictionary.
        """
        out = self.lin_ecl['NULL'] + 1.0*self.lin_ecl['CONC']
        for n in CP:
            out += cparams[n]*self.lin_ecl[n]
        return out

    def test_linear_energy_eval(self, Fx, Fc, xparams, cparams):
        """
        Test using the default parameters for SCAN2
        """
        for p in ['CON', 'CA', 'CB', 'CC', 'CD']:
            xparams[p+'X'] = SCAN2.DEFAULT_X_PARAMS[p+'X']
            cparams[p+'C'] = SCAN2.DEFAULT_C_PARAMS[p+'C']

        sig_x = self.eval_x_func(Fx, xparams)
        sig_c = self.eval_c_func(Fc, cparams)

        self.make_linear_energy_components(Fx, Fc, xparams, cparams)

        lx = self.get_Ex_from_lin(xparams)
        lc = self.get_Ec_from_lin(cparams)

        print("X - RS: {:}  Linear: {:}  Integrated: {:}".format(self.rs, lx, sig_x))
        print("C - RS: {:}  Linear: {:}  Integrated: {:}".format(self.rs, lc, sig_c))

def filter_jellium_SCAN2(xparams, cparams, XP, CP, params, dens):
    """
    Load then filter xolist and colist by < SCAN errors
    """
    # Load the Jellium densities
    rs_list = [2, 3, 4, 6]
    jellies = list(map(Jelly, rs_list))
    for j in jellies:
        j.make_linear_energy_components(SCAN2.getscan_x, SCAN2.getscan_c, xparams, cparams, XP, CP)

    jellies = [None, None]+jellies[:3]+[None]+[jellies[3]]  # Pad for missing ones.

    print("First trying exchange...")

    ref_x = numpy.array([numpy.nan, numpy.nan, 2624.0, 526.0, 157.0, numpy.nan, 22.0]) # <- TD-DFT X

    print("Jellium reference data  : ", Settings.JELLIUM_REF_DATA)
    if Settings.JELLIUM_REF_DATA == "TD-DFT":
        # TD-DFT REFERENCE DATA
        ref_x = numpy.array([numpy.nan, numpy.nan, 2624.0, 526.0, 157.0, numpy.nan, 22.0])
        ref_xc = numpy.array([numpy.nan, numpy.nan, 3466.0, 797.0, 278.0, numpy.nan, 58.0])
    elif Settings.JELLIUM_REF_DATA == "QMC":
        # QMC REFERENCE DATA
        ref_x = numpy.array([numpy.nan, numpy.nan, 2624.0, 526.0, 157.0, numpy.nan, 22.0]) # <- TD-DFT X
        ref_xc = numpy.array([numpy.nan, numpy.nan, 3392.0, 768.0, 261.0, numpy.nan, 53.0])

    ref_c = ref_xc - ref_x

    success_list_x = []
    best_err = 1e20
    for trial in tqdm(params, ascii=True, disable=Settings.NO_PROG):
        # Restore parameters from the successful exchange trial
        for i, n in enumerate(XP):
            xparams[n] = trial[i]

        # now compute the other Jellium surface errors
        jerr_xs = []
        jxs = []
        for rs in [2, 3, 4, 6]:
            sigma_x = jellies[rs].get_Ex_from_lin(xparams, XP)
            jxs.append(sigma_x)
            jerr_xs.append(100*(ref_x[rs] - sigma_x)/ref_x[rs])

        rmse_x = numpy.sqrt(numpy.mean(numpy.array(jerr_xs)**2))

        if rmse_x < abs(best_err):
            best_x = {'B1X':xparams['B1X'], 'jxs':jxs, 'jerr_x':jerr_xs}
            for n in XP:
                best_x[n] = xparams[n]
            best_err = rmse_x

        if rmse_x <= 2.845151973:  # This is PBEsol exchange value!
        # if jerr_xs[2] > -10.0 and jerr_xs[2] <= 0.0:  # These were the original error bounds, it only cared about RS = 4
            # Record the results.
            tmp = {
                'K1':xparams['K1'],
                'jerr_x_rs4':jerr_xs[2],  # Duplicant from list but useful to not have to remember indices
                'jerr_x':jerr_xs,
                'jxs':jxs,
                # 'exl':trial['exl'],
                # 'nsp_exl':trial['nsp_exl'],
                # 'ha_exl':trial['ha_exl']
                }
            for n in XP:
                tmp[n] = xparams[n]
            success_list_x.append(tmp)

    print("Found {:} successful candidates".format(len(success_list_x)))
    print("Best RS4 error for X was {:.3f}%".format(best_x['jerr_x'][2]))

    print("\nNow do correlation...")
    success_list_c = []
    best_err = 1e20
    for trial in tqdm(params, ascii=True, disable=Settings.NO_PROG):
        for i, n in enumerate(CP):
            cparams[n] = trial[i]

        jerr_cs = []
        jcs = []
        for rs in [2, 3, 4, 6]:
            sigma_c = jellies[rs].get_Ec_from_lin(cparams, CP)
            jcs.append(sigma_c)
            jerr_cs.append(100*(ref_c[rs] - sigma_c)/ref_c[rs])

        rmse_c = numpy.sqrt(numpy.mean(numpy.array(jerr_cs)**2))
        if rmse_c < abs(best_err):
            best_c = {'jcs': jcs, 'jerr_c':jerr_cs}
            for n in CP:
                best_c[n] = cparams[n]
            best_err = rmse_c
        # if abs(jerr_cs[2]) < abs(best_err):
        #     best_c = {'CAC':cparams['CAC'], 'CBC':cparams['CBC'], 'CCC':cparams['CCC'], 'jcs': jcs, 'jerr_c':jerr_cs}
        #     best_err = jerr_cs[2]

        if rmse_c < 5.0:  # WARNING: Kinda arbitrary error tolerance.
        # if jerr_cs[2] > -10.0 and jerr_cs[2] <= 0.0:  # Old bounds on RS 4
            # This one made the cut. Record the results.
            tmp = {
                'jerr_c_rs4':jerr_cs[2],  # Duplicant from list but useful to not have to remember indices
                'jerr_c':jerr_cs,
                'jcs': jcs,
                # 'ecl':trial['ecl'],
                # 'nsp_ecl':trial['nsp_ecl'],
                # 'ha_ecl':trial['ha_ecl']
                }
            for n in CP:
                tmp[n] = cparams[n]

            success_list_c.append(tmp)

    print("Found {:} successful candidates".format(len(success_list_c)))
    print("Best error RS4 for C was {:.3f}%".format(best_err))

    # To print a summary of the best, restore that one into the working dictionary
    for s in XP:
        xparams[s] = best_x[s]
    for s in CP:
        cparams[s] = best_c[s]

    # Print the summary
    print("Exchange    = A: {:}, B: {:}, C: {:}, D: {:}".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    print("Correlation = A: {:}, B: {:}, C: {:}, D: {:}".format(cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))
    print("\n    | Exchange  | Correlation | Combined")
    for i, rs in enumerate([2, 3, 4, 6]):
        jx = best_x['jxs'][i]
        jc = best_c['jcs'][i]
        jxc = 100*(jx + jc - ref_xc[rs])/ref_xc[rs]
        print("RS{:} | {:+8.3f}% |   {:+8.3f}% | {:+8.3f}%".format(rs, best_x['jerr_x'][i], best_c['jerr_c'][i], jxc))

    # It's nice to know how these parameters did for the spherical atom, so print that.
    print("\nWith these parameters...")
    exl = get_Ex_for_atoms(dens, xparams, True)
    ecl = get_Ec_for_atoms(dens, cparams, True)
    print_report(exl, ecl)

    return success_list_x, success_list_c

def get_err_jellies_for_scan(xparams, cparams):
    """
    Simple utility function to print report for the original SCAN with current parameters
    """

    rs_list = [2, 3, 4, 6]
    jellies = list(map(Jelly, rs_list))
    jellies = [None, None]+jellies[:3]+[None]+[jellies[3]]  # Pad for missing ones.
    jerr_xs = []
    jerr_cs = []
    jerr_xcs = []
    print("\n    | Exchange  | Correlation | Combined")
    for i, rs in enumerate([2, 3, 4, 6]):
        err_xc, err_x, err_c = run_jellium(rs, xparams, cparams, jellies, False)
        print("RS{:} | {:+8.3f}% |   {:+8.3f}% | {:+8.3f}%".format(rs, err_x, err_c, err_xc))
    return

def get_err_jellies_for_scan2(xparams, cparams):
    """
    Simple utility function to print report for the original SCAN with current parameters
    """

    rs_list = [2, 3, 4, 6]
    jellies = list(map(Jelly, rs_list))
    jellies = [None, None]+jellies[:3]+[None]+[jellies[3]]  # Pad for missing ones.
    jerr_xs = []
    jerr_cs = []
    jerr_xcs = []
    print("\n    | Exchange  | Correlation | Combined")
    for i, rs in enumerate([2, 3, 4, 6]):
        err_xc, err_x, err_c = run_jellium(rs, xparams, cparams, jellies, True)
        print("RS{:} | {:+8.3f}% |   {:+8.3f}% | {:+8.3f}%".format(rs, err_x, err_c, err_xc))
    return

def get_err_jellies_for_pbesol():
    """
    As PBEsol is the benchmark exchange values, this function prints a table of its errors.
    """
    print("\nPBEsol jellium energies")
    rs_list = [2, 3, 4, 6]
    jellies = list(map(Jelly, rs_list))
    jellies = [None, None]+jellies[:3]+[None]+[jellies[3]]  # Pad for missing ones.
    jerr_xs = []
    jerr_cs = []
    jerr_xcs = []
    print("\n    | Exchange  | Correlation | Combined")
    for i, rs in enumerate([2, 3, 4, 6]):
        err_xc, err_x, err_c = run_jellium(rs, {}, {}, jellies, "PBEsol")
        print("RS{:} | {:+8.3f}% |   {:+8.3f}% | {:+8.3f}%".format(rs, err_x, err_c, err_xc))
    return


# OLD FUNCTIONS for SCAN original

def filter_jellium(xparams, cparams, colist, xolist):
    success_list = []

    # Load the Jellium densities
    rs_list = [2, 3, 4, 6]
    jellies = list(map(Jelly, rs_list))
    jellies = [None, None]+jellies[:3]+[None]+[jellies[3]]  # Pad for missing ones.

    # We use kd1count+1 to handle zero indexing
    for c_trial in colist:
        # Restore parameters from the successful correlation trial
        cparams['C1C'] = c_trial['C1C']
        cparams['C2C'] = c_trial['C2C']
        cparams['DC'] = c_trial['DC']
        for x_trial in xolist:
            # Restore parameters from the successful exchange trial
            xparams['C1X'] = x_trial['C1X']
            xparams['C2X'] = x_trial['C2X']
            xparams['DX'] = x_trial['DX']

            # now compute the other Jellium surface errors
            jerr_xs = []
            jerr_cs = []
            jerr_xcs = []
            for rs in [2, 3, 4, 6]:
                err_xc, err_x, err_c = run_jellium(rs, xparams, cparams, jellies, False)
                jerr_xs.append(err_x)
                jerr_cs.append(err_c)
                jerr_xcs.append(err_xc)

            # We only care about Rs = 4. So check that
            if jerr_xcs[2] < 5.0 and jerr_xcs[2] >= 0.0:  # WARNING: Weird bounds!
                # This one made the cut. Record the results.
                success_list.append({
                    'K1':xparams['K1'],
                    'C1X':xparams['C1X'], 'C2X':xparams['C2X'], 'DX':xparams['DX'],
                    'C1C':cparams['C1C'], 'C2C':cparams['C2C'], 'DC':cparams['DC'],
                    'jerr_rs4':jerr_xcs[2],  # Duplicant from list but useful to not have to remember indices
                    'jerr_x':jerr_xs, 'jerr_c':jerr_cs, 'jerr_xc':jerr_xcs,
                    'LZc':c_trial['error'],
                    'LZx_a':x_trial['aerror'], 'LZx_b':x_trial['berror']
                    })
    return success_list

def run_jellium_x(rs, xparams, jellies, ref_x, do_SCAN2):
    if do_SCAN2:
        sigma_x = jelly.eval_x_func(SCAN2.getscan_x, xparams)
    else:
        sigma_x = jelly.eval_x_func(SCAN.getscan_x, xparams)

    return sigma_x

def run_jellium_c(rs, cparams, jellies, ref_c, do_SCAN2):
    jelly = jellies[rs]

    if do_SCAN2:
        sigma_c = jelly.eval_c_func(SCAN2.getscan_c, cparams)
    else:
        sigma_c = jelly.eval_c_func(SCAN.getscan_c, cparams)

    return err_c

def run_jellium(rs, xparams, cparams, jellies, do_SCAN2):
    # Warning! Mild discrepency between me and fortran
    # Calculate bulk density, Fermi momentum and Fermi Energy
    # Now draw the rest of the owl...

    REF_DATA = "QMC"

    # Some entries are missing so we can use the rs number directly, i.e. 2,3,4,6
    if REF_DATA == "TD-DFT":
        # TD-DFT REFERENCE DATA
        ref_x = numpy.array([numpy.nan, numpy.nan, 2624.0, 526.0, 157.0, numpy.nan, 22.0])
        ref_xc = numpy.array([numpy.nan, numpy.nan, 3466.0, 797.0, 278.0, numpy.nan, 58.0])
    elif REF_DATA == "QMC":
        # QMC REFERENCE DATA
        ref_x = numpy.array([numpy.nan, numpy.nan, 2624.0, 526.0, 157.0, numpy.nan, 22.0]) # <- TD-DFT X
        ref_xc = numpy.array([numpy.nan, numpy.nan, 3392.0, 768.0, 261.0, numpy.nan, 53.0])

    # Generate the correlation energy reference
    ref_c = ref_xc - ref_x
    jelly = jellies[rs]

    if do_SCAN2 == "PBEsol":
        sigma_x, sigma_c = jelly.eval_func(PBEsol.getPBEsol_x, PBEsol.getPBEsol_c, {}, {})
    elif do_SCAN2:
        sigma_x, sigma_c = jelly.eval_func(SCAN2.getscan_x, SCAN2.getscan_c, xparams, cparams)
    else:
        sigma_x, sigma_c = jelly.eval_func(SCAN.getscan_x, SCAN.getscan_c, xparams, cparams)

    err_xc = 100*(sigma_x + sigma_c - ref_xc[rs])/ref_xc[rs]
    err_x = 100*(sigma_x - ref_x[rs])/ref_x[rs]
    err_c = 100*(sigma_c - ref_c[rs])/ref_c[rs]

    return err_xc, err_x, err_c
