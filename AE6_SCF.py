from pyscf import gto,dft
from os import path,system,sys
import yaml

base_dir = './ref_geom/AE6/'
res_dir = './AE6_scf_results'

if not path.isdir(res_dir):
    system('mkdir '+res_dir)

"""
    geometries from Peverati, R.; Truhlar, D. G. J. Chem. Phys. 2011, 135, 191102.
    https://comp.chem.umn.edu/db/dbs/mgae109.html
"""
mols = {'SiH4':'SiH4', 'SiO':'SiO', 'S2':'S2', 'C3H4': 'C3H4_pro',
    'C2H2O2': 'HCOCOH', 'C4H8': 'C4H8_cyc'}
nsys = 6
atoms = ['H','C','O','Si','S']
urest = ['S2','H','C','O','Si','S']
syslist = atoms + [sys for sys in mols]

levelshift_d = {
    'Si': {'LSDA': 0.1}
}

kcal_to_kJ = 4.184
eH_to_eV = 27.211386245988
eV_to_kcalmol = 23.06054783061903
eH_to_kcalmol = eH_to_eV*eV_to_kcalmol

"""
LT03:
    Original reference data (in kcal/mol) from Table 1 of
    B.J. Lynch and D.G. Truhlar, J. Phys. Chem. Lett. A 107, 8996 (2003),
    doi: 10.1021/jp035287b,
    and correction, ibid. 108, 1460 (2004),
    doi: 10.1021/jp0379190

HK12_NREL:
    Reference values (in kJ/mol) are the nonrelativistic (NREL) values and
    nonrelativistic frozen-core (NREL_FC) values from Table 4 of:
    R. Haunschild and W. Klopper, Theor. Chem. Acc. 131, 1112 (2012),
    doi:10.1007/s00214-012-1112-3

PT11:
    Reference values (in kcal/mol) from the MGAE109/11 database of
    R. Peverati and D.G. Truhlar, J. Chem. Phys. 135, 191102 (2011)
    doi:10.1063/1.3663871
    Not the best estimate values, which include spin-orbit coupling

"""

benchmarks = {

    'LT03': {'SiH4': 322.40, 'SiO': 192.08, 'S2': 101.67, 'C3H4': 704.79,
        'C2H2O2': 633.35, 'C4H8': 1149.01},

    'HK12_NREL': {'SiH4': 1358.1, 'SiO': 809.2, 'S2': 434.7, 'C3H4': 2950.9,
        'C2H2O2': 2658.2, 'C4H8': 4812.1},

    'PT11': {'SiH4': 324.95, 'SiO': 193.06, 'S2': 104.25, 'C3H4': 705.06,
        'C2H2O2': 633.99, 'C4H8': 1149.37},
}
benchmarks['HK12_NREL'] = {akey: benchmarks['HK12_NREL'][akey]/kcal_to_kJ for akey in  benchmarks['HK12_NREL'] }

def molecule_parser(mol):

    atoms = {}
    tstr = ''
    nstr = ''
    nchar = len(mol)-1
    last_was_num = False
    for ichar,achar in enumerate(mol):
        if ichar > 0:
            if achar.isupper() and not last_was_num:
                atoms[tstr] = 1
                tstr = ''
            elif achar.isupper() and last_was_num:
                atoms[tstr] = int(float(nstr))
                nstr = ''
                tstr = ''
                last_was_num = False
            elif achar.isnumeric():
                last_was_num = True
                nstr += achar

        if not achar.isnumeric():
            tstr += achar
    if achar.isnumeric():
        atoms[tstr] = int(float(nstr))
    else:
        atoms[tstr] = 1

    return atoms

def get_nrlmol_bas(mol):
    atd = molecule_parser(mol)
    basd = {}
    for at in atd:
        basd[at] = gto.load('./dfo-nrlmol.dat',symb=at)
    return basd

def scf_cycle(sys, dfa, opts={}):

    def_opts = dict(bas='6-311++G(3df,3pd)', tol=1.e-7, max_cycle = 500,
        gridsize = 9, sym = False, lcart = False)

    for opt in opts:
        if opt == 'bas' and opts['bas'].lower() == 'nrlmol':
            def_opts['bas'] = get_nrlmol_bas(sys)
        else:
            def_opts[opt] = opts[opt]

    sspin = 0
    if sys in ['H']:
        sspin = 1
    elif sys in ['Si','C','O','S','S2']:
        sspin = 2

    if sys in mols:
        astr = base_dir+mols[sys]+'.xyz'
    elif sys in atoms:
        astr = '{:} 0 0 0'.format(sys)
    srep = gto.M(atom=astr, basis=def_opts['bas'], symmetry=def_opts['sym'],\
        spin=sspin, cart=def_opts['lcart'])

    if sys in urest:
        wrep = dft.UKS(srep)
    else:
        wrep = dft.RKS(srep)

    wrep.max_cycle = def_opts['max_cycle']
    wrep.conv_tol=def_opts['tol']
    wrep.grids.level = def_opts['gridsize']
    #wrep.radi_method = dft.gauss_chebyshev
    #wrep.grids.prune = None
    dft.xcfun.define_xc_(wrep._numint,dfa)
    #dft.libxc.define_xc_(wrep._numint,dfa)
    wrep.verbose = 1
    if 'lshift' in def_opts:
        wrep.level_shift = def_opts['lshift']

    econv = wrep.kernel()
    #print(sys,econv)
    if not wrep.converged:
        print('WARNING: system {:} using DFA {:} not converged'.format(sys,dfa))

    return econv

def run_ae6(dfa):

    aes = {}
    for sys in syslist:
        uopts = {}#{'bas':'nrlmol'}
        if sys in levelshift_d:
            if dfa in levelshift_d[sys]:
                uopts['lshift'] = levelshift_d[sys][dfa]
        aes[sys] = scf_cycle(sys,xcfun_imp[dfa],opts=uopts)

    aes = analysis(dfa=dfa,en_d=aes)

    return


def analysis(dfa=None,en_d={}):

    if len(en_d.keys())==0:
        en_d = yaml.load(open(res_dir+'/AE6_scf_{:}.yaml'.format(dfa),'r'), \
            Loader=yaml.Loader)['SP']

    for refset in benchmarks:
        en_d['MAE_'+refset] = 0.0
        en_d['ME_'+refset] = 0.0
    for sys in mols:
        cons = molecule_parser(sys)
        en_d[sys+'_AE'] = -en_d[sys]
        for atom in cons:
            en_d[sys+'_AE'] += cons[atom]*en_d[atom]
        en_d[sys+'_AE'] *= eH_to_kcalmol

        for refset in benchmarks:
            err = en_d[sys+'_AE'] - benchmarks[refset][sys]
            en_d['ME_'+refset] += err/6
            en_d['MAE_'+refset] += abs(err)/6

    tstr = 'SP:\n  unit: hartree\n'
    for sys in syslist:
        tstr += '  {:}: {:}\n'.format(sys,en_d[sys])
    tstr += 'AE:\n  unit: kcal/mol\n'
    for sys in mols:
        tstr += '  {:}: {:}\n'.format(sys,en_d[sys+'_AE'])
    tstr += 'Stats:\n  unit: kcal/mol\n'
    tstr += '  ME:\n'
    for refset in benchmarks:
        tstr += '    {:}: {:}\n'.format(refset,en_d['ME_'+refset])
    tstr += '  MAE:\n'
    for refset in benchmarks:
        tstr += '    {:}: {:}\n'.format(refset,en_d['MAE_'+refset])

    with open(res_dir + '/AE6_scf_{:}.yaml'.format(dfa),'w+') as tfl:
        tfl.write(tstr)

    return en_d

if __name__ == "__main__":

    dfa_to_do = ['LSDA','PBE', 'SCAN', 'r2SCAN','ASCAN']

    xcfun_imp = {'LSDA': '1.0*SLATERX, 1.0*PW92C', 'PBE': '1.0*PBEX, 1.0*PBEC',
        'r2SCAN': '1.0*R2SCANX, 1.0*R2SCANC', 'SCAN': '1.0*SCANX, 1.0*SCANC',
        'ASCAN': 'ASCANX, ASCANC'}
    libxc_imp = {'LSDA': '1.0*LDA_X, 1.0*LDA_C_PW', 'PBE': '1.0*GGA_X_PBE, 1.0*GGA_C_PBE',
        'r2SCAN': '1.0*MGGA_X_R2SCAN, 1.0*MGGA_C_R2SCAN',
        'SCAN': '1.0*MGGA_X_SCAN, 1.0*MGGA_C_SCAN'}

    if len(sys.argv) > 1:
        if sys.argv[1] == 'analysis':
            for dfa in dfa_to_do:
                analysis(dfa=dfa,en_d={})
        else:
            print('Unknown option '+sys.argv[1])
        exit()

    for dfa in dfa_to_do:
        run_ae6(dfa)
