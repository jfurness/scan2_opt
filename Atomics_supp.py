import numpy as np
from scipy.optimize import minimize_scalar, minimize
from itertools import product
from tqdm import tqdm
from int_ex import get_Ex_for_atoms
from int_ec import get_Ec_for_atoms
import SCAN
import SCAN2
import matplotlib.pyplot as plt
import Benchmarks
from math import ceil

def check_mono_x(a, beta, beta2, beta3):
    return np.all(SCAN2.interp_gradient(beta, beta2, beta3, a[0], a[1], a[2], a[3]) <= 0)

def check_mono_c(a, beta_05, beta2_05, beta3_05, beta_10, beta2_10, beta3_10):
    #  Is there a reason for this? Surely it is equivalent to check_mono_x but less efficient? 
    da_truth1 = np.all(SCAN2.interp_gradient(beta_05, beta2_05, beta3_05, a[0], a[1], a[2], a[3]) <= 0)
    da_truth2 = np.all(SCAN2.interp_gradient(beta_10, beta2_10, beta3_10, a[0], a[1], a[2], a[3]) <= 0)
    return da_truth1 and da_truth2

def beauty_comp(a, beta_05, beta2_05, beta3_05, beta_10, beta2_10, beta3_10):
    beaut1 = np.all(SCAN2.interp_deriv2(beta_05, beta2_05, beta3_05, a[0], a[1], a[2], a[3]) >= 0)
    beaut2 = np.all(SCAN2.interp_deriv2(beta_10, beta2_10, beta3_10, a[0], a[1], a[2], a[3]) <= 0)
    """
    csign = 0
    osign = 0
    cnt = 0
    for i in range(len(beta_10)) :
        csign = int(np.sign(SCAN2.interp_deriv2(beta_10[i],beta2_10[i],beta3_10[i], a[0], a[1], a[2], a[3])))
        if abs(csign - osign) == 2 :
            cnt += 1
        osign = csign
    beaut2 = cnt <= 1
    """
    return beaut1 and beaut2

def beauty_filter(aa, beta, beta2, beta3, beta_05, beta2_05, beta3_05, beta_10, beta2_10, beta3_10):
    """
    Mess of beta arguments is necessary to prevent repeating work squaring things
    """
    return check_mono_x(aa, beta, beta2, beta3)# and beauty_comp(aa, beta_05, beta2_05, beta3_05, beta_10, beta2_10, beta3_10)

def fine_adj_gen(aa, STEP):
    intv = STEP/10.0
    for da in range(-4,5):
        for db in range(-4,5):
            for dc in range(-4,5):
                a, b, c = (aa[0] + da*intv, aa[1] + db*intv, aa[2] + dc*intv)
                d = -(a + b + c)
                yield a, b, c, d

def uniform_grid_gen(STEP):
    """
    Generator function to return test set parameters without storing entire
    list in memory.
    """
    A_MIN = -12.5
    A_MAX = 3.11709
    N_A = int(ceil((A_MAX-A_MIN)/float(STEP))) # Care for integer division!

    for a_it in range(N_A):
        B_MIN = 6*(A_MIN + a_it*STEP) - 12.0  # Majik numbers from equation poles
        B_MAX = -1.64938*(-17.9121 + 4.44275*(A_MIN + a_it*STEP))
        N_B = int(ceil((B_MAX-B_MIN)/float(STEP)))

        C_MIN = -63.4268 + 3.56471*(A_MIN + a_it*STEP)
        C_MAX = 150.066 + 20.2571*(A_MIN + a_it*STEP)
        N_C = int(ceil((C_MAX-C_MIN)/float(STEP)))
        
        """
        if (A_MIN + a_it*STEP) <= -3.22367 :
            B_MIN = -40.0
        else :
            B_MIN = 6.0*(A_MIN + a_it*STEP) - 12.0  # Majik numbers from equation poles
        B_MAX = 29.2906 - 7.18362 *(A_MIN + a_it*STEP)
        N_B = int(ceil((B_MAX-B_MIN)/float(STEP)))

        C_MIN = -40.0
        C_MAX = 142.695 + 19.3914*(A_MIN + a_it*STEP)
        N_C = int(ceil((C_MAX-C_MIN)/float(STEP)))
        """



        for b_it in range(N_B):
            for c_it in range(N_C):
                a, b, c = (A_MIN + a_it*STEP, B_MIN + b_it*STEP, C_MIN + c_it*STEP)
                d = -(a + b + c)
                # d = -(6.0 + 7.0*a + 8.0*b + 9.0*c)/10.0
                yield a, b, c, d

def fit_poly_xc_separate_for_atoms(dens, bm_x, bm_c, xparams, cparams):
    """
    Function generates a set of potential X and C IE parameters, then filters them
    by the spherical atoms.

    It starts out with exchange, then correlation.

    Currently we filter based on individually exceeding SCAN RMSE (%) for X and C separately
    but this might not be the best as we later select by combined XC.
    It is impossible to do combined at this point however as the search space is too large.

    It is not very smart, so it needs to be adapted if the IE function does not
    take CAX, CBX, CCX, CAC, CBC, and CCC for paramters.
    """

    fullist = Benchmarks.BM_LIST

    print("Generating integrated components.")
    lin_exl, lin_ecl = make_linear_energy_components(dens, xparams, cparams)

    ERR_TOL_X = 0.3222
    ERR_TOL_C = 7.1632


    # Set the bounds on the parameter space for exchange.


    # Lists to collect those that pass
    x_candidates = []
    c_candidates = []
    def check_mono(a,qe) :
        if qe == 0 :
            beta = np.arange(0, 1, 0.002)
            beta2 = beta**2
            beta3 = beta**3
            return np.all(SCAN2.interp_gradient(beta, beta2, beta3, a[0], a[1], a[2], a[3]) <= 0)

        elif qe ==1 :
            beta_05 = np.arange(0, 0.5, 0.002)
            beta2_05 = beta_05**2
            beta3_05 = beta_05**3

            beta_10 = np.arange(0.5, 1.0, 0.002)
            beta2_10 = beta_10**2
            beta3_10 = beta_10**3
            da_truth1 = np.all(SCAN2.interp_gradient(beta_05, beta2_05, beta3_05, a[0], a[1], a[2], a[3]) <= 0)
            da_truth2 = np.all(SCAN2.interp_gradient(beta_10, beta2_10, beta3_10, a[0], a[1], a[2], a[3]) <= 0)
            return da_truth1 and da_truth2

    def beauty_comp(a):
        beta_05 = np.arange(0, 0.5, 0.002)
        beta2_05 = beta_05**2
        beta3_05 = beta_05**3

        beta_10 = np.arange(0.5, 1.0, 0.002)
        beta2_10 = beta_10**2
        beta3_10 = beta_10**3
        beaut1 = np.all(SCAN2.interp_deriv2(beta_05, beta2_05, beta3_05, a[0], a[1], a[2], a[3]) >= 0)
        beaut2 = np.all(SCAN2.interp_deriv2(beta_10, beta2_10, beta3_10, a[0], a[1], a[2], a[3]) <= 0)
        return beaut1 and beaut2

    def get_rms(a,detail,q):
        if q == 0 :
            xparams['CAX'] = a[0]
            xparams['CBX'] = a[1]
            xparams['CCX'] = a[2]
            xparams['CDX'] = a[3]

            exl = get_Ex_from_lin(lin_exl, xparams)
            err_x = 100*(bm_x - exl)/np.abs(bm_x)
            rms_x = np.sqrt(np.mean(np.array(err_x)**2))
            if detail == 0:
                return rms_x
            elif detail ==1:
                return [exl,err_x,rms_x]
        elif q == 1:
            cparams['CAC'] = a[0]
            cparams['CBC'] = a[1]
            cparams['CCC'] = a[2]
            cparams['CDC'] = a[3]

            ecl = get_Ec_from_lin(lin_ecl, cparams)
            err_c = 100*(bm_c - ecl)/np.abs(bm_c)
            rms_c = np.sqrt(np.mean(np.array(err_c)**2))
            if detail == 0 :
                return rms_c
            elif detail == 1 :
                return [ecl,err_c,rms_c]

    def unif_grid(par_min,par_max) :
        best_x = {}
        step = 1
        nstep = [int(ceil((par_max[i] - par_min[i])/step)) for i in range(3)]
        tmp = []
        ref_x = []
        ref_c = []
        info = []
        err_min = 1e20
        def filter(aa,q_e) :
            return check_mono(aa,q_e) and beauty_comp(aa)
        def fine_adj(aa,q) :
            if q == 0:
                tol = ERR_TOL_X
            elif q == 1:
                tol = ERR_TOL_C
            best_a = []
            best_a.append(aa)
            intv = step/10.0
            comp = get_rms(aa,0,q)
            for l in range(-4,5) :
                for m in range(-4,5) :
                    for n in range(-4,5) :
                        ab = [aa[0] + l*intv, aa[1] + m*intv, aa[2] + n*intv,0.0]
                        ab[3] = -(ab[0] + ab[1] + ab[2])
                        #ab[2] = -(-1 + ab[0] + ab[1] + ab[2])
                        #ab[3] = 0.0
                        tvar = get_rms(ab,0,q)
                        if abs(tvar) < tol :
                            best_a.append(ab)
            return best_a
        print("{:} total parameter sets".format(nstep[0]*nstep[1]*nstep[2]))
        for q in range(2):
            err_min = 1e20
            if q == 0:
                print("Testing with atomic exchange energies...")
            elif q == 1:
                print("Found {:} candidates. Best RMSE for exchange was: {:}%".format(len(ref_x), best_x['err_x']))
                print('best set {:},{:},{:},{:}'.format(best_x['CAX'],best_x['CBX'],best_x['CCX'],best_x['CDX']))
                print("Now Doing Correlation Parameters...")
                print("Testing with spherical atom correlation energies...")
            for i in tqdm(list(range(nstep[0]))) :

                b_max = -1.64938*(-17.9121 + 4.44275*(par_min[0] + i*step))
                b_min = 6*(par_min[0] + i*step) - 12.0
                nstep[1] = int(ceil((b_max - b_min)/step))
                c_min = -63.4268 + 3.56471*(par_min[0] + i*step)
                c_max = 150.066 + 20.2571*(par_min[0] + i*step)
                nstep[2] = int(ceil((c_max - c_min)/step))

                for j in range(nstep[1]) :
                    for k in range(nstep[2]) :
                        tmp.append([par_min[0] + i*step, b_min + j*step, c_min + k*step, 0.0])
                        #tmp.append([par_min[0] + i*step, par_min[1] + j*step, par_min[2] + k*step, 0.0])
                        if len(tmp) >= 10**3 :
                            for x in tmp :
                                info = []
                                a, b, c, d = x
                                d = -(a + b + c)
                                #d = -(-1 + c + a + b)
                                if list(filter([a,b,c,d],q)) :
                                    bet = []
                                    if q == 0:
                                        bet = fine_adj([a,b,c,d],q)
                                        for u in bet :
                                            info = get_rms(u,1,q)
                                            if abs(info[2]) < abs(err_min) :
                                                best_x = {'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'],
                                                'CDX':xparams['CDX'], 'B1X':xparams['B1X'], 'err_x':info[2]}
                                                err_min = info[2]
                                            if abs(info[2]) < ERR_TOL_X :
                                                ref_x.append({'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'],
                                                'CDX':xparams['CDX'],'B1X':xparams['B1X'], 'err_x':info[2], 'exl':info[0]})
                                    elif q == 1:
                                        bet = fine_adj([a,b,c,d],q)
                                        for u in bet :
                                            info = get_rms(u,1,q)
                                            if abs(info[2]) < abs(err_min):
                                                best_c = {'CAC':cparams['CAC'], 'CBC':cparams['CBC'], 'CCC':cparams['CCC'],
                                                'CDC':cparams['CDC'], 'err_c':info[2]}
                                                err_min = info[2]
                                            if abs(info[2]) < ERR_TOL_C:
                                                ref_c.append({'CAC':cparams['CAC'], 'CBC':cparams['CBC'], 'CCC':cparams['CCC'],
                                                'CDC':cparams['CDC'], 'err_c':info[2], 'ecl':info[0]})

                            tmp = []
        print("Found {:} candidates!".format(len(ref_c)))
        return ref_x,ref_c

    x_candidates, c_candidates = unif_grid([-12.5,-40.0,-40.0],[3.11709,40.0,40.0])
    #x_candidates, c_candidates = unif_grid([-6,-40,-20],[6.0,40.0,20])

    # Sort them by ascending X RMSE(%)
    x_candidates = sorted(x_candidates, key=lambda x: x['err_x'])
    with open("IE_x_{:}.csv".format(xparams['K1']), 'w') as out:
        out.write("CAX,CBX,CCX,CDX,B1x,RMSE_X\n")
        for x in x_candidates:
            out.write("{:},{:},{:},{:},{:}\n".format(x['CAX'], x['CBX'], x['CCX'], x['B1X'], x['err_x']))

    c_candidates = sorted(c_candidates, key=lambda x: x['err_c'])
    with open("IE_c_{:}.csv".format(xparams['K1']), 'w') as out:
        out.write("CAC,CBC,CCC,CDC,RMSE_C\n")
        for c in c_candidates:
            out.write("{:},{:},{:},{:}\n".format(c['CAC'], c['CBC'], c['CCC'], c['err_c']))

    # Return the successful lists
    return x_candidates, c_candidates

def get_err_xc_for_scan(dens, xparams, cparams, num=False):
    """
    Convenience function that gets the SCAN errors for the current parameter set
    """
    exl = get_Ex_for_atoms(dens, xparams, False)
    ecl = get_Ec_for_atoms(dens, cparams, False)
    print_report(exl, ecl, num=num)
    return

def get_err_xc_for_scan2(dens, xparams, cparams, num=False):
    """
    Convenience function that gets the SCAN2 errors for the current parameter set
    """
    exl = get_Ex_for_atoms(dens, xparams, True)
    ecl = get_Ec_for_atoms(dens, cparams, True)
    print_report(exl, ecl, num=num)
    return

def print_report(exl, ecl, num=False):
    """
    Utility function that prints a table showing individual and RMSE % errors for given list of
    exchange and correlation energies.
    """

    bm_x = Benchmarks.BM_X

    bm_c = Benchmarks.BM_C

    bm_xc = Benchmarks.BM_XC

    num_x = [exl[i] - bm_x[i] for i in range(len(exl))]
    err_x = [100*(exl[i] - bm_x[i])/np.abs(bm_x[i]) for i in range(len(exl))]
    rms_x = np.sqrt(np.mean(np.array(err_x)**2))

    num_c = [ecl[i] - bm_c[i] for i in range(len(ecl))]
    err_c = [100*(ecl[i] - bm_c[i])/np.abs(bm_c[i]) for i in range(len(ecl))]
    rms_c = np.sqrt(np.mean(np.array(err_c)**2))

    num_xc = [ecl[i] + exl[i] - bm_xc[i] for i in range(len(ecl))]
    err_xc = [100*(ecl[i] + exl[i] - bm_xc[i])/np.abs(bm_xc[i]) for i in range(len(ecl))]
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

    print("\n    | Exchange  | Correlation | Combined ")
    fullist = Benchmarks.BM_LIST
    for i in range(len(fullist)):
        if num:
            print("{:<4}| {:+8.4f}  |   {:+8.4f}  | {:+8.4f} Eh".format(fullist[i], num_x[i], num_c[i], num_xc[i]))
        else:
            print("{:<4}| {:+8.4f}% |   {:+8.4f}% | {:+8.4f}%".format(fullist[i], err_x[i], err_c[i], err_xc[i]))
    print("_________________________________________")
    print("RMSE| {:+8.4f}% |   {:+8.4f}% | {:+8.4f}%\n".format(rms_x, rms_c, rms_xc))
    return

def make_linear_energy_components(dens, xparams, cparams):
    """
    Function to return the integrated components for IE functions in which
    parameters enter linearly.

    An extra 'NULL' entry is included for the portion that is separate from the IE parameters

    The two dicts returned can be supplied as is to the get_E(x/c)_from_lin functions to evaluate
    energy with given parameters by simple multiplication.
    """
    XP = ["CONX", "CAX", "CBX", "CCX","CDX"]
    lin_exl = {}
    old_x = {}
    for p in XP:
        old_x[p] = xparams[p]
        xparams[p] = 0.0

    lin_exl['NULL'] = get_Ex_for_atoms(dens, xparams, True)

    for p in XP:
        xparams[p] = 1.0
        lin_exl[p] = get_Ex_for_atoms(dens, xparams, True) - lin_exl['NULL']
        xparams[p] = 0.0

    for p in XP:
        xparams[p] = old_x[p]

    # Repeat for correlation
    CP = ["CONC", "CAC", "CBC", "CCC","CDC"]
    lin_ecl = {}
    old_c = {}  # So we can restore the original value
    for p in CP:
        old_c[p] = cparams[p]
        cparams[p] = 0.0

    lin_ecl['NULL'] = get_Ec_for_atoms(dens, cparams, True)

    for p in CP:
        cparams[p] = 1.0
        lin_ecl[p] = get_Ec_for_atoms(dens, cparams, True) - lin_ecl['NULL']
        cparams[p] = 0.0

    # Restore parameters
    for p in CP:
        cparams[p] = old_c[p]


    return lin_exl, lin_ecl

def get_Ex_from_lin(lin_exl, xparams):
    """
    Takes 1st list of integrated quantities from make_linear_energy_components and
    returns total energy for the parameters stored in the parameter dictionary.
    """
    return lin_exl['NULL'] + xparams['CONX']*lin_exl['CONX'] + xparams['CAX']*lin_exl['CAX'] + xparams['CBX']*lin_exl['CBX'] + xparams['CCX']*lin_exl['CCX'] + xparams['CDX']*lin_exl['CDX']

def get_Ec_from_lin(lin_ecl, cparams):
    """
    Takes 2nd list of integrated quantities from make_linear_energy_components and
    returns total energy for the parameters stored in the parameter dictionary.
    """
    return lin_ecl['NULL'] + cparams['CONC']*lin_ecl['CONC'] + cparams['CAC']*lin_ecl['CAC'] + cparams['CBC']*lin_ecl['CBC'] + cparams['CCC']*lin_ecl['CCC'] + cparams['CDC']*lin_ecl['CDC']

def test_linear_composition(dens, xparams, cparams):
    """
    Simple unit test to validate linear construction of energy from IE function parameters.lin_exl

    Test is such that linear version must match that calculated as a full integral of the energy
    density in the traditional way.

    1st tests each component individually, then does mixed for the default parameters.
    """
    lin_exl, lin_ecl = make_linear_energy_components(dens, xparams, cparams)
    print("PRE")
    print("CONX {:}, CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(xparams['CONX'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    print("CONC {:}, CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(cparams['CONC'], cparams['CAC'], cparams['CBC'], cparams['CCC'], cparams['CDC']))

    for p in ['CON', 'CA', 'CB', 'CC']:

        xparams[p+'X'] = 3.0
        cparams[p+'C'] = 3.0

        fresh_x = get_Ex_for_atoms(dens, xparams, True)
        fresh_c = get_Ec_for_atoms(dens, cparams, True)

        print("\nIN RC for "+p+": CONX {:}, CAX {:}, CBX {:}, CCX {:}".format(xparams['CONX'], xparams['CAX'], xparams['CBX'], xparams['CCX']))
        print("IN RC for "+p+": CONC {:}, CAC {:}, CBC {:}, CCC {:}".format(xparams['CONX'], xparams['CAX'], xparams['CBX'], xparams['CCX']))
        rc_ex = get_Ex_from_lin(lin_exl, xparams)
        rc_ec = get_Ec_from_lin(lin_ecl, cparams)

        print("      |     Ex lin    |    Ex full    |     Ec lin    |    Ec full   ")
        for i, a in enumerate(Benchmarks.BM_LIST):
            print("{:4}  | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} ".format(a, rc_ex[i], fresh_x[i], rc_ec[i], fresh_c[i]))

        xparams[p+'X'] = 0.0
        cparams[p+'C'] = 0.0

    # Just for confidence, lets test mixed
    for p in ['CON', 'CA', 'CB', 'CC']:
        xparams[p+'X'] = SCAN2.DEFAULT_X_PARAMS[p+'X']
        cparams[p+'C'] = SCAN2.DEFAULT_C_PARAMS[p+'C']

    fresh_x = get_Ex_for_atoms(dens, xparams, True)
    fresh_c = get_Ec_for_atoms(dens, cparams, True)

    print("\nCONX {:}, CAX {:}, CBX {:}, CCX {:}".format(xparams['CONX'], xparams['CAX'], xparams['CBX'], xparams['CCX']))
    print("CONC {:}, CAC {:}, CBC {:}, CCC {:}".format(cparams['CONC'], cparams['CAC'], cparams['CBC'], cparams['CCC']))
    rc_ex = lin_exl['NULL'] + xparams['CONX']*lin_exl['CONX'] + xparams['CAX']*lin_exl['CAX'] + xparams['CBX']*lin_exl['CBX'] + xparams['CCX']*lin_exl['CCX']
    rc_ec = lin_ecl['NULL'] + cparams['CONC']*lin_ecl['CONC'] + cparams['CAC']*lin_ecl['CAC'] + cparams['CBC']*lin_ecl['CBC'] + cparams['CCC']*lin_ecl['CCC']
    print("      |     Ex lin    |    Ex full    |     Ec lin    |    Ec full   ")
    for i, a in enumerate(Benchmarks.BM_LIST):
        print("{:4}  | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} | {:>+13.8f} ".format(a, rc_ex[i], fresh_x[i], rc_ec[i], fresh_c[i]))


#####################################################
# Below this is old functions for different fittings
# Mostly this can be ignored.
#####################################################


def filter_rare_gas_energies(xparams, cparams, dens, candidates):
    olist = []
    for trial in candidates:
        res = run_rare_gas_energies(xparams, cparams, dens, trial)
        olist.append(res)

    return olist


def run_rare_gas_energies(xparams, cparams, dens, trial):
    # Find XC energy for He, Ne, Ar, Kr and Xe
    cparams['C1C'] = trial['C1C']
    cparams['C2C'] = trial['C2C']
    cparams['DC'] = trial['DC']
    xparams['C1X'] = trial['C1X']
    xparams['C2X'] = trial['C2X']
    xparams['DX'] = trial['DX']

    exlist = get_Ex_for_atoms(dens, xparams)
    eclist = get_Ec_for_atoms(dens, cparams)
    exclist = exlist + eclist

    ref_x = np.array([-1.0260000228881836, -12.107999801635742, -30.187999725341797, -93.889999389648438, -179.19999694824219])
    ref_c = np.array([-4.1999999433755875E-2, -0.39100000262260437, -0.72299998998641968, -1.8500000238418579, -3.0000000000000000])
    ref_xc = np.array([-1.0680000223219395, -12.498999804258347, -30.910999715328217, -95.739999413490295, -182.19999694824219])

    err_x = 100.0*(exlist - ref_x)/abs(ref_x)
    err_c = 100.0*(eclist - ref_c)/abs(ref_c)
    err_xc = 100.0*(exclist - ref_xc)/abs(ref_xc)

    names = ['He', 'Ne', 'Ar', 'Kr', 'Xe']
    for i in range(len(err_x)):
        trial['RG_'+names[i]+'_x'] = err_x[i]
        trial['RG_'+names[i]+'_c'] = err_c[i]
        trial['RG_'+names[i]+'_xc'] = err_xc[i]
    trial['RG_ME_x'] = np.mean(ref_x - exlist),
    trial['RG_ME_c'] = np.mean(ref_c - eclist),
    trial['RG_ME_xc'] = np.mean(ref_xc - exclist),
    trial['RG_MAE_x'] = np.mean(np.abs(ref_x - exlist)),
    trial['RG_MAE_c'] = np.mean(np.abs(ref_c - eclist)),
    trial['RG_MAE_xc'] = np.mean(np.abs(ref_xc - exclist)),
    trial['RG_MRE_x'] = np.mean(err_x),
    trial['RG_MRE_c'] = np.mean(err_c),
    trial['RG_MRE_xc'] = np.mean(err_xc),
    trial['RG_MARE_x'] = np.mean(np.abs(err_x)),
    trial['RG_MARE_c'] = np.mean(np.abs(err_c)),
    trial['RG_MARE_xc'] = np.mean(np.abs(err_xc)),
    trial['RG_raw_x'] = exlist,
    trial['RG_raw_c'] = eclist,
    trial['RG_raw_xc'] = exclist,
    trial['RG_err_x'] = err_x,
    trial['RG_err_c'] = err_c,
    trial['RG_err_xc'] = err_xc
    return trial


def filter_N_energy(xparams, cparams, N_dens, candidates, do_SCAN2):
    olist = []
    for trial in candidates:
        res = check_N_energy(xparams, cparams, N_dens, trial, do_SCAN2)
        olist.append(res)
    return olist


def check_N_energy(xparams, cparams, N_dens, trial, do_SCAN2):
    cparams['C1C'] = trial['C1C']
    cparams['C2C'] = trial['C2C']
    cparams['DC'] = trial['DC']
    xparams['C1X'] = trial['C1X']
    xparams['C2X'] = trial['C2X']
    xparams['DX'] = trial['DX']
    ex = get_Ex_for_atoms(N_dens, xparams, do_SCAN2)[0]  # returns single element list
    ec = get_Ec_for_atoms(N_dens, cparams, do_SCAN2)[0]

    refx = -6.597
    refc = -0.188
    refxc = refx + refc

    trial['N_err_xc'] = 100.0*(ex+ec - refxc)/abs(refxc),
    trial['N_err_x'] = 100.0*(ex - refx)/abs(refx),
    trial['N_err_c'] = 100.0*(ec - refc)/abs(refc),

    return trial


def set_b2c(cparams, xparams, He_dens, do_SCAN2):
    # print "\nSetting b2c..."
    # E_x is constant throughout optimisation so pre
    ex = get_Ex_for_atoms(He_dens, xparams, do_SCAN2)[0]
    # print "He X:", ex
    # cparams['B2C'] = 0.0889
    # ec = get_Ec_for_atoms(He_dens, cparams, do_SCAN2)[0]
    # err = abs(ex + ec + 1.068)
    # print "Running b2c = 0.0889, err: {:.8e}\n".format(err)

    for b2c in np.linspace(0.07, 0.11, 30):
        wrap_get_Ec(b2c, cparams, He_dens, ex, do_SCAN2)
    res = minimize_scalar(wrap_get_Ec, args=(cparams, He_dens, ex, do_SCAN2), tol=1e-4, bracket=(0.07,0.09))

    # print "\nFinal b2c found: {:}".format(res.x)
    # print "\nFinal b2c found: {:}".format(abs(res.x))
    # print "Final error:       {:}".format(abs(res.fun))
    # print "Final error:       {:}".format(res.fun)

def wrap_get_Ec(b2c, cparams, He_dens, ex):
    cparams['B2C'] = abs(b2c)
    ec = get_Ec_for_atoms(He_dens, cparams, True)[0]
    # err = abs(100*abs(ex + ec + 1.068)/1.068)
    err = abs(ex + ec + 1.068)

    return err

def wrap_original_Ec(b2c, cparams, He_dens, ex):
    cparams['B2C'] = abs(b2c)
    ec = get_Ec_for_atoms(He_dens, cparams, False)[0]
    err = abs(ex + ec + 1.068)

    return err

def get_ex_percent_err(xparams, dens, bm):
    exl = get_Ex_for_atoms(dens, xparams, True)
    return [100*(exl[i] - bm[i])/bm[i] for i in range(len(exl))], exl

def get_ec_percent_err(cparams, dens, bm):
    ecl = get_Ec_for_atoms(dens, cparams, True)
    return [100*(ecl[i] - bm[i])/bm[i] for i in range(len(ecl))], ecl

def fit_x_for_atoms(dens, xparams):
    ERR_TOL = 0.5

    A_MAX = 6
    A_MIN = -6
    A_STEP = 0.5

    B_MIN = 60
    B_MAX = 80
    B_STEP = 0.5

    # F1X_MIN = -1
    # F1X_MAX = -0
    # F1X_STEP = 0.025

    C_MIN = 0
    C_MAX = 10
    C_STEP = 0.25

    beta = np.arange(0,1,0.001)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta2*beta2
    om2b2 = (1-2*beta)**2
    om2b3 = (1-2*beta)**3

    caxs = np.arange(A_MIN, A_MAX, A_STEP)
    cbxs = np.arange(B_MIN, B_MAX, B_STEP)
    ccxs = np.arange(C_MIN, C_MAX, C_STEP)
    # f1xs = np.arange(F1X_MIN, F1X_MAX, F1X_STEP)
    f1xs = [-1.24]
    tot = caxs.shape[0]*cbxs.shape[0]*ccxs.shape[0]
    print("combinations: ",tot)
    possible = product(caxs, cbxs, ccxs, f1xs)

    candidates = []
    mono = []
    d = {}
    for p in tqdm(possible, total=tot):
        # Numerically check monotonicity
        # ccx = -(p[2] + 1 + p[0] + p[1]) SIMPLE POLYNOMIAL!
        # ccx = -((1 + p[0] + p[1])/p[2] + 1)
        a, b, c, f1 = p
        cdx = -(a + b + c**2*f1 + 2*c*f1 + f1 + 1)/f1
        # if np.all(interp_gradient(beta, beta2, beta3, p[0], p[1], ccx) <= 0): SIMPLE POLYNOMIAL
        # if np.all(pade_grad(beta, beta2, beta4, om2b2, om2b3, p[0], p[1], ccx) <= 0):
        if np.all(SCAN2.mod_grad(beta, beta2, beta3, beta4, om2b2, om2b3, a, b, c, cdx) <= 0):
            mono.append(p)

    print("{:} monotonic combinations found".format(len(mono)))
    print("Testing with atomic exchange energies...")
    # Check the remainders for large Z exchange limit
    err_min = 1e20

    # Given in the paper as Ex/N so multiply back by N
    xparams['CAX'] = -5.5
    xparams['CBX'] = 65.5
    xparams['CCX'] = 0
    xparams['CDX'] = 48.19

    bm = Benchmarks.BM_X

    for p in tqdm(mono):
        xparams['CAX'] = p[0]
        xparams['CBX'] = p[1]
        # xparams['CCX'] = -(p[2] + 1 + p[0] + p[1]) SIMPLE POLYNOMIAL!
        # xparams['CCX'] = -((1 + xparams['CAX'] + xparams['CBX'])/p[2] + 1)
        xparams['CCX'] = p[2]
        xparams['CDX'] = -(xparams['CAX'] + xparams['CBX'] + xparams['CCX']**2*p[3] + 2*xparams['CCX']*p[3] + p[3] + 1)/p[3]
        err = get_ex_percent_err(xparams, dens, bm)
        rms_ae = np.sqrt(np.mean(np.array(err)**2))
        if rms_ae < ERR_TOL:
            candidates.append({'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'CDX':xparams['CDX'], 'err_max':rms_ae})
                # print "ACCEPTED A: {:}, B: {:}, C: {:}. Error: a = {:}%, b = {:}%.".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], err_a, err_b)
        if rms_ae < abs(err_min):
            best = {'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'CDX':xparams['CDX'], 'err_max':rms_ae}
            err_min = err[np.argmax(np.abs(err))]

    with open("IE_exchange.csv", 'w') as out:
        out.write("A,B,C,D,RMSE\n")
        for c in candidates:
            out.write("{:},{:},{:},{:},{:}\n".format(c['CAX'], c['CBX'], c['CCX'], c['CDX'], c['err_max']))

    print("Found {:} candidates!".format(len(candidates)))

    return candidates

def fit_old_x_for_atoms(dens, xparams):
    ERR_TOL = 0.1

    C1X_MIN = 0.6
    C1X_MAX = 0.7
    C1X_STEP = 0.001

    C2X_MIN = 0.6
    C2X_MAX = 0.7
    C2X_STEP = 0.01

    # F1X_MIN = -1
    # F1X_MAX = -0
    # F1X_STEP = 0.025

    DX_MIN = 1.0
    DX_MAX = 1.5
    DX_STEP = 0.025

    c1xs = np.arange(C1X_MIN, C1X_MAX, C1X_STEP)
    c2xs = np.arange(C2X_MIN, C2X_MAX, C2X_STEP)
    dxs = np.arange(DX_MIN, DX_MAX, DX_STEP)

    print(c1xs.shape[0], c2xs.shape[0], dxs.shape[0])
    tot = c1xs.shape[0]*c2xs.shape[0]*dxs.shape[0]
    print("combinations: ",tot)
    possible = product(c1xs, c2xs, dxs)

    candidates = []
    d = {}

    print("Testing with atomic exchange energies...")
    # Check the remainders for large Z exchange limit
    err_min = 1e20

    bm = Benchmarks.BM_X

    for p in tqdm(possible,total=tot):
        xparams['C1X'] = p[0]
        xparams['C2X'] = p[1]
        xparams['DX'] = p[2]
        err = get_ex_percent_err(xparams, dens, bm)

        rms_ae = np.sqrt(np.mean(np.array(err)**2))
        if rms_ae < ERR_TOL:
            candidates.append({'C1X':xparams['C1X'], 'C2X':xparams['C2X'], 'DX':xparams['DX'], 'err_max':rms_ae})
                # print "ACCEPTED A: {:}, B: {:}, C: {:}. Error: a = {:}%, b = {:}%.".format(xparams['CAX'], xparams['CBX'], xparams['CCX'], err_a, err_b)
        if rms_ae < abs(err_min):
            best = {'C1X':xparams['C1X'], 'C2X':xparams['C2X'], 'DX':xparams['DX'], 'err_max':rms_ae}
            err_min = err[np.argmax(np.abs(err))]
    print("Best C1X: {:}, C2X: {:}, DX: {:}. Error: {:}%.".format(best['C1X'], best['C2X'], best['DX'], best['err_max']))
    for s in ["C1X","C2X","DX"]:
        xparams[s] = best[s]
    exlist = get_Ex_for_atoms(dens, xparams, True)
    fullist = Benchmarks.BM_LIST
    for i in range(len(exlist)):
        print("{:<2} : {:+.4f}%".format(fullist[i], 100*(exlist[i] - bm[i])/bm[i]))
    raise SystemExit
    return candidates

def fit_poly_x_for_atoms(dens, xparams):
    ERR_TOL = 0.5

    A_MAX = 6
    A_MIN = -6
    A_STEP = 0.05

    B_MIN = 0
    B_MAX = 80
    B_STEP = 0.05

    beta = np.arange(0, 1, 0.001)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta2*beta2
    om2b2 = (1-2*beta)**2
    om2b3 = (1-2*beta)**3

    caxs = np.arange(A_MIN, A_MAX, A_STEP)
    cbxs = np.arange(B_MIN, B_MAX, B_STEP)

    f1xs = [-1.24]
    tot = caxs.shape[0]*cbxs.shape[0]

    possible = product(caxs, cbxs, f1xs)

    candidates = []
    mono = []
    d = {}
    for p in tqdm(possible, total=tot):
        # Numerically check monotonicity
        a, b, f1 = p
        ccx = -(f1 + 1 + a + b)
        if np.all(SCAN2.interp_gradient(beta, beta2, beta3, a, b, ccx) <= 0):  # SIMPLE POLYNOMIAL
            mono.append([a, b, ccx])

    print("{:} monotonic combinations found".format(len(mono)))
    print("Testing with atomic exchange energies...")
    # Check the remainders for large Z exchange limit
    err_min = 1e20

    fullist = Benchmarks.BM_LIST
    bm = Benchmarks.BM_X

    for p in tqdm(mono):
        xparams['CAX'] = p[0]
        xparams['CBX'] = p[1]
        xparams['CCX'] = p[2]

        err = get_ex_percent_err(xparams, dens, bm)
        rms_ae = np.sqrt(np.mean(np.array(err)**2))
        if rms_ae < ERR_TOL:
            candidates.append({'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'err_max':rms_ae})
            for i, a in enumerate(fullist):
                candidates[-1][a] = err[i]
        if rms_ae < abs(err_min):
            best = {'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'err_max':rms_ae}
            err_min = rms_ae

    with open("IE_exchange.csv", 'w') as out:
        out.write("A,B,C,RMSE\n")
        for c in candidates:
            out.write("{:},{:},{:},{:}\n".format(c['CAX'], c['CBX'], c['CCX'], c['err_max']))

    print("Found {:} candidates!".format(len(candidates)))

    for s in ["CAX","CBX","CCX"]:
        xparams[s] = best[s]
    exlist = get_Ex_for_atoms(dens, xparams, True)
    print("A: {:}, B: {:}, C: {:}".format(xparams['CAX'], xparams['CBX'], xparams['CCX']))
    for i in range(len(exlist)):
        print("{:<2} : {:+.4f}%".format(fullist[i], 100*(exlist[i] - bm[i])/bm[i]))
    print("RMSE:", np.sqrt(np.mean((np.array(exlist) - np.array(bm))**2)))

    return candidates

def test_poly_c_for_atoms(dens, cparams, x_passed):
    ERR_TOL = 100
    bm = Benchmarks.BM_C

    candidates = []

    fullist = Benchmarks.BM_LIST

    print("Now running correlation atoms")
    err_min = 1e20
    for p in tqdm(x_passed):
        cparams['CAC'] = p['CAX']
        cparams['CBC'] = p['CBX']
        cparams['CCC'] = p['CCX']

        err = get_ec_percent_err(cparams, dens, bm)
        rms_ae = np.sqrt(np.mean(np.array(err)**2))

        if rms_ae < ERR_TOL:
            candidates.append({'CAC':cparams['CAC'], 'CBC':cparams['CBC'], 'CCC':cparams['CCC'], 'err_max':rms_ae})

        if rms_ae < abs(err_min):
            best = {'CAC':cparams['CAC'], 'CBC':cparams['CBC'], 'CCC':cparams['CCC'], 'err_max':rms_ae}
            err_min = rms_ae

    with open("IE_xc.csv", 'w') as out:
        out.write("A,B,C,RMSE\n")
        for c in candidates:
            out.write("{:},{:},{:},{:}\n".format(c['CAC'], c['CBC'], c['CCC'], c['err_max']))

    print("Found {:} candidates!".format(len(candidates)))

    for s in ["CAC","CBC","CCC"]:
        cparams[s] = best[s]
    eclist = get_Ec_for_atoms(dens, cparams, True)
    print("A: {:}, B: {:}, C: {:}".format(cparams['CAC'], cparams['CBC'], cparams['CCC']))
    for i in range(len(eclist)):
        print("{:<2} : {:+.4f}%".format(fullist[i], 100*(eclist[i] - bm[i])/bm[i]))
    print("RMSE:", np.sqrt(np.mean((np.array(eclist) - np.array(bm))**2)))
    return candidates

def fit_poly_xc_for_atoms(dens, xparams, cparams):
    ERR_TOL = 0.1638

    A_MAX = 2
    A_MIN = -6
    A_STEP = 0.1

    B_MIN = 15
    B_MAX = 70
    B_STEP = 0.1

    F1_MAX = -1.24
    F1_MIN = -1.0/0.174
    F1_STEP = 0.1

    beta = np.arange(0, 1, 0.001)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta2*beta2
    om2b2 = (1-2*beta)**2
    om2b3 = (1-2*beta)**3
    s2 = np.logspace(-4, 4, 1000)**2
    gs2, gb = np.meshgrid(s2, beta)

    caxs = np.arange(A_MIN, A_MAX, A_STEP)
    cbxs = np.arange(B_MIN, B_MAX, B_STEP)

    # f1xs = np.arange(F1_MIN, F1_MAX, F1_STEP)
    f1xs = np.array([-(2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)])
    tot = caxs.shape[0]*cbxs.shape[0]*f1xs.shape[0]

    possible = product(f1xs, caxs, cbxs)
    candidates = []
    mono = []
    d = {}

    print("Filtering non-monotonic functions")
    for p in tqdm(possible, total=tot):
        # Numerically check monotonicity
        f1, a, b = p
        c = -(f1 + 1 + a + b)
        if np.all(SCAN2.interp_gradient(beta, beta2, beta3, a, b, c) <= 0):  # SIMPLE POLYNOMIAL
            mono.append([a, b, c, f1])

    print("{:} monotonic combinations found ({:.1f}%)".format(len(mono), 100*len(mono)/float(tot)))

    # print "Testing that F_x > 0 for beta in [0, 1], s [10^-4, 10^4]"
    # allowed = []
    # for p in tqdm(mono):
    #     xparams['CAX'] = p[0]
    #     xparams['CBX'] = p[1]
    #     xparams['CCX'] = p[2]
    #     if np.all(SCAN2.scanFx(xparams, gs2, gb) > 0.0):
    #         allowed.append(p)
    # print "{:} allowed combinations found ({:.1f}%)".format(len(allowed), 100*len(allowed)/float(len(mono)))

    print("Testing with atomic exchange-correlation energies...")
    # Check the remainders for large Z exchange limit
    err_min = 1e20

    bm_x = Benchmarks.BM_X

    bm_c = Benchmarks.BM_C

    bm_xc = Benchmarks.BM_XC

    for p in tqdm(mono):
        # if done and p[3] < lastf1:
        #     break
        xparams['CAX'] = p[0]
        xparams['CBX'] = p[1]
        xparams['CCX'] = p[2]
        cparams['CAC'] = p[0]
        cparams['CBC'] = p[1]
        cparams['CCC'] = p[2]

        exl = get_Ex_for_atoms(dens, xparams, True)
        err_x = [100*(bm_x[i] - exl[i])/bm_x[i] for i in range(len(exl))]
        rms_x = np.sqrt(np.mean(np.array(err_x)**2))

        ecl = get_Ec_for_atoms(dens, cparams, True)
        err_c = [100*(bm_c[i] - ecl[i])/bm_c[i] for i in range(len(ecl))]
        rms_c = np.sqrt(np.mean(np.array(err_c)**2))

        err_xc = [100*(bm_xc[i] - (ecl[i] + exl[i]))/bm_xc[i] for i in range(len(ecl))]
        rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))
        if abs(rms_xc) < abs(err_min):
            best = {'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'CAC':cparams['CAC'], 'CBC':cparams['CBC'], 'CCC':cparams['CCC'], 'err_x':rms_x, 'err_c':rms_c, 'err_xc':rms_xc}
            err_min = rms_xc
        if abs(rms_xc) < ERR_TOL:
            candidates.append({'CAX':xparams['CAX'], 'CBX':xparams['CBX'], 'CCX':xparams['CCX'], 'CAC':cparams['CAC'], 'CBC':cparams['CBC'], 'CCC':cparams['CCC'], 'err_x':rms_x, 'err_c':rms_c, 'err_xc':rms_xc})
            # done = True
            # lastf1 = p[3]

    with open("IE_xc_{:}.csv".format(xparams['K1']), 'w') as out:
        out.write("A,B,C,F1,X,C,XC\n")
        for c in sorted(candidates,key=lambda x: x['err_xc']):
            out.write("{:},{:},{:},{:},{:},{:},{:}\n".format(c['CAX'], c['CBX'], c['CCX'], -c['CAX'] - c['CBX'] - c['CCX'] - 1, c['err_x'], c['err_c'], c['err_xc']))


    print("Found {:} candidates!".format(len(candidates)))

    for s in ["CAX","CBX","CCX"]:
        xparams[s] = best[s]
    for s in ["CAC","CBC","CCC"]:
        cparams[s] = best[s]
    print("A: {:}, B: {:}, C: {:}".format(cparams['CAC'], cparams['CBC'], cparams['CCC']))

    exl = get_Ex_for_atoms(dens, xparams, True)
    ecl = get_Ec_for_atoms(dens, cparams, True)
    print_report(exl, ecl)

    return candidates
