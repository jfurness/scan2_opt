import numpy as np
import scipy
from Atomics import get_Ex_for_atoms, get_Ec_non_colinear, get_Ec_for_atoms
from TestSet import assess_ae6_for_scan2
from Integrators import make_linear_energy_components
import Benchmarks
import Settings

def run_min(dens, non_spherical_dens, ae6_dens, xparams, cparams, lin_params_x, lin_params_c, xc_filter):
    print("RUNMIN")

    directory = Settings.WORK_DIR
    nstr = "XisC_" if Settings.FX_is_FC else "XnoC_"
    nstr += Settings.Fint[:4]+"_"
    nstr += Settings.H1X[:4]+"_"
    nstr += Settings.GX[:4]+"_"
    nstr += Settings.CORRELATION[:4]+"_"
    if Settings.Fint == "JPLAP":
        nstr = directory+nstr+"k{:}_rx{:}_rc{:}.csv".format(xparams['K1'], xparams['FREG'], cparams['FREG'])
    elif Settings.Fint == "SMOOTH":
        nstr = directory+nstr+"k{:}_DOWNHILL.csv".format(xparams['K1'])
    elif Settings.Fint == "TWOBETA":
        nstr = directory+nstr+"k{:}_TWOBETA_xc.csv".format(xparams['K1'])
    else:
        nstr = directory+nstr+"k{:}.csv".format(xparams['K1'])
    print("Saving results to: ", nstr)

    with open(nstr, 'w') as ofile:
        tstr = ",Interpolation,fx = fc,h1x,gx,correlation,"
        tstr += "CAX,CBX,CCX,CDX,CAC,CBC,CCC,CDC,"
        tstr += "k1,b1x,b2x,b3x,b4x,a1x,b1c,b2c,b3c,"
        if Settings.Fint == "JPLAP":
            tstr += "xreg,creg,"
        elif Settings.Fint == "SMOOTH":
            tstr += "etax,etac,"
        if Settings.DO_AE6:
            tstr += "AE6 ME,AE6 MAE,"
        if Settings.DO_BH6:
            tstr += "BH6 ME,BH6 MAE,"
        tstr += "Atoms X,Atoms C,Atoms XC,"
        tstr += "NSP X,NSP C,NSP XC,"
        if Settings.DO_AR2:
            tstr += ("Ar2_{:},"*len(Benchmarks.AR2_SEPS)).format(*Benchmarks.AR2_SEPS)
        if Settings.DO_JELLIUM:
            tstr += "Jellium X,Jellium C,Jellium XC,"
        tstr += ("{:},"*len(Benchmarks.BM_LIST)).format(*Benchmarks.BM_LIST)
        tstr += ("{:},"*len(Benchmarks.NON_SPHERICAL_LIST)).format(*Benchmarks.NON_SPHERICAL_LIST)
        if Settings.DO_AE6:
            tstr += ("{:},"*len(Benchmarks.AE6_LIST)).format(*Benchmarks.AE6_LIST)
            tstr += ("{:},"*len(Benchmarks.AE6_TESTS)).format(*Benchmarks.AE6_TESTS)
        if Settings.DO_BH6:
            tstr += ("{:},"*len(Benchmarks.BH6_LIST)).format(*Benchmarks.BH6_LIST)
            tstr += ("{:},"*len(Benchmarks.BH6_TESTS)).format(*Benchmarks.BH6_TESTS)
        if Settings.DO_JELLIUM:
            tstr += ("Jellium XC {:},"*len(Settings.JELLIUM_RS_LIST)).format(*Settings.JELLIUM_RS_LIST)
        ofile.write(tstr+"\n")

        jellies = None

        start = (1.0, 0.5, 3.5, 1.0,  5.6, -1)
        # start = (1.05293016, 0.42013875, 1.99945, 1.17315768, 2.65960889, -0.5)
        # start = (1.05293033, 0.42013875, 3.04350766, 1.17315841, 2.659608995, 0.063416865)

        XP = ["CAX","CBX","CCX","CDX"]
        CP = ["CAC","CBC","CCC","CDC"]

        if Settings.Fint == "TWOBETA":
            res = scipy.optimize.minimize(objective, start, args=(dens, non_spherical_dens, ae6_dens, jellies, xparams, cparams, ofile), method="Nelder-Mead")
        else:
            res = scipy.optimize.minimize(smooth_objective, start, args=(dens, non_spherical_dens, ae6_dens, xparams, cparams, lin_params_x, lin_params_c, XP, CP, xc_filter, ofile), method="Nelder-Mead")#, options={'eps':0.0001})

        print("final:", res.x)
    raise SystemExit("Downhill")

def smooth_objective(p, dens, non_spherical_dens, ae6_dens, xparams, cparams, lin_params_x, lin_params_c, XP, CP, xc_filter, of):
    xparams['XETA'] = abs(p[0])
    cparams['CETA'] = abs(p[1])

    lin_exl, lin_ecl = make_linear_energy_components(dens, xparams, cparams, XP, CP)
    nsp_lin_exl, nsp_lin_ecl = make_linear_energy_components(non_spherical_dens, xparams, cparams, XP, CP, non_colin=True)
    ae6_lin_exl, ae6_lin_ecl = make_linear_energy_components(ae6_dens, xparams, cparams, XP, CP, non_colin=True)

    obj = xc_filter(lin_params_x, lin_params_c, xparams, cparams, XP, CP, lin_exl, lin_ecl, nsp_lin_exl, nsp_lin_ecl, ae6_lin_exl, ae6_lin_ecl, None, None, None, None, None, of)

    print("XETA: {:}, CETA: {:}, OBJ: {:}".format(xparams['XETA'], cparams['CETA'], obj))
    return obj


def objective(params, dens, nsp_dens, ae6_dens, jellies, xparams, cparams, of):
    xparams["C1X"] = abs(params[0])
    xparams["C2X"] = abs(params[1])
    xparams["DX"] = params[2]

    cparams["C1C"] = abs(params[3])
    cparams["C2C"] = abs(params[4])
    cparams["DC"] = params[5]

    exl = get_Ex_for_atoms(dens, xparams)
    err = 100*(exl - Benchmarks.BM_X)/np.abs(Benchmarks.BM_X)
    rms_x = np.sqrt(np.mean(np.array(err)**2))

    ecl = get_Ec_for_atoms(dens, cparams)
    err = 100*(ecl - Benchmarks.BM_C)/np.abs(Benchmarks.BM_C)
    rms_c = np.sqrt(np.mean(np.array(err)**2))

    excl = exl + ecl
    err_xc = 100*(excl - Benchmarks.BM_XC)/np.abs(Benchmarks.BM_XC)
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

    ae6_exl = get_Ex_for_atoms(ae6_dens, xparams)
    ae6_ecl = get_Ec_non_colinear(ae6_dens, cparams)

    # for i in zip(Benchmarks.AE6_LIST ,ae6_exl+ae6_ecl):
    #     print i
    # raise SystemExit

    ae6_err, ae6_etot = assess_ae6_for_scan2(None, None, xparams, cparams, None, None, exl=ae6_exl, ecl=ae6_ecl)
    ae6_me = np.mean(ae6_err)
    ae6_mae = np.mean(np.abs(ae6_err))

    scanp_ae6 = 100*(ae6_mae-Settings.SCAN_AE6_MAE)/Settings.SCAN_AE6_MAE

    # obj = 0.25*rms_x + 0.25*rms_c + 0.25*rms_xc + 0.25* scanp_ae6
    # obj = 0.25*rms_x + 0.025*rms_c + 0.25*rms_xc + 0.1*ae6_mae
    # obj = 0.25*max(0, rms_x-0.3222)**2 + 0.25*max(0, rms_c-5.1632)**2 + 0.25*max(0, rms_xc-0.1583)**2 + 0.25*max(0, ae6_mae-3.135)**2
    # obj = 0.25*(rms_x/0.3222)**2 + 0.25*(rms_c/5.1632)**2 + 0.25*(rms_xc/0.1583)**2 + 0.25*(ae6_mae/3.135)**2
    obj = 0.25*max(1, rms_x/0.9)**2 + 0.25*max(1, rms_c/10.1632)**2 + 0.25*(rms_xc/0.1583)**2 + 0.25*(ae6_mae/3.135)**2


    ost = ",{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H1X, Settings.GX, Settings.CORRELATION)
    ost += "{:},{:},{:},{:},{:},{:},".format(xparams['C1X'],xparams['C2X'],xparams['DX'],cparams['C1C'],cparams['C2C'],cparams['DC'])
    ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(xparams['K1'],xparams['B1X'],xparams['B2X'],xparams['B3X'],xparams['B4X'],xparams['A1'],cparams['B1C'],cparams['B2C'],cparams['B3C'])
    ost += "{:.3f},{:.3f},{:.4f}%,{:.4f}%,{:.4f}%,".format(ae6_me, ae6_mae, rms_x, rms_c, rms_xc)
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*err_xc)
    ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[ae6_etot[x] for x in Benchmarks.AE6_LIST])
    ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*ae6_err)
    ost += "{:},{:}\n".format(scanp_ae6, obj)
    of.write(ost)

    print(params, rms_x, rms_c, rms_xc, ae6_mae, obj)

    return obj
