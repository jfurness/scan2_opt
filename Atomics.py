import numpy as np
from scipy.optimize import minimize_scalar, minimize
from itertools import product
import SCAN
import SCAN2
import matplotlib.pyplot as plt
import Benchmarks
from random import shuffle
import Settings
from Integrators import get_Ec_for_atoms, get_Ex_for_atoms, get_Ec_non_colinear, get_Ex_from_lin, get_Ec_from_lin

def fit_poly_xc_separate_for_atoms(dens, non_spherical_dens, ha_dens, ae6_dens, xparams, cparams, XP, CP, params=None):
    """
    Function generates a set of potential X and C IE parameters, then filters them
    by the spherical atoms.

    It starts out with exchange, then correlation.

    Currently we filter based on individually exceeding SCAN RMSE (%) for X and C separately
    but this might not be the best as we later select by combined XC.
    It is impossible to do combined at this point however as the search space is too large.

    XP and CP hold the names of the parameters to be adjusted in order of rising power of beta
    ie. for 4th order:
        XP = ["CAX","CBX","CCX","CDX"]
        CP = ["CAC","CBC","CCC","CDC"]
    """

    if Settings.Fint == "TWOBETA" or Settings.Fint == "ORIGINAL":
        # get_err_xc_for_scan2(dens, xparams, cparams, exact=False)
        # get_err_xc_for_scan2(dens, xparams, cparams, exact=True)
        # raise SystemExit("Atomics.py:36")

        candidates = filter_original_IE(dens, non_spherical_dens, ha_dens, xparams, cparams)
        return candidates

    print("Generating integrated components...")
    lin_exl, lin_ecl = make_linear_energy_components(dens, xparams, cparams, XP, CP)
    if Settings.LOAD_AE6:
        ae6_lin_exl, ae6_lin_ecl = make_linear_energy_components(ae6_dens, xparams, cparams, XP, CP, non_colin=True)
    else:
        ae6_lin_exl = []
        ae6_lin_ecl = []
    # nsp_lin_exl, nsp_lin_ecl = make_linear_energy_components(non_spherical_dens, xparams, cparams, XP, CP, non_colin=True)
    # ha_lin_exl, ha_lin_ecl = make_linear_energy_components(ha_dens, xparams, cparams, XP, CP)
    # raise SystemExit

    # test_linear_composition(dens, xparams, cparams, XP, CP, non_colin=False)
    # test_linear_composition(non_spherical_dens, xparams, cparams, XP, CP, non_colin=True)
    # test_linear_composition(ha_dens, xparams, cparams, XP, CP, non_colin=False)
    # raise SystemExit

    # print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams, do_SCAN=True)
    # raise SystemExit

    # ERR_TOL_C = 5.0
    # NSP_ERR_TOL_C = 6.0
    # HA_ERR_TOL_C = 40.0

    # Lists to collect those that pass
    x_candidates = []
    c_candidates = []

    print("Testing Parameters...")
    print("K1 is", xparams['K1'])
    print("B1X is ", xparams['B1X'])

    # Fastest filter is the requirent that IE be monotomic in beta = [0,1]
    # We determine this numerically from 500 equally spaced points

    if params is None:
        params = get_pretty_coefficients(xparams, cparams)

    if Settings.FX_is_FC:
        print("Trying {:} parameters.".format(len(params)))
        candidates = filter_x_is_c(params, dens, xparams, cparams, XP, CP, lin_exl, lin_ecl, ae6_lin_exl, ae6_lin_ecl)
        #candidates = fast_min(params, dens, xparams, cparams, XP, CP, lin_exl, lin_ecl, ae6_lin_exl, ae6_lin_ecl)

    else:
        print("Trying {:} parameters.".format(len(params)**2))
        candidates = filter_x_diff_c(params, params, dens, xparams, cparams, XP, CP, lin_exl, lin_ecl, ae6_lin_exl, ae6_lin_ecl)

    # if len(candidates) == 0:
    #     print "Failed to find any parameters"
    #     print "{:} were too ugly".format(n_ugly)

    # print "{:} candidates passed the spherical atoms".format(len(candidates))

    # candidates.sort(key=lambda x: x['rms_xc'])

    # for i, n in enumerate(XP):
    #     xparams[n] = candidates[-1][n]
    # for i, n in enumerate(CP):
    #     cparams[n] = candidates[-1][n]

    # print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams, exact=False)

    return candidates

def post_filter_xc_diff(dens, non_spherical_dens, ha_dens, xolist, colist, xparams, cparams, XP, CP):
    """
    The outer product of X/C sets can be huge so this must be *fast*.

    Currently we simply look at the lowest RMSE for XC error for the spherical atoms.
    As these are saved from the filter stage with the parameters this can be done by simple addition.
    """

    best = 1e20
    best_paras = {}
    fullist = Benchmarks.HA_LIST
    better = 0  # Count for how many combinations exceeded SCAN accuracy
    for x, c in tqdm(itertools.product(xolist, colist),total=len(xolist)*len(colist),leave=False):
        xc = x["exl"]+c["ecl"]
        rms_xc = numpy.sqrt(numpy.mean((100*(xc - Benchmarks.BM_XC)/numpy.abs(Benchmarks.BM_XC))**2))

        if rms_xc < Settings.BM_XC:
            out = {}
            for nx in XP:
                out[nx] = x[nx]
            for nc in CP:
                out[nc] = c[nc]
            yield out

def filter_original_IE(dens, non_spherical_dens, ha_dens, xparams, cparams):
    candidates = []

    c1xl = np.arange(Settings.C1X_MIN, Settings.C1X_MAX, Settings.C1X_STEP)
    c2xl = np.arange(Settings.C2X_MIN, Settings.C2X_MAX, Settings.C2X_STEP)
    if Settings.MATCH_SCAN_END and Settings.Fint == "TWOBETA":
        x_list = []
        for c1, c2 in product(c1xl, c2xl):
            x_list.append([c1, c2, 1.24/np.exp(-c2)])
    else:
        dxl = np.arange(Settings.DX_MIN, Settings.DX_MAX, Settings.DX_STEP)
        x_list = list(product(c1xl, c2xl, dxl))

    c1cl = np.arange(Settings.C1C_MIN, Settings.C1C_MAX, Settings.C1C_STEP)
    c2cl = np.arange(Settings.C2C_MIN, Settings.C2C_MAX, Settings.C2C_STEP)
    if Settings.MATCH_SCAN_END and Settings.Fint == "TWOBETA":
        c_list = []
        for c1, c2 in product(c1cl, c2cl):
            c_list.append([c1, c2, 0.7/np.exp(-c2)])
    else:
        dcl = np.arange(Settings.DC_MIN, Settings.DC_MAX, Settings.DC_STEP)
        c_list = list(product(c1cl, c2cl, dcl))

    XP = ["C1X", "C2X", "DX"]
    CP = ["C1C", "C2C", "DC"]

    if Settings.ERR_TOL_X is not None:
        print("Pre Filtering exchange list")
        txl = []
        x_list = x_list
        for xp in x_list:
            for i, n in enumerate(XP):
                xparams[n] = xp[i]
            exl = get_Ex_for_atoms(dens, xparams, True)
            err = 100*(exl - Benchmarks.BM_X)/np.abs(Benchmarks.BM_X)
            rms_x = np.sqrt(np.mean(np.array(err)**2))
            if abs(rms_x) <= Settings.ERR_TOL_X:
                xp = list(xp)
                xp.append(exl)
                xp.append(rms_x)
                txl.append(xp)
        print("{:} remain from original {:}".format(len(txl), len(x_list)))
        print("Total to do is now {:}".format(len(txl)*len(c_list)))
        x_list = txl
    tot = len(x_list)*len(c_list)
    for cp in tqdm(c_list, total=tot, ascii=True, miniters=1, disable=Settings.NO_PROG):
        for i, n in enumerate(CP):
            cparams[n] = cp[i]
        ecl = get_Ec_for_atoms(dens, cparams, True)
        err = 100*(ecl - Benchmarks.BM_C)/np.abs(Benchmarks.BM_C)
        rms_c = np.sqrt(np.mean(np.array(err)**2))

        if Settings.ERR_TOL_C is not None and abs(rms_c) > Settings.ERR_TOL_C:
            # If a correlation filter was set, we failed it
            continue

        for xp in x_list:
            for i, n in enumerate(XP):
                xparams[n] = xp[i]
            if Settings.ERR_TOL_X is not None:
                exl = xp[-2]
                rms_x = xp[-1]
            else:
                exl = get_Ex_for_atoms(dens, xparams, True)
                err = 100*(exl - Benchmarks.BM_X)/np.abs(Benchmarks.BM_X)
                rms_x = np.sqrt(np.mean(np.array(err)**2))

            excl = exl + ecl
            err_xc = 100*(excl - Benchmarks.BM_XC)/np.abs(Benchmarks.BM_XC)
            rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))
            if Settings.ERR_TOL_XC is not None and abs(rms_xc) > Settings.ERR_TOL_XC:
                # Failed at the combined XC filter, if it was set
                continue
            tmp = {'rms_c':rms_c, 'rms_x':rms_x, 'ecl':ecl, 'exl':exl}
            for n in CP:
                tmp[n] = cparams[n]
            for n in XP:
                tmp[n] = xparams[n]
            candidates.append(tmp)

    print("In total, got {:} candidates".format(len(candidates)))
    return candidates

def get_rmse(lin, params, PL, bm, correlation=False):
    if correlation:
        el = get_Ec_from_lin(lin, params, PL)
    else:
        el = get_Ex_from_lin(lin, params, PL)

    err = 100*(bm - el)/np.abs(bm)
    rms = np.sqrt(np.mean(np.array(err)**2))
    return el, err, rms

def get_rmse_nonlin(dens, params, bm, correlation=False, non_colin=False):
    if correlation:
        if non_colin:
            el = get_Ec_non_colinear(dens, params)
        else:
            el = get_Ec_for_atoms(dens, params)
    else:
        el = get_Ex_for_atoms(dens, params)

    err = 100*(bm - el)/np.abs(bm)
    rms = np.sqrt(np.mean(np.array(err)**2))
    return el, err, rms


def get_err_xc_for_scan(dens, xparams, cparams, num=False, non_colin=False, HA=False, exact=False):
    """
    Convenience function that gets the SCAN errors for the current parameter set
    """
    exl = get_Ex_for_atoms(dens, xparams, False)
    if non_colin:
        ecl = get_Ec_non_colinear(dens, cparams, False)
    else:
        ecl = get_Ec_for_atoms(dens, cparams, False)

    print_report(exl, ecl, num=num, non_colin=non_colin, HA=HA, exact=exact)
    return

def get_err_xc_for_scan2(dens, xparams, cparams, num=False, non_colin=False, HA=False, exact=False):
    """
    Convenience function that gets the SCAN2 errors for the current parameter set
    """
    exl = get_Ex_for_atoms(dens, xparams, True)
    if non_colin:
        ecl = get_Ec_non_colinear(dens, cparams, True)
    else:
        ecl = get_Ec_for_atoms(dens, cparams, True)

    print_report(exl, ecl, num=num, non_colin=non_colin, HA=HA, exact=exact)
    return

def print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams, do_SCAN=False, exact=False):
    if do_SCAN:
        get_err_xc_for_scan(dens, SCAN.DEFAULT_X_PARAMS, SCAN.DEFAULT_C_PARAMS)
        get_err_xc_for_scan(non_spherical_dens, SCAN.DEFAULT_X_PARAMS, SCAN.DEFAULT_C_PARAMS, non_colin=True)
        if ha_dens is not None:
            get_err_xc_for_scan(ha_dens, SCAN.DEFAULT_X_PARAMS, SCAN.DEFAULT_C_PARAMS, HA=True)
    else:
        get_err_xc_for_scan2(dens, xparams, cparams, exact=exact)
        get_err_xc_for_scan2(non_spherical_dens, xparams, cparams, non_colin=True, exact=exact)
        if ha_dens is not None:
            get_err_xc_for_scan2(ha_dens, xparams, cparams, HA=True, exact=exact)


def print_report(exl, ecl, num=False, non_colin=False, HA=False, exact=False):
    """
    Utility function that prints a table showing individual and RMSE % errors for given list of
    exchange and correlation energies.
    """

    if HA:
        bm_x = Benchmarks.HA_X
        bm_c = Benchmarks.HA_C
        bm_xc = Benchmarks.HA_XC
        fullist = Benchmarks.HA_LIST
    elif non_colin:
        bm_x = Benchmarks.NON_SPHERICAL_X
        bm_c = Benchmarks.NON_SPHERICAL_C
        bm_xc = Benchmarks.NON_SPHERICAL_XC
        fullist = Benchmarks.NON_SPHERICAL_LIST
    else:
        bm_x = Benchmarks.BM_X
        bm_c = Benchmarks.BM_C
        bm_xc = Benchmarks.BM_XC
        fullist = Benchmarks.BM_LIST


    num_x = [exl[i] - bm_x[i] for i in range(len(exl))]
    err_x = [100*(exl[i] - bm_x[i])/np.abs(bm_x[i]) for i in range(len(exl))]
    rms_x = np.sqrt(np.mean(np.array(err_x)**2))

    num_c = [ecl[i] - bm_c[i] for i in range(len(ecl))]
    err_c = [100*(ecl[i] - bm_c[i])/np.abs(bm_c[i]) for i in range(len(ecl))]
    rms_c = np.sqrt(np.mean(np.array(err_c)**2))

    num_xc = [ecl[i] + exl[i] - bm_xc[i] for i in range(len(ecl))]
    err_xc = [100*(ecl[i] + exl[i] - bm_xc[i])/np.abs(bm_xc[i]) for i in range(len(ecl))]
    rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))

    lab = max(list(map(len, fullist))+[4])
    pre = "{:<"+str(lab)+"}"

    print("\n"+" "*lab+"| Exchange  | Correlation | Combined ")
    for i in range(len(fullist)):
        if exact:
            print((pre+"| {:12.8f}  |   {:12.8f}  | {:12.8f} Eh").format(fullist[i], exl[i], ecl[i], exl[i]+ecl[i]))
        elif num:
            print((pre+"| {:+8.4f}  |   {:+8.4f}  | {:+8.4f} Eh").format(fullist[i], num_x[i], num_c[i], num_xc[i]))
        else:
            print((pre+"| {:+8.4f}% |   {:+8.4f}% | {:+8.4f}%").format(fullist[i], err_x[i], err_c[i], err_xc[i]))
    print("_____________________________________________________")
    print((pre+"| {:+8.4f}% |   {:+8.4f}% | {:+8.4f}%\n").format("RMSE",rms_x, rms_c, rms_xc))
    return


def wrap_get_Ec(b2c, cparams, He_dens, ex):
    cparams['B2C'] = abs(b2c)
    ec = SCAN2.get_single_orbital_correlation(cparams, He_dens[0][:,0]+He_dens[0][:,1], He_dens[0][:,7], np.zeros_like(He_dens[0][:,0]))
    ec = np.dot(He_dens[0][:,6], ec)
    # err = abs(100*abs(ex + ec + 1.068)/1.068)
    err = abs(ex + ec + 1.068)

    return err

def filter_rare_gas_energies(xparams, cparams, dens, candidates):
    olist = []
    for trial in candidates:
        res = run_rare_gas_energies(xparams, cparams, dens, trial)
        olist.append(res)

    return olist

def run_rare_gas_energies(xparams, cparams, dens, trial):
    # Find XC energy for He, Ne, Ar, Kr and Xe
    cparams['C1C'] = trial['C1C']
    cparams['C2C'] = trial['C2C']
    cparams['DC'] = trial['DC']
    xparams['C1X'] = trial['C1X']
    xparams['C2X'] = trial['C2X']
    xparams['DX'] = trial['DX']

    exlist = get_Ex_for_atoms(dens, xparams)
    eclist = get_Ec_for_atoms(dens, cparams)
    exclist = exlist + eclist

    ref_x = np.array([-1.0260000228881836, -12.107999801635742, -30.187999725341797, -93.889999389648438, -179.19999694824219])
    ref_c = np.array([-4.1999999433755875E-2, -0.39100000262260437, -0.72299998998641968, -1.8500000238418579, -3.0000000000000000])
    ref_xc = np.array([-1.0680000223219395, -12.498999804258347, -30.910999715328217, -95.739999413490295, -182.19999694824219])

    err_x = 100.0*(exlist - ref_x)/abs(ref_x)
    err_c = 100.0*(eclist - ref_c)/abs(ref_c)
    err_xc = 100.0*(exclist - ref_xc)/abs(ref_xc)

    names = ['He', 'Ne', 'Ar', 'Kr', 'Xe']
    for i in range(len(err_x)):
        trial['RG_'+names[i]+'_x'] = err_x[i]
        trial['RG_'+names[i]+'_c'] = err_c[i]
        trial['RG_'+names[i]+'_xc'] = err_xc[i]
    trial['RG_ME_x'] = np.mean(ref_x - exlist),
    trial['RG_ME_c'] = np.mean(ref_c - eclist),
    trial['RG_ME_xc'] = np.mean(ref_xc - exclist),
    trial['RG_MAE_x'] = np.mean(np.abs(ref_x - exlist)),
    trial['RG_MAE_c'] = np.mean(np.abs(ref_c - eclist)),
    trial['RG_MAE_xc'] = np.mean(np.abs(ref_xc - exclist)),
    trial['RG_MRE_x'] = np.mean(err_x),
    trial['RG_MRE_c'] = np.mean(err_c),
    trial['RG_MRE_xc'] = np.mean(err_xc),
    trial['RG_MARE_x'] = np.mean(np.abs(err_x)),
    trial['RG_MARE_c'] = np.mean(np.abs(err_c)),
    trial['RG_MARE_xc'] = np.mean(np.abs(err_xc)),
    trial['RG_raw_x'] = exlist,
    trial['RG_raw_c'] = eclist,
    trial['RG_raw_xc'] = exclist,
    trial['RG_err_x'] = err_x,
    trial['RG_err_c'] = err_c,
    trial['RG_err_xc'] = err_xc
    return trial


def filter_N_energy(xparams, cparams, N_dens, candidates, do_SCAN2):
    olist = []
    for trial in candidates:
        res = check_N_energy(xparams, cparams, N_dens, trial, do_SCAN2)
        olist.append(res)
    return olist


def check_N_energy(xparams, cparams, N_dens, trial, do_SCAN2):
    cparams['C1C'] = trial['C1C']
    cparams['C2C'] = trial['C2C']
    cparams['DC'] = trial['DC']
    xparams['C1X'] = trial['C1X']
    xparams['C2X'] = trial['C2X']
    xparams['DX'] = trial['DX']
    ex = get_Ex_for_atoms(N_dens, xparams, do_SCAN2)[0]  # returns single element list
    ec = get_Ec_for_atoms(N_dens, cparams, do_SCAN2)[0]

    refx = -6.597
    refc = -0.188
    refxc = refx + refc

    trial['N_err_xc'] = 100.0*(ex+ec - refxc)/abs(refxc),
    trial['N_err_x'] = 100.0*(ex - refx)/abs(refx),
    trial['N_err_c'] = 100.0*(ec - refc)/abs(refc),

    return trial


def fit_MSX_for_atoms(dens, non_spherical_dens, ha_dens, xparams, cparams):
    ERR_TOL_X = 90.3222
    NSP_ERR_TOL_X = 90.6387  # Just trying to match SCAN against acGGA BM
    HA_ERR_TOL_X = 91.6256   # Again, just SCAN against HF

    ERR_TOL_C = 97.1632
    NSP_ERR_TOL_C = 99.2035
    HA_ERR_TOL_C = 930.4658

    # Lists to collect those that pass
    x_candidates = []
    c_candidates = []

    beta = np.arange(0, 1, 0.002)
    beta2 = beta**2
    beta3 = beta**3

    err_min_x = 1e20
    nsp_err_min_x = 1e20
    ha_err_min_x = 1e20
    err_min_c = 1e20
    nsp_err_min_c = 1e20
    ha_err_min_c = 1e20

    A_MIN = 0
    A_MAX = 10
    A_STEP = 0.1
    a_l = np.arange(A_MIN, A_MAX, A_STEP)
    # a_l = np.array([SCAN2.DEFAULT_X_PARAMS["CAX"]])

    B_MIN = 0
    B_MAX = 10
    B_STEP = 0.1
    b_l = np.arange(B_MIN, B_MAX, B_STEP)

    ugrid = product(a_l, b_l)
    tot = a_l.shape[0]*b_l.shape[0]

    n_tried = 0
    n_ugly = 0

    n_sa_x = 0
    n_nsp_x = 0
    n_ha_x = 0

    n_sa_c = 0
    n_nsp_c = 0
    n_ha_c = 0

    for p in tqdm(ugrid, total=tot):
        n_tried += 1

        a, b = p

        xparams["CAX"] = a
        xparams["CBX"] = b

        exl, err_x, rms_x = get_rmse_nonlin(dens, xparams, Benchmarks.BM_X, correlation=False)

        # Keep a tally of the "best" exchange RMSE (%) for interest
        if abs(rms_x) < abs(err_min_x):
            best_x = {"CAX":xparams['CAX'], 'CBX':xparams['CBX'], 'B1X':xparams['B1X'], 'err_x':rms_x}
            err_min_x = rms_x
        if abs(rms_x) < ERR_TOL_X:
            n_sa_x += 1
            # Proceed to non-spherical atoms
            nsp_exl, nsp_err_x, nsp_rms_x = get_rmse_nonlin(non_spherical_dens, xparams, Benchmarks.NON_SPHERICAL_X, correlation=False)
            if abs(nsp_rms_x) < abs(nsp_err_min_x):
                nsp_best_x = {"CAX":xparams['CAX'], 'CBX':xparams['CBX'], 'B1X':xparams['B1X'], 'nsp_err_x':nsp_rms_x}
                nsp_err_min_x = nsp_rms_x

            if abs(nsp_rms_x) < abs(NSP_ERR_TOL_X):
                n_nsp_x += 1
                # Proceed to hollow atom
                ha_exl, ha_err_x, ha_rms_x = get_rmse_nonlin(ha_dens, xparams, Benchmarks.HA_X, correlation=False)

                if abs(ha_rms_x) < abs(ha_err_min_x):
                    ha_best_x = {"CAX":xparams['CAX'], 'CBX':xparams['CBX'], 'B1X':xparams['B1X'], 'ha_err_x':ha_rms_x}
                    ha_err_min_x = ha_rms_x

                if abs(ha_rms_x) < abs(HA_ERR_TOL_X):
                    n_ha_x += 1
                    tmp = {"CAX":xparams['CAX'], 'CBX':xparams['CBX'], 'B1X':xparams['B1X'], 'ha_err_x':ha_rms_x, 'ha_exl':ha_exl, 'nsp_err_x':nsp_rms_x, 'nsp_exl':nsp_exl, 'err_x':rms_x, 'exl':exl}
                    # Success! Add it to the list!
                    x_candidates.append(tmp)
        # NOW CORRELATION (still in loop)
        cparams["CAC"] = a
        cparams["CBC"] = b

        ecl, err_c, rms_c = get_rmse_nonlin(dens, cparams, Benchmarks.BM_C, correlation=True)

        if abs(rms_c) < abs(err_min_c):
            best_c = {"CAC":cparams['CAC'], 'CBC':cparams['CBC'], 'err_c':rms_c}
            err_min_c = rms_c
        if abs(rms_c) < ERR_TOL_C:
            n_sa_c += 1
            # Proceed to non-spherical atoms
            nsp_ecl, nsp_err_c, nsp_rms_c = get_rmse_nonlin(non_spherical_dens, cparams, Benchmarks.NON_SPHERICAL_C, correlation=True, non_colin=True)

            if abs(nsp_rms_c) < abs(nsp_err_min_c):
                nsp_best_c = {"CAC":cparams['CAC'], 'CBC':cparams['CBC'], 'nsp_err_c':nsp_rms_c}
                nsp_err_min_c = nsp_rms_c

            if abs(nsp_rms_c) < abs(NSP_ERR_TOL_C):
                n_nsp_c += 1
                # Proceed to hollow atoms
                ha_ecl, ha_err_c, ha_rms_c = get_rmse_nonlin(ha_dens, cparams, Benchmarks.HA_C, correlation=True)

                if abs(ha_rms_c) < abs(ha_err_min_c):
                    ha_best_c = {"CAC":cparams['CAC'], 'CBC':cparams['CBC'], 'ha_err_c':ha_rms_c}
                    ha_err_min_c = ha_rms_c

                if abs(ha_rms_c) < abs(HA_ERR_TOL_C):
                    n_ha_c += 1
                    tmp = {"CAC":cparams['CAC'], 'CBC':cparams['CBC'], 'err_c':rms_c, 'ecl':ecl, 'nsp_err_c':nsp_rms_c, 'nsp_ecl':nsp_ecl, 'ha_err_c':ha_rms_c, 'ha_ecl':ha_ecl}
                    c_candidates.append(tmp)

    print("Total of {:} tested".format(n_tried))

    if len(x_candidates) == 0:
        print("Failed to find any exchange parameters")
        print("{:} were too ugly".format(n_ugly))
        try:
            print("{:} passed spherical atoms (best {:.3f}%)".format(n_sa_x, best_x['err_x']))
            print("{:} passed non-spherical atoms (best {:.3f}%)".format(n_nsp_x, nsp_best_x['nsp_err_x']))
            print("{:} passed hollow atom (best {:.3f}%)".format(n_ha_x, ha_best_x['ha_err_x']))
        except UnboundLocalError:
            pass  # Never got to make even one attempt at one of the filters
    if len(c_candidates) == 0:
        print("Failed to find any correlation parameters")
        print("{:} were too ugly".format(n_ugly))
        try:
            print("{:} passed spherical atoms (best {:.3f}%)".format(n_sa_c, best_c['err_c']))
            print("{:} passed non-spherical atoms (best {:.3f}%)".format(n_nsp_c, nsp_best_c['nsp_err_c']))
            print("{:} passed hollow atom (best {:.3f}%)".format(n_ha_c, ha_best_c['ha_err_c']))
        except UnboundLocalError:
            pass  # Never got to make even one attempt at one of the filters

    if len(x_candidates) == 0 or len(c_candidates) == 0:
        raise SystemExit("Stopping in Atomics")

    print("{:} parameters tested, {:}({:.3f}%) were too ugly.".format(n_tried, n_ugly, 100*n_ugly/n_tried))
    print("Found {:<5} exchange candidates.".format(len(x_candidates)))
    print("Found {:<5} correlation candidates.".format(len(c_candidates)))
    print("Best spherical atom :   CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(best_x['CAX'], best_x['CBX'], best_x['CCX'], best_x['CDX']))
    print("                        CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(best_c['CAC'], best_c['CBC'], best_c['CCC'], best_c['CDC']))
    for a in XP:
        xparams[a] = best_x[a]
    for a in CP:
        cparams[a] = best_c[a]
    print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams)
    print(" non-spherical atom :   CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(nsp_best_x['CAX'], nsp_best_x['CBX'], nsp_best_x['CCX'], nsp_best_x['CDX']))
    print("                        CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(nsp_best_c['CAC'], nsp_best_c['CBC'], nsp_best_c['CCC'], nsp_best_c['CDC']))
    for a in XP:
        xparams[a] = nsp_best_x[a]
    for a in CP:
        cparams[a] = nsp_best_c[a]
    print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams)
    print("        hollow atom :   CAX {:}, CBX {:}, CCX {:}, CDX {:}".format(ha_best_x['CAX'], ha_best_x['CBX'], ha_best_x['CCX'], ha_best_x['CDX']))
    print("                        CAC {:}, CBC {:}, CCC {:}, CDC {:}".format(ha_best_c['CAC'], ha_best_c['CBC'], ha_best_c['CCC'], ha_best_c['CDC']))
    for a in XP:
        xparams[a] = ha_best_x[a]
    for a in CP:
        cparams[a] = ha_best_c[a]
    print_full_report(dens, non_spherical_dens, ha_dens, xparams, cparams)
    raise SystemExit("We done")


def fast_min(params, dens, xparams, cparams, XP, CP, lin_exl, lin_ecl, ae6_lin_exl, ae6_lin_ecl):

    beta = np.arange(0, 1, 0.002)
    beta2 = beta**2
    beta3 = beta**3
    beta4 = beta**4

    beta_05 = np.arange(0, 0.5, 0.002)
    beta2_05 = beta_05**2
    beta3_05 = beta_05**3

    beta_10 = np.arange(0.5, 1.0, 0.002)
    beta2_10 = beta_10**2
    beta3_10 = beta_10**3


    steps = [1.0,0.5,0.1,0.05,0.01]

    n_passed = 0

    best_atom_xc = 1e20
    best_ae6_mae = 1e20

    best_par = dict()
    best_par_c = dict()

    for j,stp in enumerate(steps) :

        if j > 0:

            a_l = np.arange(best_par['CAX'] - 4*stp*Settings.A_STEP, best_par['CAX'] + 4*stp*Settings.A_STEP, stp*Settings.A_STEP)
            b_l = np.arange(best_par['CBX'] - 4*stp*Settings.B_STEP, best_par['CBX'] + 4*stp*Settings.B_STEP, stp*Settings.B_STEP)
            c_l = np.arange(best_par['CCX'] - 4*stp*Settings.C_STEP, best_par['CCX'] + 4*stp*Settings.C_STEP, stp*Settings.C_STEP)
            d_l = np.arange(best_par['CDX'] - 4*stp*Settings.D_STEP, best_par['CDX'] + 4*stp*Settings.D_STEP, stp*Settings.D_STEP)


            ugrid = product(a_l, b_l, c_l, d_l)
            tot = a_l.shape[0]*b_l.shape[0]*c_l.shape[0]*d_l.shape[0]

            params = []
            min_fbeta1 = -(2335500*xparams['K1'] + 73000)/(320675*xparams['K1'] + 12702)
            for p in tqdm(ugrid, total=tot, ascii=True, disable=Settings.NO_PROG):
                l = list(p)
                if Settings.Fint == "BETA":
                    l[2] = -(p[2] + 1 + p[0] + p[1])

                    d1 = SCAN2.interp_gradient_poly(beta, beta2, beta3, l[0], l[1], l[2], l[3])
                elif Settings.Fint == "JPLAP" or Settings.Fint == "SMOOTH":
                    d1 = SCAN2.interp_gradient_jplap(beta, beta2, beta3, beta4, l[0], l[1], l[2], l[3])
                else:
                    raise SystemExit("ERROR: Inappropriate assessment of beauty!")

                if np.any(d1 > 0.0):
                    continue  # Reject the ugly!
                if Settings.Fint == "JPLAP" or Settings.Fint == "SMOOTH":
                    if Settings.Fint == "JPLAP" :
                        jplap_1 = SCAN2.eval_jplap(1.0, 0.0, 0.0,l[0],l[1],l[2],l[3],xparams['FREG'])
                    else :
                        jplap_1 = SCAN2.eval_smoothlap(1.0,l[0],l[1],l[2],l[3])
                    if jplap_1 < min_fbeta1:
                        continue
                    d2 = SCAN2.interp_grad2_jplap(beta, beta2, beta3, l[0], l[1], l[2], l[3])
                    last = d2[0]
                    count = 0
                    for i in range(1, d2.shape[0]):
                        if last*d2[i] < 0:  # Only possible if prev or current is negative but not both
                            count += 1
                            if count > 1:
                                break
                    if count > 1:
                        continue
                params.append(l)

        grid = params
        tot = len(params)
        candidates = []

        for p in tqdm(grid, total=tot, ascii=True, disable=Settings.NO_PROG):
            for i, n in enumerate(XP):
                xparams[n] = p[i]
            for i, n in enumerate(CP):
                cparams[n] = p[i]

            exl, err_x, rms_x = get_rmse(lin_exl, xparams, XP, Benchmarks.BM_X, correlation=False)
            ecl, err_c, rms_c = get_rmse(lin_ecl, cparams, CP, Benchmarks.BM_C, correlation=True)

            excl = exl + ecl
            err_xc = 100*(Benchmarks.BM_XC - excl)/np.abs(Benchmarks.BM_XC)
            rms_xc = np.sqrt(np.mean(np.array(err_xc)**2))
            ae6_err, ae6_etot = assess_ae6_for_scan2(ae6_lin_exl, ae6_lin_ecl, xparams, cparams, XP, CP)
            ae6_me = np.mean(ae6_err)
            ae6_mae = np.mean(np.abs(ae6_err))

            if rms_xc < best_atom_xc:
                best_atom_xc = rms_xc
                best_atom = [rms_x,rms_c]
                best_par = xparams
                best_par_c = cparams
                best_err_xc = err_xc
                best_ae6_mae = ae6_mae
                best_ae6_me = ae6_me
                best_ae6_err = ae6_err
                best_ae6_etot = ae6_etot

    ost = ",{:},{:},{:},{:},{:},".format(Settings.Fint, Settings.FX_is_FC, Settings.H1X, Settings.GX, Settings.CORRELATION)
    ost += "{:},{:},{:},{:},".format(best_par['CAX'],best_par['CBX'],best_par['CCX'],best_par['CDX'])
    if Settings.Fint == "SMOOTH" :
        ost += "{:},".format(best_par['XETA'])
    ost += "{:},{:},{:},{:},{:},{:},{:},{:},{:},".format(best_par['K1'],best_par['B1X'],best_par['B2X'],best_par['B3X'],best_par['B4X'],best_par['A1'],best_par_c['B1C'],best_par_c['B2C'],best_par_c['B3C'])
    ost += "{:.3f},{:.3f},{:.4f}%,{:.4f}%,{:.4f}%,".format(best_ae6_me, best_ae6_mae, best_atom[0], best_atom[1], best_atom_xc)
    ost += ("{:+.4f}%,"*len(Benchmarks.BM_LIST)).format(*best_err_xc)
    ost += ("{:.4f},"*len(Benchmarks.AE6_LIST)).format(*[best_ae6_etot[x] for x in Benchmarks.AE6_LIST])
    ost += ("{:.3f},"*len(Benchmarks.AE6_TESTS)).format(*best_ae6_err)

    print("Best Atomic XC was ", best_atom_xc)
    print("Best corresponding AE6 MAE was   ", best_ae6_mae)

    return ost
