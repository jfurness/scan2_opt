import matplotlib.pyplot as plt
import numpy as np
from SCAN2 import corgga_0, corgga_1
from Optimiser import read_dens
from math import pi
from itertools import product

xparams = dict(
    K1 = 0.065,
    A1 = 5.57996055999,
    C1X = 1.0,
    C2X= 0.7,
    DX = 1.24,
    CXBX = 1.0,
    MUAK = 10.0/81.0,
    K0 = 0.1740,
    B1X = 0.156632,
    B2X = 0.12083,
    B3X = 0.5,
    B4X = 1.0,
    AX = -0.7385587663820224058842300326808360
)

cparams = dict(
    C1C = 0.6,
    C2C = 0.5,
    DC = 0.7,
    B1C = 0.0378089856501,
    B2C = 0.111827704234,
    B3C = 0.166101581696,
    B4C = 1.0,
    KAILD = 0.12802585262625815,
    GAMMA = 0.031090690869654895034940863712730,
    BETA_MB = 0.066724550603149220,
    AFACTOR = 0.5,  # 0.1
    BFACTOR = 1.0,  # 0.1778
    CFACTOR = 0.1667,
    DFACTOR = 0.29633,
    BETA_RS_0 = 0.066725,  # Same as BETA_MB?
    C_TILDE = 1.467,
    P_TAU = 4.5,
    F0 = -0.9,
    A1C = 1.31088867187
)

s = np.logspace(-2, 2, 500)
rs = np.logspace(-4, 6, 600)

sv, rsv = np.meshgrid(rs, s)


diff = np.zeros((s.shape[0], rs.shape[0]))
x= 0 
for vs in s:
    y = 0
    for vrs in rs:
        ec0 = corgga_0(cparams, vrs, vs, 0.0)
        ec1 = corgga_1(cparams, vrs, vs, 0.0)
        diff[x,y] = ec1 > ec0
        if ec1 > ec0:
            print("s",vs,"rs", vrs)
        y += 1
    x += 1

cs1 = plt.contourf(np.log10(rsv), np.log10(sv), diff, extend='neither',cmap=plt.cm.OrRd, interpolation='none')
plt.ylabel("log$_{10}[r_s]$")
plt.xlabel("log$_{10}[s]$")
plt.title("$g_0(r_s, s; a_{1c} = 1.31088867187)$")
ax = plt.gca()
ax.grid(True)
plt.show()