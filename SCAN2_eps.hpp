#include "functional.hpp"
#include "constants.hpp"
#include <iostream>
#include <iomanip>
#include "config.hpp"
#include "pw92eps.hpp"

/*  SCAN2 functional
 *
 *  Implemented by James Furness
 *
*/

namespace SCAN2_eps
{
    
  template<class num> 
  static num SCAN2_X(const num, const num, const num);
  template<class num> 
  static num SCAN2_X_Fx(const num, const num);
  template<class num>
  static num SCAN2_C(const densvars<num>&); 


  template<class num>
  static num fx_unif2(const num &d)
  {
    return (-0.75*pow(3/PI,1.0/3.0))*pow(d,4.0/3.0);
  }

  template<class num>
  static num SCAN2_X(const num d_n, const num d_g, const num d_tau)
  {
    num tauW = d_g/(8.0*d_n);
    num tauUnif = 0.3*pow(3*PI2, 2.0/3.0) * pow(d_n, 5.0/3.0);

    num beta = 0.0;

    beta = (d_tau - tauW)/(d_tau + tauUnif);

    num p = 0.0;
    if (abs(d_g) > 1.0e-16) {
      p = d_g/(4*pow(3*PI2, 2.0/3.0)*pow(d_n, 8.0/3.0));
    };

    num Fx = SCAN2_X_Fx(p, beta);

    return Fx;
  }

  template<class num>
  static num SCAN2_X_Fx(num p, num beta)
  {
    const parameter k1 = 0.07;
    
    const parameter UAK = 10.0/81.0;
    const parameter b1 = 1.0;
    const parameter a1 = 5.57996055999; 
    const parameter hx0 = 1.174;

    const parameter cAx = -3.5;
    const parameter cBx = 62.7;
    const parameter cCx = -53.4719759312;
    
    num p2 = p*p;
    num b12 = b1*b1;

    
    num qt = (9.0/10.0)*(2*beta - 1.0) + (17.0/12.0)*p;
    num qt2 = qt*qt;

    num xfac = UAK*p + 146.0/2025.0*qt2/(1+qt2/b12);
    xfac -= 73.0/405.0*qt*p/(sqrt(1 + qt2/b12)*sqrt(1 + p2/b12));
    xfac += 1.0/k1*UAK*UAK*p2/(1 + p2/b12);

    num hx1 = 1.0 + k1 - k1/(1.0 + xfac/k1);

    num fx = pow(1.0 - 2*beta, 3)*(1.0 + cAx*beta + cBx*pow(beta, 2) + cCx*pow(beta, 3));

    num p14 = pow(p,1.0/4.0);
    num gx = 1.0;
    if (abs(p) > 1.0e-16) {
       gx = -expm1(-0.444/p14 - a1/sqrt(p));
    };

    num Fx1 = hx1 + fx*(hx0 - hx1);

    num Fx = Fx1*gx;

    return Fx;
  }

    
  template<class num>
  static num SCAN2_C( const densvars<num> &d ) 
  {
    const parameter cAc = -6.0;
    const parameter cBc = 10.9;
    const parameter cCc = -5.2;

    num rs = pow(4.0*PI*d.n/3.0, -(1.0/3.0));
    num sqrtrs = 0.0;
    if (abs(rs) > 1.0e-16) {
       sqrtrs = sqrt(rs);
    }

    num eLSDA1c = 0.0;
    if (abs(d.n) > 1.0e-16) {
       eLSDA1c = pw92eps::pw92eps(d);
    }

    num ds = ufunc(d.zeta, 5.0/3.0)/2.0;
    num dx = ufunc(d.zeta, 4.0/3.0)/2.0;

    num tauW = d.gnn/(8.0*d.n);
    num tauUnif = 0.3*pow(3*PI2, 2.0/3.0)*pow(d.n, 5.0/3.0)*ds;

    num beta = 0.0;
    if (abs(d.tau - tauW) > 1.0e-14) {
        beta = (d.tau - tauW)/(d.tau + tauUnif);
    }

    num s = 0.0;
    if (abs(d.gnn) > 1.0e-16) {
       s = pow(d.gnn, (1.0/2.0))/(2.0*pow(3*PI2, 1.0/3.0)*pow(d.n, 4.0/3.0));
    }

    // ec1
    const parameter gamma = 0.031091;
    const parameter P_TAU = 4.5;
    const parameter C_TILDE = 1.467;
    const parameter BETA_RS_0 = 0.066725;

    const parameter AFACTOR = 0.5;
    const parameter BFACTOR = 1.0;
    const parameter CFACTOR = 0.16667;
    const parameter DFACTOR = 0.29633;

    num beta_num = 1.0 + AFACTOR*rs*(BFACTOR + CFACTOR*rs);
    num beta_den = 1.0 + AFACTOR*rs*(1.0 + DFACTOR*rs);
    num beta_rs = BETA_RS_0*beta_num/beta_den;

    num phi = ufunc(d.zeta, 2.0/3.0)/2.0;
    num w1 = expm1(-eLSDA1c/(gamma*pow(phi, 3)));
    num t = pow((3.0*pow(PI, 2)/16.0), (1.0/3.0))*s/(phi*sqrtrs);
    num p_t = (1.0 + t/P_TAU)/(1.0 + C_TILDE*t/P_TAU);
    num A = BETA_RS_0/(gamma*w1);
    num t_til = t*sqrt(p_t*beta_rs/BETA_RS_0);
    num y = A*pow(t_til, 2);
    num g = (1.0 + y)/(1.0 + y + pow(y, 2));
    num hcore = 1.0 + BETA_RS_0/gamma*pow(t_til, 2)*g;
    num H1 = gamma*pow(phi, 3)*log(hcore);
    num e1c = eLSDA1c + H1;
    
    // Interpolation-Extrapolation function
    num fc = pow(1.0 - 2*beta, 3)*(1 + cAc*beta + cBc*pow(beta, 2) + cCc*pow(beta, 3));

    // ec0
    const parameter b1c = 0.040982;
    const parameter b2c = 0.100689;
    const parameter b3c = 0.180040;
    const parameter b4c = 2.3631;
    
    const parameter F0 = -0.9;
    const parameter CHI_2 = 0.0;
    const parameter CHI_42 = 0.445;

    num eLDA0c = -b1c/(1.0 + b2c*sqrtrs + b3c*rs);

    num gc = (1.0 - b4c*(dx - 1.0))*(1.0 - pow(d.zeta, 12));

    num cx = -(3.0/(4.0*PI))*pow(9.0*PI/4.0, 1.0/3.0)*dx;
    num beta_inf = BETA_RS_0*CFACTOR/DFACTOR;
    num chi_inf = pow(3*PI2/16.0, 2.0/3.0)*beta_inf*phi/(cx - F0);

    num g0 = 1.0/(1.0 + CHI_2*pow(s, 2) + CHI_42*pow(s, 4));

    num w0 = expm1(-eLDA0c/b1c);

    num H0  = b1c*log(1.0 + w0*(1.0 - g0));

    num e0c = (eLDA0c + H0)*gc;

    num res = e1c*d.n + fc*(e0c-e1c)*d.n;

    return res;
  }
}
