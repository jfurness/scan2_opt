import numpy as np

wfl = './scan2_param_diff_evol.csv'
with open(wfl,'r') as tinfl:
    for ln in tinfl:
        hdr = ln.strip().split(',')
        break

dat = np.genfromtxt(wfl,delimiter=',',skip_header=1,skip_footer=3)

nrun = dat[:,0].shape[0]
eta_l = dat[:,0]
k_l = dat[:,1]
dp_l = dat[:,2]
bi = np.argmin(dat[:,-1])

etab = np.sum(eta_l)/nrun
etav = np.sum(eta_l**2)/nrun
etas = (max(0.0,etav-etab**2))**(0.5)
print('eta:\n best = {:} \n mean = {:} (|diff| = {:}) \n var = {:} \n std dev = {:}\n'.format(eta_l[bi],etab,abs(etab-eta_l[bi]),etav,etas))

kb = np.sum(k_l)/nrun
kv = np.sum(k_l**2)/nrun
ks = (max(0.0,kv-kb**2))**(0.5)
print('kappa1:\n best = {:} \n mean = {:} (|diff| = {:}) \n var = {:} \n std dev = {:}\n'.format(k_l[bi],kb,abs(kb-k_l[bi]),kv,ks))

dpb = np.sum(dp_l)/nrun
dpv = np.sum(dp_l**2)/nrun
dps = (max(0.0,dpv-dpb**2))**(0.5)
print('dp:\n best = {:} \n mean = {:} (|diff| = {:}) \n var = {:} \n std dev = {:}\n'.format(dp_l[bi],dpb,abs(dpb-dp_l[bi]),dpv,dps))

print('index of best pars=',bi)
for i in range(dat.shape[1]):
    print(hdr[i],dat[bi,i])
