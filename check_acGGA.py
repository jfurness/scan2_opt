import numpy as np
from SCAN2 import corgga_0_acgga, corgga_1_acgga
from Densities_NonSpherical import Atom
from math import pi

print("Checking acGGA+\n")

cparams = dict(
    CONC = 1.0,
    CAC = -6.0,
    CBC = 10.0,
    CCC = -5.2,
    CDC = 1.0,

    CHI_2 = 0.0,
    CHI_42 = 0.445,
    B1C = 0.041,
    B2C = 0.1011944203,
    B3C = 0.18012027,
    B4C = 2.3631
)

# names = ["He", "Ne", "Ar", "Kr", "Xe"]
names = ["Li", "N", "Ne", "Na", "P", "Ar", "K", "Cr", "Cu", "As", "Kr", "Xe"]
dens = []
r, h = np.linspace(0.001, 10.0, 1000, retstep=True)
MIN_DEN = 1e-10

for n in names:
    a = Atom(n)
    d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
    idxs = d0 + d1 > MIN_DEN  # Filter all those with small densities
    dens.append(np.stack([d0[idxs], d1[idxs], g0[idxs], g1[idxs], t0[idxs], t1[idxs], 4*h*pi*r[idxs]**2], axis=1))

# n_elec = [2, 10, 18, 36, 54]
# bm = [0.022899370041, 0.037088063928, 0.041338983931, 0.051264342418, 0.056234784314]
n_elec = [3, 7, 10, 11, 15, 18, 19, 24, 29, 33, 36, 54]
bm = [0.0188, 0.0274, 0.0367, 0.0355, 0.0371, 0.0412, 0.0406, 0.043, 0.0484, 0.0494, 0.0512, 0.0562]

for i, a in enumerate(dens):
    d = a[:, 0]+a[:, 1]
    zeta = (a[:, 0] - a[:, 1])/d
    dthrd = np.exp(np.log(d)*1.0/3.0)
    rs = (0.75/pi)**(1.0/3.0)/dthrd
    g = a[:, 2]+a[:, 3]
    s = np.abs(g)/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(d, 4.0/3.0))
    # ec_eps = corgga_0_acgga(cparams, rs, s, zeta)
    ec_eps = corgga_1_acgga(cparams, rs, s, zeta)
    Ec = np.dot(a[:, -1], ec_eps*d)/n_elec[i]

    print(names[i], "Ec/N: {:.12f}".format(Ec), "err: {:.3f}".format(Ec + bm[i]))