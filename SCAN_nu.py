
import numpy as np
import os
from scipy.optimize import minimize
from multiprocessing import Pool
from functools import partial

import Settings
import SCAN2
from Ar2_pscf import jsextr
from Jellium import Jelly
from Integrators import get_Ex_for_atoms, get_Ec_for_atoms, get_Ec_non_colinear
import Benchmarks
from int_ex import get_lzlx_coef
from int_ec import get_lzlc_coef

from Optimiser_SA import generate_density, read_QUEST_densities, read_QUEST_restricted_densities

if not os.path.isdir('./scan_nu/'):
    os.system('mkdir ./scan_nu/')

if Settings.ISOORB == 'BN':
    ind = 'beta'
    conpar = 'NU'
    nu_max = 1.0
elif Settings.ISOORB == 'BA':
    ind = 'alpha'
    conpar = 'ETA'
    nu_max = 0.05

def scan_nu_errs(pars, xparams, cparams, full_dens, lz_dens, ns_dens,\
    jellies, ar2_dens, uwg = {},only_res=False,norelax=False):

    wg = {'SAXC': 0.15827686592665027, 'NSAXC': 0.3351793725497179, \
        'LZBXpe': -0.07704670067688542, 'LZBCpe': -0.014483022242160932,\
        'JSFXC': 6.254299759196872, 'AR2': 1.065323082005788}

    for akey in uwg:
        wg[akey] = uwg[akey]

    if not Settings.DO_JELLIUM:
        wg['JSFXC'] = 0.0
    if not Settings.DO_AR2:
        wg['AR2'] = 0.0

    #nwg = 0.0
    #for akey in wg:
    #    if wg[akey] > 0.0:
    #        nwg += 1

    xps = {}
    cps = {}

    if norelax:

        xpars = ['K1','C1X','C2X','DX','B3X','A1']
        cpars = ['C1C','C2C','DC','B1C','B2C','B3C','B4C']

        for akey in xpars:
            xps[akey] = xparams[akey]
        for akey in cpars:
            cps[akey] = cparams[akey]
        xps[conpar] = pars#[0]
        cps[conpar] = pars#[0]

    else:

        xpars = ['K1','C1X','C2X','DX','B3X']
        cpars = ['C1C','C2C','DC']
        par_order = [t for t in xpars]
        for t in cpars:
            par_order.append(t)

        cons_x_pars = ['A1',conpar]
        for akey in cons_x_pars:
            xps[akey] = xparams[akey]

        cons_c_pars = ['B1C','B2C','B3C','B4C',conpar]
        for akey in cons_c_pars:
            cps[akey] = cparams[akey]

        for ikey,akey in enumerate(par_order):
            if akey in xpars:
                xps[akey] = pars[ikey]
            elif akey in cpars:
                cps[akey] = pars[ikey]

    wtodo = {'SAXC': True, 'NSAXC': True, 'LZBXpe': True, 'LZBCpe': True,
    'JSFXC': Settings.DO_JELLIUM, 'AR2': Settings.DO_AR2}
    for akey in wtodo:
        if wg[akey] == 0.0:
            wtodo[akey] = False

    outd = {'SAXC': 0.0, 'NSAXC': 0.0, 'LZBX': 0.0, 'LZBC': 0.0, 'LZBXpe': 0.0, \
        'LZBCpe':0.0, 'JSFXC': 0.0, 'SRES': 0.0, 'AR2': 0.0, 'SRESV': []}
    for akey in xps:
        outd[akey] = xps[akey]
    for akey in cps:
        outd[akey] = cps[akey]

    # spherical atoms
    if wtodo['SAXC']:
        exl = get_Ex_for_atoms(full_dens, xps)
        ecl = get_Ec_for_atoms(full_dens, cps)

        num_xc = [ecl[i] + exl[i] - Benchmarks.BM_XC[i] for i in range(len(ecl))]
        err_xc = [100*(ecl[i] + exl[i] - Benchmarks.BM_XC[i])/np.abs(Benchmarks.BM_XC[i]) for i in range(len(ecl))]
        outd['SAXC'] = np.sqrt(np.mean(np.array(err_xc)**2))

    # Non-spherical atoms
    if wtodo['NSAXC']:
        exl_nsp = get_Ex_for_atoms(ns_dens, xps)
        ecl_nsp = get_Ec_non_colinear(ns_dens, cps)

        num_xc_nsp = [ecl_nsp[i] + exl_nsp[i] - Benchmarks.NON_SPHERICAL_XC[i] for i in range(len(ecl_nsp))]
        err_xc_nsp = [100*(ecl_nsp[i] + exl_nsp[i] - Benchmarks.NON_SPHERICAL_XC[i])/np.abs(Benchmarks.NON_SPHERICAL_XC[i]) for i in range(len(ecl_nsp))]
        outd['NSAXC'] = np.sqrt(np.mean(np.array(err_xc_nsp)**2))

    if wtodo['LZBXpe']:
        _, outd['LZBX'] = get_lzlx_coef(xps, lz_dens)
        outd['LZBXpe'] = 100*((Benchmarks.LZ_X_B - outd['LZBX'])/abs(Benchmarks.LZ_X_B))

    if wtodo['LZBCpe']:
        outd['LZBC'] = get_lzlc_coef(cps, lz_dens)
        outd['LZBCpe'] = 100*((Benchmarks.LZ_C_B - outd['LZBC'])/abs(Benchmarks.LZ_C_B))

    if wtodo['JSFXC']:
        jerr_xcs = []
        for i, rs in enumerate(Settings.JELLIUM_RS_LIST):
            ex = jellies[i].eval_x_func(SCAN2.getscan_x, xps)
            ec = jellies[i].eval_c_func(SCAN2.getscan_c, cps)
            exc = ex + ec
            jerr_xcs.append(100*(Benchmarks.JELLIUM_XC[i] - exc)/abs(Benchmarks.JELLIUM_XC[i]))

        outd['JSFXC'] = np.sqrt(np.mean(np.array(jerr_xcs)**2))

    if wtodo['AR2']:
        exl_ar2 = get_Ex_for_atoms(ar2_dens, xps)*Benchmarks.EH_TO_KCAL
        ecl_ar2 = get_Ec_for_atoms(ar2_dens, cps)*Benchmarks.EH_TO_KCAL
        exc_ar2 = exl_ar2 + ecl_ar2
        etot = {}

        for i, m in enumerate(Benchmarks.AR2_LIST):
            etot[m] = exc_ar2[i] + Benchmarks.AR2_NO_XC[m]

        seps = ["1.6", "1.8", "2.0"]

        cbs_a = jsextr(np.array([2,3,4]), np.array([etot['ArDZ'], etot['ArTZ'], etot['ArQZ']]))

        ar2_err = []
        for k,s in enumerate(seps):
            cbs_e = jsextr(np.array([2,3,4]), np.array([etot[s+'DZ'], etot[s+'TZ'], etot[s+'QZ']]))
            ar2_err.append(cbs_e - 2*cbs_a - Benchmarks.AR2_BM[s])
            outd['AR2'] += 100*abs(ar2_err[k])/(len(seps)*Benchmarks.AR2_BM[s])

    for akey in wg:
        outd['SRES'] += abs(wg[akey]-outd[akey])#abs(wg[akey]*outd[akey])
        if wg[akey] > 0.0:
            outd['SRESV'].append(wg[akey]-outd[akey])
    #outd['SRES'] = abs(outd['SRES']-nwg)/nwg

    if only_res:
        return outd['SRES']

    return outd


def opt_scan_nu_relax(full_dens,lz_dens,ns_dens,jellies,ar2_dens):

    xparams = dict( K1 = 0.065, A1 = 4.9479, C1X = 0.667, C2X= 0.8, DX = 1.24, B3X=0.5)

    cparams = dict(C1C = 0.64, C2C = 1.5, DC = 0.7, B1C = 0.0285764,
        B2C = 0.0889, B3C = 0.125541, B4C = 2.3631)

    nu_l = np.linspace(0.0,nu_max,51)

    par_order = ['K1','C1X','C2X','DX','B3X','C1C','C2C','DC']

    nkeys = ['SAXC','NSAXC','LZBXpe','LZBCpe']

    if Settings.DO_JELLIUM:
        nkeys.append('JSFXC')
    if Settings.DO_AR2:
        nkeys.append('AR2')
    nkeys.append('SRES')

    tstr = conpar.lower()
    for akey in par_order:
        tstr += ', {:}'.format(akey)
    for akey in nkeys:
        tstr +=', {:}'.format(akey)
    tstr += '\n'

    for inu,nu in enumerate(nu_l):

        xparams[conpar] = nu
        cparams[conpar] = xparams[conpar]
        wrap_errs = partial(scan_nu_errs, xparams=xparams, cparams=cparams, \
            full_dens=full_dens, lz_dens=lz_dens, ns_dens=ns_dens,\
            jellies=jellies, ar2_dens=[None],uwg={'AR2':0.0},only_res=True)

        c0l = []
        bdsl = []
        for akey in par_order:

            if akey in xparams:
                tval = xparams[akey]
            elif akey in cparams:
                tval = cparams[akey]
            c0l.append(tval)
            if akey == 'DX':
                bdsl.append((0.95*tval,min(1/0.174,1.05*tval)))
            else:
                bdsl.append((0.95*tval,1.05*tval))

        if inu == -1:
            tps = [tmpx for tmpx in c0l]
        else:
            opt_res = minimize(wrap_errs,c0l, method='Nelder-Mead', bounds=bdsl)
            tps = opt_res.x
            #print(nu,opt_res.x)

        for ikey,akey in enumerate(par_order):
            if akey in xparams:
                xparams[akey] = tps[ikey]
            elif akey in cparams:
                cparams[akey] = tps[ikey]

        tstr += '{:}'.format(nu)
        for akey in par_order:
            if akey in xparams:
                tval = xparams[akey]
            elif akey in cparams:
                tval = cparams[akey]
            tstr += ', {:}'.format(tval)

        tres = scan_nu_errs(tps, xparams, cparams, full_dens, lz_dens,\
            ns_dens, jellies, ar2_dens, only_res = False)

        for akey in nkeys:
            tstr +=', {:}'.format(tres[akey])
        tstr += '\n'

        ofl = open('./scan_nu/scan_{:}_nu_relaxed.csv'.format(ind),'w+')
        ofl.write(tstr)
        ofl.close()

    return

def opt_scan_nu_fixed(full_dens,lz_dens,ns_dens,jellies,ar2_dens):

    xparams = dict( K1 = 0.065, A1 = 4.9479, C1X = 0.667, C2X= 0.8, DX = 1.24, B3X=0.5)

    cparams = dict(C1C = 0.64, C2C = 1.5, DC = 0.7, B1C = 0.0285764,
        B2C = 0.0889, B3C = 0.125541, B4C = 2.3631)

    nu_l = np.linspace(0.0,nu_max,101)

    nkeys = ['SAXC','NSAXC','LZBXpe','LZBCpe']

    if Settings.DO_JELLIUM:
        nkeys.append('JSFXC')
    if Settings.DO_AR2:
        nkeys.append('AR2')
    nkeys.append('SRES')

    tstr = conpar.lower()
    for akey in nkeys:
        tstr +=', {:}'.format(akey)
    tstr += '\n'

    wrap_errs = partial(scan_nu_errs, xparams=xparams, cparams=cparams, \
        full_dens=full_dens, lz_dens=lz_dens, ns_dens=ns_dens,\
        jellies=jellies, ar2_dens=ar2_dens,only_res=False,norelax=True)

    wkrs = Pool(processes=Settings.NPROC)
    tmp = wkrs.map(wrap_errs,nu_l)
    wkrs.close()

    for ielt,elt in enumerate(tmp):
        tstr += '{:}'.format(nu_l[ielt])
        for akey in nkeys:
            tstr +=', {:}'.format(elt[akey])
        tstr += '\n'

    ofl = open('./scan_nu/scan_{:}_nu_fixed.csv'.format(ind),'w+')
    ofl.write(tstr)
    ofl.close()

    return

def opt_select(LRELAX = True):

    lz_dens = generate_density(["Ne", "Ar", "Kr", "Xe"])
    fullist = Benchmarks.BM_LIST
    full_dens = generate_density(fullist)

    ns_dens = read_QUEST_densities([x+".out.plot" for x in Benchmarks.NON_SPHERICAL_LIST])

    ar2_list = []
    for f in Benchmarks.AR2_LIST:
        sep = f[:-2]
        bas = f[-2:]
        if sep == "Ar":
            ar2_list.append("./set_dens/SCAN/Ar2/Ar_"+bas+".out.plot")
        else:
            ar2_list.append("./set_dens/SCAN/Ar2/Ar2_"+sep+"_"+bas+".out.plot")

    if Settings.DO_AR2:
        ar2_dens = read_QUEST_restricted_densities(ar2_list)
    else:
        ar2_dens = []

    jellies = []
    if Settings.DO_JELLIUM:
        for rs in Settings.JELLIUM_RS_LIST:
            jellies.append(Jelly(rs))

    if LRELAX:
        opt_scan_nu_relax(full_dens,lz_dens,ns_dens,jellies,ar2_dens)
    else:
        opt_scan_nu_fixed(full_dens,lz_dens,ns_dens,jellies,ar2_dens)


if __name__=="__main__":

    opt_select(LRELAX = False)
