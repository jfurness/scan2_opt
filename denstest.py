import numpy as np
import Densities

r = np.arange(0.01,1.01,0.5)

li = Densities.Atom("Cu")
d0, d1, g0, g1, t0, t1, l0, l1 = li.get_densities(r)
print(d0, d1)
d0, d1, g0, g1, t0, t1, l0, l1 = Densities.Cu_plus(r)
print(d0, d1)
