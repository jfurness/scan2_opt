\documentclass[12pt]{article}

\usepackage[margin=1in]{geometry}

\usepackage[T1]{fontenc}
\usepackage{palatino}

\usepackage{amsmath,amssymb,bm,graphicx,multirow}
\usepackage{xcolor,xspace}

\newcommand{\ba}{\overline{\alpha}}
\newcommand{\rs}{r_\mathrm{s}}
\newcommand{\tw}{\tau_\mathrm{W}}
\newcommand{\tu}{\tau_0}
\newcommand{\sux}{_\mathrm{x}}
\newcommand{\suc}{_\mathrm{c}}

\begin{document}

\title{Meta-GGA ingredients and keywords in ``SCAN2.py''}
\author{ADK}
\date{\today}
\maketitle

SCAN2.py assumes that the exchange enhancement factor is of the form
\begin{equation}
  F\sux(p,w) = \{ h\sux^1(p,w) + f\sux(w)[h\sux^0 - h\sux^1(p,w)] \} g\sux(p).
\end{equation}
In this equation:
\begin{itemize}
  \item $w$ is an iso-orbital indicator, controlled by the ISOORB keyword
  \item $h\sux^1$ is the slowly-varying limit of $F\sux$, controlled by the H1X keyword
  \item $f\sux$ is the interpolation function that mixes iso-orbital and slowly-varying energy densities, controlled by the FINT keyword
  \item $h\sux^0 = 1.174$ is a constant (took out the Made-Simple 2 option, controlled by H0X keywork)
  \item $g\sux$ enforces the nonuniform coordinate scaling constraint, controlled by the GX keyword.
\end{itemize}
Both ISOORB and FINT are shared between exchange and correlation.

\clearpage
\section{ISOORB keywords}

Possible ISOORB keywords are listed in Table \ref{tab:isoorb_keys}.
Note that xps, which stores the working exchange model parameters, and cps, which stores the working correlation model parameters, should include ETA entries when ISOORB = BA or BNB, and NU entries when ISOORB = BN or BNB.

\begin{table}[!h]
  \centering
  \begin{tabular}{lrp{6cm}} \hline
    ISOORB & Variable & Notes \\ \hline
    A & $\alpha=(\tau-\tw)/\tu$ & \\ \hline
    BA & $\ba=(\tau-\tw)/(\tu + \eta \tw)$ & $\eta$ modulated by xps/cps dict. entry ETA \\ \hline
    B & $\beta = (\tau-\tw)/(\tau + \tu)$ & \\ \hline
    BN & $\beta_\nu = (\tau-\tw)/(\nu \tau + \tu)$ & purely testing, not rec. $\nu$ controlled by xps/cps dict. key NU \\ \hline
    BNB & $\overline{\beta}_\nu = (\tau-\tw)/[\nu \tau + \tu + (1-\nu)\eta \tw]$ & ditto, $\eta$ also controlled by dict key ETA \\ \hline
  \end{tabular}
  \caption{ISOORB keywords}
  \label{tab:isoorb_keys}
\end{table}

\section{H1X keywords}

The various $h\sux^1$ can be sorted into functions intended to recover either the second-order gradient expansion for exchange (GEX2: R2, R2reopt, MVS, R2NU) or the fourth-order expansion (GEX4: NEW, ORIGINAL, SCANNU).
We take $\mu=10/81$ and $\mu_4 = 146/2025$.

The function ``get\_gex\_pars'' determines the values of $f\sux'(1)$, $f\sux''(1)$, $h\sux^{1\prime}(0)$, and $h\sux^{1\prime\prime}(0)$ subject to the choice of both H1X and FINT.
If FINT chooses either the SCAN (FINT = ORIGINAL) or rSCAN (FINT = rSCAN), $f\sux'(1)$ is determined by the interpolation function.
Otherwise, they can be set by constraint.

\textbf{GEX2 keywords:} Unless FINT = ORIGINAL (SCAN interpolation function) or rSCAN (rSCAN interpolation function), $h_1$ is adjusted using xps['DA']$\leq 1$.
\begin{itemize}
  \item H1X = R2 selects the r$^2$SCAN $h\sux^1(p)$
  \begin{align}
    h\sux^1(p) &= 1 + \kappa_1 - \kappa_1[ 1 + x(p)/\kappa_1]^{-1} \\
    x(p) &= \left\{ (h_1 - \mu) \exp\left[-p^2/d_{p2}^4 \right] + \mu \right\} p \\
  \end{align}
  $\kappa_1=$xps['K1'], $d_{p2}=$xps['DP'].

  \item H1X = R2reopt selects a pole-free modification of the r$^2$SCAN
  \begin{equation}
    h\sux^1(p) = \frac{1 + \mu p + (1 + \kappa_1)(p/d_{p2}^2)^2}
      {1 + (\mu - h_1)p + (p/d_{p2}^2)^2}.
  \end{equation}
  $\kappa_1=$xps['K1'], $d_{p2}=$xps['DP'].

  \item H1X = MVS selects $h\sux^1 = 1$. Not recommended except for testing purposes.
\end{itemize}

\textbf{GEX4 keywords:}
\begin{itemize}
  \item H1X = NEW selects a pole-free rational polynomial
  \begin{equation}
    h\sux^1(p) = \frac{1 + \mu p + \mu(\mu - h_1)p^2 + (1+\kappa_1) (p/d_{p2}^2)^3}
      {1 + (\mu - h_1) p + [(\mu - h_1)^2 - h_2/2] p^2 + (p/d_{p2}^2)^3}
  \end{equation}
  $h_1 \equiv h\sux^{1\prime}(0)$ and $h_2 \equiv h\sux^{1\prime\prime}(0)$ are selected to enforce the GEX4 by ``get\_gex\_pars''.
  $\eta$, $g\sux'(0)$, and $g\sux''(0)$ are used to determine $h_1$ and $h_2$.
  $\kappa_1=$xps['K1'], $d_{p2}=$xps['DP'].

  \item H1X = ORIGINAL selects the original SCAN function
  \begin{align}
    h\sux^1(p) &= 1 + \kappa_1 - \kappa_1[ 1 + x(p,\alpha)/\kappa_1]^{-1} \\
    x(p,\alpha) &= \mu p + b_4 p^2 \exp\left[-\frac{|b_4|p}{\mu} \right]
      + \left\{b_{1\mathrm{x}} p - b_{2\mathrm{x}}(c_\nu \alpha - 1)
      \exp\left[-b_{3\mathrm{x}}(c_\nu\alpha - 1)^2  \right] \right\}^2 \\
    b_{4\mathrm{x}} &= \frac{\mu^2}{\kappa_1} - \frac{25}{16}\mu_4.
  \end{align}
  When ISOORB = BA,
  \begin{align}
    c_\nu &= 1 \\
    b_{1\mathrm{x}} &= \frac{7 - 9\eta}{12}\mu_4^{1/2} \label{eq:b1x_scan_ba} \\
    b_{2\mathrm{x}} &= \frac{9}{20}\mu_4^{1/2} \\
  \end{align}
  and when ISSOORB = A, set $\eta=0$ in Eq. \ref{eq:b1x_scan_ba}.
  When ISOORB = BN,
  \begin{align}
    c_\nu &= 1+\nu \\
    b_{1\mathrm{x}} &= \frac{(7-9\nu)\mu_4^{1/2}}{12} \label{eq:b1x_scan_bnu} \\
    b_{2\mathrm{x}} &= \frac{9(1+\nu)\mu_4^{1/2}}{20},
  \end{align}
  and when ISOORB = B, set $\nu=1$ in the above equations.
\end{itemize}

\section{FINT keywords}

Unless stated otherwise, the exchange and correlation interpolation functions $f(x)$ are presumed to have the same structure.
\begin{itemize}
  \item FINT = NEW: not recommended, deprecated. For use with GEX4. On the fence about removing this.

  \item FINT = R2reopt: a piecewise polynomial like the rSCAN interpolation function, but that satisfies a few auxiliary constraints.
  $f\sux'(1)$ and $f\sux''(1)$ are constrained to recover the GEX4, and $d^j f_\mathrm{x/c}/d x^j(x_0)$ is continuous for $j=0,1,2,3,4$.
  Assumes that the SCAN parameters are fixed $c_{1\mathrm{x}}=0.667$, $c_{2\mathrm{x}}=0.8$, $d\sux=1.24$, $c_{1\mathrm{x}}=0.64$, $c_{2\mathrm{x}}=1.5$, $d\sux=0.7$, although that can be changed if needed.

  \item FINT = SMTH: rational polynomial interpolation, can be used with GEX2 or GEX4.
  In both cases,
  \begin{align}
    f^\text{SMTH}(x) &= \frac{1 + c_1 x - (1+c_1)x^2}{1 + (c_0 + c_1)x + (1 + c_1)x^2/d} \\
    c_1 &= -\frac{2 d\sux + (1 + d\sux + c_0 d\sux)f\sux'(1)}{d\sux + (1+d\sux)f\sux'(1)}.
  \end{align}
  When GEX2 is enforced (H1X = R2, R2reopt, or MVS),
  \begin{equation}
    c_0 = f\sux'(0).
  \end{equation}
  $f\sux'(0)$ = xps['FX0'] and $d\sux$ = xps['DX'].

  When GEX4 is enforced, $c_0$ is constrains $f^{\text{SMTH}\prime\prime}(1)$
  \begin{equation}
    c_0 = \frac{2d\sux + 4(1 + d\sux)f\sux'(1) + 2 (1 + d\sux)[f\sux'(1)]^2 + (1+d\sux)f\sux''(1)}
        {d\sux f\sux''(1) - 2 [f\sux'(1)]^2}.
  \end{equation}

  For correlation, $c_{0\mathrm{c}}=$ cps['C1C'], $c_{1\mathrm{c}}=$ cps['C2C'], and $d\suc=$ cps['DC'].

  \item FINT = MVS: for exchange, uses the same SMTH $f\sux(x)$.
  For correlation, uses
  \begin{equation}
    f\suc(x) = \frac{(1-x)^3}{1 + (c_{1\mathrm{c}}-3) x + c_{2\mathrm{c}} x^2 + x^3/d\suc}
  \end{equation}
  such that $f\suc'(1)=-c_{2\mathrm{c}}$.
  $c_{1\mathrm{c}}=$ cps['C1C'], $c_{2\mathrm{c}}=$ cps['C2C'], and $d\suc=$ cps['DC'].

  \item FINT = SCAN: original SCAN interpolation function
  \begin{equation}
    f^\text{SCAN}(x) = \left\{\begin{array}{lr}
      \exp\left[ -\frac{c_1 x}{1-x}\right], & x < 1 \\
      -d \exp\left[ \frac{c_2}{1-x}\right], & x > 1
    \end{array} \right.
  \end{equation}
  $c_{1\mathrm{x}}=$ xps['C1X'], $c_{2\mathrm{x}}=$ cps['C2X'], and $d\sux=$ cps['DX'].
  $c_{1\mathrm{c}}=$ cps['C1C'], $c_{2\mathrm{c}}=$ cps['C2C'], and $d\suc=$ cps['DC'].

  \item FINT = rSCAN: rSCAN-style interpolalation function
  \begin{equation}
    f^\text{rSCAN}(x) = \left\{\begin{array}{lr}
      (1-x)\sum_{i=0}^6 b_i x^i, & x < x_0 \\
      -d \exp\left[ \frac{c_2}{1-x}\right], & x \geq x_0.
    \end{array} \right.
  \end{equation}
  The $b_i$ are determined from the following seven constraints
  \begin{align}
    f^\text{rSCAN}(0) &= 1 \\
    \frac{d^j f^\text{rSCAN}}{d x^j}(0) &= \frac{d^j f^\text{SCAN}}{d x^j}(0), \qquad j = 1,2 \\
    \frac{d^j f^\text{rSCAN}}{d x^j}(x_0) &= \frac{d^j f^\text{SCAN}}{d x^j}(x_0), \qquad j = 0,1,2,3.
  \end{align}
  Parameters are determined on-the-fly, although $x_0=2.5$ isn't modified (but easily can be).
  $c_{1\mathrm{x}}=$ xps['C1X'], $c_{2\mathrm{x}}=$ cps['C2X'], and $d\sux=$ cps['DX'].
  $c_{1\mathrm{c}}=$ cps['C1C'], $c_{2\mathrm{c}}=$ cps['C2C'], and $d\suc=$ cps['DC'].

  \item

\end{itemize}

\end{document}
