\documentclass{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage[margin=1in]{geometry}

\usepackage{bm}
\usepackage{lmodern}
\newcommand{\mol}[1]{\ensuremath{_{\text{#1}}}}
\newcommand{\mr}[1]{\ensuremath{\mathrm{#1}}}
\newcommand{\be}[0]{\ensuremath{\beta}}
\renewcommand{\vec}[1]{\ensuremath{\bm{#1}}}

\begin{document}

\title{SCAN2 Update for 2018}

\date{\today}

\maketitle

\section{Fixing $a_{1x}$}

Within the exchange function we have the parameter $a_{1x}$. In the original SCAN this enters as,
\begin{equation}
    g_x(s) = 1 - \exp\left[-a_{1x} s^{-1/2}\right],
\end{equation}
in SCAN2 we change this to,
\begin{equation}
    g_x(p) = 1 - \exp\left[-\frac{0.444}{p^{1/4}} - \frac{a_{1x}}{p^{1/2}}\right].
\end{equation}
We adjust $a_{1x}$ to minimise the error in the hydrogen exchange energy. We find,
\begin{table}[h]
\centering
    \begin{tabular}{l|ccc}
                   & Reference & SCAN & SCAN2 \\
        \hline
        $a_{1x}$   & $4.9479$ & $4.94869866704$  & $5.57996055999$ \\
        Err.       & $1.48E-6$ & $5.14E-11 E_h$ & $1.00E-11 E_h$
    \end{tabular}
\end{table}

\section{Fixing parameters in $\epsilon_c^0$ by 2 electron $r_s \rightarrow 0$ correlation}

For SCAN2, we have $5$ parameters to set in $\epsilon_c^0$ which is constructed as,
\begin{align}
    \epsilon_c^0(rs, s, \zeta; b_{1c}, b_{2c}, b_{3c}, \chi_2, \chi_4^2) &= (\epsilon_c^{\mr{LSDA}_0}(r_s; b_{1c}, b_{2c}, b_{3c}) + H_0(s; b_{1c}))G_c(\zeta; b_{4c})\\
    \epsilon_c^{\mr{LSDA}_0}(r_s;  b_{1c}, b_{2c}, b_{3c}) &= -\frac{b_{1c}}{1 + b_{2c}r_s^{1/2} + b_{3c}r_s} \\
    G_c(\zeta; b_{4c}) &= (1 - b_{4c}(d_x(\zeta) - 1))(1 - \zeta^{12})\\
    d_x(\zeta) &= ((1 + \zeta)^{4/3} + (1 - \zeta)^{4/3})/2\\
    H_0(s; b_{1c}, b_{2c}, b_{3c}, \chi_2, \chi_4^2) &= b_{1c}\log\left[1 + w(r_s;  b_{1c}, b_{2c}, b_{3c})(1 - g_0(s; \chi_2, \chi_4^2))\right] \\
    w(r_s, s;  b_{1c}, b_{2c}, b_{3c}) &= \exp\left[-\frac{\epsilon_c^{\mr{LSDA}_0}(r_s; b_{1c}, b_{2c}, b_{3c})}{b_{1c}}\right] - 1 \\
    g_0(s; \chi_2, \chi_4^2) &= \frac{1}{1 + \chi_2 s^2 + \chi_4^2 s^4}\label{eq:g0_chi}
\end{align} 

We are initially only concerned with spin unpolarised norms. So $\zeta = 0$ and we can ignore $b_{4c}$. We must find, in order,
\begin{equation}
    \{\chi_2, \chi_4^2, b_{1c}, b_{3c}, b_{2c}\}
\end{equation}

We have tried restricting to a $\chi_2 = \chi_4$, but in this case we cannot simultaneously obey condition \ref{cond:esp0} and achieve an accuracy in \ref{cond:excHe} of $< 10^{-4} E_h$.

We want to fit these parameters such that, in order of importance, the following conditions are met,
\begin{enumerate}
    \item{\label{cond:esp0}$\epsilon_c^1(r_s) \leq \epsilon_c^0(r_s)$ for all $s$ and $r_s$ (including $r_s \rightarrow 0$).}
    \item{\label{cond:lsda0}$\epsilon_c^{\mr{LSDA1}}(r_s) \leq \epsilon_c^{\mr{LSDA0}}(r_s)$ for all $r_s$.}
    \item{\label{cond:zinf}The $Z \rightarrow \infty$ 2 electron correlation energy is $-0.0467\pm10^{-9} E_h$.}
    \item{\label{cond:excHe}The exchange-correlation energy of helium is $-1.068\pm10^{-9} E_h$.}
    \item{\label{cond:b1c}$b_{1c}$ is as small as possible.}
\end{enumerate}

{\color{red}Condition \ref{cond:b1c} is justified as maximising the locality of $\epsilon_{\mr{c}}^0$.}

\subsection{Fitting Process}
We make a sweep of $\{(\chi_2, \chi_4^2) \;|\; \chi_2 \in \{0, 2\} \otimes \chi_4^2 \in \{0, 2\} \}$. This becomes quite expensive as the sweep is made more precise, so we follow an adaptive grid scheme. In this we sequentially set each decimal in a grid around the previous.

For each $(\chi_2, \chi_4^2)$,
\begin{enumerate}
    \item Determine $b_{1c}$ that minimizes error in the 2 electron $Z \rightarrow \infty$ correlation energy. Reject any that cannot satisfy condition \ref{cond:zinf}.
    \item Numerically check condition \ref{cond:esp0} for $r_s \rightarrow \infty$ by direct comparison for 500 values of $s \in \{10^{-2}, 10^{5}\}$ evenly distributed in $\log_{10}$ space, reject any that break this.
    \item $b_{3c}$ is determined as $b_{3c} = b_{1c}/([1.67082-1.174]\{3.0/(4.0\pi)*(9.0\pi/4.0)^{1.0/3.0}\})$.
    \item Determine $b_{2c}$ that minimizes error in E\mol{xc}[He]. Reject any that cannot satisfy condition \ref{cond:excHe}.
    \item Numerically check condition \ref{cond:esp0} for $r_s = 1$ by direct comparison for 500 values of $s \in \{10^{-2}, 10^{5}\}$ evenly distributed in $\log_{10}$ space, reject any that break this.
    \item Numerically check condition \ref{cond:lsda0} by direct comparison for 500 values of $r_s \in \{10^{-2}, 10^{5}\}$ evenly distributed in $\log_{10}$ space, reject any that break this.
    \item Select the smallest $b_{1c}$ from the remaining parameter sets, following \ref{cond:b1c}
\end{enumerate}

Following this we find the parameter set,

\begin{table}[h]
\label{tab:Cparams}
\centering
    \begin{tabular}{ccccc}
        $\chi_2$ & $\chi_4^2$ & $b_{1c}$ & $b_{2c}$ & $b_{3c}$ \\
        \hline
        $0.000000$ & $0.444228$ & $0.040965$ & $0.100770$ & $0.179965$
    \end{tabular}
\end{table}

Figure \ref{fig:ec1ec0curves} shows the comparison of $\epsilon_c^0$ and $\epsilon_c^1$ for a range of $s$ and $r_s$.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.95\textwidth]{Graphics/FirstFit.pdf}
    \caption{Curves of $e_c^0(r_s)$ and $e_c^1(r_s)$ from the parameters of Table \ref{tab:Cparams}.}
    \label{fig:ec1ec0curves}
\end{figure}

These parameters give final energies for the helium atom of,
\begin{table}[h!]
\centering
    \begin{tabular}{cc}
        $E_{\mr{x}}[\mr{He}]$ & $-1.03109758381 E_h$ \\
        $E_{\mr{c}}[\mr{He}]$ & $-0.03690241621 E_h$
    \end{tabular}
\end{table}

\section{Alternative $g_0$ function in $\epsilon_{\mr{c}}^0$}

John proposed a simpler alternative to Eq. \ref{eq:g0_chi} around maximising the locality of $\epsilon_{\mr{c}}^0$,
\begin{equation}
    g_0(s) = 1 - \exp\left[\frac{-a_{1c}}{s^4}\right],
    \label{eq:g0_new}
\end{equation}
such that at small $s$, $g_0(s)$ is nrealy 1 and $\epsilon_{\mr{c}}^0$ is nearly local. Thus, we should try to maximise $a_{1c}$ without breaking our constraints. This argument reveals why $\chi_2$ should optimise to zero, this makes Eq. \ref{eq:g0_chi} as nearly local as possible.

On implementing and testing this we find that there is a small range of $s$ and $r_s$ values for which condition \ref{cond:esp0} is broken. This seems to be made worse with larger $a_{1c}$ and `peaks' around $r_s = 0.14$. See Figure \ref{fig:g0_viol}. This reveals the violation to be relatively mild in the worst cases and only under energetically negligible conditions. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.475\textwidth]{Graphics/worst_a1c_1_full.png}
    \includegraphics[width=0.475\textwidth]{Graphics/worst_a1c_1.png}
    \caption{Violation of condition \ref{cond:esp0} for Eq. \ref{eq:g0_new}.}
    \label{fig:g0_viol}
\end{figure}

We continued exploring it for a complete understanding. We weaken condition \ref{cond:esp0} to only requiring adherence for $r_s \rightarrow 0$ and $r_s = 1$. Maximising $a_{1c}$ (and re-determining $\{b_{1c}, b_{2c}, b_{3c}\}$) without breaking these modified conditions we find (to a precision of $\pm0.001$ in $a_{1c}$) are shown in table\ref{tab:g0params}.
\begin{table}[h!]
    \caption{Simplified $\epsilon_{\mr{c}}^0$ parameters\label{tab:g0params}}
    \centering
\begin{tabular}{cccc}
    $a_{1c}$ & $b_{1c}$ & $b_{2c}$ & $b_{3c}$ \\
    \hline
    1.31088867187 & 0.0378089856501 & 0.111827704234 & 0.166101581696
\end{tabular}
\end{table}

This is found to not be so bad. A search for $(r_s, s)$ that violate condition \ref{cond:esp0} was made and the results are shown in Figure \ref{fig:g0_scan}.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.95\textwidth]{Graphics/g0_violations.png}
    \caption{Search for points at which condition \ref{cond:esp0} is broken, highlighted red.}
    \label{fig:g0_scan}
\end{figure}

We can take what we have learned with this exploration then and use it to better justify condition \ref{cond:b1c} as maximising the locality of $\epsilon_{\mr{c}}^0$.

\section{Interpolation-Extrapolation work}

We have been trying a number of different forms for the interpolation-extrapolation (IE). First we confirmed things were working by running the original SCAN IE function. We have been using the large Z expansion coefficients as our test system. The original SCAN IE gave good results with small changes to parameters to account for the new changes to x in the exchange functional.

Moving forward we looked at the function,
\begin{equation}
    f(\beta) = \frac{(1 - 2\beta)^3(1 + A\beta + B\beta^2)}{(1+\beta^3)(1+\frac{B}{d}\beta^2)}
\end{equation}
this is somewhat dangerous in pseudopotential codes however as $\beta$ could become negative.

\subsection{Simple Polynomial}

We would prefer to use the more simple,
\begin{equation}
    f(\beta) = (1-2\beta)^3(1 + A\beta + B\beta^2 + C\beta^3).
    \label{eq:polyEI}
\end{equation}
Getting a feel for the parameter space we realise it is better to target the limit of $\beta \rightarrow 1$than $C$, such that we can define $\{A, B, f_1\}$ and determine $C$ accordingly,
\begin{equation}
    C = -(1 + A + B + f_1).
\end{equation}
We then set some bounds on the parameter search space. These were somewhat set by us exploring by hand for what worked well, and plotting the result. It seems that the $\beta = [0, 0.5]$ range is most energetically important, meaning the $\beta = [0.5, 1]$ range is difficult to target and can go crazy if freely optimised against large Z limit. We then performed a search in,
\begin{equation}
    A \in [0, 6; 0.05],\; B \in [-10, 50; 0.125], \; f_1 \in [-3, 0; 0.0125].
\end{equation}
We began by filtering away all combinations that were not monotonic across $[0, 1]$, leaving $3,325,879$ combinations to consider.

From this, the optimum was found as Figure \ref{fig:polyEI}. When $k_1 = 0.064$ it resulted in an error in large Z exchange coefficients of $a = -35.96\%$ and $b = -11.39\%$, with rare gas atom exchange energies given in Table \ref{tab:polyRG}. With this information in hand we can see that it is Xe that is bucking the trend and probably throwing off the large Z fit considerably. Maybe we should plot the interpolation results for this and see what's what and what areas of the function are important.

\begin{table}[h!]
    \caption{Rare gas atom energies for SCAN2 with Eq. \ref{eq:polyEI} as EI function and original SCAN. \label{tab:polyRG}}
    \centering
\begin{tabular}{crr}
       &     SCAN2\mol{POLY}   &     Original \\
    \hline
    He &   -1.031 & \\
    Ne &  -12.287 &  -12.108\\
    Ar &  -30.469 &  -30.188\\
    Kr &  -94.136 &  -93.890\\
    Xe & -179.044 & -179.200\\
\end{tabular}
\end{table}


\begin{figure}[h]
    \centering
    \includegraphics[width=0.95\textwidth]{Graphics/BestPoly.pdf}
    \caption{Optimal parameters of $\{A = 5.95, B = 12.25, C = -16.2\}$ for the EI function of Eq. \ref{eq:polyEI}, plotted against the original SCAN EI.}
    \label{fig:polyEI}
\end{figure}

\subsection{``Pad\v{e}'' Like}

If this fails then we will switch to using what we have been calling a Pad\v{e}-approximant-like,
\begin{equation}
    f(\beta) = \frac{(1-2\beta)^3(1 + A\beta + B\beta^2)}{1 + C\beta^4}.
\end{equation}
This is safe and gives us some ability to target the large $\beta$ domain with $C$. We can play a similar game of controlling the $\beta = 1$ point by optimising parameters as $\{A, B, f_1\}$, then determining $C$ as,
\begin{equation}
    C = -\left[\frac{1 + A + B}{f_1} + 1\right].
\end{equation}
To begin with we took the same range of parameters and ran that scan, the optimum for $k_1 = 0.064$ were $\{A = 0.0, B = 49.5, C = 15.83\}$ with $a = -11.68\%$ and $b = -0.013\%$. I'm opening up the range a bit and trying again. That gives $\{A = -3.5, B = 65.5, C = 20.2\}$ with an error of $a = -6.66\%, b = 0.10\%$.

\subsection{Modified Pade thingy}

First results came back as Best A: -0.125, B: 62.5, C: 0.0, D: 50.1088709677. Error: a = -21.4602758994%, b = -2.61679984057%.
% ('He', -1.0310975838066918)
% ('Ne', -12.311560674228742)
% ('Ar', -30.609399390547168)
% ('Kr', -94.4903307249046)
% ('Xe', -179.5481250954404)


\subsection{Fixing $F_x(\beta = 1, p = 0) = 0$}

We want to set $F_x(\beta = 1, p = 0) = 0$, so we must solve the IE function for $\beta = 1$ to obey this, denoted $f_1$. We know $g_x = 1$ so we can ignore that. So, assuming $b_{1x} = 1$,
\begin{align}
    \tilde q &= \frac{9}{10} \\
    x &= \frac{146}{4525} \\
    h_{1x} &= 1 + k_1 -\frac{k_1}{1 + \frac{x}{k_1}} \\
    0 &= h_{1x} + f_1\left[h_{0x} - h_{1x}\right] \\
    0 &= 1 + k_1 - \frac{k_1}{1 + \frac{146}{4525k_1}} + f_1\left[1.174 - \left\{1 + k_1 - \frac{k_1}{1 + \frac{146}{4525k_1}}\right\}\right] \label{eq:xfac}\\
    f_1 &= \frac{2,335,500k_1 + 73,000}{320,675k_1 + 12,702}
\end{align}

\section{A New Set of Norms}

If we take the `accurate' exchange energies and extrapolate to the large Z limit then we find an error on the order of $\sim 5\%$. So aiming for the large Z expansions as a goal is probably unwise. We can then understand why original SCAN was able to get close to the extrapolations, but all of our $\beta$ forms cannot. If we look at the plots of the density averaged indicator:
\begin{equation}
    \langle \beta \rangle = \int d n_\downarrow(\vec r)\beta_\downarrow(\vec r) + n_\uparrow(\vec r)\beta_\uparrow(\vec r) \vec r
\end{equation}
vs. $1/Z$ then we find that $\alpha$ approaches the theoretical limit of $1$ much faster than $\beta$. The real atoms we are looking at have core 1S regions that do not properly follow this approach to the uniform density limit. So lighter atoms are less reliable and must be excluded. For $\beta$ this is more severe than $\alpha$ and hence the extrapolation from real atom calculations is slower to converge, see Figure \ref{fig:lZiso}. We can show that both our $\beta$ form and the original SCAN ultimately must take the correct limit however.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.475\textwidth]{Graphics/alpha_Z.pdf}
    \includegraphics[width=0.475\textwidth]{Graphics/beta_Z_fits.pdf}
    \caption{Large Z limit extrapolations of density averaged iso-orbital indicator quantities, made from the real atoms \{Ne, Ar, Kr, Xe, Rn\}. Ignore the `quadratic' fit for $\beta$ plot.}
    \label{fig:lZiso}
\end{figure}

Instead we have decided to look at the exchange and correlation energies of spherical atoms independently: [Li, N, Ne, Na, P, Ar, K, Cr, Cu, As, Kr, Xe]. If we wish only to match the original SCAN's performance on this problem then we have to meet or beat an root mean square error (RMSE) of $0.3222\%$ in exchange and $7.3745\%$ in correlation. In combination this gives an XC RMSE of $0.1638\%$. We want to do at least as well as that.

\subsection{Understanding $\beta$ in IE Functions}

% (10/81)*p + (146/2025)*q^2/(1 + q^2/b^2) - (73/405)*q*p/(sqrt(1+q^2/b^2)*sqrt(1+p^2/b^2)) + 1/k*(10/81)^2*p^2/(1 + p^2/b^2)

\subsection{Proving $F_{\mr{x}} \geq 0$ for all $s$ when $\beta = 1$}

With 
\begin{equation}
    f_{\mr{x}}(\beta = 1) = \frac{2,335,500k_0 + 73,000}{320,675k_0 + 12,702},
\end{equation}
then 
\begin{align}
    F_{\mr{x}}[\beta = 1, p] &= \frac{2,335,500k_0 + 73,000}{320,675k_0 + 12,702}(1.0 + k_0) + \left(1 - \frac{2,335,500k_0 + 73,000}{320,675k_0 + 12,702}\right)h_{1\mr{x}}.
\end{align}
Setting $F_{\mr{x}}[\beta = 1, p] \geq 0$ and solving with the condition that $k_0 > 0$ we find, 
\begin{equation}
     F_{\mr{x}}[\beta = 1, p] \geq 0,\; \text{if},\; h_{1\mr{x}} \leq 1.36085
\end{equation}
as,
\begin{equation}
    h_{1\mr{x}} = 1 + k_1 - \frac{k1}{1 + \frac{x(\beta = 1, p)}{k_1}}
\end{equation}
and $x(\beta = 1, p) > 0,\; \forall p$, then,
\begin{equation}
    h_{1\mr{x}} \leq 1 + k_1, \forall p.
\end{equation}
So $F_{\mr{x}}[\beta = 1, p] \geq 0$ for all $p$ if $0 < k_1 \leq 0.36085$. The Lieb--Oxford bound that imposes $k_1 < 0.174$ is sufficient then to ensure $F_{\mr{x}}[\beta = 1, p] \geq 0$.

We can confirm $x \geq 0$ as
\begin{equation}
    \tilde{q}(\beta = 1, s) = \frac{9}{10} + \frac{17p}{12},
\end{equation}
Substituting this to find $x(\beta = 1, s)$ in Eq. \ref{eq:xfac},
\begin{equation}
    x(\beta = 1, p) = \frac{10}{81}p
    + \frac{146}{2025}\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)^2}{\left(\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)^2}{b_1^2} + 1\right)}
    -\frac{73}{405}\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)p}{\sqrt{\frac{p^2}{b_1^2} + 1}\sqrt{\frac{\left(\frac{17p}{12}+\frac{9}{10}\right)^2}{b_1^2}+1}} 
    + \frac{100}{6561}\frac{1}{k}\frac{p^2}{\left(\frac{p^2}{b^2} + 1\right)}
\end{equation}
we then need to prove that,
\begin{align}
    \frac{10}{81}p
    + \frac{146}{2025}\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)^2}{\left(\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)^2}{b_1^2} + 1\right)}
    + \frac{100}{6561}\frac{1}{k}\frac{p^2}{\left(\frac{p^2}{b^2} + 1\right)}
    &\geq
    \frac{73}{405}\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)p}{\sqrt{\frac{p^2}{b_1^2} + 1}\sqrt{\frac{\left(\frac{17p}{12}+\frac{9}{10}\right)^2}{b_1^2}+1}} \\
    \frac{405}{73}\left[\frac{10}{81}p
    + \frac{146}{2025}\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)^2}{\left(\frac{\left(\frac{17p}{12} + \frac{9}{10}\right)^2}{b_1^2} + 1\right)}
    + \frac{100}{6561}\frac{1}{k}\frac{p^2}{\left(\frac{p^2}{b^2} + 1\right)}\right]
    &\geq
    \frac{\left(\frac{17p}{12} + \frac{9}{10}\right)p}{\sqrt{\frac{p^2}{b_1^2} + 1}\sqrt{\frac{\left(\frac{17p}{12}+\frac{9}{10}\right)^2}{b_1^2}+1}} 
\end{align}


\section{SCAN2 Functional Derivatives}

\subsection{Generics}
\begin{align}
    c_F &= \frac{3}{10}(3\pi^2)^{2/3} \\
    \gamma &= |\nabla n|^2 \\
    \beta &= \frac{\tau - \tau^{\mr{vW}}}{\tau + \tau^{\mr{UEG}}} \\
    \frac{\partial \beta}{\partial n} &= \frac{\gamma}{8n^2(\tau^{\mr{UEG}} + \tau)} - \frac{5c_Fn^{2/3}\left(\tau - \tau^{\mr{vW}}\right)}{3(\tau^{\mr{UEG}} + \tau)^2} \\
    \frac{\partial \beta}{\partial \gamma} &= -\frac{1}{8c_Fn^{8/3} + 8n\tau} \\
    \frac{\partial \beta}{\partial \tau} &= \frac{\tau^{\mr{UEG}} + \tau^{\mr{vW}}}{(\tau + \tau^{\mr{UEG}})^2}
\end{align}

% Checked with Finite difference.
\begin{align}
    f(\beta) &= (1 - 2\beta)^3(1 + A\beta + B\beta^2 + C\beta^3)\\
    \frac{\mr{d}f}{\mr{d}\beta} &= -(1 - 2\beta)^2(A(8\beta - 1) + 2B(5\beta - 1)\beta + 12C\beta^3 - 3C\beta^2 + 6)
\end{align}

\subsection{Exchange Functional}

\begin{align}
    h_{\mr{x}0} &= 1 + k_0\\
    \frac{\partial h_{\mr{x}0}}{\partial p} &= 0\\
    \frac{\partial h_{\mr{x}0}}{\partial \beta} &= 0 \\
    \tilde{q} &= \frac{9}{10}(2\beta - 1) + \frac{17}{12}p \\
    \frac{\partial \tilde{q}}{\partial p} &= \frac{17}{12} \\
    \frac{\partial \tilde{q}}{\partial \beta} &= \frac{9}{5}\\
\end{align}
\begin{align}
    x &= \frac{10}{81}p + \frac{146}{2025}\frac{\tilde{q}^2}{1 + \frac{\tilde{q}^2}{b_{1\mr{x}}^2}} - \frac{73}{405}\frac{\tilde{q}p}{\sqrt{1 + \frac{\tilde{q}^2}{b_{1\mr{x}}^2}}\sqrt{1 + \frac{p^2}{b_{1\mr{x}}^2}}} + \frac{1}{k_1}\left(\frac{10}{81}\right)^2\frac{p^2}{1 + \frac{p^2}{b_{1\mr{x}}^2}} \\
    \frac{\partial x}{\partial p} &= 
        \frac{10}{81} + 
        \frac{292}{2025}\frac{b_{1\mr{x}}^2\tilde{q}}{(b_{1\mr{x}}^2 + \tilde{q}^2)^2}\left(\frac{\partial \tilde{q}}{\partial p}\right) -
        \frac{73}{405}\frac{1}{\sqrt{1 + \frac{p^2}{b_{1\mr{x}}^2}}\sqrt{1 + \frac{\tilde{q}^2}{b_{1\mr{x}}^2}}}
            \left\{
                p\left(\frac{\partial \tilde{q}}{\partial p}\right) - 
                \frac{p\tilde{q}^2\left(\frac{\partial \tilde{q}}{\partial p}\right)}{\tilde{q}^2 + b_{1\mr{x}}^2} - 
                \frac{p^2\tilde{q}}{p^2 + b_{1\mr{x}}^2} + 
                \tilde{q}
            \right\} +
        \frac{200}{6561k_1}\frac{b_{1\mr{x}}^4p}{(b_{1\mr{x}}^2 + p^2)^2} \\
    \frac{\partial x}{\partial \tilde{q}} &= 
        \frac{292}{2025}\frac{b_{1\mr{x}}^4\tilde{q}}{(b_{1\mr{x}}^2 + \tilde{q}^2)^2} - 
        \frac{73p}{405\sqrt{1 + \frac{p^2}{b_{1\mr{x}}^2}}\sqrt{1 + \frac{\tilde{q}^2}{b_{1\mr{x}}^2}}}\left(1 - \frac{\tilde{q}^2}{b_{1\mr{x}}^2*(1 + \frac{\tilde{q}^2}{b_{1\mr{x}}^2})}\right) \\
    \frac{\partial x}{\partial \beta} &= \left(\frac{\partial x}{\partial \tilde{q}}\right) \left(\frac{\partial \tilde{q}}{\partial \beta}\right)
\end{align}

\begin{align}
    h_{1\mr{x}} &= 1 + k_1 - \frac{k_1}{1 + \frac{x}{k_1}} \\
    \frac{\mr{d} h_{1\mr{x}}}{\mr{d} x} &= \frac{k_1^2}{(k_1 + x)^2} \\
    \frac{\partial h_{1\mr{x}}}{\partial p} &= \left(\frac{\mr{d} h_{1\mr{x}}}{\mr{d} x}\right) \left(\frac{\partial x}{\partial p}\right) \\
    \frac{\partial h_{1\mr{x}}}{\partial \beta} &= \left(\frac{\mr{d} h_{1\mr{x}}}{\mr{d} x}\right) \left(\frac{\partial x}{\partial \beta}\right)
\end{align}

\begin{align}
    g_{\mr{x}} &= 1 - \exp\left[\frac{-0.444}{p^{1/4}} - \frac{a_1}{\sqrt{p}}\right] \\
    \frac{\mr{d}g_{\mr{x}}}{\mr{d}p} &= -\frac{a_1 + 0.222p^{1/4}}{2p^{3/2}} \exp\left[-\frac{a_1 + 0.444p^{1/4}}{\sqrt{p}}\right]
\end{align}

\begin{align}
    F_{\mr{x}} &= \left[h_{\mr{x}1} + f_{\mr{x}}(\beta)(h_{\mr{x}0} - h_{\mr{x}1})\right]g_{\mr{x}} \\
    \frac{\partial F_{\mr{x}}}{\partial p} &= \frac{\partial g_{\mr{x}}}{\partial p}(-f_{\mr{x}}(\beta)h_{1\mr{x}} + h_{1\mr{x}} + f_x(\beta)h_{0\mr{x}}) - (f_x(\beta) - 1)g_{\mr{x}}\frac{\partial h_{1\mr{x}}}{\partial p} \\
    \frac{\partial F_{\mr{x}}}{\partial \beta} &= g_{\mr{x}}\left[(h_{0\mr{x}} - h_{1\mr{x}})\frac{\mr{d}f_{\mr{x}}}{\mr{d}\beta} - (f_{\mr{x}}(\beta) - 1)\frac{\partial h_{1\mr{x}}}{\partial \beta}\right]
\end{align}

\subsection{Correlation Functional}

\subsection{$\epsilon_{\mr{c}}^1$}

\begin{align}
    w &= \exp \left[-\frac{\epsilon_{\mr{LSDA}}^1}{\phi^3\Gamma}\right] \\
    \frac{\partial w}{\partial r_s} &= -(w + 1)\frac{\frac{\partial \epsilon_{\mr{LSDA}}^1}{\partial r_s}}{\Gamma\phi^3} \\
    \frac{\partial w}{\partial \zeta} &= -(w + 1)\frac{\frac{\partial \epsilon_{\mr{LSDA}}^1}{\partial r_s} - \frac{3\epsilon_{\mr{LSDA}}^1\frac{\mr{d}\phi}{\mr{d}\zeta}}{\phi}}{\Gamma\phi^3}
\end{align}

\begin{align}
    P(t) &= \frac{1 + t/p_{\tau}}{1 + \tilde{c}\frac{t}{p_{\tau}}} \\
    \frac{\mr{d}P}{\mr{d}t} &= \frac{p_{\tau} - \tilde{c}p_{\tau}}{(\tilde{c}t + p_{\tau})^2}
\end{align}

\begin{align}
    B(r_s) &= B(r_s=0)\frac{1 + ar_s(b + cr_s)}{1 + ar_s(1 + dr_s)} \\
    \frac{\mr{d}B}{\mr{d}r_s} &= B(r_s=0)\left[\frac{a(b + cr_s) + acr_s}{ar_s(dr_s + 1) + 1} - \frac{(adr_s + a(dr_s + 1))(ar_s(b + cr_s) + 1)}{(ar_s(dr_s + 1) + 1)^2}\right]
\end{align}

\begin{align}
    y &= \frac{P(t)t^2B(r_s)}{\Gamma w} \\
    \frac{\partial y}{\partial t} &= \frac{B(r_s)t(t\frac{\mr{d}P}{\mr{d}t} + 2P(t))}{\Gamma w} 
\end{align}
\section{Confirming Against Original SCAN}

To check for consistency in the implementation we re-calculate the values for the original SCAN functional, where the norms match.

\begin{table}[h!]
    \caption{$a_{1x}$ - Hydrogen exchange energy}
    \centering
    \begin{tabular}{cc}
        This work & Reference \\
        \hline
        $4.94869866704$ & $4.9479$
    \end{tabular}
\end{table}

\begin{table}[h!]
    \caption{$b_{1c}$ - High density two electron correlation energy}
    \centering
    \begin{tabular}{cc}
        This work & Reference \\
        \hline
        $0.0285763$ & $0.0285764$
    \end{tabular}
\end{table}

\begin{table}[h!]
    \caption{$b_{3c}$ - Defined from $b_{1c}$ for XC 2 electron lower bound.}
    \centering
    \begin{tabular}{cc}
        This work & Reference \\
        \hline
        $0.125541$ & $0.1255$
    \end{tabular}
\end{table}

We get some different for setting $b_{2c}$, but the paper only gives three significant figures in XC energy, so we are within tolerance. 

\begin{table}[h!]
    \caption{$b_{2c} - $Exchange-correlation energy of helium atom}
    \centering
    \begin{tabular}{ccc}
         & This work & Reference \\
        \hline
        $b_{2c}$ & $0.107619496586$ & $0.0889$ \\
        $E_c$ & $-0.0373804872709$ & $-0.0379289116029$ \\
        $E_x$ & $-1.0306195127$ & - \\
        $E_{xc}$ & $-1.068$ & - \\
        \hline
        Error $E_{xc}$ & $1.32141124431E-10$ & $5.48424301412E-4$
    \end{tabular}
\end{table}

\begin{table}[h!]
    \caption{Summary comparison of parameters}
    \centering
    \begin{tabular}{l|rrrrrr}
                    & $a_{1x}$ & $b_{1c}$  & $b_{2c}$ & $b_{3c}$ & $\chi_2$ & $\chi_4^2$ \\
        \hline
        Reference   & $4.9479$ & $0.02858$ & $0.0889$ & $0.1255$ & -        & - \\
        SCAN        & $4.9487$ & $0.02858$ & $0.1076$ & $0.1255$ & -        & - \\
        SCAN2       & $5.5800$ & $0.04097$ & $0.1008$ & $0.1800$ & $0.0$ & $0.444228$
        
    \end{tabular}
\end{table}


\begin{table}[h!]
\caption{Comparison of Helium exchange-correlation energies.}
\centering
    \begin{tabular}{c|cc}
                              & SCAN               & SCAN2 \\
        \hline
        $E_{\mr{x}}[\mr{He}]$ & $-1.0306195127$ & $-1.03109758381$ \\
        $E_{\mr{c}}[\mr{He}]$ & $-0.0373804873$ & $-0.03690241621$
    \end{tabular}
\end{table}

% \section{$r_s \rightarrow \infty$ independence from $\zeta$}

% The spin polarisation function,
% \begin{equation}
%     G_c(\zeta) = (1 - b_{4c}\left[ d_x(\zeta) - 1\right])(1 - \zeta^{12})
% \end{equation}
% is designed to make make $F_{xc}(r_s\rightarrow \infty, \zeta, s=0,\alpha = 0)$ independent of $\zeta$ for the range of $0 <|\zeta| < 0.7$. We take the same construction for $G_c$ so lets start by recovering the parameter for SCAN.
% \begin{align}
%     \epsilon_c^{\mr{LDA0}}(r_s\rightarrow \infty) &= \lim_{r_s \rightarrow \infty} \frac{-b_{1c}}{1 + b_{2c}r_s^{1/2} + b_{3c}r_s} //
%     &= \frac{b_{1c}}{b_{3c}r_s} - \frac{b_{1c}b_{2c}\left(\frac{1}{r_s}\right)^{3/2}}{b_{3c}^2} + O(r_s^2)
% \end{align}

% \begin{align}
%     H_0 = b_{1c}\ln\left[1 + w_0(1 - g+{\infty}(\zeta = 0, s))\right]
% \end{align}

% The important bit seems to be $\chi_\infty$
% \begin{align}
%     \chi_\infty(\zeta) &= \left(\frac{3\pi^2}{16}\right)^{2/3}\beta(r_s\rightarrow\infty)\phi/[c_x(\zeta) - f_0] \\
%     c_x(\zeta) &= -(3/4\pi)(9\pi/4)^{1/3}d_x(\zeta)
% \end{align}

% But this already seems to be for $r_s \rightarrow \infty$ so who knows? Slamming it all through Wolfram Alpha,
% \begin{align}
%     \lim_{r_s \rightarrow \infty}w_0 &= \lim_{r_s\rightarrow \infty}\left[ \exp\left[\frac{-b_{1c}}{(1 + b_{2c}r_s^{1/2} + b_{3c}r_s)b_{1c}}\right]\right] \\
%     &= -\frac{1}{b_{3c}r_s} + \frac{b_{2c}\left(\frac{1}{r_s}\right)^{3/2}}{b_{3c}^2} +O(r_s^2)
% \end{align}
\pagebreak

\section{Exchange interpolation-extrapolation function}

\begin{equation}
    f(\beta) = (1-2\beta)^3(1+A\beta + B\beta^2 + C\beta^3)
\end{equation}

We propose the following strategy for fixing the remaining parameters:

\begin{enumerate}
    \item{Generate a trial set of $\{A_x, B_x, C_x\}$ for exchange interpolation ensuring monotonicity of $f(\beta)$.}
    \item{Filter away those sets that do not achieve and accuracy of $<0.1\%$ in both $a$ and $b$ coefficient for the large-$Z$ asymptote of exchange energy. As calculated from accurate rare gas atom orbitals.}
    \item{From the remaining, select the set that minimise the error in X energy of Cr and Cu.}
    \item{Generate a trial set of $\{A_c, B_c, C_c\}$ for correlation interpolation, we may relax monotonicity here for $\beta > 0.5$.}
    \item{Take the set of correlation parameters that minimise error in rare gas + Cr + Cu atom correlation energy.}
\end{enumerate}

If it is not possible to independently minimise exchange and correlation errors we may have to set C parameters by to give good XC together.

We want the exchange interpolation function to be monotonic for $\beta \in [0, 1]$. Therefore,

\begin{align}
    \frac{df}{d\beta} &= -(1-2\beta)^2(A(8\beta -1) + 2B(5\beta - 1)\beta + 12C\beta^3 - 3C\beta^2 + 6) \\
    0 &\geq -(1-2\beta)^2\left[12C\beta^3 + (10B-3C)\beta^2 + (8A - 2B)\beta - A + 6 \right]
\end{align}

The factor $-(1-2\beta)^2$ is negative so,

\begin{equation}
    0 \leq 12C\beta^3 + (10B-3C)\beta^2 + (8A - 2B)\beta - A + 6
\end{equation}

For $\beta = 0$,
\begin{align}
    0 &\leq 6 - A,\\
    -A &\geq -6
\end{align}

For $\beta = 0.25$,
\begin{equation}
    A + 0.125B \geq -6
\end{equation}

For $\beta = 0.5$,
\begin{equation}
     A + 0.5B + 0.25C \geq -2
\end{equation}

For $\beta = 1$,
\begin{equation}
    7A + 8B + 9C \geq -6
\end{equation}

The strategy should then be to step through a search in $A \leq 6$. Then, given $A$ we can use the inequality for $\beta = 0.25$ to set a bound on $B$ to give $B \geq -(6 + A)/0.125$, from which we can determine a range from $\beta = 0.5$ to give $C \geq -(2 + A + 0.5B)/0.25$ in which to search. We will confirm the monotonicity across the complete domain numerically for each parameter set and reject any that fail.

This makes a large number of possible combinations, we can filter this down by demanding the error in large $Z$ exchange coefficients be $<0.1\%$. We can try about 500 parameter sets per second on my laptop so the large number of potentials is not too problematic.

I made a rough test search with spacings of $0.25$ in all 3 parameters and sadly the results are not immediately promising. The lowest error was given by $\{A_x = -8.25, B_x = 20.75, C_x = -12.5\}$, see Figure \ref{fig:mono}, which underestimated $a$ and $b$ coefficients in the large Z exchange by $82\%$ and $61\%$ respectively, not even close to what we want. A plot of the interpolation-extrapolation function is shown in Figure \ref{fig:mono}. I suspect the function is not flexible enough around the $\beta = 0.5$ region. This fitting was done using code that was confirmed by repeating the original SCAN fitting to find similar results, so we can have some confidence. Though more bug checking in the SCAN2 specific parts needs to happen before we write the function off completely I think.
\begin{figure}[h]
    \includegraphics[width=0.95\textwidth]{Graphics/f_beta_mono.pdf}
    \caption{\label{fig:mono}SCAN2 interpolation function with parameters $(A_x = -8.25, B_x = 20.75, C_x = -12.5)$}
\end{figure}


% We can set up the system of equations,

% \begin{equation}
%    \begin{bmatrix}
%     1 & 0 & 0 \\
%     1 & 0.5 & 0.25 \\
%     7 & 8 & 9
%    \end{bmatrix}
%    \cdot
%    \begin{bmatrix}
%     A \\
%     B \\
%     C \\
%    \end{bmatrix}
%    \geq
%    \begin{bmatrix}
%     -6 \\
%     -2 \\
%     -6 \\
%    \end{bmatrix}
% \end{equation}

% If we take the equality, $(A = -6, B = 10.8, C = -5.6)$, then we get the derivatives to be zero, but we do not guarantee monotonicity...



\section{Hollow Atoms}

We would like to look at the possibility of the hollow atoms as an appropriate norm. The following is a quick update on our current capabilities for performing calculations on them.

I have added the nuclear attraction integrals for the Gaussian based hollow atom potential to \textsc{Quest},
\begin{equation}
    v_{\mr{ext.}}(\vec r) = A\left(\exp[-B|\vec r - \vec R|^2] - \exp[-0.25B|\vec r - \vec R|^2]\right),
    \label{eq:HAeq}
\end{equation}
where $A$ and $B$ are free parameters defining the potential.

We have the ability to perform any conventional finite basis SCF calculation with this potential, e.g. HF, DFT, CCSD, or CCSD(T).

We must be a bit careful though because it's not so clear how fast the basis set and grid convergence for these densities will be. Everything is set up to be efficient for Coulomb atoms with most of the density at the core. I have performed a rough convergence analysis for these quantities. Figures \ref{fig:cardinal}, \ref{fig:augs} and \ref{fig:grid} show convergence with basis set cardinal number, basis set augmentations, and DFT grid density respectively. It seems that DFT grid is not hugely important (though I'm still a little concerned we may not have many longer range points, I will check more thoroughly where the points actually are). Fortunately basis set convergence seems relatively good if a well augmented set is used. It may be possible to perform an extrapolation here, but we would have to check carefully.

\begin{figure}[h]
    \includegraphics[width=0.95\textwidth]{Graphics/cardinal_funcs.pdf}
    \caption{\label{fig:cardinal}Convergence of total energy of Ne$^{2+}$ hollow atom with respect to grid basis set cardinal number.}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=0.95\textwidth]{Graphics/diffuse_funcs.pdf}
    \caption{\label{fig:augs}Convergence of total energy of Ne$^{2+}$ hollow atom with respect to grid basis set augmentations.}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=0.95\textwidth]{Graphics/grid_dens.pdf}
    \caption{\label{fig:grid}Convergence of total energy of Ne$^{2+}$ hollow atom with respect to DFT grid density.}
\end{figure}

Having carried out this initial investigation, it seems we are safe to proceed. We are looking to mimic the Ne$^{2+}$ ion where the electrons have been removed from the 1S orbital. This was achieved by converging a set of HF orbitals in the aug-cc-pCVTZ basis set, then setting the occupations for the ion before plotting the density. This gives a set of unrelaxed orbitals to model our density around. Methods such as MOM could allow us to relax these orbitals, but I don't really fancy messing around with it. We then set the $A$ and $B$ parameters by minimising,
\begin{equation}
    \omega = \int 4\pi r^2 | n_0(r) - n_{\mr{HA}}(r) | dr. 
    \label{eq:HAObj}
\end{equation}
The square and momentum (weighted by $r$) metrics were also tried to little real change. The final optimised parameters for the potential were then, $A = 37$, $B = 5.3$. The resulting density and potential are shown in Figure \ref{fig:HApot}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.95\textwidth]{Graphics/HA_pot.pdf}
    \caption{Hollow atom potential for Ne${2+}$ fit according to Eq. \ref{eq:HAObj}.}
    \label{fig:HApot}
\end{figure}

Doing this we get the correct number of orbitals bound, and seemingly the correct energetic degeneracy for those bound, as shown in Table \ref{tab:HAorbs}.


\begin{table}
\caption{\label{tab:HAorbs} Hartree--Fock orbital energies for the hollow atom system defined by Eq. \ref{eq:HAeq} with $A = 37$, and $B = 5.3$. Calculated using the t-aug-cc-pCVQZ basis set.}
\centering
\begin{tabular}{l|ccccc}
    & 1 & 2 & 3 & 4 & 5 \\
    \hline
    Occ. & 2 & 2 & 2 & 2 & 0 \\
    Energy & -4.18171 & -1.39554 & -1.39554 & -1.39554 & 0.60247
\end{tabular}
\end{table}

\begin{table}
\caption{\label{tab:HAorbs} Exchange and correlation energies for the hollow atom system defined by Eq. \ref{eq:HAeq} with $A = 37$, and $B = 5.3$. Calculated using the t-aug-cc-pCVQZ basis set and a grid of [0, 8] in 0.001$a_0$.}
\centering
\begin{tabular}{l|rrr}
    & $E_{\mr{x}}$ & $E_{\mr{c}}$ & $E_{\mr{xc}}$ \\
    \hline
    BM & -6.971802 & -0.259179 & -7.230981 \\
    SCAN & -7.085710 & -0.268151 & -7.353861 \\
    SCAN\be & -6.933916 & -0.355470 & -7.289387 \\
    \hline
    \hline
    SCAN & -0.113908 & -0.008972 & -0.122880 \\
    SCAN\be & 0.037885 & -0.096291 & -0.058406
\end{tabular}
\end{table}

\subsection{Targeting the $3d$ orbitals}

We are interested in if we can use these hollow atom constructions to single out the $3d$ orbitals self-consistently. To try this, we repeat the previous parameter search but this time against Kr with the $3d$ orbital isolated. This gives a density that looks like Figure \ref{fig:Kr3d}. This may be a challenge, there is much overlap here with other orbitals, but let's see what we can do.


\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.95\textwidth]{Graphics/Kr_3d.pdf}
    \caption{The total (blue) and $3d$ orbital (orange) density for the Kr atom using t-aug-cc-pVTZ basis set.}
    \label{fig:Kr3d}
\end{figure}

\end{document}