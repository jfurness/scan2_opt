import numpy as np
from math import pi, e


# Constant parameters
K0 = 0.1740
BETA_RS_0 = 0.066725
F0 = -0.9
AFACTOR = 0.5  # 0.1
BFACTOR = 1.0  # 0.1778
CFACTOR = 0.16667
DFACTOR = 0.29633
GAMMA = 0.031090690869654895034940863712730
C_TILDE = 1.467
P_TAU = 4.5 # Note the sup mat of acGGA paper uses 1/4.5 and multiplies
C1_CONST = 0.046644
AX = -0.7385587663820224058842300326808360
UAK = 10.0/81.0


# These are the currently 'accepted' parameters for the functional.
# Not used in optimisation, but useful for accessing functional externally.
DEFAULT_X_PARAMS = dict(
    B1X = 0.156632,
    B2X = 0.12083,
    B3X = 0.5,

    CONX = 1.0,
    CAX = -4.5,
    CBX = 60.0,
    CCX = -33.467995,
    CDX = -22.032005,
    K1 = 0.07,
    # B1X = 3.0934,
    # CONX = 1.0,
    # CAX = -9.158851375,
    # CBX = -6.447570233,
    # CCX = -55.53232254,
    # CDX = 71.91488663,
    # K1 = 0.107418078,
    A1 = 5.57996055999
)

DEFAULT_C_PARAMS = dict(
    CONC = 1.0,
    CAC = -6.0,
    CBC = 10.9,
    CCC = -5.2,
    CDC = 0.0,

    B1C = 0.0285764,
    B2C = 0.0889,
    B3C = 0.125541,
    # KAILD = 0.12802585262625815,
    KAILD = 0.128026,
    # GAMMA = 0.031090690869654895034940863712730,
    GAMMA = 0.031091,
    # BETA_MB = 0.066724550603149220,
    BETA_MB = 0.066725,
    AFACTOR = 0.1,
    BFACTOR = 0.1778,
    BETA_RS_0 = 0.066725,  # Same as BETA_MB?
    C_TILDE = 1.467,
    P_TAU = 4.5,
    F0 = -0.9
)

def getscan_x(params, d0, d1, g0, g1, t0, t1, only_0=False, only_Fx=False):
    """
    Evaluates SCAN2 exchange for given orbitals
    INPUT:
        d = density
        g = |grad density|     !! Note, not squared! !!
        t = tau
    
    Optional parameters change output for debugging

        Normally outputs total X energy density from spin scaling

        If only_0 is True we only return spin 0 X energy density

        Else if only_Fx is True we return Fx for spin 0 and 1 independently
    """

    # First spin 0
    rho = 2*d0
    drho = 2*g0
    tauw = drho**2/(8*rho)
    tau_rho = 2*t0
    p = drho**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))
    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0)

    beta = (tau_rho - tauw)/(tau_rho + tau_unif)

    beta[tau_rho - tauw <= 1e-14] = 0.0

    # construct LDA exchange energy density
    exunif_0 = AX*rho**(1.0/3.0)
    exlda_0 = exunif_0*rho

    # and enhancement factor

    Fx0 = scanFx(params, p, beta)

    Ex_0 = exlda_0*Fx0
    if only_0:  # Test for zero spin 1 density i.e. H atom
        return Ex_0

    if not np.any(d1):
        return Ex_0/2.0

    # Now spin 1
    rho = 2*d1
    drho = 2*g1
    tauw = drho**2/(8*rho)
    tau_rho = 2*t1
    p = drho**2/(4*(3*pi**2)**(2.0/3.0)*rho**(8.0/3.0))
    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*rho**(5.0/3.0)
    beta = (tau_rho - tauw)/(tau_rho + tau_unif)
    beta[tau_rho - tauw <= 1e-14] = 0.0
    # construct LDA exchange energy density
    exunif_1 = AX*rho**(1.0/3.0)
    exlda_1 = exunif_1*rho

    # and enhancement factor
    Fx1 = scanFx(params, p, beta)
    Ex_1 = exlda_1*Fx1

    if only_Fx:
        return Fx0, Fx1

    return (Ex_0 + Ex_1)/2.0


def scanFx(params, p, beta):
    # make HX0
    hx0 = 1.0 + K0

    # REGULARISED HX1
    p2 = p**2
    b12 = params['B1X']**2
    qt = (9.0/10.0)*(2*beta - 1) + 17.0/12.0*p
    qt2 = qt*qt
    xfac = 146.0/2025.0*qt2/(1+qt2/b12) 
    xfac -= 73.0/405.0*qt*p/(np.sqrt(1+qt2/b12)*np.sqrt(1 + p2/b12))
    xfac += 1.0/params['K1']*UAK**2*p2/(1 + p2/b12)
    xfac *= 16*beta**2*(1-beta)**2
    xfac += 10.0/81.0*p
    hx1 = 1.0 + params['K1'] - params['K1']/(1.0 + xfac/params['K1'])

    # Beta interpolation function
    f_beta = (1 - 2*beta)**3*(params['CONX'] + params['CAX']*beta + params['CBX']*beta**2 + params['CCX']*beta**3 + params['CDX']*beta**4)

    # gx
    p14 = p**(1.0/4.0)
    # print p
    gx = np.ones(p.shape)
    idxs = p > 1e-16
    gx[p > 0.0] = -np.expm1(-params['A1']/p14[p > 0.0])  # SCAN gx
    # gx[idxs] = -np.expm1(-0.444/p14[idxs] - params['A1']/np.sqrt(p[idxs])) # SCANBETA gx

    # Fx
    Fx = hx1 + f_beta*(hx0 - hx1)
    return Fx*gx

def getscan_c(params, dT, gT, tT, zeta):
    """
    Note: g0 and g1 are absolute value of gradients
    """
    gTT = gT**2
    tauw = gTT/(8*dT)

    ds_zeta = (np.power(1.0 + zeta, 5.0/3.0) + np.power(1.0 - zeta, 5.0/3.0))/2.0
    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0
    tau0 = 0.3*np.power(3*pi**2, 2.0/3.0)*np.power(dT, 5.0/3.0)*ds_zeta
    beta = (tT - tauw)/(tT + tau0)
    beta[tT - tauw <= 1e-14] = 0.0
    
    # Beta interpolation Function
    f_beta = (1 - 2*beta)**3*(params['CONC'] + params['CAC']*beta + params['CBC']*beta**2 + params['CCC']*beta**3 + params['CDC']*beta**4)
    # for i in range(beta.shape[0]):
    #     print beta[i], f_beta[i]

    dthrd = np.exp(np.log(dT)*1.0/3.0)
    rs = (0.75/pi)**(1.0/3.0)/dthrd

    s = np.abs(gT)/(2.0*(3.0*pi**2)**(1.0/3.0)*np.power(dT, 4.0/3.0))

    eppgga0 = corgga_0(params, rs, s, zeta)
    eppgga1 = corgga_1(params, rs, s, zeta)

    epp = eppgga1 + f_beta*(eppgga0 - eppgga1)

    return dT*epp

def corgga_0(params, rs, s, zeta):
    # _0 does not refer to spin in function name

    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
    afix_T = np.sqrt(pi/4.0)*np.power(9.0*pi/4.0, 1.0/6.0)

    sqrt_rs = np.sqrt(rs)
    f1 = 1.0 + params['B2C']*sqrt_rs + params['B3C']*rs
    ec0_lda = -params['B1C']/f1

    dx_zeta = (np.power(1.0 + zeta, 4.0/3.0) + np.power(1.0 - zeta, 4.0/3.0))/2.0

    gc_zeta = (1.0 - 2.3631*(dx_zeta - 1.0)) * (1.0 - pow(zeta, 12))  
    w0 = np.expm1(-ec0_lda/params['B1C'])

    gf_inf = 1.0/(1.0 + 4.0*params['KAILD']*s**2)**(1.0/4.0)

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = params['B1C']*np.log(hcore0)

    EPPGGA = (ec0_lda + h0)*gc_zeta

    return EPPGGA

def corgga_1(params, rs, s, zeta):
    dthrd = rs/(0.75/pi)**(1.0/3.0)
    phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0

    afix_T = np.sqrt(pi/4.0)*np.power(9.0*pi/4.0, 1.0/6.0)

    T = afix_T*s/np.sqrt(rs)/phi
    FK = (3.0*pi**2)**(1.0/3.0)*dthrd
    sk = np.sqrt(4.0*FK/pi)

    EC, H = corpbe_rtpss(rs, zeta, T, phi, sk)

    beta_num = 1.0 + params['AFACTOR']*rs
    beta_den = 1.0 + params['BFACTOR']*rs
    beta = params['BETA_MB']*beta_num/beta_den

    phi3 = phi**3
    pon = -EC/(phi3*params['GAMMA'])
    w = np.expm1(pon)

    A = beta/(params['GAMMA']*w)
    V = A*T**2

    f_g = 1.0/np.power(1.0 + 4.0*V, 0.25)

    hcore = 1.0 + w*(1.0 - f_g)
    ah = params['GAMMA']*phi**3
    H = ah*np.log(hcore)

    return EC+H

def corpbe_rtpss(rs, zeta, T, phi, sk):
    GAM = 0.51984209978974632953442121455650  # 2^(4/3)-2
    FZZ = 8.0/(9.0*GAM)
    gamma = 0.031090690869654895034940863712730  # (1-log(2))/pi^2
    bet_mb = 0.066724550603149220
    sqrt_rs = np.sqrt(rs)

    EU, EURS = gcor2(0.03109070, 0.213700, 7.59570, 3.58760, 1.63820, 0.492940, sqrt_rs)
    EP, EPRS = gcor2(0.015545350, 0.205480, 14.11890, 6.19770, 3.36620, 0.625170, sqrt_rs)
    ALFM, ALFRSM = gcor2(0.01688690, 0.111250, 10.3570, 3.62310, 0.880260, 0.496710, sqrt_rs)

    ALFC = -ALFM
    Z4 = zeta**4

    # LDA part of the energy
    F = ((1.0 + zeta)**(4.0/3.0) + (1.0 - zeta)**(4.0/3.0) - 2.0)/GAM
    EC = EU*(1.0 - F*zeta**4) + EP*F*Z4 - ALFM*F*(1.0 - Z4)/FZZ

    # PBE correction
    bet = bet_mb*(1.0 + 0.1*rs)/(1.0 + 0.1778*rs)

    delt = bet/gamma
    phi3 = phi**3
    pon = -EC/(phi3*gamma)
    B = delt/(np.exp(pon) - 1.0)
    B2 = B**2
    T2 = T**2
    T4 = T**4
    Q4 = 1.0 + B*T2
    Q5 = 1.0 + B*T2 + B2*T4
    H = phi3*(bet/delt)*np.log(1.0 + delt*Q4*T2/Q5)   # Non-local part of correlation

    return EC, H


def gcor2(A, A1, B1, B2, B3, B4, sqrtrs):
    Q0 = -2.0*A*(1.0 + A1*sqrtrs*sqrtrs)
    Q1 = 2.0*A*sqrtrs*(B1 + sqrtrs*(B2 + sqrtrs*(B3 + B4*sqrtrs)))
    Q2 = np.log(1.0 + 1.0/Q1)
    GG = Q0*Q2
    Q3 = A*(B1/sqrtrs + 2.0*B2 + sqrtrs*(3.0*B3 + 4.0*B4*sqrtrs))
    GGRS = -2.0*A*A1*Q2 - Q0*Q3/(Q1*(1.0 + Q1))
    return GG, GGRS


def eps_c_0_high_dens_zeta_0(params, s, chi_42=None):
    """
    Assuming zeta = 0

    In this, chi_42 is set by b1c, it is a free parameter later and in the main functional
    """


    chi_0_zeta_0 = BETA_RS_0*(3*pi**2/16.0)**(2.0/3.0)/(C_TILDE*GAMMA)*np.exp(-C1_CONST/GAMMA)
    if chi_42 is None:
        print("SETTING CHI_42 from B1C")
        chi_42 = params['B1C']/GAMMA*(e - 1)/e*chi_0_zeta_0**2
    else:
        foo = params['B1C']/GAMMA*(e - 1)/e*chi_0_zeta_0**2

    chi_2 = GAMMA/params['B1C']*chi_0_zeta_0**(-3)*chi_42**2*(e - 1)/e

    # chi = chi_0_zeta_0 
    g_0 = 1.0/(1.0 + chi_2*s**2 + chi_42*s**4)
    return params['B1C']*np.log(1 - g_0*(e-1)/e)

def eps_c_1_high_dens_zeta_0(params, s):
    chi_0_zeta_0 = BETA_RS_0*(3*pi**2/16.0)**(2.0/3.0)/(C_TILDE*GAMMA)*np.exp(-C1_CONST/GAMMA)
    chi = chi_0_zeta_0
    g_0 = 1.0/(1.0 + chi*s**2 + chi**2*s**4)
    return GAMMA*np.log(1 - g_0)


def working_eps_c_0(params, s):
    """
    Minimal epsilon_c 0 required for working.
    """

    g_0 = 1.0/(1.0 + params['CHI_2']*s**2 + params['CHI_42']*s**4)

    return params['B1C']*np.log(1 - g_0*(e-1)/e)


# Dedicated gradient of IE functions. Needed for numerically ensuring monotonicity.
# It is probably possible to analytically determine monotonicity criteria...

def interp_gradient(beta, beta2, beta3, A, B, C, D):
    return (1 - 2*beta)**3*(A + 2*B*beta + 3*beta2*C + 4*beta3*D) - 6*(1 - 2*beta)**2*(1 + A*beta + B*beta2 + beta3*C + beta3*beta*D)

def interp_deriv2(beta, beta2, beta3, A, B, C, D):
    temp = -12.0 + 6.0*A - B + (-24.0*A + 16.0*B - 3*C)*beta
    temp += 2.0*(3.0*A - 17.0*B + 18.0*C)*beta2 - 12.0*(4.0*(A + B) + 9.0*C)*beta3
    temp += 84.0*(A + B + C)*(beta2**2)
    temp *= 2.0*(-1.0 + 2.0*beta)
    return temp