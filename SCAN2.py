import numpy as np
import Settings

from revinterp import get_coeffs
from SCAN2_IEF import ief_select, get_pars_from_g1g2, get_gex_pars, get_rpoly_coeff

pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286198
AX = -0.7385587663820223
K0 = 0.174
UAK = 0.12345679012345678#10/81

GAMMA = 0.0310906908696549#(1- np.log(2))/pi**2

if Settings.BETARS == 'revTPSS':
    BETA_MB = 0.066725
    AFACTOR = 0.1
    BFACTOR = 0.1778
    BETA_INF = BETA_MB*AFACTOR/BFACTOR
elif Settings.BETARS == 'ACGGA':
    BETA_MB = 0.066724526517184
    AFACTOR = 0.5
    BFACTOR = 1.0
    CFACTOR = 0.16667
    DFACTOR = 0.29633
    BETA_INF = BETA_MB*CFACTOR/DFACTOR

DEFAULT_X_PARAMS = {
    'ETA':1.e-2, 'DP':0.361, 'CCX': 1.24, 'CAX': -0.667,
    'CBX': 0.21,'CDX':0.361 ,'K1': 0.065, 'A1': 4.948698660008248,
    'DX': 1.24, 'C1X': 0.667, 'C2X': 0.8
}

DEFAULT_C_PARAMS = {
    'ETA':1.e-2, 'DP':0.361, 'CAC': -0.64, 'CBC': 0.7,'CCC':0.0 ,'CDC':0.361,
    'DC':0.7, 'C1C': 0.64, 'C2C': 1.5, 'B1C': 0.028576417226272535,
    'B2C': 0.10754700015884122, 'B3C': 0.12554127064929602
}

def scan_hx1(p,alpha,xps,retderivs=False,ascan=False):

    mu4 = 146/2025
    mu4h = mu4**(0.5)
    if Settings.ISOORB == 'A':
        b1x = 7*mu4h/12
        b2x = 9/20*mu4h
        oma = 1.0 - alpha
    elif Settings.ISOORB == 'BA':
        b1x = (7 - 9*xps['ETA'])*mu4h/12
        b2x = 9/20*mu4h
        oma = 1.0 - alpha
    elif Settings.ISOORB == 'B':
        b1x = -mu4h/6
        b2x = 9/10*mu4h
        oma = 1.0 - 2*alpha
    elif Settings.ISOORB == 'BN':
        b1x = (7 - 9*xps['NU'])*mu4h/12
        b2x = 9/20*(1 + xps['NU'])*mu4h
        oma = 1.0 - (1 + xps['NU'])*alpha

    p2 = p**2
    oma2 = oma**2

    if ascan:
        k1 = 16*UAK**2/(25*mu4)
        wfac = 0.0
        vexp = np.exp(-xps['B3X']*oma2)
        vfac = b1x*p + b2x*oma*vexp
    else:
        k1 = xps['K1']
        b4x = UAK**2/k1 - 25*mu4/16
        wexp = np.exp(-abs(b4x)*p/UAK)
        wfac = b4x*p2*wexp

        vexp = np.exp(-xps['B3X']*oma2)
        vfac = b1x*p + b2x*oma*vexp

    yfac = UAK*p + wfac + vfac**2

    hx1_den = 1.0 + yfac/k1
    hx1 = 1.0 + k1 - k1/hx1_den

    if retderivs:
        dhx1 = 1/hx1_den**2

        if ascan:
            d_wfac_dp = 0.0
            d_vfac_dp = b1x
            d_vfac_da = b2x*vexp*(2*xps['B3X']*oma2 - 1 )
        else:
            d_wfac_dp = b4x*wexp*p*(2 - abs(b4x)*p/UAK)
            d_vfac_dp = b1x
            d_vfac_da = b2x*vexp*(2*xps['B3X']*oma2 - 1 )

        d_yfac_dp = UAK + d_wfac_dp + 2*vfac*d_vfac_dp
        d_yfac_da = 2*vfac*d_vfac_da

        dhx1dp = d_yfac_dp*dhx1
        dhx1da = d_yfac_da*dhx1

        return hx1, dhx1dp, dhx1da

    return hx1

def getscan_x(params, d0, d1, g0, g1, t0, t1, l0, l1, only_0=False, only_Fx=False, unp=False):
    # First spin 0
    """
    from fort.min_mod_r2 import min_mod_r2_x
    npts = d0.shape[0]
    ex = np.zeros(npts)
    for i in range(npts):
        ex[i],_,_,_,_,_,_ = min_mod_r2_x(d0[i],d1[i],g0[i],g1[i],t0[i],t1[i])
    return ex
    """
    # construct LDA exchange energy density
    exunif_0 = AX*(2*d0)**(1.0/3.0)
    exlda_0 = exunif_0*(2*d0)

    # and enhancement factor
    Fx0 = get_scanFx(params, 2*d0, 2*g0, 2*t0, 2*l0)

    Ex_0 = exlda_0*Fx0

    if only_0:  # Test for zero spin 1 density i.e. H atom
        return Ex_0/2.0
    if not np.any(d1):
        return Ex_0/2.0
    if unp:
        return Ex_0

    # Now spin 1

    # construct LDA exchange energy density
    exunif_1 = AX*(2*d1)**(1.0/3.0)
    exlda_1 = exunif_1*(2*d1)

    # and enhancement factor
    Fx1 = get_scanFx(params, 2*d1, 2*g1, 2*t1, 2*l1)

    # Fx1 = scanFx(params, p, beta)
    Ex_1 = exlda_1*Fx1

    if only_Fx:
        return Fx0, Fx1
    return (Ex_0 + Ex_1)/2.0

def get_scanFx(xps, rho, drho, tau, lap):

    kf = (3*pi**2*rho)**(1/3)
    p = (drho/(2*kf*rho))**2
    tauw = drho**2/(8*rho)
    tau0 = 0.3*kf**2*rho

    if Settings.ISOORB == 'A':
        alpha = (tau - tauw)/tau0
    elif Settings.ISOORB == 'BA':
        alpha = (tau - tauw)/(tau0 + xps['ETA']*tauw)
    elif Settings.ISOORB == 'B':
        alpha = (tau - tauw)/(tau + tau0)
    elif Settings.ISOORB == 'BN':
        alpha = (tau - tauw)/(xps['NU']*tau + tau0)
    elif Settings.ISOORB == 'BNB':
        alpha = (tau - tauw)/(xps['NU']*tau + tau0 + (1-xps['NU'])*xps['ETA']*tauw)

    alpha[alpha < 0.0] = 0.0

    Fx = scanFx(xps, p, alpha)

    return Fx

def scanFx(xps, p, alpha):

    hx0 = 1.174

    f1,f2,h1,h2 = get_gex_pars(xps)

    if Settings.GX == "POLY":
        """
        gx = 1/(1 + p/xps['A1'])**(1/4)
        g1 = -1/(4*xps['A1'])
        g2 = 5/(16*xps['A1']**2)
        """
        gx = 1/(1 + (p/xps['A1'])**2)**(1/8)
    elif Settings.GX == "ORIGINAL":
        gx = np.ones(p.shape)
        gx[p > 1.e-16] = 1.0 - np.exp(-xps['A1']/p[p > 1.e-16]**(0.25))
    else:
        raise SystemError("ERROR: Typo in G1X,",Settings.G1X)

    if Settings.H1X == "NEW":

        """
        k1 = -2*h1**2/h2
        xf = (UAK + np.exp(-(p/xps['DP']**2)**2)*(h1 - UAK))*p
        hx1 = 1.0 + k1 - k1/(1.0 + xf/k1)
        """
        """
        xexp = np.exp(-(p/xps['DP']**2)**2)
        xf = (UAK + np.exp(-(p/xps['DP']**2)**2)*(h1 - UAK))*p + (h1**2/xps['K1'] + h2/2*xexp)*p**2
        hx1 = 1.0 + xps['K1'] - xps['K1']/(1.0 + xf/xps['K1'])
        """
        pscl = p**3/xps['DP']**6
        hx1_num = 1 + UAK*p + UAK*(UAK - h1)*p**2 + (1+xps['K1'])*pscl
        hx1_den = 1 + (UAK - h1)*p + ((UAK - h1)**2 - h2/2)*p**2 + pscl
        hx1 = hx1_num/hx1_den

    elif Settings.H1X == "ORIGINAL":

        hx1 = scan_hx1(p,alpha,xps,retderivs=False,ascan=False)

    elif Settings.H1X == "ASCAN":

        hx1 = scan_hx1(p,alpha,xps,retderivs=False,ascan=True)

    elif Settings.H1X == 'R2':

        xf = ((h1 - UAK)*np.exp(-(p/xps['DP']**2)**2) + UAK)*p
        hx1 = 1.0 + xps['K1'] - xps['K1']/(1.0 + xf/xps['K1'])

    elif Settings.H1X == 'R2reopt':

        #pscl2 = (p/xps['DP']**2)**2
        #hx1 = (1 + UAK*p + (1 + xps['K1'])*pscl2)/(1 + (UAK-h1)*p + pscl2)
        hx1_num = 1.0 + (1.0 + xps['K1'])*UAK*p/xps['K1']
        hx1_den = 1.0 + (UAK/xps['K1'] + (UAK - h1)*np.exp(-(p/xps['DP']**2)**2))*p
        hx1 = hx1_num/hx1_den

    elif Settings.H1X == 'RP':

        hx1_num = 1 + xps['DP']*p + (1 + xps['K1'])*xps['DP']*p**2/xps['K1']
        hx1_den = 1 + (xps['DP'] - h1)*p + xps['DP']*p**2/xps['K1']
        hx1 = hx1_num/hx1_den

    elif Settings.H1X == 'MVS':

        hx1 = 1.0

    elif Settings.H1X == 'ADJGE2':

        hx1 = 1.0 + xps['K1'] - xps['K1']/(1 + xps['DA']*p/xps['K1'])

    elif Settings.H1X == 'SIMPLE':

        hx1 = 1.0 + xps['K1'] - xps['K1']/(1 + h1*p/xps['K1'])

    else:
        raise SystemError("ERROR: Typo in H1X,",Settings.H1X)

    ief = ief_select(alpha,xps,'x',retderiv=False)

    fx = (hx1 + ief*(hx0 - hx1))*gx

    return fx

def get_single_orbital_exchange(xps, d, g):

    rho = 2*d
    drho = 2*g

    exunif = AX*rho**(1/3)
    exlda = exunif*rho

    p = drho**2/(4*(3*pi**2)**(2/3)*rho**(8/3))

    hx0 = 1 + K0
    if Settings.GX == "POLY":
        #gx = 1/(1 + (p/xps['A1'])**4)**(1/16)
        #gx = 1/(1 + p/xps['A1'])**(1/4)
        gx = 1/(1 + (p/xps['A1'])**2)**(1/8)
    elif Settings.GX == "ORIGINAL":
        p14 = p**(1.0/4.0)
        gx = np.ones(p.shape)
        idxs = p > 1e-16
        gx[idxs] = -np.expm1(-xps['A1']/p14[idxs])

    return exlda*hx0*gx/2.0

def spinf(z,n):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    return (opz**n + omz**n)/2.0

def spinf_w_deriv(z,n,oderiv=1):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    f = (opz**n + omz**n)/2.0
    df = n*(opz**(n-1) - omz**(n-1))/2.0
    if oderiv == 1:
        return f, df
    elif oderiv == 2:
        ddf = n*(n-1)*(opz**(n-2) + omz**(n-2))/2.0
        return f, df, ddf

def getscan_c(cps, n, gn, tau, lap, zeta, unp=False):

    """
    from fort.min_mod_r2 import min_mod_r2_c
    npts = n.shape[0]
    ec = np.zeros(npts)
    for i in range(npts):
        ec[i],_,_,_,_ = min_mod_r2_c(n[i]*(1+zeta[i])/2,n[i]*(1-zeta[i])/2,gn[i],tau[i])
    return ec
    """

    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2
    p[gn**2 <= 1e-16] = 0.0

    # spin-independent tau_w
    tauw = gn**2/(8*n)
    """
    # spin-dependent tau_w
    tauw = np.zeros(nu.shape)
    tauw[nu>0.0] = gnu[nu>0.0]**2/(8*nu[nu>0.0])
    tauw[nd>0.0] += gnd[nd>0.0]**2/(8*nd[nd>0.0])
    """

    tau0 = 0.3*kf**2*n*spinf(zeta,5/3)

    if Settings.ISOORB == 'A':
        alpha = (tau - tauw)/tau0
    elif Settings.ISOORB == 'BA':
        alpha = (tau - tauw)/(tau0 + cps['ETA']*tauw)
    elif Settings.ISOORB == 'B':
        alpha = (tau - tauw)/(tau + tau0)
    elif Settings.ISOORB == 'BN':
        alpha = (tau - tauw)/(cps['NU']*tau + tau0)
    elif Settings.ISOORB == 'BNB':
        alpha = (tau - tauw)/(cps['NU']*tau + tau0 + (1-cps['NU'])*cps['ETA']*tauw)

    alpha[alpha < 0.0] = 0.0

    if Settings.CORRELATION == "NEW":
        eps0 = corgga_0_scan2(cps,rs,p,zeta)
        eps1 = corgga_1_scan2(cps,rs,p,zeta,unp=unp)
    elif Settings.CORRELATION == "ORIGINAL":
        eps0 = corgga_0_ori(cps, rs, p, zeta)
        eps1 = corgga_1_ori(cps, rs, p, zeta,unp=unp)
    elif Settings.CORRELATION in ["R2","R2reopt"]:
        eps0,eps1 = corgga_r2scan(cps, rs, p, zeta)

    ief = ief_select(alpha,cps,'c',retderiv=False)

    epsc = eps1*(1 - ief) + ief*eps0

    return n*epsc


def ec_pw92(rs,z,unp=False):

    unp_pars = [0.031091,0.21370,7.5957,3.5876,1.6382,0.49294]
    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """
    rsh = rs**(0.5)

    def g(v):
        q0 = -2*v[0]*(1 + v[1]*rs)
        q1 = 2*v[0]*(v[2]*rsh + v[3]*rs + v[4]*rs*rsh + v[5]*rs*rs)
        q1p = v[0]*(v[2]/rsh + 2*v[3] + 3*v[4]*rsh + 4*v[5]*rs)
        logt = np.log(1 + 1/q1)
        dg = -2*v[0]*v[1]*logt - q0*q1p/(q1**2 + q1)
        return q0*logt,dg

    ec0,d_ec0_drs = g(unp_pars)
    if unp:
        return ec0,d_ec0_drs,np.zeros(ec0.shape)

    pol_pars = [0.015545,0.20548,14.1189,6.1977,3.3662,0.62517]
    alp_pars = [0.016887,0.11125,10.357,3.6231,0.88026,0.49671]

    fz_den = 0.5198420997897464#(2**(4/3)-2)
    fdd0 = 1.7099209341613653#8/9/fz_den
    dxz,d_dxz_dz = spinf_w_deriv(z,4/3)
    fz = 2*(dxz - 1)/fz_den
    d_fz_dz = 2*d_dxz_dz/fz_den

    ec1,d_ec1_drs = g(pol_pars)
    ac,d_ac_drs = g(alp_pars)
    z4 = z**4
    fzz4 = fz*z4
    ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

    d_ec_drs = d_ec0_drs*(1 - fzz4) + d_ec1_drs*fzz4 - d_ac_drs/fdd0*(fz - fzz4)

    d_ec_dz = -ac*d_fz_dz/fdd0 + (fz*4*z**3 + d_fz_dz*z4)*(ac/fdd0 + ec1 - ec0)

    return ec,d_ec_drs,d_ec_dz


def corgga_0_scan2(cps,rs,p,z):

    ec0_lda = -cps['B1C']/(1.0 + cps['B2C']*rs**(0.5) + cps['B3C']*rs)

    dx_zeta = spinf(z,4/3)
    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*cps['B3C']/cps['B1C']
    gc_zeta = (1 - bfac*(dx_zeta - 1)) * (1 - z**(12))

    chi_inf = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))
    g_inf = 1/(1 + 4*chi_inf*p)**(0.25)
    w0 = np.expm1(-ec0_lda/cps['B1C'])
    h0 = cps['B1C']*np.log(1 + w0*(1-g_inf))

    return (ec0_lda + h0)*gc_zeta

def corgga_1_scan2(cps,rs,p,z,unp=False):

    if Settings.BETARS == 'revTPSS':
        brs = BETA_MB*(1 + AFACTOR*rs)/(1 + BFACTOR*rs)
    elif Settings.BETARS == 'ACGGA':
        brs = BETA_MB*(1 + AFACTOR*rs*(BFACTOR + CFACTOR*rs))/(1 + AFACTOR*rs*(1 + DFACTOR*rs))

    phi = spinf(z,2/3)
    gp3 = GAMMA*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)

    ecl,_,_ = ec_pw92(rs,z,unp=unp)
    w1 = np.expm1(-ecl/gp3)
    aa = brs/GAMMA/w1
    y = aa*t2

    gt = 1/(1 + 4*y + 10*y**2)**(0.25)

    h = gp3*np.log(1 + w1*np.tanh(y))#(1 - gt))

    return ecl + h

def corgga_0_ori(params, rs, p, zeta):
    # _0 does not refer to spin in function name

    sqrt_rs = np.sqrt(rs)
    f1 = 1.0 + params['B2C']*sqrt_rs + params['B3C']*rs
    ec0_lda = -params['B1C']/f1

    dx_zeta = spinf(zeta,4/3)

    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*params['B3C']/params['B1C']
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - pow(zeta, 12))
    w0 = np.expm1(-ec0_lda/params['B1C'])

    chi_inf = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))
    gf_inf = 1.0/(1.0 + 4.0*chi_inf*p)**(1.0/4.0)

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = params['B1C']*np.log(hcore0)

    EPPGGA = (ec0_lda + h0)*gc_zeta

    return EPPGGA

def corgga_1_ori(params, rs, p, zeta, unp=False):

    dthrd = rs/(0.75/pi)**(1.0/3.0)
    phi = spinf(zeta,2/3)

    afix_T2 = pi/4.0*(9*pi/4)**(1/3)

    T2 = afix_T2*p/rs/phi**2
    EC,_,_ = ec_pw92(rs,zeta,unp=unp)

    if Settings.BETARS == 'revTPSS':
        beta = BETA_MB*(1 + AFACTOR*rs)/(1 + BFACTOR*rs)
    elif Settings.BETARS == 'ACGGA':
        beta = BETA_MB*(1 + AFACTOR*rs*(BFACTOR + CFACTOR*rs))/(1 + AFACTOR*rs*(1 + DFACTOR*rs))

    phi3 = phi**3
    pon = -EC/(phi3*GAMMA)
    w = np.expm1(pon)

    A = beta/(GAMMA*w)
    V = A*T2

    f_g = 1.0/np.power(1.0 + 4.0*V, 0.25)

    hcore = 1.0 + w*(1.0 - f_g)
    ah = GAMMA*phi**3
    H = ah*np.log(hcore)

    return EC+H

def corgga_r2scan(cps,rs,p,z,unp=False,ret0=False):

    sqrt_rs = np.sqrt(rs)
    f1 = 1.0 + cps['B2C']*sqrt_rs + cps['B3C']*rs
    ec0_lda = -cps['B1C']/f1

    dx_zeta = spinf(z,4/3)
    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*cps['B3C']/cps['B1C']
    gc_zeta = (1.0 - bfac*(dx_zeta - 1.0)) * (1.0 - pow(z, 12))

    d_ec0_drs = gc_zeta*cps['B1C']/f1**2 * (0.5*cps['B2C']/rs**(0.5) + cps['B3C'])

    w0 = np.expm1(-ec0_lda/cps['B1C'])

    chi_inf = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))
    gf_inf = 1.0/(1.0 + 4.0*chi_inf*p)**(1.0/4.0)

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = cps['B1C']*np.log(hcore0)

    eps0 = (ec0_lda + h0)*gc_zeta
    if ret0:
        return eps0

    if Settings.BETARS == 'revTPSS':
        brs = BETA_MB*(1 + AFACTOR*rs)/(1 + BFACTOR*rs)
    elif Settings.BETARS == 'ACGGA':
        brs = BETA_MB*(1 + AFACTOR*rs*(BFACTOR + CFACTOR*rs))/(1 + AFACTOR*rs*(1 + DFACTOR*rs))

    phi = spinf(z,2/3)
    gp3 = GAMMA*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)

    ecl,d_ecl_drs,_ = ec_pw92(rs,z,unp=unp)
    w1 = np.expm1(-ecl/gp3)
    aa = brs/GAMMA/w1
    y = aa*t2

    ds_zeta = spinf(z,5.0/3.0)

    dp2 = cps['DP']

    _, dief1 = ief_select(np.ones(1),cps,'c',retderiv=True,cpoles=False)
    df1 = dief1[0]
    """
    if Settings.Fint == 'R2':
        df1 = -0.7114023342889984
        #dp2 = 0.361
        #eta = 0.001
    elif Settings.Fint == 'R2reopt':
        cfc = get_coeffs('c', use_stored=True)
        df1 = -np.sum(cfc)
    elif Settings.Fint in ['ORIGINAL','MVS','BRAT','ASCAN']:
        df1 = 0.0
    elif Settings.Fint == 'SMTH':
        df1 = (cps['C2C'] - 2*(1 + cps['C2C'])) \
            /(1 + cps['C1C'] + cps['C2C'] + (1+cps['C2C'])/cps['DC'])
    elif Settings.Fint == 'JPP' or Settings.Fint == 'BPOLY':
        df1 = cps['C2C']
    elif Settings.Fint == 'BP':
        df1 = cps['F1C']
    elif Settings.Fint == 'R2NU':
        bcl = get_rpoly_coeff(cps['C1C'],cps['C2C'],cps['DC'],2.5,osmth=3)
        df1 = -np.sum(bcl)
    """

    if Settings.ISOORB == 'A':
        fac1 = 20/27
        fac2 = 0.0
    elif Settings.ISOORB == 'BA':
        fac1 = 20/27
        fac2 = -45/27*cps['ETA']
        #dy_fac = df1/(27.0*ds_zeta*gp3*w1) * (20*rs*(d_ec0_drs - d_ecl_drs) \
        #    - 45*cps['ETA']*(ec0_lda*gc_zeta - ecl))
    elif Settings.ISOORB == 'B':
        fac1 = 5/27
        fac2 = -5/12
    elif Settings.ISOORB == 'BNB':
        fac1 = 20/(27*(1 + cps['NU']))
        fac2 = -5*(cps['NU'] + (1-cps['NU'])*cps['ETA'])/(3*(1 + cps['NU']))

    dy_damp = p*np.exp(-(p/dp2**2)**2)
    dy_fac = df1/(ds_zeta*gp3*w1)*(fac1*rs*(d_ec0_drs - d_ecl_drs) \
        +fac2*(ec0_lda*gc_zeta - ecl) )
    delta_y = dy_fac*dy_damp

    if Settings.CORRELATION == 'R2':

        if np.any(y - delta_y < -0.01):
            tmpr = 1e20*np.ones_like(p)
            return tmpr, tmpr
        gt = 1.0/(1.0 + 4*(y - delta_y))**(0.25)

    elif Settings.CORRELATION == 'R2reopt':
        """
        dc = df1/(27.0*ds_zeta*gp3*w1) * (20*rs*(d_ec0_drs - d_ecl_drs) \
            - 45*eta*(ec0_lda*gc_zeta - ecl))
        dp4 = dp2**4
        gt_int_num = 1 + 4*y + 4*dc*p
        gt_int_den = 1 + 8*y + 4*y**2/dp4
        gt = (gt_int_num/gt_int_den)**(0.25)
        """
        #ydy = y - delta_y
        #dc= df1/(27.0*ds_zeta*gp3*w1) * (20*rs*(d_ec0_drs - d_ecl_drs) \
        #    - 45*eta*(ec0_lda*gc_zeta - ecl))
        gt_num = 1 + 8*delta_y
        gt_den = 1 + 8*y + 16*y**2
        gt = (gt_num/gt_den)**(0.125)

    h = gp3*np.log(1 + w1*(1 - gt))

    return eps0, ecl + h

def eps_c_0_high_dens_zeta_0(cps, s):
    """
    Assuming zeta = 0
    """
    #return corgga_0_scan2(cps,0.0,s**2,0.0)
    if Settings.CORRELATION == "NEW":
        eps0 = corgga_0_scan2(cps, 0.0, s**2, 0.0)
    elif Settings.CORRELATION == "ORIGINAL":
        eps0 = corgga_0_ori(cps, 0.0, s**2, 0.0)
    elif Settings.CORRELATION in ["R2","R2reopt"]:
        eps0 = corgga_r2scan(cps, 1.e-18, s**2, 0.0,ret0=True)
    return eps0


def eps_c_1_high_dens_zeta_0(cps, s):
    """
    Assuming zeta = 0
    """
    #return corgga_1_scan2(cps,1.e-18,s**2,0.0)
    if Settings.CORRELATION == "NEW":
        eps1 = corgga_1_scan2(cps, 1.e-18, s**2, 0.0)
    elif Settings.CORRELATION == "ORIGINAL":
        eps1 = corgga_1_ori(cps, 1.e-18, s**2, 0.0)
    elif Settings.CORRELATION in ["R2","R2reopt"]:
        _,eps1 = corgga_r2scan(cps, 1.e-18, s**2, 0.0)
    return eps1

def get_single_orbital_correlation(params, dT, gT, zeta):

    gTT = gT**2
    p = gTT/(4.0*(3.0*pi**2)**(2.0/3.0)*dT**(8.0/3.0))
    idxs = gTT <= 1e-16
    p[idxs] = 0.0

    rs = (0.75/pi/dT)**(1.0/3.0)

    if Settings.CORRELATION == "NEW":
        eps0 = corgga_0_scan2(params, rs, p, zeta)
    elif Settings.CORRELATION == "ORIGINAL":
        eps0 = corgga_0_ori(params, rs, p, zeta)
    elif Settings.CORRELATION in ["R2","R2reopt"]:
        eps0 = corgga_r2scan(params, rs, p, zeta,ret0=True)
    else:
        print("ERROR: Single orbital correlation function not set",Settings.CORRELATION)
        raise NotImplementedError
    return dT*eps0

"""
    Routines needed for computation of SCAN 2 derivatives
"""

def get_scan2_xc_w_sl_derivs(xps, cps, d0, d1, g0, g1, gt, t0, t1, l0, l1, only_0=False):

    ex, d_ex_dn0, d_ex_dn1, d_ex_dgn0, d_ex_dgn1, d_ex_dt0, d_ex_dt1 = \
        getscan2_x_w_derivs(xps, d0, d1, g0, g1, t0, t1, l0, l1, only_0=only_0)
    ec, d_ec_dn0, d_ec_dn1, d_ec_dgn, d_ec_dtau = \
        getscan_c_w_derivs(cps, d0, d1, np.abs(gt), t0+t1, l0+l1)

    # warning, assuming collinear spin

    vxcn0 = d_ex_dn0 + d_ec_dn0
    vxcn1 = d_ex_dn1 + d_ec_dn1

    vxcg0 = g0*d_ex_dgn0/np.abs(g0) + (g0 + g1)*d_ec_dgn/np.abs(gt)
    if only_0:
        vxcg1 = np.zeros(d0.shape)
        vxcn0 -= (d_ex_dt0 + d_ec_dtau)*t0/d0
        vxcg0 += g0*(d_ex_dt0 + d_ec_dtau)/(4*d0)
    else:
        vxcg1 = g1*d_ex_dgn1/np.abs(g1) + (g0 + g1)*d_ec_dgn/np.abs(gt)

    return ex + ec, vxcn0, vxcn1, vxcg0, vxcg1

def getscan2_x_w_derivs(params, d0, d1, g0, g1, t0, t1, l0, l1, only_0=False):
    # First spin 0

    Ex0,dEx0dn,dEx0dgn,dEx0dtau = get_scan2_Ex_w_derivs(params, 2*d0, 2*np.abs(g0), 2*t0, 2*l0)

    if only_0 or not np.any(d1):  # Test for zero spin 1 density i.e. H atom
        dum = np.zeros(d0.shape)
        return Ex0/2.0, dEx0dn, dum, dEx0dgn, dum, dEx0dtau, dum
    # Now spin 1

    Ex1,dEx1dn,dEx1dgn,dEx1dtau = get_scan2_Ex_w_derivs(params, 2*d1, 2*np.abs(g1), 2*t1, 2*l1)

    return (Ex0 + Ex1)/2.0, dEx0dn, dEx1dn, dEx0dgn, dEx1dgn, dEx0dtau, dEx1dtau


def get_scan2_Ex_w_derivs(xps, rho, drho, tau, lap):

    kf = (3*pi**2*rho)**(1/3)

    exlda = AX*rho**(4/3)
    d_exlda_dn = -kf/pi

    p = (drho/(2*kf*rho))**2
    d_p_dn = -8/3*p/rho
    d_p_dgn = drho/(2*(kf*rho)**2)

    tauw = drho**2/(8*rho)
    d_tauw_dn = -tauw/rho
    d_tauw_dgn = drho/(4*rho)

    tau0 = 0.3*kf**2*rho
    d_tau0_dn = kf**2/2

    if Settings.ISOORB == 'A':
        alpha = (tau - tauw)/tau0
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + d_tau0_dn*alpha)/tau0
        d_alpha_dgn = -d_tauw_dgn/tau0
        d_alpha_dtau = 1/tau0

    elif Settings.ISOORB == 'BA':
        alpha_den = tau0 + xps['ETA']*tauw
        alpha = (tau - tauw)/alpha_den
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + (d_tau0_dn + xps['ETA']*d_tauw_dn)*alpha)/alpha_den
        d_alpha_dgn = -(1 + xps['ETA']*alpha)*d_tauw_dgn/alpha_den
        d_alpha_dtau = 1/alpha_den

    elif Settings.ISOORB == 'B':
        alpha_den = tau + tau0
        alpha = (tau - tauw)/alpha_den
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + d_tau0_dn*alpha)/alpha_den
        d_alpha_dgn = -d_tauw_dgn/alpha_den
        d_alpha_dtau = (1 - alpha)/alpha_den

    Fx, d_Fx_dp, d_Fx_da = scan2_Fx_w_derivs(xps, p, alpha)
    Ex = Fx*exlda

    d_Ex_dn = d_exlda_dn*Fx + exlda*(d_Fx_dp*d_p_dn + d_Fx_da*d_alpha_dn)
    d_Ex_dgn = exlda*(d_Fx_dp*d_p_dgn + d_Fx_da*d_alpha_dgn)
    d_Ex_dtau = exlda*d_Fx_da*d_alpha_dtau

    return Ex, d_Ex_dn, d_Ex_dgn, d_Ex_dtau


def scan2_Fx_w_derivs(xps, p, alpha):

    hx0 = 1.174

    f1,f2,h1,h2 = get_gex_pars(xps)

    if Settings.GX == 'ORIGINAL':
        pmask = p > 1.e-16
        gexp = np.exp(-xps['A1']/p**(0.25))
        gx = np.ones(p.shape)
        gx[pmask] = 1.0 - gexp[pmask]
        dgxdp = np.zeros(p.shape)
        dgxdp[pmask] = -0.25*xps['A1']*gexp[pmask]/p[pmask]**(1.25)

    elif Settings.GX == 'POLY':
        #gx = 1/(1 + (p/xps['A1'])**4)**(1/16)
        #dgxdp = -p**3/xps['A1']**4/4 * gx**(17)
        """
        gx = 1/(1 + p/xps['A1'])**(1/4)
        dgxdp = -1/(4*xps['A1'])*gx**5
        g1 = -1/(4*xps['A1'])
        g2 = 5/(16*xps['A1']**2)
        """
        gx = 1/(1 + (p/xps['A1'])**2)**(1/8)
        dgxdp = -p/(4*xps['A1']**2)*gx**9

    dhx1da = np.zeros_like(alpha)

    if Settings.H1X == "NEW":

        dp6 = xps['DP']**6
        pscl = p**3/dp6

        hx1_num = 1 + UAK*p + UAK*(UAK - h1)*p**2 + (1+xps['K1'])*pscl
        d_hx1_num_dp = UAK + 2*UAK*(UAK - h1)*p + 3*(1+xps['K1'])*p**2/dp6

        hx1_den = 1 + (UAK - h1)*p + ((UAK - h1)**2 - h2/2)*p**2 + pscl
        d_hx1_den_dp = UAK - h1 + (2*(UAK - h1)**2 - h2)*p + 3*p**2/dp6

        hx1 = hx1_num/hx1_den
        dhx1dp = (d_hx1_num_dp - d_hx1_den_dp*hx1 )/hx1_den

    elif Settings.H1X == 'R2reopt':

        """
        dp4 = xps['DP']**4
        pscl2 = p**2/dp4
        hx1_num = 1 + UAK*p + (1 + xps['K1'])*pscl2
        hx1_den = 1 + (UAK-h1)*p + pscl2
        hx1 = hx1_num/hx1_den
        dhx1dp = (UAK + 2*(1 + xps['K1'])*p/dp4 - (UAK - h1 + 2*p/dp4)*hx1)/hx1_den
        """
        hx1_num = 1.0 + (1.0 + xps['K1'])*UAK*p/xps['K1']
        dp4 = xps['DP']**4
        dfac = np.exp(-p**2/dp4)
        d_dfac_dp = -2*p/dp4*dfac
        hx1_den = 1.0 + (UAK/xps['K1'] + (UAK - h1)*dfac)*p
        hx1 = hx1_num/hx1_den

        d_hx1_num_dp = (1.0 + xps['K1'])*UAK/xps['K1']
        d_hx1_den_dp = UAK/xps['K1'] + (UAK - h1)*dfac + (UAK - h1)*d_dfac_dp*p

        dhx1dp = (d_hx1_num_dp - d_hx1_den_dp*hx1)/hx1_den

    elif Settings.H1X == 'RP':

        hx1_num = 1 + xps['DP']*p + (1 + xps['K1'])*xps['DP']*p**2/xps['K1']
        d_hx1_num_dp = xps['DP'] + 2*(1 + xps['K1'])*xps['DP']*p/xps['K1']

        hx1_den = 1 + (xps['DP'] - h1)*p + xps['DP']*p**2/xps['K1']
        d_hx1_den_dp = (xps['DP'] - h1) + 2*xps['DP']*p/xps['K1']

        hx1 = hx1_num/hx1_den
        dhx1dp = (d_hx1_num_dp - d_hx1_den_dp*hx1)/hx1_den

    elif Settings.H1X == 'MVS':

        hx1 = np.ones_like(p)
        dhx1dp = np.zeros_like(p)

    elif Settings.H1X == 'ADJGE2':

        hx1_den = 1 + xps['DA']*p/xps['K1']
        hx1 = 1.0 + xps['K1'] - xps['K1']/hx1_den
        dhx1dp = xps['DA']/hx1_den**2

    elif Settings.H1X == 'SIMPLE':

        hx1_den = 1 + h1*p/xps['K1']
        hx1 = 1.0 + xps['K1'] - xps['K1']/hx1_den
        dhx1dp = h1/hx1_den**2

    elif Settings.H1X == 'ORIGINAL':

        hx1, dhx1dp, dhx1da = scan_hx1(p,alpha,xps,retderivs=True,ascan=False)

    elif Settings.H1X == "ASCAN":

        hx1, dhx1dp, dhx1da = scan_hx1(p,alpha,xps,retderivs=True,ascan=True)

    ief, diefda = ief_select(alpha,xps,'x',retderiv=True)

    fxint = hx1 + ief*(hx0 - hx1)
    fx = fxint*gx

    dfxdp = dhx1dp*(1-ief)*gx + fxint*dgxdp
    dfxda = diefda*(hx0 - hx1)*gx + dhx1da*(1-ief)*gx

    return fx, dfxdp, dfxda

def getscan_c_w_derivs(cps, nup, ndn, gn, tau, lap, unp=False):

    n = nup + ndn
    zeta = np.minimum(np.maximum((nup - ndn)/n,-0.99999999999990),0.99999999999990)
    d_zeta_dnu = 2*ndn/n**2
    d_zeta_dnd = -2*nup/n**2

    rs = (3/(4*pi*n))**(1/3)
    d_rs_dn = -rs/(3*n)

    kf = (3*pi**2*n)**(1/3)

    agn = np.abs(gn)
    agn[gn**2 <= 1e-16] = 0.0
    p = (agn/(2*kf*n))**2
    d_p_dn = -8/3*p/n
    d_p_dgn = agn/(2*(kf*n)**2)

    # spin-independent tau_w
    tauw = gn**2/(8*n)
    d_tauw_dn = -tauw/n
    d_tauw_dgn = gn/(4*n)

    """
    # spin-dependent tau_w
    tauw = np.zeros(nu.shape)
    tauw[nu>0.0] = gnu[nu>0.0]**2/(8*nu[nu>0.0])
    tauw[nd>0.0] += gnd[nd>0.0]**2/(8*nd[nd>0.0])
    """

    ds_zeta,d_ds_zeta_dz = spinf_w_deriv(zeta,5/3)
    tau0_unp = 0.3*kf**2*n
    tau0 = tau0_unp*ds_zeta
    d_tau0_dn = kf**2/2*ds_zeta
    d_tau0_dzeta = tau0_unp*d_ds_zeta_dz

    if Settings.ISOORB == 'A':
        alpha = (tau - tauw)/tau0
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + d_tau0_dn*alpha)/tau0
        d_alpha_dzeta = -d_tau0_dzeta*alpha/tau0
        d_alpha_dgn = -d_tauw_dgn/tau0
        d_alpha_dtau = 1/tau0

    elif Settings.ISOORB == 'BA':
        alpha_den = tau0 + cps['ETA']*tauw
        alpha = (tau - tauw)/alpha_den
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + (d_tau0_dn + cps['ETA']*d_tauw_dn)*alpha)/alpha_den
        d_alpha_dzeta = -d_tau0_dzeta*alpha/alpha_den
        d_alpha_dgn = -(1 + cps['ETA']*alpha)*d_tauw_dgn/alpha_den
        d_alpha_dtau = 1/alpha_den

    elif Settings.ISOORB == 'B':
        alpha_den = tau + tau0
        alpha = (tau - tauw)/alpha_den
        alpha[alpha < 0.0] = 0.0

        d_alpha_dn = -(d_tauw_dn + d_tau0_dn*alpha)/alpha_den
        d_alpha_dzeta = -d_tau0_dzeta*alpha/alpha_den
        d_alpha_dgn = -d_tauw_dgn/alpha_den
        d_alpha_dtau = (1 - alpha)/alpha_den


    if Settings.CORRELATION == "NEW":
        eps0, d_eps0_drs, d_eps0_dz, d_eps0_dp = corgga_0_scan2_w_derivs(cps,rs,p,zeta)
        eps1, d_eps1_drs, d_eps1_dz, d_eps1_dp = corgga_1_scan2_w_derivs(cps,rs,p,zeta,unp=unp)
    elif Settings.CORRELATION in ["R2","R2reopt"]:
        eps0, d_eps0_drs, d_eps0_dz, d_eps0_dp, eps1, d_eps1_drs, d_eps1_dz,\
            d_eps1_dp = corgga_r2scan_w_derivs(cps,rs,p,zeta)

    ief, diefda = ief_select(alpha,cps,'c',retderiv=True)

    omief = 1 - ief
    epsc = eps1*omief + ief*eps0
    d_epsc_drs = d_eps1_drs*omief + ief*d_eps0_drs
    d_epsc_dz = d_eps1_dz*omief + ief*d_eps0_dz
    d_epsc_dp = d_eps1_dp*omief + ief*d_eps0_dp
    d_epsc_da = diefda*(eps0 - eps1)

    ec = n*epsc

    d_ec_dn = epsc + n*(d_epsc_drs*d_rs_dn + d_epsc_dp*d_p_dn + d_epsc_da*d_alpha_dn)
    spind = n*(d_epsc_dz + d_epsc_da*d_alpha_dzeta)
    d_ec_dnu = d_ec_dn + spind*d_zeta_dnu
    d_ec_dnd = d_ec_dn + spind*d_zeta_dnd

    d_ec_dgn = n*(d_epsc_dp*d_p_dgn + d_epsc_da*d_alpha_dgn)

    d_ec_dtau = n*d_epsc_da*d_alpha_dtau

    return ec, d_ec_dnu, d_ec_dnd, d_ec_dgn, d_ec_dtau


def corgga_0_scan2_w_derivs(cps,rs,p,z):

    rsh = rs**(0.5)
    ec0den = 1.0 + cps['B2C']*rsh + cps['B3C']*rs
    ec0_lda = -cps['B1C']/ec0den
    d_ec0_lda_drs = cps['B1C']*(0.5*cps['B2C']/rsh + cps['B3C'])/ec0den**2

    dxz,d_dxz_dz = spinf_w_deriv(z,4/3)
    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*cps['B3C']/cps['B1C']

    gc1 = 1 - bfac*(dxz - 1)
    gc2 = 1 - z**(12)
    gc_zeta = gc1*gc2
    d_gc_zeta_dz = -bfac*d_dxz_dz*gc2 - 12*gc1*z**(11)

    chi_inf = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))
    g_inf_den = (1 + 4*chi_inf*p)**(0.25)
    omg_inf = 1 - 1/g_inf_den
    d_omg_inf_dp = chi_inf/g_inf_den**5

    w0 = np.expm1(-ec0_lda/cps['B1C'])
    d_w0_drs = -d_ec0_lda_drs/cps['B1C']*(1 + w0)

    h0c = 1 + w0*omg_inf
    h0 = cps['B1C']*np.log(h0c)
    d_h0_drs = cps['B1C']*d_w0_drs*omg_inf/h0c
    d_h0_dp = cps['B1C']*w0*d_omg_inf_dp/h0c

    ecunp = (ec0_lda + h0)
    ec0 = ecunp*gc_zeta
    d_ec0_drs = (d_ec0_lda_drs + d_h0_drs)*gc_zeta
    d_ec0_dz = ecunp*d_gc_zeta_dz
    d_ec0_dp = d_h0_dp*gc_zeta

    return ec0, d_ec0_drs, d_ec0_dz, d_ec0_dp

def corgga_1_scan2_w_derivs(cps,rs,p,z,unp=False):

    if Settings.BETARS == 'revTPSS':
        brs_den = 1 + BFACTOR*rs
        brs = BETA_MB*(1 + AFACTOR*rs)/brs_den
        d_brs_drs = BETA_MB*( AFACTOR - BFACTOR)/brs_den**2
    elif Settings.BETARS == 'ACGGA':
        brs_den = 1 + AFACTOR*rs*(1 + DFACTOR*rs)
        brs = BETA_MB*(1 + AFACTOR*rs*(BFACTOR + CFACTOR*rs) )/brs_den
        d_brs_drs = AFACTOR*( BETA_MB*(BFACTOR + 2*CFACTOR*rs) \
            - (1 + 2*DFACTOR*rs)*brs )/brs_den

    phi, d_phi_dz = spinf_w_deriv(z,2/3)
    gp3 = GAMMA*phi**3

    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)
    d_t2_drs = -t2/rs
    d_t2_dz = -2*t2*d_phi_dz/phi
    d_t2_dp = (3*pi**2/16)**(2/3)/(phi**2*rs)

    ecl,d_ecl_drs,d_ecl_dz = ec_pw92(rs,z,unp=unp)

    w1 = np.expm1(-ecl/gp3)
    opw1 = 1 + w1
    d_w1_drs = -d_ecl_drs/gp3*opw1
    d_w1_dz = (-d_ecl_dz + 3*ecl*d_phi_dz/phi)*opw1/gp3

    aa = brs/(GAMMA*w1)
    y = aa*t2
    d_y_drs = ( d_brs_drs/(GAMMA*w1) - d_w1_drs*aa/w1 )*t2 + aa*d_t2_drs
    d_y_dz = -d_w1_dz*aa*t2/w1 + aa*d_t2_dz
    d_y_dp = aa*d_t2_dp

    """
    gt_den = (1 + 4*y + 10*y**2)**(0.25)
    dgt = (1 + 5*y)/gt_den**5
    omgt = 1 - 1/gt_den
    """

    omgt = np.tanh(y)
    exp2y = np.exp(-2*y)
    dgt = 4*exp2y/(1 + exp2y)**2 # = 1/cosh(y)**2

    d_omgt_drs = dgt*d_y_drs
    d_omgt_dz = dgt*d_y_dz
    d_omgt_dp = dgt*d_y_dp

    hc = 1 + w1*omgt
    h = gp3*np.log(hc)
    d_h_drs = gp3*(d_w1_drs*omgt + w1*d_omgt_drs)/hc
    d_h_dz = 3*GAMMA*phi**2*d_phi_dz*np.log(hc) + gp3*( d_w1_dz*omgt + w1*d_omgt_dz)/hc
    d_h_dp = gp3*w1*d_omgt_dp/hc

    ec1 = ecl + h
    d_ec1_drs = d_ecl_drs + d_h_drs
    d_ec1_dz = d_ecl_dz + d_h_dz
    d_ec1_dp = d_h_dp

    return ec1, d_ec1_drs, d_ec1_dz, d_ec1_dp

def corgga_r2scan_w_derivs(cps,rs,p,z):

    sqrt_rs = np.sqrt(rs)
    f1 = 1.0 + cps['B2C']*sqrt_rs + cps['B3C']*rs
    d_f1_drs = cps['B2C']/(2*sqrt_rs) + cps['B3C']
    d_f1_drs2 = -cps['B2C']/(4*sqrt_rs**3)
    ec0_lda = -cps['B1C']/f1
    d_ec0lda_drs = cps['B1C']*d_f1_drs/f1**2
    d_ec0lda_drs2 = -2*d_ec0lda_drs*d_f1_drs/f1 + d_f1_drs2*cps['B1C']/f1**2

    dxz,d_dxz_dz = spinf_w_deriv(z,4/3)
    bfac = 1.174*(3/4*(3/(2*pi))**(2/3))*cps['B3C']/cps['B1C']
    gc1 = 1 - bfac*(dxz - 1)
    gc2 = 1 - z**(12)
    gcz = gc1*gc2
    d_gcz_dz = -bfac*d_dxz_dz*gc2 - 12*gc1*z**(11)

    ecl0 = ec0_lda*gcz
    d_ecl0_drs = d_ec0lda_drs*gcz
    d_ecl0_dz = ec0_lda*d_gcz_dz
    d_ecl0_drs2 = d_ec0lda_drs2*gcz
    d_ecl0_drsdz = d_ec0lda_drs*d_gcz_dz

    w0 = np.expm1(-ec0_lda/cps['B1C'])
    d_w0_drs = -d_ec0lda_drs/cps['B1C']*(w0 + 1)

    chi_inf = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))
    gf_inf = 1.0/(1.0 + 4.0*chi_inf*p)**(1.0/4.0)
    d_gf_inf_dp = -chi_inf*gf_inf**5

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = cps['B1C']*np.log(hcore0)
    d_h0_drs = cps['B1C']*d_w0_drs*(1.0 - gf_inf)/hcore0
    d_h0_dp = -cps['B1C']*w0*d_gf_inf_dp/hcore0

    eps0 = (ec0_lda + h0)*gcz
    d_eps0_drs = (d_ec0lda_drs + d_h0_drs)*gcz
    d_eps0_dz = (ec0_lda + h0)*d_gcz_dz
    d_eps0_dp = d_h0_dp*gcz

    if Settings.BETARS == 'revTPSS':
        brs_den = 1 + BFACTOR*rs
        brs = BETA_MB*(1 + AFACTOR*rs)/brs_den
        d_brs_drs = BETA_MB*( AFACTOR - BFACTOR)/brs_den**2
    elif Settings.BETARS == 'ACGGA':
        brs_den = 1 + AFACTOR*rs*(1 + DFACTOR*rs)
        brs = BETA_MB*(1 + AFACTOR*rs*(BFACTOR + CFACTOR*rs) )/brs_den
        d_brs_drs = AFACTOR*( BETA_MB*(BFACTOR + 2*CFACTOR*rs) \
            - (1 + 2*DFACTOR*rs)*brs )/brs_den

    phi,d_phi_dz = spinf_w_deriv(z,2/3)
    gp3 = GAMMA*phi**3

    ct2 = (3*pi**2/16)**(2/3)
    t2 = ct2*p/(phi**2*rs)
    d_t2_drs = -t2/rs
    d_t2_dz = -2*t2*d_phi_dz/phi
    d_t2_dp = ct2/(phi**2*rs)

    ecl,d_ecl_drs,d_ecl_dz,d_ecl_drs2, d_ecl_drsdz = ec_pw92_for_r2c(rs,z)

    w1 = np.expm1(-ecl/gp3)
    opw1 = 1 + w1
    d_w1_drs = -d_ecl_drs/gp3*opw1
    d_w1_dz = (-d_ecl_dz + 3*ecl*d_phi_dz/phi)*opw1/gp3

    aa = brs/(GAMMA*w1)
    y = aa*t2
    d_y_drs = ( d_brs_drs/(GAMMA*w1) - d_w1_drs*aa/w1 )*t2 + aa*d_t2_drs
    d_y_dz = -d_w1_dz*aa*t2/w1 + aa*d_t2_dz
    d_y_dp = aa*d_t2_dp

    dp2 = cps['DP']#
    #eta = cps['ETA']#1.e-3
    ds_zeta,d_ds_zeta_dz = spinf_w_deriv(z,5/3)

    _, dief1 = ief_select(np.ones(1),cps,'c',retderiv=True,cpoles=False)
    df1 = dief1[0]
    """
    if Settings.Fint == 'R2':
        df1 = -0.7114023342889984
    elif Settings.Fint == 'R2reopt':
        cfc = get_coeffs('c', use_stored=True)
        df1 = -np.sum(cfc)
    elif Settings.Fint in ['ORIGINAL','MVS','BRAT','ASCAN']:
        df1 = 0.0
    elif Settings.Fint == 'SMTH':
        df1 = (cps['C2C'] - 2*(1 + cps['C2C']))/(1 + cps['C1C'] \
            + cps['C2C'] + (1+cps['C2C'])/cps['DC'])
    elif Settings.Fint == 'JPP' or Settings.Fint == 'BPOLY':
        df1 = cps['C2C']
    """

    if Settings.ISOORB == 'A':
        fac1 = 20/27
        fac2 = 0.0
    elif Settings.ISOORB == 'BA':
        fac1 = 20/27
        fac2 = -45/27*cps['ETA']
    elif Settings.ISOORB == 'B':
        fac1 = 5/27
        fac2 = -5/12

    dyfac = df1/(ds_zeta*gp3*w1)
    d_dyfac_drs = -dyfac*d_w1_drs/w1
    d_dyfac_dz = -dyfac*(d_ds_zeta_dz/ds_zeta + 3*d_phi_dz/phi + d_w1_dz/w1)

    dyc = fac1*rs*(d_ecl0_drs - d_ecl_drs) + fac2*(ecl0 - ecl)
    d_dyc_drs = (fac1 + fac2)*(d_ecl0_drs - d_ecl_drs) + fac1*rs*(d_ecl0_drs2 - d_ecl_drs2)
    d_dyc_dz = fac1*rs*(d_ecl0_drsdz - d_ecl_drsdz) + fac2*(d_ecl0_dz - d_ecl_dz)

    """
    dyfac = df1/(27.0*ds_zeta*gp3*w1)
    d_dyfac_drs = -dyfac*d_w1_drs/w1
    d_dyfac_dz = -dyfac*(d_ds_zeta_dz/ds_zeta + 3*d_phi_dz/phi + d_w1_dz/w1)

    dyc = 20*rs*(d_ecl0_drs - d_ecl_drs) - 45*eta*(ecl0 - ecl)
    d_dyc_drs = (20 - 45*eta)*(d_ecl0_drs - d_ecl_drs) + 20*rs*(d_ecl0_drs2 - d_ecl_drs2)
    d_dyc_dz = 20*rs*(d_ecl0_drsdz - d_ecl_drsdz) - 45*eta*(d_ecl0_dz - d_ecl_dz)
    """

    if Settings.CORRELATION == 'R2':
        damp = np.exp(-(p/dp2**2)**2)
        dydamp = p*damp
        d_dydamp_dp = (1 - 2*p**2/dp2**4)*damp

        delta_y = dyfac*dyc*dydamp
        d_dy_drs = d_dyfac_drs*dyc*dydamp + dyfac*d_dyc_drs*dydamp
        d_dy_dz = d_dyfac_dz*dyc*dydamp + dyfac*d_dyc_dz*dydamp
        d_dy_dp = dyfac*dyc*d_dydamp_dp

        tmp = 1.0 + 4*(y - delta_y)
        if np.any(tmp < 0.0):
            tarr = 1.e20*np.ones_like(y)
            return tarr,tarr,tarr,tarr,tarr,tarr,tarr,tarr
        gt = 1.0/tmp**(0.25)
        dgt = -gt**5
        d_gt_drs = dgt*(d_y_drs - d_dy_drs)
        d_gt_dz = dgt*(d_y_dz - d_dy_dz)
        d_gt_dp = dgt*(d_y_dp - d_dy_dp)

    elif Settings.CORRELATION == 'R2reopt':

        damp = np.exp(-(p/dp2**2)**2)
        dydamp = p*damp
        d_dydamp_dp = (1 - 2*p**2/dp2**4)*damp

        delta_y = dyfac*dyc*dydamp
        d_dy_drs = d_dyfac_drs*dyc*dydamp + dyfac*d_dyc_drs*dydamp
        d_dy_dz = d_dyfac_dz*dyc*dydamp + dyfac*d_dyc_dz*dydamp
        d_dy_dp = dyfac*dyc*d_dydamp_dp

        gt_num = 1 + 8*delta_y
        gt_den = 1 + 16*y + 16*y**2
        gt = (gt_num/gt_den)**(0.125)

        d_gt_drs = gt*(d_dy_drs/gt_num - (1 + 4*y)*d_y_drs/gt_den)
        d_gt_dz = gt*(d_dy_dz/gt_num - (1 + 4*y)*d_y_dz/gt_den)
        d_gt_dp = gt*(d_dy_dp/gt_num - (1 + 4*y)*d_y_dp/gt_den)

    hcore1 = 1 + w1*(1 - gt)
    if np.any(hcore1<0.0):
        tarr = 1.e20*np.ones_like(y)
        return tarr,tarr,tarr,tarr,tarr,tarr,tarr,tarr
    h = gp3*np.log(hcore1)
    d_h_drs = gp3*(d_w1_drs*(1-gt) -w1*d_gt_drs)/hcore1
    d_h_dz = 3*h*d_phi_dz/phi + gp3*(d_w1_dz*(1-gt) -w1*d_gt_dz)/hcore1
    d_h_dp = -gp3*w1*d_gt_dp/hcore1

    eps1 = ecl + h
    d_eps1_drs = d_ecl_drs + d_h_drs
    d_eps1_dz = d_ecl_dz + d_h_dz
    d_eps1_dp = d_h_dp

    return eps0, d_eps0_drs, d_eps0_dz, d_eps0_dp, eps1, d_eps1_drs, d_eps1_dz, d_eps1_dp


def ec_pw92_for_r2c(rs,z):

    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """

    rsh = rs**(0.5)
    def g(v):

        q0 = -2*v[0]*(1 + v[1]*rs)
        dq0 = -2*v[0]*v[1]

        q1 = 2*v[0]*(v[2]*rsh + v[3]*rs + v[4]*rs*rsh + v[5]*rs*rs)
        dq1 = v[0]*(v[2]/rsh + 2*v[3] + 3*v[4]*rsh + 4*v[5]*rs)
        ddq1 = v[0]*(-0.5*v[2]/rsh**3 + 3/2*v[4]/rsh + 4*v[5])

        q2 = np.log(1 + 1/q1)
        dq2 = -dq1/(q1**2 + q1)
        ddq2 = (dq1**2*(1 + 2*q1)/(q1**2 + q1) - ddq1)/(q1**2 + q1)

        g = q0*q2
        dg = dq0*q2 + q0*dq2
        ddg = 2*dq0*dq2 + q0*ddq2

        return g,dg,ddg

    unp_pars = [0.031091,0.21370,7.5957,3.5876,1.6382,0.49294]
    pol_pars = [0.015545,0.20548,14.1189,6.1977,3.3662,0.62517]
    alp_pars = [0.016887,0.11125,10.357,3.6231,0.88026,0.49671]

    fz_den = 0.5198420997897464#(2**(4/3)-2)
    fdd0 = 1.7099209341613653#8/9/fz_den
    dxz,d_dxz_dz = spinf_w_deriv(z,4/3)
    fz = 2*(dxz - 1)/fz_den
    d_fz_dz = 2*d_dxz_dz/fz_den

    ec0,d_ec0_drs,d_ec0_drs2 = g(unp_pars)
    ec1,d_ec1_drs,d_ec1_drs2 = g(pol_pars)
    ac,d_ac_drs,d_ac_drs2 = g(alp_pars)
    z4 = z**4
    fzz4 = fz*z4
    ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

    d_ec_drs = d_ec0_drs*(1 - fzz4) + d_ec1_drs*fzz4 - d_ac_drs/fdd0*(fz - fzz4)

    d_ec_dz = -ac*d_fz_dz/fdd0 + (fz*4*z**3 + d_fz_dz*z4)*(ac/fdd0 + ec1 - ec0)

    d_ec_drs2 = d_ec0_drs2*(1 - fzz4) + d_ec1_drs2*fzz4 - d_ac_drs2/fdd0*(fz - fzz4)

    d_ec_drsdz = -d_ac_drs*d_fz_dz/fdd0 + (fz*4*z**3 + d_fz_dz*z4)*(d_ac_drs/fdd0\
        + d_ec1_drs - d_ec0_drs)

    return ec, d_ec_drs, d_ec_dz, d_ec_drs2,  d_ec_drsdz


def scan2_ief_derivs(alpha,xps,cps):

    _, diefxda = ief_select(alpha,xps,'x',retderiv=True,cpoles=True)

    _, diefcda = ief_select(alpha,cps,'c',retderiv=True,cpoles=True)

    return diefxda, diefcda


if __name__=="__main__":

    eta = 0.0293843160684444#0.12903
    a1x = 6.5543010899064695
    g1 = 0.0
    g2 = 0.0#-1/(4*a1x**2)
    f1,f2,h1,h2 = get_pars_from_g1g2(g1,g2,eta,K0)
    print('f1',f1)
    print('f2',f2)
    print('h1',h1)
    print('h2',h2)
    exit()
    cfx = get_coeffs('x',xpars={'f1': f1,'f2': f2})
    str = 'X: ['
    for i in range(cfx.shape[0]):
        str += '{:},'.format(cfx[i])
    str = str[:-1] +']'
    print(str)

    cfc = get_coeffs('c', use_stored=True)
    str = 'C: ['
    for i in range(cfc.shape[0]):
        str += '{:},'.format(cfc[i])
    str = str[:-1] +']'
    print(str)
    exit()

    print(eps_c_0_high_dens_zeta_0(DEFAULT_C_PARAMS, 1))
    print(eps_c_1_high_dens_zeta_0(DEFAULT_C_PARAMS, 1))
