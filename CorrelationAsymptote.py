from int_ec import opt_c1c_lzlc
from int_ec import get_lzlc_coef
import scipy

def find_c2c_then_c1c(dens, cparams):
    nsteps = 12
    C2C_START = 0.5
    C2C_RANGE = 1.2

    C1C_START = 1.0
    colist = []  # Stores successful high-z results
    success_list = []  # Stores those that pass the Jellium as well

    for c2c_count in range(nsteps):
        cparams['C2C'] = C2C_START + c2c_count*C2C_RANGE/float(nsteps)

        # Dictionary containing results and last bccoef
        c_res = {'ncount': 0, 'success': False, 'bccoef': None, 'error':None, 'C1C': None, 'C2C':None, 'DC':None}
        opt_c1c_lzlc(cparams, c_res, dens, C1C_START, False)

        if c_res['success']:
            # If at least 1 C2C, C1C pair passed thresholds add the success list
            colist.append(c_res)

    return colist

def interp_params_SCAN2(dens, cparams):
    """
    To begin with, lets just do a search in increasing CAC, 
    then smallest CBC, and the smallest CCC that fits
    """

    ERR_TOL = 0.1

    CAC_START = 0.0
    CAC_RANGE = 20.0
    CAC_STEPS = 100

    CBC_START = 0.0
    CBC_RANGE = 10.0
    CBC_STEPS = 50

    CCC_PREC = 0.1
    max_CCC = 2.0

    candidates = []

    accpeted = 0
    rejected = 0

    rej = []
    d = {}  # Needed to store bc_coef

    for i in range(CAC_STEPS):
        cparams['CAC'] = CAC_START + i*CAC_RANGE/float(CAC_STEPS)
        for j in range(CBC_STEPS):
            cparams['CCC'] = CBC_START + j*CBC_RANGE/float(CBC_STEPS)
            cparams['CBC'] = -1.24 - (cparams['CAC'] + cparams['CCC'])
            err = get_lzlc_coef(cparams, d, dens, True)
            res = {
                    'CAC':cparams['CAC'],
                    'CBC':cparams['CBC'],
                    'CCC':cparams['CCC'],
                    'error':err,
                    'bccoef':d['bccoef']
                    }
            if abs(err) < ERR_TOL:
                candidates.append(res)
                print("Accepted {:}, {:}, {:}".format(cparams['CAC'],cparams['CBC'],cparams['CCC']))
                accpeted += 1
            else:
                print("Rejected {:}, {:}, {:} with {:}".format(cparams['CAC'],cparams['CBC'],cparams['CCC'], err))
                rej.append(res)
                rejected += 1

    rej = sorted(rej, key=lambda x: x['error'])
    print('Best failure was {:}, {:}, {:} with {:}'.format(rej[-1]['CAC'],rej[-1]['CBC'],rej[-1]['CCC'], rej[-1]['error']))   
    print("{:} possible candidates were found, {:} were rejected.".format(accpeted, rejected))


def c_global_opt(dens, cparams):
    print("Running Nelder-Mead from (1, 1, 1)...")
    res = scipy.optimize.minimize(obj_func, (1, 1), args = (dens, cparams), method='Nelder-Mead', options={'fatol':0.01,'xatol':10})
    print('Best failure was {:}, {:}, {:} with {:}%'.format(cparams['CAC'], cparams['CBC'], cparams['CCC'], res.fun))   



def obj_func(x, dens, cparams):
    cparams['CAC'] = x[0]
    cparams['CCC'] = x[1]
    cparams['CBC'] = 0.24 - x[0] - x[1]
    d = {}
    err = abs(get_lzlc_coef(cparams, d, dens, True))
    print("Trying {:}, {:}, {:} got {:}, sum, {:}".format(cparams['CAC'], cparams['CBC'], cparams['CCC'], err, -(1 + cparams['CAC']+ cparams['CBC']+cparams['CCC'])))
    return err

