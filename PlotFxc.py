import numpy as np
import SCAN2
import Settings
from Densities_NonSpherical import Atom, GridGenerator
import matplotlib.pyplot as plt
from math import pi
import h5py
from Optimiser import read_QUEST_densities

def read_dens(fnames):
    olist = []
    for f in fnames:
        print("   - Reading ",f)
        with h5py.File(f, 'r') as inp:
            rho = np.array(inp['rho'])
            idxs = rho[:,0]+rho[:,1] > 1e-12  # Filter out really small densities
            grd = np.array(inp['grd'])
            vabs = lambda a,b: np.sum(np.multiply(a,b),axis=0)
            ga = grd[:,0:3]
            gb = grd[:,4:7]
            gaa = np.array(list(map(vabs, ga, ga))) # |grad|^2 spin 0
            gbb = np.array(list(map(vabs, gb, gb))) # |grad|^2 spin 1
            gab = np.array(list(map(vabs, ga, gb)))
            gtt = gaa + gbb + 2*gab
            tau = np.array(inp['tau'])
            xyz = np.array(inp['xyz'])

            out = np.zeros((rho[idxs].shape[0], 10))
            out[:,0] = rho[idxs,0]
            out[:,1] = rho[idxs,1]
            out[:,2] = np.sqrt(gaa[idxs])
            out[:,3] = np.sqrt(gbb[idxs])
            out[:,4] = tau[idxs,0]
            out[:,5] = tau[idxs,1]
            out[:,6] = xyz[idxs,2]
            out[:,7] = np.sqrt(gtt[idxs])
            lap = np.array(inp['lap'])
            out[:,8] = lap[idxs,0]
            out[:,9] = lap[idxs,1]

            olist.append(out)

    return olist

def do_mols(xparams, xparams_ori):
    fig = plt.figure()
    names = ["Ar2", "SiO", "S2", "SiH4"]

    A2B = 1.88971616463207

    markers = {
            "Ar2":[["Ar", 1.0*A2B]],
            "SiO":[["Si", 0.0], ["O", 1.512667*A2B]],
            "S2":[["S", 0.946295*A2B]],
            "SiH4":[["H", 1.47671*A2B], ["Si", 0.0]]
        }


    dens = read_dens(["/Users/james/Documents/19_06_03_plottingatoms/"+x+".out.plot" for x in names])

    for i, n in enumerate(names):
        ax = fig.add_subplot(2, 2, i+1)

        a = dens[i]

        fx0, fx1 = SCAN2.getscan_x(xparams, a[:, 0], a[:, 1], a[:, 2], a[:, 3], a[:, 4], a[:, 5], a[:, 8], a[:, 9], only_Fx=True)

        ax.plot(a[:,6] , fx0, label="$F_x$[SCAN$\\beta_s$] Maj.", c='b')
        ax.plot(a[:,6], fx1, ls = "--", label="$F_x$[SCAN$\\beta_s$] Min.", c='b')

        ax.set_ylim([0.5, 1.174])

        for m in markers[n]:
            al = ax.get_ylim()
            ax.text(m[1], al[0]*1.1, m[0], horizontalalignment="center")
            ax.vlines(m[1], 0.0, 1.174, linestyles=":", colors="gray")

        ax.set_xlim(np.min(a[:,6]), np.max(a[:,6]))

    Settings.Fint = "ORIGINAL"
    Settings.H1X = "ORIGINAL"

    for i, n in enumerate(names):
        ax = fig.add_subplot(2, 2, i+1)
        a = dens[i]
        fx0_ori, fx1_ori = SCAN2.getscan_x(xparams_ori, a[:, 0], a[:, 1], a[:, 2], a[:, 3], a[:, 4], a[:, 5], a[:, 8], a[:, 9], only_Fx=True)

        ax.plot(a[:,6], fx0_ori, ls="-", label="$F_x$[SCAN] Maj.", c='r')
        ax.plot(a[:,6], fx1_ori, ls="--", label="$F_x$[SCAN] Min.", c='r')
        ax.set_title(n)

        if i%2 == 0:
            ax.set_ylabel("$F_x$")
        if i > 1:
            ax.set_xlabel("$z (a_0)$")

        if i == 0:
            ax.legend(loc=4)

    plt.show()
    raise SystemExit



xparams = dict(
    K1 = 0.1738,
    A1 = 5.57996056,
    CONX = 1.0,
    CAX = 1.48,
    CBX = 2.96,
    CCX = 20.53,
    CDX = -18.98,
    FREG = 1.0,
    XETA = 0.703703703704,

    CXBX = 1.0,
    B1X = 0.0,
    B2X = 0.0,
    B3X = 0.0,
    B4X = 0.0
)

xparams_ori = dict(
    K1 = 0.065,
    A1 = 4.9479,
    C1X = 0.667,
    C2X= 0.8,
    DX = 1.24,
    CXBX = 1.0,

    XETA = 20.0/27.0,

    B1X = 0.156632,
    B2X = 0.12083,
    B3X = 0.5,
    B4X = 0.2218
    )

do_mols(xparams, xparams_ori)

names = ["Li", "N", "Ne", "Ar", "Cr", "Cu", "Kr", "Xe"]

dens = []
RMIN = 0.001
RMAX = 6.0

r, h = np.linspace(RMIN, RMAX, 1000, retstep=True)
# n, r, wt = GridGenerator.make_grid(200)

for n in names:
    a = Atom(n)
    d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
    idxs = d0 + d1 > 1.0e-10  # Filter all those with small densities
    try:
        grid = 4*pi*wt[idxs]*r[idxs]**2
    except NameError:
        grid = 4*pi*h*r[idxs]**2
    dens.append(np.stack([d0[idxs], d1[idxs], g0[idxs], g1[idxs], t0[idxs], t1[idxs], grid, g0[idxs]+g1[idxs], l0[idxs], l1[idxs]], axis=1))


fx_list = []
ex_list = []

fig = plt.figure()

for i, a in enumerate(dens):
    ax = fig.add_subplot(len(names)/2, 2, i+1)
    fx0, fx1 = SCAN2.getscan_x(xparams, a[:, 0], a[:, 1], a[:, 2], a[:, 3], a[:, 4], a[:, 5], a[:, 8], a[:, 9], only_Fx=True)
    ex = SCAN2.getscan_x(xparams, a[:, 0], a[:, 1], a[:, 2], a[:, 3], a[:, 4], a[:, 5], a[:, 8], a[:, 9])

    rdens = a[:,6]*(a[:,0]+a[:,1])
    rax = ax.twinx()
    rax.plot(r, 1.174*rdens/np.max(rdens), c='gray', label='$n(r)$')
    rax.set_ylim([0,1.174])
    if i == 0:
        rax.legend(loc=3)
    rax.set_yticks([])

    ax.set_zorder(rax.get_zorder() + 1)
    ax.patch.set_visible(False)
    ax.plot(r, fx0, label="$F_x$[SCAN$\\beta_s$] Maj.", c='b')
    ax.plot(r, fx1, ls="--", label="$F_x$[SCAN$\\beta_s$] Min.", c='b')
    ax.text(0.25, 0.25, names[i], transform=ax.transAxes)

    fx_list.append([fx0, fx1])
    ex_list.append(ex)

Settings.Fint = "ORIGINAL"
Settings.H1X = "ORIGINAL"

ori_fx_list = []
ori_ex_list = []


for i, a in enumerate(dens):
    ax = fig.add_subplot(len(names)/2, 2, i+1)
    fx0_ori, fx1_ori = SCAN2.getscan_x(xparams_ori, a[:, 0], a[:, 1], a[:, 2], a[:, 3], a[:, 4], a[:, 5], a[:, 8], a[:, 9], only_Fx=True)
    ex_ori= SCAN2.getscan_x(xparams_ori, a[:, 0], a[:, 1], a[:, 2], a[:, 3], a[:, 4], a[:, 5], a[:, 8], a[:, 9])

    ax.plot(r, fx0_ori, ls="-", label="$F_x$[SCAN] Maj.", c='r')
    ax.plot(r, fx1_ori, ls="--", label="$F_x$[SCAN] Min.", c='r')
    ax.text(0.25, 0.25, names[i], transform=ax.transAxes)

    if i%2 == 0:
        ax.set_ylabel("$F_x$")
    if i > 5:
        ax.set_xlabel("$r (a_0)$")

    if i == 0:
        ax.legend()

    ax.set_xlim([RMIN,RMAX])

    ori_fx_list.append([fx0_ori, fx1_ori])
    ori_ex_list.append(ex_ori)
    print(names[i], np.dot(ex_ori, a[:,6]))




plt.tight_layout()
plt.show()


