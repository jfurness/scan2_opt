import numpy as np
from os import system

from SCAN2 import get_scan2_Ex_w_derivs,getscan_c_w_derivs,DEFAULT_X_PARAMS,DEFAULT_C_PARAMS
#from fort.min_mod_r2 import min_mod_r2_x_unp,min_mod_r2_c
from fort.SMGGA import smgga_x_unp, smgga_fx,smgga_c

test_fort = True
#if test_fort:
#    system('cd fort ; f2py -c min_mod_r2.f90 -m min_mod_r2')

def test_scan2_derivs():

    nrs = 20
    nzeta = 40
    ntau = 20

    rsl = np.linspace(0.1,20,nrs)
    zl = np.linspace(-0.9,0.9,nzeta)

    d_ex_d_n_avg = 0.0
    d_ec_d_nup_avg = 0.0
    d_ec_d_ndn_avg = 0.0

    d_ex_d_gn_avg = 0.0
    d_ec_d_gn_avg = 0.0

    nl = 3/(4*np.pi*rsl**3)
    step = nl.min()/10

    tw_min = nl.min()/8 # minimum possible value of KED
    tstep = tw_min/10

    p = np.linspace(1.e-6,9.0,nrs) # typical range of p in real systems
    pfac = (4*(3*np.pi**2)**(2/3))
    gstep = p[0]*pfac*nl.min()**(8/3)/10

    alpha = np.linspace(0.0,10.0,ntau)

    xnorm = nrs**2*ntau
    cnorm = nrs**2*nzeta*ntau

    xvar = np.zeros((xnorm,3))
    cvar = np.zeros((cnorm,4))

    ixvar = 0
    icvar = 0

    """
    bl = np.linspace(0.0,1,1000)
    ptmp = bl[:-1]/(1 - bl[:-1])
    bb,pp = np.meshgrid(bl,ptmp)
    fx = np.zeros((bl.shape[0],ptmp.shape[0]))
    for i in range(bl.shape[0]):
        for j in range(ptmp.shape[0]):
            fx[i,j],_,_ = smgga_fx(ptmp[j],bl[j])
            if fx[i,j] < 0.0:
                print(ptmp[j],bl[j],fx[i,j])
    #print(pp[fx>0.0])
    #print(bb[fx>0.0])
    exit()
    """
    """
    rho,drho = np.linspace(np.log(1.e-6),np.log(1.e3),10000,retstep=True)
    r = np.exp(rho)
    wg = 4*np.pi*r**3*drho
    n = np.exp(-2*r)/np.pi
    gn = 2*n
    n[n<1.e-14] = 1.e-14
    gn[gn<1.e-14] = 1.e-14
    t = gn**2/(8*n)
    ex = np.zeros(r.shape)
    ec = np.zeros(r.shape)
    for i in range(r.shape[0]):
        ex[i],_,_,_ = smgga_x_unp(2*n[i],2*gn[i],2*t[i])
        ec[i],_,_,_,_ = smgga_c(n[i],0.0,gn[i],t[i])
    print(np.sum(wg*ex/2),np.sum(wg*ec))
    exit()
    #"""

    for n in nl:

        gnl = (p*pfac*n**(8/3))**(0.5)
        for gn in gnl:

            #tl = gn**2/(8*n)*np.linspace(1.0,3.0,ntau)
            t0 = 3/10*(3*np.pi**2*n)**(2/3)*n
            tw = gn**2/(8*n)
            tl = t0*alpha + tw

            for tau in tl:

                xvar[ixvar,0] = n
                xvar[ixvar,1] = gn
                xvar[ixvar,2] = tau
                ixvar += 1

                for z in zl:

                    nup = n/2*(1 + z)
                    ndn = n/2*(1 - z)

                    cvar[icvar,0] = nup
                    cvar[icvar,1] = ndn
                    cvar[icvar,2] = gn
                    cvar[icvar,3] = tau
                    icvar += 1

    if test_fort:
        ex = np.zeros(xnorm)
        dexdn = np.zeros(xnorm)
        dexdgn = np.zeros(xnorm)
        dexdt = np.zeros(xnorm)
        ex_n1 = np.zeros(xnorm)
        ex_gn1 = np.zeros(xnorm)
        ex_t1 = np.zeros(xnorm)
        #p = xvar[:,1]**2/(4*(3*np.pi**2)**(2/3)*xvar[:,0]**(8/3))
        #fx = np.zeros(xnorm)
        for i in range(xnorm):
            ex[i],dexdn[i],dexdgn[i],dexdt[i] = smgga_x_unp(xvar[i,0], xvar[i,1], xvar[i,2])
            #fx[i] = ex[i]*(-4*np.pi/(3*(3*np.pi**2)**(1/3)))/xvar[i,0]**(4/3)
            ex_n1[i],_,_,_ = smgga_x_unp(xvar[i,0]+step, xvar[i,1], xvar[i,2])
            ex_gn1[i],_,_,_ = smgga_x_unp(xvar[i,0], xvar[i,1]-gstep, xvar[i,2])
            ex_t1[i],_,_,_ = smgga_x_unp(xvar[i,0], xvar[i,1], xvar[i,2] + tstep)
    else:
        ex,dexdn,dexdgn,dexdt = get_scan2_Ex_w_derivs(DEFAULT_X_PARAMS, xvar[:,0], \
            xvar[:,1], xvar[:,2], 0.0)

        ex_n1,_,_,_ = get_scan2_Ex_w_derivs(DEFAULT_X_PARAMS, xvar[:,0] + step ,\
            xvar[:,1], xvar[:,2], 0.0)

        # subtract step for the gradient derivative to ensure that tau >= tau_W
        ex_gn1,_,_,_ = get_scan2_Ex_w_derivs(DEFAULT_X_PARAMS, xvar[:,0], \
            xvar[:,1] - gstep, xvar[:,2], 0.0)

        ex_t1,_,_,_ = get_scan2_Ex_w_derivs(DEFAULT_X_PARAMS, xvar[:,0], xvar[:,1],\
            xvar[:,2] + tstep, 0.0)

    d_ex_d_n_avg = np.sum(np.abs(dexdn - (ex_n1-ex)/step))/xnorm
    d_ex_d_gn_avg = np.sum(np.abs(dexdgn - (ex - ex_gn1)/gstep))/xnorm
    d_ex_d_t_avg = np.sum(np.abs(dexdt - (ex_t1 - ex)/tstep))/xnorm

    if test_fort:
        ec = np.zeros(cnorm)
        decdnup = np.zeros(cnorm)
        decdndn = np.zeros(cnorm)
        decdgn = np.zeros(cnorm)
        decdt = np.zeros(cnorm)
        ec_nup1 = np.zeros(cnorm)
        ec_ndn1 = np.zeros(cnorm)
        ec_gn1 = np.zeros(cnorm)
        ec_t1 = np.zeros(cnorm)
        for i in range(cnorm):
            ec[i], decdnup[i], decdndn[i], decdgn[i], decdt[i] = \
                smgga_c(cvar[i,0], cvar[i,1], cvar[i,2], cvar[i,3])
            ec_nup1[i], _, _, _, _ = smgga_c(cvar[i,0]+step, cvar[i,1], cvar[i,2],\
                cvar[i,3])
            ec_ndn1[i], _, _, _, _ = smgga_c(cvar[i,0], cvar[i,1]+step, cvar[i,2],\
                cvar[i,3])
            ec_gn1[i], _, _, _, _ = smgga_c(cvar[i,0], cvar[i,1], cvar[i,2]-gstep,\
                cvar[i,3])
            ec_t1[i], _, _, _, _ = smgga_c(cvar[i,0], cvar[i,1], cvar[i,2], \
                cvar[i,3] + tstep)

    else:
        ec, decdnup, decdndn, decdgn, decdt = getscan_c_w_derivs(DEFAULT_C_PARAMS,\
            cvar[:,0], cvar[:,1], cvar[:,2], cvar[:,3], 0.0)

        ec_nup1, _, _, _, _ = getscan_c_w_derivs(DEFAULT_C_PARAMS, cvar[:,0] + step,\
            cvar[:,1], cvar[:,2], cvar[:,3], 0.0)

        ec_ndn1, _, _, _, _ = getscan_c_w_derivs(DEFAULT_C_PARAMS, cvar[:,0], \
            cvar[:,1] + step, cvar[:,2], cvar[:,3], 0.0)

        ec_gn1, _, _, _, _ = getscan_c_w_derivs(DEFAULT_C_PARAMS, cvar[:,0], \
            cvar[:,1], cvar[:,2] - gstep, cvar[:,3], 0.0)

        ec_t1, _, _, _, _ = getscan_c_w_derivs(DEFAULT_C_PARAMS, cvar[:,0], \
            cvar[:,1], cvar[:,2], cvar[:,3] + tstep, 0.0)

    d_ec_d_nup_avg = np.sum(np.abs(decdnup - (ec_nup1-ec)/step))/cnorm
    d_ec_d_ndn_avg = np.sum(np.abs(decdndn - (ec_ndn1-ec)/step))/cnorm
    d_ec_d_gn_avg = np.sum(np.abs(decdgn - (ec - ec_gn1)/gstep))/cnorm
    d_ec_d_t_avg = np.sum(np.abs(decdt - (ec_t1-ec)/tstep))/cnorm

    return d_ex_d_n_avg, d_ex_d_gn_avg, d_ex_d_t_avg, d_ec_d_nup_avg,\
        d_ec_d_ndn_avg, d_ec_d_gn_avg, d_ec_d_t_avg

if __name__=="__main__":

    exn,exg,ext,ecn0,ecn1,ecg,ect = test_scan2_derivs()
    print('< d ex / d n > = {:}'.format(exn))
    print('< d ex / d |grad n| > = {:}'.format(exg))
    print('< d ex / d tau > = {:}'.format(ext))

    print('< d ec / d n_up > = {:}'.format(ecn0))
    print('< d ec / d n_dn > = {:}'.format(ecn1))
    print('< d ec / d |grad n| > = {:}'.format(ecg))
    print('< d ec / d tau > = {:}'.format(ect))
