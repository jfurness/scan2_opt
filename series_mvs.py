import numpy as np
#from functools import partial
from scipy.optimize import least_squares, bisect, minimize_scalar, minimize
import h5py # for QUEST densities
import matplotlib.pyplot as plt
from os import system,path
from itertools import product

# for getting spherical Hartree-Fock densities
import Settings
from Densities import Atom, GridGenerator
import Benchmarks
from Jellium import Jelly
from int_ex import get_coeff_from_ex
from int_ec import get_coeff_from_ec

if not path.isdir('./series_figs/'):
    system('mkdir ./series_figs')

# Load defaults with r2 / SCAN values
# This is what the routine starts with, just so all variables are properly initialized
xpars_default = {'A1': 4.9479, 'K1': 0.065, 'DX': 1.24, 'CP': 0.27}

cpars_default = {'B1C': 0.0285764, 'B2C': 0.0889, 'B3C': 0.125541, 'DC': 0.7}

# used to say if a calculation is unrestricted (None), restricted ('UNP'), or one-electron ('OES')
atom_rest = {}
for at in Benchmarks.BM_LIST+Benchmarks.NON_SPHERICAL_LIST:
    atom_rest[at] = None
for at in ['Ne','Ar','Kr','Xe']:
    atom_rest[at] = 'UNP'

"""
    Constants
"""
bl_ref = np.linspace(0.0,1.0,5000)

soft_zero = 1.e-12

pi = np.pi
GAMMA = (1.0 - np.log(2.0))/pi**2

MIN_DEN = 1e-10
K0 = 0.174
MU_GE2 = 10/81

BETA_MB = 0.066725
if Settings.BETARS == 'revTPSS':
    AFACTOR = 0.1778
    BFACTOR = 0.1/AFACTOR
    CFACTOR = 0.0
    DFACTOR = 0.0
    BETA_INF = BETA_MB*BFACTOR
elif Settings.BETARS == 'ACGGA':
    AFACTOR = 0.5
    BFACTOR = 1.0
    CFACTOR = 0.16667
    DFACTOR = 0.29633
    BETA_INF = BETA_MB*CFACTOR/DFACTOR

def beta_gec(rs):
    beta_num = 1 + AFACTOR*rs*(BFACTOR + CFACTOR*rs)
    beta_den = 1 + AFACTOR*rs*(1 + DFACTOR*rs)
    return BETA_MB*beta_num/beta_den

cpars_default['CHI_inf'] = (3*pi**2/16)**(2/3)*BETA_INF/(0.9 - 3*(3/(16*pi))**(2/3))

"""
    Series interpolation function, can be used for X or C.
    Expects to get a limit for f(beta=1) = ief_inf
"""

def fx_non_neg_test(ax,xpars):
    ptmp = 0.99*bl_ref/(xpars['CP']*(1 - 0.99*bl_ref))
    fxtmp = series_MGGA_Fx(ptmp,bl_ref,ax,xpars)
    return np.any(fxtmp < 0.0)

def ief(b,pars,imin=1):
    tbm1 = 2*b - 1
    tmp = np.zeros_like(b)
    tmp[:] = tbm1

    ief = np.zeros_like(b)

    for i in range(len(pars)):
        ief += pars[i]*tmp
        tmp *= tbm1

    return ief

def dief(b,pars,imin=1):
    tbm1 = 2*b - 1
    tmp = tbm1**(imin-1)
    dief = np.zeros_like(b)
    for i in range(len(pars)):
        dief += (i+imin)*pars[i]*tmp
        tmp *= tbm1
    return 2*dief

"""
def get_ief_pars(b_pars,ief_inf,imin=1,cons_f1=False):

    if cons_f1:
        Mb = len(b_pars)+3
        lbd = 1
    else:
        Mb = len(b_pars)+2
        lbd = 0
    a = np.zeros(Mb)

    if cons_f1:
        a[0] = -54*MU_GE2/(65*K0)

    a[lbd:-2] = b_pars
    s1 = 0.0
    for i in range(Mb-1):
        s1 += (-1)**(i+imin)*a[i]

    s1 = (-1)**(Mb+imin-1)*(1 - s1)
    s2 = -(ief_inf + np.sum(a[:-2]))

    a[-1] = 0.5*(s2 + s1)
    a[-2] = 0.5*(s2 - s1)

    return a
"""

def get_ief_pars(b_pars,d,imin=1,cons_f1=True):

    M = len(b_pars)
    if cons_f1:
        Mb = M+4
        lbd = 1
    else:
        Mb = M+3
        lbd = 0
    a = np.zeros(Mb)

    if cons_f1:
        a[0] = -54*MU_GE2/(65*K0)

    a[lbd:-3] = b_pars
    s1 = 0.0
    s3 = 0.0
    for i in range(M+1):
        s1 += (-1)**(i+imin)*a[i]
        s3 -= (i+imin)*a[i]

    s1 = (-1)**(M + 4)*(1 - s1)
    s2 = -(d + np.sum(a[:-3]))

    a[-1] = ( s3 - (M + 2)*(s1+s2)/2 + (M + 3)*(s1-s2)/2) /2
    a[-2] = (s2 - s1)/2
    a[-3] = (s1 + s2 )/2 - a[-1]

    return a

"""
    Exchange subroutines
"""

def series_MGGA_Fx(p,beta,ax,xpars):

    ief_x = ief(beta,ax,imin=1)

    gx = np.ones_like(p)
    pmsk = p > 1.e-16
    gx[pmsk] = -np.expm1(-xpars['A1']/p[pmsk]**(0.25))

    return (1 + K0*ief_x)*gx

def e_x_series_MGGA_unp(n,gn,tau,b_pars,xpars):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3*kf*n/(4*pi)
    p = (gn/(2*kf*n))**2

    tau_W = gn**2/(8*n)
    tau_TF = 0.3*kf**2*n
    beta = np.minimum(1.0,np.maximum(0.0,(tau - tau_W)/(tau + tau_TF)))

    ax = get_ief_pars(b_pars,xpars['DX'],imin=1,cons_f1=True)
    Fx = series_MGGA_Fx(p,beta,ax,xpars)
    return exlda*Fx

def e_x_series_MGGA(n0,n1,g0,g1,t0,t1,b_pars,xpars,typ=None):

    ex0 = e_x_series_MGGA_unp(2*n0,2*g0,2*t0,b_pars,xpars)
    if typ is None:
        ex1 = e_x_series_MGGA_unp(2*n1,2*g1,2*t1,b_pars,xpars)
        ex = (ex0 + ex1)/2
    elif typ == 'UNP':
        # spin-unpolarized system
        ex = ex0
    elif typ == 'OES':
        # fully spin-polarized, one-electron system with zero spin-down density
        ex = ex0/2
    return ex

def get_Ex_atoms(dens,b_pars,xpars,typ=None):

    ex_dens = e_x_series_MGGA(dens[:,0],dens[:,1],dens[:,2],dens[:,3],dens[:,4],\
        dens[:,5], b_pars, xpars,typ=typ)
    ex_dens[dens[:, 0] + dens[:, 1] < MIN_DEN] = 0.0

    return np.dot(dens[:,6],ex_dens)

"""
    Ix(ij) matrix elements of Ex
"""

def get_Ix_unp(n,gn,tau,wg,xpars,rank,mask=None):

    kf = (3*pi**2*n)**(1/3)
    exlda = -3*kf*n/(4*pi)

    p = (gn/(2*kf*n))**2

    tau_W = gn**2/(8*n)
    tau_TF = 0.3*kf**2*n
    beta = np.minimum(1.0,np.maximum(0.0,(tau - tau_W)/(tau + tau_TF)))

    tbm1 = 2*beta - 1

    gx = np.ones_like(p)
    pmsk = p > 1.e-16
    gx[pmsk] = -np.expm1(-xpars['A1']/p[pmsk]**(0.25))

    Ix = np.zeros(rank)

    if mask is None:
        mask = np.ones(p.shape,dtype=bool)

    tmpigrd = wg*exlda*gx

    for i in range(rank):
        Ix[i] = np.sum(tmpigrd[mask])
        tmpigrd *= tbm1

    return Ix

def get_Ix(n0,n1,g0,g1,t0,t1,wg,xpars,rank,typ=None):

    msk = n0 + n1 >= MIN_DEN
    Ix0 = get_Ix_unp(2*n0,2*g0,2*t0,wg,xpars,rank,mask=msk)
    if typ is None:
        Ix1 = get_Ix_unp(2*n1,2*g1,2*t1,wg,xpars,rank,mask=msk)
        Ix = (Ix0 + Ix1)/2
    elif typ == 'UNP':
        # spin-unpolarized system
        Ix = Ix0
    elif typ == 'OES':
        # fully spin-polarized, one-electron system with zero spin-down density
        Ix = Ix0/2

    return Ix

def get_Ex_from_Ix(Ix,b_pars,xpars):

    a = get_ief_pars(b_pars,xpars['DX'],imin=1,cons_f1=True)
    Ex = Ix[0] + K0*np.dot(a,Ix[1:])

    return Ex

"""
    Correlation subroutines
"""

def spinf(z,n):
    opz = np.minimum(2,np.maximum(0.0,1+z))
    omz = np.minimum(2,np.maximum(0.0,1-z))
    return (opz**n + omz**n)/2.0

def eps_c_pw92(rs,z,unp=False):

    """
        J.P. Perdew and Y. Wang,
        ``Accurate and simple analytic representation of the electron-gas correlation energy'',
        Phys. Rev. B 45, 13244 (1992).
        https://doi.org/10.1103/PhysRevB.45.13244
    """
    rsh = rs**(0.5)

    def g(v):
        q0 = -2*v[0]*(1 + v[1]*rs)
        q1 = 2*v[0]*(v[2]*rsh + v[3]*rs + v[4]*rs*rsh + v[5]*rs*rs)
        logt = np.log(1 + 1/q1)
        return q0*logt

    unp_pars = [0.031091,0.21370,7.5957,3.5876,1.6382,0.49294]
    ec0 = g(unp_pars)
    if unp:
        return ec0

    pol_pars = [0.015545,0.20548,14.1189,6.1977,3.3662,0.62517]
    alp_pars = [0.016887,0.11125,10.357,3.6231,0.88026,0.49671]

    fz_den = 2**(4/3)-2
    fdd0 = 8/9/fz_den
    dxz = spinf(z,4/3)
    fz = 2*(dxz - 1)/fz_den

    ec1 = g(pol_pars)
    ac = g(alp_pars)
    z4 = z**4
    fzz4 = fz*z4
    ec = ec0 - ac/fdd0*(fz - fzz4) + (ec1 - ec0)*fzz4

    return ec

def cgga0(rs,z,p,cps):

    f1 = 1.0 + cps['B2C']*rs**(0.5) + cps['B3C']*rs
    ec0_lda = -cps['B1C']/f1

    dx_zeta = spinf(z,4/3)
    hx0 = 1.174
    b4c = hx0*(3/4*(3/(2*pi))**(2/3))*cps['B3C']/cps['B1C']
    gc_zeta = (1.0 - b4c*(dx_zeta - 1.0)) * (1.0 - z**(12))

    w0 = np.expm1(-ec0_lda/cps['B1C'])

    gf_inf = 1.0/(1.0 + 4.0*cps['CHI_inf']*p)**(1.0/4.0)

    hcore0 = 1.0 + w0*(1.0 - gf_inf)
    h0 = cps['B1C']*np.log(hcore0)

    eps0 = (ec0_lda + h0)*gc_zeta

    return eps0


def cgga1(rs,z,p,cps,unp=False):

    phi = spinf(z,2/3)
    gp3 = GAMMA*phi**3
    t2 = (3*pi**2/16)**(2/3)*p/(phi**2*rs)

    ecl = eps_c_pw92(rs,z,unp=unp)
    w1 = np.expm1(-ecl/gp3)
    aa = beta_gec(rs)/GAMMA/w1
    y = aa*t2

    gt = 1.0/(1.0 + 4*y)**(0.25)

    h = gp3*np.log(1 + w1*(1 - gt))

    eps1 = ecl + h

    return eps1

def eps_c_pbe(rs,z,p,var='revTPSS',unp=False):
    """
        PBE: [PBE1996]
        PBEsol: [P2008]
    """
    phi = spinf(z,2/3)
    gp3 = GAMMA*phi**3
    t2 = (3.0*pi**2/16.0)**(2.0/3.0)*p/(phi**2*rs)

    ecl = eps_c_pw92(rs,z,unp=unp)
    beta = {'PBE': BETA_MB, 'PBEsol': 0.046,
    'revTPSS': BETA_MB*(1 + 0.1*rs)/(1 + 0.1778*rs),
    }

    aa = beta[var]/GAMMA/np.expm1(-ecl/gp3)
    v = aa*t2
    v2 = v**2

    gt = t2*(1.0 + v)/(1.0 + v + v2)
    hc = 1.0 + beta[var]/GAMMA*gt

    ec = ecl + gp3*np.log(hc)

    return ec

def epsc(n0,n1,gn,tau,b_pars,cps,unp=False):

    n = n0 + n1
    zeta = np.minimum(1.0,np.maximum(-1.0,(n0 - n1)/n))

    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2
    p[gn**2 <= 1e-16] = 0.0

    tau_W = gn**2/(8*n)
    tau_TF = 0.3*kf**2*n
    beta = np.minimum(1.0,np.maximum(0.0,(tau - tau_W)/(tau + tau_TF)))

    ac = get_ief_pars(b_pars,cps['DC'],imin=3)
    tbm1 = 2*beta - 1
    tmp = tbm1**3

    ief_c = np.zeros_like(beta)

    for i in range(len(ac)):
        ief_c += ac[i]*tmp
        tmp *= tbm1

    eps_c0 = cgga0(rs,zeta,p,cps)
    eps_c1 = cgga1(rs,zeta,p,cps,unp=unp)
    eps_c = eps_c1*(1 - ief_c) + eps_c0*ief_c

    return n*eps_c

def get_Ec_atoms(dens,b_pars,cpars,typ=None):
    ec_dens = epsc(dens[:,0],dens[:,1],dens[:,7],dens[:,4]+dens[:,5],b_pars,cpars,\
        unp=(typ=='UNP'))
    ec_dens[dens[:, 0] + dens[:, 1] < MIN_DEN] = 0.0
    return np.dot(dens[:,6],ec_dens)

def get_Ic(n0,n1,gn,tau,wg,cpars,rank,typ=None,mask=None):

    n = n0 + n1
    if mask is None:
        msk = n > MIN_DEN
    else:
        msk = mask
    zeta = np.minimum(1.0,np.maximum(-1.0,(n0 - n1)/n))

    rs = (3/(4*pi*n))**(1/3)
    kf = (3*pi**2*n)**(1/3)
    p = (gn/(2*kf*n))**2
    p[gn**2 <= 1e-16] = 0.0

    tau_W = gn**2/(8*n)
    tau_TF = 0.3*kf**2*n
    beta = np.minimum(1.0,np.maximum(0.0,(tau - tau_W)/(tau + tau_TF)))

    tbm1 = 2*beta - 1

    Ic = np.zeros(1)#rank)

    eps_c0 = cgga0(rs,zeta,p,cpars)
    #eps_c1 = cgga1(rs,zeta,p,cpars,unp=(typ == 'UNP'))
    eps_c1 = eps_c_pbe(rs,zeta,p,var='revTPSS',unp=(typ == 'UNP'))

    ec_0m1 = (eps_c0 - eps_c1)*n

    tmpigrd = eps_c1*n*wg
    Ic[0] = np.sum(tmpigrd[msk])
    return Ic

    tmpigrd = ec_0m1*tbm1**3*wg
    for i in range(1,rank):
        Ic[i] = np.sum(tmpigrd[msk])
        tmpigrd *= tbm1

    return Ic

def get_Ec_from_Ic(Ic,b_pars,cpars):
    #ac = get_ief_pars(b_pars,cpars['DC'],imin=3)
    Ec = Ic[0] #+ np.dot(ac,Ic[1:])
    return Ec


"""
    Auxiliary fitting routines
"""

def bracket(func,x0,dx,nstep=1000):
    osgn = np.sign(func(x0))
    a = x0
    success = False
    for ix in range(nstep):
        b = a + dx
        csgn = np.sign(func(b))
        if osgn*csgn < 0.0:
            success = True
            break
        a = b

    if not success:
        print('WARNING: could not bracket root')

    return a,b,success

def fit_h_atom():

    rmin = 1.e-6
    rmax = 1.e2
    Nr = 5000
    rho,drho = np.linspace(np.log(rmin),np.log(rmax),Nr,retstep=True)
    r = np.exp(rho)
    wg = 4*pi*r**3*drho

    n = np.exp(-2*r)/pi
    agn = 2*n
    tau = agn**2/(8*n)

    h_ex_exact = -0.3125

    exlda_sr = -2**(1/3)*3*(3*pi**2*n)**(1/3)*n/(4*pi)
    hx0 = 1 + K0
    ex0 = hx0*exlda_sr
    p_sr = 2**(-2/3)*(agn/(2*(3*pi**2*n)**(1/3)*n))**2
    pmsk = p_sr > 1.e-16
    pm_sr = p_sr[pmsk]

    def hobj(cc):
        gx = np.ones_like(p_sr)
        gx[pmsk] = -np.expm1(-cc/pm_sr**(0.25))
        exden = ex0*gx
        return h_ex_exact - np.sum(wg*exden)

    ca,cb,csuc = bracket(hobj,4.8,5.1,nstep=1000)
    if not csuc:
        print('Could not bracket root for A1 (hydrogen atom) param')
        return 1e20
    a1 = bisect(hobj,ca,cb)

    return a1

def set_ec0_lda_pars_scan2(cps,xps,He_dens):

    # Grid
    h = 0.001
    r = np.arange(0.0, 35.0, h)

    # Make density, 2e Z->inf hydrogenic, zeta = 0
    d = 2/pi*np.exp(-2*r)
    s = np.exp(2*r/3)/(6*pi)**(1/3)

    td = {akey: cps[akey] for akey in cps}

    def tmpf(b1c):
        td['B1C'] = b1c
        return cgga0(0.0,0.0,s**2,td)

    c_den = tmpf(1.0)
    Ec = 4*pi*h*np.sum(r**2*d*c_den)
    cps['B1C'] = -0.0467/Ec
    print("Optimal B1C:", cps['B1C'])
    c_den_opt = tmpf(cps['B1C'])
    Ec_opt = np.sum(4*pi*r**2*h*d*c_den_opt)
    print("Is consistent? ", abs(Ec_opt + 0.0467) < 1e-16)

    cps['B3C'] = cps['B1C']*4*pi/3*(4/(9*pi))**(1/3)/(0.67082 - 0.174)
    He_ex = get_Ex_atoms(He_dens, [], xps)

    td = {akey: cps[akey] for akey in cps}

    def tobj(b2c):
        td['B2C'] = b2c
        He_ec = get_Ec_atoms(He_dens,[],td,typ='UNP')
        err = abs(He_ex + He_ec + 1.068)
        return err

    b2c_res = minimize_scalar(tobj, bracket=(0.07,0.09))

    if b2c_res.fun > 1e-9:
        print("Failed on b2c")
        return None
    cps['B2C'] = abs(b2c_res.x)

    return cps['B1C'],cps['B2C'], cps['B3C']

"""
    Density related stuff ripped from Optimiser.py
    Thanks James!
"""

def generate_density(alist):
    """
    Utility function to generate and arrange the orbitals for
    the atoms in alist (strings of atomic symbols).
    """
    out = []
    n, r, wt = GridGenerator.make_grid(200)
    for a in map(Atom, alist):
        d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
        idxs = d0 + d1 > MIN_DEN  # Filter all those with small densities
        # Later functions expect density, |grad|, tau, spherical integration weight.
        # spin 0 then 1 for each quantity
        grid = 4*pi*wt[idxs]*r[idxs]**2

        assert max( np.dot(l0[idxs], grid), np.dot(l1[idxs], grid)) < 1e-3, \
            "Laplacian integral failed for {:}".format(a)

        out.append(np.stack([d0[idxs], d1[idxs], g0[idxs], g1[idxs], t0[idxs], \
            t1[idxs], grid, g0[idxs]+g1[idxs], l0[idxs], l1[idxs]], axis=1))
    return out

def read_QUEST_densities(fnames):
    """
    Reads density from QUEST output format.
    Used to read full DFT grid for non-spherical atoms.

    Note! Extra total gradient as non-colinear density gradients
    are aparent!
    """
    olist = []
    for f in fnames:
        print("   - Reading ",f)
        with h5py.File(f, 'r') as inp:
            rho = np.array(inp['rho'])
            idxs = rho[:,0]+rho[:,1] > 1e-12  # Filter out really small densities
            grd = np.array(inp['grd'])
            vabs = lambda a,b: np.sum(np.multiply(a,b),axis=0)
            ga = grd[:,0:3]
            gb = grd[:,4:7]
            gaa = np.array(list(map(vabs, ga, ga))) # |grad|^2 spin 0
            gbb = np.array(list(map(vabs, gb, gb))) # |grad|^2 spin 1
            gab = np.array(list(map(vabs, ga, gb)))
            gtt = gaa + gbb + 2*gab
            tau = np.array(inp['tau'])
            xyz = np.array(inp['xyz'])

            out = np.zeros((rho[idxs].shape[0], 10))
            out[:,0] = rho[idxs,0]
            out[:,1] = rho[idxs,1]
            out[:,2] = np.sqrt(gaa[idxs])
            out[:,3] = np.sqrt(gbb[idxs])
            out[:,4] = tau[idxs,0]
            out[:,5] = tau[idxs,1]
            try:
                out[:,6] = xyz[idxs,3]  # Grid weights
            except IndexError:
                # Weights are missing so assume this is a spherical integal
                print("No integration weights found in ", f, " assuming spherical")
                out[:,6] = np.ones_like(rho[idxs,0])
            out[:,7] = np.sqrt(gtt[idxs])

            if Settings.LOAD_LAP:
                lap = np.array(inp['lap'])
                out[:,8] = lap[idxs,0]
                out[:,9] = lap[idxs,1]

                # Test laplacian correctness
                lapint = max(np.dot(out[:,8], out[:,6]), np.dot(out[:,9], out[:,6]))
                assert lapint < 1e-3, \
                    "Laplacian integral failed for {:} at {:}".format(f, lapint)

            olist.append(out)

    return olist

def read_QUEST_restricted_densities(fnames):
    """
    Reads density from QUEST output format for spin restricted format.

    Note! Extra total gradient as non-colinear density gradients
    are aparent!
    """
    olist = []
    for f in fnames:
        print("   - Reading ",f)
        with h5py.File(f, 'r') as inp:
            rho = np.array(inp['rho'])
            idxs = rho[:,0] > 1e-12  # Filter out really small densities
            grd = np.array(inp['grd'])
            vabs = lambda a,b: np.sum(np.multiply(a,b),axis=0)
            ga = grd[:,0:3]
            gaa = np.array(list(map(vabs, ga, ga))) # |grad|^2 spin 0
            tau = np.array(inp['tau'])
            xyz = np.array(inp['xyz'])

            out = np.zeros((rho[idxs].shape[0], 10))
            out[:,0] = rho[idxs,0]/2.0
            out[:,1] = rho[idxs,0]/2.0

            out[:,2] = np.sqrt(gaa[idxs])/2.0
            out[:,3] = np.sqrt(gaa[idxs])/2.0
            out[:,4] = tau[idxs,0]/2.0
            out[:,5] = tau[idxs,0]/2.0
            try:
                out[:,6] = xyz[idxs,3]
            except IndexError:
                out[:,6] = 4*pi*xyz[idxs,2]**2*(xyz[1,2]-xyz[0,2])
            out[:,7] = np.sqrt(gaa[idxs])

            if Settings.LOAD_LAP:
                lap = np.array(inp['lap'])
                out[:,8] = lap[idxs,0]/2.0
                out[:,9] = lap[idxs,0]/2.0

                # Test laplacian correctness
                assert max( np.dot(out[:,8], out[:,6]), np.dot(out[:,9], out[:,6])) < 1e-3, \
                    "Laplacian integral failed for {:}".format(f)
            else:
                try:
                    lap = np.array(inp['lap'])
                    print("WARNING: Laplacian data is present but we are NOT keeping it!")
                except KeyError:
                    continue

            olist.append(out)

    return olist

"""
    Residuum errors
"""

def jsextr(zs, ens):
    # used to extrapolate to CBS from finite zeta basis sets
    x = np.power(zs,-3.0)
    poly = np.polyfit(x, ens, 1)
    return poly[1]  # Return only intercept

def get_errors(Ix_d,Ic_d,xpars,cpars,bx_pars,bc_pars,only_obj=False,\
    penalize_deriv=False):

    resd = {
        'OBJ': 0.0, 'GoF': 0.0, 'SA XC MAPE': 0.0, 'SA X MAPE': 0.0, 'SA C MAPE': 0.0,
        'NSA XC MAPE': 0.0, 'NSA X MAPE': 0.0, 'NSA C MAPE': 0.0,
        'LZ Bx': 0.0, 'LZ Bx PE': 0.0, 'LZ Bc': 0.0, 'LZ Bc PE': 0.0,
        'JS XC MAPE': 0.0, 'JS X MAPE': 0.0, 'JS C MAPE': 0.0,
        'AR2 XC MAPE': 0.0
        }
    wgts = {'SA': 0.15, 'NSA': 0.3, 'LZ': 1.0, 'JS': 3.0, 'AR2': 0.6}
    norm_pe = []
    #for sep in Benchmarks.AR2_SEPS:
    #    resd['AR2 '+sep] = 0.0

    n_sa = len(Benchmarks.BM_LIST)
    sa_x = np.zeros(n_sa)
    sa_c = np.zeros(n_sa)
    lz_ats = ['Ne','Ar','Kr','Xe']
    lz_x_d = {}
    lz_c_d = {}
    for iat,at in enumerate(Benchmarks.BM_LIST):
        sa_x[iat] = get_Ex_from_Ix(Ix_d[at],bx_pars,xpars)
        sa_c[iat] = get_Ec_from_Ic(Ic_d[at],bc_pars,cpars)
        if at in lz_ats:
            lz_x_d[at] = sa_x[iat]
            lz_c_d[at] = sa_c[iat]

    sa_xc = sa_x + sa_c
    sa_xc_pe = 100*(sa_xc-Benchmarks.BM_XC)/(Benchmarks.BM_XC)
    sa_x_pe = 100*(sa_x-Benchmarks.BM_X)/(Benchmarks.BM_X)
    sa_c_pe = 100*(sa_c-Benchmarks.BM_C)/(Benchmarks.BM_C)
    for ierr,err in enumerate(sa_xc_pe):
        resd['GoF'] += err**2
        norm_pe.append(err/wgts['SA'])
        #norm_pe.append(sa_x_pe[ierr]/wgts['SA'])
        #norm_pe.append(sa_c_pe[ierr]/wgts['SA'])

    resd['SA XC MAPE'] = np.sum( np.abs(sa_xc_pe) )/n_sa
    resd['SA X MAPE'] = np.sum( np.abs(sa_x_pe) )/n_sa
    resd['SA C MAPE'] = np.sum( np.abs(sa_c_pe) )/n_sa

    _, resd['LZ Bx'] = get_coeff_from_ex([lz_x_d['Ne'],lz_x_d['Ar'],lz_x_d['Kr'],lz_x_d['Xe']])
    resd['LZ Bx PE'] = 100*(resd['LZ Bx'] - Benchmarks.LZ_X_B)/(Benchmarks.LZ_X_B)
    resd['GoF'] += resd['LZ Bx PE']**2
    norm_pe.append(resd['LZ Bx PE']/wgts['LZ'])

    resd['LZ Bc'] = get_coeff_from_ec([lz_c_d['Ne'],lz_c_d['Ar'],lz_c_d['Kr'],lz_c_d['Xe']])
    resd['LZ Bc PE'] = 100*(resd['LZ Bc'] - Benchmarks.LZ_C_B)/(Benchmarks.LZ_C_B)
    resd['GoF'] += resd['LZ Bc PE']**2
    norm_pe.append(resd['LZ Bc PE']/wgts['LZ'])

    n_nsa = len(Benchmarks.NON_SPHERICAL_LIST)
    nsa_x = np.zeros(n_nsa)
    nsa_c = np.zeros(n_nsa)

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        nsa_x[iat] = get_Ex_from_Ix(Ix_d[at],bx_pars,xpars)
        nsa_c[iat] = get_Ec_from_Ic(Ic_d[at],bc_pars,cpars)

    nsa_xc = nsa_x + nsa_c

    nsa_xc_pe = 100*(nsa_xc-Benchmarks.NON_SPHERICAL_XC)/(Benchmarks.NON_SPHERICAL_XC)
    tmp = np.array(Benchmarks.NON_SPHERICAL_X)
    nsa_x_pe = 100*(nsa_x - tmp)/tmp
    tmp = np.array(Benchmarks.NON_SPHERICAL_C)
    nsa_c_pe = 100*(nsa_c - tmp)/tmp
    for ierr,err in enumerate(nsa_xc_pe):
        resd['GoF'] += err**2
        norm_pe.append(err/wgts['NSA'])
        #norm_pe.append(nsa_x_pe[ierr]/wgts['NSA'])
        #norm_pe.append(nsa_c_pe[ierr]/wgts['NSA'])

    resd['NSA XC MAPE'] = np.sum( np.abs(nsa_xc_pe) )/n_nsa
    resd['NSA X MAPE'] = np.sum( np.abs(nsa_x_pe) )/n_nsa
    resd['NSA C MAPE'] = np.sum( np.abs(nsa_c_pe) )/n_nsa

    n_js = len(Settings.JELLIUM_RS_LIST)
    if Settings.DO_JELLIUM:

        sigx = np.zeros(n_js)
        sigc = np.zeros(n_js)

        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):
            rs_str = str(rs)
            sigx[irs] = get_Ex_from_Ix(Ix_d[rs_str],bx_pars,xpars) \
                - Ix_d[rs_str+'_unif']
            sigc[irs] = get_Ec_from_Ic(Ic_d[rs_str],bc_pars,cpars) \
                - Ic_d[rs_str+'_unif']

        sigxc = sigx + sigc

        js_xc_pe = 100*(sigxc - Benchmarks.JELLIUM_XC)/Benchmarks.JELLIUM_XC
        js_x_pe = 100*(sigx - Benchmarks.JELLIUM_X)/Benchmarks.JELLIUM_X
        js_c_pe = 100*(sigc - Benchmarks.JELLIUM_C)/Benchmarks.JELLIUM_C
        for ierr,err in enumerate(js_xc_pe):
            resd['GoF'] += err**2
            norm_pe.append(err/wgts['JS'])
            #norm_pe.append(js_x_pe[ierr]/wgts['JS'])
            #norm_pe.append(js_c_pe[ierr]/wgts['JS'])

        resd['JS XC MAPE'] = np.sum( np.abs(js_xc_pe) )/n_js
        resd['JS X MAPE'] = np.sum( np.abs(js_x_pe) )/n_js
        resd['JS C MAPE'] = np.sum( np.abs(js_c_pe) )/n_js

    n_ar2 = len(Benchmarks.AR2_LIST)
    if Settings.DO_AR2:

        exl_ar2 = np.zeros(n_ar2)
        ecl_ar2 = np.zeros(n_ar2)

        for i,sep in enumerate(Benchmarks.AR2_LIST):
            # conversion to kcal/mol already included in Ix_d and Ic_d
            exl_ar2[i] = get_Ex_from_Ix(Ix_d[sep],bx_pars,xpars)
            ecl_ar2[i] = get_Ec_from_Ic(Ic_d[sep],bc_pars,cpars)

        exc_ar2 = exl_ar2 + ecl_ar2
        etot = {}

        for i, sep in enumerate(Benchmarks.AR2_LIST):
            etot[sep] = exc_ar2[i] + Benchmarks.AR2_NO_XC[sep]

        cbs_a = jsextr(np.array([2,3,4]), np.array([etot['ArDZ'], etot['ArTZ'], etot['ArQZ']]))

        ar2_err = []
        n_seps = len(Benchmarks.AR2_SEPS)
        for i,sep in enumerate(Benchmarks.AR2_SEPS):
            cbs_e = jsextr(np.array([2,3,4]), np.array([etot[sep+'DZ'], etot[sep+'TZ'], etot[sep+'QZ']]))
            ar2_err.append(cbs_e - 2*cbs_a - Benchmarks.AR2_BM[sep])
            err = 100*ar2_err[i]/Benchmarks.AR2_BM[sep]
            resd['GoF'] += err**2
            norm_pe.append(err/wgts['AR2'])
            resd['AR2 XC MAPE'] += 100*abs(ar2_err[i])/(n_seps*Benchmarks.AR2_BM[sep])

    """
    resd['OBJ'] = Settings.NORMWG['SA']*abs(resd['SA XC MAPE']) \
        + Settings.NORMWG['NSA']*abs(resd['NSA XC MAPE']) \
        + Settings.NORMWG['LZBX']*abs(resd['LZ Bx PE']) \
        + Settings.NORMWG['LZBX']*abs(resd['LZ Bc PE']) \
        + Settings.NORMWG['JS']*abs(resd['JS XC MAPE']) \
        + Settings.NORMWG['AR2']*abs(resd['AR2 XC MAPE'])
    """

    ngof = n_sa + n_nsa + 2 # 2 is for large-Z coeffs
    if Settings.DO_JELLIUM:
        ngof += n_js
    if Settings.DO_AR2:
        ngof += n_ar2
    resd['GoF'] = resd['GoF']**(0.5)/( (ngof - len(bx_pars))**2 + 1 )**(0.5)

    resd['OBJ'] = ( n_sa*resd['SA XC MAPE'] + n_nsa*resd['NSA XC MAPE'] + abs(resd['LZ Bx PE']) \
        + abs(resd['LZ Bc PE']) + n_js*resd['JS XC MAPE'] + n_ar2*resd['AR2 XC MAPE'] )/ngof

    if only_obj:
        #return resd['OBJ']
        if penalize_deriv:
            diefx = dief(bl_ref,get_ief_pars(bx_pars,xpars['DX'],imin=1,cons_f1=True),imin=1)
            nbadx = diefx[diefx>soft_zero].shape[0]
            norm_pe.append(nbadx)#/max(1.e-2,nbadx))
            diefc = dief(bl_ref,get_ief_pars(bc_pars,cpars['DC'],imin=3,cons_f1=False),imin=3)
            nbadc = diefc[diefc>soft_zero].shape[0]
            norm_pe.append(nbadc)#/max(1.e-2,nbadc))

        ax = get_ief_pars(bx_pars,xpars['DX'],imin=1,cons_f1=True)
        ptmp = 0.99*bl_ref/(xpars['CP']*(1 - 0.99*bl_ref))
        fxtmp = series_MGGA_Fx(ptmp,bl_ref,ax,xpars)
        norm_pe.append(fxtmp[fxtmp<0.0].shape[0])

        return norm_pe
    return resd

"""
    Core Optimization
"""

def validate_implementation(xpars,cpars,full_dens,non_spherical_dens,jellies,ar2_dens):

    # no free pars. sufficient for testing
    xrank = 4
    crank = 3

    Ix_d = {}
    Ic_d = {}

    for iat,at in enumerate(Benchmarks.BM_LIST):
        tdens = full_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at])
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at])

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        tdens = non_spherical_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at])
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at])

    if Settings.DO_JELLIUM:
        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):
            tobj = jellies[irs]

            iwg = tobj.weights*tobj.hat*tobj.CFACT
            tmsk = tobj.d >= 1e-20
            rs_str = str(rs)
            gdens = np.abs(tobj.g)
            ked = 2*tobj.tau_0

            Ix_d[rs_str] = get_Ix_unp(tobj.d,gdens,ked,\
                iwg,xpars,xrank,mask=tmsk)

            exuni = -3.0*tobj.kF0/(4.0*pi)
            sxuni = np.dot(tobj.d*exuni, iwg)
            Ix_d[rs_str + '_unif'] = sxuni

            Ic_d[rs_str] = get_Ic(tobj.d_0,tobj.d_1,gdens,ked,\
                iwg,cpars,crank,typ='UNP',mask=tmsk)

            Ic_d[rs_str + '_unif'] = np.dot(tobj.d*tobj.ECPW92(), iwg)

    if Settings.DO_AR2:
        for i in range(len(Benchmarks.AR2_LIST)):
            tdens = ar2_dens[i]
            nm = Benchmarks.AR2_LIST[i]
            Ix_d[nm] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
                tdens[:,5],tdens[:,6],xpars,xrank,typ='UNP')*Benchmarks.EH_TO_KCAL
            Ic_d[nm] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
                tdens[:,6],cpars,crank,typ='UNP')*Benchmarks.EH_TO_KCAL

    tstr = 'Atom, Ex linear, Ex full, Diff X, Ec linear, Ec full, Diff C\n'
    for iat,at in enumerate(Benchmarks.BM_LIST):
        ex = get_Ex_atoms(full_dens[iat],[],xpars,typ=atom_rest[at])
        iex = get_Ex_from_Ix(Ix_d[at],[],xpars)

        ec = get_Ec_atoms(full_dens[iat],[],cpars,typ=atom_rest[at])
        iec = get_Ec_from_Ic(Ic_d[at],[],cpars)
        tstr += ('{:},'*6 + '{:}\n').format(at,iex,ex,ex-iex,iec,ec,ec-iec)

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        ex = get_Ex_atoms(non_spherical_dens[iat],[],xpars,typ=atom_rest[at])
        iex = get_Ex_from_Ix(Ix_d[at],[],xpars)

        ec = get_Ec_atoms(non_spherical_dens[iat],[],cpars,typ=atom_rest[at])
        iec = get_Ec_from_Ic(Ic_d[at],[],cpars)

        tstr += ('{:},'*6 + '{:}\n').format(at,iex,ex,ex-iex,iec,ec,ec-iec)

    if Settings.DO_JELLIUM:
        tstr += '\nJellium Surface rs, sig_x lin, sig_x full, diff x, sig_c lin, sig_c full, diff c\n'
        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):

            rs_str = str(rs)
            sx_lin = get_Ex_from_Ix(Ix_d[rs_str],[],xpars)-Ix_d[rs_str+'_unif']

            def xfun(ps,n0,n1,g0,g1,t0,t1,l0,l1):
                return e_x_series_MGGA(n0,n1,g0,g1,t0,t1,[],xpars,typ='UNP')
            sx_full = jellies[irs].eval_x_func(xfun, {})

            sc_lin = get_Ec_from_Ic(Ic_d[rs_str],[],cpars) - Ic_d[rs_str+'_unif']

            def cfun(ps,n,gn,t,lap,zeta):
                return epsc(n/2,n/2,gn,t,[],cpars,unp=True)

            sc_full = jellies[irs].eval_c_func(cfun, {})

            tstr += ('{:},'*6 + '{:}\n').format(rs,sx_lin,sx_full,sx_full-sx_lin,\
                sc_lin,sc_full,sc_full-sc_lin)

    if Settings.DO_AR2:
        tstr += '\nAr2, Ex lin, Ex full, diff x, Ec lin, Ec full, diff c\n'
        for i,sep in enumerate(Benchmarks.AR2_LIST):
            ex = get_Ex_atoms(ar2_dens[i],[],xpars,typ='UNP')*Benchmarks.EH_TO_KCAL
            iex = get_Ex_from_Ix(Ix_d[sep],[],xpars)

            ec = get_Ec_atoms(ar2_dens[i],[],cpars,typ='UNP')*Benchmarks.EH_TO_KCAL
            iec = get_Ec_from_Ic(Ic_d[sep],[],cpars)
            tstr += ('{:},'*6 + '{:}\n').format(sep,iex,ex,ex-iex,iec,ec,ec-iec)

    with open('./test_series_mvs.csv','w+') as tfl:
        tfl.write(tstr)

    return


def plot_fitted_funcs(Nbx,Nbc,lpars,xpars,cpars):

    fig, ax = plt.subplots(figsize=(6,4))
    bxps = lpars[:Nbx]
    bcps = lpars[Nbx:]
    cax =  get_ief_pars(bxps,xpars['DX'],imin=1,cons_f1=True)
    cac = get_ief_pars(bcps,cpars['DC'],imin=3)

    bl = np.linspace(0.0,1.0,2000)
    tbm1 = 2*bl - 1

    tmp = np.ones_like(bl)
    tmp[:] = tbm1

    ief_x = np.zeros_like(bl)
    for i in range(len(cax)):
        ief_x += cax[i]*tmp
        tmp *= tbm1
    ax.plot(bl,ief_x,color='darkblue',label='$f_\\mathrm{x}(\\beta)$')

    tmp = tbm1**3
    ief_c = np.zeros_like(bl)
    for i in range(len(cac)):
        ief_c += cac[i]*tmp
        tmp *= tbm1

    ax.plot(bl,ief_c,color='darkorange',linestyle='--',label='$f_\\mathrm{c}(\\beta)$')
    ax.legend(fontsize=12)
    ax.set_xlim([0.0,1.0])
    ax.set_xlabel('$\\beta$',fontsize=12)
    ax.set_ylabel('$f(\\beta)$',fontsize=12)

    cstr = '_Nbx={:}_Npx={:}_Nbc={:}'.format(Nbx,Npx,Nbc)
    plt.savefig('./series_figs/ief_xc{:}.pdf'.format(cstr),dpi=600,bbox_inches='tight')

    plt.cla()
    plt.clf()
    plt.close()

    return

def opt_fixed_nl_pars(partup,xpars,cpars,cons_pars,sa_dens,nsa_dens,jellies,\
    ar2_dens,cpbds=None,penalize_deriv=False):

    Ncons = len(cons_pars)

    Nbx, Nbc = partup
    xrank = Nbx+5
    crank = Nbc#+3

    Ix_d = {}
    Ic_d = {}

    for iat,at in enumerate(Benchmarks.BM_LIST):
        tdens = sa_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at])
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at])

    for iat,at in enumerate(Benchmarks.NON_SPHERICAL_LIST):
        tdens = nsa_dens[iat]
        Ix_d[at] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
            tdens[:,5],tdens[:,6],xpars,xrank,typ=atom_rest[at])
        Ic_d[at] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
            tdens[:,6],cpars,crank,typ=atom_rest[at])

    if Settings.DO_JELLIUM:
        for irs,rs in enumerate(Settings.JELLIUM_RS_LIST):
            tobj = jellies[irs]

            iwg = tobj.weights*tobj.hat*tobj.CFACT
            tmsk = tobj.d >= 1e-20
            rs_str = str(rs)
            gdens = np.abs(tobj.g)
            ked = 2*tobj.tau_0

            Ix_d[rs_str] = get_Ix_unp(tobj.d,gdens,ked,\
                iwg,xpars,xrank,mask=tmsk)

            exuni = -3.0*tobj.kF0/(4.0*pi)
            sxuni = np.dot(tobj.d*exuni, iwg)
            Ix_d[rs_str + '_unif'] = sxuni

            Ic_d[rs_str] = get_Ic(tobj.d_0,tobj.d_1,gdens,ked,\
                iwg,cpars,crank,typ='UNP',mask=tmsk)

            Ic_d[rs_str + '_unif'] = np.dot(tobj.d*tobj.ECPW92(), iwg)

    if Settings.DO_AR2:
        for i in range(len(Benchmarks.AR2_LIST)):
            tdens = ar2_dens[i]
            nm = Benchmarks.AR2_LIST[i]
            Ix_d[nm] = get_Ix(tdens[:,0],tdens[:,1],tdens[:,2],tdens[:,3],tdens[:,4], \
                tdens[:,5],tdens[:,6],xpars,xrank,typ='UNP')*Benchmarks.EH_TO_KCAL
            Ic_d[nm] = get_Ic(tdens[:,0],tdens[:,1],tdens[:,7],tdens[:,4]+tdens[:,5],\
                tdens[:,6],cpars,crank,typ='UNP')*Benchmarks.EH_TO_KCAL

    def wrap_obj(comb_pars):
        bx_pars = comb_pars[:Nbx]
        bc_pars = comb_pars[Nbx:Nbx+Nbc]
        conp = comb_pars[Nbx+Nbc:]
        for ipar,apar in enumerate(cons_pars):
            if apar in xpars:
                xpars[apar] = conp[ipar]
            if apar in cpars:
                cpars[apar] = conp[ipar]
        return get_errors(Ix_d,Ic_d,xpars,cpars,bx_pars,bc_pars,only_obj=True,\
            penalize_deriv=penalize_deriv)

    #if Nbx == 0 and Nbc == 0:
    #    return [], get_errors(Ix_d,Ic_d,xpars,cpars,[],[],only_obj=False)

    lbds = []
    ubds = []
    for i in range(Nbx+Nbc):
        lbds.append(-np.inf)
        ubds.append(np.inf)

    if cpbds is not None:
        for abd in cpbds:
            lbds.append(abd[0])
            ubds.append(abd[1])
    else:
        for i in range(Ncons):
            lbds.append(-np.inf)
            ubds.append(np.inf)

    x0 = []
    for i in range(Nbx+Nbc):
        x0.append(1.0)
    for apar in cons_pars:
        if apar in xpars:
            x0.append(xpars[apar])
        elif apar in cpars:
            x0.append(cpars[apar])

    lsrd = least_squares(wrap_obj,x0,bounds=(lbds,ubds))
    #print(lsrd)
    ax = lsrd.x[:Nbx]
    ac = lsrd.x[Nbx:Nbx+Nbc]
    conp = lsrd.x[Nbx+Nbc:]
    for ipar,apar in enumerate(cons_pars):
        if apar in xpars:
            xpars[apar] = conp[ipar]
        if apar in cpars:
            cpars[apar] = conp[ipar]

    return lsrd.x, get_errors(Ix_d,Ic_d,xpars,cpars,ax,ac,only_obj=False)

def main_opt(nlpar_list,bounds,Nbx=0,Nbc=0,validate_lin=False,ramp=True,\
    penalize_deriv=False):


    # first step, load in defaults for parameters
    xpars = {akey: xpars_default[akey] for akey in xpars_default}
    cpars = {akey: cpars_default[akey] for akey in cpars_default}

    # now enforce Hydrogen atom constraint on exchange energy
    xpars['A1'] = fit_h_atom()
    print('Hydrogen atom constraint:\n      A1 = {:}'.format(xpars['A1']))

    # Now enforce helium atom constraint on correlation energy
    He_dens = generate_density(["He"])

    cpars['B1C'], cpars['B2C'], cpars['B3C'] = set_ec0_lda_pars_scan2(cpars,xpars,He_dens[0])
    tstr = 'Two-electron correlation parameters:\n      B1C = {:}\n'.format(cpars['B1C'])
    tstr += '      B2C = {:}\n      B3C = {:}'.format(cpars['B2C'], cpars['B3C'])

    print(tstr)

    full_dens = generate_density(Benchmarks.BM_LIST)

    non_spherical_dens = read_QUEST_densities([x+".out.plot" for x in Benchmarks.NON_SPHERICAL_LIST])

    jellies = []
    if Settings.DO_JELLIUM:
        for rs in Settings.JELLIUM_RS_LIST:
            jellies.append(Jelly(rs))

    ar2_list = []
    ar2_dens = []
    if Settings.DO_AR2:

        if Settings.SCAN2_DENS:
            ar2_root = "set_dens/SCAN2/Ar2/"
        else:
            ar2_root = "set_dens/SCAN/Ar2/"

        for f in Benchmarks.AR2_LIST:
            sep = f[:-2]
            bas = f[-2:]
            if sep == "Ar":
                ar2_list.append(ar2_root+"Ar_"+bas+".out.plot")
            else:
                ar2_list.append(ar2_root+"Ar2_"+sep+"_"+bas+".out.plot")

        ar2_dens = read_QUEST_restricted_densities(ar2_list)


    """
        Use this to test whether the series meta-GGA energies are computed correctly
    """
    if validate_lin:
        validate_implementation(xpars,cpars,full_dens,non_spherical_dens,\
            jellies,ar2_dens)
        exit()

    if ramp:
        pspace = list(product(np.arange(0,Nbx+1,1),np.arange(0,Nbc+1,1)))
    else:
        pspace = [(Nbx,Nbc)]

    #nl_par_spc = product(*[np.linspace(bounds[i][0],bounds[i][1],10) for i in range(len(bounds))])

    ostr = 'Nbx, Nbc'
    for akey in nlpar_list:
        ostr += ', {:}'.format(akey)

    ostr += ', X IEF Monotonic decr.'
    for i in range(Nbx+4):
        ostr += ', a_x{:}'.format(1+i)

    ostr += ', C IEF Monotonic decr.'
    #for i in range(Nbc+2):
    #    ostr += ', a_c{:}'.format(3+i)

    ostr += ', Non-negative Fx'

    for ipv,pv in enumerate(pspace):

        ibx,ibc = pv

        #lin_pars, res = opt_fixed_nl_pars((ibx,ibc),xpars,cpars,full_dens,\
        #    non_spherical_dens,jellies,ar2_dens)
        lin_pars, resd = opt_fixed_nl_pars((ibx,ibc),xpars,cpars,nlpar_list,full_dens,\
            non_spherical_dens,jellies,ar2_dens,cpbds=bounds,penalize_deriv=penalize_deriv)

        nl_pars_fit = lin_pars[ibx+ibc:]
        for ipar,apar in enumerate(nlpar_list):
            if apar in xpars:
                xpars[apar] = nl_pars_fit[ipar]
            if apar in cpars:
                cpars[apar] = nl_pars_fit[ipar]

        ax = get_ief_pars(lin_pars[:ibx],xpars['DX'],imin=1,cons_f1=True)
        ac = get_ief_pars(lin_pars[ibx:ibx+ibc],cpars['DC'],imin=3)

        if ipv == 0:
            for akey in resd:
                ostr += ', {:}'.format(akey)
            ostr += '\n'

        ostr += '{:}, {:}'.format(*pv)

        for val in nl_pars_fit:
            ostr += ', {:}'.format(val)

        ostr += ', {:}'.format(np.all(dief(bl_ref,ax,imin=1) < soft_zero))
        for i in range(len(ax)):
            ostr += ', {:}'.format(ax[i])
        for i in range(len(ax),Nbx+4):
            ostr += ', '

        ostr += ', {:}'.format(np.all(dief(bl_ref,ac,imin=3) < soft_zero))
        #for i in range(len(ac)):
        #    ostr += ', {:}'.format(ac[i])
        #for i in range(len(ac),Nbc+2):
        #    ostr += ', '

        ostr += ', {:}'.format(not fx_non_neg_test(ax,xpars))
        for akey in resd:
            ostr += ', {:}'.format(resd[akey])
        ostr += '\n'

        with open('./fits/series_mvs.csv','w+') as tfl:
            tfl.write(ostr)

    return

if __name__ == "__main__":

    main_opt(['DX'],[(0.0,1/.174)],Nbx=5,Nbc=0,validate_lin=False,\
        ramp=True,penalize_deriv=False)
    exit()

    bl = np.linspace(0.0,1.0,1001)
    pl = np.linspace(0.0,1.e1,1001)

    imin = 1
    d = 1.24
    b = get_ief_pars([0.5,-0.25,1.7,-.1],d,imin=imin,cons_f1=True)

    ief_x = ief(bl,b,imin=imin)
    print(ief(np.zeros(1),b,imin=imin)[0])
    print(ief(np.ones(1),b,imin=imin)[0],d)
    print(dief(0.5*np.ones(1),b,imin=imin)[0],-108*MU_GE2/(65*K0))
    print(dief(np.ones(1),b,imin=imin)[0])

    plt.plot(bl,ief_x)
    plt.plot(bl,dief(bl,b,imin=imin))
    plt.xlim(0.0,1.0)
    plt.hlines(0.0,0.0,1.0,color='k')
    plt.show()
