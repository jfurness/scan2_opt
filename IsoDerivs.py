import numpy as np
from math import pi
import matplotlib.pyplot as plt
from Densities_NonSpherical import Atom

def make_beta(d0, d1, g0, g1, gt, t0, t1):
    d = d0 + d1
    zeta = (d0 - d1)/d
    zeta[zeta > 0.99999999999] = 0.99999999999
    zeta[zeta < -0.99999999999] = -0.99999999999
    tT = t0 + t1

    g00 = g0**2
    g11 = g1**2
    gTT = gt**2
    gnn = (gTT - g00 - g11)/2.0
    tauw = 1.0/8.0*(gTT/d)
    ds_zeta = (np.power(1.0 + zeta, 5.0/3.0) + np.power(1.0 - zeta, 5.0/3.0))/2.0
    try:
        foo = np.power(d, 5.0/3.0)
    except FloatingPointError:
        print(d)
        raise SystemExit
    tau0 = 0.3*np.power(3*pi**2, 2.0/3.0)*np.power(d, 5.0/3.0)*ds_zeta
    beta = (tT - tauw)/(tT + tau0)

    d_zeta_d0 = 2*d1/d**2
    d_zeta_d1 = -2*d0/d**2

    d_ds_zeta_d0 = 5.0/3.0*((1.0 + zeta)**(2.0/3.0) - (1.0 - zeta)**(2.0/3.0))*d_zeta_d0/2.0
    d_ds_zeta_d1 = 5.0/3.0*((1.0 + zeta)**(2.0/3.0) - (1.0 - zeta)**(2.0/3.0))*d_zeta_d1/2.0

    dtau0_d0 = 5.0/3.0*tau0/d + tau0*d_ds_zeta_d0/ds_zeta
    dtau0_d1 = 5.0/3.0*tau0/d + tau0*d_ds_zeta_d1/ds_zeta

    dtauw_d0 = -tauw/d
    dtauw_d1 = -tauw/d

    db_dd0 = dtau0_d0*(tauw - tT) - (tau0 + tT)*dtauw_d0
    db_dd0 /= (tT + tau0)**2
    db_dd1 = dtau0_d1*(tauw - tT) - (tau0 + tT)*dtauw_d1
    db_dd1 /= (tT + tau0)**2

    dgnn_dg0 = gnn/g0
    dgnn_dg1 = gnn/g1

    dtauw_dg0 = 1.0/4.0/d*(g0 + dgnn_dg0)
    dtauw_dg1 = 1.0/4.0/d*(g1 + dgnn_dg1)

    db_dg0 = -dtauw_dg0/(tT + tau0)
    db_dg1 = -dtauw_dg1/(tT + tau0)

    db_dt0 = (tauw + tau0)/(tT + tau0)**2
    db_dt1 = (tauw + tau0)/(tT + tau0)**2

    return beta, db_dd0, db_dd1, db_dg0, db_dg1, db_dt0, db_dt1

def make_alpha(d0, d1, g0, g1, gt, t0, t1):
    d = d0 + d1
    zeta = (d0 - d1)/d
    zeta[zeta > 0.99999999999] = 0.99999999999
    zeta[zeta < -0.99999999999] = -0.99999999999
    tT = t0 + t1

    g00 = g0**2
    g11 = g1**2
    gTT = gt**2
    gnn = (gTT - g00 - g11)/2.0
    tauw = 1.0/8.0*(gTT/d)
    ds_zeta = (np.power(1.0 + zeta, 5.0/3.0) + np.power(1.0 - zeta, 5.0/3.0))/2.0
    tau0 = 0.3*np.power(3*pi**2, 2.0/3.0)*np.power(d, 5.0/3.0)*ds_zeta
    alpha = (tT - tauw)/tau0

    d_zeta_d0 = 2*d1/d**2
    d_zeta_d1 = -2*d0/d**2

    d_ds_zeta_d0 = 5.0/3.0*((1.0 + zeta)**(2.0/3.0) - (1.0 - zeta)**(2.0/3.0))*d_zeta_d0/2.0
    d_ds_zeta_d1 = 5.0/3.0*((1.0 + zeta)**(2.0/3.0) - (1.0 - zeta)**(2.0/3.0))*d_zeta_d1/2.0

    dtau0_d0 = 5.0/3.0*tau0/d + tau0*d_ds_zeta_d0/ds_zeta
    dtau0_d1 = 5.0/3.0*tau0/d + tau0*d_ds_zeta_d1/ds_zeta

    dtauw_d0 = -tauw/d
    dtauw_d1 = -tauw/d

    da_dd0 = (-dtauw_d0 - alpha*dtau0_d0)/tau0
    da_dd1 = (-dtauw_d1 - alpha*dtau0_d1)/tau0

    dgnn_dg0 = gnn/g0
    dgnn_dg1 = gnn/g1

    dtauw_dg0 = 1.0/4.0/d*(g0 + dgnn_dg0)
    dtauw_dg1 = 1.0/4.0/d*(g1 + dgnn_dg1)

    da_dg0 = -dtauw_dg0/tau0
    da_dg1 = -dtauw_dg1/tau0

    da_dt0 = 1.0/tau0
    da_dt1 = 1.0/tau0

    return alpha, da_dd0, da_dd1, da_dg0, da_dg1, da_dt0, da_dt1

def new_alpha(d0, d1, g0, g1, gt, t0, t1):
    alpha, da_dd0, da_dd1, da_dg0, da_dg1, da_dt0, da_dt1 = make_alpha(d0, d1, g0, g1, gt, t0, t1)

    # ma = alpha/(1.0 + alpha)
    # dma_da = alpha/(1 + alpha**2)

    ma = alpha**2/(1.0 + alpha**2)
    dma_da = 2*alpha/(1 + alpha**2)**2

    return ma, dma_da*da_dd0, dma_da*da_dd1, dma_da*da_dg0, dma_da*da_dg1, dma_da*da_dt0, dma_da*da_dt1

def get_fdiff(func, d0, d1, g0, g1, t0, t1):
    h = 1e-10

    fdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0+h, d1, g0, g1, g0+g1, t0, t1)
    bdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0-h, d1, g0, g1, g0+g1, t0, t1)
    df_dd0 = (fdd-bdd)/(2*h)

    fdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1+h, g0, g1, g0+g1, t0, t1)
    bdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1-h, g0, g1, g0+g1, t0, t1)
    df_dd1 = (fdd-bdd)/(2*h)

    fdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0+h, g1, g0+g1+h, t0, t1)
    bdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0-h, g1, g0+g1-h, t0, t1)
    df_dg0 = (fdd-bdd)/(2*h)

    fdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0, g1+h, g0+g1+h, t0, t1)
    bdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0, g1-h, g0+g1-h, t0, t1)
    df_dg1 = (fdd-bdd)/(2*h)
    
    fdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0, g1, g0+g1, t0+h, t1)
    bdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0, g1, g0+g1, t0-h, t1)
    df_dt0 = (fdd-bdd)/(2*h)

    fdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0, g1, g0+g1, t0, t1+h)
    bdd, dd0, dd1, dg0, dg1, dt0, dt1 = func(d0, d1, g0, g1, g0+g1, t0, t1-h)
    df_dt1 = (fdd-bdd)/(2*h)

    return df_dd0, df_dd1, df_dg0, df_dg1, df_dt0, df_dt1

h = 0.01
r = np.arange(0.001, 10.0, h)
atom = "C"
a = Atom(atom)
d0, d1, g0, g1, t0, t1, l0, l1 = a.get_densities(r)
idxs = (d0+d1) > 1e-10
d0 = d0[idxs]
d1 = d1[idxs]
g0 = np.abs(g0[idxs])
g1 = np.abs(g1[idxs])
t0 = t0[idxs]
t1 = t1[idxs]
r = r[idxs]

alpha, da_dd0, da_dd1, da_dg0, da_dg1, da_dt0, da_dt1 = make_alpha(d0, d1, g0, g1, g0+g1, t0, t1)
fda_dd0, fda_dd1, fda_dg0, fda_dg1, fda_dt0, fda_dt1 = get_fdiff(make_alpha, d0, d1, g0, g1, t0, t1)

ma, dma_dd0, dma_dd1, dma_dg0, dma_dg1, dma_dt0, dma_dt1 = new_alpha(d0, d1, g0, g1, g0+g1, t0, t1)
fdma_dd0, fdma_dd1, fdma_dg0, fdma_dg1, fdma_dt0, fdma_dt1 = get_fdiff(new_alpha, d0, d1, g0, g1, t0, t1)

beta, db_dd0, db_dd1, db_dg0, db_dg1, db_dt0, db_dt1 = make_beta(d0, d1, g0, g1, g0+g1, t0, t1)
fdb_dd0, fdb_dd1, fdb_dg0, fdb_dg1, fdb_dt0, fdb_dt1 = get_fdiff(make_beta, d0, d1, g0, g1, t0, t1)

print(np.sum(4*pi*r**2*(d0+d1)*h))

fig = plt.figure()

ax0 = fig.add_subplot(221)
ax1 = fig.add_subplot(222)
ax2 = fig.add_subplot(223)
ax3 = fig.add_subplot(224)

xr = 5
ax0.set_xlim([0,xr])
ax1.set_xlim([0,xr])
ax2.set_xlim([0,xr])
ax3.set_xlim([0,xr])

do_alpha = False
do_fdiff = False
do_spins = True
do_beta = True

if do_beta:
    ax0.plot(r, beta, c='r', label="$\\beta$")

    ax1.plot(r, db_dd0, c='r', label="d$\\beta$/d$n_0$")
    ax2.plot(r, db_dg0, c='r', label="d$\\beta$/d$|\\nabla n_0|$")
    ax3.plot(r, db_dt0, c='r', label="d$\\beta$/d$\\tau_0$")
    
    if do_spins:
        ax1.plot(r, db_dd1, c='b', label="d$\\beta$/d$n_1$")
        ax2.plot(r, db_dg1, c='b', label="d$\\beta$/d$|\\nabla n_1|$")
        ax3.plot(r, db_dt1, c='b', label="d$\\beta$/d$\\tau_1$")

    if do_fdiff:
        ax1.plot(r, fdb_dd0, ls=':', c='k')
        ax2.plot(r, fdb_dg0, ls=':', c='k')
        ax3.plot(r, fdb_dt0, ls=':', c='k')

        ax1.plot(r, fdb_dd1, ls=':', c='k')
        ax2.plot(r, fdb_dg1, ls=':', c='k')
        ax3.plot(r, fdb_dt1, ls=':', c='k')

if do_alpha:
    ax0.plot(r, alpha)
    ax0.set_ylim([0,5])

    ax1.plot(r, da_dd0, label="d$\\alpha$/d$n_0$")
    ax1.plot(r, fda_dd0, ls="--")
    # ax1.plot(r,da_dd1)
    ax1.set_ylim([-20,20])
    ax1.legend()

    ax2.plot(r, da_dg0, label="d$\\alpha$/d$|\\nabla n_0|$")
    ax2.plot(r, fda_dg0, ls="--")
    # ax2.plot(r, da_dg1)
    ax2.set_ylim([-30,10])
    ax2.legend()

    ax3.plot(r, da_dt0, label="d$\\alpha$/d$\\tau_0$")
    ax3.plot(r, fda_dt0, ls="--")
    # ax3.plot(r, da_dt1)
    ax3.set_ylim([-10,40])
    ax3.legend()
else:
    ax0.plot(r, ma, ls='--', label="$\\alpha^{\\prime}$")
    ax0.set_ylim([0,1])

    ax1.plot(r, dma_dd0, c='#cc0000', ls='--', label="d$\\alpha^{\\prime}$/d$n_0$")
    ax1.set_ylim([-20,20])

    ax2.plot(r, dma_dg0, c='#cc0000', ls='--', label="d$\\alpha^{\\prime}$/d$|\\nabla n_0|$")
    ax2.set_ylim([-30,10])

    ax3.plot(r, dma_dt0, c='#cc0000', ls='--', label="d$\\alpha^{\\prime}$/d$\\tau_0$")
    ax3.set_ylim([-10,40])

    if do_fdiff:
        ax1.plot(r, fdma_dd0, c='k', ls=":")
        ax2.plot(r, fdma_dg0, c='k', ls=":")
        ax3.plot(r, fdma_dt0, c='k', ls=":")

    if do_spins:
        ax1.plot(r, dma_dd1, c='#0000cc', ls='--', label="d$\\alpha^{\\prime}$/d$n_1$")
        ax2.plot(r, dma_dg1, c='#0000cc', ls='--', label="d$\\alpha^{\\prime}$/d$|\\nabla n_1|$")
        ax3.plot(r, dma_dt1, c='#0000cc', ls='--', label="d$\\alpha^{\\prime}$/d$\\tau_1$")

        if do_fdiff:
            ax1.plot(r, fdma_dd1, c='k', ls=":")
            ax2.plot(r, fdma_dg1, c='k', ls=":")
            ax3.plot(r, fdma_dt1, c='k', ls=":")

ax0.legend()
ax1.legend()
ax2.legend()
ax3.legend()

plt.suptitle(atom)
plt.show()


