import numpy
from scipy.linalg import lstsq
import SCAN2
from Atomics import get_Ex_for_atoms

"""
Utility module for quickly getting exchange and correlation energy densities for a
given set of orbitals.

Also handles Large Z limit expansion.
"""


# OLD ROUTINES FOR LARGE Z LIMIT


def opt_c1x_lzlx(xparams, res, dens, do_SCAN2):
    er_tol = 0.1
    maxit = 20

    deltac1x = 0.001
    c1x_old = xparams['C1X']
    aerr, berr = get_lzlx_coef(xparams, res, dens)
    res['C1X'] = xparams['C1X']
    res['C2X'] = xparams['C2X']
    res['DX'] = xparams['DX']
    res['aerror'] = aerr
    res['berror'] = berr
    if abs(berr) <= er_tol:
        res['success'] = True
        return

    while abs(berr) > er_tol and res['ncount'] < maxit:
        res['ncount'] += 1
        xparams['C1X'] = xparams['C1X'] + deltac1x
        fwd_aerr, fwd_berr = get_lzlx_coef(xparams, res, dens, do_SCAN2)

        xparams['C1X'] = (fwd_berr*c1x_old - berr*xparams['C1X'])/(fwd_berr - berr)

        if xparams['C1X'] < 0.0:
            xparams['C1X'] = c1x_old/2.0

        aerr, berr = get_lzlx_coef(xparams, res, dens, do_SCAN2)
        res['C1X'] = xparams['C1X']
        res['C2X'] = xparams['C2X']
        res['DX'] = xparams['DX']
        res['aerror'] = aerr
        res['berror'] = berr
        c1x_old = xparams['C1X']

        if abs(berr) <= er_tol and abs(aerr) <= er_tol:
            res['success'] = True
            break


def show_res(xparams, dens):
    axcoefref = 0.24327
    bxcoefref = -0.22586

    exlist = get_Ex_for_atoms(dens, xparams, False)

    for f in zip(["Ne","Ar","Kr","Xe"],exlist[1:]):
        print(f)
    axcoef, bxcoef = get_coeff_from_ex(exlist[1:]) # Skip helium
    print("a: {:.2f}%, b: {:.2f}%".format(100*(axcoef - axcoefref)/axcoefref, 100*(bxcoef - bxcoefref)/bxcoefref))


def get_lzlx_coef(xparams, dens, exlist_in=[]):
    if len(exlist_in) >0:
        exlist = exlist_in
    else:
        exlist = get_Ex_for_atoms(dens, xparams)

    axcoef, bxcoef = get_coeff_from_ex(exlist)
    return axcoef, bxcoef

def get_coeff_from_ex(exlist):
    # atoms contains He and we don't want that, ignore element 0

    # Ec = a*Z*ln(Z) + b*Z, where a is LDA coef.
    # We fit Ec/Z - a*ln(Z) vs. 1/n, where n is the principle quantum number
    zlist = [10.0, 18.0, 36.0, 54.0]  # List of nuclear charges
    exRGLDA = [-11.033, -27.863, -88.624, -170.562]  # LDA rare gas

    xl = [x**(-1.0/3.0) for x in zlist]

    ylist = []
    for i in range(len(zlist)):
        ylist.append((exlist[i] - exRGLDA[i])/zlist[i])

    A = numpy.vstack([xl, numpy.ones(len(zlist))]).T
    axcoef, bxcoef = lstsq(A, numpy.array(ylist))[0]

    return axcoef, bxcoef

def get_accurate_lz_ex():
    a, b = get_coeff_from_ex([-12.108, -30.188, -93.89, -179.2])
    print("Accurate a: {:}, b: {:}".format(a, b))
    axcoefref = 0.24327
    bxcoefref = -0.22586
    print("\t-\tError: a: {:}%, b: {:}%".format(100*(a - axcoefref)/axcoefref, 100*(b - bxcoefref)/bxcoefref))
