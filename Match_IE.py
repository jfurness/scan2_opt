import numpy as np
import matplotlib.pyplot as plt
from Optimiser import read_QUEST_densities, read_QUEST_restricted_densities, generate_density
from Densities import Atom
import Benchmarks
import Settings
from Jellium import Jelly
from math import pi
import glob
from scipy.optimize import minimize

def get_from_dat(a, alpha):
    iso_0 = find_iso_sr(2*a[:, 0], 2*a[:, 2], 2*a[:, 4], alpha)
    iso_1 = find_iso_sr(2*a[:, 1], 2*a[:, 3], 2*a[:, 5], alpha)
    iso = find_iso(a[:, 0]+a[:, 1], a[:, 7], a[:, 4]+a[:, 5], (a[:, 0] - a[:, 1])/(a[:, 0] + a[:, 1]), alpha)

    return iso_0, iso_1, iso

def find_iso_sr(d, g, t, alpha):
    tau_unif = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d**(5.0/3.0)
    tauw = (g**2)/(8*d)
    if alpha:
        alpha = (t - tauw)/(tau_unif)
        return alpha
    else:
        beta = (t - tauw)/(t + tau_unif)
        return beta

def find_iso(d, g, t, z, alpha):
    ds_zeta = (np.power(1.0 + z, 5.0/3.0) + np.power(1.0 - z, 5.0/3.0))/2.0
    tau_unif = 0.3*np.power(3*pi**2, 2.0/3.0)*np.power(d, 5.0/3.0)*ds_zeta
    tauw = (g**2)/(8*d)
    if alpha:
        alpha = (t - tauw)/(tau_unif)
        return alpha
    else:
        beta = (t - tauw)/(t + tau_unif)
        return beta

def match_obj(p, beta, f_scan, wts):
    A, B, C, D = p
    f_beta = (1 - 2*beta)*(1 + A*beta + B*beta**2 + C*beta**3 + D*beta**4)

    out = np.sum(wts*np.abs(f_beta - f_scan))
    print(p, out)
    return out

fullist = Benchmarks.BM_LIST
spherical = generate_density(fullist)

non_spherical_dens = read_QUEST_densities([x+".out.plot" for x in Benchmarks.NON_SPHERICAL_LIST])

rest_list_ae6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_AE6]
unrest_list_ae6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_AE6]
rest_list_bh6 = ["set_dens/SCAN/"+x+".Rscan.out.plot" for x in Benchmarks.REST_BH6]
unrest_list_bh6 = ["set_dens/SCAN/"+x+".Uscan.out.plot" for x in Benchmarks.UNREST_BH6]
ar2_root = "set_dens/SCAN/Ar2/"

# ae6_dens = read_QUEST_restricted_densities(rest_list_ae6) + read_QUEST_densities(unrest_list_ae6)

# bh6_dens = read_QUEST_restricted_densities(rest_list_bh6) + read_QUEST_densities(unrest_list_bh6)
# 
# ar2_dens = read_QUEST_restricted_densities(glob.glob(ar2_root+"*QZ*.plot"))

jellies = []

for rs in Settings.JELLIUM_RS_LIST:
    j = Jelly(rs)
    idx = j.d > 1e-12
    jellies.append(np.array([
            j.d_0[idx], j.d_1[idx],
            j.grd_0[idx], j.grd_1[idx],
            j.tau_0[idx], j.tau_1[idx],
            j.weights[idx],
            j.g[idx],
            j.lap_0[idx], j.lap_1[idx]
        ]
        ).T)

# data = np.vstack(spherical + non_spherical_dens + ae6_dens + bh6_dens + ar2_dens + jellies)
data = np.vstack(spherical + non_spherical_dens + jellies)
print(data.shape)

AX = -0.7385587663820224058842300326808360
exunif = AX*(data[:, 0] + data[:, 1])**(1.0/3.0)
exlda = exunif*(data[:, 0] + data[:, 1])

# wts = data[:, 6]#*(data[:, 0] + data[:, 1])
wts = data[:, 6]*np.abs(exlda)

alpha_0, alpha_1, alpha_T = get_from_dat(data, True)
f_alpha = np.zeros_like(alpha_T)
oma = 1 - alpha_T
f_alpha[alpha_T < 1.0] = np.exp(-0.667*alpha_T[alpha_T < 1.0]/oma[alpha_T < 1.0])
f_alpha[alpha_T > 1.0] = -1.24*np.exp(0.8/oma[alpha_T > 1.0])

beta_0, beta_1, beta_T = get_from_dat(data, True)

# start = (2, -8.5, 11, -5)
start = (-5, 15.5, 1.5, -5)

res = minimize(match_obj, start, args=(beta_T, f_alpha, wts), method="Nelder-Mead")

A, B, C, D = res.x

beta = np.linspace(0, 1, 1000)
func = (1 - 2*beta)*(1 + A*beta + B*beta**2 + C*beta**3 + D*beta**4)

plt.plot(beta, func)
plt.xlim([0,1])
plt.show()