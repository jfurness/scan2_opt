from pyscf import gto,dft
from os import path,system,sys
import yaml

base_dir = './ref_geom/BH6/'
res_dir = './BH6_scf_results'

if not path.isdir(res_dir):
    system('mkdir '+res_dir)

xcfun_imp = {'LSDA': '1.0*SLATERX, 1.0*PW92C', 'PBE': '1.0*PBEX, 1.0*PBEC',
    'r2SCAN': '1.0*R2SCANX, 1.0*R2SCANC', 'SCAN': '1.0*SCANX, 1.0*SCANC',
    'ASCAN': 'ASCANX, ASCANC'}
libxc_imp = {'LSDA': '1.0*LDA_X, 1.0*LDA_C_PW', 'PBE': '1.0*GGA_X_PBE, 1.0*GGA_C_PBE',
    'r2SCAN': '1.0*MGGA_X_R2SCAN, 1.0*MGGA_C_R2SCAN',
    'SCAN': '1.0*MGGA_X_SCAN, 1.0*MGGA_C_SCAN'}

"""
    geometries from Peverati, R.; Truhlar, D. G. J. Chem. Phys. 2011, 135, 191102.
    https://comp.chem.umn.edu/db/dbs/htbh38.html
"""

inpd = {
    'CH3': {'Charge': 0, '2S': 1, 'R': False},
    'CH4': {'Charge': 0, '2S': 0, 'R': True},
    'H': {'Charge': 0, '2S': 1, 'R': False},
    'H2': {'Charge': 0, '2S': 0, 'R': True},
    'H2O': {'Charge': 0, '2S': 0, 'R': True},
    'H2S': {'Charge': 0, '2S': 0, 'R': True},
    'HS': {'Charge': 0, '2S': 1, 'R': False},
    'O': {'Charge': 0, '2S': 2, 'R': False},
    'OH': {'Charge': 0, '2S': 1, 'R': False},
    'TS4': {'Charge': 0, '2S': 1, 'R': False},
    'TS12': {'Charge': 0, '2S': 2, 'R': False},
    'TS13': {'Charge': 0, '2S': 1, 'R': False}
}
syslist = [akey for akey in inpd]

levelshift_d = {
    'HS': {'LSDA': 0.1},
    'O': {'ASCAN': 0.1}
}

kcal_to_kJ = 4.184
eH_to_eV = 27.211386245988
eV_to_kcalmol = 23.06054783061903
eH_to_kcalmol = eH_to_eV*eV_to_kcalmol

"""
LT03:
    Original reference data (in kcal/mol) from Table 1 of
    B.J. Lynch and D.G. Truhlar, J. Phys. Chem. Lett. A 107, 8996 (2003),
    doi: 10.1021/jp035287b,
    and correction, ibid. 108, 1460 (2004),
    doi: 10.1021/jp0379190

HK12_NREL:
    Reference values (in kJ/mol) are the nonrelativistic (NREL) values and
    nonrelativistic frozen-core (NREL_FC) values from Table 4 of:
    R. Haunschild and W. Klopper, Theor. Chem. Acc. 131, 1112 (2012),
    doi:10.1007/s00214-012-1112-3
"""

benchmarks = {
    'LT03': {
        'OH + CH4 -> CH3 + H2O': 6.7,
        'CH3 + H2O -> OH + CH4': 19.6,
        'H + OH -> O + H2': 10.7,
        'O + H2 -> H + OH': 13.1,
        'H + H2S -> H2 + HS': 3.6,
        'H2 + HS -> H + H2S': 17.3
    },

    'HK12_NREL': {
        'OH + CH4 -> CH3 + H2O': 25.7,
        'CH3 + H2O -> OH + CH4': 81.7,
        'H + OH -> O + H2': 45.3,
        'O + H2 -> H + OH': 54.6,
        'H + H2S -> H2 + HS': 16.0,
        'H2 + HS -> H + H2S': 71.4
    }

}

benchmarks['HK12_NREL'] = {akey: benchmarks['HK12_NREL'][akey]/kcal_to_kJ for akey in  benchmarks['HK12_NREL'] }

nsys = 6


def get_bh(en_d):
    rxd = {
        'OH + CH4 -> CH3 + H2O': en_d['TS4'] - en_d['OH'] - en_d['CH4'],
        'CH3 + H2O -> OH + CH4': en_d['TS4'] - en_d['CH3'] - en_d['H2O'],
        'H + OH -> O + H2': en_d['TS12'] - en_d['OH'] - en_d['H'],
        'O + H2 -> H + OH': en_d['TS12'] - en_d['O'] - en_d['H2'],
        'H + H2S -> H2 + HS': en_d['TS13'] - en_d['H'] - en_d['H2S'],
        'H2 + HS -> H + H2S': en_d['TS13'] - en_d['H2'] - en_d['HS']
    }
    for abh in rxd:
        rxd[abh] *= eH_to_kcalmol

    err_d = {}
    for refset in benchmarks:
        me = 0.0
        mae = 0.0
        for abh in rxd:
            tmperr = rxd[abh] - benchmarks[refset][abh]
            me += tmperr
            mae += abs(tmperr)
        err_d['ME_'+refset] = me / (1.0*nsys)
        err_d['MAE_'+refset] = mae / (1.0*nsys)
    return rxd, err_d

def scf_cycle(sys, dfa, opts={}):

    def_opts = dict(bas='6-311++G(3df,3pd)', tol=1.e-7, max_cycle = 500,
        gridsize = 9, sym = False, xc_lib = 'XCFun')

    for opt in opts:
        def_opts[opt] = opts[opt]

    srep = gto.M(atom=base_dir+sys+'.xyz', basis=def_opts['bas'], \
        symmetry=def_opts['sym'],spin=inpd[sys]['2S'],charge=inpd[sys]['Charge'])

    if inpd[sys]['R']:
        wrep = dft.RKS(srep)
    else:
        wrep = dft.UKS(srep)

    wrep.max_cycle = def_opts['max_cycle']
    wrep.conv_tol = def_opts['tol']
    wrep.grids.level = def_opts['gridsize']

    xcstr = dfa
    if def_opts['xc_lib'] == 'XCFun':
        wrep._numint.libxc = dft.xcfun
        if dfa in xcfun_imp:
            xcstr = xcfun_imp[dfa]
    else:
        if dfa in libxc_imp:
            xcstr = libxc_imp[dfa]
    wrep.xc = xcstr

    wrep.verbose = 1

    if 'levelshift' in def_opts:
        wrep.level_shift = def_opts['levelshift']
    if sys in levelshift_d:
        if dfa in levelshift_d[sys]:
            wrep.level_shift = levelshift_d[sys][dfa]

    econv = wrep.kernel()
    if not wrep.converged:
        print('WARNING: system {:} using DFA {:} not converged'.format(sys,dfa))

    return econv

def BH6_analysis(dfa=None,en_d={}):

    if len(en_d.keys()) == 0:

        en_d = yaml.load(open(res_dir+'/BH6_scf_{:}.yaml'.format(dfa),'r'), \
            Loader=yaml.Loader)['SP']

    rx_d, err_d = get_bh(en_d)

    tstr = 'SP:\n  unit: hartree\n'
    for asys in inpd:
        tstr += '  {:}: {:}\n'.format(asys,en_d[asys])
    tstr += 'BH:\n  unit: kcal/mol\n'
    for asys in rx_d:
        tstr += '  {:}: {:}\n'.format(asys,rx_d[asys])
    tstr += 'Stats:\n  unit: kcal/mol\n'
    tstr += '  ME:\n'
    for refset in benchmarks:
        tstr += '    {:}: {:}\n'.format(refset,err_d['ME_'+refset])
    tstr += '  MAE:\n'
    for refset in benchmarks:
        tstr += '    {:}: {:}\n'.format(refset,err_d['MAE_'+refset])

    with open(res_dir + '/BH6_scf_{:}.yaml'.format(dfa),'w+') as tfl:
        tfl.write(tstr)

    return

def run_BH6(dfa,uopts={}):

    en_d = {}
    for asys in inpd:
        en_d[asys] = scf_cycle(asys, dfa, opts=uopts)

    BH6_analysis(dfa=dfa,en_d=en_d)

    return

if __name__ == "__main__":

    dfa_l = ['LSDA','PBE','SCAN','r2SCAN','ASCAN']

    if len(sys.argv) > 1:
        if sys.argv[1] == 'analysis':
            for dfa in dfa_l:
                BH6_analysis(dfa=dfa,en_d={})
        else:
            print('Unknown option '+sys.argv[1])
        exit()

    for dfa in dfa_l:
        run_BH6(dfa,uopts={})
