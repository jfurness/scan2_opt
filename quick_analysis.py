import numpy as np
import h5py

def run_analysis():
    key_order = ['ETA', 'K1', 'CAX', 'CBX', 'CCX', 'CAC', 'CBC',
    'SAXC', 'NSAXC', 'LZBX','LZBXpe','LZBC','LZBCpe','JSFXC','SRES']

    wfile = './scan2_param.hdf5'

    if wfile.split('.')[-1]=='csv':
        dat = np.genfromtxt(wfile,delimiter=',',skip_header=1)

        ibp = np.argmin(dat[:,-1])
        bp = dat[ibp]

    elif wfile.split('.')[-1]=='hdf5':

        with h5py.File(wfile,'r') as tmpfl:
            dat = np.zeros((len(key_order),tmpfl['ETA'].shape[0]))
            for ikey,akey in enumerate(key_order):
                dat[ikey] = tmpfl[akey]
            if tmpfl.attrs['sorted']:
                bp = dat[1]
            else:
                bp = dat[np.argmin(dat[:,-1])]

    conv = {'CAX': "fx'(0)", 'CBX': 'dp', 'CCX': 'dx', 'CAC': "fc'(0)", 'CBC': 'dc'}
    wfl = open('scan2_best_param.csv','w+')
    wfl.write('Parameter, Value\n')
    for ikey,akey in enumerate(key_order):
        if akey == 'SAXC':
            wfl.write('Norm, RMS PE\n')
        if akey in ['LZBX','LZBC']:
            continue

        if akey in conv:
            wfl.write('{:}, {:}\n'.format(conv[akey],bp[ikey]))
        else:
            wfl.write('{:}, {:}\n'.format(akey,bp[ikey]))
    wfl.close()


    return

if __name__=="__main__":
    run_analysis()
