import numpy as np
from Densities import Atom
import matplotlib.pyplot as plt
from math import pi

plotGrid = np.arange(0.001, 10, 0.01)

clist = ['#377eb8','#e41a1c','#4daf4a','#984ea3']

fullist = ["Li", "N", "Ne", "Na", "P", "Ar", "K", "Cr", "Cu", "As", "Kr", "Xe"]
# fullist = ["Ne"]
fig = plt.figure()
ax = fig.add_subplot(111)
for a in fullist:
    atom = Atom(a)
    d0, d1, g0, g1, t0, t1, l0, l1 = atom.get_densities(plotGrid)

    tueg_0 = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d0**(5.0/3.0)
    tauw_0 = g0**2/(d0*8.0)
    alpha_0 = (t0 - tauw_0)/tueg_0
    beta_0 = (t0 - tauw_0)/(t0 + tueg_0)
    ax.plot(plotGrid[d0 > 1e-10], alpha_0[d0 > 1e-10], label="$\\alpha_{\\uparrow}(r)$", c=clist[0])
    ax.plot(plotGrid[d0 > 1e-10], 2*alpha_0[d0 > 1e-10]/(1 + alpha_0[d0 > 1e-10]), label="$2[\\alpha_{\\uparrow}(r)/(1+\\alpha_{\\uparrow}(r))]$", c=clist[2])
    ax.plot(plotGrid[d0 > 1e-10], 2*beta_0[d0 > 1e-10], label="$2\\beta_{\\uparrow}(r)$", c=clist[1])

    if not np.allclose(d0, d1):
        tueg_1 = 3.0/10.0*(3*pi**2)**(2.0/3.0)*d1**(5.0/3.0)
        tauw_1 = g1**2/(8*d1)
        alpha_1 = (t1 - tauw_1)/tueg_1
        beta_1 = (t1 - tauw_1)/(t1 + tueg_1)
        ax.plot(plotGrid[d1 > 1e-10], alpha_1[d1 > 1e-10], label="$\\alpha_{\\downarrow}(r)$", c=clist[0], ls=':')
        ax.plot(plotGrid[d1 > 1e-10], 2*alpha_1[d1 > 1e-10]/(1 + alpha_1[d1 > 1e-10]), label="$2[\\alpha_{\\downarrow}(r)/(1+\\alpha_{\\downarrow}(r))]$", c=clist[2], ls=':')
        ax.plot(plotGrid[d1 > 1e-10], 2*beta_1[d1 > 1e-10], label="$2\\beta_{\\downarrow}(r)$", c=clist[1], ls=':')
    ax.set_ylim([0,3.0])
    ax.set_xlabel("$r (a_0)$")
    ax.set_xlim([0,10])
    ax.legend()
    ax.set_title(a)
    # plt.show()
    fig.savefig("atomPlots/"+a+".pdf")
    ax.clear()