import sys

with open(sys.argv[1], 'r') as inp:
    flip = False
    for line in inp:
        if not flip:
            one = line.strip()
        else:
            print(one +" "+ line.strip())
        flip = not flip
