import SCAN
import SCAN2
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

def mod_scan2Fx(params, p, beta, alpha):
    p2 = p**2
    b12 = params['B1X']**2

    oma = 1.0 - alpha
    oma2 = oma*oma

    # make HX0
    K0 = 0.1740
    hx0 = 1.0 + K0

    B1X = 0.156632
    B2X = 0.12083
    B3X = 0.5
    muak = 10.0/81.0
    
    # make HX1
    cfb4 = muak**2/params['K1'] - 0.112654

    if cfb4 > 0.0:
        wfac = cfb4*p2*np.exp(-cfb4*p/muak)
    else:
        wfac = cfb4*p2*np.exp(cfb4*p/muak)
    vfac = B1X*p + B2X*oma*np.exp(-B3X*oma2)
    yfac = muak*p + wfac + vfac**2
    hx1 = 1.0 + params['K1'] - params['K1']/(1.0 + yfac/params['K1'])
    # Beta interpolation function
    f_beta = (1 - 2*beta)**3*(params['CONX'] + params['CAX']*beta + params['CBX']*beta**2 + params['CCX']*beta**3 + params['CDX']*beta**4)

    # gx
    p14 = p**(1.0/4.0)
    # print p
    gx = np.ones(p.shape)
    gx[p > 1e-16] = -np.expm1(-0.444/p14[p > 1e-16] - params['A1']/np.sqrt(p[p > 1e-16]))

    # Fx
    Fx = hx1 + f_beta*(hx0 - hx1)
    return Fx*gx

def obj_func(params, xparams, v_p, v_beta, scan_fx, h_grid):
    xparams['K1'] = params[0]
    xparams['B1X'] = params[1]
    xparams['CAX'] = params[2]
    xparams['CBX'] = params[3]
    xparams['CCX'] = params[4]
    xparams['CDX'] = params[5]


    s2_fx = SCAN2.scanFx(xparams, v_p, v_beta)

    diff = np.square(s2_fx - scan_fx)
    obj = np.sum(diff*h_grid)
    print(("{:},"*7).format(obj, xparams['K1'], xparams['B1X'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))

    return obj

def lin_obj_h1x(params, xparams, v_p, v_beta, scan_fx, h):
    xparams['K1'] = abs(params[0])
    xparams['B1X'] = abs(params[1])

    s2 = SCAN2.scanFx(xparams, v_p, v_beta)
    diffs = np.sum(np.square(s2 - scan_fx)*h)
    print(("{:},"*3).format(diffs, xparams['K1'], xparams['B1X']))
    return diffs

def lin_obj_IE(params, xparams, v_p, v_betas, scan_fxs, h):
    xparams['CAX'] = params[0]
    xparams['CBX'] = params[1]
    xparams['CCX'] = params[2]
    xparams['CDX'] = params[3]

    s2 = [0.0, 0.0, 0.0]
    diffs = [0.0, 0.0, 0.0]
    for i in range(3):
        s2[i] = SCAN2.scanFx(xparams, v_p, v_betas[i])
        diffs[i] = np.sum(np.square(s2[i] - scan_fxs[i])*h)

    obj = sum(diffs)/3.0
    print(("{:},"*10).format(obj, diffs[0], diffs[1], diffs[2], xparams['K1'], xparams['B1X'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    return obj

def lin_obj_full(params, xparams, v_p, v_betas, v_alphas):
    xparams['K1'] = abs(params[0])
    # xparams['K1'] = 0.092
    xparams['B1X'] = abs(params[1])
    xparams['CAX'] = params[2]
    xparams['CBX'] = params[3]
    xparams['CCX'] = params[4]
    xparams['CDX'] = params[5]

    s2 = []
    so = []
    for i in range(v_alphas.shape[0]):
        s2.append(SCAN2.scanFx(xparams, v_p, v_betas[i]))
        so.append(SCAN.scanFx(SCAN.DEFAULT_X_PARAMS, v_p, v_alphas[i]))
    
    obj = np.sum((np.array(s2) - np.array(so))**2)/float(v_alphas.shape[0])

    print(("{:},"*7).format(obj, xparams['K1'], xparams['B1X'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))
    return obj


p, p_h = np.linspace(0, 90, 400, retstep=True)
n_p = p.shape[0]
alpha = np.array([0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 2.0, 10.0, 100.0, 1000.0])
n_alpha = alpha.shape[0]

alphas = np.array([np.ones_like(p)*a for a in alpha])

betas = alphas/(alphas + 5*p[None,:]/3.0 + 1.0)

xparams = {}
for key in list(SCAN2.DEFAULT_X_PARAMS.keys()):
    xparams[key] = SCAN2.DEFAULT_X_PARAMS[key]


Twostep = False
if Twostep:
    opt_tup_h1x = (
        0.092,
        32.99
    )
    opt_tup_IE = (
        -56.6,
        237.71,
        168.38,
        -348.84
    )
    print("a1,K1,B1X")
    res = optimize.minimize(lin_obj_h1x, opt_tup_h1x, args=(xparams, p, betas[1], so_fxs[1], p_h), method='Nelder-Mead')
    print("=========================================")

    xparams['K1'] = res.x[0]
    xparams['B1X'] = res.x[1]

    print("obj,a0,a1,a_inf,K1,B1X,CAX,CBX,CCX,CDX")
    # res = optimize.minimize(obj_func, opt_tup, args=(xparams, v_p, v_beta, so_fx, h_grid), method='Nelder-Mead', options={'adaptive':True})
    res = optimize.minimize(lin_obj_IE, opt_tup_IE, args=(xparams, p, betas, so_fxs, p_h), method='Nelder-Mead', options={'adaptive':True})

    print("Final ======================")
    # print ("{:},"*7).format(res.fun, *res.x)
    print(("{:},"*7).format(res.fun, xparams['K1'], xparams['B1X'], xparams['CAX'], xparams['CBX'], xparams['CCX'], xparams['CDX']))

    xparams['CAX'] = res.x[0]
    xparams['CBX'] = res.x[1]
    xparams['CCX'] = res.x[2]
    xparams['CDX'] = res.x[3]

    x = np.sqrt(p)

    fig = plt.figure()
    ax_0 = fig.add_subplot(311)
    ax_0.plot(x, so_fxs[0], label='SCAN[$\\alpha=0$]')
    ax_0.plot(x, SCAN2.scanFx(xparams, p, betas[0]), label='SCAN2', ls='--')
    ax_0.set_xlim([0,max(x)])
    ax_0.set_ylim([0.75,1.5])
    ax_0.set_ylabel("$F_\\mathrm{x}[s, \\alpha=0]$")
    ax_0.legend()

    ax_1 = fig.add_subplot(312)

    ax_1.plot(x, so_fxs[1], label='SCAN[$\\alpha=1$]')
    ax_1.plot(x, SCAN2.scanFx(xparams, p, betas[1]), label='SCAN2', ls='--')
    ax_1.set_xlim([0,max(x)])
    ax_1.set_ylim([0.75,1.5])
    ax_1.set_ylabel("$F_\\mathrm{x}[s, \\alpha=1]$")
    ax_1.legend()

    ax_2 = fig.add_subplot(313)
    ax_2.plot(x, so_fxs[2], label='SCAN[$\\alpha=100,000$]')
    ax_2.plot(x, SCAN2.scanFx(xparams, p, betas[2]), label='SCAN2', ls='--')
    ax_2.set_xlim([0,max(x)])
    ax_2.set_ylim([0.75,1.5])
    ax_2.set_xlabel("s")
    ax_2.set_ylabel("$F_\\mathrm{x}[s, \\alpha=100,000]$")
    ax_2.legend()

    plt.tight_layout()
    plt.show()

else:
    # opt_tup_full = (
    #     SCAN2.DEFAULT_X_PARAMS['K1'],
    #     SCAN2.DEFAULT_X_PARAMS['B1X'],
    #     SCAN2.DEFAULT_X_PARAMS['CAX'],
    #     SCAN2.DEFAULT_X_PARAMS['CBX'],
    #     SCAN2.DEFAULT_X_PARAMS['CCX'],
    #     SCAN2.DEFAULT_X_PARAMS['CDX']
    # )
    opt_tup_full = (
        0.07,
        32.99,
        -56.6,
        237.71,
        168.38,
        -348.84
    )
    res = optimize.minimize(lin_obj_full, opt_tup_full, args=(xparams, p, betas, alphas), method='Nelder-Mead', options={'adaptive':True})

    xparams['K1'] = abs(res.x[0])
    xparams['B1X'] = abs(res.x[1])
    xparams['CAX'] = res.x[2]
    xparams['CBX'] = res.x[3]
    xparams['CCX'] = res.x[4]
    xparams['CDX'] = res.x[5]

    x = np.sqrt(p)

    fig = plt.figure()
    for i in range(2):
        for j in range(5):
            idx = j+i*5
            ax = fig.add_subplot(5, 2, idx+1)
            ax.plot(x, SCAN.scanFx(SCAN.DEFAULT_X_PARAMS, p, alphas[idx]), label='alpha={:}'.format(alphas[idx,0]))
            ax.plot(x, SCAN2.scanFx(xparams, p, betas[idx]), label='SCAN2', ls='--')
            ax.set_xlim([0,max(x)])
            ax.set_ylim([0.75,1.5])
            # ax.set_xlabel("s")
            ax.legend()
    plt.tight_layout()
    plt.show()

