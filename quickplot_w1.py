import numpy as np
import matplotlib.pyplot as plt
import SCAN2

rs = np.logspace(-3,6, 1000)

zeta = 0.0
phi = (np.power(1.0 + zeta, 2.0/3.0) + np.power(1.0 - zeta, 2.0/3.0))/2.0
GAMMA = 0.5198421

elsda = SCAN2.lsda_1(rs, zeta)
w1 = np.expm1(-elsda/(GAMMA*phi**3))

plt.semilogx(rs, w1*rs)
plt.show()
