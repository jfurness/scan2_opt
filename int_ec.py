import numpy
from scipy.linalg import lstsq
from math import log
import SCAN
import SCAN2
from Atomics import get_Ec_for_atoms

"""
Utility module for evaluating correlation funcitonals
"""

# OLD LARGE Z LIMIT

def opt_c1c_lzlc(cparams, res, dens, c1c_start, do_SCAN2):
    ER_TOL = 0.1
    # Some gradient descent search algorithm now, needs a priming step
    cparams['C1C'] = c1c_start
    D_C1C = 1e-3
    c1c_old = cparams['C1C']
    err = get_lzlc_coef(cparams, res, dens, do_SCAN2)
    res['C1C'] = cparams['C1C']
    res['C2C'] = cparams['C2C']
    res['DC'] = cparams['DC']
    res['error'] = err
    if abs(err) <= ER_TOL:
        res['success'] = True
        return

    while abs(err) > ER_TOL and res['ncount'] < 20:
        res['ncount'] += 1
        cparams['C1C'] = cparams['C1C'] + D_C1C

        err_fwd = get_lzlc_coef(cparams, res, dens, do_SCAN2)

        cparams['C1C'] = (err_fwd*c1c_old - err*cparams['C1C'])/(err_fwd - err)
        if cparams['C1C'] < 0:
            cparams['C1C'] = c1c_old/2.0

        err = get_lzlc_coef(cparams, res, dens, do_SCAN2)
        res['error'] = err
        c1c_old = cparams['C1C']
        res['C1C'] = cparams['C1C']
        res['C2C'] = cparams['C2C']
        res['DC'] = cparams['DC']

        if abs(err) <= ER_TOL:
            res['success'] = True
            break


def get_lzlc_coef(cparams, dens, eclist_in=[]):
    if len(eclist_in) >0:
        eclist = eclist_in
    else:
        eclist = get_Ec_for_atoms(dens, cparams)

    bccoef = get_coeff_from_ec(eclist)

    return bccoef

def get_coeff_from_ec(eclist):
    # atoms contains He and we don't want that, ignore element 0

    # Ec = a*Z*ln(Z) + b*Z, where a is LDA coef.
    # We fit Ec/Z - a*ln(Z) vs. 1/n, where n is the principle quantum number
    zlist = [10.0, 18.0, 36.0, 54.0]  # List of nuclear charges

    xl = [1.0/float(x) for x in range(2, 6)]
    acoef_LDA = -0.02072  # Local density linear term
    ylist = []
    for i in range(len(zlist)):
        ylist.append(eclist[i]/zlist[i] - acoef_LDA*log(zlist[i]))

    A = numpy.vstack([xl, numpy.ones(len(zlist))]).T
    slope, bccoef = lstsq(A, numpy.array(ylist))[0]

    return bccoef
